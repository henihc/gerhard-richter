<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="quotes";
$html->menu_admin_sub_selected="add_quote";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




if( !empty( $_GET['quotes_categoryid'] ) ) $url="?quotes_categoryid=".$_GET['quotes_categoryid'];

$query_where_quote=" WHERE quoteid='".$_GET['quoteid']."' ";
$query_quote=QUERIES::query_quotes($db,$query_where_quote,$query_order,$query_limit);
$results=$db->query($query_quote['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);


print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'text_en', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_de', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_fr', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_it', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_zh', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'source_text_en', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'source_text_de', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'source_text_fr', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'source_text_it', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'source_text_zh', { toolbar : 'Small' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-text li a','#div-admin-tabs-info-text .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-source li a','#div-admin-tabs-info-source .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-source-text li a','#div-admin-tabs-info-source-text .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";




    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( $count )
            {
                print "\t<tr>\n";
                    print "\t<td style='text-align:right;' colspan='2'>\n";
                        $values_url=array();
                        $values_url['quoteid']=$row['quoteid'];
                        $values_url['location']=1;
                        $quote_url=UTILS::get_quote_url($db,$values_url);
                        print "\t<a href='".$quote_url['url']."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                    print "\t</td>\n";

                print "\t</tr>\n";
            }

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( !empty($_GET['quotes_categoryid']) ) $quotes_categoryid=$_GET['quotes_categoryid'];
                elseif( $_GET['error'] ) $quotes_categoryid=$_SESSION['new_record']['quotes_categoryid'];
                else $quotes_categoryid=$row['quotes_categoryid'];
                print "\t<th class='th_align_left'>Category</th>\n";
                print "\t<td>\n";
                    $class['select_sub_categories']="select_sub_categories";
                    admin_html::select_sub_categories($db,"quotes_categoryid",$quotes_categoryid,$onchange,$class,$disable,$where);
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year</th>\n";
                print "\t<td><input type='text' name='year' id='year' value='".$row['year']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left' style='vertical-align:top;'>Text</th>\n";
                print "\t<td>\n";
                    print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-text' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_en=$_SESSION['new_record']['text_en'];
                            else $text_en=$row['text_en'];
                            print "\t<textarea name='text_en' id='text_en' rows='25' cols='120'>".$text_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_de=$_SESSION['new_record']['text_de'];
                            else $text_de=$row['text_de'];
                            print "\t<textarea name='text_de' id='text_de' rows='25' cols='120'>".$text_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_fr=$_SESSION['new_record']['text_fr'];
                            else $text_fr=$row['text_fr'];
                            print "\t<textarea name='text_fr' id='text_fr' rows='25' cols='120'>".$text_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_it=$_SESSION['new_record']['text_it'];
                            else $text_it=$row['text_it'];
                            print "\t<textarea name='text_it' id='text_it' rows='25' cols='120'>".$text_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_zh=$_SESSION['new_record']['text_zh'];
                            else $text_zh=$row['text_zh'];
                            print "\t<textarea name='text_zh' id='text_zh' rows='25' cols='120'>".$text_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print"\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Source</th>\n";
                print "\t<td>\n";
                    print "\t<ul id='ul-admin-tabs-source' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-source' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_en=$_SESSION['new_record']['source_en'];
                            else $source_en=$row['source_en'];
                            print "\t<input type='text' name='source_en' id='source_en' value=\"".$source_en."\" class='admin-input-large'>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_de=$_SESSION['new_record']['source_de'];
                            else $source_de=$row['source_de'];
                            print "\t<input type='text' name='source_de' id='source_de' value=\"".$source_de."\" class='admin-input-large'>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_fr=$_SESSION['new_record']['source_fr'];
                            else $source_fr=$row['source_fr'];
                            print "\t<input type='text' name='source_fr' id='source_fr' value=\"".$source_fr."\" class='admin-input-large'>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_it=$_SESSION['new_record']['source_it'];
                            else $source_it=$row['source_it'];
                            print "\t<input type='text' name='source_it' id='source_it' value=\"".$source_it."\" class='admin-input-large'>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_zh=$_SESSION['new_record']['source_zh'];
                            else $source_zh=$row['source_zh'];
                            print "\t<input type='text' name='source_zh' id='source_zh' value=\"".$source_zh."\" class='admin-input-large'>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print"\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Source text</th>\n";
                print "\t<td>\n";
                    print "\t<ul id='ul-admin-tabs-source-text' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-source-text' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_text_en=$_SESSION['new_record']['source_text_en'];
                            else $source_text_en=$row['source_text_en'];
                            //print "\t<input type='text' name='source_text_en' id='source_text_en' value='".$source_text_en."' class='admin-input-large'>\n";
                            print "\t<textarea id='source_text_en' name='source_text_en' cols='121' rows='2'>".$source_text_en."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_text_de=$_SESSION['new_record']['source_text_de'];
                            else $source_text_de=$row['source_text_de'];
                            //print "\t<input type='text' name='source_text_de' id='source_text_de' value='".$source_text_de."' class='admin-input-large'>\n";
                            print "\t<textarea id='source_text_de' name='source_text_de' cols='121' rows='2'>".$source_text_de."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_text_fr=$_SESSION['new_record']['source_text_fr'];
                            else $source_text_fr=$row['source_text_fr'];
                            //print "\t<input type='text' name='source_text_fr' id='source_text_fr' value='".$source_text_fr."' class='admin-input-large'>\n";
                            print "\t<textarea id='source_text_fr' name='source_text_fr' cols='121' rows='2'>".$source_text_fr."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_text_it=$_SESSION['new_record']['source_text_it'];
                            else $source_text_it=$row['source_text_it'];
                            //print "\t<input type='text' name='source_text_it' id='source_text_it' value='".$source_text_it."' class='admin-input-large'>\n";
                            print "\t<textarea id='source_text_it' name='source_text_it' cols='121' rows='2'>".$source_text_it."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $source_text_zh=$_SESSION['new_record']['source_text_zh'];
                            else $source_text_zh=$row['source_text_zh'];
                            //print "\t<input type='text' name='source_text_zh' id='source_text_zh' value='".$source_text_zh."' class='admin-input-large'>\n";
                            print "\t<textarea id='source_text_zh' name='source_text_zh' cols='121' rows='2'>".$source_text_zh."</textarea>";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print"\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);                    
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/quotes/categories/edit/del.php?quotes_categoryid=".$row['quotes_categoryid']."','".addslashes($row['title_en'])."','');\" title='delete this category'>delete</a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/quotes/edit/del.php?quoteid=".$row['quoteid']."";
                        $options_delete['text1']="This quote";
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='quoteid' value='".$row['quoteid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";


    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,9,$row['quoteid']);
    #end




unset($_SESSION['new_record']);

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
