<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$text_en=$db->filter_parameters($_POST['text_en'],1);
$text_de=$db->filter_parameters($_POST['text_de'],1);
$text_fr=$db->filter_parameters($_POST['text_fr'],1);
$text_it=$db->filter_parameters($_POST['text_it'],1);
$text_zh=$db->filter_parameters($_POST['text_zh'],1);
$source_text_en=$db->filter_parameters($_POST['source_text_en'],1);
$source_text_de=$db->filter_parameters($_POST['source_text_de'],1);
$source_text_fr=$db->filter_parameters($_POST['source_text_fr'],1);
$source_text_it=$db->filter_parameters($_POST['source_text_it'],1);
$source_text_zh=$db->filter_parameters($_POST['source_text_zh'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->filter_parameters($_POST);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['text_de'];
$text_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_fr'];
$text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_it'];
$text_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['source_de'];
$source_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['source_fr'];
$source_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['source_it'];
$source_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_QUOTES."(date_created) VALUES(NOW())");  
    $quoteid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $quoteid=$_POST['quoteid'];
} 
else UTILS::redirect("/");

$query="UPDATE ".TABLE_QUOTES." SET 
            quotes_categoryid='".$_POST['quotes_categoryid']."',
            year='".$_POST['year']."',
            text_en='".$text_en."',
            text_de='".$text_de."',
            text_fr='".$text_fr."',
            text_it='".$text_it."',
            text_zh='".$text_zh."',
            source_en='".$_POST['source_en']."',
            source_de='".$_POST['source_de']."',
            source_fr='".$_POST['source_fr']."',
            source_it='".$_POST['source_it']."',
            source_zh='".$_POST['source_zh']."',
            text_search_1='".$text_search_de['text_lower_converted']."',
            text_search_2='".$text_search_de['text_lower_german_converted']."',
            text_search_3='".$text_search_fr['text_lower_converted']."',
            text_search_4='".$text_search_it['text_lower_converted']."',
            source_text_en='".$source_text_en."',
            source_text_de='".$source_text_de."',
            source_text_fr='".$source_text_fr."',
            source_text_it='".$source_text_it."',
            source_text_zh='".$source_text_zh."',
            source_search_1='".$source_search_de['text_lower_converted']."',
            source_search_2='".$source_search_de['text_lower_german_converted']."',
            source_search_3='".$source_search_fr['text_lower_converted']."',
            source_search_4='".$source_search_it['text_lower_converted']."'";

if( $_POST['submit']=="update" ) $query.=" ,date_modify=NOW() ";
$query.=" WHERE quoteid='".$quoteid."' ";

if( $_POST['submit']=="add" )
{
    $db->query($query);

    admin_utils::admin_log($db,1,35,$quoteid);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);

    admin_utils::admin_log($db,2,35,$quoteid);
}
else
{
    admin_utils::admin_log($db,"",35,$_POST['quoteid'],2);
}

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    # RELATION adding
    admin_utils::add_relation($db,9,$quoteid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}

unset($_SESSION['new_record']);

$url="/admin/quotes/edit/?quoteid=".$quoteid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&quotes_categoryid=".$_POST['quotes_categoryid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
//UTILS::redirect("/admin/quotes/edit/?quotes_categoryid=".$_POST['quotes_categoryid']);
?>
