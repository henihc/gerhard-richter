<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="quotes";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( !empty( $_GET['quotes_categoryid'] ) ) $url="?quotes_categoryid=".$_GET['quotes_categoryid'];

    
    $class['select_sub_categories']="select_sub_categories";
    admin_html::select_sub_categories($db,"quotes_categoryid",$_GET['quotes_categoryid'],"onchange=\"location.href='?quotes_categoryid='+document.getElementById('quotes_categoryid').options[selectedIndex].value\"",$class,$disable);


    $query="SELECT *,DATE_FORMAT(date_modify ,'%d %b %Y %H:%i:%s') AS date_modify2,DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2 FROM ".TABLE_QUOTES." ";
    if( !empty($_GET['quotes_categoryid']) ) $query.=" WHERE quotes_categoryid='".$_GET['quotes_categoryid']."' ";
    $query.=" ORDER BY sort ";
    $query_quotes.=$query." LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];

    $results=$db->query($query);
    $count=$db->numrows($results);
    $pages=@ceil($count/$_GET['sp']);
    $results_quotes=$db->query($query_quotes);

    if( $count>0 )
    {

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],"/admin/quotes/?quotes_categoryid=","",$class,false);

        print "\t<table class='admin-table' cellspacing='0' style='width:900px;'>\n";

            print "\t<thead>\n";
                print "\t<tr>\n";
                    print "\t<th>id</th>\n";
                    print "\t<th>year</th>\n";
                    print "\t<th style='width:250px;'>text_en</th>\n";
                    print "\t<th style='width:250px;'>text_de</th>\n";
                    print "\t<th style='width:250px;'>text_fr</th>\n";
                    print "\t<th style='width:250px;'>text_it</th>\n";
                    print "\t<th style='width:250px;'>text_zh</th>\n";
                    print "\t<th style='width:150px;'>source_en</th>\n";
                    print "\t<th style='width:150px;'>source_de</th>\n";
                    print "\t<th style='width:150px;'>source_fr</th>\n";
                    print "\t<th style='width:150px;'>source_it</th>\n";
                    print "\t<th style='width:150px;'>source_zh</th>\n";
                print "\t</tr>\n";            
            print "\t</thead>\n";

            while($row=$db->mysql_array($results_quotes))
            {
                $row=UTILS::html_decode($row);
                print "\t<tr>\n";
                    print "\t<td><a href='/admin/quotes/edit/?quoteid=".$row['quoteid']."&quotes_categoryid=".$row['quotes_categoryid']."' title='Edit this quote' class='a_edit_item' ><u>".$row['quoteid']."</u></a></td>\n";
                    print "\t<td>".$row['year']."</td>\n";
                    print "\t<td>".$row['text_en']."</td>\n";
                    print "\t<td>".$row['text_de']."</td>\n";
                    print "\t<td>".$row['text_fr']."</td>\n";
                    print "\t<td>".$row['text_it']."</td>\n";
                    print "\t<td>".$row['text_zh']."</td>\n";
                    print "\t<td>".$row['source_en']."</td>\n";
                    print "\t<td>".$row['source_de']."</td>\n";
                    print "\t<td>".$row['source_fr']."</td>\n";
                    print "\t<td>".$row['source_it']."</td>\n";
                    print "\t<td>".$row['source_zh']."</td>\n";
                print "\t</tr>\n";

            }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],"/admin/quotes/?quotes_categoryid=","",$class,false);

    }
    else print "\t<p class='error' >No data found!</p>\n";



        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
