<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="quotes";
$html->menu_admin_sub_selected="add_quotes_category";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $query_where_quote=" WHERE quotes_categoryid='".$_GET['quotes_categoryid']."' ";
    $query_quote=QUERIES::query_quotes_categories($db,$query_where_quote,$query_order,$query_limit);
    $results=$db->query($query_quote['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='table_admin_newrecord' cellspacing='0'>\n";

            if( !empty($row['date_modified2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified2']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date Created</td>\n";
                    print "\t<td>".$row['date_created2']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( $_GET['error'] ) $sub_categoryid=$_SESSION['new_record']['sub_categoryid'];
                else $sub_categoryid=$row['sub_categoryid'];
                print "\t<td>Sub Category</td>\n";
                print "\t<td>\n";
                    $where=" AND quotes_categoryid!='".$_GET['quotes_categoryid']."' AND sub_categoryid='' ";
                    $class['select_sub_categories']="select_sub_categories";
                    admin_html::select_sub_categories($db,"sub_categoryid",$sub_categoryid,$onchange,$class,$disable,$where);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_GET['error'] && $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                elseif( $row['enable'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td>Enable</td>\n";
                print "\t<td><input name='enable' $checked type='checkbox' value='1' class='' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_en'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>*Title EN</td>\n";
                if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                else $title_en=$row['title_en'];
                print "\t<td>";
                    print "<input name='title_en' type='text' value='".$title_en."' class='input_field_admin' />";
                    $url="/biography/quotes";
                    if( !empty($row['sub_categoryid']) )
                    { 
                        $results_category=$db->query("SELECT * FROM ".TABLE_QUOTES_CATEGORIES." WHERE quotes_categoryid='".$row['sub_categoryid']."'");
                        $row_category=$db->mysql_array($results_category);

                        $url.="/".$row_category['titleurl_en'];
                        $url.="/".$row['titleurl_en'];
                    }
                    else $url.="/".$row['titleurl_en'];
                    print "&nbsp;<a href='".$url."' title='".$row['title_en']."'><u>".$row['titleurl']."</u></a>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_short_en'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>*Title short EN</td>\n";
                if( $_GET['error'] ) $title_short_en=$_SESSION['new_record']['title_short_en'];
                else $title_short_en=$row['title_short_en'];
                print "\t<td>";
                    print "<input name='title_short_en' type='text' value='".$title_short_en."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title DE</td>\n";
                if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                else $title_de=$row['title_de'];
                print "\t<td>";
                    print "<input name='title_de' type='text' value='".$title_de."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title short DE</td>\n";
                if( $_GET['error'] ) $title_short_de=$_SESSION['new_record']['title_short_de'];
                else $title_short_de=$row['title_short_de'];
                print "\t<td>";
                    print "<input name='title_short_de' type='text' value='".$title_short_de."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title FR</td>\n";
                if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                else $title_fr=$row['title_fr'];
                print "\t<td>";
                    print "<input name='title_fr' type='text' value='".$title_fr."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title short FR</td>\n";
                if( $_GET['error'] ) $title_short_fr=$_SESSION['new_record']['title_short_fr'];
                else $title_short_fr=$row['title_short_fr'];
                print "\t<td>";
                    print "<input name='title_short_fr' type='text' value='".$title_short_fr."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title IT</td>\n";
                if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                else $title_it=$row['title_it'];
                print "\t<td>";
                    print "<input name='title_it' type='text' value='".$title_it."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title short IT</td>\n";
                if( $_GET['error'] ) $title_short_it=$_SESSION['new_record']['title_short_it'];
                else $title_short_it=$row['title_short_it'];
                print "\t<td>";
                    print "<input name='title_short_it' type='text' value='".$title_short_it."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title ZH</td>\n";
                if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                else $title_zh=$row['title_zh'];
                print "\t<td>";
                    print "<input name='title_zh' type='text' value='".$title_zh."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title short ZH</td>\n";
                if( $_GET['error'] ) $title_short_zh=$_SESSION['new_record']['title_short_zh'];
                else $title_short_zh=$row['title_short_zh'];
                print "\t<td>";
                    print "<input name='title_short_zh' type='text' value='".$title_short_zh."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<td>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=19 AND itemid2='".$row['quotes_categoryid']."' ) OR ( typeid2=17 AND typeid1=19 AND itemid1='".$row['quotes_categoryid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_o__imageid_".$imageid.".jpg";
                    $link="/images/size_o__imageid_".$imageid.".jpg";

                    if( !empty($imageid) ) print "\t<img src='".$src."' alt='' width='148' />\n";
                    else print "Category image";
                    print "<br />Height = 93px<br />Width = 148px";
                print "\t</td>\n";
                print "\t<td>";
                    print "<input type='file' name='image' />";
                print "</td>\n";
            print "\t</tr>\n";

                /*
            print "\t<tr>\n";
                print "\t<td>Description EN</td>\n";
                if( $_GET['error'] ) $desc_en=$_SESSION['new_record']['desc_en'];
                else $desc_en=$row['desc_en'];
                print "\t<td><textarea rows='6' cols='75' name='desc_en'>".$desc_en."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Description DE</td>\n";
                if( $_GET['error'] ) $desc_de=$_SESSION['new_record']['desc_de'];
                else $desc_de=$row['desc_de'];
                print "\t<td><textarea rows='6' cols='75' name='desc_de'>".$desc_de."</textarea></td>\n";
            print "\t</tr>\n";
                 */
            print "\t<tr>\n";
                print "\t<td>Admin notes</td>\n";
                if( $_GET['error'] ) $notes=$_SESSION['new_record']['notes'];
                else $notes=$row['notes'];
                print "\t<td><textarea rows='6' cols='35' name='notes'>".$notes."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/quotes/categories/edit/del.php?quotes_categoryid=".$row['quotes_categoryid']."','".addslashes($row['title_en'])."','');\" title='delete this category'>delete</a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/quotes/categories/edit/del.php?quotes_categoryid=".$row['quotes_categoryid']."";
                        $options_delete['text1']="Quotes - Categories - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='quotes_categoryid' value='".$row['quotes_categoryid']."' />\n";
    print "\t</form>\n";

        # printing out relations
        if( $count>0 ) admin_html::show_relations($db,19,$row['quotes_categoryid']);
        #end


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
