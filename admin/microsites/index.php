<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="microsites";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $results=$db->query("SELECT 
        *,
        DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
        DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2 
        FROM ".TABLE_MICROSITES." 
        ORDER BY sort ");
    $count=$db->numrows($results);

    if( $count>0 )
    {
        $ii=1;
        print "\t<br /><br />\n";
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>ID</th>\n";
                print "\t<th>Title EN</th>\n";
                print "\t<th>enable</th>\n";
                print "\t<th>date modified</th>\n";
                print "\t<th>date created</th>\n";
                print "\t<th colspan='2'>&nbsp;</th>\n";
            print "\t</tr>\n";
        print "\t</table>\n";

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {
                print "\t<li id='item_".$row['micrositeid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move'>".$row['micrositeid']."</td>\n";
                            print "\t<td><a href='/admin/microsites/edit/?micrositeid=".$row['micrositeid']."' title='Edit this microsite' class='a_edit_item' ><u>".$row['title_en']."</u></a></td>\n";
                            if( $row['enable'] ) $enable="yes";
                            else $enable="no";
                            print "\t<td>".$enable."</td>\n";
                            print "\t<td>".$row['date_modified2']."</td>\n";
                            print "\t<td>".$row['date_created2']."</td>\n";
                            print "\t<td><a href='/admin/microsites/images/?micrositeid=".$row['micrositeid']."' title='View microsite images'><u>View images</u></a></td>\n";
                            print "\t<td><a href='/admin/microsites/images/edit/?micrositeid=".$row['micrositeid']."' title='Add microsite images'><u>Add images</u></a></td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "\t</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {   
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }   

    }
    else print "\t<p class='error'>No data found!</p>\n";




print "\t</div>\n";



$html->foot();

?>
