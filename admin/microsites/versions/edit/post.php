<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['micrositeid']) ) $_SESSION['new_record']['error']['micrositeid']=true;
if( empty($_POST['number']) ) $_SESSION['new_record']['error']['number']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/microsites/versions/edit/?micrositeid=".$_POST['micrositeid']."&error=true");

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST,1);

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_MICROSITES_VERSIONS," WHERE micrositeid='".$_POST['micrositeid']."' ");
    $db->query("INSERT INTO ".TABLE_MICROSITES_VERSIONS."(sort,date_created) VALUES('".$maxsort."',NOW())");
    $id=$db->return_insert_id();

    admin_utils::admin_log($db,1,32,$id);
}
else 
{
    $id=$_POST['versionid'];

    admin_utils::admin_log($db,2,32,$id);
}


if( isset($_POST['submit']) )
{
    $db->query("UPDATE ".TABLE_MICROSITES_VERSIONS." SET 
        micrositeid='".$_POST['micrositeid']."',
        number='".$_POST['number']."',
        title_en='".$_POST['title_en']."',
        title_de='".$_POST['title_de']."',
        title_fr='".$_POST['title_fr']."',
        title_it='".$_POST['title_it']."',
        title_zh='".$_POST['title_zh']."',
        text_en='".$_POST['text_en']."',
        text_de='".$_POST['text_de']."',
        text_fr='".$_POST['text_fr']."',
        text_it='".$_POST['text_it']."',
        text_zh='".$_POST['text_zh']."',
        date_modified=NOW()
        WHERE versionid='".$id."'");
}

unset($_SESSION['new_record']);



UTILS::redirect("/admin/microsites/versions/edit/?versionid=".$id."&task=".$_POST['submit']);
