<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="microsites";
$html->menu_admin_sub_selected="add_version";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


echo "
<a href='/admin/microsites/versions/?micrositeid=".$_GET['micrositeid']."' title='Microsites versions'><u>Microsites versions</u></a>&nbsp;&nbsp;
<a href='/admin/microsites/versions/edit/?micrositeid=".$_GET['micrositeid']."' title='Add version'><u>Add version</u></a>&nbsp;&nbsp;
<br /><br />";


    $query_where_microsite=" WHERE versionid='".$_GET['versionid']."' ";
    $query_microsite=QUERIES::query_microsites_versions($db,$query_where_microsite,$query_order,$query_limit);
    $results=$db->query($query_microsite['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='table_admin_newrecord' cellspacing='0'>\n";

            if( !empty($row['date_modified2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modify</td>\n";
                    print "\t<td>".$row['date_modified2']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date Created</td>\n";
                    print "\t<td>".$row['date_created2']."</td>\n";
                print "\t</tr>\n";
            } 


            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['number'] ) $class="error";
                else $class="";
                print "\t<td class='$class'>*Microsite</td>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $micrositeid=$_SESSION['new_record']['micrositeid'];
                    elseif( !empty($row['micrositeid']) ) $micrositeid=$row['micrositeid'];
                    else $micrositeid=$_GET['micrositeid'];
                    print admin_html::select_microsites($db,"micrositeid",$micrositeid,"",$class=array(),$disable);
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['number'] ) $class="error";
                else $class="";
                print "\t<td class='$class'>*Number</td>\n";
                if( $_GET['error'] ) $number=$_SESSION['new_record']['number'];
                else $number=$row['number'];
                print "\t<td><input type='text' name='number' id='number' value='".$number."' class='admin_input'></td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Title EN</td>\n";
                print "\t<td><input type='text' name='title_en' id='title_en' value='".$row['title_en']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title DE</td>\n";
                print "\t<td><input type='text' name='title_de' id='title_de' value='".$row['title_de']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title FR</td>\n";
                print "\t<td><input type='text' name='title_fr' id='title_fr' value='".$row['title_fr']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title IT</td>\n";
                print "\t<td><input type='text' name='title_it' id='title_it' value='".$row['title_it']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title ZH</td>\n";
                print "\t<td><input type='text' name='title_zh' id='title_zh' value='".$row['title_zh']."' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td style='vertical-align:top;'>Text EN</td>\n";
                print "\t<td><textarea name='text_en' id='text_en' rows='5' cols='54'>".$row['text_en']."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td style='vertical-align:top;'>Text DE</td>\n";
                print "\t<td><textarea name='text_de' id='text_de' rows='5' cols='54'>".$row['text_de']."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td style='vertical-align:top;'>Text FR</td>\n";
                print "\t<td><textarea name='text_fr' id='text_fr' rows='5' cols='54'>".$row['text_fr']."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td style='vertical-align:top;'>Text IT</td>\n";
                print "\t<td><textarea name='text_it' id='text_it' rows='5' cols='54'>".$row['text_it']."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td style='vertical-align:top;'>Text ZH</td>\n";
                print "\t<td><textarea name='text_zh' id='text_zh' rows='5' cols='54'>".$row['text_zh']."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm('/admin/microsites/versions/edit/del.php?micrositeid=".$row['micrositeid']."&amp;versionid=".$row['versionid']."');\" title='delete this version'>delete</a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/microsites/versions/edit/del.php?micrositeid=".$row['micrositeid']."&amp;versionid=".$row['versionid']."";
                        $options_delete['text1']="Microsite - version - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input type='submit' name='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='versionid' value='".$row['versionid']."' />\n";
    print "\t</form>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
