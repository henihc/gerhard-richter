<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="microsites";
$html->menu_admin_sub_selected="microsite_versions";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




echo "
<a href='/admin/microsites/versions/?micrositeid=".$_GET['micrositeid']."' title='Microsites versions'><u>Microsites versions</u></a>&nbsp;&nbsp;
<a href='/admin/microsites/versions/edit/?micrositeid=".$_GET['micrositeid']."' title='Add version'><u>Add version</u></a>&nbsp;&nbsp;
<br /><br />";

print "Microsite ";
admin_html::select_microsites($db,"micrositeid",$_GET['micrositeid'],"onchange=\"location.href='?micrositeid='+document.getElementById('micrositeid').options[selectedIndex].value\"",$class=array(),$disable);

if( !empty($_GET['micrositeid']) )
{

    $query="SELECT 
        *,
        DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
        DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2 
        FROM ".TABLE_MICROSITES_VERSIONS." 
        WHERE micrositeid='".$_GET['micrositeid']."' 
        ORDER BY sort ";
    $results=$db->query($query);
    $count=$db->numrows($results);

    if( $count>0 )
    {

        $ii=1;
        print "\t<p> ID / Title EN / Date modified / Date created</p><br />\n";
        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            $results=$db->query($query);
            while($row=$db->mysql_array($results))
            {
                print "\t<li id='item_".$row['versionid']."'  >";
                    print "\t<td><a href='/admin/microsites/versions/edit/?versionid=".$row['versionid']."' title='Edit this version' class='a_edit_item' ><u>".$row['versionid']."</u></a></td>\n";
                    print "\t<td>".$row['title_en']."</td>\n";
                    print "\t<td>".$row['date_modified2']."</td>\n";
                    print "\t<td>".$row['date_created2']."</td>\n";
                print "\t</li>\n";

            }

        print "\t</ul>\n";

    for( $i=1;$i<=$ii;$i++ )
    {
        print "\t<script type='text/javascript'>\n";
        print "\tSortable.create('sortlist".$i."',\n";
            print "\t{\n";
                print "\tonUpdate: function()\n";
                print "\t{\n";
                    print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                    print "\t{\n";
                        print "\tmethod: 'post',\n";
                        print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                    print "\t});\n";
                print "\t}\n";
            print "\t});\n";
        print "\t</script>\n";
    }



    }
    else print "\t<p class='error' >No data found!</p>\n";

}

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
