<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['micrositeid']) ) UTILS::redirect("/admin/microsites");

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=3&itemid=".$_GET['micrositeid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="microsites";
$html->menu_admin_sub_selected="add_microsite";

$html->metainfo();

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        # ckeditor text
        print "\tCKEDITOR.replace( 'text_en', { toolbar : 'Large', width : '950px', height : '400px' } );\n";
        print "\tCKEDITOR.replace( 'text_de', { toolbar : 'Large', width : '950px', height : '400px' } );\n";
        print "\tCKEDITOR.replace( 'text_fr', { toolbar : 'Large', width : '950px', height : '400px' } );\n";
        print "\tCKEDITOR.replace( 'text_it', { toolbar : 'Large', width : '950px', height : '400px' } );\n";
        print "\tCKEDITOR.replace( 'text_zh', { toolbar : 'Large', width : '950px', height : '400px' } );\n";
        # tabs tite
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        # tabs desc-small
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc-small li a','#div-admin-tabs-info-desc-small .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        # tabs text
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-text li a','#div-admin-tabs-info-text .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
    # after submit disable submit button
    print "window.addEvent('domready',function() {";
        print "var subber = $$('.submit');";
        print "subber.addEvent('click',function() {";
            print "subber.set('disabled','disabled');";
            print "subber.set('value','Submitting...');";
            //print "(function() { subber.disabled = false; subber.set('value','Resubmit'); }).delay(10000);"; // how much time?  10 seconds
        print "});";
    print "});";

print "\t</script>\n";

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    $query_where_microsite=" WHERE micrositeid='".$_GET['micrositeid']."' ";
    $query_microsite=QUERIES::query_microsites($db,$query_where_microsite,$query_order,$query_limit);
    $results=$db->query($query_microsite['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date Created</td>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                print "\t<td>Enable</td>\n";
                if( $row['enable'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td><input type='checkbox' name='enable' $checked id='enable' value='1' class='admin_input'></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['title_en'];
                            print "\t<textarea class='form' name='title_en' cols='29' rows='3'>".$title_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['title_de'];
                            print "\t<textarea class='form' name='title_de' cols='29' rows='3'>".$title_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['title_fr'];
                            $title_fr=html_entity_decode($title_fr,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='title_fr' cols='29' rows='3'>".$title_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['title_it'];
                            $title_it=html_entity_decode($title_it,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='title_it' cols='29' rows='3'>".$title_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['title_zh'];
                            print "\t<textarea class='form' name='title_zh' cols='29' rows='3'>".$title_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";




            print "\t<tr>\n";
                print "\t<th class='' style='text-align:center;'>\n";
                    if( $count )
                    {
                        $query_related_images="SELECT 
                            COALESCE(
                                r1.itemid1, 
                                r2.itemid2 
                            ) as itemid,
                            COALESCE(
                                r1.sort, 
                                r2.sort 
                            ) as sort_rel, 
                            COALESCE(
                                r1.relationid, 
                                r2.relationid 
                            ) as relationid_rel
                            FROM 
                                ".TABLE_MICROSITES." m 
                            LEFT JOIN 
                                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 3 and r1.itemid2 = m.micrositeid 
                            LEFT JOIN 
                                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 3 and r2.itemid1 = m.micrositeid
                            WHERE m.micrositeid=".$row['micrositeid']."
                            ORDER BY sort_rel ASC, relationid_rel DESC
                            LIMIT 1
                        ";
                        $results_related_image=$db->query($query_related_images);
                        $row_related_image=$db->mysql_array($results_related_image);
                    }
                    if( !empty($row_related_image['itemid']) )
                    {
                        $src="/images/size_s__imageid_".$row_related_image['itemid'].".jpg";
                        $link="/images/size_o__imageid_".$row_related_image['itemid'].".jpg";
                        print "\t<a href='".$link."' title='View large size image' target='blank' >\n";
                            print "\t<img src='".$src."' alt='' />\n";
                        print "\t</a>\n";
                    }
                    else print "Image";
                print "\t</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<input type='file' name='image' value='' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";






            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Preview description text</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-desc-small' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc-small' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_small_en=$_SESSION['new_record']['desc_small_en'];
                            else $desc_small_en=$row['desc_small_en'];
                            print "\t<textarea class='form' name='desc_small_en' cols='29' rows='3'>".$desc_small_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_small_de=$_SESSION['new_record']['desc_small_de'];
                            else $desc_small_de=$row['desc_small_de'];
                            print "\t<textarea class='form' name='desc_small_de' cols='29' rows='3'>".$desc_small_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_small_fr=$_SESSION['new_record']['desc_small_fr'];
                            else $desc_small_fr=$row['desc_small_fr'];
                            $desc_small_fr=html_entity_decode($desc_small_fr,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='desc_small_fr' cols='29' rows='3'>".$desc_small_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_small_it=$_SESSION['new_record']['desc_small_it'];
                            else $desc_small_it=$row['desc_small_it'];
                            $desc_small_it=html_entity_decode($desc_small_it,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='desc_small_it' cols='29' rows='3'>".$desc_small_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_small_zh=$_SESSION['new_record']['desc_small_zh'];
                            else $desc_small_zh=$row['desc_small_zh'];
                            print "\t<textarea class='form' name='desc_small_zh' cols='29' rows='3'>".$desc_small_zh."</textarea>\n";
                        print "\t</div>\n";                        
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Text</th>\n";
                print "\t<td colspan='5'>\n";

                    print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-text' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_en=$_SESSION['new_record']['text_en'];
                            else $text_en=$row['text_en'];
                            print "\t<textarea class='form' name='text_en' cols='29' rows='3'>".$text_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_de=$_SESSION['new_record']['text_de'];
                            else $text_de=$row['text_de'];
                            print "\t<textarea class='form' name='text_de' cols='29' rows='3'>".$text_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_fr=$_SESSION['new_record']['text_fr'];
                            else $text_fr=$row['text_fr'];
                            print "\t<textarea class='form' name='text_fr' cols='29' rows='3'>".$text_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_it=$_SESSION['new_record']['text_it'];
                            else $text_it=$row['text_it'];
                            print "\t<textarea class='form' name='text_it' cols='29' rows='3'>".$text_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_zh=$_SESSION['new_record']['text_zh'];
                            else $text_zh=$row['text_zh'];
                            print "\t<textarea class='form' name='text_zh' cols='29' rows='3'>".$text_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='5'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' id='itemid' name='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td></td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input type='submit' name='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='micrositeid' value='".$row['micrositeid']."' />\n";
    print "\t</form>\n";

    # printing out relations
    if( $count>0 ) admin_html::show_relations($db,3,$row['micrositeid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
