<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;

$text_en=$db->filter_parameters($_POST['text_en'],1);
$text_de=$db->filter_parameters($_POST['text_de'],1);
$text_fr=$db->filter_parameters($_POST['text_fr'],1);
$text_it=$db->filter_parameters($_POST['text_it'],1);
$text_zh=$db->filter_parameters($_POST['text_zh'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->filter_parameters($_POST);

//print_r($_POST);

if( $_POST['submit']=="add" )
{
}
else
{
    $micrositeid=$_POST['micrositeid'];
    $_POST['submit']=="update";
}


if( $_POST['submit']=="update" )
{
    $db->query("UPDATE ".TABLE_MICROSITES." SET 
        title_en='".$_POST['title_en']."',
        title_de='".$_POST['title_de']."',
        title_fr='".$_POST['title_fr']."',
        title_it='".$_POST['title_it']."',
        title_zh='".$_POST['title_zh']."',
        desc_small_en='".$_POST['desc_small_en']."',
        desc_small_de='".$_POST['desc_small_de']."',
        desc_small_fr='".$_POST['desc_small_fr']."',
        desc_small_it='".$_POST['desc_small_it']."',
        desc_small_zh='".$_POST['desc_small_zh']."',
        text_en='".$text_en."',
        text_de='".$text_de."',
        text_fr='".$text_fr."',
        text_it='".$text_it."',
        text_zh='".$text_zh."',
        enable='".$_POST['enable']."',
        date_modified=NOW()
        WHERE micrositeid='".$micrositeid."'");
}

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,30,$micrositeid);
    }
    elseif( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,30,$micrositeid);
    }

    #image upload
    if( $_FILES['image']['error']==0 )
    {
        $fileName = str_replace (" ", "_", $_FILES['image']['name']);
        $tmpName  = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $fileType = $_FILES['image']['type'];
        
        if (is_uploaded_file($tmpName))
        {

            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            $getimagesize = getimagesize($tmpName);
                 
            switch( $getimagesize['mime'] )
            {
                case 'image/gif'  : $ext = ".gif"; break;
                case 'image/png'  : $ext = ".png"; break;
                case 'image/jpeg' : $ext = ".jpg"; break;
                case 'image/bmp'  : $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;


            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT_BOOKS,THUMB_XS_WIDTH_BOOKS,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,SIZE_MICROSITE_SMALL_HEIGHT,SIZE_MICROSITE_SMALL_WIDTH,$dir.'/small/'.$new_file_name);        
            //UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT_BOOKS,THUMB_M_WIDTH_BOOKS,$dir.'/medium/'.$new_file_name);
            //UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT_BOOKS,THUMB_L_WIDTH_BOOKS,$dir.'/large/'.$new_file_name);
            //UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT_BOOKS,THUMB_XL_WIDTH_BOOKS,$dir.'/xlarge/'.$new_file_name);


                #copy original
                if (!copy($tmpName, $new_file_path))
                {
                    exit("Error Uploading File.");
                }
                else
                {
                    $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                    # RELATION adding
                    admin_utils::add_relation($db,17,$imageid,3,$micrositeid,$_POST['relations_sort']);
                    #end RELATION adding
                }
            

        }
    }
    #image upload end

    # RELATION adding
    admin_utils::add_relation($db,3,$micrositeid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}
else
{
    admin_utils::admin_log($db,"",30,$_POST['micrositeid'],2);
}





unset($_SESSION['new_record']);

$url="/admin/microsites/edit/?micrositeid=".$micrositeid."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
