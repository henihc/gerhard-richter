<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['micrositeid']) ) UTILS::redirect("/admin/microsites");

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="microsites";
$html->menu_admin_sub_selected="add_microsite";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



echo "

<a href='/admin/microsites/images/?micrositeid=".$_GET['micrositeid']."&versionid=".$_GET['versionid']."' title='View microsite images'><u>View images</u></a>&nbsp;&nbsp;
<a href='/admin/microsites/images/edit/?micrositeid=".$_GET['micrositeid']."&versionid=".$_GET['versionid']."' title='Add images to microsite'><u>Add images</u></a>&nbsp;&nbsp;
<br /><br />";



        if( $_GET['micrositeid']==1 ) admin_html::select_versions($db,"versionid",$_GET['versionid'],"onchange=\"location.href='?micrositeid=".$_GET['micrositeid']."&versionid='+document.getElementById('versionid').options[selectedIndex].value\"",$class=array(),$disable);

        if( !empty($_GET['versionid']) )
        {
            $query="SELECT
                mi.*,
                DATE_FORMAT(mi.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
                DATE_FORMAT(mi.date_created ,'%d %b %Y %H:%i:%s') AS date_created2
                FROM ".TABLE_MICROSITES_IMAGES." mi
                WHERE mi.micrositeid='".$_GET['micrositeid']."' AND mi.versionid='".$_GET['versionid']."' ORDER BY mi.sort";
        }
        else
        {
            $query="SELECT mi.*,DATE_FORMAT(mi.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,DATE_FORMAT(mi.date_created ,'%d %b %Y %H:%i:%s') AS date_created2 FROM ".TABLE_MICROSITES_IMAGES." mi WHERE mi.micrositeid='".$_GET['micrositeid']."' AND (mi.versionid IS NULL OR mi.versionid=0 OR mi.versionid='') ORDER BY mi.sort";
        }


        $results=$db->query($query);
        $count=$db->numrows($results);

    if( $count>0 )
    {

        $ii=1;
        print "\t<table cellpadding='0' cellspacing='0' class='table_sort_list' >";
            print "\t<tr>";
                print "\t<td>ID</td>\n";
                print "\t<td>Image</td>\n";
                print "\t<td>Title EN</td>\n";
                print "\t<td>Number</td>\n";
                print "\t<td>Date modified</td>\n";
                print "\t<td>Date created</td>\n";
            print "\t</tr>";
        print "\t</table>\n";
        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while($row=$db->mysql_array($results))
            {
                print "\t<li id='item_".$row['imageid']."'  >";
                    print "\t<table cellpadding='0' cellspacing='0' class='table_sort_list' >";
                        print "\t<tr>";
                            print "\t<td><a href='/admin/microsites/images/edit/?imageid=".$row['imageid']."&amp;versionid=".$row['versionid']."&amp;micrositeid=".$row['micrositeid']."' title='Edit this image' class='a_edit_item' ><u>".$row['imageid']."</u></a></td>\n";
                            print "\t<td><img src='".DATA_PATH_DATADIR."/images/microsites/xsmall/".$row['imageid'].".jpg' alt='' /></td>\n";
                            print "\t<td>".$row['title_en']."</td>\n";
                            print "\t<td>".$row['number']."</td>\n";
                            print "\t<td>".$row['date_modified2']."</td>\n";
                            print "\t<td>".$row['date_created2']."</td>\n";
                        print "\t</tr>";
                    print "\t</table>\n";
                print "\t</li>\n";

            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
            print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }

    }



        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
