<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//admin_html::admin_sign_in();

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST);

### image upload
if($_FILES['Filedata']['error']==0)
{
    $fileName = strtolower(str_replace (" ", "_", $_FILES['Filedata']['name']));
    $tmpName  = $_FILES['Filedata']['tmp_name'];
    $fileSize = $_FILES['Filedata']['size'];
    $fileType = $_FILES['Filedata']['type'];

    if ( is_uploaded_file($tmpName) )
    {   

        if( $_POST['submit']=="edit" ) 
        {   
            $imageid=$_POST['imageid'];
        }   
        else 
        {   

            $maxsort=UTILS::max_table_value($db,"sort",TABLE_MICROSITES_IMAGES," WHERE micrositeid='".$_POST['micrositeid']."' AND versionid='".$_POST['versionid']."' ");
            $db->query("INSERT INTO ".TABLE_MICROSITES_IMAGES."(
                micrositeid,
                versionid,
                number,
                title_en,
                title_de,
                title_fr,
                title_it,
                title_zh,
                sort,
                date_created
            ) VALUES(
                '".$_POST['micrositeid']."',
                '".$_POST['versionid']."',
                '".$_POST['number']."',
                '".$_POST['title_en']."',
                '".$_POST['title_de']."',
                '".$_POST['title_fr']."',
                '".$_POST['title_it']."',
                '".$_POST['title_zh']."',
                '".$maxsort."',
                NOW()
            ) ");
            $imageid=$db->return_insert_id();

            admin_utils::admin_log($db,1,31,$imageid);
        }   



        $getimagesize = getimagesize($tmpName);

        switch( $getimagesize['mime'] )
        {   
            case 'image/gif' : $imgSrc = imagecreatefromgif($tmpName); $ext = ".gif"; break;
            case 'image/png' : $imgSrc = imagecreatefrompng($tmpName); $ext = ".png"; break;
            case 'image/jpeg' : $imgSrc = imagecreatefromjpeg($tmpName); $ext = ".jpg"; break;
            case 'image/bmp' : $imgSrc = UTILS::imagecreatefrombmp($tmpName); $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }

        $dir=DATA_PATH."/images/microsites";
        $new_file_name=$imageid.$ext;
        $new_file_path=$dir."/original/".$new_file_name;


        UTILS::resize_image($tmpName,$fileName,SIZE_XSMALL_4900_COLOURS_HEIGHT,SIZE_XSMALL_4900_COLOURS_WIDTH,$dir.'/xsmall/'.$imageid.".jpg");
        UTILS::resize_image($tmpName,$fileName,SIZE_SMALL_4900_COLOURS_HEIGHT,SIZE_SMALL_4900_COLOURS_WIDTH,$dir.'/small/'.$imageid.".jpg");
        UTILS::resize_image($tmpName,$fileName,SIZE_MEDIUM_4900_COLOURS_HEIGHT,SIZE_MEDIUM_4900_COLOURS_WIDTH,$dir.'/medium/'.$imageid.".jpg");
        UTILS::resize_image($tmpName,$fileName,SIZE_LARGE_4900_COLOURS_HEIGHT,SIZE_LARGE_4900_COLOURS_WIDTH,$dir.'/large/'.$imageid.".jpg");

        # copy original
        if (!copy($tmpName, $new_file_path))
        {
            # if an error occurs the file could not
            # be written, read or possibly does not exist
            print "Error Uploading File.";
        }
        else
        {
            print "ok";
            $db->query("UPDATE ".TABLE_MICROSITES_IMAGES." SET src='".$new_file_name."' WHERE imageid='".$imageid."'");
        }
        #end copy original

     }

}



### image upload
if($_FILES['pair_image']['error']==0)
{
    $fileName = strtolower(str_replace (" ", "_", $_FILES['pair_image']['name']));
    $tmpName  = $_FILES['pair_image']['tmp_name'];
    $fileSize = $_FILES['pair_image']['size'];
    $fileType = $_FILES['pair_image']['type'];

    if ( is_uploaded_file($tmpName) )
    {   

        if( $_POST['submit']=="edit" ) 
        {   
            $imageid=$_POST['imageid'];
        }   

        $getimagesize = getimagesize($tmpName);

        switch( $getimagesize['mime'] )
        {   
            case 'image/gif' : $imgSrc = imagecreatefromgif($tmpName); $ext = ".gif"; break;
            case 'image/png' : $imgSrc = imagecreatefrompng($tmpName); $ext = ".png"; break;
            case 'image/jpeg' : $imgSrc = imagecreatefromjpeg($tmpName); $ext = ".jpg"; break;
            case 'image/bmp' : $imgSrc = UTILS::imagecreatefrombmp($tmpName); $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }

        $dir=DATA_PATH."/images/microsites";
        $new_file_name=$imageid.$ext;
        $new_file_path=$dir."/pair/".$new_file_name;

        # copy original
        if (!copy($tmpName, $new_file_path))
        {
            # if an error occurs the file could not
            # be written, read or possibly does not exist
            print "Error Uploading File.";
        }
        else
        {
            print "ok";
        }
        #end copy original

     }

}



if( isset($_POST['submit']) )
{
    
    if( !empty($_POST['imageid']) ) 
    {
        $db->query("UPDATE ".TABLE_MICROSITES_IMAGES." SET 
            versionid='".$_POST['versionid']."',
            number='".$_POST['number']."',
            title_en='".$_POST['title_en']."',
            title_de='".$_POST['title_de']."', 
            title_fr='".$_POST['title_fr']."', 
            title_it='".$_POST['title_it']."', 
            title_zh='".$_POST['title_zh']."', 
            date_modified=NOW() 
            WHERE imageid='".$_POST['imageid']."'");

        admin_utils::admin_log($db,2,31,$_POST['imageid']);
    }
    
    UTILS::redirect("/admin/microsites/images/edit/?imageid=".$_POST['imageid']."&micrositeid=".$_POST['micrositeid']."&versionid=".$_POST['versionid']."&task=".$_POST['submit']);
}
# file upload end



