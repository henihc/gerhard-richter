<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['micrositeid']) ) UTILS::redirect("/admin/microsites");

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# swfupload
$html->css[]="/admin/js/swfupload/swfupload.css";
$html->js[]="/admin/js/swfupload/swfupload.js";
$html->js[]="/admin/js/swfupload/swfupload.queue.js";
$html->js[]="/admin/js/swfupload/fileprogress.js";
$html->js[]="/admin/js/swfupload/handlers.js";

# menu selected
$html->menu_admin_selected="microsites";
$html->menu_admin_sub_selected="add_microsite";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




echo "

<a href='/admin/microsites/images/?micrositeid=".$_GET['micrositeid']."&versionid=".$_GET['versionid']."' title='View microsite images'><u>View images</u></a>&nbsp;&nbsp;
<a href='/admin/microsites/images/edit/?micrositeid=".$_GET['micrositeid']."&versionid=".$_GET['versionid']."' title='Add images to microsite'><u>Add images</u></a>&nbsp;&nbsp;
<br /><br />";

    $results=$db->query("SELECT
        *,
        DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
        DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2
        FROM ".TABLE_MICROSITES_IMAGES."
        WHERE imageid='".$_GET['imageid']."' ");
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    if( !$count )
    {

        print "\t<script type='text/javascript'>\n";

            print "\t\tvar swfu;\n";

            print "\t\twindow.onload = function() {\n";
                print "\t\tvar settings = {\n";
                    print "\t\tflash_url : '/js/swfupload/swfupload.swf',\n";
                    print "\t\tupload_url: '/admin/microsites/images/edit/post/post.php',\n"; // Relative to the SWF file
                    print "\t\tpost_params: {'PHPSESSID' : '".session_id()."', 'title_en' : '', 'title_de' : '', 'title_fr' : '', 'title_it' : '', 'title_zh' : '', 'micrositeid' : '".$_GET['micrositeid']."', 'versionid' : '', 'number' : '' },\n";
                    print "\t\tfile_size_limit : '100 MB',\n";
                    print "\t\tfile_types : '*.jpg;*.gif;*.png,*.bmp',\n";
                    print "\t\tfile_types_description : 'All Files',\n";
                    print "\t\tfile_upload_limit : 0,\n";
                    print "\t\tfile_queue_limit : 0,\n";
                    print "\t\tcustom_settings : {\n";
                        print "\t\tprogressTarget : 'fsUploadProgress',\n";
                        print "\t\tcancelButtonId : 'btnCancel'\n";
                    print "\t\t},\n";
                    print "\t\tdebug: false,\n";

                    // Button settings
                    print "\t\tbutton_image_url: '/g/swfupload.png',\n"; // Relative to the Flash file
                    print "\t\tbutton_width: '65',\n";
                    print "\t\tbutton_height: '29',\n";
                    print "\t\tbutton_placeholder_id: 'spanButtonPlaceHolder',\n";
                    print "\t\tbutton_text: '<span class=\"theFont\">Upload</span>',\n";
                    print "\t\tbutton_text_style: '.theFont { font-size: 16; }',\n";

                    // The event handler functions are defined in handlers.js
                    print "\t\tfile_queued_handler : fileQueued,\n";
                    print "\t\tfile_queue_error_handler : fileQueueError,\n";
                    print "\t\tfile_dialog_complete_handler : fileDialogComplete,\n";
                    print "\t\tupload_start_handler : uploadStart,\n";
                    print "\t\tupload_progress_handler : uploadProgress,\n";
                    print "\t\tupload_error_handler : uploadError,\n";
                    print "\t\tupload_success_handler : uploadSuccess,\n";
                    print "\t\tupload_complete_handler : uploadComplete,\n";
                    print "\t\tqueue_complete_handler : queueComplete\n";  // Queue plugin event
                print "\t\t};\n";

                print "\t\tswfu = new SWFUpload(settings);\n";
            print "\t\t};\n";

        print "\t</script>\n";

    }


        print "\t<form action='post/post.php' method='post' enctype='multipart/form-data' >\n";
            print "\t<table class='table_admin_newrecord' cellspacing='0'>\n";


                if( !empty($row['date_modified2']) )
                {
                    print "\t<tr>\n";
                        print "\t<td>Date modify</td>\n";
                        print "\t<td>".$row['date_modified2']."</td>\n";
                    print "\t</tr>\n";
                }

                if( !empty($row['date_created2']) )
                {
                    print "\t<tr>\n";
                        print "\t<td>Date Created</td>\n";
                        print "\t<td>".$row['date_created2']."</td>\n";
                    print "\t</tr>\n";
                }
;

                if( $_GET['micrositeid']==1 )
                {
                    print "\t<tr>\n";
                        print "\t<td>Version</td>\n";
                        print "\t<td>";
                            if( !empty($row['versionid']) ) $versionid=$row['versionid'];
                            else $versionid=$_GET['versionid'];
                            admin_html::select_versions($db,"versionid",$versionid,"",$class=array(),$disable);
                        print "</td>\n";
                    print "\t</tr>\n";
                }

                print "\t<tr>\n";
                    print "\t<td>Number</td>\n";
                    print "\t<td><input type='text' name='number' id='number' value='".$row['number']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>Image title EN</td>\n";
                    print "\t<td><input type='text' name='title_en' id='title_en' value='".$row['title_en']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>Image title DE</td>\n";
                    print "\t<td><input type='text' name='title_de' id='title_de' value='".$row['title_de']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>Image title FR</td>\n";
                    print "\t<td><input type='text' name='title_fr' id='title_fr' value='".$row['title_fr']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>Image title IT</td>\n";
                    print "\t<td><input type='text' name='title_it' id='title_it' value='".$row['title_it']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>Image title ZH</td>\n";
                    print "\t<td><input type='text' name='title_zh' id='title_zh' value='".$row['title_zh']."' class='input_field_admin' /></td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>\n";
                    if( $count )
                    {
                        print "\t<a href='".DATA_PATH_DATADIR."/images/microsites/large/".$row['imageid'].".jpg' target='blank' title='View image in full screen'>\n";
                            print "\t<img src='".DATA_PATH_DATADIR."/images/microsites/xsmall/".$row['imageid'].".jpg' alt='image".$row['imageid']."' />\n";
                        print "\t</a>\n";
                    }
                        else print "Images";
                    print "\t</td>\n";
                    print "\t<td>\n";

                        if( $count )
                        {
                            //print "\t<input type='file' name='Filedata' />\n";
                        }
                        else
                        {
                            /*
                            print "\t<fieldset class='flash' id='fsUploadProgress'>\n";
                                print "\t<legend>Upload Queue</legend>\n";
                            print "\t</fieldset>\n";
                            print "\t<div id='divStatus'>0 Files Uploaded</div>\n";
                            print "\t<div>\n";
                                print "\t<span id='spanButtonPlaceHolder'></span>\n";
                                print "\t<input id='btnCancel' type='button' value='Cancel All Uploads' onclick='swfu.cancelQueue();' disabled='disabled' style='font-size: 8pt;' />\n";
                            print "\t</div>\n";
                             */
                        }

                    print "\t</td>\n";
                print "\t</tr>\n";

                if( $count && $row['micrositeid']==3 )
                {
                    print "\t<tr>\n";
                        print "\t<td>";
                            print "\t<a href='".DATA_PATH_DATADIR."/images/microsites/pair/".$row['imageid'].".jpg' target='blank' title='View image in full screen'>";
                                print "\t<img src='".DATA_PATH_DATADIR."/images/microsites/pair/".$row['imageid'].".jpg' alt='Pair image' width='50' height='50' />\n";
                            print "</a>\n";
                        print "</td>\n";
                        print "\t<td><input type='file' name='pair_image' /></td>\n";
                    print "\t</tr>\n";
                }



                if( $count )
                {
                    print "\t<tr>\n";
                        print "\t<td>";
                            if( $count )
                            {
                                //print "<a href='#' onclick=\"delete_confirm2('/admin/microsites/images/edit/del.php?imageid=".$row['imageid']."&micrositeid=".$row['micrositeid']."&versionid=".$_GET['versionid']."');\" title='delete this image'>delete</a>";
                                $options_delete=array();
                                $options_delete['url_del']="/admin/microsites/images/edit/del.php?imageid=".$row['imageid']."&micrositeid=".$row['micrositeid']."&versionid=".$_GET['versionid']."";
                                $options_delete['text1']="Microsite - Image - ".$row['title_en'];
                                //$options_delete['text2']="";
                                //$options_delete['html_return']=1;
                                admin_html::delete_button($db,$options_delete);
                            }
                            else print "* Mandatory";
                        print "</td>\n";
                        if( $count ) $value="edit";
                        else $value="add";
                        print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
                    print "\t</tr>\n";
                }



            print "\t</table>\n";
            print "\t<input type='hidden' name='imageid' value='".$row['imageid']."' />\n";
            //print "\t<input type='hidden' name='versionid' value='".$row['versionid']."' />\n";
            print "\t<input type='hidden' name='micrositeid' value='".$row['micrositeid']."' />\n";
        print "\t</form>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
