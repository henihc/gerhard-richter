<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

$titleurl=UTILS::convert_fortitleurl($db,TABLE_PHOTOS,$_POST['title'],"photoID",$photoid,"titleurl","en");

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_PHOTOS."(date_created) VALUES(NOW())");  
    $photoid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $photoid=$_POST['photoid'];
} 
else UTILS::redirect("/");

$query="UPDATE ".TABLE_PHOTOS." SET 
            title='".$_POST['title']."',
            titleurl='".$titleurl."',
            description='".$_POST['description']."',
            year='".$_POST['year']."',
            notes='".$_POST['notes']."'";
if( $_POST['submit']=="update" ) $query.=" ,date_modified=NOW() ";
$query.=" WHERE photoID='".$photoid."'";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,34,$photoid);
    }
    elseif( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,34,$photoid);
    }

    # RELATION adding
    admin_utils::add_relation($db,8,$photoid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}
else
{
    admin_utils::admin_log($db,"",34,$_POST['photoid'],2);
}


#image upload
if( $_FILES['image']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['image']['name']);
    $tmpName  = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];

	if( is_uploaded_file($tmpName) )
    {
        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

        $getimagesize = getimagesize($tmpName);
                             
        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }   

        $dir=DATA_PATH."/images_new";
        $new_file_name=$imageid.".jpg";
        $new_file_path=$dir."/original/".$imageid.$ext;

		UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
		UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
		UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
		UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
		UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_LLL_HEIGHT,THUMB_LLL_WIDTH,$dir.'/lllarge/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,8,$photoid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o.png";
                $src_cache_uri=$imageid.".png";
                UTILS::cache_uri($src,$src_cache_uri,1);
                #end
            }

	}
}
#end image upload


$url="/admin/photos/edit/?photoid=".$photoid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
