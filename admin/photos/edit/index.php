<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# menu selected
$html->menu_admin_selected="biography";
$html->menu_admin_sub_selected="add_photo";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



$query_where_photo=" WHERE photoID='".$_GET['photoid']."' ";
$query_photo=QUERIES::query_photos($db,$query_where_photo,$query_order,$query_limit);
$results=$db->query($query_photo['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
if( $count>0 ) $row['photoid']=$row['photoID'];



    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($_GET['task']) )
            {
                if( $_GET['task']=="update" ) $text="Update";
                elseif( $_GET['task']=="add" ) $text="Insert ";
                print "\t<tr>\n";
                    print "\t<td colspan='2' style='color:green;'>".$text." successful!</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</th>\n";
                print "\t<td>";
                    print "\t<textarea id='title' name='title' cols='67' rows='2'>".$row['title']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</th>\n";
                print "\t<td>";
                    print "\t<textarea name='description' cols='67' rows='7'>".$row['description']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year</th>\n";
                print "\t<td>";
                    print admin_html::get_years("",$row['year'],"");
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=8 AND itemid2='".$row['photoid']."' ) OR ( typeid2=17 AND typeid1=8 AND itemid1='".$row['photoid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                $row_related_image=$db->mysql_array($results_related_image);
                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                if( empty($imageid) ) $text="Image";
                else
                {
                    $src="/images/size_m__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    $text="<a href='".$link."'>";
                        $text.="<img src='".$src."' alt='' />";
                    $text.="</a>";
                }
                print "\t<th class='th_align_left'>".$text."</th>\n";
                print "\t<td>";
                    print "\t<input type='file' name='image' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";                    
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</th>\n";
                print "\t<td>";
                    print "\t<textarea rows='7' cols='99' name='notes' >".$row['notes']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    print "\t<input type='button' name='delete' value='delete' onclick=\"delete_confirm2('del.php?photoid=".$row['photoid']."')\" />\n";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='photoid' value='".$row['photoid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,8,$row['photoid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
