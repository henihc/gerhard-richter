CKEDITOR.stylesSet.add( 'my_styles',
[
    // Block-level styles
    { name : 'Blue Title', element : 'a', styles : { 'color' : 'green' } },
    { name : 'Red Title' , element : 'h3', styles : { 'color' : 'Red' } },
 
    // Inline styles
    { name : 'CSS Style', element : 'span', attributes : { 'class' : 'my_style' } },
    { name : 'Marker: Yellow', element : 'span', styles : { 'background-color' : 'Yellow' } }
]);
