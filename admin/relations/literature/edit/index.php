<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="literature";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    $query_rel_lit_where=" WHERE relationid_lit='".$_GET['relationid_lit']."' ";
    $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
    $results_rel_lit=$db->query($query_rel_lit['query']);
    $count=$db->numrows($results_rel_lit);
    $row=$db->mysql_array($results_rel_lit);

    if( !$count ) $row['relationid']=$_GET['relationid'];

    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table'>\n";

            if( $_GET['task']=="update" ) print "<tr><td colspan='2' style='color:green'>Update successful!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='2' style='color:green'>Insert successful!</td></tr>";


            print "\t<tr>\n";
                print "\t<td>Options</td>\n";
                print "\t<td>\n";
                    print "\t<a href='/admin/books/edit/?bookid=".$_GET['bookid']."' class='a_admin_edit' >Back to book edit</a>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Rel. mentioned</td>\n";
                print "\t<td>\n";
                    if( $row['show_mentioned'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_MENTIONED."</b> <input type='checkbox' name='show_mentioned' id='show_mentioned' value='1' ".$checked." /><br />\n";
                    print "\t<textarea name='mentioned' cols='90' rows='1'>".$row['mentioned']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
 

            print "\t<tr>\n";
                print "\t<td>Rel. discussed</td>\n";
                print "\t<td>\n";
                    if( $row['show_discussed'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_DISCUSSED."</b> <input type='checkbox' name='show_discussed' id='show_discussed' value='1' ".$checked." /><br />\n";
                    print "\t<textarea name='discussed' cols='90' rows='1'>".$row['discussed']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
 

            print "\t<tr>\n";
                print "\t<td rowspan='4'>Rel. illustrated</td>\n";
                print "\t<td>\n";
                    print "\tBlack and white illustrations<br />\n";
                    print "\t<textarea name='illustrated_bw' cols='90' rows='1'>".$row['illustrated_bw']."</textarea>&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_BW.")\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $row['show_bw'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_ILLUSTRATED."&nbsp;".LANG_ARTWORK_BOOKS_REL_BW."</b> <input type='checkbox' name='show_bw' id='show_bw' value='1' ".$checked." /><br />\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                print "\t<td>\n";
                    print "\tColor illustrations<br />\n";
                    print "\t<textarea name='illustrated' cols='90' rows='1'>".$row['illustrated']."</textarea>&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_COLOUR.")</b>\n";
                    if( $row['illustrated_typeid'] ) $checked=" checked='checked' ";
                    else $checked="";
                    //print "\t<br /><input type='checkbox' ".$checked." value='1' id='illustrated_typeid' name='illustrated_typeid'>\n";
                    //print "\tTo add <b>(color)</b> at the end of page numbers\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $row['show_illustrated'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." ".LANG_ARTWORK_BOOKS_REL_COLOUR."</b> <input type='checkbox' name='show_illustrated' id='show_illustrate' value='1' ".$checked." /><br />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $count && !empty($row['pages']) )
            {
                print "\t<tr>\n";
                    print "\t<td>Pages</td>\n";
                    print "\t<td>\n";
                        print $row['pages'];
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<td>\n";
                    //if( $count>0 ) print "\t<a href='#' title='delete medium' onclick=\"delete_confirm2('/admin/medium/edit/del.php?mediumid=".$row['mID']."')\"><u>delete</u></a>\n";
                    //else print "*Mandatory";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='relationid_lit' value='".$row['relationid_lit']."' />\n";
        print "\t<input type='hidden' name='relationid' value='".$row['relationid']."' />\n";
    print "\t</form>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
