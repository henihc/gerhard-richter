<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_RELATIONS_LITERATURE."(relationid,date_created) VALUES('".$_POST['relationid']."',NOW())");
    $relationid_lit=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $relationid_lit=$_POST['relationid_lit'];
}

$query="UPDATE ".TABLE_RELATIONS_LITERATURE." SET 
            mentioned='".$_POST['mentioned']."',
            show_mentioned='".$_POST['show_mentioned']."',
            discussed='".$_POST['discussed']."',
            show_discussed='".$_POST['show_discussed']."',";
//$query.="   illustrated_typeid='".$_POST['illustrated_typeid']."',";
$query.="   illustrated='".$_POST['illustrated']."',
            illustrated_bw='".$_POST['illustrated_bw']."',
            show_illustrated='".$_POST['show_illustrated']."',
            show_bw='".$_POST['show_bw']."'
        WHERE relationid_lit='".$relationid_lit."'";

if( $_POST['submit']=="add" )
{
    $db->query($query);
    admin_utils::admin_log($db,1,10,$_POST['relationid']);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);
    admin_utils::admin_log($db,2,10,$_POST['relationid']);
}
else
{
    admin_utils::admin_log($db,"",10,$_POST['relationid'],2);
}

$options_relation_lit=array();
$options_relation_lit['relationid']=$_POST['relationid'];
admin_utils::order_literature_pages($db,$relationid_lit,$options_relation_lit);

UTILS::redirect("/admin/relations/literature/edit/?relationid_lit=".$relationid_lit."&bookid=".$_GET['bookid']."&task=".$_POST['submit']);
?>
