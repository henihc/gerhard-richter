<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);

if( !empty($_GET['typeid2']) )
{
    $sort_column=UTILS::get_sort_column($db,$_GET['typeid2']);
    $results=$db->query("SELECT relationid,typeid1,itemid1,typeid2,itemid2,".$sort_column." FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$_GET['typeid1']."' AND itemid1='".$_GET['itemid1']."' AND typeid2='".$_GET['typeid2']."' ) OR ( typeid2='".$_GET['typeid1']."' AND itemid2='".$_GET['itemid1']."' AND typeid1='".$_GET['typeid2']."' ) ORDER BY ".$sort_column." ASC, relationid DESC ");
    $count=$db->numrows($results);

    $i=0;
    print '{';
        if( $_GET['typeid2']==2 ) 
        {
            print '"array'.$i.'": ["by_date_sale", "By date"],';
            $i++;
        }
        elseif( $_GET['typeid2']==4 ) 
        {
            print '"array'.$i.'": ["by_date_exhibitions", "By date"],';
            $i++;
        }
        print '"array'.$i.'": ["", "First"]';
        if( $count>0 ) 
        {
            $i++;
            print ',"array'.($i).'": ["'.($count+1).'", "Last"]';
        }
        if( $_GET['typeid2']==1 || $_GET['typeid2']==13 || $_GET['typeid2']==14 ) 
        {
            $i++;
            print ',"array'.$i.'": ["by_number", "By number"]';
        }
        while( $row=$db->mysql_array($results) )
        {
            $i++;
            if( $i>1 )
            {
                $text="Before - ";
                if( $_GET['typeid2']==1 || $_GET['typeid2']==13 || $_GET['typeid2']==14 )
                {
                    $paintid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $results_painting=$db->query("SELECT artworkID, titleEN, titleDE, number FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' LIMIT 1");
                    $row_painting=$db->mysql_array($results_painting);
                    $text.=$paintid;
                    if( !empty($row_painting['number']) ) $text.=" - ".$row_painting['number'];
                    $row_painting['titleEN']=str_replace("\n", "",$row_painting['titleEN']);
                    $row_painting['titleEN']=str_replace("\r", "",$row_painting['titleEN']);
                    $row_painting['titleDE']=str_replace("\n", "",$row_painting['titleDE']);
                    $row_painting['titleDE']=str_replace("\r", "",$row_painting['titleDE']);
                    if( !empty($row_painting['titleEN']) ) $text.=" - ".$row_painting['titleEN'];
                    if( !empty($row_painting['titleDE']) ) $text.=" - ".$row_painting['titleDE'];
                    //print $row['relationid']."-".$paintid."-".$row[$sort_column]."<br />";
                }
                if( $_GET['typeid2']==2 )
                {
                    $saleid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $query_sale_history="SELECT a.house AS auction_house, s.saleName AS sale_name, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.saleID='".$saleid."'";
                    $results_sale=$db->query($query_sale_history);
                    $row_sale=$db->mysql_array($results_sale);
                    $text.=$saleid;
                    if( !empty($row_sale['sale_date']) ) $text.=" - ".$row_sale['sale_date'];
                    if( !empty($row_sale['auction_house']) ) $text.=" - ".$row_sale['auction_house'];
                    if( !empty($row_sale['sale_name']) ) $text.=" - ".$row_sale['sale_name'];
                }
                if( $_GET['typeid2']==4 )
                {
                    $exhibitionid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $results_exh=$db->query("SELECT title_original, locationEN FROM ".TABLE_EXHIBITIONS." WHERE exID='".$exhibitionid."' LIMIT 1");
                    $row_exh=$db->mysql_array($results_exh);
                    $text.=$exhibitionid;
                    $row_painting['title_original']=str_replace("\n", "",$row_painting['title_original']);
                    $row_painting['title_original']=str_replace("\r", "",$row_painting['title_original']);
                    $row_painting['locationEN']=str_replace("\n", "",$row_painting['locationEN']);
                    $row_painting['locationEN']=str_replace("\r", "",$row_painting['locationEN']);
                    if( !empty($row_exh['title_original']) ) $text.=" - ".$row_exh['title_original'];
                    if( !empty($row_exh['locationEN']) ) $text.=" - ".$row_exh['locationEN'];
                }
                if( $_GET['typeid2']==12 )
                {
                    $bookid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $results_book=$db->query("SELECT title FROM ".TABLE_BOOKS." WHERE id='".$bookid."' LIMIT 1");
                    $row_book=$db->mysql_array($results_book);
                    $text.=$bookid;
                    if( !empty($row_book['title']) ) $text.=" - ".$row_book['title'];
                }
                if( $_GET['typeid2']==15 )
                {
                    $noteid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $results_note=$db->query("SELECT noteEN,noteDE,noteCH FROM ".TABLE_PAINTING_NOTE." WHERE noteid='".$noteid."' LIMIT 1");
                    $row_note=$db->mysql_array($results_note);
                    $text.=$noteid;
                    $row_note['noteEN']=str_replace("\n", "",$row_note['noteEN']);
                    $row_note['noteEN']=str_replace("\r", "",$row_note['noteEN']);
                    $row_note['noteDE']=str_replace("\n", "",$row_note['noteDE']);
                    $row_note['noteDE']=str_replace("\r", "",$row_note['noteDE']);
                    if( !empty($row_note['noteEN']) ) $text.=" - ".$row_note['noteEN'];
                    if( !empty($row_note['noteDE']) ) $text.=" - ".$row_note['noteDE'];
                }
                if( $_GET['typeid2']==16 )
                {
                    $museumid=UTILS::get_relation_id($db,$_GET['typeid2'],$row);
                    $results_museum=$db->query("SELECT museum FROM ".TABLE_MUSEUM." WHERE muID='".$museumid."' LIMIT 1");
                    $row_museum=$db->mysql_array($results_museum);
                    $text.=$museumid;
                    $row_museum['museum']=str_replace("\n", "",$row_museum['museum']);
                    $row_museum['museum']=str_replace("\r", "",$row_museum['museum']);
                    if( !empty($row_museum['museum']) ) $text.=" - ".$row_museum['museum'];
                }

                if( !empty($text) ) print ',"array'.$i.'": ["'.$row[$sort_column].'", "'.$text.'"]';
            }
        }
        //if( $count>0 ) print ',"array'.($i+1).'": ["'.($count+1).'", "Last"]';
    print '}';
}
else
{
    print '{';
        print '"array'.$i.'": ["", "First"]';
    print '}';
}


?>
