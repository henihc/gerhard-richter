<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['typeid1']) || empty($_GET['itemid1']) || empty($_GET['typeid2']) ) UTILS::redirect("/admin");

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="paintings";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $query_where_relations=" WHERE (itemid1='".$_GET['itemid1']."' AND typeid1='".$_GET['typeid1']."' AND typeid2='".$_GET['typeid2']."') OR (typeid1='".$_GET['typeid2']."' AND itemid2='".$_GET['itemid1']."' AND typeid2='".$_GET['typeid1']."') ";
    $sort_column=UTILS::get_sort_column($db,$_GET['typeid2']);
    $query_order=" ORDER BY ".$sort_column." ASC, relationid DESC ";
    
    $query_relations=QUERIES::query_relations($db,$query_where_relations,$query_order);
    $results_relations=$db->query($query_relations['query']);
    $count=$db->numrows($results_relations);

    if( $_SESSION['debug_page'] ) print $query_relations['query'];

    if( $count>0 )
    {
        $ii=1;
        $relation=admin_html::relation($db,$_GET['typeid2']);
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>".$relation['id_name']."</th>\n";
                if( $_GET['typeid2']==1 || $_GET['typeid2']==14 )
                {
                    print "\t<th>number</th>\n";
                    print "\t<th>image</th>\n";
                    print "\t<th>titleEN</th>\n";
                    print "\t<th>titleDE</th>\n";
                    print "\t<th>artwork</th>\n";
                }
                if( $_GET['typeid2']==2 )
                {
                    print "\t<th>sale date</th>\n";
                    print "\t<th>auction house</th>\n";
                    print "\t<th>sale name</th>\n";
                }
                if( $_GET['typeid2']==4 )
                {
                    print "\t<th>title_original</th>\n";
                    print "\t<th>date</th>\n";
                    print "\t<th>type</th>\n";
                    print "\t<th>image</th>\n";
                }
                if( $_GET['typeid2']==10 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>titleEN</th>\n";
                }
                if( $_GET['typeid2']==12 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>title</th>\n";
                    print "\t<th>year</th>\n";
                }
                if( $_GET['typeid2']==6 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>video title EN</th>\n";
                    print "\t<th>text en</th>\n";
                }
                if( $_GET['typeid2']==15 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>noteEN</th>\n";
                    print "\t<th>noteDE</th>\n";
                }
                if( $_GET['typeid2']==16 )
                {
                    print "\t<th>museum</th>\n";
                }
                if( $_GET['typeid2']==17 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>src old</th>\n";
                }
                if( $_GET['typeid2']==23 )
                {
                    print "\t<th>color_code</th>\n";
                    print "\t<th>title EN</th>\n";
                    print "\t<th>title DE</th>\n";
                }
                if( $_GET['typeid2']==24 )
                {
                    print "\t<th>image</th>\n";
                    print "\t<th>type</th>\n";
                }
                //print "\t<th>date created</th>\n";
                print "\t<th>sort number</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n"; 

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results_relations) )
            {
                if( $row['typeid1']==$_GET['typeid2'] ) $itemid=$row['itemid1'];
                elseif( $row['typeid2']==$_GET['typeid2'] ) $itemid=$row['itemid2'];
                print "\t<li id='item_".$row['relationid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">".$itemid."</td>\n";
                            if( $_GET['typeid2']==1 || $_GET['typeid2']==14 )
                            {
                                $query_exh="SELECT artworkID, titleEN, titleDE, number FROM ".TABLE_PAINTING." WHERE paintID='".$itemid."' ";
                                $results_exh=$db->query($query_exh);
                                $row_exh=$db->mysql_array($results_exh);
                                $artwork=UTILS::get_artwork_info($db,$row_exh['artworkID']);
                                print "\t<td>".$row_exh['number']."</td>";
                                print "\t<td>";
                                    print "<img src='/includes/retrieve.image.php?size=xs&paintID=".$itemid."' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_exh['titleEN']."</td>";
                                print "\t<td>".$row_exh['titleDE']."</td>";
                                print "\t<td>".$artwork['artworkEN']."</td>";
                            }
                            if( $_GET['typeid2']==2 )
                            {
                                $query_sale_history="SELECT a.house AS auction_house, s.saleName AS sale_name, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.saleID='".$itemid."'";
                                $results_sale_history=$db->query($query_sale_history);
                                $row_sale_history=$db->mysql_array($results_sale_history);
                                print "\t<td>".$row_sale_history['sale_date']."</td>";
                                print "\t<td>".$row_sale_history['auction_house']."</td>";
                                print "\t<td>".$row_sale_history['sale_name']."</td>";
                            }
                            if( $_GET['typeid2']==4 )
                            {
                                $query_where_exhibitions=" WHERE exID='".$itemid."'";
                                $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions);
                                $query_exhibitions=$query_exhibitions['query'];
                                $results_exhibitions=$db->query($query_exhibitions);
                                $row_exhibitions=$db->mysql_array($results_exhibitions);
                                $date=exhibition_date($db,$row_exhibitions);
                                print "\t<td>";
                                    print $row_exhibitions['title_original'];
                                        if( !empty($row_exhibitions['locationEN']) || !empty($row_exhibitions['locationDE']) )
                                        {
                                            print "\t<span style='color:#666;'>\n";
                                                if( !empty($row_exhibitions['locationEN']) ) print "<br />".$row_exhibitions['locationEN'];
                                                elseif( !empty($row_exhibitions['locationDE']) ) print "<br />".$row_exhibitions['locationDE'];
                                            print "\t</span>\n";
                                        }
                                print "</td>";
                                print "\t<td>".$date."</td>";
                                print "\t<td>".$row_exhibitions['type']."</td>";
                                print "\t<td>";
                                    print "<img src='/includes/retrieve.image.php?size=xs&exID=".$itemid."' alt='' />";
                                print "</td>";
                            }
                            if( $_GET['typeid2']==10 )
                            {
                                $query_where_video=" WHERE v.videoID='".$itemid."' ";
                                $query_video=QUERIES::query_videos($db,$query_where_video);
                                $results_video=$db->query($query_video['query']);
                                $row_video=$db->mysql_array($results_video);
                                print "\t<td>";
                                    print "<img src='/includes/retrieve.image.php?size=m&videoID=".$itemid."' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_video['titleEN']."</td>";
                            }
                            if( $_GET['typeid2']==12 )
                            {
                                $query_where_books=" WHERE id='".$itemid."' ";
                                $query_books=QUERIES::query_books($db,$query_where_books);
                                $results=$db->query($query_books['query']);
                                $count=$db->numrows($results);
                                $row_literature=$db->mysql_array($results,0);

                                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row_literature['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row_literature['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                $row_related_image=$db->mysql_array($results_related_image);
                                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                $src="/images/size_s__imageid_".$imageid.".jpg";

                                print "\t<td>";
                                    print "<img src='".$src."' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_literature['title']."</td>";
                                print "\t<td>".$row_literature['date_display_year']."</td>";
                            }
                            if( $_GET['typeid2']==6 )
                            {
                                $query_where_related_video=" WHERE related_videoid='".$itemid."' ";
                                $query_related_video=QUERIES::query_related_videos($db,$query_where_related_video);
                                $results_related_video=$db->query($query_related_video['query']);
                                $row_related_video=$db->mysql_array($results_related_video);
                                $results_related_videos_video_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=6 AND itemid1='".$itemid."' AND typeid2=10 ) OR ( typeid2=6 AND itemid2='".$itemid."' AND typeid1=10 ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                $row_related_videos_video_relations=$db->mysql_array($results_related_videos_video_relations);
                                $videoid=UTILS::get_relation_id($db,"10",$row_related_videos_video_relations);
                                $query_where_video=" WHERE videoID='".$videoid."' ";
                                $query_video=QUERIES::query_videos($db,$query_where_video);
                                $results_video=$db->query($query_video['query']);
                                $row_video=$db->mysql_array($results_video);
                                print "\t<td style='vertical-align: top;'>";
                                    print "<img src='/includes/retrieve.image.php?size=m&videoID=".$videoid."' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_video['titleEN']."</td>";
                                print "\t<td>".$row_related_video['text_en']."</td>";
                            }
                            if( $_GET['typeid2']==15 )
                            {
                                $query_where_note=" WHERE noteid='".$itemid."' ";
                                $query_note=QUERIES::query_painting_notes($db,$query_where_note);
                                $results_note=$db->query($query_note['query']);
                                $row_note=$db->mysql_array($results_note);
                                print "\t<td>";
                                    print "<img src='/includes/retrieve.image.php?size=ll&thumb=1&noteid=".$itemid."' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_note['noteEN']."</td>";
                                print "\t<td>".$row_note['noteDE']."</td>";
                            }
                            if( $_GET['typeid2']==16 )
                            {
                                $query_where_museum=" WHERE muID='".$itemid."' ";
                                $query_museum=QUERIES::query_museums($db,$query_where_museum);
                                $results_museum=$db->query($query_museum['query']);
                                $row_museum=$db->mysql_array($results_museum);
                                print "\t<td>".$row_museum['museum']."</td>";
                            }
                            if( $_GET['typeid2']==17 )
                            {
                                $query_where_image=" WHERE imageid='".$itemid."' ";
                                $query_image=QUERIES::query_images($db,$query_where_image);
                                $results_image=$db->query($query_image['query']);
                                $row_image=$db->mysql_array($results_image);
                                print "\t<td>";
                                    print "<img src='/images/imageid_".$row_image['imageid']."__size_s.jpg' alt='' />";
                                print "</td>";
                                print "\t<td>".$row_image['src_old']."</td>";
                            }
                            if( $_GET['typeid2']==23 )
                            {
                                $query_where_color=" WHERE colorid='".$itemid."' ";
                                $query_color=QUERIES::query_painting_colors($db,$query_where_color);
                                $results_color=$db->query($query_color['query']);
                                $row_color=$db->mysql_array($results_color);
                                print "\t<td style='background-color:#".$row_color['color_code'].";'>".$row_color['color_code']."</td>";
                                print "\t<td>".$row_color['title_en']."</td>";
                                print "\t<td>".$row_color['title_de']."</td>";
                            }
                            if( $_GET['typeid2']==24 )
                            {
                                $query_where_photo=" WHERE painting_photoid='".$itemid."' ";
                                $query_photo=QUERIES::query_painting_images($db,$query_where_photo);
                                $results_photo=$db->query($query_photo['query']);
                                $row_photo=$db->mysql_array($results_photo);
                                print "\t<td><img src='/images/paintingphotoid_".$row_photo['painting_photoid']."__size_xs.jpg' alt='' /></td>";
                                if( $row_photo['typeid']==1 ) $type=LANG_ARTWORK_PHOTOS_TAB_DEATILS;
                                elseif( $row_photo['typeid']==2 ) $type=LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS;
                                print "\t<td>".$type."</td>";
                            }

                            //print "\t<td>".$row['date_created_admin']."</td>\n";
                            print "\t<td>".$row[$sort_column]."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {    
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('sort.php?count=".$ii."&sort_column=".$sort_column."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }    


    }
    else print "\t<p class='error'>No data found!</p>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
