<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");
$search = $_POST['search'];
$relation = admin_html::relation($db,$_POST['relation_typeid']);
$result = array();

// Some simple validation
if (is_string($search) && strlen($search) > 2 && strlen($search) < 64 && !empty($_POST['relation_typeid']))
{
    $dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_DB, DB_USER, DB_PASSWORD);
    $utf8=strtolower(str_replace("-", "",DB_ENCODEING));
    $dbh->exec("set names ".$utf8);

    #old
    $db=new dbCLASS;
 
    // Building the query
    //if( $_POST['relation_typeid']==1 || $_POST['relation_typeid']==13 || $_POST['relation_typeid']==14 ) $query="SELECT paintID,titleEN,titleDE,number,src FROM ".TABLE_PAINTING." WHERE titleEN LIKE :search OR titleDE LIKE :search OR number LIKE :search";
    if( $_POST['relation_typeid']==1 || $_POST['relation_typeid']==13 || $_POST['relation_typeid']==14 ) 
    {
        //$query="SELECT paintID,titleEN,titleDE,number,src FROM ".TABLE_PAINTING." WHERE titleEN LIKE :search OR titleDE LIKE :search OR number LIKE :search";
        $query="SELECT p.paintID,p.artworkID,p.titleEN,p.titleDE,p.number,p.src FROM ".TABLE_PAINTING." p WHERE ";

        $pattern_number_cr_number_search="/((cr)|(CR))+\W?[0-9]+/";
        $numeric=str_replace("-","",$search);
        if( preg_match($pattern_number_cr_number_search, $search) || is_numeric($numeric) )
        {
            if( preg_match($pattern_number_cr_number_search, $search, $matches) )
            {   
                preg_match("/[0-9]+/", $matches[0], $matches_numbers);
                $search_number=$matches_numbers[0];
            }   
            elseif( is_numeric($numeric) ) $search_number=$search;
            $values_number_search=array();
            $values_number_search['number']=$search_number;
            $numer_search=UTILS::prepare_cr_number_search($db,$values_number_search);

            //$query.=" ".$numer_search;
            $query.=" p.number LIKE '".$search_number."%'";
            //print $query;
        }
        else
        {
            # prepare search keyword
            $values_search=array();
            $values_search['painting_search']=1;
            $values_search['db']=$db;
            $title_search=UTILS::prepear_search($search,$values_search);
            # END prepare search keyword
            
            # prepare match
            $values_search_paintings=array();
            $values_search_paintings['search']=$title_search;
            $query_match=QUERIES_SEARCH::query_search_paintings($db,$values_search_paintings);
            # END prepare match

            $query.=$query_match;
        }
    }
    elseif( $_POST['relation_typeid']==2 ) $query="SELECT saleID,saleName,number FROM ".TABLE_SALEHISTORY." WHERE saleName LIKE :search ";
    elseif( $_POST['relation_typeid']==3 ) $query="SELECT micrositeid,title_en FROM ".TABLE_MICROSITES." WHERE title_en LIKE :search ";
    elseif( $_POST['relation_typeid']==4 ) $query="SELECT exID,title_original FROM ".TABLE_EXHIBITIONS." WHERE title_original LIKE :search OR title_original_search_1 LIKE :search OR title_original_search_2 LIKE :search ";
    elseif( $_POST['relation_typeid']==7 ) $query="SELECT newsID,titleEN FROM ".TABLE_NEWS." WHERE titleEN LIKE :search ";
    elseif( $_POST['relation_typeid']==8 ) $query="SELECT photoID,title,description,year FROM ".TABLE_PHOTOS." WHERE title LIKE :search OR description LIKE :search OR year LIKE :search ";
    elseif( $_POST['relation_typeid']==9 ) $query="SELECT quoteid,text_en FROM ".TABLE_QUOTES." WHERE text_en LIKE :search ";
    elseif( $_POST['relation_typeid']==10 ) $query="SELECT videoID,titleEN,image FROM ".TABLE_VIDEO." WHERE titleEN LIKE :search ";
    elseif( $_POST['relation_typeid']==6 ) $query="SELECT related_videoid,text_en FROM ".TABLE_RELATED_VIDEOS." WHERE text_en LIKE :search ";
    elseif( $_POST['relation_typeid']==11 ) $query="SELECT audioid,tracktitleen,src_img FROM ".TABLE_AUDIO." WHERE tracktitleen LIKE :search ";
    elseif( $_POST['relation_typeid']==12 ) 
    {
        $query="SELECT id,title,author FROM ".TABLE_BOOKS." WHERE title LIKE :search OR title_search_1 LIKE :search OR title_search_2 LIKE :search OR author LIKE :search OR author_search_1 LIKE :search OR author_search_2 LIKE :search ";
    }
    elseif( $_POST['relation_typeid']==15 ) $query="SELECT noteid,src,noteEN,noteDE FROM ".TABLE_PAINTING_NOTE." WHERE noteEN LIKE :search OR noteDE LIKE :search ";
    elseif( $_POST['relation_typeid']==16 ) $query="SELECT muID AS museumid,museum FROM ".TABLE_MUSEUM." WHERE museum LIKE :search OR museum_search_1 LIKE :search OR museum_search_2 LIKE :search ";
    elseif( $_POST['relation_typeid']==17 ) $query="SELECT galleryid,title_en FROM ".TABLE_GALLERIES." WHERE title_en LIKE :search OR title_de LIKE :search ";
    elseif( $_POST['relation_typeid']==23 ) $query="SELECT colorid,title_en,title_de FROM ".TABLE_PAINTING_COLORS." WHERE title_en LIKE :search OR title_de LIKE :search ";
    $stmt = $dbh->prepare($query);
 
    // The % as wildcard
    $search_sql='%'.$search.'%';
    $stmt->bindParam(':search', $search_sql, PDO::PARAM_STR, 12);
    //if( $stmt->execute(array('%' . $search . '%')) )
    //print_r($stmt);
    if( $stmt->execute() )
    {
        $count = $stmt->rowCount();
        // Filling the results with usernames
        $i=0;
        while ( $row = $stmt->fetch() )
        {
            //print_r($row);
            //$result[] = $row[$relation['id_name']];
            if( $_POST['relation_typeid']==1 || $_POST['relation_typeid']==13 || $_POST['relation_typeid']==14 )
            {
                $title="";
                if( !empty($row['src']) ) $title.="<img src='/includes/retrieve.image.php?paintID=".$row['paintID']."&size=xs' alt='' /> - ";
                $title.=$row['number']." - ";
                $title.=UTILS::get_artwork_name($db,$row['artworkID']);
                if( !empty($row['titleEN']) ) $title.=" - ".$row['titleEN'];
                if( !empty($row['titleDE']) ) $title.=" - ".$row['titleDE'];
                $result[] = $title.":::".$row['paintID'];
            }
            elseif( $_POST['relation_typeid']==2 )
            {
                $title=$row['saleName'];
                if( !empty($row['number']) ) $title.=" - ".$row['number'];
                $title.=" - ".$row['saleID'];
                $result[] = $title.":::".$row['saleID'];
            }
            elseif( $_POST['relation_typeid']==3 )
            {
                $title=$row['title_en'];
                $result[] = $title.":::".$row['micrositeid'];
            }
            elseif( $_POST['relation_typeid']==4 )
            {
                $title="";
                if( !empty($row['src']) ) $title.="<img src='/includes/retrieve.image.php?exID=".$row['exID']."&size=xs' alt='' /> - ";
                $title.=$row['title_original'];
                if( !empty($row['locationEN']) ) $title.=" - ".$row['locationEN'];
                $result[] = $title.":::".$row['exID'];
            }
            elseif( $_POST['relation_typeid']==7 )
            {
                $title=$row['titleEN'];
                $title.=" - ".$row['newsID'];
                $result[] = $title.":::".$row['newsID'];
            }
            elseif( $_POST['relation_typeid']==8 )
            {
                $title="<img src='/includes/retrieve.image.php?photoID=".$row['photoID']."&size=t&thumb=1&maxwidth=50&maxheight=50' alt='' />";
                if( !empty($row['title']) ) $title.=" - ".$row['year'];
                if( !empty($row['title']) ) $title.=" - ".$row['title'];
                $title.=" - ".$row['photoID'];
                $result[] = $title.":::".$row['photoID'];
            }
            elseif( $_POST['relation_typeid']==9 )
            {
                $title=strip_tags(mb_substr($row['text_en'], 0, 150));
                $title.=" - ".$row['quoteid'];
                $result[] = $title.":::".$row['quoteid'];
            }
            elseif( $_POST['relation_typeid']==10 )
            {
                $title="<img src='/includes/retrieve.image.php?videoID=".$row['videoID']."&size=t&thumb=1&maxwidth=50&maxheight=50' alt='' /> - ";
                $title.=$row['titleEN'];
                $result[] = $title.":::".$row['videoID'];
            }
            elseif( $_POST['relation_typeid']==6 )
            {
                $stmt_vid_rel = $dbh->prepare("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=6 AND itemid1='".$row['related_videoid']."' AND typeid2=10 ) OR ( typeid2=6 AND itemid2='".$row['related_videoid']."' AND typeid1=10 ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                $stmt_vid_rel->execute();
                $row_vid_rel=$stmt_vid_rel->fetch();
                $videoid=UTILS::get_relation_id($db,"10",$row_vid_rel);
                $title="<img src='/includes/retrieve.image.php?videoID=".$videoid."&size=t&thumb=1&maxwidth=50&maxheight=50' alt='' /> - ";
                $title.=strip_tags(mb_substr($row['text_en'], 0, 150));
                $result[] = $title.":::".$row['related_videoid'];
            }
            elseif( $_POST['relation_typeid']==11 )
            {
                $title="<img src='/includes/retrieve.image.php?audioid=".$row['audioid']."&size=t&thumb=1&maxwidth=50&maxheight=50' alt='' /> - ";
                $title.=$row['tracktitleen'];
                $result[] = $title.":::".$row['audioid'];
            }
            elseif( $_POST['relation_typeid']==12 )
            {
                $title="";
                $stmt_book_img = $dbh->prepare("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC");
                $stmt_book_img->execute();
                $row_book_img=$stmt_book_img->fetch();
                $imageid=UTILS::get_relation_id($db,"17", $row_book_img);
                if( !empty($imageid) ) $title.="<img src='/images/size_xs__imageid_".$imageid.".jpg' alt='' /> - ";
                $title.=$row['title'];
                $title.=" - ".$row['author'];
                $result[] = $title.":::".$row['id'];
            }
            elseif( $_POST['relation_typeid']==15 )
            {
                $title="<img src='/includes/retrieve.image.php?noteid=".$row['noteid']."&size=ll&thumb=1&maxwidth=50&maxheight=50' alt='' /> - ";
                $title.=$row['noteEN'];
                $title.=" - ".$row['noteDE'];
                $result[] = $title.":::".$row['noteid'];
            }
            elseif( $_POST['relation_typeid']==16 )
            {
                $title=$row['museum'];
                $result[] = $title.":::".$row['museumid'];
            }
            elseif( $_POST['relation_typeid']==17 )
            {
                $title=$row['title_en'];
                $result[] = $title.":::".$row['galleryid'];
            }
            elseif( $_POST['relation_typeid']==23 )
            {
                $title=$row['title_en'];
                $title.=" - ".$row['title_de'];
                $result[] = $title.":::".$row['colorid'];
            }
            $i++;
        }
    }
}
//else print "not string";

// Finally the JSON, including the correct content-type
header('Content-type: application/json');
 
echo json_encode($result); // see NOTE!
//print '[{"value":23,"text":"brazil"},{"value":124,"text":"ffffff"},{"value":56,"text":"fgdfgdf"}]';
//print '["text1::23","text2::2","text3::45","text4::8"]';
?>
