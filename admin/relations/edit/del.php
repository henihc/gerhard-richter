<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


if( empty($_GET['relationid']) ) 
{
    admin_utils::admin_log($db,3,10,$_GET['relationid'],2);

    UTILS::redirect("/");
}
else
{
    $results=$db->query("SELECT * FROM ".TABLE_RELATIONS." WHERE relationid='".$_GET['relationid']."' ");
    $row=$db->mysql_array($results);

    # sale id
    if( $row['typeid1']==2 && $row['typeid2']==1 ) 
    {    
        $sale_saleid=$row['itemid1'];
    }    
    if( $row['typeid2']==2 && $row['typeid1']==1 )
    {    
        $sale_saleid=$row['itemid2'];
    }
    if( !empty($sale_saleid) )
    {
        $db->query("UPDATE ".TABLE_SALEHISTORY." SET paintID=NULL WHERE saleID='".$sale_saleid."' ");
    } 
    # END sale id

    # painting image
    if( ( $row['typeid1']==17 && $row['typeid2']==1 ) || ( $row['typeid1']==1 && $row['typeid2']==17 )  ) 
    {
        if( $row['typeid1']==17 && $row['typeid2']==1 ) 
        {    
            $paintid=$row['itemid2'];
        }    
        if( $row['typeid2']==17 && $row['typeid1']==1 )
        {    
            $paintid=$row['itemid1'];
        }        
        $db->query("UPDATE ".TABLE_PAINTING." SET imageid=NULL WHERE paintID='".$paintid."' LIMIT 1 ");
    }
    # END painting image

    # deleting associated painting or atlas
    if( $row['typeid2']==13 || $row['typeid2']==14 )
    {
        # atlas
        if( $row['typeid2']==13 )
        {
            $atlas_paintid=$row['itemid2'];
            $paintid=$row['itemid1'];
            $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND itemid1='".$atlas_paintid."' AND typeid2=14 AND itemid2='".$paintid."' ) OR ( typeid2=1 AND itemid2='".$atlas_paintid."' AND typeid1=14 AND itemid1='".$paintid."' ) LIMIT 1");
        }
        # associated
        elseif( $row['typeid2']==14 )
        {
            $atlas_paintid=$row['itemid1'];
            $paintid=$row['itemid2'];
            $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND itemid1='".$paintid."' AND typeid2=13 AND itemid2='".$atlas_paintid."' ) OR ( typeid2=1 AND itemid2='".$paintid."' AND typeid1=13 AND itemid1='".$atlas_paintid."' ) LIMIT 1");
        }
        //print "typeid1=".$row['typeid1']." | itemid1=".$row['itemid1']." | atlasid=".$atlas_paintid;
        //print "<br />typeid2=".$row['typeid2']." | itemid2=".$row['itemid2']." | paintid=".$paintid;;
        //exit();
    }
    # END deleting associated painting or atlas

    $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE relationid='".$_GET['relationid']."' ");
    $db->query("DELETE FROM ".TABLE_RELATIONS_LITERATURE." WHERE relationid='".$_GET['relationid']."' ");    

    # check if exhibtion has some relation
    if( $row['typeid1']==4 || $row['typeid2']==4 )
    {
        $exhibitionid=UTILS::get_relation_id($db,4,$row);
        $query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
        $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
        $results_exh=$db->query($query_exhibition['query']);
        $row_exh=$db->mysql_array($results,0);

        $values_exhibition_rel=array();
        $values_exhibition_rel['exhibitionid']=$exhibitionid;
        $values_exhibition_rel['src_guide']=$row_exh['src_guide'];
        $relations=UTILS::check_relations($db,$values_exhibition_rel);
        if( $relations )
        {    
            $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=1 WHERE exID = '".$exhibitionid."'");
        }    
        else 
        {    
            $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=0 WHERE exID = '".$exhibitionid."'");
        }    
    }
    # end

    admin_utils::admin_log($db,3,10,$_GET['relationid']);
}



$url=getenv('HTTP_REFERER');
if( !empty($_GET['relation_typeid']) ) $url.="&relation_typeid=".$_GET['relation_typeid'];
$url.="#relations";
UTILS::redirect($url);
?>
