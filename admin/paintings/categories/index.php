<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="categories";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( empty($_GET['lang']) ) $_GET['lang']="en";

$sort_by=UTILS::select_lang($_GET['lang'],"sort");
$sort_how="asc";

    print "Language - ";
    $options_select_languages=array();
    $options_select_languages['name']='lang';
    $options_select_languages['id']='lang';
    $options_select_languages['selectedid']=$_GET['lang'];
    $options_select_languages['onchange']="onchange=\"location.href='?lang='+this.options[selectedIndex].value+'&artworkid='+document.getElementById('artworkid').options[document.getElementById('artworkid').selectedIndex].value\"";
    admin_html::select_languages_new($db,$options_select_languages);

    print "&nbsp;Artwork - ";
    admin_html::select_artworks($db,"artworkid",$_GET['artworkid'],"onchange=\"location.href='?lang='+document.getElementById('lang').options[document.getElementById('lang').selectedIndex].value+'&artworkid='+this.options[selectedIndex].value\"",$class,$disable," WHERE artworkID=1 OR artworkID=2 OR artworkID=13 OR artworkID=6 OR artworkID=15 ",$multiple="",0);
    print "<br /><br />";

    $results=$db->query("SELECT * FROM ".TABLE_CATEGORY." WHERE artworkID='".$_GET['artworkid']."' AND ( sub_catID IS NULL || sub_catID='' || sub_catID=0 ) ORDER BY ".$sort_by." ".$sort_how);
    //$results=$db->query("SELECT * FROM ".TABLE_CATEGORY." ORDER BY ".$sort_by." ".$sort_how);
    $count=$db->numrows($results);

    if( $count>=1 )
    {

        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>catID</th>\n";
                print "\t<th>artworkEN</th>\n";
                print "\t<th>".UTILS::select_lang($_GET['lang'],"category")."</th>\n";
                print "\t<th>enable</th>\n";
                print "\t<th>image</th>\n";
            print "\t</tr>\n";
        print "\t</table>\n";

        $ii=1;
        print "\t<ul id='sortlist".$ii."' class='sortlist'>\n";

        while( $row=$db->mysql_array($results) )
        {
            $row=UTILS::html_decode($row);

            print "\t<li id='item_".$row['catID']."'  >";
                print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                    print "\t<tr>\n";
                        print "\t<td class='td_move'>sort</td>\n";
                        print "\t<td >";
                            print "<a name='".$row['catID']."' href='/admin/paintings/categories/edit/?catid=".$row['catID']."&artworkid=".$_GET['artworkid']."' title='edit this category' class='link_edit'><u>".$row['catID']."</u></a>";
                        print "</td>\n";
                        print "\t<td>";
                            $results_artwork=$db->query("SELECT artworkEN FROM ".TABLE_ARTWORKS." WHERE artworkID='".$row['artworkID']."'");
                            $row_artwork=$db->mysql_array($results_artwork);
                            print $row_artwork['artworkEN'];
                        print "</td>\n";
                        print "\t<td>\n";
                            print $row[UTILS::select_lang($_GET['lang'],"category")];
                        print "</td>\n";
                        print "\t<td>\n";
                            if( $row['enable']==1 ) $enable="yes";
                            else $enable="no";
                            print $enable;
                        print "</td>\n";
                        print "\t<td>\n";
                            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=18 AND itemid2='".$row['catID']."' ) OR ( typeid2=17 AND typeid1=18 AND itemid1='".$row['catID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                            $row_related_image=$db->mysql_array($results_related_image);
                            $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                            $src="/images/size_s__imageid_".$imageid.".jpg";
                            $link="/images/size_l__imageid_".$imageid.".jpg";

                            print "\t<img src='".$src."' alt='' />\n";
                        print "\t</td>\n";
                    print "\t</tr>\n";
                print "\t</table>\n";
            print "</li>\n";

                $results_sub=$db->query("SELECT * FROM ".TABLE_CATEGORY." WHERE artworkID='".$row['artworkID']."' AND sub_catID='".$row['catID']."' ORDER BY ".$sort_by." ".$sort_how);
                $count_sub=$db->numrows($results_sub);

                if( $count_sub>0 )
                {
                    $ii++;
                    print "\t<li>\n";
                        print "\t<ul id='sortlist$ii' class='sortlist'>\n";
                            while($row_sub=$db->mysql_array($results_sub))
                            {
                                print "\t<li id='item_".$row_sub['catID']."'  >";
                                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_sub'>\n";
                                        print "\t<tr>\n";
                                            print "\t<td class='td_move'>sort</td>\n";
                                            print "\t<td >";
                                                print "<a name='".$row_sub['catID']."' href='/admin/paintings/categories/edit/?catid=".$row_sub['catID']."&artworkid=".$_GET['artworkid']."' title='edit this category' class='link_edit'><u>".$row_sub['catID']."</u></a>";
                                            print "</td>\n";
                                            print "\t<td>";
                                                $results_artwork=$db->query("SELECT artworkEN FROM ".TABLE_ARTWORKS." WHERE artworkID='".$row['artworkID']."'");
                                                $row_artwork=$db->mysql_array($results_artwork);
                                                print $row_artwork['artworkEN'];
                                            print "</td>\n";
                                            print "\t<td>\n";
                                                print $row_sub[UTILS::select_lang($_SESSION['lang'],"category")];
                                            print "</td>\n";
                                            print "\t<td>\n";
                                                if( $row_sub['enable']==1 ) $enable="yes";
                                                else $enable="no";
                                                print $enable;
                                            print "</td>\n";
                                            print "\t<td>\n";
                                                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=18 AND itemid2='".$row_sub['catID']."' ) OR ( typeid2=17 AND typeid1=18 AND itemid1='".$row_sub['catID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                                $row_related_image=$db->mysql_array($results_related_image);
                                                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                                $src="/images/size_s__imageid_".$imageid.".jpg";
                                                $link="/images/size_l__imageid_".$imageid.".jpg";
                    
                                                print "\t<img src='".$src."' alt='' />\n";
                                            print "\t</td>\n";
                                        print "\t</tr>\n";
                                    print "\t</table>\n";

                                print "\t</li>\n";
                            }
                        print "\t</ul>\n";
                    print "\t</li>\n";
                }



        }

        print "\t</ul>\n";
        
        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."&lang=".$_GET['lang']."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }

    }
    elseif( !empty($_GET['artworkid']) ) print "\t<p class='error'>No data found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
