<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['categoryEN']) ) $_SESSION['new_record']['error']['categoryEN']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/paintings/categories/edit/?catid=".$_POST['catid']."&artworkid=".$_POST['selected_artworkid']."&error=true");

$db=new dbCLASS;


//$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST,1);

if( $_POST['submit']=="add" )
{

    $db->query("INSERT INTO ".TABLE_CATEGORY."(
        categoryEN)
        VALUES(
        '".$_POST['categoryEN']."')"
        );  
    $catid=$db->return_insert_id();
    admin_utils::admin_log($db,1,5,$catid);
}
elseif( $_POST['submit']=="update" )
{
    $catid=$_POST['catid'];
    admin_utils::admin_log($db,2,5,$catid);
}
else
{
    admin_utils::admin_log($db,"",5,$_POST['catid'],2);
}

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_CATEGORY,$_POST['categoryEN'],"catID",$catid,"titleurl_en","en");

//$categoryEN=$db->db_replace_quotes($db,$_POST['categoryEN']);
//$categoryDE=$db->db_replace_quotes($db,$_POST['categoryDE']);
//$categoryFR=$db->db_replace_quotes($db,$_POST['categoryFR']);
//$categoryIT=$db->db_replace_quotes($db,$_POST['categoryIT']);
//$categoryZH=$db->db_replace_quotes($db,$_POST['categoryZH']);

    $query="UPDATE ".TABLE_CATEGORY." SET
        sub_catID='".$_POST['sub_catID']."',
        artworkID='".$_POST['artworkid']."',
        categoryEN='".$_POST['categoryEN']."',
        categoryDE='".$_POST['categoryDE']."',
        categoryFR='".$_POST['categoryFR']."',
        categoryIT='".$_POST['categoryIT']."',
        categoryZH='".$_POST['categoryZH']."',
        titleurl='".$titleurl_en."',
        descriptionEN='".$_POST['descriptionEN']."',
        descriptionDE='".$_POST['descriptionDE']."',
        descriptionFR='".$_POST['descriptionFR']."',
        descriptionIT='".$_POST['descriptionIT']."',
        descriptionZH='".$_POST['descriptionZH']."',
        type='".$_POST['type']."',
        enable='".$_POST['enable']."'";
    if( $_POST['submit']=="add" )
    {
        $maxsort=UTILS::max_table_value($db,"sortEN",TABLE_CATEGORY,$where);
        $query.=" ,
            sortEN='".$maxsort."',
            sortDE='".$maxsort."',
            sortZH='".$maxsort."' ";
    }
    $query.=" WHERE catID='".$catid."' ";



if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

}


    #image upload
	if($_FILES['image']['error']==0)
	{

	$fileName = str_replace (" ", "_", $_FILES['image']['name']);
	$tmpName  = $_FILES['image']['tmp_name'];
	$fileSize = $_FILES['image']['size'];
	$fileType = $_FILES['image']['type'];

		if (is_uploaded_file($tmpName))
		{

            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            $getimagesize = getimagesize($tmpName);
    
            switch( $getimagesize['mime'] )
            {   
                case 'image/gif'  : $ext = ".gif"; break;
                case 'image/png'  : $ext = ".png"; break;
                case 'image/jpeg' : $ext = ".jpg"; break;
                case 'image/bmp'  : $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }   

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name); 

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,18,$catid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
                //$src_cache_uri=$imageid.".jpg";
                //UTILS::cache_uri($src,$src_cache_uri,1);
                #end
            }

		}

	}
    #end image upload



unset($_SESSION['new_record']);
UTILS::redirect("/admin/paintings/categories/edit/?catid=".$catid."&artworkid=".$_POST['artworkid']."&task=".$_POST['submit']);

?>
