<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_category";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'descriptionEN', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'descriptionDE', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'descriptionFR', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'descriptionIT', { toolbar : 'Small' } );\n";
        print "\tCKEDITOR.replace( 'descriptionZH', { toolbar : 'Small' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc li a','#div-admin-tabs-info-desc .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";


    $results=$db->query("SELECT * FROM ".TABLE_CATEGORY." WHERE catID='".$_GET['catid']."'");
    $count=$db->numrows($results);
    $row=$db->mysql_array($results,0);

    if( !empty($_GET['catid']) && $count==0 ) UTILS::redirect("/admin");

    if( $count==1 ) $value="update";
    else $value="add";
    
    print "\t<h1>".ucwords($value)." category</h1><br />\n";
    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table'>\n";

            if( $_GET['task']=="update" ) print "<tr><td colspan='2' style='color:green'>Update successful!</td></tr>";
            if( $_GET['task']=="insert" ) print "<tr><td colspan='2' style='color:green'>Insert successful!</td></tr>";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td>\n";
                    if( $row['enable'] || $count<=0 ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\t<input type='checkbox' ".$checked." name='enable' value='1' />\n";
                print "\t</td>";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Artwork</th>\n";
                print "\t<td>\n";
                    if( !$count && !empty($_GET['artworkid']) ) $artworkID=$_GET['artworkid'];
                    elseif( $_GET['error'] ) $artworkID=$_SESSION['new_record']['artworkID'];
                    else $artworkID=$row['artworkID'];
                    //print admin_html::get_artworks($db,"",$artworkID,"","");
                    print admin_html::select_artworks($db,"artworkid",$artworkID,"onchange=\"$('sub_catID').load('options_categories.php?artworkid='+this.options[selectedIndex].value);\"",$class,$disable," WHERE artworkID=1 OR artworkID=2 OR artworkID=13 OR artworkID=6 OR artworkID=15 ",$multiple="",0);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sub-Category</th>\n";
                print "\t<td>\n";
                    if( $_GET['error'] ) $sub_catID=$_SESSION['new_record']['sub_catID'];
                    else $sub_catID=$row['sub_catID'];
                    print admin_html::select_categories_admin($db,"sub_catID",$sub_catID,"",$class,$disable," WHERE artworkID='".$_GET['artworkid']."' AND catID!='".$row['catID']."' AND ( sub_catID IS NULL || sub_catID='' || sub_catID=0 ) ");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th style='text-align:center;'>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=18 AND itemid2='".$row['catID']."' ) OR ( typeid2=17 AND typeid1=18 AND itemid1='".$row['catID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_l__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    if( !empty($imageid) ) print "<a href='".$link."' ><img src='".$src."' alt='' /></a>";
                    else print "Image";

                    print "<br />Height - 93px";
                    print "<br />Width - 148px";
                    
                print "\t</th>\n";
                print "\t<td>\n";
                    print "\t<input type='file' name='image' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_SESSION['new_record']['error']['categoryEN'] ) $class="error";
                    else $class="";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab ".$class."' href='#' >*English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $categoryEN=$_SESSION['new_record']['categoryEN'];
                            else $categoryEN=$row['categoryEN'];
                            print "\t<textarea id='categoryEN' name='categoryEN' cols='30' rows='1'>".$categoryEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $categoryDE=$_SESSION['new_record']['categoryDE'];
                            else $categoryDE=$row['categoryDE'];
                            print "\t<textarea id='categoryDE' name='categoryDE' cols='30' rows='1'>".$categoryDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $categoryFR=$_SESSION['new_record']['categoryFR'];
                            else $categoryFR=$row['categoryFR'];
                            print "\t<textarea id='categoryFR' name='categoryFR' cols='30' rows='1'>".$categoryFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $categoryIT=$_SESSION['new_record']['categoryIT'];
                            else $categoryIT=$row['categoryIT'];
                            print "\t<textarea id='categoryIT' name='categoryIT' cols='30' rows='1'>".$categoryIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $categoryZH=$_SESSION['new_record']['categoryZH'];
                            else $categoryZH=$row['categoryZH'];
                            print "\t<textarea id='categoryZH' name='categoryZH' cols='30' rows='1'>".$categoryZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</td>\n";
                print "\t<td colspan='5'>\n";

                    print "\t<ul id='ul-admin-tabs-desc' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea rows='7' cols='99' name='descriptionEN' id='descriptionEN' >".$row['descriptionEN']."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea rows='7' cols='99' name='descriptionDE' id='descriptionDE' >".$row['descriptionDE']."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea rows='7' cols='99' name='descriptionFR' id='descriptionFR' >".$row['descriptionFR']."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea rows='7' cols='99' name='descriptionIT' id='descriptionIT' >".$row['descriptionIT']."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea rows='7' cols='99' name='descriptionZH' id='descriptionZH' >".$row['descriptionZH']."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $count>0 )
                    {
                        //print "\t<input type='button' value='delete' title='Delete this painting' onclick=\"delete_confirm2('del.php/?catid=".$row['catID']."&artworkid=".$_GET['artworkid']."','','Are you sure you wish to delete Category - ".addslashes($row['categoryEN'])."?')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="del.php/?catid=".$row['catID']."&artworkid=".$_GET['artworkid']."";
                        $options_delete['text1']="Painting - Category - ".$row['categoryEN'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                print "\t</td>\n";
                print "\t<td>\n";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                    print "\t\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='catid' value='".$row['catID']."' />\n";
        print "\t<input type='hidden' name='selected_artworkid' value='".$_GET['artworkid']."' />\n";
    print "\t</form>\n";

        # printing out relations
        if( $count>0 ) admin_html::show_relations($db,18,$row['catID']);
        #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
