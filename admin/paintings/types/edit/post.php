<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$_SESSION['new_record']=$_POST;
if( empty($_POST['titleEN']) ) $_SESSION['new_record']['error']['titleEN']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/paintings/types/edit/?artworkid=".$_POST['artworkid']."&error=true");

$db=new dbCLASS;


$desc_en=$db->db_prepare_input($_POST['desc_en'],1);
$desc_de=$db->db_prepare_input($_POST['desc_de'],1);
$desc_fr=$db->db_prepare_input($_POST['desc_fr'],1);
$desc_it=$db->db_prepare_input($_POST['desc_it'],1);
$desc_zh=$db->db_prepare_input($_POST['desc_zh'],1);

$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    /*
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_ARTWORKS,"");
    $db->query("INSERT INTO ".TABLE_ARTWORKS."(sort,date_created) VALUES('".$maxsort."',NOW()) ");
    $artworkid=$db->return_insert_id();
    admin_utils::admin_log($db,1,4,$artworkid);
    */
}
elseif( $_POST['submit']=="update" )
{
    $artworkid=$_POST['artworkid'];
    admin_utils::admin_log($db,2,4,$artworkid);
}
else
{
    admin_utils::admin_log($db,"",4,$_POST['artworkid'],2);
}

//$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_EXHIBITIONS,$_POST['titleEN'],"exID",$artworkid,"titleurl_en","en");

# replace quotes
/*
$title_en=$db->db_replace_quotes($db,$_POST['titleEN']);
$title_de=$db->db_replace_quotes($db,$_POST['titleDE']);
$title_fr=$db->db_replace_quotes($db,$_POST['titleFR']);
$title_it=$db->db_replace_quotes($db,$_POST['titleIT']);
$title_zh=$db->db_replace_quotes($db,$_POST['titleZH']);
*/
# END replace quotes

    $query="UPDATE ".TABLE_ARTWORKS." SET ";

    $query.="artworkEN='".$_POST['titleEN']."',
        artworkDE='".$_POST['titleDE']."',
        artworkFR='".$_POST['titleFR']."',
        artworkIT='".$_POST['titleIT']."',
        artworkZH='".$_POST['titleZH']."',";

    $query.="desc_en='".$desc_en."',
        desc_de='".$desc_de."',
        desc_fr='".$desc_fr."',
        desc_it='".$desc_it."',
        desc_zh='".$desc_zh."'";
    //if( $_POST['submit']=="update" )$query.=", date_modified=NOW() ";
    $query.=" WHERE artworkID='".$artworkid."' ";



//print_r($_POST);
//exit();

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    # RELATION adding
    //admin_utils::add_relation($db,23,$colorid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding

}



unset($_SESSION['new_record']);


UTILS::redirect("/admin/paintings/types/edit/?artworkid=".$artworkid."&task=".$_POST['submit']);
