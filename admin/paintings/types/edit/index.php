<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

#calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";
$html->css[]="/admin/js/calendar/calendar.css";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_type";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    $query_where_artworks=" WHERE artworkID='".$_GET['artworkid']."' ";
    $query_artworks=QUERIES::query_artworks($db,$query_where_artworks,$query_order,$query_limit);
    $results=$db->query($query_artworks['query_without_limit']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'desc_en', { toolbar : 'Large', width : '800px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'desc_de', { toolbar : 'Large', width : '800px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'desc_fr', { toolbar : 'Large', width : '800px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'desc_it', { toolbar : 'Large', width : '800px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'desc_zh', { toolbar : 'Large', width : '800px', height : '200px' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc li a','#div-admin-tabs-info-desc .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";

    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date created</td>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleEN=$_SESSION['new_record']['titleEN'];
                            else $titleEN=$row['artworkEN'];
                            print "\t<textarea class='form' name='titleEN' cols='29' rows='3'>".$titleEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleDE=$_SESSION['new_record']['titleDE'];
                            else $titleDE=$row['artworkDE'];
                            print "\t<textarea class='form' name='titleDE' cols='29' rows='3'>".$titleDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleFR=$_SESSION['new_record']['titleFR'];
                            else $titleFR=$row['artworkFR'];
                            $titleFR=html_entity_decode($titleFR,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='titleFR' cols='29' rows='3'>".$titleFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleIT=$_SESSION['new_record']['titleIT'];
                            else $titleIT=$row['artworkIT'];
                            $titleIT=html_entity_decode($titleIT,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='titleIT' cols='29' rows='3'>".$titleIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleZH=$_SESSION['new_record']['titleZH'];
                            else $titleZH=$row['artworkZH'];
                            print "\t<textarea class='form' name='titleZH' cols='29' rows='3'>".$titleZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<ul id='ul-admin-tabs-desc' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='desc_en' id='desc_en' cols='110' rows='7'>".$row['desc_en']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='desc_de' id='desc_de' cols='110' rows='7'>".$row['desc_de']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='desc_fr' id='desc_fr' cols='110' rows='7'>".$row['desc_fr']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='desc_it' id='desc_it' cols='110' rows='7'>".$row['desc_it']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='desc_zh' id='desc_zh' cols='110' rows='7'>".$row['desc_zh']."</textarea>";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    //if( $count ) print "<a href='#' onclick=\"delete_confirm2('/admin/paintings/colors/edit/del.php?colorid=".$row['colorid']."');\" title='delete this color'>delete</a>";
                    //else print "* Mandatory";
                    print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='artworkid' value='".$row['artworkID']."' />\n";
    print "\t</form>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
