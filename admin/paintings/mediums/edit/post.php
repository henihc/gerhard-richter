<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;

$medium_en=$db->db_prepare_input($_POST['mediumEN'],1);
$medium_de=$db->db_prepare_input($_POST['mediumDE'],1);
$medium_fr=$db->db_prepare_input($_POST['mediumFR'],1);
$medium_it=$db->db_prepare_input($_POST['mediumIT'],1);
$medium_zh=$db->db_prepare_input($_POST['mediumZH'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST,1);

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_MEDIA."(sort) VALUES(1) ");
    $mediumid=$db->return_insert_id();
    admin_utils::admin_log($db,1,7,$mediumid);
}
elseif( $_POST['submit']=="update" )
{
    $mediumid=$_POST['mediumid'];
    admin_utils::admin_log($db,2,7,$mediumid);
}
else
{
    admin_utils::admin_log($db,"",7,$_POST['mediumid'],2);
}

$query="UPDATE ".TABLE_MEDIA." SET 
            mediumEN='".$medium_en."',
            mediumDE='".$medium_de."',
            mediumFR='".$medium_fr."',
            mediumIT='".$medium_it."',
            mediumZH='".$medium_zh."'
        WHERE mID='".$mediumid."'";

if( $_POST['submit']=="add" )
{
    $db->query($query);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);
}


UTILS::redirect("/admin/paintings/mediums/edit/?mediumid=".$mediumid."&task=".$_POST['submit']);
?>
