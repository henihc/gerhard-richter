<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['get']['sp']=20;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

#tabs
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
//$html->js[]="/admin/js/ckeditor/ckeditor.js";
//$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_medium";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $results=$db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mID='".$_GET['mediumid']."'");
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        //print "\tCKEDITOR.replace( 'mediumEN', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        //print "\tCKEDITOR.replace( 'mediumDE', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        //print "\tCKEDITOR.replace( 'mediumFR', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        //print "\tCKEDITOR.replace( 'mediumIT', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        //print "\tCKEDITOR.replace( 'mediumZH', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
    print "\t};\n";
print "\t</script>\n";


    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table'>\n";

            if( $_GET['task']=="update" ) print "<tr><td colspan='2' style='color:green'>Update successful!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='2' style='color:green'>Insert successful!</td></tr>";

            print "\t<tr>\n";
                print "\t<td>MediumEN</td>\n";
                print "\t<td>\n";
                    $mediumEN=html_entity_decode($row['mediumEN'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea name='mediumEN' id='mediumEN' cols='90' rows='1'>".$mediumEN."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            
            print "\t<tr>\n";
                print "\t<td>MediumDE</td>\n";
                print "\t<td>\n";
                    $mediumDE=html_entity_decode($row['mediumDE'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea name='mediumDE' id='mediumDE' cols='90' rows='1'>".$mediumDE."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            
            print "\t<tr>\n";
                print "\t<td>MediumFR</td>\n";
                print "\t<td>\n";
                    $mediumFR=html_entity_decode($row['mediumFR'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea name='mediumFR' id='mediumFR' cols='90' rows='1'>".$mediumFR."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>MediumIT</td>\n";
                print "\t<td>\n";
                    $mediumIT=html_entity_decode($row['mediumIT'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea name='mediumIT' id='mediumIT' cols='90' rows='1'>".$mediumIT."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>MediumZH</td>\n";
                print "\t<td>\n";
                    $mediumZH=html_entity_decode($row['mediumZH'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea name='mediumZH' id='mediumZH' cols='90' rows='1'>".$mediumZH."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $count>0 )
                    {
                        //print "\t<input type='button' value='delete' title='Delete this painting' onclick=\"delete_confirm2('/admin/paintings/mediums/edit/del.php?mediumid=".$row['mID']."','','Are you sure you wish to delete Medium - ".addslashes($row['mediumEN'])."?')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/paintings/mediums/edit/del.php?mediumid=".$row['mID']."";
                        $options_delete['text1']="Painting - Medium - ".$row['mediumEN'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "*Mandatory";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='mediumid' value='".$row['mID']."' />\n";
    print "\t</form>\n";

    $results_used=$db->query("SELECT paintID,number, titleEN, titleDE FROM ".TABLE_PAINTING." WHERE mID='".$row['mID']."' ORDER BY sort2, titleEN ASC ");
    $count_used=$db->numrows($results_used);
    if( $count_used>0 && $count )
    {
        print "\t<ul class='tabsadmin' id='ul_link_tabs'>\n";
            print "\t<li class='Images tab'>\n";
                print "\t<h4 style='top: 0px;'><a class='Images name selected' name='Images' href='#Images'>Paintings</a></h4>\n";
            print "\t</li>\n";
        print "\t</ul>\n";
        print "\t<br class='clear'>\n";
        print "\t<div class='tabsadmin-info'>\n";
            print "\t<div id='' class='holder Colors-info hide'>\n";
                print "\t&nbsp;items found: ".$count_used."\n";
                print "\t<table class='table-links'>\n";
                    print "\t<tbody>\n";
                        print "\t<tr>\n";
                            print "\t<th style='width:70px;text-align:center;'>paintid</th>\n";
                            print "\t<th style=''>number</th>\n";
                            print "\t<th style=''>title en</th>\n";
                            print "\t<th style=''>title de</th>\n";
                        print "\t</tr>\n";
                        while( $row_used=$db->mysql_array($results_used,0) )
                        {
                            print "\t<tr>\n";
                                print "\t<td style='width:70px;text-align:center;'>\n";
                                    print "\t<a title='edit this item' href='/admin/paintings/edit/?paintid=".$row_used['paintID']."'><u>".$row_used['paintID']."</u></a>\n";
                                print "\t</td>\n";
                                print "\t<td style='text-align:center;'>\n";
                                    print $row_used['number'];
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    print $row_used['titleEN'];
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    print $row_used['titleDE'];
                                print "\t</td>\n";
                            print "\t</tr>\n";
                        }
                    print "\t</tbody>\n";
                print "\t</table>\n";
            print "\t</div>\n";
        print "\t</div>\n";


    }

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
