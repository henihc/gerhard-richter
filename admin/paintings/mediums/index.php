<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=20;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="mediums";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
{
    $_GET['sort_by']="mID";
    $_GET['sort_how']="asc";
} 

    $query="SELECT * FROM ".TABLE_MEDIA." ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    if( $_GET['sp']!='all' ) $query_media=$query." LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    else $query_media=$query;
    $results=$db->query($query);
    $count=$db->numrows($results);
    $pages=@ceil($count/$_GET['sp']);
    $results=$db->query($query_media);

    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        else $url="?p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);
        print "\t<a href='/admin/paintings/mediums/?sp=all&sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."' title='Show all'>Show all</a>\n";

        print "\t<table class='admin-table'>\n";
            print "\t<tr>\n";
                //1
                if( $_GET['sort_by']=='mID' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mID&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by medium ID'><u>mediumid</u></a></th>";
                //2
                if( $_GET['sort_by']=='mediumEN' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mediumEN&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by mediumEN'><u>mediumEN</u></a></th>";
                //3
                if( $_GET['sort_by']=='mediumDE' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mediumDE&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by mediumDE'><u>mediumDE</u></a></th>";
                //4
                if( $_GET['sort_by']=='mediumFR' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mediumFR&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by mediumFR'><u>mediumFR</u></a></th>";
                //5
                if( $_GET['sort_by']=='mediumIT' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mediumIT&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by mediumIT'><u>mediumIT</u></a></th>";
                //6
                if( $_GET['sort_by']=='mediumZH' )
                {
                    if( $_GET['sort_how']=='asc' ) $sort='desc';
                    else $sort='asc';
                }
                else $sort='asc';
                print "<th><a href='?sort_by=mediumZH&sort_how=".$sort."&sp=".$_GET['sp']."' title='Order by mediumZH'><u>mediumZH</u></a></th>";
                print "<th>used on site</th>";
            print "\t</tr>\n";

            while( $row=$db->mysql_array($results) )
            {
                $row=UTILS::html_decode($row);
                $results_used=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE mID='".$row['mID']."' ");
                $count_used=$db->numrows($results_used);
                print "\t<tr>\n";
                    print "\t<td style='text-align:center;'><a href='/admin/paintings/mediums/edit/?mediumid=".$row['mID']."' title='edit this medium'><u>".$row['mID']."</u></a></td>\n";
                    print "\t<td>".$row['mediumEN']."</td>\n";
                    print "\t<td>".$row['mediumDE']."</td>\n";
                    print "\t<td>".$row['mediumFR']."</td>\n";
                    print "\t<td>".$row['mediumIT']."</td>\n";
                    print "\t<td>".$row['mediumZH']."</td>\n";
                    print "\t<td style='text-align:center;'>".$count_used."</td>\n";
                print "\t</tr>\n";
            }
        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);
    
    }
    else print "\t<p class='error'>No data found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
