<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/js/sort/prototype.js";
$html->js[]="/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="colors";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();





    $query_order=" ORDER BY sort ASC ";
    $query_colors=QUERIES::query_painting_colors($db,$query_where_colors,$query_order,$query_limit);   
    $results=$db->query($query_colors['query_without_limit']);
    $count=$db->numrows($results);

    if( $count>0 )
    {   
        $ii=1;
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>ID</th>\n";
                print "\t<th>Title EN</th>\n";
                print "\t<th>Title DE</th>\n";
                print "\t<th>Title FR</th>\n";
                print "\t<th>Title IT</th>\n";
                print "\t<th>Title ZH</th>\n";
                print "\t<th>date modified</th>\n";
                print "\t<th>date created</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n";    

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {

                if( !empty($row['color_code']) ) $style="style='background:#".$row['color_code'].";'";
                else $style="";
                print "\t<li id='item_".$row['colorid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">".$row['colorid']."</td>\n";
                            print "\t<td>";
                                print "<a name='".$row['colorid']."' href='/admin/paintings/colors/edit/?colorid=".$row['colorid']."' title='edit product info' class='link_edit'>".$row['title_en']."</a>";
                            print "</td>\n";
                            print "\t<td>".$row['title_de']."</td>\n";
                            print "\t<td>".$row['title_fr']."</td>\n";
                            print "\t<td>".$row['title_it']."</td>\n";
                            print "\t<td>".$row['title_zh']."</td>\n";
                            print "\t<td><span>".$row['date_modified_admin']."</span></td>\n";
                            print "\t<td><span>".$row['date_created_admin']."</span></td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";

            }

        print "\t</ul>\n";

    }

    if( $count<=0 && $count_all<=0 ) print "\t<p class='error'>No data found!</p>\n";

    for( $i=1;$i<=$ii;$i++ )
    {
        print "\t<script type='text/javascript'>\n";
        print "\tSortable.create('sortlist".$i."',\n";
            print "\t{\n";
                print "\tonUpdate: function()\n";
                print "\t{\n";
                    print "\tnew Ajax.Request('edit/sorting.php?count=".$ii."',\n";
                    print "\t{\n";
                        print "\tmethod: 'post',\n";
                        print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                    print "\t});\n";
                print "\t}\n";
            print "\t});\n";
        print "\t</script>\n";
    }

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
