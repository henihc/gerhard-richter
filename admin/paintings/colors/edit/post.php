<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) ) $_SESSION['new_record']['error']['title_en']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/paintings/colors/edit/?colorid=".$_POST['colorid']."&error=true");

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_PAINTING_COLORS,"");
    $db->query("INSERT INTO ".TABLE_PAINTING_COLORS."(sort,date_created) VALUES('".$maxsort."',NOW()) ");
    $colorid=$db->return_insert_id();
    admin_utils::admin_log($db,1,8,$colorid);
}
elseif( $_POST['submit']=="update" )
{
    $colorid=$_POST['colorid'];
    admin_utils::admin_log($db,2,8,$colorid);
}
else
{
    admin_utils::admin_log($db,"",8,$_POST['colorid'],2);
}

    $query="UPDATE ".TABLE_PAINTING_COLORS." SET 
        color_code='".$_POST['color_code']."',
        title_en='".$_POST['title_en']."',
        title_de='".$_POST['title_de']."',
        title_fr='".$_POST['title_fr']."',
        title_it='".$_POST['title_it']."',
        title_zh='".$_POST['title_zh']."'";
    if( $_POST['submit']=="update" )$query.=", date_modified=NOW() ";
    $query.=" WHERE colorid='".$colorid."' ";



//print_r($_POST);
//exit();

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    # RELATION adding
    admin_utils::add_relation($db,23,$colorid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding

}



unset($_SESSION['new_record']);


UTILS::redirect("/admin/paintings/colors/edit/?colorid=".$colorid."&task=".$_POST['submit']);
