<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# colorpicker
$html->js[]="/admin/js/jscolor/jscolor.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=23&itemid=".$_GET['colorid'];

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_color";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $query_where_colors=" WHERE colorid='".$_GET['colorid']."' ";
    $query_colors=QUERIES::query_painting_colors($db,$query_where_colors,$query_order,$query_limit);
    $results=$db->query($query_colors['query_without_limit']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='table_admin_newrecord' cellspacing='0'>\n";

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date created</td>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            if( !empty($row['color_code']) ) $style="style='background:".$row['color_code'].";'";
            else $style="";

            print "\t<tr>\n";
                print "\t<td >Color code</td>\n";
                if( $_GET['error'] ) $color_code=$_SESSION['new_record']['color_code'];
                else $color_code=$row['color_code'];
                print "\t<td>";
                    print "<input name='color_code' type='text' value='".$color_code."' class='color input_field_admin_medium' ".$style." />";
                    //print "&nbsp;<span style='background:#D138CE;'> Example color code - #D138CE </span>\n";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_en'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>*Title EN</td>\n";
                if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                else $title_en=$row['title_en'];
                print "\t<td>";
                    print "<input name='title_en' type='text' value='".$title_en."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_de'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>Title DE</td>\n";
                if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                else $title_de=$row['title_de'];
                print "\t<td>";
                    print "<input name='title_de' type='text' value='".$title_de."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_fr'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>Title FR</td>\n";
                if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                else $title_fr=$row['title_fr'];
                print "\t<td>";
                    print "<input name='title_fr' type='text' value='".$title_fr."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_it'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>Title IT</td>\n";
                if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                else $title_it=$row['title_it'];
                print "\t<td>";
                    print "<input name='title_it' type='text' value='".$title_it."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_zh'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>Title CN</td>\n";
                if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                else $title_zh=$row['title_zh'];
                print "\t<td>";
                    print "<input name='title_zh' type='text' value='".$title_zh."' class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td class='th_align_left'>Relation</td>\n";
                print "\t<td colspan='2'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",23,$row['colorid'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "\t<input type='button' value='delete' title='Delete this painting' onclick=\"delete_confirm2('/admin/paintings/colors/edit/del.php?colorid=".$row['colorid']."','','Are you sure you wish to delete Color - ".addslashes($row['title_en'])."?')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/paintings/colors/edit/del.php?colorid=".$row['colorid']."";
                        $options_delete['text1']="Painting - Color - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='colorid' value='".$row['colorid']."' />\n";
    print "\t</form>\n";

    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,23,$row['colorid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
