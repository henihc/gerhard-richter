<?php
ini_set('display_errors','1');
//ini_set('display_startup_errors','1');
error_reporting (E_ALL ^ E_NOTICE);
set_time_limit(0);

require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$db=new dbCLASS;


# resize painting images
$i=0;
if( !empty($_GET['paintid']) ) $query_where=" WHERE paintID='".$_GET['paintid']."' ";
if( !empty($_GET['limit']) && is_numeric($_GET['limit']) ) 
{
    $limit=str_replace(".",",",$_GET['limit']);
    $query_limit=" LIMIT ".$limit." ";
}
elseif( $_GET['limit']=="nolimit" ) $query_limit=" ";
else $query_limit=" LIMIT 2 ";

if( !empty($_GET['from']) && !empty($_GET['to']) ) $query_limit=" LIMIT ".$_GET['from']." ".$_GET['to'];

$query_painting=QUERIES::query_painting($db,$query_where,"",$query_limit);
$results=$db->query($query_painting['query']);
$results_total=$db->query($query_painting['query_without_limit']);
$count=$db->numrows($results);
$count_total=$db->numrows($results_total);

# CSV headlines
$colors=array();
$line=array( 
    "paintid", "imageid", 
    "c1-l", "c1-a", "c1-b", "c1-weight", 
    "c2-l", "c2-a", "c2-b", "c2-weight", 
    "c3-l", "c3-a", "c3-b", "c3-weight", 
    "c4-l", "c4-a", "c4-b", "c4-weight", 
    "c5-l", "c5-a", "c5-b", "c5-weight", 
    "c6-l", "c6-a", "c6-b", "c6-weight", 
    "c7-l", "c7-a", "c7-b", "c7-weight", 
    "c8-l", "c8-a", "c8-b", "c8-weight", 
    "c9-l", "c9-a", "c9-b", "c9-weight", 
    "c10-l", "c10-a", "c10-b", "c10-weight");
array_push($colors,$line); 
#END CSV headlines

while( $row=$db->mysql_array($results) )
{
    $i++;
    if( $_GET['import'] && $i==1 )
    {
        print "Painting count - ".$count_total."<br />";
        print "Limit - ".$limit."<br />";
        print "<br />";
    }
    $query_related_images="SELECT 
        COALESCE(
            r1.itemid1, 
            r2.itemid2 
        ) as itemid,
        COALESCE(
            r1.sort, 
            r2.sort 
        ) as sort_rel, 
        COALESCE(
            r1.relationid, 
            r2.relationid 
        ) as relationid_rel
        FROM 
            ".TABLE_PAINTING." p 
        LEFT JOIN 
            ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID 
        LEFT JOIN 
            ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
        WHERE p.paintID=".$row['paintID']."
        ORDER BY sort_rel ASC, relationid_rel DESC
        LIMIT 1
    ";
    $results_related_image=$db->query($query_related_images);
    $row_related_image=$db->mysql_array($results_related_image);
    $count_related_image=$db->numrows($results_related_image);

    if( $count_related_image )
    {
        $imageid=$row_related_image['itemid'];
        if( !empty($imageid) )
        {
            $line=array();
            array_push($line, $row['paintID']);
            array_push($line, $imageid);
            $query_where_images=" WHERE imageid='".$imageid."' ";
            $query_images=QUERIES::query_images($db,$query_where_images);
            $results_images=$db->query($query_images['query']);
            $row_images=$db->mysql_array($results_images);

            $dir=DATA_PATH_DISPLAY."/images_new";
            $file_name=$row_images['imageid'].".jpg";
            $img_src=$dir."/xlarge/".$file_name;

            $options_color_extract=array();
            $options_color_extract['num_results']=$_GET['num_results'];
            $options_color_extract['delta']=$_GET['delta'];
            $options_color_extract['export']=$_GET['export'];
            $colors_array=admin_utils::extract_colors($db,$img_src,$options_color_extract);

            # VISIBLE INFO PRINT OUT
            if( $_GET['visible'] )
            {
                print "$i - <a href='/images/imageid_".$row_images['imageid']."__size_xl.jpg' target='_blank'>";
                    print "<img src='/images/imageid_".$row_images['imageid']."__size_l.jpg' alt='image not available' alt='' />";
                print "</a><br />";
                print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>EDIT PAINTING</a><br />";
                print "<a href='/admin/images/edit/?imageid=".$row_images['imageid']."' target='_blank'>EDIT IMAGE</a>";
                print "<br /><br />";

                print "<table border=1>";
                    print "<tr>";
                        print "<th>Color</th>";
                        print "<th>HEX</th>";
                        print "<th>Percentage</th>";
                        print "<th>HSL</th>";
                        //print "<th>HSL NEW</th>";
                        print "<th>LAB</th>";
                        print "<th>Name</th>";
                    print "</tr>";
 
                    foreach($colors_array as $key => $value)
                    {
                        print "<tr>";
                            print "<td style=\"background-color:#".$value['hex'].";\"></td>";
                            print "<td>".$value['hex']."</td>";
                            print "<td>".$value['percentage']."</td>";
                            print "<td style='vertical-align:top;'>H:".$value['hsl'][0]."<br/>S:".$value['hsl'][1]."<br />L:".$value['hsl'][2]."</td>";
                            //print "<td style='vertical-align:top;'>H:".$value['hsl_new'][0]."<br/>S:".$value['hsl_new'][1]."<br />L:".$value['hsl_new'][2]."</td>";
                            print "<td style='vertical-align:top;'>L:".$value['lab'][0]."<br/>A:".$value['lab'][1]."<br />B:".$value['lab'][2]."</td>";
                            print "<td style='vertical-align:top;'>".$value['name']."</td>";
                        print "</tr>";
                    }
                print "</table>";
                print "<br /><br /><hr /><br /><br />";
            }

            # EXPORT
            if( $_GET['export'] )
            {
                foreach($colors_array as $key => $value)
                {
                    array_push($line, $value['lab'][0], $value['lab'][1], $value['lab'][2], $value['percentage']);
                }
            }

            # IMPORT and link colors to paintings
            /*
            if( $_GET['import'] )
            {
                foreach($colors_array as $key => $value)
                {
                    $query_where_colors=" WHERE title_en='".$value['name']."' ";
                    $query_colors=QUERIES::query_painting_colors($db,$query_where_colors,$query_order,$query_limit);
                    $results_colors=$db->query($query_colors['query_without_limit']);
                    $count_colors=$db->numrows($results_colors);
                    $row_colors=$db->mysql_array($results_colors);
                    
                    # insert into colors table
                    if( !$count_colors )
                    {
                        $db->query("INSERT INTO ".TABLE_PAINTING_COLORS."(
                                color_code,
                                hsl_h,
                                hsl_s,
                                hsl_l,
                                lab_l,
                                lab_a,
                                lab_b,
                                title_en,
                                date_created
                            ) VALUES(
                                '".$value['hex']."',
                                '".$value['hsl'][0]."',
                                '".$value['hsl'][1]."',
                                '".$value['hsl'][2]."',
                                '".$value['lab'][0]."',
                                '".$value['lab'][1]."',
                                '".$value['lab'][2]."',
                                '".$value['name']."',
                                NOW()
                            )");
                        $colorid=$db->return_insert_id();
                    }
                    else $colorid=$row_colors['colorid'];

                    # add painting relation
                    $relationid=admin_utils::add_relation($db,1,$row['paintID'],23,$colorid,$_POST['relations_sort']);
                    if( !empty($value['percentage']) ) $db->query("INSERT INTO ".TABLE_RELATIONS_PAINTING_COLORS."(relationid,percentage,date_created) VALUES('".$relationid['relationid']."','".$value['percentage']."',NOW()) ");

                }
            }
                 */


        }

    }
    array_push($colors,$line); 
}

# EXPORT
if( $_GET['export'] )
{
    $csv_filename="paintings_colors_".time().".csv";
    $fhandle = fopen($_SERVER["DOCUMENT_ROOT"].'/admin/artworks/colors/export/csv/'.$csv_filename, 'w');
    foreach ($colors as $fields) {
        fputcsv($fhandle, $fields);
    }
    fclose($fhandle);
    print "Paintings count - ".$count_total."<br />";
    print "Total rows - ".(count($colors)-1)."<br />";
    print "<a href='/admin/artworks/colors/export/csv/".$csv_filename."' title='".$csv_filename."'>".$csv_filename."</a>";
}
//print_r($colors);



?>
