<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


if( empty($_GET['paintid']) ) 
{
	admin_utils::admin_log($db,3,3,$_GET['paintid'],2,1);
    UTILS::redirect("/admin/paintings/?".$_GET['paintid']."&error=true");
}
else
{
    $db->query("UPDATE ".TABLE_PAINTING." SET imageid=NULL,src=NULL WHERE paintID='".$_GET['paintid']."' ");
    admin_utils::admin_log($db,3,3,$_GET['paintid'],1,1);
}


UTILS::redirect("/admin/paintings/edit/?paintid=".$_GET['paintid']."&task=delete_image");
?>
