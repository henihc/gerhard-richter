<?php
//header("Content-Type: text/html; charset=utf-8");
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['year']) ) $_SESSION['new_record']['error']['year']=true;
if( empty($_POST['titleEN']) && empty($_POST['titleDE']) ) $_SESSION['new_record']['error']['title']=true;
if( empty($_POST['artworkID']) ) $_SESSION['new_record']['error']['artworkID']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/paintings/edit/?paintid=".$_POST['paintid']."&error=true");

$db=new dbCLASS;


$artwork_notesEN=$db->db_prepare_input($_POST['artwork_notesEN'],1);
$artwork_notesDE=$db->db_prepare_input($_POST['artwork_notesDE'],1);
$artwork_notesFR=$db->db_prepare_input($_POST['artwork_notesFR'],1);
$artwork_notesIT=$db->db_prepare_input($_POST['artwork_notesIT'],1);
$artwork_notesZH=$db->db_prepare_input($_POST['artwork_notesZH'],1);

$title_clean_de=$_POST['titleDE'];
//print "|".$title_clean_de;

$_POST=UTILS::html_decode($_POST);
//print "|".$_POST['titleDE'];
$_POST=$db->db_prepare_input($_POST,1);
//print_r($_POST);
//exit;
//$_POST['number']=str_replace(" ","",$_POST['number']);

if( empty($_POST['editionID']) ) $_POST['editionID']="NULL";
if( empty($_POST['edition_individual']) ) $_POST['edition_individual']="0";

# SORT number
    //$maxsort=UTILS::max_table_value($db,"sort",TABLE_PAINTING," WHERE artworkID='".$_POST['artworkID']."' ");
    $values=array();
    if( empty($_POST['number']) )
    {
        //$values['number']=$maxsort;
        #checking if date is not empty
        if( !empty($_POST['date']) && $_POST['date']!="0000-00-00" ) $values['date']=$_POST['date'];
        elseif( !empty($_POST['year']) ) $values['year']=$_POST['year'];
    }
    else $values['number']=$_POST['number'];
    $values['year_drawings']=$_POST['year'];
    $row_artwork=UTILS::get_artwork_info($db,$_POST['artworkID']);
    if( $row_artwork['artworkID']==1 || $row_artwork['artworkID']==2 || $row_artwork['artworkID']==13 ) $values['artwork_sort']=1;
    else $values['artwork_sort']=$row_artwork['sort'];
    $values['artworkid']=$row_artwork['artworkID'];
    $results_painting=$db->query("SELECT sort,sort2,sort3 FROM ".TABLE_PAINTING." WHERE paintID='".$_POST['paintid']."' ");
    $row_painting=$db->mysql_array($results_painting,0);
    $values['sort_number_current']=$row_painting['sort2'];
    $values['title_en']=$_POST['titleEN'];
    $values['paintid']=$_POST['paintid'];
    $values['return_array']=1;
    $sort=UTILS::painting_sort_number($db,$values);
    $number_search=$sort[0]%10000;
    $number_search2=$sort[1];
    if( empty($number_search2) ) $number_search2=$number_search;
#end SORT number

//print "sort2=".$sort[0]."<br />";
//print "number_search2=".$number_search2."<br />";

//$title_en=$db->db_replace_quotes($db,$_POST['titleEN']);
$title_en=$_POST['titleEN'];
if( empty($_POST['paintid']) )
{
    $db->query("INSERT INTO ".TABLE_PAINTING."(titleEN,created) VALUES('".$title_en."',NOW()) ");
    $paintid=$db->return_insert_id();
    $submit="add";
    admin_utils::admin_log($db,1,3,$paintid);
}
elseif( !empty($_POST['paintid']) )
{
    $paintid=$_POST['paintid'];
    $submit="update";
    admin_utils::admin_log($db,2,3,$paintid);
}
else
{
    admin_utils::admin_log($db,"",3,$_POST['paintid'],2);
}


# SORT number
    //$maxsort=UTILS::max_table_value($db,"sort",TABLE_PAINTING," WHERE artworkID='".$_POST['artworkID']."' ");
    # sort2
    $values_sort2=array();
    if( empty($_POST['number']) )
    {
        //$values_sort2['number']=$maxsort;
        #checking if date is not empty
        if( !empty($_POST['date']) && $_POST['date']!="0000-00-00" ) $values_sort2['date']=$_POST['date'];
        elseif( !empty($_POST['year']) ) $values_sort2['year']=$_POST['year'];
    }
    else
    {
        if( $_POST['number']=="zu 415" ) $number_for_sort=415;
        else $number_for_sort=$_POST['number'];
        $values_sort2['number']=$number_for_sort;
    }
    $values_sort2['year_drawings']=$_POST['year'];
    $row_artwork=UTILS::get_artwork_info($db,$_POST['artworkID']);
    if( $row_artwork['artworkID']==1 || $row_artwork['artworkID']==2 || $row_artwork['artworkID']==13 ) $values_sort2['artwork_sort']=1;
    else $values_sort2['artwork_sort']=$row_artwork['sort'];
    $values_sort2['artworkid']=$row_artwork['artworkID'];
    $values_sort2['title_en']=$_POST['titleEN'];
    $values_sort2['paintid']=$paintid;
    $values_sort2['return_array']=1;
    $sort=UTILS::painting_sort_number($db,$values_sort2);
    if( $_POST['paintid']==11581 ) $sort[0]=4000001.001;
    if( $_POST['paintid']==17677 ) $sort[0]=4000001.000;
    # END sort2
    if( !empty($_POST['artworkID2']) )
    {
        $values_sort3=array();
        if( empty($_POST['number']) )
        {
            #checking if date is not empty
            if( !empty($_POST['date']) && $_POST['date']!="0000-00-00" ) $values_sort3['date']=$_POST['date'];
            elseif( !empty($_POST['year']) ) $values_sort3['year']=$_POST['year'];
        }
        else $values_sort3['number']=$_POST['number'];
        $values_sort3['year_drawings']=$_POST['year'];
        $row_artwork=UTILS::get_artwork_info($db,$_POST['artworkID2']);
        if( $row_artwork['artworkID']==1 || $row_artwork['artworkID']==2 || $row_artwork['artworkID']==13 ) $values_sort3['artwork_sort']=1;
        else $values_sort3['artwork_sort']=$row_artwork['sort'];
        $values_sort3['artworkid']=$row_artwork['artworkID'];
        $values_sort3['title_en']=$_POST['titleEN'];
        $values_sort3['paintid']=$paintid;
        $values_sort3['return_array']=1;
        $sort3=UTILS::painting_sort_number($db,$values_sort3);
        //print_r($sort3);
        $sort3="'".$sort3[0]."'";
        //print "|".$sort3."|<br />";

        //exit;
    }
    else $sort3="NULL";
    # END sort2
    //$db->query("UPDATE ".TABLE_PAINTING." SET sort='".$sort."', sort3=".$sort3." WHERE paintID='".$paintid."' ");
    $db->query("UPDATE ".TABLE_PAINTING." SET sort3=".$sort3." WHERE paintID='".$paintid."' ");
#end SORT number

if( empty($_POST['date']) ) $date="NULL";
else $date="'".$_POST['date']."'";

if( !empty($_POST['titleEN']) ) $title_to_use=$_POST['titleEN'];
elseif( !empty($_POST['titleDE']) ) $title_to_use=$_POST['titleDE'];
if( empty($_POST['titleEN']) ) $title_to_use_en=$title_to_use;
else $title_to_use_en=$_POST['titleEN'];
if( empty($_POST['titleDE']) ) $title_to_use_de=$title_to_use;
else $title_to_use_de=$_POST['titleDE'];
if( empty($_POST['titleFR']) ) $title_to_use_fr=$title_to_use;
else $title_to_use_fr=$_POST['titleFR'];
if( empty($_POST['titleIT']) ) $title_to_use_it=$title_to_use;
else $title_to_use_it=$_POST['titleIT'];
if( empty($_POST['titleEN']) ) $title_to_use_zh=$title_to_use;
else $title_to_use_zh=$_POST['titleEN'];

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_PAINTING,$title_to_use_en,"paintID",$paintid,"titleurl_en","en");

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['titleDE'];
$title_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleFR'];
$title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleIT'];
$title_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

# replace quotes
//$title_de=$db->db_replace_quotes($db,$_POST['titleDE']);
//$title_fr=$db->db_replace_quotes($db,$_POST['titleFR']);
//$title_it=$db->db_replace_quotes($db,$_POST['titleIT']);
//$title_zh=$db->db_replace_quotes($db,$_POST['titleZH']);
$title_de=$_POST['titleDE'];
$title_fr=$_POST['titleFR'];
$title_it=$_POST['titleIT'];
$title_zh=$_POST['titleZH'];
//$notes=$db->db_replace_quotes($db,$_POST['notes']);
$notes=$_POST['notes'];
# END replace quotes

if($paintid==5471)
{
    //$title_de=$title_clean_de;
    //print "|".$title_de;
    //exit;
}

//print_r($_POST);
//print $titleurl_en;
//print $sort[0];
//exit;

if( !empty($_POST['artworkID2']) ) $artworkID2="'".$_POST['artworkID2']."'";
else $artworkID2="NULL";

$year_search=$_POST['year'];
if( preg_match("/^c\. +/", $_POST['year']) )
{
    $year_search=str_replace("c. ", "", $year_search);
}
$year_search=substr($year_search,0,4);

$query="UPDATE ".TABLE_PAINTING." SET
                artworkID='".$_POST['artworkID']."',
                artworkID2=".$artworkID2.",
                editionID=".$_POST['editionID'].",
                edition_individual=".$_POST['edition_individual'].",
			    titleEN='".$title_en."',
				titleDE='".$title_de."',
				titleFR='".$title_fr."',
				titleIT='".$title_it."',
				titleZH='".$title_zh."',
                titleurl='".$titleurl_en."',
				title_search_1='".$title_search_de['text_lower_converted']."',
				title_search_2='".$title_search_de['text_lower_german_converted']."',
				title_search_3='".$title_search_fr['text_lower_converted']."',
				title_search_4='".$title_search_it['text_lower_converted']."',
                year='".$_POST['year']."',
                year_search='".$year_search."',
                date=".$date.",
                number='".$_POST['number']."',
                number_old='".$_POST['number_old']."',
                number_search='".$number_search."',
                number_search2='".$number_search2."',";

                /*
                if( $_SESSION['debug_page'] )
                {
                    $query.="ednumber='".$_POST['ednumber']."',
                    ednumberroman='".$_POST['ednumberroman']."',
                    ednumberap='".$_POST['ednumberap']."',
                    citylifepage='".$_POST['citylifepage']."',";
                }
                */

			    $query.="catID='".$_POST['catID']."',
				height='".$_POST['height']."',
				width='".$_POST['width']."',
				size='".$_POST['size']."',
				size_parts='".$_POST['size_parts']."',
				size_typeid='".$_POST['size_typeid']."',
				mID='".$_POST['mID']."',
				locationid='".$_POST['locationid']."',
				cityid='".$_POST['cityid']."',
				countryid='".$_POST['countryid']."',
                optionid_loan_type='".$_POST['optionid_loan_type']."',
                loan_locationid='".$_POST['locationid1']."',
                loan_cityid='".$_POST['cityid1']."',
                loan_countryid='".$_POST['countryid1']."',
                collection_notes_en='".$_POST['collection_notes_en']."',
                collection_notes_de='".$_POST['collection_notes_de']."',
                collection_notes_fr='".$_POST['collection_notes_fr']."',
                collection_notes_it='".$_POST['collection_notes_it']."',
                collection_notes_zh='".$_POST['collection_notes_zh']."',
                sort2='".$sort[0]."',
                optionid_photograph_courtesy='".$_POST['optionid_photograph_courtesy']."',
                optionid_photograph_courtesy_values='".$_POST['optionid_photograph_courtesy_values']."',
                artwork_notesEN='".$artwork_notesEN."',
                artwork_notesDE='".$artwork_notesDE."',
                artwork_notesFR='".$artwork_notesFR."',
                artwork_notesIT='".$artwork_notesIT."',
                artwork_notesZH='".$artwork_notesZH."',
                notes='".$notes."',";
                $query.="destroyed='".$_POST['destroyed']."',";
                $query.="enable='".$_POST['enable']."',
                keywords='".$_POST['keywords']."' ";
                //if( $_POST['submit']=="update" ) $query.=", modified=NOW() ";
                if( $submit=="update" ) $query.=", modified=NOW() ";
$query.=" WHERE paintID='".$paintid."' ";

//if( $_POST['submit']=="add" || $_POST['submit']=="update" )
if( $submit=="add" || $submit=="update" )
{
    $db->query($query);

    # RELATION adding
    admin_utils::add_relation($db,1,$paintid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    //print "-".$_POST['relation_typeid']."-".$_POST['relations_sort'];
    //exit;

    if( $_POST['relation_typeid']==13 )
    {
        admin_utils::add_relation($db,1,$_POST['itemid'],14,$paintid,$_POST['relations_sort']);
    }

    if( $_POST['relation_typeid']==14 )
    {
        admin_utils::add_relation($db,1,$_POST['itemid'],13,$paintid,$_POST['relations_sort']);
    }

    #end RELATION adding
}

#image upload
$image_upload=0;
if($_FILES['image']['error']==0)
{
    $fileName = str_replace (" ", "_", $_FILES['image']['name']);
    $fileName = str_replace ("'", "", $fileName);
    $fileName = str_replace ('"', '', $fileName);
    $tmpName  = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];

		if ( is_uploaded_file($tmpName) )
		{
            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            $getimagesize = getimagesize($tmpName);

            switch( $getimagesize['mime'] )
            {
                case 'image/gif'  : $ext = ".gif"; break;
                case 'image/png'  : $ext = ".png"; break;
                case 'image/jpeg' : $ext = ".jpg"; break;
                case 'image/bmp'  : $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;

		    UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
		    UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
		    UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
		    UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
		    UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);
		    UTILS::resize_image($tmpName,$fileName,THUMB_XXL_HEIGHT,THUMB_XXL_WIDTH,$dir.'/xxlarge/'.$new_file_name);

            #edition
            /*
            # SNOW WHITE
            if( $_POST['artworkID']==4 && $_POST['editionID']==12813 )
            {
                UTILS::resize_image($tmpName,$fileName,SIZE_SNOWWHITE_HEIGHT,SIZE_SNOWWHITE_WIDTH,$dir.'/xlarge_snowwhite/'.$new_file_name);
            }
            # WAR CUT
            if( $_POST['artworkID']==4 && $_POST['editionID']==12672 )
            {
                UTILS::resize_image($tmpName,$fileName,SIZE_WARCUT_HEIGHT,SIZE_WARCUT_WIDTH,$dir.'/xlarge_warcut/'.$new_file_name);
            }
            # FLORENCE
            if( $_POST['artworkID']==4 && $_POST['editionID']==12798 )
            {
                UTILS::resize_image($tmpName,$fileName,SIZE_FLORENCE_HEIGHT,SIZE_FLORENCE_WIDTH,$dir.'/xlarge_florence/'.$new_file_name);
            }
            */
            #atlas
            if( $_POST['artworkID']==3 )
            {
                UTILS::resize_image($tmpName,$fileName,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,$dir.'/llarge/'.$new_file_name);
            }

			#copy original
			if (!copy($tmpName, $new_file_path))
			{
			    exit("Error Uploading File.");
			}
			else
            {
                $image_upload=1;
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,1,$paintid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100.png";
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__border_1.png";
                $src_cache_uri=$imageid.".png";
                UTILS::cache_uri($src,$src_cache_uri,1);
                print "<br />".$src;
                #end

                # edition - SNOW WHITE
                if( $_POST['artworkID']==4 && $_POST['editionID']==12813 )
                {
                    # create cache image
                    $values=array();
                    $values['cache_dir']="cache_microsites";
                    $src="/images/imageid_".$imageid."__thumb_1__size_o__maxwidth_146__maxheight_102__quality_100.jpg";
                    $src_cache_uri=$imageid.".jpg";
                    UTILS::cache_uri($src,$src_cache_uri,1,$values);
                    #end
                }

                $query_image="UPDATE ".TABLE_PAINTING." SET imageid='".$imageid."' WHERE paintID='".$paintid."' LIMIT 1 ";
                $db->query($query_image);

                # add available colors and add relation to it
                /*
                $query_related_images="SELECT
                    COALESCE(
                        r1.itemid1,
                        r2.itemid2
                    ) as itemid,
                    COALESCE(
                        r1.sort,
                        r2.sort
                    ) as sort_rel,
                    COALESCE(
                        r1.relationid,
                        r2.relationid
                    ) as relationid_rel
                    FROM
                        ".TABLE_PAINTING." p
                    LEFT JOIN
                        ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
                    LEFT JOIN
                        ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
                    WHERE p.paintID=".$paintid."
                    ORDER BY sort_rel ASC, relationid_rel DESC
                    LIMIT 1
                ";
                $results_related_image=$db->query($query_related_images);
                $row_related_image=$db->mysql_array($results_related_image);
                $count_related_image=$db->numrows($results_related_image);

                if( $count_related_image )
                {
                    $imageid=$row_related_image['itemid'];
                    if( !empty($imageid) )
                    {
                        # deleting all old color relations
                        $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND itemid1='".$paintid."' AND typeid2=23 ) OR ( typeid2=1 AND itemid2='".$paintid."' AND typeid1=23 ) ");
                        # END deleting all old color relations
                        $line=array();
                        array_push($line, $row['paintID']);
                        array_push($line, $imageid);
                        $query_where_images=" WHERE imageid='".$imageid."' ";
                        $query_images=QUERIES::query_images($db,$query_where_images);
                        $results_images=$db->query($query_images['query']);
                        $row_images=$db->mysql_array($results_images);

                        $dir=DATA_PATH_DISPLAY."/images_new";
                        $file_name=$row_images['imageid'].".jpg";
                        $img_src=$dir."/xlarge/".$file_name;

                        $options_color_extract=array();
                        $options_color_extract['num_results']=$_GET['num_results'];
                        $options_color_extract['delta']=$_GET['delta'];
                        $options_color_extract['export']=$_GET['export'];
                        $colors_array=admin_utils::extract_colors($db,$img_src,$options_color_extract);

                        foreach($colors_array as $key => $value)
                        {
                            $query_where_colors=" WHERE title_en='".$value['name']."' ";
                            $query_colors=QUERIES::query_painting_colors($db,$query_where_colors,$query_order,$query_limit);
                            $results_colors=$db->query($query_colors['query_without_limit']);
                            $count_colors=$db->numrows($results_colors);
                            $row_colors=$db->mysql_array($results_colors);

                            # insert into colors table
                            if( !$count_colors )
                            {
                                $db->query("INSERT INTO ".TABLE_PAINTING_COLORS."(
                                        color_code,
                                        hsl_h,
                                        hsl_s,
                                        hsl_l,
                                        lab_l,
                                        lab_a,
                                        lab_b,
                                        title_en,
                                        date_created
                                    ) VALUES(
                                        '".$value['hex']."',
                                        '".$value['hsl'][0]."',
                                        '".$value['hsl'][1]."',
                                        '".$value['hsl'][2]."',
                                        '".$value['lab'][0]."',
                                        '".$value['lab'][1]."',
                                        '".$value['lab'][2]."',
                                        '".$value['name']."',
                                        NOW()
                                    )");
                                $colorid=$db->return_insert_id();
                            }
                            else $colorid=$row_colors['colorid'];

                            # add painting relation
                            $relationid=admin_utils::add_relation($db,1,$paintid,23,$colorid,$_POST['relations_sort']);
                            if( !empty($value['percentage']) ) $db->query("INSERT INTO ".TABLE_RELATIONS_PAINTING_COLORS."(relationid,percentage,date_created) VALUES('".$relationid['relationid']."','".$value['percentage']."',NOW()) ");

                        } # foreach end
                    } # !empty($imageid) end
                } # $count_related_image end
                 */
                #END

			}

		}
}
# END image upload

$query_images="SELECT
    COALESCE(
        r1.itemid1,
        r2.itemid2
    ) as itemid,
    COALESCE(
        r1.sort1,
        r2.sort1
    ) as sort_rel,
    COALESCE(
        r1.relationid,
        r2.relationid
    ) as relationid_rel
    FROM
        ".TABLE_PAINTING." p
    LEFT JOIN
        ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
    LEFT JOIN
        ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
    WHERE p.paintID=".$paintid."
    ORDER BY sort_rel ASC, relationid_rel DESC
    LIMIT 1
";
$results_images=$db->query($query_images);
$count_images=$db->numrows($results_images);

if( $count_images )
{
    $row_images=$db->mysql_array($results_images);
    $imageid=$row_images['itemid'];
    $src=DATA_PATH."/images_new/original/".$imageid.".jpg";
    list($width_img_original, $height_img_original) = @getimagesize($src);
    if( empty($width_img_original) ) $width_img_original="NULL";
    if( empty($height_img_original) ) $height_img_original="NULL";
    //print "width=".$width_img_original." X height=".$height_img_original;
}

# if additional info then save addition info

/*
if( $_POST['artworkID']==6 || $_POST['artworkID2']==6
    || $_POST['artworkID']==7 || $_POST['artworkID2']==7
    || $_POST['artworkID']==8 || $_POST['artworkID2']==8
    || ( ($_POST['artworkID']==4 || $_POST['artworkID2']==4) && ( $_POST['number']=='72' || $_POST['number']=='78' ) ) )
{
*/
    $verso_signed_1_3=$_POST['verso_signed_1_3'];
    $verso_signed_2_3=$_POST['verso_signed_2_3'];
    $verso_dated_1_3=$_POST['verso_dated_1_3'];
    $verso_dated_2_3=$_POST['verso_dated_2_3'];
    $verso_numbered_3=$_POST['verso_numbered_3'];
    $verso_inscribed_3=$_POST['verso_inscribed_3'];
    $recto_signed_1_3=$_POST['recto_signed_1_3'];
    $recto_signed_2_3=$_POST['recto_signed_2_3'];
    $recto_dated_1_3=$_POST['recto_dated_1_3'];
    $recto_dated_2_3=$_POST['recto_dated_2_3'];
    $recto_numbered_3=$_POST['recto_numbered_3'];
    $recto_inscribed_3=$_POST['recto_inscribed_3'];
    $inscriptions=$_POST['opp_inscriptions'];
    $notations=$_POST['opp_notations'];
    $city=$_POST['opp_city'];
    $country=$_POST['opp_country'];
    $collection=$_POST['opp_collection'];
    $private_col=$_POST['opp_private_col'];
    $copyright=$_POST['opp_copyright'];
    $source=$_POST['opp_source'];
    $licence=$_POST['opp_licence'];

    $values_query_painting_opp=array();
    $values_query_painting_opp['where']=" WHERE paintid='".$paintid."' ";
    $query_painting_opp=QUERIES::query_painting_opp($db,$values_query_painting_opp);
    $results_opp=$db->query($query_painting_opp['query']);
    $count_opp=$db->numrows($results_opp);
    $row_opp=$db->mysql_array($results_opp,0);

    if( !$count_opp )
    {
        $query_opp_add=" INSERT INTO ".TABLE_PAINTING_OPP."(paintid,date_created) VALUES('".$paintid."',NOW()); ";
        $db->query($query_opp_add);
        $oppinfoid=$db->return_insert_id();
    }
    else $oppinfoid=$row_opp['oppinfoid'];
    $query_opp="UPDATE ".TABLE_PAINTING_OPP." SET ";

    if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
    {
        $query_opp.=" verified_verso='".$_POST['verified_verso']."', ";
        $query_opp.=" verified_recto='".$_POST['verified_recto']."', ";
    }

    $query_opp.="
        optionid_verso_signed_1_1='".$_POST['optionid_verso_signed_1_1']."',
        optionid_verso_signed_1_2='".$_POST['optionid_verso_signed_1_2']."',
        verso_signed_1_3='".$verso_signed_1_3."',
        optionid_verso_signed_2_1='".$_POST['optionid_verso_signed_2_1']."',
        optionid_verso_signed_2_2='".$_POST['optionid_verso_signed_2_2']."',
        verso_signed_2_3='".$verso_signed_2_3."',
        optionid_verso_dated_1_1='".$_POST['optionid_verso_dated_1_1']."',
        optionid_verso_dated_1_2='".$_POST['optionid_verso_dated_1_2']."',
        verso_dated_1_3='".$verso_dated_1_3."',
        optionid_verso_dated_2_1='".$_POST['optionid_verso_dated_2_1']."',
        optionid_verso_dated_2_2='".$_POST['optionid_verso_dated_2_2']."',
        verso_dated_2_3='".$verso_dated_2_3."',
        optionid_verso_numbered_1='".$_POST['optionid_verso_numbered_1']."',
        optionid_verso_numbered_2='".$_POST['optionid_verso_numbered_2']."',
        verso_numbered_3='".$verso_numbered_3."',
        optionid_verso_inscribed_1='".$_POST['optionid_verso_inscribed_1']."',
        optionid_verso_inscribed_2='".$_POST['optionid_verso_inscribed_2']."',
        verso_inscribed_3='".$verso_inscribed_3."',
        optionid_recto_signed_1_1='".$_POST['optionid_recto_signed_1_1']."',
        optionid_recto_signed_1_2='".$_POST['optionid_recto_signed_1_2']."',
        recto_signed_1_3='".$recto_signed_1_3."',
        optionid_recto_signed_2_1='".$_POST['optionid_recto_signed_2_1']."',
        optionid_recto_signed_2_2='".$_POST['optionid_recto_signed_2_2']."',
        recto_signed_2_3='".$recto_signed_2_3."',
        optionid_recto_dated_1_1='".$_POST['optionid_recto_dated_1_1']."',
        optionid_recto_dated_1_2='".$_POST['optionid_recto_dated_1_2']."',
        recto_dated_1_3='".$recto_dated_1_3."',
        optionid_recto_dated_2_1='".$_POST['optionid_recto_dated_2_1']."',
        optionid_recto_dated_2_2='".$_POST['optionid_recto_dated_2_2']."',
        recto_dated_2_3='".$recto_dated_2_3."',
        optionid_recto_numbered_1='".$_POST['optionid_recto_numbered_1']."',
        optionid_recto_numbered_2='".$_POST['optionid_recto_numbered_2']."',
        recto_numbered_3='".$recto_numbered_3."',
        optionid_recto_inscribed_1='".$_POST['optionid_recto_inscribed_1']."',
        optionid_recto_inscribed_2='".$_POST['optionid_recto_inscribed_2']."',
        recto_inscribed_3='".$recto_inscribed_3."',
        inscriptions='".$inscriptions."',
        notations='".$notations."',
        city='".$city."',
        country='".$country."',
        collection='".$collection."',
        private_col='".$private_col."',
        copyright='".$copyright."',
        publish='".$_POST['opp_publish']."',
        source='".$source."',
        ready_for_usage='".$_POST['opp_ready_for_usage']."',
        licence='".$licence."',
        width='".$_POST['opp_width']."',
        height='".$_POST['opp_height']."'
        ";
    if( $count_opp ) $query_opp.=", date_modified=NOW() ";
    $query_opp.=" WHERE oppinfoid='".$oppinfoid."' ";
    $db->query($query_opp);

    if( $image_upload )
    {
        list($width, $height) = getimagesize($new_file_path);
        if( empty($width) ) $width="NULL";
        if( empty($height) ) $height="NULL";
        //print $width."-".$height;
        $query="UPDATE ".TABLE_PAINTING_OPP." SET width=".$width.", height=".$height." WHERE oppinfoid='".$oppinfoid."' ";
        $db->query($query);
    }
//}
# END



unset($_SESSION['new_record']);


if( $_SESSION['debug_page'] )
{
    //exit();
}

$url="/admin/paintings/edit/?paintid=".$paintid."&relation_typeid=".$_POST['relation_typeid']."&task=".$submit;
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
