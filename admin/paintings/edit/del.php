<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


if( empty($_GET['paintid']) ) 
{
	admin_utils::admin_log($db,3,3,$_GET['paintid'],2);
    UTILS::redirect("/admin/paintings/?".$_GET['paintid']."&error=true");
}
else
{
    $db->query("DELETE FROM ".TABLE_PAINTING." WHERE paintID='".$_GET['paintid']."' ");
    $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND itemid1='".$_GET['paintid']."' ) OR ( typeid2=1 AND itemid2='".$_GET['paintid']."' ) ");

    admin_utils::admin_log($db,3,3,$_GET['paintid']);
}


UTILS::redirect("/admin/paintings/edit/?task=delete");
?>
