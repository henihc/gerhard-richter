<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

set_time_limit(0);

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="paintings";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


    print "Artwork - ";
    $where=" WHERE artworkID!=1 AND artworkID!=2 AND artworkID!=13 AND artworkID!=15 AND artworkID!=10 AND artworkID!=11 AND artworkID!=14 AND artworkID!=12 ";
    admin_html::select_artworks($db,"artworkid",$_GET['artworkid'],"onchange=\"location.href='?artworkid='+document.getElementById('artworkid').options[selectedIndex].value\"",$class=array(),$disable,$where);

    $query_paintings="SELECT p.paintID,p.number,p.titleEN,p.titleDE,p.year,DATE_FORMAT(p.date, '%d-%m-%Y') AS date,p.sort,p.sort2 FROM ".TABLE_PAINTING." p ";

    if( $_GET['artworkid']=="paintings" ) $query_artwork=" artworkID=1 OR artworkID=2 OR artworkID=13 ";
    else $query_artwork=" artworkID='".$_GET['artworkid']."' ";
    $query_paintings.=" WHERE ".$query_artwork;
    $query_paintings.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    $results=$db->query($query_paintings);
    $count=$db->numrows($results);

    if( $count>0 )
    {
        $ii=1;

        print "\t<br /><br /><table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";    
                print "\t<th>paintid</th>\n";
                print "\t<th>number</th>\n";
                //print "\t<th>image</th>\n";
                print "\t<th>titleEN</th>\n";
                print "\t<th>titleDE</th>\n";
                print "\t<th>year</th>\n";
                print "\t<th>date</th>\n";
                print "\t<th>sort</th>\n";
                print "\t<th>sort2</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n"; 

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {
                if( $row['paintID']==$_GET['paintid'] ) $style="style='background:red;'";
                else $style="";
                print "\t<li id='item_".$row['paintID']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">";
                                print "\t<a name='".$row['paintID']."'>".$row['paintID']."</a>";
                            print "\t</td>\n";
                            print "\t<td ".$style.">".$row['number']."</td>";
                            /*
                            print "\t<td ".$style.">";
                                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                $row_related_image=$db->mysql_array($results_related_image);
                                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                if( !empty($imageid) ) $src="/datadir/images_new/xsmall/".$imageid.".jpg";
                                else $src="/datadir/images/size_xs__imageid_".$imageid.".jpg";
                                print "\t<img src='".$src."' alt='' />\n";
                            print "</td>";
                            */
                            print "\t<td ".$style.">".$row['titleEN']."</td>";
                            print "\t<td ".$style.">".$row['titleDE']."</td>";
                            print "\t<td ".$style.">".$row['year']."</td>";
                            print "\t<td ".$style.">".$row['date']."</td>";
                            print "\t<td ".$style.">".$row['sort']."</td>";
                            print "\t<td ".$style.">".$row['sort2']."</td>";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('sort.php?count=".$ii."&artworkid=".$_GET['artworkid']."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }


    }
    elseif( !empty($_GET['artworkid']) )
    {
        print "\t<p class='p_admin_no_data_found'>No paintings found!</p>\n";
    }

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
