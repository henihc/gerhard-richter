<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
//if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=20;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=1&itemid=".$_GET['paintid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="paintings";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();





$values_query_painting=array();
$values_query_painting['columns']=", title_search_1, title_search_2, title_search_3";
$query_where_painting=" WHERE paintID='".$_GET['paintid']."' ";
$query_painting=QUERIES::query_painting($db,$query_where_painting,"","","",$values_query_painting);
$results=$db->query($query_painting['query']);

$row=$db->mysql_array($results,0);
$count=$db->numrows($results);
if( $count>0 ) $row['paintid']=$row['paintID'];


$values_query_painting_opp=array();
$values_query_painting_opp['where']=" WHERE paintid='".$row['paintid']."' ";
$query_painting_opp=QUERIES::query_painting_opp($db,$values_query_painting_opp);
$results_opp=$db->query($query_painting_opp['query']);
$row_opp=$db->mysql_array($results_opp,0);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'artwork_notesEN', { toolbar : 'Large', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'artwork_notesDE', { toolbar : 'Large', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'artwork_notesFR', { toolbar : 'Large', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'artwork_notesIT', { toolbar : 'Large', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'artwork_notesZH', { toolbar : 'Large', width : '950px', height : '40px' } );\n";

        print "\tCKEDITOR.replace( 'collection_notes_en', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'collection_notes_de', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'collection_notes_fr', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'collection_notes_it', { toolbar : 'Small', width : '950px', height : '40px' } );\n";
        print "\tCKEDITOR.replace( 'collection_notes_zh', { toolbar : 'Small', width : '950px', height : '40px' } );\n";



        if( $_SESSION['userID']!=1 && $_SESSION['userID']!=19 )
        {
            print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
                print "hash:false,";
                print "transitionDuration:0,";
                print "startIndex:0,";
                print "hover:true";
            print "});";
        }
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-colnotes li a','#div-admin-tabs-info-colnotes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "new DatePicker($$('.date'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler' });\n";
    print "\t};\n";
    print "window.addEvent('domready',function() {";
        print "var subber = $$('.submit');";
        print "subber.addEvent('click',function() {";
            print "subber.set('disabled','disabled');";
            print "subber.set('value','Submitting...');";
            //print "(function() { subber.disabled = false; subber.set('value','Resubmit'); }).delay(10000);"; // how much time?  10 seconds
        print "});";
    print "});";

print "\t</script>\n";

    print "\t<form action='/admin/paintings/edit/post.php' method='post' enctype='multipart/form-data'>\n";

        print "\t<table class='admin-table'>\n";

            #error and update info
            if( $_GET['error'] ) print "<tr><td colspan='6' class='error'>*Please complete mandatory fields</td></tr>";
            if( $_GET['task']=="update" ) print "<tr><td colspan='6' class='valid'>Update successfully!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='6' class='valid'>Insert successfully!</td></tr>";
            #error and update info end

            #next previous buttons
            if( $count>0 )
            {
                $previous_next_item_admin=admin_html::previous_next_item_admin($db,"paintID",$row['paintid'],"/admin/paintings/edit/?paintid=",$_SESSION['admin']['search_query'],0);
                if( !empty($previous_next_item_admin) )
                {
                    print "\t<tr>\n";
                        print "\t<td colspan='6' style='text-align:center;'>\n";
                            print $previous_next_item_admin;
                        print "\t</td>\n";
                    print "\t</tr>\n";
                }
            }
            # END next previous buttons

            $html_submit_tr="";
            $html_submit_tr.="\t<tr>\n";
                $html_submit_tr.="\t<td>\n";
                    if( $count>0 )
                    {
                        //$html_submit_tr.="\t<input type='button' value='delete' title='Delete this painting' onclick=\"delete_confirm2('/admin/paintings/edit/del.php?paintid=".$row['paintid']."','Painting - ".addslashes($row['titleEN'])."','')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/paintings/edit/del.php?paintid=".$row['paintid']."";
                        $options_delete['text1']="Painting - ".$row['titleEN']."";
                        //$options_delete['text2']="";
                        $options_delete['html_return']=1;
                        $html_submit_tr.=admin_html::delete_button($db,$options_delete);
                    }
                    $html_submit_tr.="<input type='reset' value='reset' />";
                $html_submit_tr.="\t</td>\n";
                $html_submit_tr.="\t<td colspan='5'>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    $html_submit_tr.="\t<input type='submit' name='submit' class='submit' value='".$value."' />\n";
                    $options_painting_url=array();
                    $options_painting_url['paintid']=$row['paintID'];
                    $painting_url=UTILS::get_painting_url($db,$options_painting_url);
                    if( $count ) $html_submit_tr.="\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='".$painting_url['url']."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                $html_submit_tr.="\t</td>\n";
            $html_submit_tr.="\t</tr>\n";

            print $html_submit_tr;

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td style='width:75px;'>\n";
                    if( $row['enable'] || $count<=0 ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\t<input type='checkbox' ".$checked." name='enable' value='1' />\n";
                print "\t</th>";
                print "\t<th style='text-align:center;width:130px;'>";
                    print "Destroyed";
                print "</th>\n";
                print "\t<td colspan='2'>\n";
                    if( $row['destroyed'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\t<input type='checkbox' ".$checked." name='destroyed' value='1' />\n";
                print "\t</td>\n";
                print "\t<td style='text-align:left;'>\n";

                print "\t</td>\n";

            print "\t</tr>\n";

if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
{
          print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title <br /><br />English <br /><br /><br />German <br /><br /><br />French <br /><br /><br />Italian <br /><br /><br />Chinese</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $titleEN=$_SESSION['new_record']['titleEN'];
                    else $titleEN=$row['titleEN'];
                    print "\t<br /><br /><textarea class='form' name='titleEN' cols='29' rows='3'>".$titleEN."</textarea>\n";
                    if( $_GET['error'] ) $titleDE=$_SESSION['new_record']['titleDE'];
                    else $titleDE=$row['titleDE'];
                    print "\t<br /><textarea class='form' name='titleDE' cols='29' rows='3'>".$titleDE."</textarea>\n";
                    if( $_GET['error'] ) $titleFR=$_SESSION['new_record']['titleFR'];
                    else $titleFR=$row['titleFR'];
                    $titleFR=html_entity_decode($titleFR,ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<br /><textarea class='form' name='titleFR' cols='29' rows='3'>".$titleFR."</textarea>\n";
                    if( $_GET['error'] ) $titleIT=$_SESSION['new_record']['titleIT'];
                    else $titleIT=$row['titleIT'];
                    $titleIT=html_entity_decode($titleIT,ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<br /><textarea class='form' name='titleIT' cols='29' rows='3'>".$titleIT."</textarea>\n";
                    if( $_GET['error'] ) $titleZH=$_SESSION['new_record']['titleZH'];
                    else $titleZH=$row['titleZH'];
                    print "\t<br /><textarea class='form' name='titleZH' cols='29' rows='3'>".$titleZH."</textarea>\n";
                    if( $_SESSION['debug_page'] )
                    {
                        print "<br />Title URL = ".$row['titleurl'];
                        print "<br />Title search = ".$row['title_search_1']." | ".$row['title_search_2']." | ".$row['title_search_3'];
                    }
                print "\t</td>\n";
            print "\t</tr>\n";
}
else
{
//print $_SESSION['userID'];
            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleEN=$_SESSION['new_record']['titleEN'];
                            else $titleEN=$row['titleEN'];
                            print "\t<textarea class='form' name='titleEN' cols='29' rows='3'>".$titleEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleDE=$_SESSION['new_record']['titleDE'];
                            else $titleDE=$row['titleDE'];
                            print "\t<textarea class='form' name='titleDE' cols='29' rows='3'>".$titleDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleFR=$_SESSION['new_record']['titleFR'];
                            else $titleFR=$row['titleFR'];
                            $titleFR=html_entity_decode($titleFR,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='titleFR' cols='29' rows='3'>".$titleFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleIT=$_SESSION['new_record']['titleIT'];
                            else $titleIT=$row['titleIT'];
                            $titleIT=html_entity_decode($titleIT,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='titleIT' cols='29' rows='3'>".$titleIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleZH=$_SESSION['new_record']['titleZH'];
                            else $titleZH=$row['titleZH'];
                            print "\t<textarea class='form' name='titleZH' cols='29' rows='3'>".$titleZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                    if( $_SESSION['debug_page'] )
                    {
                        print "<br />Title URL = ".$row['titleurl'];
                        print "<br />Title search = ".$row['title_search_1']." | ".$row['title_search_2']." | ".$row['title_search_3'];
                    }
                print "\t</td>\n";
            print "\t</tr>\n";
}

            print "\t<tr>\n";
                print "\t<th class='' style='text-align:center;'>\n";
                    if( $count && !empty($row['imageid']) )
                    {
                        /*
                        $query_related_images="SELECT
                            COALESCE(
                                r1.itemid1,
                                r2.itemid2
                            ) as itemid,
                            COALESCE(
                                r1.sort,
                                r2.sort
                            ) as sort_rel,
                            COALESCE(
                                r1.relationid,
                                r2.relationid
                            ) as relationid_rel
                            FROM
                                ".TABLE_PAINTING." p
                            LEFT JOIN
                                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
                            LEFT JOIN
                                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
                            WHERE p.paintID=".$row['paintid']."
                            ORDER BY sort_rel ASC, relationid_rel DESC
                            LIMIT 1
                        ";
                        $results_related_image=$db->query($query_related_images);
                        $row_related_image=$db->mysql_array($results_related_image);
                        */
                    //}
                    //if( !empty($row_related_image['itemid']) )
                    //{
                        //$src="/images/size_m__imageid_".$row_related_image['itemid'].".jpg";
                        //$link="/images/size_o__imageid_".$row_related_image['itemid'].".jpg";
                        $src="/images/size_m__imageid_".$row['imageid'].".jpg";
                        $link="/images/size_o__imageid_".$row['imageid'].".jpg";
                        print "\t<a href='".$link."' title='View large size image' target='blank' >\n";
                            print "\t<img src='".$src."' alt='' />\n";
                        print "\t</a>\n";
                    }
                    else print "Image";
                print "\t</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<input type='file' name='image' value='' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['artworkID'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Artwork Type</th>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $artworkID=$_SESSION['new_record']['artworkID'];
                    else $artworkID=$row['artworkID'];
                            //var myHTMLRequest = new Request.HTML().get('options_categories.php?artworkid='this.options[selectedIndex].value);
                    $onchnage="
                        if( this.options[selectedIndex].value==1 || this.options[selectedIndex].value==2 || this.options[selectedIndex].value==12 || this.options[selectedIndex].value==13 || this.options[selectedIndex].value==15 )
                        {
                            $('catID').load('options_categories.php?artworkid='+this.options[selectedIndex].value);
                            document.getElementById('tr-category').style.display='';
                        }
                        else if( this.options[selectedIndex].value==5 || this.options[selectedIndex].value==6 || this.options[selectedIndex].value==7 || this.options[selectedIndex].value==8 )
                        {
                            $('catID').load('options_categories.php?artworkid='+this.options[selectedIndex].value);
                            document.getElementById('tr-category').style.display='';
                        }
                        else
                        {
                            document.getElementById('tr-category').style.display='none';
                        }
                        if( this.options[selectedIndex].value==5 || this.options[selectedIndex].value==7 || this.options[selectedIndex].value==8 )
                        {
                            document.getElementById('artworkID2').style.display='';
                        }
                        else
                        {
                            document.getElementById('artworkID2').style.display='none';
                        }
                        ";
                    print admin_html::get_artworks($db,"",$artworkID,"","onchange=\"".$onchnage."\"");
                    /*
                    if( $artworkID==5 || $artworkID==7 || $artworkID==8 || !$count )
                    {
                        if( $_GET['error'] ) $artworkID2=$_SESSION['new_record']['artworkID2'];
                        else $artworkID2=$row['artworkID2'];
                        print admin_html::get_artworks($db,"2",$artworkID2,"","");
                    }*/

                    $options_artworkid2=array();
                    if( $artworkID==5 || $artworkID==7 || $artworkID==8 ) $options_artworkid2['style']="";
                    else $options_artworkid2['style']="display:none;";
                    if( $_GET['error'] ) $artworkID2=$_SESSION['new_record']['artworkID2'];
                    else $artworkID2=$row['artworkID2'];
                    print admin_html::get_artworks($db,"2",$artworkID2,"","",$options_artworkid2);

                print "\t</td>\n";
            print "\t</tr>\n";


            if(
                $row['artworkID']==1 ||
                $row['artworkID']==2 ||
                $row['artworkID']==13 ||
                $row['artworkID']==12 ||
                $row['artworkID']==6 ||
                $row['artworkID']==15 ||
                !isset($_GET['paintid'])
            )
            {
                $style='';
            }
            else
            {
                $style='display:none;';
            }
            print "\t<tr id='tr-category' style='".$style."'>\n";
                print "\t<th class='th_align_left'>Category</th>\n";
                    print "\t<td colspan='5'>\n";
                        admin_html::select_categories_admin($db,"catID",$row['catID'],"","",""," WHERE artworkID='".$row['artworkID']."' ");
                print "\t</td>\n";
            print "\t</tr>\n";

            if( !empty($row['editionID']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Series/Editions</td>\n";
                    print "\t<td colspan='5'>\n";
                        print admin_html::get_editions($db,"",$row['editionID']);
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

            if( $row['artworkID']==4 )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Edition individual</td>\n";
                    print "\t<td colspan='5'>\n";
                        if( $row['edition_individual'] ) $checked="checked='checked'";
                        else $checked="";
                        print "\t<input type='checkbox' name='edition_individual' value='1' ".$checked." />\n";
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['year'] ) $class="error";
                else $class="";
                print "\t<th class='th_align_left ".$class."'>*Year</th>\n";
                print "\t<td>\n";
                    if( $_GET['error'] ) $year=$_SESSION['new_record']['year'];
                    else $year=$row['year'];
                    print admin_html::get_years("",$year,"");
                print "\t</td>\n";
                print "\t<td style='width:130px;' colspan='4'>";
                    if( $_GET['error'] ) $date=$_SESSION['new_record']['date'];
                    else $date=$row['date_admin'];
                    print "\t<span style='font-weight:700;'>Date </span><input type='text' value='".$date."' id='date' class='date dashboard' name='date' />";
                    print "\t<button type='button' class='calendar-icon date_toggler'></button>\n";
                print "\t</td>";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                    print "\tNumber\n";
                print "\t</th>\n";
                print "\t<td colspan='1'>\n";
                    if( $_GET['error'] ) $number=$_SESSION['new_record']['number'];
                    else $number=$row['number'];
                    print "\t<input type='text' name='number' value='".$number."' />\n";
                    if( $_SESSION['debug_page'] ) print " ".$row['number_search'];
                print "\t</td>\n";
                print "\t<th class='th_align_left'>\n";
                    print "\tOriginal number\n";
                print "\t</th>\n";
                print "\t<td colspan='3'>\n";
                    if( $_GET['error'] ) $number_old=$_SESSION['new_record']['number_old'];
                    else $number_old=$row['number_old'];
                    print "\t<input type='text' name='number_old' value='".$number_old."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            #these are used only for microsite paintings

            /*
            if( !empty($row['editionID']) && $_SESSION['debug_page'] )
            {
                print "\t<tr>\n";
                    print "\t<td colspan='6' class='td_no_padding'>\n";
                        print "\t<table>\n";
                            print "\t<tr>\n";
                                print "\t<th class='td_inside_table_first' style='width:165px;'>ed.Number</th>\n";
                                print "\t<td class='td_inside_table'>\n";
                                    print "\t<input type='text' name='ednumber' value='".$row['ednumber']."' class='input_field_admin_small' />\n";
                                print "\t</td>\n";
                                # Firenze microsite specipic
                                if( $row['editionID']==12798 )
                                {
                                    print "\t<th class='td_inside_table'>ed.Number(/XII)</th>\n";
                                    print "\t<td class='td_inside_table'>\n";
                                        print "\t<input type='text' name='ednumberroman' value='".$row['ednumberroman']."' class='input_field_admin_small' />\n";
                                    print "\t</td>\n";
                                    print "\t<th class='td_inside_table'>ed.Number(/7a.p.)</th>\n";
                                    print "\t<td class='td_inside_table_last'>\n";
                                        print "\t<input type='text' name='ednumberap' value='".$row['ednumberap']."' class='input_field_admin_small' />\n";
                                    print "\t</td>\n";
                                }
                            print "\t</tr>\n";
                        print "\t</table>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";

                if( $row['editionID']==12798 )
                {
                    # Firenze microsite specipic
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>City life page </th>\n";
                        print "\t<td colspan='5'>\n";
                            print "\t<input type='text' name='citylifepage' value='".$row['citylifepage']."' />\n";
                        print "\t</td>\n";
                    print "\t</tr>\n";
                }
            }
            */
            #end


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Height x Width</th>\n";
                print "\t<td colspan='5' style='width:340px'>\n";
                    if( $_GET['error'] ) $height=$_SESSION['new_record']['height'];
                    else $height=$row['height'];
                    print "\t<input type='text' name='height' value='".$height."' />\n";
                    print "\t X \n";
                    if( $_GET['error'] ) $width=$_SESSION['new_record']['width'];
                    else $width=$row['width'];
                    print "\t<input type='text' name='width' value='".$width."' />\n";

                    print "&nbsp;Size\n";
                    if( $_GET['error'] ) $size_parts=$_SESSION['new_record']['size_parts'];
                    else $size_parts=$row['size_parts'];
                    $options_select_size_type=array();
                    $options_select_size_type['name']="size_parts";
                    $options_select_size_type['selectedid']=$size_parts;
                    print admin_html::select_size_parts($db,$options_select_size_type);

                    print "&nbsp;";
                    if( $_GET['error'] ) $size_typeid=$_SESSION['new_record']['size_typeid'];
                    else $size_typeid=$row['size_typeid'];
                    $options_select_size_type=array();
                    $options_select_size_type['name']="size_typeid";
                    $options_select_size_type['selectedid']=$size_typeid;
                    print admin_html::select_painting_size_types($db,$options_select_size_type);
                    if( $_GET['error'] ) $size=$_SESSION['new_record']['size'];
                    else $size=$row['size'];
                    print "\t<input type='text' name='size' value='".$size."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Medium</th>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $museumid=$_SESSION['new_record']['mID'];
                    else $museumid=$row['mID'];
                    print admin_html::get_medium($db,"",$museumid);
                print "\t</td>\n";
            print "\t</tr>\n";

            /*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Museum</td>\n";
                print "\t<td colspan='5'>\n";
                    print admin_html::select_museums($db,"muID",$row['muID']);
                print "\t</td>\n";
            print "\t</tr>\n";
             */

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Photograph by</th>\n";
                print "\t<td>\n";
                    if( $_GET['error'] ) $optionid_photograph_courtesy=$_SESSION['new_record']['optionid_photograph_courtesy'];
                    else $optionid_photograph_courtesy=$row['optionid_photograph_courtesy'];
                    $options_select_photograph_courtesy=array();
                    $options_select_photograph_courtesy['name']="";
                    $options_select_photograph_courtesy['id']="";
                    select_dropdown($db,5,$optionid_photograph_courtesy,$options_select_photograph_courtesy);
                print "\t</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $optionid_photograph_courtesy_values=$_SESSION['new_record']['optionid_photograph_courtesy_values'];
                    else $optionid_photograph_courtesy_values=$row['optionid_photograph_courtesy_values'];
                    $options_select_photograph_courtesy_values=array();
                    $options_select_photograph_courtesy_values['name']="";
                    $options_select_photograph_courtesy_values['id']="";
                    select_dropdown($db,6,$optionid_photograph_courtesy_values,$options_select_photograph_courtesy_values);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Artwork notes</th>\n";
                print "\t<td colspan='5'>\n";

                    print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artwork_notesEN=$_SESSION['new_record']['artwork_notesEN'];
                            else $artwork_notesEN=$row['artwork_notesEN'];
                            print "\t<textarea class='form' name='artwork_notesEN' cols='29' rows='3'>".$artwork_notesEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artwork_notesDE=$_SESSION['new_record']['artwork_notesDE'];
                            else $artwork_notesDE=$row['artwork_notesDE'];
                            print "\t<textarea class='form' name='artwork_notesDE' cols='29' rows='3'>".$artwork_notesDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artwork_notesFR=$_SESSION['new_record']['artwork_notesFR'];
                            else $artwork_notesFR=$row['artwork_notesFR'];
                            print "\t<textarea class='form' name='artwork_notesFR' cols='29' rows='3'>".$artwork_notesFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artwork_notesIT=$_SESSION['new_record']['artwork_notesIT'];
                            else $artwork_notesIT=$row['artwork_notesIT'];
                            print "\t<textarea class='form' name='artwork_notesIT' cols='29' rows='3'>".$artwork_notesIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artwork_notesZH=$_SESSION['new_record']['artwork_notesZH'];
                            else $artwork_notesZH=$row['artwork_notesZH'];
                            print "\t<textarea class='form' name='artwork_notesZH' cols='29' rows='3'>".$artwork_notesZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            $options_tr_location=array();
            $options_tr_location['row']=$row;
            $options_tr_location['count']=$count;
            $options_tr_location['html']=$html;
            $options_tr_location['museums']=1;
            admin_html::draw_tr_location($db,$options_tr_location);


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Collection notes</th>\n";
                print "\t<td colspan='5'>\n";

                    print "\t<ul id='ul-admin-tabs-colnotes' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-colnotes' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $collection_notes_en=$_SESSION['new_record']['collection_notes_en'];
                            else $collection_notes_en=$row['collection_notes_en'];
                            print "\t<textarea class='form' name='collection_notes_en' cols='29' rows='3'>".$collection_notes_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $collection_notes_de=$_SESSION['new_record']['collection_notes_de'];
                            else $collection_notes_de=$row['collection_notes_de'];
                            print "\t<textarea class='form' name='collection_notes_de' cols='29' rows='3'>".$collection_notes_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $collection_notes_fr=$_SESSION['new_record']['collection_notes_fr'];
                            else $collection_notes_fr=$row['collection_notes_fr'];
                            print "\t<textarea class='form' name='collection_notes_fr' cols='29' rows='3'>".$collection_notes_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $collection_notes_it=$_SESSION['new_record']['collection_notes_it'];
                            else $collection_notes_it=$row['collection_notes_it'];
                            print "\t<textarea class='form' name='collection_notes_it' cols='29' rows='3'>".$collection_notes_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $collection_notes_zh=$_SESSION['new_record']['collection_notes_zh'];
                            else $collection_notes_zh=$row['collection_notes_zh'];
                            print "\t<textarea class='form' name='collection_notes_zh' cols='29' rows='3'>".$collection_notes_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            /*
            if( $row['artworkID']==5 || $row['artworkID2']==5
            	|| $row['artworkID']==6 || $row['artworkID2']==6
                || $row['artworkID']==7 || $row['artworkID2']==7
                || $row['artworkID']==8 || $row['artworkID2']==8
                || ( ($row['artworkID']==4 || $row['artworkID2']==4) && ( $row['number']=='72' || $row['number']=='78' ) ) ) $style='';
            else $style='display:none;';
            print "\t<tr id='tr-opp-additional-info' style='".$style."'>\n";
            */
            print "\t<tr id='tr-opp-additional-info' >\n";
                print "\t<td class='th_align_left' colspan='6'>\n";
                    print "\t<table  class='admin-table'>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left' colspan='2'>Additional info</th>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left' style='text-align:right;'>";
                                if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
                                {
                                    print "<span class='opp-verso-recto'>Verified Recto</span>";
                                }
                                print "<span class='opp-verso-recto'>Recto Signed</span>";
                                print "<span class='opp-verso-recto'>Also Signed</span>";
                                print "<span class='opp-verso-recto'>Dated</span>";
                                print "<span class='opp-verso-recto'>Also Dated</span>";
                                print "<span class='opp-verso-recto'>Numbered</span>";
                                print "<span class='opp-verso-recto'>Inscribed</span>";
                            print "</th>\n";
                            print "\t<td>";
                                if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
                                {
                                    if( $row_opp['verified_recto'] ) $checked=" checked='checked' ";
                                    else $checked="";
                                    print "\t<input type='checkbox' ".$checked." name='verified_recto' value='1' />\n";
                                    print "<br />";
                                }

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_signed_1_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_signed_1_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_signed_1_3' name='recto_signed_1_3' cols='100' rows='1'>".$row_opp['recto_signed_1_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_signed_2_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_signed_2_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_signed_2_3' name='recto_signed_2_3' cols='100' rows='1'>".$row_opp['recto_signed_2_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_dated_1_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_dated_1_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_dated_1_3' name='recto_dated_1_3' cols='100' rows='1'>".$row_opp['recto_dated_1_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_dated_2_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_dated_2_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_dated_2_3' name='recto_dated_2_3' cols='100' rows='1'>".$row_opp['recto_dated_2_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_numbered_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_numbered_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_numbered_3' name='recto_numbered_3' cols='100' rows='1'>".$row_opp['recto_numbered_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_inscribed_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_recto_inscribed_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='recto_inscribed_3' name='recto_inscribed_3' cols='100' rows='1'>".$row_opp['recto_inscribed_3']."</textarea>";
                            print "</td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left' style='text-align:right;'>";
                                if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
                                {
                                    print "<span class='opp-verso-recto'>Verified Verso</span>";
                                }
                                print "<span class='opp-verso-recto'>Verso Signed</span>";
                                print "<span class='opp-verso-recto'>Also Signed</span>";
                                print "<span class='opp-verso-recto'>Dated</span>";
                                print "<span class='opp-verso-recto'>Also Dated</span>";
                                print "<span class='opp-verso-recto'>Numbered</span>";
                                print "<span class='opp-verso-recto'>Inscribed</span>";
                            print "</th>\n";
                            print "\t<td>";

                                if( $_SESSION['userID']==1 || $_SESSION['userID']==19 || $_SESSION['userID']==31 )
                                {
                                    if( $row_opp['verified_verso'] ) $checked=" checked='checked' ";
                                    else $checked="";
                                    print "\t<input type='checkbox' ".$checked." name='verified_verso' value='1' />\n";
                                    print "<br />";
                                }

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_signed_1_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_signed_1_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_signed_1_3' name='verso_signed_1_3' cols='100' rows='1'>".$row_opp['verso_signed_1_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_signed_2_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_signed_2_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_signed_2_3' name='verso_signed_2_3' cols='100' rows='1'>".$row_opp['verso_signed_2_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_dated_1_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_dated_1_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_dated_1_3' name='verso_dated_1_3' cols='100' rows='1'>".$row_opp['verso_dated_1_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_dated_2_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_dated_2_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_dated_2_3' name='verso_dated_2_3' cols='100' rows='1'>".$row_opp['verso_dated_2_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_numbered_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_numbered_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_numbered_3' name='verso_numbered_3' cols='100' rows='1'>".$row_opp['verso_numbered_3']."</textarea>";
                                print "<br />";

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_inscribed_1";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                $options_select_loan_type=array();
                                $options_select_loan_type['name']="optionid_verso_inscribed_2";
                                $options_select_loan_type['id']=$options_select_loan_type['name'];
                                select_dropdown($db,3,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                                print "<textarea id='verso_inscribed_3' name='verso_inscribed_3' cols='100' rows='1'>".$row_opp['verso_inscribed_3']."</textarea>";
                            print "</td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Inscriptions</th>\n";
                            print "\t<td><input type='text' id='opp_inscriptions' name='opp_inscriptions' value='".$row_opp['inscriptions']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Notations</th>\n";
                            print "\t<td>";
                                //print "<input type='text' id='opp_notations' name='opp_notations' value='".$row_opp['notations']."' class='input-relation-itemid-sort' />";
                                print "<textarea id='opp_notations' name='opp_notations' cols='120' rows='1'>".$row_opp['notations']."</textarea>";
                            print "</td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>City</th>\n";
                            print "\t<td><input type='text' id='opp_city' name='opp_city' value='".$row_opp['city']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Country</th>\n";
                            print "\t<td><input type='text' id='opp_country' name='opp_country' value='".$row_opp['country']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Collection</th>\n";
                            print "\t<td><input type='text' id='opp_collection' name='opp_collection' value='".$row_opp['collection']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Private Collection</th>\n";
                            print "\t<td><input type='text' id='opp_private_col' name='opp_private_col' value='".$row_opp['private_col']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Copyright / Photographer</th>\n";
                            print "\t<td><input type='text' id='opp_copyright' name='opp_copyright' value='".$row_opp['copyright']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Publish</th>\n";
                            if( $row_opp['publish'] ) $checked="checked='checked'";
                            else $checked="";
                            print "\t<td><input type='checkbox' id='opp_publish' name='opp_publish' value='1' ".$checked." class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Source</th>\n";
                            print "\t<td><input type='text' id='opp_source' name='opp_source' value='".$row_opp['source']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Ready for Usage</th>\n";
                            if( $row_opp['ready_for_usage'] ) $checked="checked='checked'";
                            else $checked="";
                            print "\t<td style='text-align:left;'><input type='checkbox' id='opp_ready_for_usage' name='opp_ready_for_usage' value='1' ".$checked." class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Licence</th>\n";
                            print "\t<td><input type='text' id='opp_licence' name='opp_licence' value='".$row_opp['licence']."' class='input-relation-itemid-sort' /></td>\n";
                        print "\t</tr>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>HighRes W x H</th>\n";
                            print "\t<td>";
                                print "<input type='text' id='opp_width' name='opp_width' value='".$row_opp['width']."' class='' /> X ";
                                print "<input type='text' id='opp_height' name='opp_height' value='".$row_opp['height']."' class='' />";
                            print "</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Keywords</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $keywords=$_SESSION['new_record']['keywords'];
                    else $keywords=$row['keywords'];
                    print "\t<textarea rows='2' cols='99' name='keywords' >".$keywords."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $relation_typeid=$_SESSION['new_record']['relation_typeid'];
                    else $relation_typeid=$_GET['relation_typeid'];
                    print admin_html::select_relations($db,"relation_typeid",$relation_typeid,$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' id='itemid' name='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class=array();
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",1,$row['paintid'],2,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_GET['error'] ) $notes=$_SESSION['new_record']['notes'];
                    else $notes=UTILS::html_decode($row['notes']);
                    print "\t<textarea rows='8' cols='126' name='notes' >".$notes."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $count>0 && $_SESSION['debug_page'] )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Sort</td>\n";
                    print "\t<td colspan='5'>\n";
                        //print "\t<a href='/admin/paintings/edit/sort/sorting.php?paintid=".$row['paintid']."#".$row['paintid']."' title='' ><u>sort paintings</u></a>\n";
                        if( $row['artworkID']=="1" || $row['artworkID']=="2" || $row['artworkID']=="13" ) $artworkid="paintings";
                        else $artworkid=$row['artworkID'];
                        //print "\t<a href='/admin/paintings/edit/sort/?paintid=".$row['paintid']."&artworkid=".$artworkid."#".$row['paintid']."' title='' ><u>sort paintings</u></a>\n";
                        print "sort2 = ".$row['sort2'];
                        print "&nbsp;sort3 = ".$row['sort3'];
                    print "\t</td>\n";
                print "\t</tr>\n";

            }

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td colspan='5'>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td colspan='5'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print $html_submit_tr;

            #next previous buttons
            if( $count>0 )
            {
                if( !empty($previous_next_item_admin) )
                {
                    print "\t<tr>\n";
                        print "\t<td colspan='6' style='text-align:center;'>\n";
                            print $previous_next_item_admin;
                        print "\t</td>\n";
                    print "\t</tr>\n";
                }
            }
            # END next previous buttons

        print "\t</table>\n";
        print "\t<input type='hidden' name='paintid' value='".$row['paintid']."' />\n";
    print "\t</form>\n";

    # printing out relations
    $options_show_relations=array();
    $options_show_relations['html']=$html;
    $options_show_relations['row']=$row;
    if( $count>0 ) admin_html::show_relations($db,1,$row['paintid'],$options_show_relations);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
