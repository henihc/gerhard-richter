<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="notes_images";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY noteid DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_notes=QUERIES::query_painting_notes($db,$query_where_notes,$query_order,$query_limit);

    
    $results=$db->query($query_notes['query_without_limit']);
    $count=$db->numrows($results);
    $results_notes=$db->query($query_notes['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) 
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    if( $_GET['sort_by']=='noteid' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=noteid&sort_how=$sort_how' title='Order by noteid'><u>noteid</u></a></th>\n";

                    print "\t<th>image</th>\n";

                    if($_GET['sort_by']=='noteEN')
                    {
                        if($_GET['sort_how']=='asc')$sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:200px;'><a href='?sort_by=noteEN&sort_how=$sort_how' title='Order by noteEN'><u>noteEN</u></a></th>\n";

                    if($_GET['sort_by']=='noteDE')
                    {
                        if($_GET['sort_how']=='asc')$sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:200px;'><a href='?sort_by=noteDE&sort_how=$sort_how' title='Order by noteDE'><u>noteDE</u></a></th>\n";

                    if($_GET['sort_by']=='noteZH')
                    {
                        if($_GET['sort_how']=='asc')$sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:200px;'><a href='?sort_by=noteZH&sort_how=$sort_how' title='Order by noteZH'><u>noteZH</u></a></th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


        while( $row=$db->mysql_array($results_notes) )
        {
            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/paintings/notes/edit/?noteid=".$row['noteid']."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$row['noteid']."</a>\n";
                print "\t</td>";
                print "\t<td style='text-align:center;'>\n";
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=15 AND itemid2='".$row['noteid']."' ) OR ( typeid2=17 AND typeid1=15 AND itemid1='".$row['noteid']."' ) ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        $src="/images/size_ll__imageid_".$imageid.".jpg";
                        $link="/images/size_ll__imageid_".$imageid.".jpg";
                        if( !empty($imageid) ) print "<img src='".$src."' alt='' />";
                        else print "&nbsp;";
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row['noteEN'];
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row['noteDE'];
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row['noteZH'];
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No notes found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
