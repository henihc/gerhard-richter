<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=15&itemid=".$_GET['noteid'];

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_notes_image";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();





$db=new dbCLASS;


print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";

    $query_where_note=" WHERE noteid='".$_GET['noteid']."' ";
    $query_note=QUERIES::query_painting_notes($db,$query_where_note,$query_order,$query_limit);
    $results=$db->query($query_note['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table'>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=15 AND itemid2='".$row['noteid']."' ) OR ( typeid2=17 AND typeid1=15 AND itemid1='".$row['noteid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        $src="/images/size_ll__imageid_".$imageid.".jpg";
                        $link="/images/size_o__imageid_".$imageid.".jpg";
                        if( !empty($imageid) ) print "<a href='".$link."' ><img src='".$src."' alt='' /></a>";
                        else print "Image";
                print "\t</th>\n";
                print "\t<td>";
                    print "\t<input type='file' name='image' />";
                print "</td>\n";
            print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Text</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='noteEN' id='notes_en' cols='99' rows='6'>".$row['noteEN']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='noteDE' id='notes_de' cols='99' rows='6'>".$row['noteDE']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='noteFR' id='notes_fr' cols='99' rows='6'>".$row['noteFR']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='noteIT' id='notes_it' cols='99' rows='6'>".$row['noteIT']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='noteZH' id='notes_zh' cols='99' rows='6'>".$row['noteZH']."</textarea>\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Display thumbs</th>\n";
                print "\t<td>";
                    admin_html::select_display_tab_images($db,"display",$row['display']);
                print "</td>\n";
            print "\t</tr>\n";

           print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",15,$row['noteid'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $count>0 )
                    {
                        //print "\t<a href='#' onclick=\"delete_confirm2('/admin/paintings/notes/edit/del.php?noteid=".$row['noteid']."','".addslashes($row['noteEN'])."','')\" title=''><u>delete</u></a>\n";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/paintings/notes/edit/del.php?noteid=".$row['noteid']."";
                        $options_delete['text1']="Painting - Notes image - ".$row['noteEN'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "*Mandatory";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='noteid' value='".$row['noteid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,15,$row['noteid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
