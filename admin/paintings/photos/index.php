<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="photos";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    $query_order=" ORDER BY painting_photoid DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    //print $query_limit;
    $query_photos=QUERIES::query_painting_images($db,$query_where_photos,$query_order,$query_limit);


    $results=$db->query($query_photos['query_without_limit']);
    $count=$db->numrows($results);
    $results_photos=$db->query($query_photos['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {

        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) )
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    print "\t<th>painting_photoid</th>\n";
                    print "\t<th>paintid</th>\n";
                    print "\t<th>image</th>\n";
                    print "\t<th>type</th>\n";
                    print "\t<th>date modified</th>\n";
                    print "\t<th>date created</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


        while( $row=$db->mysql_array($results_photos) )
        {
            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/paintings/photos/edit/?painting_photoid=".$row['painting_photoid']."' style='text-decoration:underline;color:blue;' title='edit this photo'>".$row['painting_photoid']."</a>\n";
                print "\t</td>";
                print "\t<td style='text-align:center;'>\n";
                    $results_related_paint=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND typeid2=24 AND itemid2='".$row['painting_photoid']."' ) OR ( typeid2=1 AND typeid1=24 AND itemid1='".$row['painting_photoid']."' ) ORDER BY sort ASC, relationid DESC ");
                    $row_related_paint=$db->mysql_array($results_related_paint);
                    $paintid=UTILS::get_relation_id($db,"1",$row_related_paint);
                    print "\t<a href='/admin/paintings/edit/?paintid=".$paintid."' style='text-decoration:underline;color:blue;' title='edit painting'>".$paintid."</a>\n";
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                        $src="/images/paintingphotoid_".$row['painting_photoid']."__size_xs.jpg";
                        $link="/images/paintingphotoid_".$row['painting_photoid']."__size_l.jpg";
                        if( !empty($row['src']) ) print "<img src='".$src."' alt='' />";
                        else print "&nbsp;";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $row['typeid']==1 ) $type="Details";
                    elseif( $row['typeid']==2 ) $type="Installation Shots";
                    print $type;
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row['date_modified_admin'];
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row['date_created_admin'];
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No photos found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
