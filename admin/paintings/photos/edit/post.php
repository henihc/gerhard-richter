<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['typeid']) ) $_SESSION['new_record']['error']['typeid']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/paintings/photos/edit/?painting_photoid=".$_POST['painting_photoid']."&error=true");

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST);

$count_files=count($_FILES['image']['error']);
//print $count_files."-";

$query="UPDATE ".TABLE_PAINTING_PHOTOS." SET
            typeid='".$_POST['typeid']."',
            notes_admin='".$_POST['notes_admin']."'";
if( $_POST['submit']=="update" ) $query.=", date_modified=NOW()";


if( $_POST['submit']=="update" )
{
    $painting_photoid=$_POST['painting_photoid'];
    $query_update=" WHERE painting_photoid='".$painting_photoid."'";


            # RELATION adding
            admin_utils::add_relation($db,24,$painting_photoid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
            #end RELATION adding
}

### image upload
for($i=0;$i<$count_files;$i++ )
{
    /*
    print $i."<br />";
    print_r($_FILES);
    print "<br /><hr /><br />";
    */

    # generate image id
    if( $_POST['submit']=="add" )
    {
        $error_check=$_FILES['image']['error'][$i];
        $fileName = str_replace (" ", "_", $_FILES['image']['name'][$i]);
        $tmpName  = $_FILES['image']['tmp_name'][$i];
        $fileSize = $_FILES['image']['size'][$i];
        $fileType = $_FILES['image']['type'][$i];
    }
    elseif( $_POST['submit']=="update" )
    {
        $error_check=$_FILES['image']['error'];
        $fileName = str_replace (" ", "_", $_FILES['image']['name']);
        $tmpName  = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $fileType = $_FILES['image']['type'];
    }
    # END generate image id

    if( $error_check==0 )
    {
        if ( is_uploaded_file($tmpName) )
        {
            if( $_POST['submit']=="add" )
            {
                $maxsort=UTILS::max_table_value($db,"sort",TABLE_PAINTING_PHOTOS,"");
                $db->query("INSERT INTO ".TABLE_PAINTING_PHOTOS."(enable,sort,date_created) VALUES(1,'".$maxsort."',NOW()) ");
                $painting_photoid=$db->return_insert_id();
            }

            # update query
            $query_update=$query." WHERE painting_photoid='".$painting_photoid."' ";

            if( $_POST['submit']=="add" || $_POST['submit']=="update" )
            {
                $db->query($query_update);
                if( $_POST['submit']=="add" )
                {
                    admin_utils::admin_log($db,1,6,$painting_photoid);
                }
                if( $_POST['submit']=="update" )
                {
                    admin_utils::admin_log($db,2,6,$painting_photoid);
                }
            }
            else
            {
                admin_utils::admin_log($db,"",6,$painting_photoid,2);
            }
            # END update query

            $getimagesize = getimagesize($tmpName);

            switch( $getimagesize['mime'] )
            {
                case 'image/gif' : $imgSrc = imagecreatefromgif($tmpName); $ext = ".gif"; break;
                case 'image/png' : $imgSrc = imagecreatefrompng($tmpName); $ext = ".png"; break;
                case 'image/jpeg' : $imgSrc = imagecreatefromjpeg($tmpName); $ext = ".jpg"; break;
                case 'image/bmp' : $imgSrc = UTILS::imagecreatefrombmp($tmpName); $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }

            $dir=DATA_PATH."/painting/photos";
            $new_file_name=$painting_photoid.".jpg";
            $new_file_path=$dir."/original/".$painting_photoid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,SIZE_PAINTING_PHOTOS_LARGE_HEIGHT,SIZE_PAINTING_PHOTOS_LARGE_WIDTH,$dir.'/large/'.$new_file_name);
            //UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

            # copy original
            if (!copy($tmpName, $new_file_path))
            {
                print "Error Uploading File.";
            }
            else
            {
                print "ok";
                $db->query("UPDATE ".TABLE_PAINTING_PHOTOS." SET src_old='".$painting_photoid."-".$fileName."', src='".$new_file_name."' WHERE painting_photoid='".$painting_photoid."'");

                # create cache image
                //$src="/images/paintingphotoid_".$painting_photoid."__size_o__thumb_1__border_1.png";
                $src="/images/paintingphotoid_".$painting_photoid."__thumb_1__size_o.png";
                $src_cache_uri=$painting_photoid.".png";
                $values['url']="/painting/photos";
                //$values['debug']=1;
                UTILS::cache_uri($src,$src_cache_uri,1,$values);
                #end

            }
            #end copy original

            # RELATION adding
            admin_utils::add_relation($db,24,$painting_photoid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
            #end RELATION adding

        }
    }
}




$url="/admin/paintings/photos/edit/?painting_photoid=".$painting_photoid."&paintid=".$_POST['paintid']."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
