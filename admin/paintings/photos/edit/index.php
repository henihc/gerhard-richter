<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=24&itemid=".$_GET['painting_photoid'];

# menu selected
$html->menu_admin_selected="paintings";
$html->menu_admin_sub_selected="add_photo";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



$query_where_painting_photo=" WHERE painting_photoid='".$_GET['painting_photoid']."' ";
$query_painting_photo=QUERIES::query_painting_images($db,$query_where_painting_photo);
$results_painting_photo=$db->query($query_painting_photo['query_without_limit']);
$count=$db->numrows($results_painting_photo);
$row=$db->mysql_array($results_painting_photo);


    $query_where_image=" WHERE painting_photoid='".$_GET['painting_photoid']."' ";
    $query_image=QUERIES::query_painting_images($db,$query_where_image,$query_order,$query_limit);
    $results=$db->query($query_image['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            #error and update info
            if( $_GET['error'] ) print "<tr><td colspan='6' class='error'>*Please complete mandatory fields</td></tr>";
            if( $_GET['task']=="update" ) print "<tr><td colspan='6' class='valid'>Update successfully!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='6' class='valid'>Insert successfully!</td></tr>";
            #error and update info end

                if( !empty($row['date_modified_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date modified</th>\n";
                        print "\t<td>".$row['date_modified_admin']."</td>\n";
                    print "\t</tr>\n";
                }   

                if( !empty($row['date_created_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date created</th>\n";
                        print "\t<td>".$row['date_created_admin']."</td>\n";
                    print "\t</tr>\n";
                } 

                print "\t<tr>\n";
                    if( $_SESSION['new_record']['error']['typeid'] ) $class="error";
                    else $class="";
                    print "\t<th class='th_align_left ".$class."'>\n";
                        print "*Type";
                    print "\t</th>\n";
                    print "\t<td>\n";
                        if( $_GET['error'] ) $typeid=$_SESSION['new_record']['typeid'];
                        else $typeid=$row['typeid'];
                        $values=array();
                        $values['name']="typeid";
                        $values['selectedid']=$typeid;
                        admin_html::select_painting_photo_type($db,$values);
                    print "\t</td>\n";
                print "\t</tr>\n";

 /*               
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Image title</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_en' id='title_en' value='".$row['title_en']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_de' id='title_de' value='".$row['title_de']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_fr' id='title_fr' value='".$row['title_fr']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_ch' id='title_ch' value='".$row['title_ch']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";
  */

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>\n";
                        if( $count ) 
                        {
                            print "\t<a href='/images/paintingphotoid_".$row['painting_photoid']."__size_l.jpg' target='blank' title='View image in full screen'>\n";
                                print "\t<img src='/images/paintingphotoid_".$row['painting_photoid']."__size_xs.jpg' alt='image".$row['painting_photoid']."' />\n";
                            print "\t</a>\n";
                        }
                        else print "Image";
                    print "\t</th>\n";
                    print "\t<td>\n";
                        if( $count==1 ) 
                        {
                            $multiple="";
                            $name="image";
                        }
                        else 
                        {
                            $multiple="multiple=''";
                            $name="image[]";
                        }
                        print "\t<input type='file' name='".$name."' ".$multiple." />\n";
                        //print "\t<input type='file' name='image' />\n";
                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Admin notes</td>\n";
                    print "\t<td>\n";
                        print "\t<textarea rows='7' cols='99' name='notes_admin' id='notes_admin' >".$row['notes_admin']."</textarea>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Relation</th>\n";
                    print "\t<td>\n";
                        print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                        print "&nbsp;";
                        print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                        print "&nbsp;";
                        $class['select_relations_sort']="select_relations_sort";
                        print admin_html::select_relations_sort($db,"relations_sort",24,$row['painting_photoid'],1,$selected,$class);
                        print "&nbsp;";
                        print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                    print "\t</td>\n";
                print "\t</tr>\n";


                print "\t<tr>\n";
                    print "\t<td>";
                        if( $count )
                        {
                            //print "\t<input type='button' value='delete' title='Delete this painting' onclick=\"delete_confirm2('del.php?painting_photoid=".$row['painting_photoid']."','','Are you sure you wish to delete this Painting Photo?')\" />\n";
                            $options_delete=array();
                            $options_delete['url_del']="del.php?painting_photoid=".$row['painting_photoid']."";
                            $options_delete['text1']="Painting - Photo";
                            //$options_delete['text2']="";
                            //$options_delete['html_return']=1;
                            admin_html::delete_button($db,$options_delete);
                        }
                        else print "* Mandatory";
                    print "</td>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
                print "\t</tr>\n";

            print "\t</table>\n";
            print "\t<input type='hidden' name='painting_photoid' value='".$row['painting_photoid']."' />\n";
        print "\t</form>\n";

        # printing out relations
        if( $count>0 ) admin_html::show_relations($db,24,$row['painting_photoid']);
        #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
