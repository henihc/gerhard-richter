<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->head('admin');

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);
$_GET=UTILS::ifemptyGET($_GET,40);

print "\t<div id='div_admin'>\n";

    # menu admin
    print "\t<a href='/admin' title=''><u>main menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/museums' title='Museums'><u>museums</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/museums/edit' title='Add museum'><u>add museum</u></a><br /><br />\n";
    #end menu admin

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY muID ASC ";
    if( $_GET['sp']!='all' ) $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_museums=QUERIES::query_museums($db,$query_where_museums,$query_order,$query_limit);

    
    $results=$db->query($query_museums['query_without_limit']);
    $count=$db->numrows($results);
    $results_museums=$db->query($query_museums['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {   
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        else $url="?p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);
        print "\t<a href='/admin/museums/?sp=all&sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."' title='Show all'>Show all</a>\n";

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    if( $_GET['sort_by']=='muID' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:70px;'><a href='?sort_by=muID&sort_how=".$sort_how."&sp=".$_GET['sp']."' title='Order by museum id'><u>museumid</u></a></th>\n";

                    if( $_GET['sort_by']=='museum' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=museum&sort_how=".$sort_how."&sp=".$_GET['sp']."' title='Order by museum'><u>museum</u></a></th>\n";
                    print "\t<th>paintings</th>\n";
                    print "\t<th>literature</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


            while( $row=$db->mysql_array($results_museums) )
            {
                $results_museums_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=16 AND itemid1='".$row['muID']."' AND typeid2=1 ) OR ( typeid2=16 AND itemid2='".$row['muID']."' AND typeid1=1 ) ");
                $count_museums_relations=$db->numrows($results_museums_relations);
                $results_museums_relations2=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=16 AND itemid1='".$row['muID']."' AND typeid2=12 ) OR ( typeid2=16 AND itemid2='".$row['muID']."' AND typeid1=12 ) ");
                $count_museums_relations_books=$db->numrows($results_museums_relations2);
                print "\t<tr>\n";

                    print "\t<td style='text-align:center;'>\n";
                        print "\t<a href='/admin/museums/edit/?museumid=".$row['muID']."' style='text-decoration:underline;color:blue;' title='edit this museum'>".$row['muID']."</a>\n";
                    print "\t</td>";
                    print "\t<td>\n";
                        print $row['museum'];
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print $count_museums_relations;
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print $count_museums_relations_books;
                    print "\t</td>\n";

                print "\t</tr>\n";
            }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No museum found!</p>\n";

print "\t</div>\n";


$html->foot();
?>
