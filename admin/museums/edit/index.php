<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->css[]="/css/autocompleter.css";
$html->js[]="/js/common.js";
$html->js[]="/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/js/autocompleter/observer.js";
$html->js[]="/js/autocompleter/autocompleter.js";
$html->js[]="/js/autocompleter/autocompleter.request.js";
$html->js[]="/js/autocompleter/autocompleter.load.js";
$html->js[]="/js/tabs/mootools-tabs-admin.js";
$html->js[]="/js/relations_sort.php?relation_typeid=16&itemid=".$_GET['museumid'];
$html->head('admin');

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);

$query_where_museum=" WHERE muID='".$_GET['museumid']."' ";
$query_museum=QUERIES::query_museums($db,$query_where_museum,$query_order,$query_limit);
$results=$db->query($query_museum['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
if( $count>0 ) $row['museumid']=$row['muID'];

print "\t<div id='div_admin' >\n";

    # menu admin
    print "\t<a href='/admin' title=''><u>main menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/museums' title='Museums'><u>museums</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/museums/edit' title='Add museum'><u>add museum</u></a><br /><br />\n";
    #end menu admin

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($_GET['task']) )
            {
                if( $_GET['task']=="delete_image" ) $text="Image delete";
                elseif( $_GET['task']=="delete" ) $text="Exhibition delete";
                elseif( $_GET['task']=="update" ) $text="Update";
                elseif( $_GET['task']=="add" ) $text="Insert ";
                print "\t<tr>\n";
                    print "\t<td colspan='2' style='color:green;'>".$text." successful!</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Museum title</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $museum=$_SESSION['new_record']['museum'];
                    else $museum=$row['museum'];
                    print "\t<textarea id='museum' name='museum' cols='67' disabled='disabled' rows='2'>".$museum."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th th_align_left'>Collection additional</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $collection_desc_en=$_SESSION['new_record']['collection_desc_en'];
                    else $collection_desc_en=$row['collection_desc_en'];
                    print "\t<textarea id='museum' name='collection_desc_en' disabled='disabled' cols='67' rows='2'>".$collection_desc_en."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            $options_tr_location=array();
            $options_tr_location['row']=$row;
            $options_tr_location['count']=$count;
            $options_tr_location['html']=$html;
            $options_tr_location['museums']=1;
            admin_html::draw_tr_location($db,$options_tr_location);

            /*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",16,$row['museumid'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";
             */

            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $count ) print "\t<a href='#' title='Delete this museum' onclick=\"delete_confirm2('del.php?museumid=".$row['museumid']."','".addslashes($row['museum'])."','')\" ><u>delete</u></a>\n";
                    else print "*Mandatory";
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='museumid' value='".$row['museumid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

    # printing out relations    
    if( $count>0 ) 
    {
        $options_show_relations=array();
        $options_show_relations['html']=$html;
        admin_html::show_relations($db,16,$row['museumid'],$options_show_relations);
    }
    #end

print "\t</div>\n";


unset($_SESSION['new_record']);
$html->foot();
?>
