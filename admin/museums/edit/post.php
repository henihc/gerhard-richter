<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
//if( empty($_POST['museum']) && empty($_POST['museum']) ) $_SESSION['new_record']['error']['museum']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/museums/edit/?museumid=".$_POST['museumid']."&error=true");

$html = new html_elements;
$html->head('admin');

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['museum'];
$museum_search=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_MUSEUM."(date_created) VALUES(NOW())");  
    $museumid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $museumid=$_POST['museumid'];
} 
else UTILS::redirect("/");

$museum_search_value=$museum_search['text_lower_converted']." ".$museum_search['text_lower_german_converted'];

$query="UPDATE ".TABLE_MUSEUM." SET 
        locationid='".$_POST['locationid']."',
        cityid='".$_POST['cityid']."',
        countryid='".$_POST['countryid']."',
        optionid_loan_type='".$_POST['optionid_loan_type']."',
        loan_locationid='".$_POST['locationid1']."',
        loan_cityid='".$_POST['cityid1']."',
        loan_countryid='".$_POST['countryid1']."'";
if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
$query.=" WHERE muID='".$museumid."' ";

if( $_POST['submit']=="add" )
{
    $db->query($query);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);
}


if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    # RELATION adding
    admin_utils::add_relation($db,16,$museumid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}


unset($_SESSION['new_record']);
$url="/admin/museums/edit/?museumid=".$museumid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
