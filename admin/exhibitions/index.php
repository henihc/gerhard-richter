<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="exhibitions";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY start_year_admin DESC, typeid DESC, start_month_admin DESC, start_day_admin DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    //$query_limit = " LIMIT 4000";
    $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions,$query_order,$query_limit);

    
    $results=$db->query($query_exhibitions['query_without_limit']);
    $count=$db->numrows($results);
    $results_exhibitions=$db->query($query_exhibitions['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) 
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    if( $_GET['sort_by']=='exID' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=exID&sort_how=$sort_how' title='Order by exhibition id'><u>id</u></a></th>\n";

                    if( $_GET['sort_by']=='title_original' )
                    {
                        if($_GET['sort_how']=='asc') $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=title_original&sort_how=$sort_how' title='Order by title'><u>title original - location </u></a></th>\n";

                    if($_GET['sort_by']=='date_from')
                    {
                        if($_GET['sort_how']=='asc')$sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:280px;'><a href='?sort_by=date_from&sort_how=$sort_how' title='Order by date'><u>date</u></a></th>\n";

                    if($_GET['sort_by']=='typeid')
                    {
                        if($_GET['sort_how']=='asc') $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=typeid&sort_how=$sort_how' title='Order by type'><u>type</u></a></th>\n";

                    if($_GET['sort_by']=='enable')
                    {
                        if($_GET['sort_how']=='asc') $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=enable&sort_how=$sort_how' title='Order by enable'><u>enable</u></a></th>\n";

                    print "\t<th>&nbsp;</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


        while( $row=$db->mysql_array($results_exhibitions) )
        {
            $row=UTILS::html_decode($row);
            print "\t<tr>\n";
                print "\t<td>\n";
                    print "\t<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$row['exID']."</a>\n";
                print "\t</td>";
                print "\t<td>\n";
                    print $row['title_original'];
                    $values_location=array();
                    $values_location['row']=$row;
                    $location=location($db,$values_location);
                    if( !empty($location) )
                    {
                        print "\t<span style='color:#666;'>\n";
                            print "<br />".$location;
                        print "\t</span>\n";
                    }
                print "\t</td>\n";
                $date=exhibition_date($db,$row);
                print "\t<td>\n";
                    print $date;
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $row['typeid']==1 ) $type="Solo";
                    elseif( $row['typeid']==2 ) $type="Group";
                    print $type;
                print "\t</td>\n";
                print "\t<td>\n";
                    if( $row['enable']==1 ) $enable="Yes";
                    else $enable="No";
                    print $enable;
                print "\t</td>\n";
                print "\t<td>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exID']."' ) ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_xs__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    print "\t<a href='".$link."' title='' >\n";
                        print "\t<img src='".$src."' alt='' />\n";
                    print "\t</a>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No exhibitions found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
