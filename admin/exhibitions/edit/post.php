<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;

$_POST=$db->filter_parameters($_POST,1);

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_EXHIBITIONS."(date_created) VALUES(NOW())");
    $exhibitionid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $exhibitionid=$_POST['exhibitionid'];
}
else UTILS::redirect("/");

$query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
$query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
$results=$db->query($query_exhibition['query']);
$row=$db->mysql_array($results,0);

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_EXHIBITIONS,$_POST['title_original'],"exID",$exhibitionid,"titleurl_en","en");

//print_r($_POST);print "<br />";
//print_r($_POST['title_original']);print "<br />";
//print_r($titleurl_en);print "<br />";
//exit;
if( empty($_POST['start_date']) ) $start_date="NULL";
else $start_date="'".$_POST['start_date']."'";
if( empty($_POST['end_date']) ) $end_date="NULL";
else $end_date="'".$_POST['end_date']."'";

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['titleDE'];
$title_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleFR'];
$title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleIT'];
$title_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_original'];
$title_search_original=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

# prepare search_keywords
$values_accented=array();
$values_accented['text']=$_POST['title_original'];
$search_keywords=UTILS::replace_accented_letters($db,$values_accented);
# END prepare search_keywords

$query="UPDATE ".TABLE_EXHIBITIONS." SET
        newsid='".$_POST['news_exhibition']."',
        title_original='".$_POST['title_original']."',
        title_original_search_1='".$title_search_original['text_lower_converted']."',
        title_original_search_2='".$title_search_original['text_lower_german_converted']."',
        titleEN='".$_POST['titleEN']."',
        titleDE='".$_POST['titleDE']."',
        titleFR='".$_POST['titleFR']."',
        titleIT='".$_POST['titleIT']."',
        titleZH='".$_POST['titleZH']."',
        title_search_1='".$title_search_de['text_lower_converted']."',
        title_search_2='".$title_search_de['text_lower_german_converted']."',
        title_search_3='".$title_search_fr['text_lower_converted']."',
        title_search_4='".$title_search_it['text_lower_converted']."',
        titleurl='".$titleurl_en."',
        description_enable='".$_POST['description_enable']."',
        descriptionEN='".$_POST['descriptionEN']."',
        descriptionDE='".$_POST['descriptionDE']."',
        descriptionFR='".$_POST['descriptionFR']."',
        descriptionIT='".$_POST['descriptionIT']."',
        descriptionZH='".$_POST['descriptionZH']."',
        typeid='".$_POST['typeid']."',
        notes='".$_POST['notes']."',
        locationid='".$_POST['locationid']."',
        cityid='".$_POST['cityid']."',
        countryid='".$_POST['countryid']."',
        guide_desc='".$_POST['guide_desc']."',
        date_from=".$start_date.",
        date_to=".$end_date.",
        showmonth_start='".$_POST['date_type_start']."',
        showmonth_end='".$_POST['date_type_end']."',
        showyear_start='".$_POST['showyear_start']."',
        showyear_end='".$_POST['showyear_end']."',
        tour_button_url='".$_POST['tour_button_url']."',
        enable='".$_POST['enable']."',
        search_keywords='".$search_keywords['text_lower_converted']."'";
if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
$query.=" WHERE exID='".$exhibitionid."' ";

//print $query;
//exit;

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    # RELATION adding
    admin_utils::add_relation($db,4,$exhibitionid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,15,$_GET['exhibitionid']);
    }
    elseif( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,15,$_GET['exhibitionid']);
    }
    else
    {
        admin_utils::admin_log($db,"",15,$_POST['exhibitionid'],2);
    }
}

### image upload
if($_FILES['exh_cover_image']['error']==0)
{
	$fileName = str_replace (" ", "_", $_FILES['exh_cover_image']['name']);
    $fileName = str_replace ("'", "", $fileName);
    $fileName = str_replace ('"', "", $fileName);
	$tmpName  = $_FILES['exh_cover_image']['tmp_name'];
	$fileSize = $_FILES['exh_cover_image']['size'];
	$fileType = $_FILES['exh_cover_image']['type'];

		if( is_uploaded_file($tmpName) )
		{

            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            $getimagesize = getimagesize($tmpName);

            switch( $getimagesize['mime'] )
            {
                case 'image/gif'  : $ext = ".gif"; break;
                case 'image/png'  : $ext = ".png"; break;
                case 'image/jpeg' : $ext = ".jpg"; break;
                case 'image/bmp'  : $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,4,$exhibitionid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o__border_1__maxwidth_38__maxheight_45.jpg";
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__border_1__maxwidth_38__maxheight_45.png";
                //$src_cache_uri=$imageid.".jpg";
                $src_cache_uri=$imageid.".png";
                UTILS::cache_uri($src,$src_cache_uri,1);
                #end

                # if image uploaded enable it
                $db->query("UPDATE ".TABLE_EXHIBITIONS." SET image_enable=1 WHERE exID = '".$exhibitionid."' LIMIT 1 ");

            }


		}
}
#end image upload

# leaflet upload
if($_FILES['guide']['error']==0)
{
    $fileName = str_replace (" ", "_", $_FILES['guide']['name']);
    $fileName = str_replace ("", "'", $fileName);
    $fileName = str_replace ("", '"', $fileName);
    $tmpName  = $_FILES['guide']['tmp_name'];
    $fileSize = $_FILES['guide']['size'];
    $fileType = $_FILES['guide']['type'];

    $new_file_path=DATA_PATH."/files/guides/".$fileName;

    if( is_uploaded_file($tmpName) )
    {
        if (!copy($tmpName, $new_file_path))
        {
            exit("Error Uploading File.");
        }
        else $db->query("UPDATE ".TABLE_EXHIBITIONS." SET src_guide='".$fileName."' WHERE exID = '".$exhibitionid."'");
    }
}
#end leaflet upload

# check if exhibtion has some relation
$values_exhibition_rel=array();
$values_exhibition_rel['exhibitionid']=$exhibitionid;
$values_exhibition_rel['src_guide']=$row['src_guide'];
$relations=UTILS::check_relations($db,$values_exhibition_rel);
if( $relations )
{
    $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=1 WHERE exID = '".$exhibitionid."'");
}
else
{
    $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=0 WHERE exID = '".$exhibitionid."'");
}
# end

# ************************
# if exhibition doesnt have exhibition catalogue image uploaded
# check if it has any painting relation with valid image and use this
# image as an exhibition cover image

$values_query_paint=array();
$values_query_paint['columns']=",titleEN as title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table, r.relationid";
$query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);

$results_related_paint=$db->query("SELECT r.typeid1,r.itemid1,r.typeid2,r.itemid2,".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=4 AND r.itemid1='".$exhibitionid."' AND r.typeid2=1 ) OR ( r.typeid2=4 AND r.itemid2='".$exhibitionid."' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ");
$count_related_paint=$db->numrows($results_related_paint);
$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$exhibitionid."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$exhibitionid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
$row_related_image=$db->mysql_array($results_related_image);
$imageid1=UTILS::get_relation_id($db,"17",$row_related_image);
//print $count_related_paint."-".$imageid1."<br />";
//print_r($row_related_image);
if( empty($imageid1) && $count_related_paint>0 )
{
    $row_related_paint=$db->mysql_array($results_related_paint);
    $paintid=UTILS::get_relation_id($db,"1",$row_related_paint);
    $results_related_image2=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$paintid."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$paintid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image2=$db->mysql_array($results_related_image2);
    $imageid2=UTILS::get_relation_id($db,"17",$row_related_image2);
    if( !empty($imageid2) )
    {
        $fileName=$imageid2.".jpg";
        $tmpName=DATA_PATH."/images_new/original/".$fileName;
        $src_display="/images/size_o__imageid_".$imageid2.".jpg";

        /*
        print "paint imageid=<a href='".$src_display."'>".$imageid2."</a> - ";
        print "paintid=".$paintid." - count paintings=".$count_related_paint." - ";
        print "\t<a href='/admin/exhibitions/edit/?exhibitionid=".$exhibitionid."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$exhibitionid."</a>\n";
        print "<br />";
        */


        //exit;
        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

        $dir=DATA_PATH."/images_new";
        $new_file_name=$imageid.".jpg";
        $new_file_path=$dir."/original/".$imageid.".jpg";

        UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

        #copy original
        if (!copy($tmpName, $new_file_path))
        {
            exit("Error Uploading File.");
        }
        else
        {
            $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

            # RELATION adding
            admin_utils::add_relation($db,17,$imageid,4,$exhibitionid,$_POST['relations_sort']);
            #end RELATION adding

            # create cache image
            $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__border_1__maxwidth_38__maxheight_45.png";
            $src_cache_uri=$imageid.".png";
            UTILS::cache_uri($src,$src_cache_uri,1);
            #end

            $db->query("UPDATE ".TABLE_EXHIBITIONS." SET image_enable=0 WHERE exID = '".$exhibitionid."'");
            //exit;
        }
    }
}

# END ********************

unset($_SESSION['new_record']);
$url="/admin/exhibitions/edit/?exhibitionid=".$exhibitionid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
