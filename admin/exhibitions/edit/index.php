<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# sort relation select drop dwon list
$html->js[]="/admin/js/relations_sort.php?relation_typeid=4&itemid=".$_GET['exhibitionid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="exhibitions";
$html->menu_admin_sub_selected="add_exhibition";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$query_where_exhibition=" WHERE exID='".$_GET['exhibitionid']."' ";
$query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
$results=$db->query($query_exhibition['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results,0);
if( $count>0 ) $row['exhibitionid']=$row['exID'];

print "\t<script type='text/javascript'>\n";
    print "\twindow.addEvent('domready', function() {";
        print "new DatePicker($$('.start_date'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_start' });\n";
        print "new DatePicker($$('.end_date'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_end' });\n";
        print "\tCKEDITOR.replace('descriptionEN', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('descriptionDE', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('descriptionFR', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('descriptionIT', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('descriptionZH', { toolbar : 'Large' });\n";
        print "\tvar tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{\n";
            print "\thash:false,\n";
            print "\ttransitionDuration:0,\n";
            print "\tstartIndex:0,\n";
            print "\thover:true\n";
        print "\t});\n";
            /*
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-location li a','#div-admin-tabs-info-location .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
             */
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc li a','#div-admin-tabs-info-desc .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t});\n";
print "\t</script>\n";

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($_GET['task']) )
            {
                if( $_GET['task']=="delete_image" ) $text="Image delete";
                elseif( $_GET['task']=="delete" ) $text="Exhibition delete";
                elseif( $_GET['task']=="update" ) $text="Update";
                elseif( $_GET['task']=="add" ) $text="Insert ";
                print "\t<tr>\n";
                    print "\t<td colspan='2' style='color:green;'>".$text." successful!</td>\n";
                print "\t</tr>\n";
            }

            if( $count>0 )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Options</th>\n";
                    print "\t<td>\n";
                    $query_next_previous_exhibition="SELECT
                        exID AS exhibitionid,
                        DATE_FORMAT(date_from ,'%d') AS start_day_admin,
                        DATE_FORMAT(date_from ,'%m') AS start_month_admin,
                        DATE_FORMAT(date_from ,'%Y') AS start_year_admin
                        FROM ".TABLE_EXHIBITIONS."
                        ORDER BY
                        start_year_admin DESC,
                        typeid DESC,
                        start_month_admin DESC,
                        start_day_admin DESC ";
                    admin_html::previous_next_item_admin($db,"exhibitionid",$row['exhibitionid'],"/admin/exhibitions/edit/?exhibitionid=",$query_next_previous_exhibition);
                    print "\t</td>\n";
                    print "\t<td style='text-align:right;'>\n";
                        $values_exh_url=array();
                        $values_exh_url['exhibitionid']=$row['exhibitionid'];
                        $url=UTILS::get_exhibition_url($db,$values_exh_url);
                        if( $count ) print "\t<a href='".$url."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                    print "\t</td>\n";

                print "\t</tr>\n";
            }

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td colspan='2'>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td colspan='2'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td colspan='2'>";
                    if( $row['enable'] || $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                    elseif( $count && !$row['enable'] ) $checked="";
                    else $checked="checked='checked'";
                    print "\t<input name='enable' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Type</th>\n";
                print "\t<td colspan='2'>\n";
                    $values_exh_typeid=array();
                    $values_exh_typeid['name']="typeid";
                    $values_exh_typeid['selectedid']=$row['typeid'];
                    admin_html::select_exhibition_typeid($db,$values_exh_typeid);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>News exhibition</th>\n";
                print "\t<td colspan='2'>";
                    if( !empty($row['newsid']) ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' id='news_exhibition' ".$checked." name='news_exhibition' value='1' />";
                    if( $count )
                    {
                        print "\t<br /><div style='float:left;' >\n";
                            $title_original_show=html_entity_decode($row['title_original'],ENT_NOQUOTES,DB_ENCODEING);
                            print $title_original_show.", ";
                            $values_location=array();
                            $values_location['row']=$row;
                            $location=location($db,$values_location);
                            print $location.", ";
                            $date=exhibition_date($db,$row);
                            print $date;
                        print "\t</div>\n";
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Tour button url</th>\n";
                print "\t<td colspan='2'>";
                    print "<input type='text' id='tour_button_url' name='tour_button_url' value='".$row['tour_button_url']."' class='admin-input-large'>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title original</th>\n";
                print "\t<td colspan='2'>";
                    $title_original=html_entity_decode($row['title_original'],ENT_NOQUOTES,DB_ENCODEING);
                    print "\t<textarea id='title_original' name='title_original' cols='121' rows='2'>".$title_original."</textarea>";
                    $values_exh_url=array();
                    $values_exh_url['exhibitionid']=$row['exID'];
                    $exh_url=UTILS::get_exhibition_url($db,$values_exh_url);
                    print "<br /><a href='".$exh_url."' ><u>".HTTP_SERVER.$exh_url."</u></a>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>Title</td>\n";
                print "\t<td colspan='2'>\n";

                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleEN=$_SESSION['new_record']['titleEN'];
                            else $titleEN=$row['titleEN'];
                            $titleEN=html_entity_decode($titleEN,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea name='titleEN' cols='121' rows='2'>".$titleEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleDE=$_SESSION['new_record']['titleDE'];
                            else $titleDE=$row['titleDE'];
                            $titleDE=html_entity_decode($titleDE,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea name='titleDE' cols='121' rows='2'>".$titleDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleFR=$_SESSION['new_record']['titleFR'];
                            else $titleFR=$row['titleFR'];
                            $titleFR=html_entity_decode($titleFR,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea name='titleFR' cols='121' rows='2'>".$titleFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleIT=$_SESSION['new_record']['titleIT'];
                            else $titleIT=$row['titleIT'];
                            $titleIT=html_entity_decode($titleIT,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea name='titleIT' cols='121' rows='2'>".$titleIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleZH=$_SESSION['new_record']['titleZH'];
                            else $titleZH=$row['titleZH'];
                            $titleZH=html_entity_decode($titleZH,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea name='titleZH' cols='121' rows='2'>".$titleZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<table class='admin-table' cellspacing='0'>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Country</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="countryid";
                                $values['selectedid']=$row['countryid'];
                                $values['onchange']=" onchange=\"
                                                        $('cityid').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-city').style.display='';\" ";
                                admin_html::select_locations_country($db,$values);
                                if( $count ) UTILS::admin_edit("/admin/locations/edit/?typeid=3&countryid=".$row['countryid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-city' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>City</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="cityid";
                                $values['selectedid']=$row['cityid'];
                                $values['onchange']=" onchange=\"
                                                        $('locationid').load('/admin/locations/options_locations.php?countryid='+document.getElementById('countryid').options[document.getElementById('countryid').selectedIndex].value+'&cityid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-location').style.display='';\" ";
                                $values['where']=" WHERE countryid='".$row['countryid']."' ";
                                admin_html::select_locations_city($db,$values);
                                if( $count && !empty($row['cityid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=2&cityid=".$row['cityid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['cityid']) && empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-location' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>Location</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="locationid";
                                $values['selectedid']=$row['locationid'];
                                $values['where']=" WHERE countryid='".$row['countryid']."' AND cityid='".$row['cityid']."' ";
                                admin_html::select_locations($db,$values);
                                if( $count && !empty($row['locationid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=1&locationid=".$row['locationid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";

/*
                    print "\t<br /><br /><ul id='ul-admin-tabs-location' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-location' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $locationEN=$_SESSION['new_record']['locationEN'];
                            else $locationEN=$row['locationEN'];
                            $locationEN=html_entity_decode($locationEN,ENT_NOQUOTES,DB_ENCODEING);
                            //print "\t<textarea name='locationEN' cols='121' rows='2'>".$locationEN."</textarea>\n";
                            print $locationEN;
                            print "\t<input type='hidden' name='locationEN' value='".$locationEN."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $locationDE=$_SESSION['new_record']['locationDE'];
                            else $locationDE=$row['locationDE'];
                            $locationDE=html_entity_decode($locationDE,ENT_NOQUOTES,DB_ENCODEING);
                            //print "\t<textarea name='locationDE' cols='121' rows='2'>".$locationDE."</textarea>\n";
                            print $locationDE;
                            print "\t<input type='hidden' name='locationDE' value='".$locationDE."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $locationFR=$_SESSION['new_record']['locationFR'];
                            else $locationFR=$row['locationFR'];
                            $locationFR=html_entity_decode($locationFR,ENT_NOQUOTES,DB_ENCODEING);
                            //print "\t<textarea name='locationFR' cols='121' rows='2'>".$locationFR."</textarea>\n";
                            print $locationFR;
                            print "\t<input type='hidden' name='locationFR' value='".$locationFR."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $locationZH=$_SESSION['new_record']['locationZH'];
                            else $locationZH=$row['locationZH'];
                            $locationZH=html_entity_decode($locationZH,ENT_NOQUOTES,DB_ENCODEING);
                            //print "\t<textarea name='location' cols='121' rows='2'>".$locationZH."</textarea>\n";
                            print $locationZH;
                            print "\t<input type='hidden' name='locationZH' value='".$locationZH."' />\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
 */


                print "\t</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable description</th>\n";
                print "\t<td colspan='2'>";
                    if( $row['description_enable']==1 ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' id='description_enable' ".$checked." name='description_enable' value='1' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</th>\n";
                print "\t<td colspan='2'>";

                    print "\t<ul id='ul-admin-tabs-desc' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='descriptionEN' id='descriptionEN' cols='110' rows='7'>".$row['descriptionEN']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='descriptionDE' id='descriptionDE' cols='110' rows='7'>".$row['descriptionDE']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='descriptionFR' id='descriptionFR' cols='110' rows='7'>".$row['descriptionFR']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='descriptionIT' id='descriptionIT' cols='110' rows='7'>".$row['descriptionIT']."</textarea>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            print "\t<textarea name='descriptionZH' id='descriptionZH' cols='110' rows='7'>".$row['descriptionZH']."</textarea>";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exhibitionid']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exhibitionid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                $row_related_image=$db->mysql_array($results_related_image);
                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                if( empty($imageid) ) $text="Image";
                else
                {
                    $src="/images/size_s__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    $text="<a href='".$link."'>";
                        $text.="<img src='".$src."' alt='' />";
                    $text.="</a>";
                }
                print "\t<th>".$text."</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input type='file' name='exh_cover_image' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                    if( !empty($row['src_guide']) )
                    {
                        $src=DATA_PATH_DISPLAY."/files/guides/".$row['src_guide'];
                        $titleurl=UTILS::row_text_value($db,$row,"titleurl");
                        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                        $mime=finfo_file($finfo, $src);
                        finfo_close($finfo);
                        $tmp=explode(".", $row['src_guide']);
                        $ext=$tmp[count($tmp)-1];
                        print "\t<a href='/exhibitions/guide/exhibition-guide-".$titleurl.".".$ext."' title='Download file'><u>Guide</u></a>\n";
                        //print "\t<input type='button' name='delete' value='delete' onclick=\"delete_confirm2('del_guide.php?exhibitionid=".$row['exhibitionid']."','Guide','')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="del_guide.php?exhibitionid=".$row['exhibitionid']."";
                        $options_delete['text1']="Exhibition guide";
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "\tGuide\n";
                print "\t</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input type='file' name='guide' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Guide desc</th>\n";
                print "\t<td colspan='2'>";
                    print "<input type='text' id='guide_desc' name='guide_desc' value='".$row['guide_desc']."' class='admin-input-large'>";
                print "</td>\n";
            print "\t</tr>\n";

/*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location EN</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<textarea id='locationEN' name='locationEN' cols='67' rows='2'>".$row['locationEN']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location DE</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<textarea id='locationDE' name='locationDE' cols='67' rows='2'>".$row['locationDE']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location ZH</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<textarea id='locationZH' name='locationZH' cols='67' rows='2'>".$row['locationZH']."</textarea>";
                print "</td colspan='2'>\n";
            print "\t</tr>\n";
*/

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Month display</th>\n";
                print "\t<td colspan='2'>";
                    print "To display only month check this<br />";
                    if( $row['showmonth_start']==1 ) $checked="checked='checked'";
                    else $checked="";
                    print "\tFor start date: <input type='checkbox' id='date_type_start' ".$checked." name='date_type_start' value='1' />";
                    if( $row['showmonth_end']==1 ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<br />For end date: <input type='checkbox' id='date_type_end' ".$checked." name='date_type_end' value='1' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year Display</th>\n";
                print "\t<td colspan='2'>";
                    print "To display only year check this<br />";
                    if( $row['showyear_start']==1 ) $checked="checked='checked'";
                    else $checked="";
                    print "\tFor start date: <input type='checkbox' id='showyear_start' ".$checked." name='showyear_start' value='1' /> ";
                    if( $row['showyear_end']==1 ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<br />For end date: <input type='checkbox' id='showyear_end' ".$checked." name='showyear_end' value='1' /> ";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                $date=exhibition_date($db,$row);
                print "\t<th class='th_align_left'>Start date</th>\n";
                print "\t<td style='width:130px;'>";
                    print "\t<input type='text' value='".$row['start_date_admin']."' id='start_date' class='start_date dashboard' name='start_date' />";
                    print "\t<button type='button' class='calendar-icon date_toggler_start'></button>\n";
                print "\t</td>";
                print "\t<td>";
                    print "<span>&nbsp;&nbsp;&nbsp;&nbsp;".$date."</span>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>End date</th>\n";
                print "\t<td>";
                    print "\t<input id='end_date' class='end_date dashboard' name='end_date' type='text' value='".$row['end_date_admin']."' />";
                    print "\t<button type='button' class='calendar-icon date_toggler_end'></button>\n";
                print "\t</td>";
                print "\t<td>";
                    print "&nbsp;";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='2'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",4,$row['exhibitionid'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</th>\n";
                print "\t<td colspan='2'>";
                    $row['notes']=UTILS::html_decode($row['notes']);
                    print "\t<textarea rows='7' cols='99' name='notes' >".$row['notes']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    if( $count )
                    {
                        //print "\t<input type='button' name='delete' value='delete' onclick=\"delete_confirm2('del.php?exhibitionid=".$row['exhibitionid']."','".addslashes($row['title_original'])."','')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="del.php?exhibitionid=".$row['exhibitionid']."";
                        $options_delete['text1']="Exhibition - ".$row['title_original'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "*Mandatory";
                print "\t</td>\n";
                print "\t<td colspan='2'>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='exhibitionid' value='".$row['exhibitionid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

    # printing out relations
    if( $count>0 ) admin_html::show_relations($db,4,$row['exhibitionid']);
    #end


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
