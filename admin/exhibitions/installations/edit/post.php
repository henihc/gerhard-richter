<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_EXHIBITIONS_INSTALLATIONS."(date_created) VALUES(NOW())");  
    $installationid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $installationid=$_POST['installationid'];
} 
else UTILS::redirect("/");

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_EXHIBITIONS_INSTALLATIONS,$_POST['title_en'],"installationid",$installationid,"titleurl_en","en");

$query="UPDATE ".TABLE_EXHIBITIONS_INSTALLATIONS." SET
            title_en='".$_POST['title_en']."',
            title_de='".$_POST['title_de']."',
            title_fr='".$_POST['title_fr']."',
            title_it='".$_POST['title_it']."',
            title_zh='".$_POST['title_zh']."',
            titleurl='".$titleurl_en."',
            notes_admin='".$_POST['notes_admin']."' ";
if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
$query.=" WHERE installationid='".$installationid."' ";

if( $_POST['submit']=="add" )
{
    $db->query($query);

    admin_utils::admin_log($db,1,17,$installationid);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);

    admin_utils::admin_log($db,2,17,$installationid);
}
else
{
    admin_utils::admin_log($db,"",17,$_POST['installationid'],2);
}

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    # RELATION adding
    admin_utils::add_relation($db,5,$installationid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}


unset($_SESSION['new_record']);
$url="/admin/exhibitions/installations/edit/?installationid=".$installationid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
