<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

if( !empty($_GET['installationid']) )
{
    $db=new dbCLASS;
    

    $_GET=$db->db_prepare_input($_GET);

    $db->query("DELETE FROM ".TABLE_EXHIBITIONS_INSTALLATIONS." WHERE installationid='".$_GET['installationid']."' ");
    $db->query("DELETE FROM ".TABLE_EXHIBITIONS_INSTALLATIONS_IMAGES." WHERE installationid='".$_GET['installationid']."' ");
    $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=5 AND itemid1='".$_GET['installationid']."' ) OR ( typeid2=5 AND itemid2='".$_GET['installationid']."' ) ");

    admin_utils::admin_log($db,3,17,$_GET['installationid']);

    

    UTILS::redirect("/admin/exhibitions/installations/?task=delete");
}
else 
{
	admin_utils::admin_log($db,3,17,$_GET['installationid'],2);

	UTILS::redirect("/");
}
