<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

#autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=5&itemid=".$_GET['installationid'];

# menu selected
$html->menu_admin_selected="exhibitions";
$html->menu_admin_sub_selected="add_installation";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$query_where_installation=" WHERE installationid='".$_GET['installationid']."' ";
$query_installation=QUERIES::query_exhibitions_installations($db,$query_where_installation,$query_order,$query_limit);
$results=$db->query($query_installation['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";


    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($_GET['task']) )
            {
                if( $_GET['task']=="delete" ) $text="Installation delete";
                elseif( $_GET['task']=="update" ) $text="Update";
                elseif( $_GET['task']=="add" ) $text="Insert ";
                print "\t<tr>\n";
                    print "\t<th class='th_align_left' colspan='2' style='color:green;'>".$text." successful!</th>\n";
                print "\t</tr>\n";
            }

            if( $count )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Options</th>\n";
                    print "\t<td>";
                        print "\t<ol>\n";
                            print "\t<li>1. <a href='/admin/exhibitions/installations/images/edit/?installationid=".$row['installationid']."' title='Add images' style='color:blue;'><u>Add images</u></a></li>\n";
                        print "\t</ol>\n";
                    print "</td>\n";
                    print "\t<td style='text-align:right;'>\n";
                        $values_exh_url=array();
                        $values_exh_url['installationid']=$row['installationid'];
                        $url=UTILS::get_exhibition_url($db,$values_exh_url);
                        if( $count ) print "\t<a href='".$url."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td colspan='2'>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td colspan='2'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</td>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['title_en'];
                            print "\t<input type='text' name='title_en' id='title_en' value=\"".$title_en."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['title_de'];
                            print "\t<input type='text' name='title_de' id='title_de' value=\"".$title_de."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['title_fr'];
                            print "\t<input type='text' name='title_fr' id='title_fr' value=\"".$title_fr."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['title_it'];
                            print "\t<input type='text' name='title_it' id='title_it' value=\"".$title_it."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['title_zh'];
                            print "\t<input type='text' name='title_zh' id='title_zh' value=\"".$title_zh."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "<br /><a href='".$url."' ><u>".HTTP_SERVER.$url."</u></a>";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='2'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",5,$row['installationid'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<textarea rows='7' cols='99' name='notes_admin' >".$row['notes_admin']."</textarea>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count>0 )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/exhibitions/installations/edit/del.php?installationid=".$row['installationid']."','".addslashes($row['title_en'])."','');\" title='Delete this installation'><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/exhibitions/installations/edit/del.php?installationid=".$row['installationid']."";
                        $options_delete['text1']="Exhibitions - Installation - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count>0 ) $value="update";
                else $value="add";
                print "\t<td colspan='2'><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='installationid' value='".$row['installationid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,5,$row['installationid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
