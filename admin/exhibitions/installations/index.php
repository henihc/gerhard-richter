<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="exhibitions";
$html->menu_admin_sub_selected="installations";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY installationid DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_installations=QUERIES::query_exhibitions_installations($db,$query_where_installations,$query_order,$query_limit);

    
    $results=$db->query($query_installations['query_without_limit']);
    $count=$db->numrows($results);
    $results_installations=$db->query($query_installations['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {   
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) 
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    if( $_GET['sort_by']=='installationid' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=installationid&sort_how=$sort_how' title='Order by installation id'><u>installationid</u></a></th>\n";

                    print "\t<th>image</th>\n";
                    print "\t<th>exhibitionid</th>\n";
                    print "\t<th>title exh. original - location</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


        while( $row=$db->mysql_array($results_installations) )
        {
            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/exhibitions/installations/edit/?installationid=".$row['installationid']."' style='text-decoration:underline;color:blue;' title='Edit this installation'>".$row['installationid']."</a>\n";
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=5 AND itemid2='".$row['installationid']."' ) OR ( typeid2=17 AND typeid1=5 AND itemid1='".$row['installationid']."' ) ORDER BY sort ASC, relationid DESC ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_xs__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    if( !empty($imageid) )
                    {
                        print "\t<a href='".$link."' title='' >\n";
                            print "\t<img src='".$src."' alt='' />\n";
                        print "\t</a>\n";
                    }
                    else print "no image";
                print "\t</td>\n";
                $results_exhibition_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=5 AND itemid1='".$row['installationid']."' AND typeid2=4 ) OR ( typeid2=5 AND itemid2='".$row['installationid']."' AND typeid1=4 ) ORDER BY sort ASC, relationid DESC ");
                $row_exhibition_relations=$db->mysql_array($results_exhibition_relations);
                $exhibitionid=UTILS::get_relation_id($db,"4",$row_exhibition_relations);
                $query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
                $query_order_exhibition=" ORDER BY e.date_from ASC ";
                $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order_exhibition);
                $results_exhibition=$db->query($query_exhibition['query']);
                $row_exhibition=$db->mysql_array($results_exhibition);
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/exhibitions/edit/?exhibitionid=".$exhibitionid."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$exhibitionid."</a>\n";
                print "\t</td>\n";
                print "\t<td>\n";
                    print $row_exhibition['title_original'];
                    if( !empty($row_exhibition['locationEN']) || !empty($row_exhibition['locationDE']) )
                    {   
                        print "\t<span style='color:#666;'>\n";
                            if( !empty($row_exhibition['locationEN']) ) print "<br />".$row_exhibition['locationEN'];
                            elseif( !empty($row_exhibition['locationDE']) ) print "<br />".$row_exhibition['locationDE'];
                        print "\t</span>\n";
                    }  
                print "\t</td>";
                print "\t<td>\n";
                    print "\t<a href='/admin/exhibitions/installations/images/edit/?installationid=".$row['installationid']."' style='text-decoration:underline;color:blue;' title='Add installation images'>add images</a>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No exhibition installations found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
