<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['installationid']) ) UTILS::redirect('/admin/exhibitions/installations');

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# swf uploader
$html->css[]="/admin/js/swfupload/swfupload.css";
$html->js[]="/admin/js/swfupload/swfupload.js";
$html->js[]="/admin/js/swfupload/swfupload.queue.js";
$html->js[]="/admin/js/swfupload/fileprogress.js";
$html->js[]="/admin/js/swfupload/handlers.js";


# menu selected
$html->menu_admin_selected="exhibitions";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();


$query_where_installation_image=" WHERE installationid='".$_GET['installationid']."' AND imageid='".$_GET['imageid']."' ";
$query_installation_image=QUERIES::query_exhibitions_installations_images($db,$query_where_installation_image,$query_order,$query_limit);
$results=$db->query($query_installation_image['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";

        if( $count<=0 )
        {
            print "\t\tvar swfu;\n";
                print "\t\tvar settings = {\n";
                    print "\t\tflash_url : '/admin/js/swfupload/swfupload.swf',\n";
                    print "\t\tupload_url: '/admin/exhibitions/installations/images/edit/post/post.php',\n"; // Relative to the SWF file
                    print "\t\tpost_params: {'PHPSESSID' : '".session_id()."', 'title_en' : '', 'title_de' : '', 'title_fr' : '', 'title_it' : '', 'title_zh' : '', 'notes_en' : '', 'notes_de' : '', 'notes_fr' : '', 'notes_it' : '', 'notes_zh' : '', 'installationid' : '".$_GET['installationid']."' },\n";
                    print "\t\tfile_size_limit : '100 MB',\n";
                    print "\t\tfile_types : '*.jpg;*.gif;*.png,*.bmp',\n";
                    print "\t\tfile_types_description : 'All Files',\n";
                    print "\t\tfile_upload_limit : 0,\n";
                    print "\t\tfile_queue_limit : 0,\n";
                    print "\t\tcustom_settings : {\n";
                        print "\t\tprogressTarget : 'fsUploadProgress',\n";
                        print "\t\tcancelButtonId : 'btnCancel'\n";
                    print "\t\t},\n";
                    print "\t\tdebug: false,\n";

                    // Button settings
                    print "\t\tbutton_image_url: '/img/swfupload.png',\n"; // Relative to the Flash file
                    print "\t\tbutton_width: '65',\n";
                    print "\t\tbutton_height: '29',\n";
                    print "\t\tbutton_placeholder_id: 'spanButtonPlaceHolder',\n";
                    print "\t\tbutton_text: '<span class=\"theFont\">Upload</span>',\n";
                    print "\t\tbutton_text_style: '.theFont { font-size: 16; }',\n";

                    // The event handler functions are defined in handlers.js
                    print "\t\tfile_queued_handler : fileQueued,\n";
                    print "\t\tfile_queue_error_handler : fileQueueError,\n";
                    print "\t\tfile_dialog_complete_handler : fileDialogComplete,\n";
                    print "\t\tupload_start_handler : uploadStart,\n";
                    print "\t\tupload_progress_handler : uploadProgress,\n";
                    print "\t\tupload_error_handler : uploadError,\n";
                    print "\t\tupload_success_handler : uploadSuccess,\n";
                    print "\t\tupload_complete_handler : uploadComplete,\n";
                    print "\t\tqueue_complete_handler : queueComplete\n";  // Queue plugin event
                print "\t\t};\n";

                print "\t\tswfu = new SWFUpload(settings);\n";
            }
    print "\t};\n";
print "\t</script>\n";

    print "\t<form action='post/post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($_GET['task']) )
            {
                if( $_GET['task']=="update" ) $text="Update";
                elseif( $_GET['task']=="add" ) $text="Insert ";
                print "\t<tr>\n";
                    print "\t<td colspan='2' style='color:green;'>".$text." successful!</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Options</th>\n";
                print "\t<td>";
                    print "\t<ol>\n";
                        print "\t<li>1. <a href='/admin/exhibitions/installations/images/edit/?installationid=".$_GET['installationid']."' title='Add images' style='color:blue;'><u>Add images</u></a></li>\n";
                        print "\t<li>2. <a href='/admin/exhibitions/installations/edit/?installationid=".$_GET['installationid']."' title='Back to installation edit' style='color:blue;'><u>Back to installation edit</u></a></li>\n";
                    print "\t</ol>\n";
                print "</td>\n";
            print "\t</tr>\n";

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Image title</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_en' id='title_en' value='".$row['title_en']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_de' id='title_de' value='".$row['title_de']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_fr' id='title_fr' value='".$row['title_fr']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_it' id='title_it' value='".$row['title_it']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_zh' id='title_zh' value='".$row['title_zh']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Notes</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_en' id='notes_en' cols='99' rows='6'>".$row['notes_en']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_de' id='notes_de' cols='99' rows='6'>".$row['notes_de']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_fr' id='notes_fr' cols='99' rows='6'>".$row['notes_fr']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_it' id='notes_it' cols='99' rows='6'>".$row['notes_it']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_zh' id='notes_zh' cols='99' rows='6'>".$row['notes_zh']."</textarea>\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                    if( $count ) print "\t<img src='/includes/retrieve.image.php?installation_imageid=".$row['imageid']."&size=s' alt='' />\n";
                    else print "Images";
                print "\t</th>\n";
                print "\t<td>\n";
                    if( $count>0 )
                    {
                        print "\t<input type='file' name='Filedata' />\n";
                    }
                    else
                    {
                      
                        print "\t<fieldset class='flash' id='fsUploadProgress'>\n";
                            print "\t<legend>Upload Queue</legend>\n";
                        print "\t</fieldset>\n";
                        print "\t<div id='divStatus'>0 Files Uploaded</div>\n";
                        print "\t<div>\n";
                            print "\t<span id='spanButtonPlaceHolder'></span>\n";
                            print "\t<input id='btnCancel' type='button' value='Cancel All Uploads' onclick='swfu.cancelQueue();' disabled='disabled' style='font-size: 8pt;' />\n";
                        print "\t</div>\n";
                    
                    }
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $count>0 )
            {
                print "\t<tr>\n";
                    print "\t<td>";
                        if( $count>0 )
                        {
                            //print "<a href='#' onclick=\"delete_confirm2('/admin/exhibitions/installations/images/edit/del.php?imageid=".$row['imageid']."&installationid=".$row['installationid']."');\" title='delete this image'><u>delete</u></a>";
                            $options_delete=array();
                            $options_delete['url_del']="/admin/exhibitions/installations/images/edit/del.php?imageid=".$row['imageid']."&installationid=".$row['installationid']."";
                            $options_delete['text1']="Exhibitions - Installation - Image - ".$row['title_en'];
                            //$options_delete['text2']="";
                            //$options_delete['html_return']=1;
                            admin_html::delete_button($db,$options_delete);
                        }
                        else print "* Mandatory";
                    print "</td>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
                print "\t</tr>\n";
            }



        print "\t</table>\n";
        print "\t<input type='hidden' name='installationid' value='".$row['installationid']."' />\n";
        print "\t<input type='hidden' name='imageid' value='".$row['imageid']."' />\n";
    print "\t</form>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
