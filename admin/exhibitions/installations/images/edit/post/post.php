<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_search.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/utils.php");

//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//exit('ok');
//admin_html::admin_sign_in();

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);


if( isset($_POST['submit']) )
{
    if( !empty($_POST['installationid']) ) 
    {
        $db->query("UPDATE ".TABLE_IMAGES." SET 
                title_en='".$_POST['title_en']."', 
                title_de='".$_POST['title_de']."', 
                title_fr='".$_POST['title_fr']."', 
                title_it='".$_POST['title_it']."', 
                title_zh='".$_POST['title_zh']."', 
                notes_en='".$_POST['notes_en']."', 
                notes_de='".$_POST['notes_de']."',  
                notes_fr='".$_POST['notes_fr']."',  
                notes_it='".$_POST['notes_it']."',  
                notes_zh='".$_POST['notes_zh']."',  
                date_modified=NOW() 
                WHERE imageid='".$_POST['imageid']."'");
        admin_utils::admin_log($db,2,17,$_POST['installationid'],1,$_POST['imageid']);
    }
    else
    {
        $imageid=$_POST['imageid'];

        admin_utils::admin_log($db,1,17,$_POST['installationid'],1,$imageid);
    }
}
else
{
    admin_utils::admin_log($db,"",17,$_POST['installationid'],2,$_POST['imageid']);
}

### image upload
if( $_FILES['Filedata']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['Filedata']['name']);
    $tmpName  = $_FILES['Filedata']['tmp_name'];
    $fileSize = $_FILES['Filedata']['size'];
    $fileType = $_FILES['Filedata']['type'];

    if ( is_uploaded_file($tmpName) )
    {   
        if( !empty($_POST['installationid']) ) 
        {
            if( empty($_POST['imageid']) )
            {
                $db->query("INSERT INTO ".TABLE_IMAGES."(title_en,title_de,title_fr,title_it,title_zh,notes_en,notes_de,notes_fr,notes_it,notes_zh,enable,date_created) 
                                    VALUES('".$_POST['title_en']."','".$_POST['title_de']."','".$_POST['title_fr']."','".$_POST['title_it']."','".$_POST['title_zh']."',
                                            '".$_POST['notes_en']."','".$_POST['notes_de']."','".$_POST['notes_fr']."','".$_POST['notes_it']."','".$_POST['notes_zh']."',1,NOW()) ");
                $imageid=$db->return_insert_id();           
            }
        }
    }   

                $getimagesize = getimagesize($tmpName);
    
                switch( $getimagesize['mime'] )
                {   
                    case 'image/gif'  : $ext = ".gif"; break;
                    case 'image/png'  : $ext = ".png"; break;
                    case 'image/jpeg' : $ext = ".jpg"; break;
                    case 'image/bmp'  : $ext = ".bmp"; break;
                    default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
                }   

                $dir=DATA_PATH."/images_new";
                $new_file_name=$imageid.".jpg";
                $new_file_path=$dir."/original/".$imageid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,SIZE_INSTAL_PHOTO_XLARGE_HEIGHT,SIZE_INSTAL_PHOTO_XLARGE_WIDTH,$dir.'/xlarge/'.$new_file_name); 

            #copy original
            if (!copy($tmpName, $new_file_path))
            {   
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,5,$_POST['installationid'],$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o.png";
                $src_cache_uri=$imageid.".png";
                UTILS::cache_uri($src,$src_cache_uri,1);

                /*
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__bgcolor_e4e4e4.jpg";
                $src_cache_uri=$imageid.".jpg";
                $values_cache=array();
                $values_cache['cache_dir']="cache2";
                UTILS::cache_uri($src,$src_cache_uri,1,$values_cache);
                 */
                #end

                print "Ok!";
            }

}
# file upload end

if( isset($_POST['submit']) )
{
    UTILS::redirect("/admin/exhibitions/installations/images/edit/?installationid=".$_POST['installationid']."&imageid=".$imageid."&task=".$_POST['submit']);
    
}


