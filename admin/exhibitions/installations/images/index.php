<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

if( empty($_GET['installationid']) ) UTILS::redirect('/admin/exhibitions/installations');

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/js/sort/prototype.js";
$html->js[]="/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="exhibitions";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY sort ASC ";
    //$query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_where_installations_images=" WHERE installationid='".$_GET['installationid']."' ";
    $query_installations_images=QUERIES::query_exhibitions_installations_images($db,$query_where_installations_images,$query_order,$query_limit);

    $results=$db->query($query_installations_images['query_without_limit']);
    $count=$db->numrows($results);
    $results_installations_images=$db->query($query_installations_images['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    { 

        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>sort</th>\n";
                print "\t<th>imageid</th>\n";
                print "\t<th>image</th>\n";
                print "\t<th>titleEN</th>\n";
                print "\t<th>notesEN</th>\n";
            print "\t</tr>\n";
        print "\t</table>\n";

        $ii=1;
        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results_installations_images,0) )
            {
                print "\t<li id='item_".$row['imageid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' >sort</td>\n";
                            print "\t<td style='text-align:center;'>\n";
                                print "\t<a href='/admin/exhibitions/installations/images/edit/?imageid=".$row['imageid']."&installationid=".$row['installationid']."' style='text-decoration:underline;color:blue;' title='Edit this installation image'>".$row['imageid']."</a>\n";
                            print "\t</td>\n";
                            print "\t<td>";
                                print "<img src='/includes/retrieve.image.php?installation_imageid=".$row['imageid']."&size=xs' alt='' />";
                            print "</td>\n";
                            print "\t<td>".$row['title_en']."</td>\n";
                            print "\t<td>".$row['notes_en']."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }


    }
    else print "\t<p class='p_admin_no_data_found'>No images found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
