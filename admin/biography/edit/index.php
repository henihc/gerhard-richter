<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="biography";
$html->menu_admin_sub_selected="add_biography";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$query_where_biography=" WHERE biographyid='".$_GET['biographyid']."' ";
$query_biography=QUERIES::query_biography($db,$query_where_biography,$query_order,$query_limit);
$results=$db->query($query_biography['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'text_en', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_de', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_fr', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_it', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_zh', { toolbar : 'Large' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title-left li a','#div-admin-tabs-info-title-left .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-text li a','#div-admin-tabs-info-text .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";



    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th style='text-align:left;'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th style='text-align:left;'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( $_GET['error'] && $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                elseif( $row['enable'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<th style='text-align:left;'>Enable</th>\n";
                print "\t<td><input name='enable' $checked type='checkbox' value='1' class='' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_SESSION['new_record']['error']['title_en'] ) $class="error";
                    else $class="";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab ".$class."' href='#' >*English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['title_en'];
                            print "\t<input type='text' name='title_en' id='title_en' value=\"".$title_en."\" class='admin-input-large'>";
                            if( !empty($row['titleurl']) ) print "\t<a href='/biography/".$row['titleurl']."' title='".$row['titleurl']."'><u>".$row['titleurl']."</u></a>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['title_de'];
                            print "\t<input type='text' name='title_de' id='title_de' value=\"".$title_de."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['title_fr'];
                            print "\t<input type='text' name='title_fr' id='title_fr' value=\"".$title_fr."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['title_it'];
                            print "\t<input type='text' name='title_it' id='title_it' value=\"".$title_it."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['title_zh'];
                            print "\t<input type='text' name='title_zh' id='title_zh' value=\"".$title_zh."\" class='admin-input-large'>";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title left</td>\n";
                print "\t<td colspan='5'>\n";
                    if( $_SESSION['new_record']['error']['title_left_en'] ) $class="error";
                    else $class="";
                    print "\t<ul id='ul-admin-tabs-title-left' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab ".$class."' href='#' >*English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title-left' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_left_en=$_SESSION['new_record']['title_left_en'];
                            else $title_left_en=$row['title_left_en'];
                            print "\t<input type='text' name='title_left_en' id='title_left_en' value=\"".$title_left_en."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_left_de=$_SESSION['new_record']['title_left_de'];
                            else $title_left_de=$row['title_left_de'];
                            print "\t<input type='text' name='title_left_de' id='title_left_de' value=\"".$title_left_de."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_left_fr=$_SESSION['new_record']['title_left_fr'];
                            else $title_left_fr=$row['title_left_fr'];
                            print "\t<input type='text' name='title_left_fr' id='title_left_fr' value=\"".$title_left_fr."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_left_it=$_SESSION['new_record']['title_left_it'];
                            else $title_left_it=$row['title_left_it'];
                            print "\t<input type='text' name='title_left_it' id='title_left_it' value=\"".$title_left_it."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_left_zh=$_SESSION['new_record']['title_left_zh'];
                            else $title_left_zh=$row['title_left_zh'];
                            print "\t<input type='text' name='title_left_zh' id='title_left_zh' value=\"".$title_left_zh."\" class='admin-input-large'>";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th style='vertical-align:top;text-align:left;'>Text</th>\n";
                print "\t<td>\n";
                    print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-text' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_en=$_SESSION['new_record']['text_en'];
                            else $text_en=$row['text_en'];
                            print "\t<textarea name='text_en' id='text_en' rows='25' cols='120'>".$text_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_de=$_SESSION['new_record']['text_de'];
                            else $text_de=$row['text_de'];
                            print "\t<textarea name='text_de' id='text_de' rows='25' cols='120'>".$text_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_fr=$_SESSION['new_record']['text_fr'];
                            else $text_fr=$row['text_fr'];
                            print "\t<textarea name='text_fr' id='text_fr' rows='25' cols='120'>".$text_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_it=$_SESSION['new_record']['text_it'];
                            else $text_it=$row['text_it'];
                            print "\t<textarea name='text_it' id='text_it' rows='25' cols='120'>".$text_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_zh=$_SESSION['new_record']['text_zh'];
                            else $text_zh=$row['text_zh'];
                            print "\t<textarea name='text_zh' id='text_zh' rows='25' cols='120'>".$text_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print"\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th style='text-align:left;'>Admin notes</th>\n";
                if( $_GET['error'] ) $notes_admin=$_SESSION['new_record']['notes_admin'];
                else $notes_admin=$row['notes_admin'];
                print "\t<td><textarea rows='6' cols='120' name='notes_admin'>".$notes_admin."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable); 
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "<a name='delete' href='#delete' onclick=\"delete_confirm2('/admin/biography/edit/del.php?biographyid=".$row['biographyid']."','".addslashes($row['title_en'])."','');\" title='delete this biography'><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/biography/edit/del.php?biographyid=".$row['biographyid']."";
                        $options_delete['text1']="Biography - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";


        print "\t</table>\n";
        print "\t<input type='hidden' name='biographyid' value='".$row['biographyid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";


    # printing out relations    
    if( $count>0 ) admin_html::show_relations($db,20,$row['biographyid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
