<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) ) $_SESSION['new_record']['error']['title_en']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/biography/edit/?error=true");

$db=new dbCLASS;



function replace_html_things($text,$options=array())
{
    $text=str_replace("&lsquo;","&#39;",$text);
    $text=str_replace("&rsquo;","&#39;",$text);
    if( $options['lang']!="de" )  $text=str_replace("&ldquo;","&quot;",$text);
    $text=str_replace("&rdquo;","&quot;",$text);
    if( $options['lang']!="de" ) $text=str_replace("&bdquo;","&quot;",$text);
    $text=str_replace("&OElig;","&#338;",$text);
    $text=str_replace("&oelig;","&#339;",$text);

    return $text;
}


$_POST['text_en']=replace_html_things($_POST['text_en']);
$options_replace_html=array();
$options_replace_html['lang']="de";
$_POST['text_de']=replace_html_things($_POST['text_de'],$options_replace_html);
$_POST['text_fr']=replace_html_things($_POST['text_fr']);
$_POST['text_it']=replace_html_things($_POST['text_it']);
$_POST['text_zh']=replace_html_things($_POST['text_zh']);

$text_en=$db->db_prepare_input($_POST['text_en'],1);
$text_de=$db->db_prepare_input($_POST['text_de'],1);
$text_fr=$db->db_prepare_input($_POST['text_fr'],1);
$text_it=$db->db_prepare_input($_POST['text_it'],1);
$text_zh=$db->db_prepare_input($_POST['text_zh'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST);

print $text_de;

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['title_de'];
$title_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_fr'];
$title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_it'];
$title_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_de'];
$text_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_fr'];
$text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_it'];
$text_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_BIOGRAPHY,"");
    $db->query("INSERT INTO ".TABLE_BIOGRAPHY."(sort,date_created) VALUES('".$maxsort."',NOW())");  
    $biographyid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $biographyid=$_POST['biographyid'];
} 
else UTILS::redirect("/");

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_BIOGRAPHY,$_POST['title_en'],"biographyid",$biographyid,"titleurl_en","en");


//print $titleurl_fr;
//exit;
$query="UPDATE ".TABLE_BIOGRAPHY." SET 
            title_en=\"".$_POST['title_en']."\",
            title_left_en=\"".$_POST['title_left_en']."\",
            titleurl=\"".$titleurl_en."\",
            title_de=\"".$_POST['title_de']."\",
            title_left_de=\"".$_POST['title_left_de']."\",
            title_fr=\"".$_POST['title_fr']."\",
            title_left_fr=\"".$_POST['title_left_fr']."\",
            title_it=\"".$_POST['title_it']."\",
            title_left_it=\"".$_POST['title_left_it']."\",
            title_zh=\"".$_POST['title_zh']."\",
            title_left_zh=\"".$_POST['title_left_zh']."\",
            title_search_1='".$title_search_de['text_lower_converted']."',
            title_search_2='".$title_search_de['text_lower_german_converted']."',
            title_search_3='".$title_search_fr['text_lower_converted']."',
            title_search_4='".$title_search_it['text_lower_converted']."',
            text_en='".$text_en."',
            text_de='".$text_de."',
            text_fr='".$text_fr."',
            text_it='".$text_it."',
            text_zh='".$text_zh."',
            text_search_1='".$text_search_de['text_lower_converted']."',
            text_search_2='".$text_search_de['text_lower_german_converted']."',
            text_search_3='".$text_search_fr['text_lower_converted']."',
            text_search_4='".$text_search_it['text_lower_converted']."',
            notes_admin='".$_POST['notes_admin']."',
            enable='".$_POST['enable']."' ";
if( $_POST['submit']=="update" ) $query.=" ,date_modified=NOW() ";
$query.=" WHERE biographyid='".$biographyid."' ";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,33,$biographyid);
    }
    elseif( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,33,$biographyid);
    }

    # RELATION adding
    admin_utils::add_relation($db,20,$biographyid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}
else
{
    admin_utils::admin_log($db,"",33,$_POST['biographyid'],2);
}


$url="/admin/biography/edit/?biographyid=".$biographyid."&relation_typeid=".$_POST['relation_typeid']."&p=".$_POST['p']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
