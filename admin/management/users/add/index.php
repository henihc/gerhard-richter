<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

$html->menu_admin_selected="management";
$html->menu_admin_sub_selected="add_users";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

?>

<h1>Add User</h1><br />
<form action="add.php" method="post" enctype="multipart/form-data">
<table class='admin-table'>
<tr><td>fname</td><td><input type='text' name='fname' value='' /></td></tr>
<tr><td>lname</td><td><input type='text' name='lname' value='' /></td></tr>
<tr><td>login</td><td><input type='text' name='login' value='' /></td></tr>
<tr><td>password</td><td><input type='password' name='password' autocomplete='off' value='' /></td></tr>
<tr><td>email</td><td><input type='text' name='email' value='' /></td></tr>

<tr><td>Auth</td><td>
<select name='auth'>


<option value='1' >Admin(read & write)</option>
<option value='0' >User(read only)</option>
</select>
</td></tr>
<tr><td>
<input type='reset' />
</td><td>
<input type='submit' name='add_user' value='Add User' /></td></tr>
</table>
</form>



<?php
        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>