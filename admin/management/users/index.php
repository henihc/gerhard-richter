<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

$html->menu_admin_selected="management";
$html->menu_admin_sub_selected="users";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


if(empty($var_array[0])||empty($var_array[1])){$var_array[0]="userID";$var_array[1]="asc";} 

echo "<table class='admin-table'><tr>";
//1
if($var_array[0]=='userID'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?userID&$sort' title='Order by user ID'><u>userID</u></a></strong></th>";

//2
if($var_array[0]=='fname'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?fname&$sort' title='Order by fname'><u>fname</u></a></strong></th>";

//3
if($var_array[0]=='lname'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?lname&$sort' title='Order by lname'><u>lname</u></a></strong></th>";
//4
if($var_array[0]=='login'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?login&$sort' title='Order by login'><u>login</u></a></strong></th>";
//5
if($var_array[0]=='email'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?email&$sort' title='Order by email'><u>email</u></a></strong></th>";
//5
if($var_array[0]=='auth'){
if($var_array[1]=='asc')$sort='desc';
else $sort='asc';
}
else $sort='asc';
//echo "<th><strong><a href='?auth&$sort' title='Order by auth'><u>auth</u></a></strong></th>";
//echo "<th><strong>Logs</strong></th>";

echo "</tr>\n";



$query=$db->query("SELECT * from ".TABLE_ADMIN." order by ".$var_array[0]." ".$var_array[1]);
while($row=$db->mysql_array($query)){
echo "
<tr>
<td><a href='/admin/management/users/edit/?".$row['userID']."' style='text-decoration:underline;color:blue;' title='edit this user'>".$row['userID']."</a></td>";
echo "<td>".$row['fname']."</td>
<td>".$row['lname']."</td>
<td>".$row['login']."</td>";



echo "<td>".$row['email']."</td>";
//echo "<td>".$row['auth']."</td>";

//echo "<td><a style='text-decoration:underline' href='/admin/logs/?userID=".$row['userID']."'>Logs</a></td>";

echo "</tr>\n";



}
echo "</table>\n";




        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>