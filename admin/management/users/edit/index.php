<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

$html->menu_admin_selected="management";
$html->menu_admin_sub_selected="add_users";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

$var_array=UTILS::url_var($db,$_SERVER["QUERY_STRING"]);

$query=$db->query("SELECT * FROM ".TABLE_ADMIN." WHERE userID='".$var_array[0]."'");
$row=$db->mysql_array($query);

?>

<h1>Edit User</h1><br />
<form action="edit.php?<?php print $var_array[0]; ?>" method="post" enctype="multipart/form-data">
<table class='admin-table'>


<?php
if($var_array[0]=="error") print "<tr><td colspan='2' style='color:red'>*Please complete mandatory fields</td></tr>";
if($var_array[1]=="update") print "<tr><td colspan='2' style='color:green'>Update successful!</td></tr>";
if($var_array[1]=="insert") print "<tr><td colspan='2' style='color:green'>Insert successful!</td></tr>";
?>

<tr><td>fname</td><td><input type='text' name='fname' value='<?php print $row['fname']; ?>' /></td></tr>
<tr><td>lname</td><td><input type='text' name='lname' value='<?php print $row['lname']; ?>' /></td></tr>
<tr><td>login</td><td><input type='text' name='login' value='<?php print $row['login']; ?>' /></td></tr>
<tr><td>password</td><td><input type='password' name='password' value='' autocomplete='off' /></td></tr>
<tr><td>email</td><td><input type='text' name='email' value='<?php print $row['email']; ?>' /></td></tr>

<tr><td>Auth</td><td>
<select name='auth'>

<?php
if($row['auth']==0) $selected2="selected='selected'";
else $selected1="selected='selected'";
?>
<option value='1' <?php print $selected1; ?> >Admin(read & write)</option>
<option value='0' <?php print $selected2; ?> >User(read only)</option>
</select>
</td></tr>
<tr><td>
<input type='reset' />
</td><td>
<input type='submit' name='update_user' value='Update User' /></td></tr>
</table>
</form>



<?php
        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>