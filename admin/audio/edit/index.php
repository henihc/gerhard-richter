<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config_basic.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="audios";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$query_where_audio=" WHERE audioid='".$_GET['audioid']."' ";
$query_audio=QUERIES::query_audios($db,$query_where_audio,$query_order,$query_limit);
$results=$db->query($query_audio['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace('descriptionen');\n";
        print "\tCKEDITOR.replace('descriptionde');\n";
        print "\tCKEDITOR.replace('descriptionfr');\n";
        print "\tCKEDITOR.replace('descriptionit');\n";
        print "\tCKEDITOR.replace('descriptionzh');\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-track li a','#div-admin-tabs-info-track .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-artwork li a','#div-admin-tabs-info-artwork .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc li a','#div-admin-tabs-info-desc .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td>";
                    if( $row['enable'] || $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input name='enable' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['tracktitle'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Track title</td>\n";
                print "\t<td>\n";

                    print "\t<ul id='ul-admin-tabs-track' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-track' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $tracktitleen=$_SESSION['new_record']['tracktitleen'];
                            else $tracktitleen=$row['tracktitleen'];
                            print "\t<textarea name='tracktitleen' cols='29' rows='3'>".$tracktitleen."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $tracktitlede=$_SESSION['new_record']['tracktitlede'];
                            else $tracktitlede=$row['tracktitlede'];
                            print "\t<textarea name='tracktitlede' cols='29' rows='3'>".$tracktitlede."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $tracktitlefr=$_SESSION['new_record']['tracktitlefr'];
                            else $tracktitlefr=$row['tracktitlefr'];
                            print "\t<textarea name='tracktitlefr' cols='29' rows='3'>".$tracktitlefr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $tracktitleit=$_SESSION['new_record']['tracktitleit'];
                            else $tracktitleit=$row['tracktitleit'];
                            print "\t<textarea name='tracktitleit' cols='29' rows='3'>".$tracktitleit."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $tracktitlezh=$_SESSION['new_record']['tracktitlezh'];
                            else $tracktitlezh=$row['tracktitlezh'];
                            print "\t<textarea name='tracktitlezh' cols='29' rows='3'>".$tracktitlezh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</td>\n";
                print "\t<td>\n";

                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleen=$_SESSION['new_record']['titleen'];
                            else $titleen=$row['titleen'];
                            print "\t<textarea name='titleen' cols='29' rows='3'>".$titleen."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titlede=$_SESSION['new_record']['titlede'];
                            else $titlede=$row['titlede'];
                            print "\t<textarea name='titlede' cols='29' rows='3'>".$titlede."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titlefr=$_SESSION['new_record']['titlefr'];
                            else $titlefr=$row['titlefr'];
                            print "\t<textarea name='titlefr' cols='29' rows='3'>".$titlefr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleit=$_SESSION['new_record']['titleit'];
                            else $titleit=$row['titleit'];
                            print "\t<textarea name='titleit' cols='29' rows='3'>".$titleit."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titlezh=$_SESSION['new_record']['titlezh'];
                            else $titlezh=$row['titlezh'];
                            print "\t<textarea name='titlezh' cols='29' rows='3'>".$titlezh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $year=$_SESSION['new_record']['year'];
                    else $year=$row['year'];
                    admin_html::get_years("",$year,"");
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Height x Width</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $height=$_SESSION['new_record']['height'];
                    else $height=$row['height'];
                    print "\t<input name='height' type='text' value='".$height."' />";
                    if( $_GET['error'] ) $width=$_SESSION['new_record']['width'];
                    else $width=$row['width'];
                    print "\t x <input name='width' type='text' value='".$width."' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Artwork</td>\n";
                print "\t<td>\n";

                    print "\t<ul id='ul-admin-tabs-artwork' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-artwork' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artworken=$_SESSION['new_record']['artworken'];
                            else $artworken=$row['artworken'];
                            print "\t<textarea name='artworken' cols='29' rows='3'>".$artworken."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artworkde=$_SESSION['new_record']['artworkde'];
                            else $artworkde=$row['artworkde'];
                            print "\t<textarea name='artworkde' cols='29' rows='3'>".$artworkde."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artworkfr=$_SESSION['new_record']['artworkfr'];
                            else $artworkfr=$row['artworkfr'];
                            print "\t<textarea name='artworkfr' cols='29' rows='3'>".$artworkfr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artworkit=$_SESSION['new_record']['artworkit'];
                            else $artworkit=$row['artworkit'];
                            print "\t<textarea name='artworkit' cols='29' rows='3'>".$artworkit."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $artworkzh=$_SESSION['new_record']['artworkzh'];
                            else $artworkzh=$row['artworkzh'];
                            print "\t<textarea name='artworkzh' cols='29' rows='3'>".$artworkzh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Number</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $number=$_SESSION['new_record']['number'];
                    else $number=$row['number'];
                    print "\t<input name='number' type='text' value='".$number."' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</th>\n";
                print "\t<td>";

                    print "\t<ul id='ul-admin-tabs-desc' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $descriptionen=$_SESSION['new_record']['descriptionen'];
                            else $descriptionen=$row['descriptionen'];
                            print "\t<textarea name='descriptionen' cols='90' rows='3'>".$descriptionen."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $descriptionde=$_SESSION['new_record']['descriptionde'];
                            else $descriptionde=$row['descriptionde'];
                            print "\t<textarea name='descriptionde' cols='90' rows='3'>".$descriptionde."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $descriptionfr=$_SESSION['new_record']['descriptionfr'];
                            else $descriptionfr=$row['descriptionfr'];
                            print "\t<textarea name='descriptionfr' cols='90' rows='3'>".$descriptionfr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $descriptionit=$_SESSION['new_record']['descriptionit'];
                            else $descriptionit=$row['descriptionit'];
                            print "\t<textarea name='descriptionit' cols='90' rows='3'>".$descriptionit."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $descriptionzh=$_SESSION['new_record']['descriptionzh'];
                            else $descriptionzh=$row['descriptionzh'];
                            print "\t<textarea name='descriptionzh' cols='90' rows='3'>".$descriptionzh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=11 AND itemid2='".$row['audioid']."' ) OR ( typeid2=17 AND typeid1=11 AND itemid1='".$row['audioid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_s__imageid_".$imageid.".jpg";
                    $link="/images/size_o__imageid_".$imageid.".jpg";
                    if( !empty($imageid) ) $image="<img src='".$src."' alt='' />";
                    else $image="Image";
                    print $image;
                print "</th>\n";
                print "\t<td>";
                    print "\t<input name='image' type='file' value='' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    print "Mp3 file";
                print "</th>\n";
                print "\t<td>";
                    print "\t<input name='mp3' type='file' value='' />";
                    $mp3_url=DATA_PATH."/audio/mp3/".$row['audioid'].".mp3";
                    if( file_exists($mp3_url) )
                    {   
                        print "\t<a href='/datadir/audio/mp3/".$row['audioid'].".mp3' title=''><u>".$row['audioid'].".mp3</u></a>\n";
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    print "Ogg file";
                print "</th>\n";
                print "\t<td>";
                    print "\t<input name='ogg' type='file' value='' />";
                    $ogg_url=DATA_PATH."/audio/ogg/".$row['audioid'].".ogg";
                    if( file_exists($ogg_url) )
                    {   
                        print "\t<a href='/datadir/audio/ogg/".$row['audioid'].".ogg' title=''><u>".$row['audioid'].".ogg</u></a>\n";
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( $count )
                    {
                        //$mandatory="<a href='#' title='' onclick=\"delete_confirm2('/admin/audio/edit/del.php?audioid=".$row['audioid']."','".addslashes($row['title_en'])."','')\" /><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/audio/edit/del.php?audioid=".$row['audioid']."";
                        $options_delete['text1']="Audio - ".$row['title_en'];
                        //$options_delete['text2']="";
                        $options_delete['html_return']=1;
                        $mandatory=admin_html::delete_button($db,$options_delete);
                    }
                    else $mandatory="* Mandatory";
                    print $mandatory;
                print "</th>\n";
                print "\t<td>";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input name='submit' type='submit' value='".$value."' />";
                print "</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='audioid' value='".$row['audioid']."' />\n";
    print "\t</form>\n";

    # printing out relations
    admin_html::show_relations($db,11,$row['audioid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
