<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['tracktitleen']) && empty($_POST['tracktitlede']) ) $_SESSION['new_record']['error']['tracktitle']=true;
if( empty($_POST['titleen']) && empty($_POST['titlede']) ) $_SESSION['new_record']['error']['title']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/audio/edit/?audioid=".$_POST['audioid']."&error=true");

$db=new dbCLASS;


$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_AUDIO."(date) VALUES(NOW())");  
    $audioid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $audioid=$_POST['audioid'];
} 
else UTILS::redirect("/");

$query="UPDATE ".TABLE_AUDIO." SET
            tracktitleen='".$_POST['tracktitleen']."', 
            tracktitlede='".$_POST['tracktitlede']."', 
            tracktitlefr='".$_POST['tracktitlefr']."', 
            tracktitleit='".$_POST['tracktitleit']."', 
            tracktitlezh='".$_POST['tracktitlezh']."', 
            titleen='".$_POST['titleen']."', 
            titlede='".$_POST['titlede']."', 
            titlefr='".$_POST['titlefr']."', 
            titleit='".$_POST['titleit']."', 
            titlezh='".$_POST['titlezh']."', 
            year='".$_POST['year']."', 
            width='".$_POST['width']."', 
            height='".$_POST['height']."', 
            artworken='".$_POST['artworken']."', 
            artworkde='".$_POST['artworkde']."', 
            artworkfr='".$_POST['artworkfr']."', 
            artworkit='".$_POST['artworkit']."', 
            artworkzh='".$_POST['artworkzh']."', 
            number='".$_POST['number']."', 
            descriptionen='".$_POST['descriptionen']."', 
            descriptionde='".$_POST['descriptionde']."', 
            descriptionfr='".$_POST['descriptionfr']."', 
            descriptionit='".$_POST['descriptionit']."', 
            descriptionzh='".$_POST['descriptionzh']."', 
            enable='".$_POST['enable']."'
        WHERE audioid='".$audioid."' ";

if( $_POST['submit']=="add" )
{
    $db->query($query);

    admin_utils::admin_log($db,1,28,$audioid);
}
elseif( $_POST['submit']=="update" )
{
    $db->query($query);

    admin_utils::admin_log($db,2,28,$audioid);
}
else
{
    admin_utils::admin_log($db,"",28,$_POST['audioid'],2);
}

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    # RELATION adding
    admin_utils::add_relation($db,11,$audioid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}
 
### image file upload
if( $_FILES['image']['error']==0 )
{

    $fileName = str_replace (" ", "_", $_FILES['image']['name']);
    $tmpName  = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];

    if ( is_uploaded_file($tmpName) )
    {
        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

        $getimagesize = getimagesize($tmpName);

        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }   

        $dir=DATA_PATH."/images_new";
        $new_file_name=$imageid.".jpg";
        $new_file_path=$dir."/original/".$imageid.$ext;

        UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,11,$audioid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
                //$src_cache_uri=$imageid.".jpg";
                //UTILS::cache_uri($src,$src_cache_uri,1);
                #end
            }


     }

}
# image file upload end

### mp3 file upload
if( $_FILES['mp3']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['mp3']['name']);
    $tmpName  = $_FILES['mp3']['tmp_name'];
    $fileSize = $_FILES['mp3']['size'];
    $fileType = $_FILES['mp3']['type'];

    if ( is_uploaded_file($tmpName) )
    {
        $getimagesize = getimagesize($tmpName);

        switch( $_FILES['mp3']['type'] )
        {
            case 'audio/mpeg'  : $ext = ".mp3"; break;
            case 'audio/mpg'  : $ext = ".mp3"; break;
            default : exit("Unsupported image format! Supported formats: MPEG,MPG");
        }   

        $dir=DATA_PATH."/audio/mp3";
        $new_file_path=$dir."/".$audioid.$ext;
        $newfileName_original=$audioid.$ext;
             
        # copy original
        if (!copy($tmpName, $new_file_path))
        {
            print "Error Uploading File.";
            //exit();
        }
        else
        {
            $db->query("UPDATE ".TABLE_AUDIO." SET src_audio='".$newfileName_original."' WHERE audioid='".$audioid."'");
        }
     }
}
# mp3 file upload end

### ogg file upload
if( $_FILES['ogg']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['ogg']['name']);
    $tmpName  = $_FILES['ogg']['tmp_name'];
    $fileSize = $_FILES['ogg']['size'];
    $fileType = $_FILES['ogg']['type'];

    if ( is_uploaded_file($tmpName) )
    {
        $getimagesize = getimagesize($tmpName);

        switch( $_FILES['ogg']['type'] )
        {
            case 'video/ogg'  : $ext = ".ogg"; break;
            case 'audio/ogg'  : $ext = ".ogg"; break;
            case 'application/ogg'  : $ext = ".ogg"; break;
            default : exit("Unsupported image format! Supported formats: OggS");
        }   

        $dir=DATA_PATH."/audio/ogg";
        $new_file_path=$dir."/".$audioid.$ext;
        $newfileName_original=$audioid.$ext;
             
        # copy original
        if (!copy($tmpName, $new_file_path))
        {
            print "Error Uploading File.";
            //exit();
        }
        else
        {
            $db->query("UPDATE ".TABLE_AUDIO." SET src_audio='".$newfileName_original."' WHERE audioid='".$audioid."'");
        }
     }
}
# ogg file upload end


unset($_SESSION['new_record']);

$url="/admin/audio/edit/?audioid=".$audioid."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
