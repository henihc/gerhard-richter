<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="audios";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY sort ASC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_audios=QUERIES::query_audios($db,$query_where_audios,$query_order,$query_limit);

    $results=$db->query($query_audios['query_without_limit']);
    $count=$db->numrows($results);
    $results_audios=$db->query($query_audios['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {   
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) )
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        //admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        $ii=1;
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>audioid</th>\n";
                print "\t<th>image</th>\n";
                print "\t<th>track title EN</th>\n";
                print "\t<th>date created</th>\n";
            print "\t</tr>\n";
        print "\t</table>\n";

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {
                print "\t<li id='item_".$row['audioid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">".$row['audioid']."</td>\n";
                            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=11 AND itemid2='".$row['audioid']."' ) OR ( typeid2=17 AND typeid1=11 AND itemid1='".$row['audioid']."' ) ");
                            $row_related_image=$db->mysql_array($results_related_image);
                            $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                            $src="/images/size_s__imageid_".$imageid.".jpg";
                            $link="/images/size_o__imageid_".$imageid.".jpg";
                            print "\t<td><img src='".$src."' alt='' /></td>\n";
                            print "\t<td>";
                                print "<a name='".$row['audioid']."' href='/admin/audio/edit/?audioid=".$row['audioid']."' title='edit audio' class='link_edit'>".$row['tracktitleen']."</a>";
                            print "\t</td>";
                            print "\t<td>".$row['date_created_list_admin']."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }

        //admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No audio found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
