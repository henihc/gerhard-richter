/* <![CDATA[ */
document.addEvent('domready', function() 
{
    var inputWord2 = $('itemid');

    // An element as indicator, shown during background request
    var indicator = $('loading');
    $('loading').setStyle('display', 'none');

    new Autocompleter.Request.JSON(inputWord2, '/admin/relations/edit/search.php',
    {
        'indicator': indicator,
        'multiple': true,
        'selectFirst': true,
        'selectMode': false,
        'minLength': 2,
        'postData': {'relation_typeid': $('relation_typeid').options[$('relation_typeid').selectedIndex].value}
    });

});
/* ]]> */
