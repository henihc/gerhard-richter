function delete_confirm(element,url){
	var answer = confirm("Do you really want to delete?")
	if (answer){
	  if(element!='')
	  {
	  document.getElementById(element).action=url;
	  document.getElementById(element).submit();
	  }
	  else window.location=url;
	}
}

var next_previous_valid=1;

//function delete_confirm2(url,title_text="",title_text2="")
function delete_confirm2(url,title_text,title_text2)
{
    var text_alert="";
    if( title_text2=="" )
    {
        text_alert=text_alert+"Are you sure you wish to delete?";
        if( title_text!="" ) text_alert=text_alert+"\n\n"+title_text;
    }
    else if( title_text2 ) 
    {
        text_alert=title_text2;
    }
    else text_alert="Are you sure you wish to delete?";
    var agree=confirm(text_alert);
    if (agree) window.location=url;
    else return false ;
}

var counter=0;

  function AddInput(div_elm,div_ins,element)
  {
    counter++;
    var newAttachment=document.getElementById(div_elm).cloneNode(true);
    newAttachment.id="";
    newAttachment.style.display="block";
    var newField = newAttachment.childNodes;
    for (var i=0; i<newField.length; i++)
    {
      var theName=newField[i].name;
      var theId=newField[i].id;
      newField[i].value="";
      if (theName) newField[i].name = theName+counter;
      if (theId) newField[i].id = theId+counter;
    }
		var newDiv=document.createElement("div");
		newDiv.appendChild(newAttachment);
		var refE = document.getElementById(div_ins);
		var parE = refE.parentNode;
    parE.insertBefore(newDiv,refE);
  }


var clone_number = 1;
function add_item_clone(clone_item,insert_item) 
{ 
    if(clone_number > 9)
    {
        alert('Sorry you can only add 10 items');
        exit(0);
    }
    else
    {
        var new_clone=document.getElementById(clone_item).cloneNode(true);
        var new_insert=document.getElementById(insert_item);
        new_clone.id=clone_item+clone_number;
        var new_field = new_clone.childNodes;
        for (var i=0; i<new_field.length; i++)
        {
            var the_id=new_field[i].id;
            var the_name=new_field[i].name;
            if (the_id) 
            {
                var new_id=the_id+clone_number;
                new_field[i].id = new_id;
                new_field[i].setAttribute("onclick", "remove_item_clone('"+clone_item+clone_number+"','"+insert_item+"');");
                
            }
            if (the_name && the_name=="number_list[]")
            {
                new_field[i].value="";
            } 
        }
        new_insert.appendChild(new_clone);
    }
    clone_number++; 
}
 
function remove_item_clone(remove_item,insert_item) 
{ 
    var elm = document.getElementById(remove_item); 
    document.getElementById(insert_item).removeChild(elm); 
    clone_number = clone_number - 1; // decrement the max file upload counter if the file is removed 
} 


//changing selected claas for pre defined search at literature section
  function changeClass(element)
  {
    document.getElementById("search_all").className='first';
    document.getElementById("search_2010").className='';
    document.getElementById("search_2000").className='';
    document.getElementById("search_1990").className='';
    document.getElementById("search_1980").className='';
    document.getElementById("search_1970").className='';
    document.getElementById("search_1960").className='';
    if( element!='' ) element.className='selected first';
  }

    function search_lit_change_years(year_from,year_to)
    {
        var navRoot = document.getElementById("year-from");
        for (i=0; i<navRoot.length; i++) 
        {
            if( navRoot.options[i].value==year_from )
            {
                navRoot.options[i].selected=true;
            }
        }
        var navRoot = document.getElementById("year-to");
        for (i=0; i<navRoot.length; i++) 
        {
            if( navRoot.options[i].value==year_to )
            {
                navRoot.options[i].selected=true;
            }
        }

    }

//Function to disable specified form field
  function disable(field)
  {
  element=document.getElementById(field);
  if(element.disabled==false)element.disabled=true;
  else element.disabled=false;
  }

   function changeClassToggle(ele, cls)
    {   
      if(!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)')))
      {   
        ele.className += " " + cls;
      }   
      else
      {   
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
      }   
    }   


//Function to reset search from fields
  //function reset_serch(from,to,number_value,day_value,month_value,year_value)
  function reset_serch(number_value)
  {
	document.getElementById('artworkID1').options[0].selected=true;
	document.getElementById('title').value='';
	document.getElementById('number').innerHTML=number_value;
	document.getElementById('number1').value='';
	document.getElementById('museum').value='';
	document.getElementById('year-from').options[0].selected=true;
	document.getElementById('year-to').options[0].selected=true;
	document.getElementById('size-height-min').value='';
	document.getElementById('size-height-max').value='';
	document.getElementById('size-width-min').value='';
	document.getElementById('size-width-max').value='';
    document.getElementById('input-number-from').value='';
    document.getElementById('input-number-to').value='';
    document.getElementById('div-right-search-date').style.display='none';
    document.getElementById('div-right-search-year').style.display='';
    document.getElementById('date-day-from').value='';
    document.getElementById('date-month-from').value='';
    document.getElementById('date-year-from').value='';
    document.getElementById('date-day-to').value='';
    document.getElementById('date-month-to').value='';
    document.getElementById('date-year-to').value='';
	document.getElementById('colorid').options[0].selected=true;
    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++)
    {
        if( inputs[i].name=="number_list[]" ) inputs[i].value='';
        var elm=document.getElementById('div-number-list'+i);
        if( elm ) document.getElementById('div-number-lists').removeChild(elm);
    }

        changeClassToggle(document.getElementById("div-number-quick-search"), "hide");
        changeClassToggle(document.getElementById("div-number-advanced-search"), "hide");

  }

//Function to reset search EXH from fields
  function reset_serch_exh(title_text,location_text,keyword_text)
  {
	document.getElementById('title_search').value="";
	document.getElementById('location').value="";
	document.getElementById('keyword').value="";
	document.getElementById('year-from').options[0].selected=true;
	document.getElementById('year-to').options[0].selected=true;
  }

  function reset_search_quotes(to)
  {
	document.getElementById('keyword').value="";
	document.getElementById('year-from').options[0].selected=true;
	document.getElementById('year-to').options[0].selected=true;
  }

  function reset_search_videos(keyword)
  {
	document.getElementById('keyword').value="";
  }

  function reset_serch_lit(from,to,keyword)
  {
	document.getElementById('author_book').value="";
	document.getElementById('title_book').value="";
	document.getElementById('keyword_book').value="";
	document.getElementById('books_catid').options[0].selected=true;
	document.getElementById('languageid').options[0].selected=true;
	document.getElementById('year-from').options[0].selected=true;
	document.getElementById('year-to').options[0].selected=true;
  }

  function reset_serch_lit_new()
  {
    document.getElementById('author_book').value="";
    document.getElementById('title_book').value="";
    document.getElementById('keyword_book').value="";
    document.getElementById('books_catid').options[0].selected=true;
    document.getElementById('languageid').options[0].selected=true;
    //mysel = document.getElementById('languageid');
    //mysel.selectedIndex = 0;
    //console.debug(mysel.value);
    //console.debug($('#languageid'));
    document.getElementById('year-from').options[0].selected=true;
    document.getElementById('year-to').options[0].selected=true;

      //Executes your code when the DOM is ready.  Acts the same as $(document).ready().
      $(function() {
        // Note: This code assumes you have already called the selectBoxIt() method somewhere else in your code
        // Retrieves all of the SelectBoxIt methods
        var selectBox = $("select#books_catid").data("selectBox-selectBoxIt");
        selectBox.refresh(0);
        var selectBox = $("select#languageid").data("selectBox-selectBoxIt");
        selectBox.refresh(0);
        var selectBox = $("select#year-from").data("selectBox-selectBoxIt");
        selectBox.refresh(0);
        var selectBox = $("select#year-to").data("selectBox-selectBoxIt");
        selectBox.refresh(0);
      });

  }

  function reset_serch_articles(from,to,keyword)
  {
	document.getElementById('author_article').value="";
	document.getElementById('title_article').value="";
	document.getElementById('keyword_article').value="";
	document.getElementById('languageid').options[0].selected=true;
	document.getElementById('articles_catid').options[0].selected=true;
	document.getElementById('year-from').options[0].selected=true;
	document.getElementById('year-to').options[0].selected=true;
  }

function clear_text_field(e,text) {
    if( e.value==text) e.value="";
    else if( e.value.length==0 ) e.value=text;
}

                    function popup(url,width,height,scrollbars) 
                    {
                        var left   = (screen.width  - width)/2;
                        var top    = (screen.height - height)/2;
                        var params = 'width='+width+', height='+height;
                        params += ', top='+top+', left='+left;
                        params += ', directories=no';
                        params += ', location=no';
                        params += ', menubar=no';
                        params += ', resizable=no';
                        params += ', scrollbars='+scrollbars+'';
                        params += ', status=no';
                        params += ', toolbar=no';
                        newwin=window.open(url,'windowname5', params);
                        if (window.focus) {newwin.focus()}
                        return false;
                    }



function witch_key_pressed(evt,e)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if( charCode==13 ) 
    {
        AJAX('search.php?orderby=year&orderhow=DESC&page=1&limit=0&per_page='+document.getElementById('per_page').options[document.getElementById('per_page').selectedIndex].value+'&author='+document.getElementById('author_book').value+'&title='+document.getElementById('title_book').value+'&books_catid='+document.getElementById('books_catid').options[document.getElementById('books_catid').selectedIndex].value+'&year-from='+document.getElementById('year-from').options[document.getElementById('year-from').selectedIndex].value+'&year-to='+document.getElementById('year-to').options[document.getElementById('year-to').selectedIndex].value+'&test=0');changeClass(document.getElementById('search_all'));
    }
}


function change_class(element,class_name,link,link_more,link_close)
{
    if( document.getElementById(element).className=='p-category-desc div_text_wysiwyg p-opened' ) 
    {
        document.getElementById(element).className='p-category-desc';
        link.innerHTML=link_more;
    }
    else 
    {
        document.getElementById(element).className=class_name;
        link.innerHTML=link_close;
    }
}

function change_class2(element,class_name,link,link_more,link_close)
{
    if( document.getElementById(element).className=='margin div-brief-desc p-opened' ) 
    {
        document.getElementById(element).className='div-brief-desc';
        link.innerHTML=link_more;
        link.className = "a-read-more-opp-form"
    }
    else 
    {
        document.getElementById(element).className=class_name;
        link.innerHTML=link_close;
        link.className = link.className + " a-read-more-close"
    }
}

function validate_opp_form(text,text2,jquery)
{
    var valid_color="#fff";
    var error_color="red";

    if( jquery('#email') )
    {   
        var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var item = jquery('#email');
        var item_text = jquery('#email_text');
        var error1 = false;
        if(!filter.test(item.val()))
        {   
            error1 = true;
            item_text.css('color',error_color);
        }   
        else
        {   
            error1 = false;
            item_text.css('color',valid_color);
        }   
    }   

    if( jquery('#first_name') )
    {   
    var item = jquery('#first_name');
    var item_text = jquery('#first_name_text');
    var error2 = false;
        if( item.val().length<=0 )
        {
            error2 = true;
            item_text.css('color',error_color);
        }
        else
        {
            error2 = false;
            item_text.css('color',valid_color);
        }
    }

    if( jquery('#last_name') )
    {   
    var item = jquery('#last_name');
    var item_text = jquery('#last_name_text');
    var error3 = false;
        if( item.val().length<=0 )
        {
            error3 = true;
            item_text.css('color',error_color);
        }
        else
        {
            error3 = false;
            item_text.css('color',valid_color);
        }
    }

    if( jquery('#codenumb') )
    {   
    var item = jquery('#codenumb');
    var item_text = jquery('#codenumb_error');
    var error4 = false;
        if( item.val().length<=0 )
        {
            error4 = true;
            item_text.css('color',error_color);
        }
        else
        {
            error4 = false;
            item_text.css('color',valid_color);
        }
    }

    if( jquery('#agree') )
    {   
        var item = jquery('#agree');
        var error5 = false;
        if( item.is(':checked')==false )
        {
            error5 = true;
            alert(text);
        }
        else
        {
            error5 = false;
        }
    }

    if( !error1 && !error2 && !error3 && !error4 && !error5 ) 
    {
        jquery('#submit').attr('type','submit');
    }
    else if( !error5 ) alert(text2);

}

function change_element_type(name,new_type) 
{
    var submit_btn = document.getElementById(name);
    submit_btn.type=new_type;
    submit_btn.setAttribute('onclick', 'validate_opp_form()'); 
}

function change_element_type2(e,new_type) 
{
    var submit_btn = e;
    submit_btn.type=new_type;
}

function clear_input(e,value) 
{
    if( e.value==value) e.value="";
    else if( e.value.length==0 ) e.value=value;
}

function enable_input(e,some_name) 
{
    var element_enable=document.getElementById(some_name);
    if( e.value=='' ) element_enable.disabled=true;
    else element_enable.disabled=false;
}

function on_search_select_change(e)
{
    if( e.options[e.selectedIndex].value==5 || e.options[e.selectedIndex].value==6 || e.options[e.selectedIndex].value==8 )
    {
        document.getElementById('div-right-search-year').style.display='none';
        document.getElementById('div-right-search-date').style.display='';
    }
    else
    {
        document.getElementById('div-right-search-year').style.display='';
        document.getElementById('div-right-search-date').style.display='none';
    }
}

function replace_placholder(e_name)
{
    var e=document.getElementById(e_name);
    var e_placeholder=e.getAttribute('placeholder');
    var e_value=e.getAttribute('value');
    //alert(e_value);
    //alert(e_placeholder);
    if(e_value==e_placeholder) e.value='';
    else e.value=e_placeholder;
}

  function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?")
    var query = search.replace(regex, "$1").replace(/&$/, '')
    return (query.length > 2 ? query + "&" : "?") + param + "=" + newval
 }
