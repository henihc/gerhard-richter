CKEDITOR.stylesSet.add( 'my_styles',
[
    // Block-level styles
    { name : 'Blue Title', element : 'h2', styles : { 'color' : 'Blue' } },
    { name : 'Red Title' , element : 'a', styles : { 'color' : 'Red' } },
 
    // Inline styles
    { name : 'CSS Style', element : 'span', attributes : { 'class' : 'my_style' } },
    { name : 'Marker: Yellow', element : 'span', styles : { 'background-color' : 'Yellow' } }
]);

        CKEDITOR.editorConfig = function( config )
        {   
            config.entities = false;
            config.htmlEncodeOutput = false;

            config.language = 'en';
            config.uiColor = '#AADC6E';
            config.width  = '773px' ;
            config.height = '150px' ;
            config.filebrowserBrowseUrl = '';
            config.filebrowserImageBrowseUrl = '';
            config.filebrowserUploadUrl = '/admin/inc/ckeditor/upload.php';
            config.filebrowserImageUploadUrl = '/admin/inc/ckeditor/upload.php?type=Images';
            //config.stylesSet = 'my_styles:/admin/css/ckeditor_styles.js';
            //config.stylesSet = [];
            //config.stylesSet = 'my_styles';
            config.extraPlugins = 'youtube';
            config.toolbar_Small =
            [   
                ['Source', 'Bold', 'Italic', 'NumberedList', 'BulletedList', 'Link', 'Unlink', 'Maximize', 'ShowBlocks']
            ];  
            config.toolbar_Large =
            [   

    { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','-','Templates' ] },
    //{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
    //{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'styles', items : [ 'Format','FontSize' ] },
    { name: 'insert', items : [ 'Image','Flash','Youtube','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    '/',
    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
    { name: 'colors', items : [ 'TextColor','BGColor' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','BidiLtr','BidiRtl' ] },
    { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }

            ];  
            
        }   
        CKEDITOR.on( 'instanceReady', function( ev )
        {
            ev.editor.dataProcessor.writer.indentationChars = '\t';
            ev.editor.dataProcessor.writer.selfClosingEnd = ' />';
            ev.editor.dataProcessor.writer.lineBreakChars = '\n';
        });

/*
            filebrowserBrowseUrl : '', 
            //filebrowserBrowseUrl : '/browser/browse.php',
            //filebrowserImageBrowseUrl : '/browser/browse.php?type=Images',
            filebrowserImageBrowseUrl : '', 
            filebrowserUploadUrl : '/uploader/upload.php',
            filebrowserImageUploadUrl : '/uploader/upload.php?type=Images',
            //skin : 'kama'
            skin : 'office2003'
*/
