        CKEDITOR.editorConfig = function( config )
        {   
            config.language = 'en';
            config.uiColor = '#AADC6E';
            config.toolbar = 'Full';
            config.width  = '773px' ;
            config.height = '200px' ;
            config.filebrowserBrowseUrl = '';
            config.filebrowserImageBrowseUrl = '';
            config.filebrowserUploadUrl = '/admin/inc/ckeditor/upload.php';
            config.filebrowserImageUploadUrl = '/admin/inc/ckeditor/upload.php?type=Images';
            config.extraPlugins = 'youtube';
        }   
        CKEDITOR.on( 'instanceReady', function( ev )
        {
            ev.editor.dataProcessor.writer.indentationChars = '\t';
            ev.editor.dataProcessor.writer.selfClosingEnd = ' />';
            ev.editor.dataProcessor.writer.lineBreakChars = '\n';
        });

/*
            filebrowserBrowseUrl : '', 
            //filebrowserBrowseUrl : '/browser/browse.php',
            //filebrowserImageBrowseUrl : '/browser/browse.php?type=Images',
            filebrowserImageBrowseUrl : '', 
            filebrowserUploadUrl : '/uploader/upload.php',
            filebrowserImageUploadUrl : '/uploader/upload.php?type=Images',
            //skin : 'kama'
            skin : 'office2003'
*/
