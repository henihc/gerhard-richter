        CKEDITOR.editorConfig = function( config )
        {   
            config.language = 'en';
            config.uiColor = '#AADC6E';
            //config.toolbar = 'Full';
            //config.toolbar = 'Basic';
            config.width  = '773px' ;
            config.height = '150px' ;
            config.filebrowserBrowseUrl = '';
            config.filebrowserImageBrowseUrl = '';
            config.filebrowserUploadUrl = '/admin/inc/ckeditor/upload.php';
            config.filebrowserImageUploadUrl = '/admin/inc/ckeditor/upload.php?type=Images';
            config.extraPlugins = 'youtube';
            config.toolbar_Basic =
            [   
                ['Source', 'Bold', 'Italic', 'NumberedList', 'BulletedList', 'Link', 'Unlink']
            ];  
            
        }   
        CKEDITOR.on( 'instanceReady', function( ev )
        {
            ev.editor.dataProcessor.writer.indentationChars = '\t';
            ev.editor.dataProcessor.writer.selfClosingEnd = ' />';
            ev.editor.dataProcessor.writer.lineBreakChars = '\n';
        });

/*
            filebrowserBrowseUrl : '', 
            //filebrowserBrowseUrl : '/browser/browse.php',
            //filebrowserImageBrowseUrl : '/browser/browse.php?type=Images',
            filebrowserImageBrowseUrl : '', 
            filebrowserUploadUrl : '/uploader/upload.php',
            filebrowserImageUploadUrl : '/uploader/upload.php?type=Images',
            //skin : 'kama'
            skin : 'office2003'
*/
