window.addEvent('domready', function() 
{
	var tabs = $$('.tabsadmin li.tab');
	var links = $$('.tabsadmin li a.name');
	var info = $$('.tabsadmin-info .holder');

	var clear = new Element('br').addClass('clear');
	
	if($('ul_link_tabs'))
    {
	    info.addClass('hide');
	    info[0].removeClass('hide');


        if( document.getElementById('this_type_selected') )
        {   
            var divs = document.getElementsByTagName('div');
            for (var i = 0; i < divs.length; i++) 
            {   
                var div = divs[i];
                if( div.className.indexOf('holder')==0 ) div.addClass('hide');
                document.getElementById('this_type_selected').removeClass('hide');
            }   
        }   
        else
        {   
            $$('.tabsadmin h4 a').removeClass('selected');
            $$('.tabsadmin li a')[0].addClass('selected');
        }   

	    $$('.tabsadmin-info').each(function(el) 
        {
		    el.inject('ul_link_tabs', 'after');
		});

	    tabs.setStyles('float: left; margin-right: 3px;');
	
	    $$('.tabsadmin li h4').setStyle('top', '0'); 
	
	    clear.inject('ul_link_tabs', 'after');
	
	    links.each(function(items) 
        {
		    items.addEvent('click', function(e) 
            {
			    e = new Event(e).stop();
			    links.removeClass('selected');
			    items.addClass('selected');
			    var tabClass = items.getParent().getParent().getProperty('class');
			    var tabClassSplit = tabClass.split(" ");
			    info.each(function(el) 
                {
				    var infoClass = el.getProperty('class');
				    if ( !infoClass.test(tabClassSplit[0]) ) 
                    {
					    el.addClass('hide');
				    }
				    else 
                    {
					    el.removeClass('hide');
				    }
			    });
		    });
	    });
    };

});
