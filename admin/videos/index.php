<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="videos";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


    if( !isset($_GET['categoryid']) ) $_GET['categoryid']=1;

    print "Category - ";
    $options_select_video_cat=array();
    $options_select_video_cat['name']="categoryid";
    $options_select_video_cat['id']="categoryid";
    $options_select_video_cat['onchange']="onchange=\"location.href='?categoryid='+document.getElementById('categoryid').options[selectedIndex].value\"";
    $options_select_video_cat['where']=" WHERE NULLIF(sub_categoryid, '') IS NULL ";
    $options_select_video_cat['selectedid']=$_GET['categoryid'];
    $options_select_video_cat['sub_categories']=1;
    $select_categories_video=admin_html::select_video_categories($db,$options_select_video_cat);
    print $select_categories_video['html_print'];
    print "<br /><br />";

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY sort ASC ";
    if( !empty($_GET['categoryid']) ) $query_where_videos=" WHERE categoryid='".$_GET['categoryid']."' ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_videos=QUERIES::query_videos($db,$query_where_videos,$query_order,$query_limit);

    $results=$db->query($query_videos['query_without_limit']);
    $count=$db->numrows($results);
    $results_videos=$db->query($query_videos['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {   
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) 
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        //admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        $ii=1;
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>videoid</th>\n";
                print "\t<th>image</th>\n";
                print "\t<th>title EN</th>\n";
                print "\t<th>time EN</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n"; 

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {   
                $row=UTILS::html_decode($row);
                print "\t<li id='item_".$row['videoID']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">".$row['videoID']."</td>\n";
                            print "\t<td><img src='/includes/retrieve.image.php?videoID=".$row['videoID']."&size=xs' alt='' /></td>\n";
                            print "\t<td>".$row['timeEN']."</td>\n";
                            print "\t<td>";
                                print "<a name='".$row['videoID']."' href='/admin/videos/edit/?videoid=".$row['videoID']."&categoryid=".$_GET['categoryid']."' title='edit video' class='link_edit'>".$row['titleEN']."</a>";
                            print "\t</td>";
                            print "\t<td>".$row['titleDE']."</td>\n";
                            print "\t<td>".$row['titleFR']."</td>\n";
                            print "\t<td>".$row['titleIT']."</td>\n";
                            print "\t<td>".$row['titleZH']."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }

        //admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No videos found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
