<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="videos_categories";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




    $query_where_videos=" WHERE NULLIF(sub_categoryid, '') IS NULL ";
    $query_order=" ORDER BY sort_en ";
    $query_quote=QUERIES::query_videos_categories($db,$query_where_videos,$query_order,$query_limit);
    $results=$db->query($query_quote['query']);
    $count=$db->numrows($results);

    if( $count>0 )
    {
        $ii=1;
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>ID</th>\n";
                print "\t<th>Image</th>\n";
                print "\t<th>Title EN</th>\n";
                print "\t<th>Title DE</th>\n";
                print "\t<th>Title FR</th>\n";
                print "\t<th>Title IT</th>\n";
                print "\t<th>Title ZH</th>\n";
                //print "\t<th>date modified</th>\n";
                //print "\t<th>date created</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n"; 

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while( $row=$db->mysql_array($results) )
            {
                $src="/datadir/images/video/categories/xsmall/".$row['src'];
                print "\t<li id='item_".$row['categoryid']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">".$row['categoryid']."</td>\n";
                            print "\t<td>";
                                print "\t<img src='".$src."' alt='' />\n";
                            print "\t</td>";
                            print "\t<td>";
                                print "<a name='".$row['categoryid']."' href='/admin/videos/categories/edit/?categoryid=".$row['categoryid']."' title='edit category info' class='link_edit'>".$row['title_en']."</a>";
                            print "\t</td>";
                            print "\t<td>".$row['title_de']."</td>";
                            print "\t<td>".$row['title_fr']."</td>";
                            print "\t<td>".$row['title_it']."</td>";
                            print "\t<td>".$row['title_zh']."</td>";
                            //print "\t<td>".$row['date_modified2']."</td>\n";
                            //print "\t<td>".$row['date_created2']."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";

                $results_sub=$db->query("SELECT 
                    *,
                    DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
                    DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2 
                    FROM ".TABLE_VIDEO_CATEGORIES." 
                    WHERE sub_categoryid='".$row['categoryid']."' 
                    ORDER BY sort_en ");
                $count_sub=$db->numrows($results_sub);

                if( $count_sub>0 )
                {
                    $ii++;
                    print "\t<li>\n";
                        print "\t<ul id='sortlist$ii' class='sortlist'>\n";
                            while($row_sub=$db->mysql_array($results_sub))
                            {
                                $src2="/datadir/images/video/categories/xsmall/".$row_sub['src'];
                                print "\t<li id='item_".$row_sub['categoryid']."'  >";
                                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_sub'>\n";
                                        print "\t<tr>\n";
                                            print "\t<td class='td_move' ".$style.">".$row_sub['categoryid']."</td>\n";
                                            print "\t<td>";
                                                print "\t<img src='".$src2."' alt='' />\n";
                                            print "\t</td>";
                                            print "\t<td>";
                                                print "<a name='".$row_sub['categoryid']."' href='/admin/videos/categories/edit/?categoryid=".$row_sub['categoryid']."' title='edit category info' class='link_edit'>".$row_sub['title_en']."</a>";
                                            print "\t</td>";
                                            print "\t<td>".$row_sub['title_de']."</td>";
                                            print "\t<td>".$row_sub['title_fr']."</td>";
                                            print "\t<td>".$row_sub['title_it']."</td>";
                                            print "\t<td>".$row_sub['title_zh']."</td>";
                                            //print "\t<td>".$row_sub['date_modified2']."</td>\n";
                                            //print "\t<td>".$row_sub['date_created2']."</td>\n";
                                        print "\t</tr>\n";
                                    print "\t</table>\n";
                                print "</li>\n";
                            }
                        print "\t</ul>\n";
                    print "\t</li>\n";
                }

            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {    
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sorting.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }    


    }
    else print "\t<p class='error'>No data found!</p>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
