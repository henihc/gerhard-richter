<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) ) $_SESSION['new_record']['error']['title_en']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/videos/categories/edit/?categoryid=".$_POST['categoryid']."&error=true");

$db=new dbCLASS;

$_POST=$db->filter_parameters($_POST,1);

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort_en",TABLE_VIDEO_CATEGORIES,"");
    $db->query("INSERT INTO ".TABLE_VIDEO_CATEGORIES."(sort_en,date_created) VALUES('".$maxsort."',NOW()) ");
    $categoryid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" ) 
{
    $categoryid=$_POST['categoryid'];
}

$titleurl=UTILS::convert_fortitleurl($db,TABLE_VIDEO_CATEGORIES,$_POST['title_en'],"categoryid","","titleurl","en");

//print $titleurl_en;
//print_r($_POST);
//exit();

    $query="UPDATE ".TABLE_VIDEO_CATEGORIES." SET 
        sub_categoryid='".$_POST['sub_categoryid']."',
        title_en='".$_POST['title_en']."',
        titleurl='".$titleurl."',
        title_de='".$_POST['title_de']."',
        title_fr='".$_POST['title_fr']."',
        title_it='".$_POST['title_it']."',
        title_zh='".$_POST['title_zh']."',
        desc_en='".$_POST['desc_en']."',
        desc_de='".$_POST['desc_de']."',
        desc_fr='".$_POST['desc_fr']."',
        desc_it='".$_POST['desc_it']."',
        desc_zh='".$_POST['desc_zh']."',
        enable='".$_POST['enable']."',
        notes_admin='".$_POST['notes_admin']."'";
    if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
    $query.=" WHERE categoryid='".$categoryid."' ";


if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,26,$categoryid);
    }
    elseif( $_POST['submit']=="update" ) 
    {
        admin_utils::admin_log($db,2,26,$categoryid);
    }
}
else
{
    admin_utils::admin_log($db,"",26,$_POST['categoryid'],2);
}


### image upload
if($_FILES['image']['error']==0)
{

    $fileName = str_replace (" ", "_", $_FILES['image']['name']);
    $tmpName  = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];

    if ( is_uploaded_file($tmpName) )
    {   
        $imageid=$categoryid;

        $getimagesize=getimagesize($tmpName);
        list($width, $height) = $getimagesize;
   
        switch( $getimagesize['mime'] )
        {   
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }

        $dir=DATA_PATH."/images/video/categories";
        $new_file_name=$imageid.".jpg";

        UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
        //UTILS::resize_image($tmpName,$fileName,SIZE_SMALL_QUOTES_CAT_HEIGHT,SIZE_SMALL_QUOTES_CAT_WIDTH,$dir.'/small/'.$new_file_name);
        //UTILS::resize_image($tmpName,$fileName,SIZE_MEDIUM_QUOTES_CAT_HEIGHT,SIZE_MEDIUM_QUOTES_CAT_WIDTH,$dir.'/medium/'.$new_file_name);
        //UTILS::resize_image($tmpName,$fileName,SIZE_LARGE_QUOTES_CAT_HEIGHT,SIZE_LARGE_QUOTES_CAT_WIDTH,$dir.'/large/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,$height,$width,$dir.'/original/'.$new_file_name);

        $db->query("UPDATE ".TABLE_VIDEO_CATEGORIES." SET src='".$new_file_name."' WHERE categoryid='".$imageid."'");

     }

}




unset($_SESSION['new_record']);


UTILS::redirect("/admin/videos/categories/edit/?categoryid=".$categoryid."&task=".$_POST['submit']);