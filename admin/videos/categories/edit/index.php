<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=3&itemid=".$_GET['micrositeid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="add_videos_category";

$html->metainfo();

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        # ckeditor text
        print "\tCKEDITOR.replace( 'desc_en', { toolbar : 'Large', width : '850px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'desc_de', { toolbar : 'Large', width : '850px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'desc_fr', { toolbar : 'Large', width : '850px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'desc_it', { toolbar : 'Large', width : '850px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'desc_zh', { toolbar : 'Large', width : '850px', height : '100px' } );\n";
        # tabs tite
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        # tabs desc-small
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-desc-small li a','#div-admin-tabs-info-desc-small .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
    # after submit disable submit button
    print "window.addEvent('domready',function() {";
        print "var subber = $$('.submit');";
        print "subber.addEvent('click',function() {";
            print "subber.set('disabled','disabled');";
            print "subber.set('value','Submitting...');";
            //print "(function() { subber.disabled = false; subber.set('value','Resubmit'); }).delay(10000);"; // how much time?  10 seconds
        print "});";
    print "});";

print "\t</script>\n";

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    $query_where_quote=" WHERE categoryid='".$_GET['categoryid']."' ";
    $query_quote=QUERIES::query_videos_categories($db,$query_where_quote,$query_order,$query_limit);
    $results=$db->query($query_quote['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( !empty($row['date_modified2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified2']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date Created</td>\n";
                    print "\t<td>".$row['date_created2']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( $_GET['error'] ) $sub_categoryid=$_SESSION['new_record']['sub_categoryid'];
                else $sub_categoryid=$row['sub_categoryid'];
                print "\t<th class='th_align_left'>Sub Category</th>\n";
                print "\t<td>\n";

                    $options_select_video_cat=array();
                    $options_select_video_cat['name']="sub_categoryid";
                    $options_select_video_cat['id']="sub_categoryid";
                    $options_select_video_cat['where']=" WHERE NULLIF(sub_categoryid, '') IS NULL AND categoryid!='".$_GET['categoryid']."' ";
                    $options_select_video_cat['selectedid']=$sub_categoryid;
                    $options_select_video_cat['sub_categories']=0;
                    $select_categories_video=admin_html::select_video_categories($db,$options_select_video_cat);
                    print $select_categories_video['html_print'];

                    if( $count )
                    {
                        $options_video_cat_url=array();
                        $options_video_cat_url['categoryid']=$row['categoryid'];
                        $video_category_url=UTILS::get_video_cat_url($db,$options_video_cat_url);
                        print "\t<a href='".$video_category_url['url']."' title='View in page' class='a-view-on-page-new'><u>View in page</u></a>\n";
                    }

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_GET['error'] && $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                elseif( $row['enable'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td><input name='enable' $checked type='checkbox' value='1' class='' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                    if( !empty($row['src']) ) print "\t<img src='".DATA_PATH_DATADIR."/images/video/categories/original/".$row['src']."' alt='' />\n";
                    else print "Category image";
                    print "<br />Height = 93px<br />Width = 148px";
                print "\t</th>\n";
                print "\t<td>";
                    print "<input type='file' name='image' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['title_en'];
                            print "\t<textarea class='form' name='title_en' cols='29' rows='3'>".$title_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['title_de'];
                            print "\t<textarea class='form' name='title_de' cols='29' rows='3'>".$title_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['title_fr'];
                            print "\t<textarea class='form' name='title_fr' cols='29' rows='3'>".$title_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['title_it'];
                            print "\t<textarea class='form' name='title_it' cols='29' rows='3'>".$title_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['title_zh'];
                            print "\t<textarea class='form' name='title_zh' cols='29' rows='3'>".$title_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Description</th>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<ul id='ul-admin-tabs-desc-small' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-desc-small' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_en=$_SESSION['new_record']['desc_en'];
                            else $desc_en=$row['desc_en'];
                            print "\t<textarea class='form' name='desc_en' cols='29' rows='3'>".$desc_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_de=$_SESSION['new_record']['desc_de'];
                            else $desc_de=$row['desc_de'];
                            print "\t<textarea class='form' name='desc_de' cols='29' rows='3'>".$desc_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_fr=$_SESSION['new_record']['desc_fr'];
                            else $desc_fr=$row['desc_fr'];
                            $desc_fr=html_entity_decode($desc_fr,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='desc_fr' cols='29' rows='3'>".$desc_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_it=$_SESSION['new_record']['desc_it'];
                            else $desc_it=$row['desc_it'];
                            $desc_it=html_entity_decode($desc_it,ENT_NOQUOTES,DB_ENCODEING);
                            print "\t<textarea class='form' name='desc_it' cols='29' rows='3'>".$desc_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $desc_zh=$_SESSION['new_record']['desc_zh'];
                            else $desc_zh=$row['desc_zh'];
                            print "\t<textarea class='form' name='desc_zh' cols='29' rows='3'>".$desc_zh."</textarea>\n";
                        print "\t</div>\n";                        
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</th>\n";
                if( $_GET['error'] ) $notes_admin=$_SESSION['new_record']['notes_admin'];
                else $notes_admin=$row['notes_admin'];
                print "\t<td><textarea rows='6' cols='35' name='notes_admin'>".$notes_admin."</textarea></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/quotes/categories/edit/del.php?quotes_categoryid=".$row['quotes_categoryid']."','".addslashes($row['title_en'])."','');\" title='delete this category'>delete</a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/videos/categories/edit/del.php?categoryid=".$row['categoryid']."";
                        $options_delete['text1']="Videos - Categories - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</th>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='categoryid' value='".$row['categoryid']."' />\n";
    print "\t</form>\n";



        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
