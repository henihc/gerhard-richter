<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) && empty($_POST['title_de']) ) $_SESSION['new_record']['error']['title']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/videos/edit/?videoid=".$_POST['videoid']."&categoryid=".$_POST['selected_categoryid']."&error=true");

$db=new dbCLASS;



$info_en=$db->db_prepare_input($_POST['info_en'],1);
$info_de=$db->db_prepare_input($_POST['info_de'],1);
$info_fr=$db->db_prepare_input($_POST['info_fr'],1);
$info_it=$db->db_prepare_input($_POST['info_it'],1);
$info_zh=$db->db_prepare_input($_POST['info_zh'],1);

$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['title_de'];
$title_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_fr'];
$title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_it'];
$title_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['name'];
$name_search=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['info_de'];
$info_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['info_fr'];
$info_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['info_it'];
$info_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

# replace quotes
$title_en=$db->db_replace_quotes($db,$_POST['title_en']);
$title_de=$db->db_replace_quotes($db,$_POST['title_de']);
$title_fr=$db->db_replace_quotes($db,$_POST['title_fr']);
$title_it=$db->db_replace_quotes($db,$_POST['title_it']);
$title_zh=$db->db_replace_quotes($db,$_POST['title_zh']);
# END replace quotes

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_VIDEO."(date) VALUES(NOW())");  
    $videoid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $videoid=$_POST['videoid'];
} 
else UTILS::redirect("/");

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_PAINTING,$_POST['title_en'],"videoID",$videoid,"titleurl_en","en");

$query="UPDATE ".TABLE_VIDEO." SET 
            categoryid='".$_POST['categoryid']."',
            titleEN='".$title_en."',
            titleDE='".$title_de."',
            titleFR='".$title_fr."',
            titleIT='".$title_it."',
            titleZH='".$title_zh."',
            title_search_1='".$title_search_de['text_lower_converted']."',
            title_search_2='".$title_search_de['text_lower_german_converted']."',
            title_search_3='".$title_search_fr['text_lower_converted']."',
            title_search_4='".$title_search_it['text_lower_converted']."',
            titleurl='".$titleurl_en."',
            name='".$_POST['name']."',
            name_search_1='".$name_search_de['text_lower_converted']."',
            name_search_2='".$name_search_de['text_lower_german_converted']."',
            locationid='".$_POST['locationid']."',
            cityid='".$_POST['cityid']."',
            countryid='".$_POST['countryid']."',
            year='".$_POST['year']."',
            timeEN='".$_POST['time_en']."',
            timeDE='".$_POST['time_de']."',
            timeFR='".$_POST['time_fr']."',
            timeIT='".$_POST['time_it']."',
            timeZH='".$_POST['time_zh']."',
            info_en='".$info_en."',
            info_de='".$info_de."',
            info_fr='".$info_fr."',
            info_it='".$info_it."',
            info_zh='".$info_zh."',
            info_search_1='".$info_search_de['text_lower_converted']."',
            info_search_2='".$info_search_de['text_lower_german_converted']."',
            info_search_3='".$info_search_fr['text_lower_converted']."',
            info_search_4='".$info_search_it['text_lower_converted']."',
            enable='".$_POST['enable']."',
            height='".$_POST['height']."',
            width='".$_POST['width']."',
            watermark='".$_POST['watermark']."' 
        WHERE videoID='".$videoid."'";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    # RELATION adding
    admin_utils::add_relation($db,10,$videoid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,26,$videoid);  
    }
    elseif( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,26,$videoid); 
    }
}
else
{
    admin_utils::admin_log($db,"",26,$_POST['videoid'],2);
}

#image thumb upload
if( $_FILES['image_thumb']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['image_thumb']['name']);
    $tmpName  = $_FILES['image_thumb']['tmp_name'];
    $fileSize = $_FILES['image_thumb']['size'];
    $fileType = $_FILES['image_thumb']['type'];

	if (is_uploaded_file($tmpName))
	{
        $getimagesize = getimagesize($tmpName);
         
        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }
                       
        $dir=DATA_PATH."/images/video";
        $new_file_path=$dir."/original/".$videoid.$ext;
        $newfileName_original=$videoid.$ext;
        $newfileName=$videoid.".jpg";

	    UTILS::resize_image($tmpName,$fileName,SIZE_VIDEO_XSMALL_HEIGHT,SIZE_VIDEO_XSMALL_WIDTH,$dir.'/xsmall/'.$newfileName);
	    UTILS::resize_image($tmpName,$fileName,SIZE_VIDEO_MEDIUM_HEIGHT,SIZE_VIDEO_MEDIUM_WIDTH,$dir.'/medium/'.$newfileName);
		
		#copy original
        /*
        if (!copy($tmpName, $new_file_path))
		{
		    print "Error Uploading File.";
		    exit();
		}

	    $db->query("UPDATE ".TABLE_VIDEO." SET image='".$newfileName_original."' WHERE videoID = '".$videoid."'");
         */
	}

}
#end image thumb upload

#image large upload
if( $_FILES['image_large']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['image_large']['name']);
    $tmpName  = $_FILES['image_large']['tmp_name'];
    $fileSize = $_FILES['image_large']['size'];
    $fileType = $_FILES['image_large']['type'];

	if (is_uploaded_file($tmpName))
	{
        $getimagesize = getimagesize($tmpName);
         
        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }
                       
        $dir=DATA_PATH."/images/video";
        $new_file_path=$dir."/original/".$videoid.$ext;
        $newfileName_original=$videoid.$ext;
        $newfileName=$videoid.".jpg";

	    UTILS::resize_image($tmpName,$fileName,SIZE_VIDEO_LARGE_HEIGHT,SIZE_VIDEO_LARGE_WIDTH,$dir.'/large/'.$newfileName);
		
		#copy original
		if (!copy($tmpName, $new_file_path))
		{
		    print "Error Uploading File.";
		    exit();
		}

	    $db->query("UPDATE ".TABLE_VIDEO." SET image='".$newfileName_original."' WHERE videoID = '".$videoid."'");
	}

}
#end image large upload

# qt upload
if( $_FILES['qtfile']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['qtfile']['name']);
    $tmpName  = $_FILES['qtfile']['tmp_name'];
    $tmp=explode(".", $fileName);

    if( is_uploaded_file($tmpName) )
    {
        $dir=DATA_PATH."/videos/qt";
        $new_file_path=$dir."/".$videoid.".mov";
        #copy file
        if (!copy($tmpName, $new_file_path))
        {
            print "Error Uploading File.";
            exit();
        }
        else $db->query("UPDATE ".TABLE_VIDEO." SET file_qt='".$videoid."' WHERE videoID = '".$videoid."'");
    }
}
#end qt upload

# flv upload
if( $_FILES['flashflvfile']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['flashflvfile']['name']);
    $tmpName  = $_FILES['flashflvfile']['tmp_name'];
    $tmp=explode(".", $fileName);

    if (is_uploaded_file($tmpName))
    {   
        $dir=DATA_PATH."/videos/flash/";
        $new_file_path=$dir."/".$videoid.".flv";
        #copy file
        if (!copy($tmpName, $new_file_path))
        {   
            print "Error Uploading File.";
            exit();
        }   
        else $db->query("UPDATE ".TABLE_VIDEO." SET file='".$videoid."' WHERE videoID = '".$videoid."'");
    }   
}   
#end flv upload

# m4v upload
if( $_FILES['m4vfile']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['m4vfile']['name']);
    $tmpName  = $_FILES['m4vfile']['tmp_name'];
    $tmp=explode(".", $fileName);

    if (is_uploaded_file($tmpName))
    {   
        $dir=DATA_PATH."/videos/m4v/";
        $new_file_path=$dir."/".$videoid.".m4v";
        #copy file
        if (!copy($tmpName, $new_file_path))
        {   
            print "Error Uploading File.";
            exit();
        }   
        else $db->query("UPDATE ".TABLE_VIDEO." SET file='".$videoid."' WHERE videoID = '".$videoid."'");
    }   
}   
#end m4v upload

# ogv upload
if( $_FILES['ogvfile']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['ogvfile']['name']);
    $tmpName  = $_FILES['ogvfile']['tmp_name'];
    $tmp=explode(".", $fileName);

    if (is_uploaded_file($tmpName))
    {   
        $dir=DATA_PATH."/videos/ogv/";
        $new_file_path=$dir."/".$videoid.".ogv";
        #copy file
        if (!copy($tmpName, $new_file_path))
        {   
            print "Error Uploading File.";
            exit();
        }
        else $db->query("UPDATE ".TABLE_VIDEO." SET file='".$videoid."' WHERE videoID = '".$videoid."'");
    }   
}   
#end ogv upload

# srt upload
if( $_FILES['srtfile']['error']==0 )
{
    $fileName = str_replace (" ", "_", $_FILES['srtfile']['name']);
    $tmpName  = $_FILES['srtfile']['tmp_name'];
    $tmp=explode(".", $fileName);

    if (is_uploaded_file($tmpName))
    {   
        $maxsort=UTILS::max_table_value($db,"sort",TABLE_VIDEO_SUBTITLES,"");
        $db->query("INSERT INTO ".TABLE_VIDEO_SUBTITLES."(videoid,languageid,sort,date_created) VALUES('".$videoid."','".$_POST['languageid']."','".$maxsort."',NOW())");  
        $subtitleid=$db->return_insert_id();

        $dir=DATA_PATH."/videos/subtitles";
        $new_file_path=$dir."/".$subtitleid.".srt";
        #copy file
        if (!copy($tmpName, $new_file_path))
        {   
            print "Error Uploading File.";
            exit();
        }
        else $db->query("UPDATE ".TABLE_VIDEO_SUBTITLES." SET enable=1 WHERE subtitleid = '".$subtitleid."'");
    }   
}   
#end srt upload



unset($_SESSION['new_record']);

$url="/admin/videos/edit/?videoid=".$videoid."&categoryid=".$_POST['categoryid']."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
