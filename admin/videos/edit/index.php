<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=10&itemid=".$_GET['videoid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="media";
$html->menu_admin_sub_selected="add_video";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



$query_where_video=" WHERE videoID='".$_GET['videoid']."' ";
$query_video=QUERIES::query_videos($db,$query_where_video,$query_order,$query_limit);
$results=$db->query($query_video['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);
if( $count>0 ) $row['videoid']=$row['videoID'];


print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'info_en', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'info_de', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'info_fr', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'info_it', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'info_zh', { toolbar : 'Large' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-time li a','#div-admin-tabs-info-time .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-info li a','#div-admin-tabs-info-info .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";


    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                print "\t<td>";
                    if( $row['enable'] || $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input name='enable' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
                print "\t<td style='text-align:right;'>\n";
                    $values=array();
                    $values['videoid']=$row['videoid'];
                    $video_url=UTILS::get_video_url($db,$values);
                    if( $count ) print "\t<a href='".$video_url['url']."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Category</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] ) $categoryid=$_SESSION['new_record']['categoryid'];
                    else $categoryid=$row['categoryid'];
                    $options_select_video_cat=array();
                    $options_select_video_cat['name']="categoryid";
                    $options_select_video_cat['id']="categoryid";
                    $options_select_video_cat['where']=" WHERE NULLIF(sub_categoryid, '') IS NULL ";
                    $options_select_video_cat['selectedid']=$categoryid;
                    $options_select_video_cat['sub_categories']=1;
                    $select_categories_video=admin_html::select_video_categories($db,$options_select_video_cat);
                    print $select_categories_video['html_print'];
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' class='".$class."' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['titleEN'];
                            print "\t<input type='text' id='title_en' name='title_en' value='".$title_en."' class='admin-input-large' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['titleDE'];
                            print "\t<input type='text' id='title_de' name='title_de' value='".$title_de."' class='admin-input-large' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['titleFR'];
                            print "\t<input type='text' id='title_fr' name='title_fr' value=\"".$title_fr."\" class='admin-input-large' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['titleIT'];
                            print "\t<input type='text' id='title_it' name='title_it' value=\"".$title_it."\" class='admin-input-large' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['titleZH'];
                            print "\t<input type='text' id='title_zh' name='title_zh' value='".$title_zh."' class='admin-input-large' />\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='".$class." th_align_left'>Name</td>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $name=$_SESSION['new_record']['name'];
                    else $name=$row['name'];
                    print "\t<input type='text' name='name' value='".$name."'>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<table class='admin-table' cellspacing='0'>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Country</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="countryid";
                                $values['selectedid']=$row['countryid'];
                                $values['onchange']=" onchange=\"
                                                        $('cityid').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-city').style.display='';\" ";
                                admin_html::select_locations_country($db,$values);
                                if( $count ) UTILS::admin_edit("/admin/locations/edit/?typeid=3&countryid=".$row['countryid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-city' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>City</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="cityid";
                                $values['selectedid']=$row['cityid'];
                                $values['onchange']=" onchange=\"
                                                        $('locationid').load('/admin/locations/options_locations.php?countryid='+document.getElementById('countryid').options[document.getElementById('countryid').selectedIndex].value+'&cityid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-location').style.display='';\" ";
                                $values['where']=" WHERE countryid='".$row['countryid']."' ";
                                admin_html::select_locations_city($db,$values);
                                if( $count && !empty($row['cityid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=2&cityid=".$row['cityid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";


                        if( empty($row['cityid']) && empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-location' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>Location</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="locationid";
                                $values['selectedid']=$row['locationid'];
                                $values['where']=" WHERE countryid='".$row['countryid']."' AND cityid='".$row['cityid']."' ";
                                admin_html::select_locations($db,$values);
                                if( $count && !empty($row['locationid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=1&locationid=".$row['locationid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year</th>\n";
                print "\t<td colspan='5'>\n";
                    print admin_html::get_years("",$row['year'],"");
                print "\t</td>\n";
            print "\t</tr>\n";

/*
            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['titlelong'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Title long<br /> English German Chinese</td>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $titlelong_en=$_SESSION['new_record']['titlelong_en'];
                    else $titlelong_en=$row['titlelong_en'];
                    print "\t<textarea name='titlelong_en' cols='29' rows='3'>".$titlelong_en."</textarea>\n";
                    if( $_GET['error'] ) $titlelong_de=$_SESSION['new_record']['titlelong_de'];
                    else $titlelong_de=$row['titlelong_de'];
                    print "\t<textarea name='titlelong_de' cols='29' rows='3'>".$titlelong_de."</textarea>\n";
                    if( $_GET['error'] ) $titlelong_zh=$_SESSION['new_record']['titlelong_zh'];
                    else $titlelong_zh=$row['titlelong_zh'];
                    print "\t<textarea name='titlelong_zh' cols='29' rows='3'>".$titlelong_zh."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
 */


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Time</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<ul id='ul-admin-tabs-time' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-time' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $time_en=$_SESSION['new_record']['time_en'];
                            else $time_en=$row['timeEN'];
                            print "\t<input type='text' id='time_en' name='time_en' value='".$time_en."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $time_de=$_SESSION['new_record']['time_de'];
                            else $time_de=$row['timeDE'];
                            print "\t<input type='text' id='time_de' name='time_de' value='".$time_de."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $time_fr=$_SESSION['new_record']['time_fr'];
                            else $time_fr=$row['timeFR'];
                            print "\t<input type='text' id='time_fr' name='time_fr' value='".$time_fr."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $time_it=$_SESSION['new_record']['time_it'];
                            else $time_it=$row['timeIT'];
                            print "\t<input type='text' id='time_it' name='time_it' value='".$time_it."' />\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $time_zh=$_SESSION['new_record']['time_zh'];
                            else $time_zh=$row['timeZH'];
                            print "\t<input type='text' id='time_zh' name='time_zh' value='".$time_zh."' />\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Height x Width</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] ) $height=$_SESSION['new_record']['height'];
                    else $height=$row['height'];
                    print "\t<input name='height' type='text' value='".$height."' />";
                    if( $_GET['error'] ) $width=$_SESSION['new_record']['width'];
                    else $width=$row['width'];
                    print "\t x <input name='width' type='text' value='".$width."' />";
                print "</td>\n";
            print "\t</tr>\n";

/*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>DescriptionEN</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] ) $descriptionEN=$_SESSION['new_record']['descriptionEN'];
                    else $descriptionEN=$row['descriptionEN'];
                    print "\t<textarea name='descriptionEN' cols='90' rows='4'>".$descriptionEN."</textarea>\n";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>DescriptionDE</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] ) $descriptionDE=$_SESSION['new_record']['descriptionDE'];
                    else $descriptionDE=$row['descriptionDE'];
                    print "\t<textarea name='descriptionDE' cols='90' rows='4'>".$descriptionDE."</textarea>\n";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>DescriptionZH</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] ) $descriptionZH=$_SESSION['new_record']['descriptionZH'];
                    else $descriptionZH=$row['descriptionZH'];
                    print "\t<textarea name='descriptionZH' cols='90' rows='4'>".$descriptionZH."</textarea>\n";
                print "</td>\n";
            print "\t</tr>\n";
 */
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Information</th>\n";
                print "\t<td colspan='2'>";

                    print "\t<ul id='ul-admin-tabs-info' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-info' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $info_en=$_SESSION['new_record']['info_en'];
                            else $info_en=$row['info_en'];
                            print "\t<textarea name='info_en' id='info_en' cols='90' rows='6'>".$info_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $info_de=$_SESSION['new_record']['info_de'];
                            else $info_de=$row['info_de'];
                            print "\t<textarea name='info_de' id='info_de' cols='90' rows='6'>".$info_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $info_fr=$_SESSION['new_record']['info_fr'];
                            else $info_fr=$row['info_fr'];
                            print "\t<textarea name='info_fr' id='info_fr' cols='90' rows='6'>".$info_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $info_it=$_SESSION['new_record']['info_it'];
                            else $info_it=$row['info_it'];
                            print "\t<textarea name='info_it' id='info_it' cols='90' rows='6'>".$info_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $info_zh=$_SESSION['new_record']['info_zh'];
                            else $info_zh=$row['info_zh'];
                            print "\t<textarea name='info_zh' id='info_zh' cols='90' rows='6'>".$info_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";


                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Watermark</th>\n";
                print "\t<td colspan='2'>";
                    if( $_GET['error'] && $_SESSION['new_record']['watermark'] ) $checked="checked='checked'";
                    elseif( !$row['watermark'] && $count>0 ) $checked="";
                    else $checked="checked='checked'";
                    print "\t<input name='watermark' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( !empty($row['image']) ) $image="<a href='/includes/retrieve.image.php?videoID=".$row['videoid']."&size=m' title='View in actual size' target='_blank'><img src='/includes/retrieve.image.php?videoID=".$row['videoid']."&size=m&thumb=1&border=1&maxwidth=100&maxheight=100' alt='' /></a>";
                    else $image="Image Thumbnail";
                    print $image;
                    print "<br />Height - ".SIZE_VIDEO_SMALL_HEIGHT."px<br />Width - ".SIZE_VIDEO_SMALL_WIDTH."px";
                print "</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='image_thumb' type='file' value='' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( !empty($row['image']) ) $image="<a href='/includes/retrieve.image.php?videoID=".$row['videoid']."&size=o' title='View in large size' target='_blank'><img src='/includes/retrieve.image.php?videoID=".$row['videoid']."&size=t&maxwidth=170&maxheight=170&thumb=1&border=1' alt='' /></a>";
                    else $image="Image Large";
                    print $image;
                    print "<br />Height - ".SIZE_VIDEO_LARGE_HEIGHT."px<br />Width - ".SIZE_VIDEO_LARGE_WIDTH."px";
                print "</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='image_large' type='file' value='' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>*.flv video</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='flashflvfile' type='file' value='' />";
                    $flash_url=DATA_PATH."/videos/flash/".$row['file'].".flv";
                    if( file_exists($flash_url) )
                    {
                        print "\t<a href='/includes/retrieve.video.php?videoID=".$row['videoid']."&type=f' title=''><u>".$row['file'].".flv</u></a>\n";
                        //print "\t&nbsp;&nbsp;<a href='/videos/detail.php?vID=".$row['videoid']."&amp;type=F&amp;enable=1' title=''><u>flash video</u></a>\n";
                        //print "\t&nbsp<a class='a_edit_item' onclick=\"delete_confirm2('/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=flv','flv file','')\" title='Delete flv file'><img src='/g/delete.gif' alt='' /></a>\n";
                        print "\t&nbsp";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=flv";
                        $options_delete['text1']="Videos - flv file - ".$row['file'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>*.m4v video</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='m4vfile' type='file' value='' />";
                    $m4v_url=DATA_PATH."/videos/m4v/".$row['file'].".m4v";
                    if( file_exists($m4v_url) )
                    {
                        print "\t<a href='/includes/retrieve.video.php?videoID=".$row['videoid']."&type=m4v' title=''><u>".$row['file'].".m4v</u></a>\n";
                        //print "\t&nbsp;&nbsp;<a href='/videos/detail.php?vID=".$row['videoid']."&amp;type=f&amp;enable=1' title=''><u>m4v video</u></a>\n";
                        //print "\t&nbsp<a class='a_edit_item' onclick=\"delete_confirm2('/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=m4v','m4v file','')\" title='Delete m4v file'><img src='/g/delete.gif' alt='' /></a>\n";
                        print "\t&nbsp";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=m4v";
                        $options_delete['text1']="Videos - m4v file - ".$row['file'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>*.ogv video</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='ogvfile' type='file' value='' />";
                    $ogv_url=DATA_PATH."/videos/ogv/".$row['file'].".ogv";
                    if( file_exists($ogv_url) )
                    {
                        print "\t<a href='/includes/retrieve.video.php?videoID=".$row['videoid']."&type=ogv' title=''><u>".$row['file'].".ogv</u></a>\n";
                        //print "\t&nbsp;&nbsp;<a href='/videos/detail.php?vID=".$row['videoid']."&amp;type=f&amp;enable=1' title=''><u>ogv video</u></a>\n";
                        //print "\t&nbsp<a class='a_edit_item' onclick=\"delete_confirm2('/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=ogv','ogv file','')\" title='Delete ogv file'><img src='/g/delete.gif' alt='' /></a>\n";
                        print "\t&nbsp";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/videos/edit/del_video_file.php?videoid=".$row['videoid']."&type=ogv";
                        $options_delete['text1']="Videos - ogv file - ".$row['file'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>*.srt subtitles</th>\n";
                print "\t<td colspan='2'>";
                    admin_html::select_languages($db,"languageid",$selectedid,$class=array());
                    print "\t&nbsp;<input name='srtfile' type='file' value='' />";
                    $results_subtitles=$db->query("SELECT * FROM ".TABLE_VIDEO_SUBTITLES." WHERE videoid='".$row['videoid']."' ORDER BY sort ");
                    $count_subtitles=$db->numrows($results_subtitles);
                    if( $count_subtitles )
                    {
                        $i=0;
                        while( $row_subtitles=$db->mysql_array($results_subtitles) )
                        {
                            $i++;
                            if( $i>1 ) $comma=", ";
                            $srt_url=DATA_PATH."/videos/subtitles/".$row_subtitles['subtitleid'].".srt";
                            //print $srt_url;
                            print "\t".$comma."<a href='/includes/retrieve.video_subtitles.php?subtitleid=".$row_subtitles['subtitleid']."' title=''><u>";
                            //$options_languages=array();
                            //$options_languages['languageid']=$row_subtitles['languageid'];
                            //$language_info=$html->languages($db,$options_languages);

                            $options_languages=array();
                            $options_languages['not_load_lang_file']=1;
                            $language_info=UTILS::load_lang_file($db,$options_languages);

                            if( file_exists($srt_url) )
                            {
                                print $language_info['languages'][$row_subtitles['languageid']]['title_display']." ".$row_subtitles['subtitleid'].".srt";
                            }
                            else print $language_info['languages'][$row_subtitles['languageid']]['title_display']." - subtitle file ".$row_subtitles['subtitleid'].".srt not found";
                            print "</u></a>";
                            //print "\t&nbsp<a onclick=\"delete_confirm2('/admin/videos/edit/del_subtitle.php?subtitleid=".$row_subtitles['subtitleid']."&videoid=".$row['videoid']."','".addslashes($language_info['languages'][$row_subtitles['languageid']]['title_display'])."','')\" title=''><img src='/g/delete.gif' alt='' /></a>\n";
                            print "\t&nbsp";
                            $options_delete=array();
                            $options_delete['url_del']="/admin/videos/edit/del_subtitle.php?subtitleid=".$row_subtitles['subtitleid']."&videoid=".$row['videoid']."";
                            $options_delete['text1']="Videos - Subtitle file - ".$language_info['languages'][$row_subtitles['languageid']]['title_display'];
                            //$options_delete['text2']="";
                            //$options_delete['html_return']=1;
                            admin_html::delete_button($db,$options_delete);
                        }
                    }
                print "</td>\n";
            print "\t</tr>\n";

/*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Quicktime video</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<input name='qtfile' type='file' value='' />";
                    $quicktime_url=DATA_PATH."/videos/qt/".$row['file_qt'].".mov";
                    if( file_exists($quicktime_url) ) 
                    {
                        print "\t<a href='/includes/retrieve.video.php?videoID=".$row['videoid']."&type=q' title=''><u>".$row['file_qt'].".mov</u></a>\n";
                        print "\t&nbsp;&nbsp;<a href='/videos/detail.php?vID=".$row['videoid']."&amp;type=Q&amp;enable=1' title=''><u>quicktime video</u></a>\n";
                    }                   
                print "</td>\n";
            print "\t</tr>\n";
*/


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='2'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' id='itemid' name='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",10,$row['videoid'],2,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( $count )
                    {
                        //$mandatory="<a href='#' title='' onclick=\"delete_confirm2('/admin/videos/edit/del.php?videoid=".$row['videoid']."','".addslashes($row['titleEN'])."','')\" /><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/videos/edit/del.php?videoid=".$row['videoid']."";
                        $options_delete['text1']="Video - ".$row['titleEN'];
                        //$options_delete['text2']="";
                        $options_delete['html_return']=1;
                        $mandatory=admin_html::delete_button($db,$options_delete);
                    }
                    else $mandatory="* Mandatory";
                    print $mandatory;
                print "</th>\n";
                print "\t<td colspan='2'>";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input name='submit' type='submit' value='".$value."' />";
                print "</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='videoid' value='".$row['videoid']."' />\n";
        print "\t<input type='hidden' name='selected_categoryid' value='".$_GET['categoryid']."' />\n";
    print "\t</form>\n";

    # printing out relations
    admin_html::show_relations($db,10,$row['videoid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
