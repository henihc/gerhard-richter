<?php

class admin_utils
{

    public static function extract_colors($db,$img_src,$options=array())
    {
        if( empty($options['num_results']) ) $options['num_results']=10;
        if( empty($options['reduce_brightness']) ) $options['reduce_brightness']=1;
        if( empty($options['reduce_gradients']) ) $options['reduce_gradients']=1;
        if( empty($options['delta']) ) $options['delta']=30;

        $ex=new GetMostCommonColors();
        $color_convert=new Lux_Color;
        $colors=$ex->Get_Color( $img_src, $options['num_results'], $options['reduce_brightness'], $options['reduce_gradients'], $options['delta']);

        $lab=array();
        $hsl_new=array();
        $colors_array=array();

        if( count($colors)>0 )
        {
            foreach ( $colors as $hex => $count_colors )
            {
                if ( $count_colors > 0 )
                {
                    # LAB
                    $color_lab = new Color();
                    $color_lab->fromHex($hex);
                    $matchColor = $color_lab->toLabCie();
                    $lab[0]=$matchColor['l'];
                    $lab[1]=$matchColor['a'];
                    $lab[2]=$matchColor['b'];
                    #END LAB

                    # HSL
                    $hsl=$color_convert->hex2hsl($hex);
                    # END HSL

                    # normalize HSL
                    if( !empty($hsl[0]) ) $hsl_new[0]=round(360*$hsl[0]);
                    else $hsl_new[0]=0;
                    if( !empty($hsl[1]) ) $hsl_new[1]=round(100*$hsl[1]);
                    else $hsl_new[1]=0;
                    if( !empty($hsl[2]) ) $hsl_new[2]=round(100*$hsl[2]);
                    else $hsl_new[2]=0;
                    # END normalize HSL

                    # Get color name
                    $color_name="";

                    #H
                    if(in_array($hsl_new[0], range(0, 18))) $color_name_h="Red";
                    if(in_array($hsl_new[0], range(18, 35))) $color_name_h="Orange";
                    if(in_array($hsl_new[0], range(36, 68))) $color_name_h="Yellow";
                    if(in_array($hsl_new[0], range(69, 158))) $color_name_h="Green";
                    if(in_array($hsl_new[0], range(159, 265))) $color_name_h="Blue";
                    if(in_array($hsl_new[0], range(266, 331))) $color_name_h="Magenta";
                    if(in_array($hsl_new[0], range(331, 360))) $color_name_h="Red";

                    #L
                    if( $hsl_new[1]<5 && in_array($hsl_new[2], range(0, 10)) ) $color_name="Black";
                    else if(in_array($hsl_new[2], range(0, 3))) $color_name="Black";
                    elseif(in_array($hsl_new[2], range(90, 100))) $color_name="White";
                    elseif(in_array($hsl_new[1], range(0, 10)))
                    {
                        $color_name="Grey";
                        if(in_array($hsl_new[2], range(3, 20))) $color_name="Dark ".$color_name;
                        elseif(in_array($hsl_new[2], range(70, 90))) $color_name="Light ".$color_name;
                    }
                    elseif(in_array($hsl_new[2], range(3, 20))) $color_name="Dark ".$color_name_h;
                    elseif(in_array($hsl_new[2], range(70, 90))) $color_name="Light ".$color_name_h;

                    #S
                    elseif(in_array($hsl_new[1], range(11, 20))) $color_name="Dull ".$color_name_h;
                    elseif( in_array($hsl_new[0], range(25, 45)) && in_array($hsl_new[1], range(21, 60)) && ( in_array($hsl_new[2], range(3, 20)) || in_array($hsl_new[2], range(21, 50)) ) ) $color_name="Brown";
                    else $color_name=$color_name_h;
                    if( $color_name=="Dark Yellow" ) $color_name="Dark Olive";
                    #END Get color name

                    if( $options['export']==1 )
                    {
                        $colors_array[$hex]['hex']=$hex;
                        //$colors_array[$hex]['hsl'][0]=$hsl[0];
                        //$colors_array[$hex]['hsl'][1]=$hsl[1];
                        //$colors_array[$hex]['hsl'][2]=$hsl[2];
                        $colors_array[$hex]['hsl'][0]=$hsl_new[0];
                        $colors_array[$hex]['hsl'][1]=$hsl_new[1];
                        $colors_array[$hex]['hsl'][2]=$hsl_new[2];
                        $colors_array[$hex]['hsl_new'][0]=$hsl_new[0];
                        $colors_array[$hex]['hsl_new'][1]=$hsl_new[1];
                        $colors_array[$hex]['hsl_new'][2]=$hsl_new[2];
                        $colors_array[$hex]['lab'][0]=$lab[0];
                        $colors_array[$hex]['lab'][1]=$lab[1];
                        $colors_array[$hex]['lab'][2]=$lab[2];
                        $colors_array[$hex]['name']=$color_name;
                        $colors_array[$hex]['match_color']=$matchColor;
                    }
                    elseif( empty($colors_array[$color_name]) )
                    {
                        $colors_array[$color_name]['hex']=$hex;
                        //$colors_array[$color_name]['hsl'][0]=$hsl[0];
                        //$colors_array[$color_name]['hsl'][1]=$hsl[1];
                        //$colors_array[$color_name]['hsl'][2]=$hsl[2];
                        $colors_array[$color_name]['hsl'][0]=$hsl_new[0];
                        $colors_array[$color_name]['hsl'][1]=$hsl_new[1];
                        $colors_array[$color_name]['hsl'][2]=$hsl_new[2];
                        $colors_array[$color_name]['hsl_new'][0]=$hsl_new[0];
                        $colors_array[$color_name]['hsl_new'][1]=$hsl_new[1];
                        $colors_array[$color_name]['hsl_new'][2]=$hsl_new[2];
                        $colors_array[$color_name]['lab'][0]=$lab[0];
                        $colors_array[$color_name]['lab'][1]=$lab[1];
                        $colors_array[$color_name]['lab'][2]=$lab[2];
                        $colors_array[$color_name]['name']=$color_name;
                        $colors_array[$color_name]['match_color']=$matchColor;
                    }

                    if( $options['export']==1 ) $colors_array[$hex]['percentage']=$count_colors;
                    else $colors_array[$color_name]['percentage']=$colors_array[$color_name]['percentage']+$count_colors;
                }
            }
        }

        return $colors_array;

    } #END colors extract function


    public static function add_relation($db,$typeid1,$itemid1,$typeid2,$itemid2,$sort_relation="",$import=0)
    {
        if( !function_exists('sort_number') )
        {
            function sort_number($a, $b)
            {
                if( $a["sort"]>=$b["sort"] ) $tmp=true;
                else $tmp=false;
                return $tmp;
            }
        }

        if( !function_exists('cmp') )
        {
            function cmp($a, $b)
            {
                if( $a["date_diff"]>=$b["date_diff"] ) $tmp=false;
                else $tmp=true;
                return $tmp;
            }
        }

        if( !function_exists('cmp2') )
        {
            function cmp2($a, $b)
            {
                if( $a["time"]>=$b["time"] ) $tmp=false;
                else $tmp=true;
                return $tmp;
            }

        }

        if( !empty($typeid2) && !empty($itemid2) )
        {
            $itemids=explode(",", $itemid2);
            foreach( $itemids as $itemid )
            {
                $query_search="SELECT relationid FROM ".TABLE_RELATIONS." WHERE (typeid1='".$typeid1."' AND itemid1='".$itemid1."' AND typeid2='".$typeid2."' AND itemid2='".$itemid."') OR (typeid2='".$typeid1."' AND itemid2='".$itemid1."' AND typeid1='".$typeid2."' AND itemid1='".$itemid."')";
                //print $query_search."<br /><br />";
                $results_search=$db->query($query_search);
                $count_search=$db->numrows($results_search);
                if( $count_search<=0 && is_numeric($itemid) )
                {
                    # exhibition and book
                        # if linking exhibition catalogue(book with books_catid 2 and 3) (typeid 12) to exhibition(typeid 4) add all catalogu exhibtions to this exhibition and also add this exhibition to all catalogu exhibitions
                        # also works other way around when linking exhibition to an exhibition catalogue(book with books_catid 2 and 3) and then link exhibtiion to catalogue exhibtiions
                    if( ( $typeid1==4 && $typeid2==12 ) || ( $typeid1==12 && $typeid2==4 ) )
                    {
                        if( $typeid1==12 && $typeid2==4 )
                        {
                            $exhibitionid=$itemid;
                            $bookid=$itemid1;
                        }
                        else
                        {
                            $exhibitionid=$itemid1;
                            $bookid=$itemid;
                        }
                        //print_r($itemids);
                        //print "<br />typeid1=".$typeid1."-typeid2=".$typeid2."-itemid=".$itemid."-itemid1=".$itemid1;
                        //print "<br />exhibitionid=".$exhibitionid." - bookid=".$bookid;
                        //exit('here');
                        $query_where_books=" WHERE id='".$bookid."' AND ( books_catid=2 OR books_catid=3 ) ";
                        $query_books=QUERIES::query_books($db,$query_where_books);
                        $results=$db->query($query_books['query']);
                        $count_book=$db->numrows($results);
                        if( $count_book )
                        {
                            $query_literature=QUERIES::query_books($db);
                            $results_lit_rel_exh=$db->query("SELECT
                                    typeid1,itemid1,typeid2,itemid2
                                FROM ".TABLE_RELATIONS." r
                                WHERE
                                    ( r.typeid1=12 AND r.itemid1='".$bookid."' AND r.typeid2=4 ) OR
                                    ( r.typeid2=12 AND r.itemid2='".$bookid."' AND r.typeid1=4 ) ");
                            $count_lit_rel_exh=$db->numrows($results_lit_rel_exh);
                            //print "<br />count_lit_rel_exh=".$count_lit_rel_exh;
                            while( $row_lit_rel_exh=$db->mysql_array($results_lit_rel_exh) )
                            {
                                $exhibitionid_rel=UTILS::get_relation_id($db,"4",$row_lit_rel_exh);
                                //print "<br /><br />".$exhibitionid."<br />";
                                # linking this exhibtiion to the book catalogu exhibitions
                                admin_utils::add_relation($db,4,$exhibitionid,4,$exhibitionid_rel,$_POST['relations_sort']);
                                # linking book catalogu exhibition  to this exhibtiion
                                admin_utils::add_relation($db,4,$exhibitionid_rel,12,$bookid,$_POST['relations_sort']);
                            }
                        }
                        //exit;
                    }
                    # END exhibition and book

                    # SORT
                    $tmp_key="";
                    # sales history
                    if( $typeid2==2 && $sort_relation=="by_date_sale" )
                    {
                        $results_sale_new=$db->query("SELECT saleName, saleDate FROM ".TABLE_SALEHISTORY." WHERE saleID='".$itemid."' LIMIT 1");
                        $row_sale_new=$db->mysql_array($results_sale_new);
                        $i=0;
                        $sale=array();
                        $results_sale_relations=$db->query("SELECT relationid,typeid1,itemid1,typeid2,itemid2,sort FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$typeid1."' AND itemid1='".$itemid1."' AND typeid2='".$typeid2."' ) OR ( typeid2='".$typeid1."' AND itemid2='".$itemid1."' AND typeid1='".$typeid2."' ) ORDER BY sort ASC, relationid DESC ");
                        while( $row_sale_relations=$db->mysql_array($results_sale_relations) )
                        {
                            $saleid=UTILS::get_relation_id($db,$typeid2,$row_sale_relations);
                            $results_sale=$db->query("SELECT saleName, saleDate FROM ".TABLE_SALEHISTORY." WHERE saleID='".$saleid."' LIMIT 1");
                            $row_sale=$db->mysql_array($results_sale);

                            $results_diff=$db->query("SELECT DATEDIFF('".$row_sale['saleDate']."','".$row_sale_new['saleDate']."') AS diff");
                            $row_diff=$db->mysql_array($results_diff);
                            $sale[$i]['saleid']=$saleid;
                            if( empty($row_diff['diff']) ) $sale[$i]['date_diff']=-1;
                            else $sale[$i]['date_diff']=$row_diff['diff'];
                            $sale[$i]['sort']=$row_sale_relations['sort'];
                            $sale[$i]['relationid']=$row_sale_relations['relationid'];
                            $sale[$i]['saleDate']=$row_sale['saleDate'];
                            $i++;
                        }
                        $sale[$i]['saleid']=$itemid;
                        $sale[$i]['date_diff']=0;
                        $sale[$i]['saleDate']=$row_sale_new['saleDate'];

                        usort($sale, "cmp");
                        foreach( $sale as $key => $value )
                        {
                            if( $itemid==$value['saleid'] ) $tmp_key=$key;
                            elseif( empty($tmp_key) )
                            {
                                $sale[$key]['sort']++;
                                $db->query("UPDATE ".TABLE_RELATIONS." SET sort='".$sale[$key]['sort']."' WHERE relationid='".$value['relationid']."' ");
                            }
                            elseif( !empty($tmp_key) )
                            {
                                $sale[$key]['sort']++;
                                $db->query("UPDATE ".TABLE_RELATIONS." SET sort='".$sale[$key]['sort']."' WHERE relationid='".$value['relationid']."' ");
                            }
                        }

                        if( empty($tmp_key) ) $sort=0;
                        elseif( isset($sale[($tmp_key+1)]) ) $sort=$sale[($tmp_key+1)]['sort']-1;
                        else $sort=$sale[($tmp_key-1)]['sort']+1;
                        $sort="'".$sort."'";
                        //print_r($sale);
                        //print "sort-".$sort."<br/>";
                        //print "tmp_key-".$tmp_key;

                    }
                    # END sales history
                    # Exhibition
                    elseif( $typeid2==4 && $sort_relation=="by_date_exhibitions" )
                    {
                        $results_exh_new=$db->query("SELECT title_original, date_from FROM ".TABLE_EXHIBITIONS." WHERE exID='".$itemid."' LIMIT 1");
                        $row_exh_new=$db->mysql_array($results_exh_new);
                        $i=0;
                        $exh=array();
                        $results_exh_relations=$db->query("SELECT relationid,typeid1,itemid1,typeid2,itemid2,sort FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$typeid1."' AND itemid1='".$itemid1."' AND typeid2='".$typeid2."' ) OR ( typeid2='".$typeid1."' AND itemid2='".$itemid1."' AND typeid1='".$typeid2."' ) ORDER BY sort ASC, relationid DESC ");
                        while( $row_exh_relations=$db->mysql_array($results_exh_relations) )
                        {
                            $exhibitionid=UTILS::get_relation_id($db,$typeid2,$row_exh_relations);
                            $results_exh=$db->query("SELECT title_original, date_from FROM ".TABLE_EXHIBITIONS." WHERE exID='".$exhibitionid."' LIMIT 1");
                            $row_exh=$db->mysql_array($results_exh);

                            $results_diff=$db->query("SELECT DATEDIFF('".$row_exh['date_from']."','".$row_exh_new['date_from']."') AS diff");
                            $row_diff=$db->mysql_array($results_diff);
                            $exh[$i]['exhibitionid']=$exhibitionid;
                            if( empty($row_diff['diff']) ) $exh[$i]['date_diff']=-1;
                            else $exh[$i]['date_diff']=$row_diff['diff'];
                            $exh[$i]['sort']=$row_exh_relations['sort'];
                            $exh[$i]['relationid']=$row_exh_relations['relationid'];
                            $exh[$i]['date_from']=$row_exh['date_from'];
                            //print $row_exh['title_original']." : ".$row_diff['diff']."<br />";
                            $i++;
                        }
                        $exh[$i]['exhibitionid']=$itemid;
                        $exh[$i]['date_diff']=0;
                        $exh[$i]['date_from']=$row_exh_new['date_from'];

                        usort($exh, "cmp");
                        foreach( $exh as $key => $value )
                        {
                            if( $itemid==$value['exhibitionid'] ) $tmp_key=$key;
                            elseif( empty($tmp_key) )
                            {
                                $sale[$key]['sort']++;
                                $db->query("UPDATE ".TABLE_RELATIONS." SET sort='".$sale[$key]['sort']."' WHERE relationid='".$value['relationid']."' ");
                            }
                            elseif( !empty($tmp_key) )
                            {
                                $exh[$key]['sort']++;
                                $db->query("UPDATE ".TABLE_RELATIONS." SET sort='".$exh[$key]['sort']."' WHERE relationid='".$value['relationid']."' ");
                            }
                        }

                        if( empty($tmp_key) ) $sort=0;
                        elseif( isset($exh[($tmp_key+1)]) ) $sort=$exh[($tmp_key+1)]['sort'];
                        else $sort=$exh[($tmp_key-1)]['sort']+1;
                        $sort="'".$sort."'";
                        //print_r($exh);
                        //print "sort-".$sort."<br/>";
                        //print "tmp_key-".$tmp_key;

                    }
                    # END Exhibition
                    elseif( !empty($sort_relation) )
                    {
                        $sort="'".$sort_relation."'";
                    }
                    else $sort="NULL";
                    # END SORT

                    # adding relation item
                    $sort_column=UTILS::get_sort_column($db,$typeid2);

                    $db->query("INSERT INTO ".TABLE_RELATIONS."(typeid1,itemid1,typeid2,itemid2,".$sort_column.",date_created) VALUES('".$typeid1."','".$itemid1."','".$typeid2."','".$itemid."',".$sort.",NOW()) ");
                    $relationid=$db->return_insert_id();
                    # END adding relation item

                    # sail history
                    if( $typeid1==2 && $typeid2==1 )
                    {
                        $sale_saleid=$itemid1;
                        $sale_paintid=$itemid2;
                    }
                    if( $typeid2==2 && $typeid1==1 )
                    {
                        $sale_saleid=$itemid2;
                        $sale_paintid=$itemid1;
                    }
                    if( !empty($sale_saleid) )
                    {
                        $db->query("UPDATE ".TABLE_SALEHISTORY." SET paintID='".$sale_paintid."' WHERE saleID='".$sale_saleid."' ");
                    }
                    # END sail history

                    # check if exhibtion has some relation
                    if( !empty($exhibitionid) )
                    {
                        $query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
                        $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
                        $results_exh=$db->query($query_exhibition['query']);
                        $row_exh=$db->mysql_array($results,0);

                        $values_exhibition_rel=array();
                        $values_exhibition_rel['exhibitionid']=$exhibitionid;
                        $values_exhibition_rel['src_guide']=$row_exh['src_guide'];
                        $relations=UTILS::check_relations($db,$values_exhibition_rel);
                        if( $relations )
                        {
                            $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=1 WHERE exID = '".$exhibitionid."'");
                        }
                        else
                        {
                            $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=0 WHERE exID = '".$exhibitionid."'");
                        }
                    }
                    # end

                    admin_utils::admin_log($db,1,10,$relationid);
                }
            }
        }

        $values_return=array();
        $values_return['relationid']=$relationid;
        return $values_return;

    } #END add relation

    public static function if_empty_get($db,$values)
    {
        if( empty($values['get']['p']) ) $values['get']['p']=1;
        else $values['get']['p']=preg_replace("/[^0-9]/", "", $values['get']['p']);
        if( $values['get']['sp']!='all' ) $values['get']['sp']=preg_replace("/[^0-9]/", "", $values['get']['sp']);

        ### if no page divideing variable spassed
        if( $values['get']['sp']=='all' ) $values['get']['sp']=1000000;
        if( empty($values['get']['sp']) )
        {
            $values['get']['p']=1;
            if( $values['sp']==0 )
            {
                $values['get']['sp']=SHOW_PER_PAGE;
                if( empty($_SESSION['sp']) ) $_SESSION['sp']=SHOW_PER_PAGE;
            }
            else
            {
                $values['get']['sp']=$values['sp'];
                if( empty($_SESSION['sp']) ) $_SESSION['sp']=$values['sp'];
            }
        }
        else
        {
            $_SESSION['sp']=$values['get']['sp'];
        }
        #end

        return $values['get'];
    }

    public static function admin_log($db,$actionid,$typeid,$itemid="",$success=1,$imageid="")
    {
        # $actionid
        # 1 - ADD
        # 2 - UPDATE
        # 3 - DELETE
        # 4 - SORT
        # 5 - LOG-IN
        # 6 - LOG-OUT

        # $typeid
        # 1 - home
        # 2 - admin users
        # 3 - paintings
        # 4 - paintings/types
        # 5 - paintings/categories
        # 6 - paintings/photos
        # 7 - paintings/mediums
        # 8 - paintings/colors
        # 9 - paintings/notes
        # 10 - relations
        # 11 - literature
        # 12 - literature/categories
        # 13 - literature/languages
        # 14 - literature/sort-publications
        # 15 - exhibitions
        # 16 - exhibitions guide
        # 17 - exhibitions/installations
        # 18 - locations
        # 19 - locations/cities
        # 20 - locations/countries
        # 21 - auctions-sales
        # 22 - auctions-sales/auctionhouse
        # 23 - auctions-sales/currencys
        # 24 - pages
        # 25 - images
        # 26 - videos
        # 27 - videos/subtitles
        # 28 - audios
        # 29 - news
        # 30 - microsites
        # 31 - microsites/images
        # 32 - microsites/versions
        # 33 - biography
        # 34 - biography/photos
        # 35 - quotes
        # 36 - quotes/categories

        # $success
        # 1 - success
        # 2 - fail

        $db->query("INSERT INTO ".TABLE_ADMIN_LOGS."(actionid,typeid,itemid,imageid,success,userid,date_created) VALUES('".$actionid."','".$typeid."','".$itemid."','".$imageid."','".$success."','".$_SESSION['userID']."',NOW()) ");
    }

    public static function recto_verso_location($db,$front_back,$typeid="",$locationid="")
    {
        if( $typeid==1 && $front_back == 1 ) $type="on backing board";
        elseif( $typeid==1 ) $type="on mount";
        elseif( $typeid==2 ) $type="on photograph";

        if( $locationid==1 ) $location="upper left";
        elseif( $locationid==2 ) $location="upper centre";
        elseif( $locationid==3 ) $location="upper right";
        elseif( $locationid==4 ) $location="centre left";
        elseif( $locationid==5 ) $location="centre middle";
        elseif( $locationid==6 ) $location="centre right";
        elseif( $locationid==7 ) $location="lower left";
        elseif( $locationid==8 ) $location="lower centre";
        elseif( $locationid==9 ) $location="lower right";

        # verso - front
        if( $front_back == 1 )
        {


        }
        # recto - back
        if( $front_back == 2 )
        {

        }

        if( !empty($typeid) ) $return=$type;
        elseif( !empty($locationid) ) $return=$location;

        return $return;
    }

    public static function char($text)
    {
        $text = htmlentities($text, ENT_NOQUOTES, "UTF-8");
        $text = htmlspecialchars_decode($text);
        return $text;
    }

        public static function prepare_for_csv($db,$variable,$options=array())
        {
            //$variable=str_replace (";", ".", $variable);
            //$variable=str_replace ("\r\n", "", $variable);
            //$variable=str_replace ("    ", "", $variable);

            if( $options['strip_tags'] ) $variable=strip_tags($variable);
            else
            {
                $variable=str_replace ("\n", "", $variable);
                $variable=str_replace ("\r", "", $variable);
                //$variable="\"".$variable."\"";
                $variable="~".$variable."~";
            }

            //$variable = html_entity_decode($variable,ENT_NOQUOTES);
            //$variable = admin_utils::char($variable);
            //$variable = htmlentities($variable, ENT_QUOTES, "UTF-8");

            return $variable;
        }

    function str_word_count_unique($str = '', $format = 0, $charlist = NULL)
    {
        switch ($format) {
            case 0:
                return count(array_unique(str_word_count($str, 1, $charlist)));
                break;
            default:
                return array_unique(str_word_count($str, $format, $charlist));
                break;
        }
    }

    public static function order_pages($db,$pages_array_all,$options=array())
    {
        $pages="";

        $pages_array_all=array_unique($pages_array_all);

        if( is_array($pages_array_all) )
        {
            //print_r($pages_array_all);

            $pages_array_range=array();
            foreach ($pages_array_all as $key => $page) {
                //print $page."<br />";
                preg_match("/^[0-9]*-[0-9]*/i", $page, $matches);
                if( !empty($matches[0]) )
                {
                    unset($pages_array_all[$key]);
                    $pages_array_range=array_merge($pages_array_range,$matches);
                }
            }

            //print_r($pages_array_all);
            //print_r($pages_array_range);

            //print "<br />";
            $i=0;
            $page_min_array=array();
            if( is_array($pages_array_range) )
            {
                foreach ($pages_array_range as $key1 => $page1) {
                    $pages_range1=preg_split('@-@', $page1, NULL, PREG_SPLIT_NO_EMPTY);
                    $range1 = range($pages_range1[0], $pages_range1[1]);

                    foreach ($pages_array_range as $key2 => $page2) {
                        if( $key1!=$key2 )
                        {
                            $pages_range2=preg_split('@-@', $page2, NULL, PREG_SPLIT_NO_EMPTY);

                            $range2 = range($pages_range2[0], $pages_range2[1]);
                            if( !in_array($pages_range1[0], $range2) ){
                                //print "unset-min=".$pages_range1[0]."<br />";
                                //print "key=".$key1." | number=".$pages_range1[0]."<br />";
                                //unset($pages_array_range[$key1]);
                            }
                            else
                            {
                                //print "<strong>key=".$key1." | number=".$pages_range1[0]."-".$pages_range1[1]."</strong><br />";
                                //print "<strong>key=".$key2." | number=".$pages_range2[0]."-".$pages_range2[1]."</strong><br />";
                                $new_range=array_merge($range1,$range2);
                                //print_r($new_range);
                                //print_r($range1);
                                //print_r($range2);
                                //print min($new_range)."-".max($new_range)."<br />";
                                $pages_array_range[$key1]=min($new_range)."-".max($new_range);
                                unset($pages_array_range[$key2]);
                            }
                        }
                    }



                    $i++;
                }

            }
            //print_r($pages_array_all);

            $pages_array_all=array_merge($pages_array_all,$pages_array_range);

            asort($pages_array_all,SORT_NUMERIC);

            //$pages_array_all=array_values($pages_array_all);

            //print_r($pages_array_all);

            $i=0;
            foreach ($pages_array_all as  $page) {
                if( !empty($page) )
                {
                    if( $i>0 ) $pages.=", ";
                    $pages.=$page;
                }
                $i++;
            }

            if( !empty($options['referece_text']) && !empty($pages) ) $pages=$options['referece_text'].$pages;
            if( !empty($options['referece_text2']) && !empty($pages) ) $pages=$pages.$options['referece_text2'];

        }

        return $pages;
    }

    public static function order_literature_pages($db,$relationid_lit,$options=array())
    {
        $query_rel_lit_where=" WHERE relationid_lit='".$relationid_lit."' ";
        $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
        $results_rel_lit=$db->query($query_rel_lit['query']);
        $count=$db->numrows($results_rel_lit);
        if( $count )
        {
            $row=$db->mysql_array($results_rel_lit);

            //$search = array("."," ", ",(", "(", "),",")");
            //$replace = array(",","", ",", "", "," ,"");

            $search = array("."," ");
            $replace = array(",","");

            $illustrated = str_replace($search, $replace, $row['illustrated']);
            $illustrated_bw = str_replace($search, $replace, $row['illustrated_bw']);

            $mentioned = str_replace($search, $replace, $row['mentioned']);
            $discussed = str_replace($search, $replace, $row['discussed']);

            //print "--".$mentioned."--";

            /*
            $show_illustrated=array();
            $show_bw=array();
            $show_mentioned=array();
            $show_discussed=array();
            if( $row['show_illustrated'] ) $show_illustrated[]="col";
            if( $row['show_bw'] ) $show_bw[]="b/w";
            if( $row['show_mentioned'] ) $show_mentioned[]="";
            if( $row['show_discussed'] ) $show_discussed[]="";
            */

            $illustrated_pages=preg_split('@,@', $illustrated, NULL, PREG_SPLIT_NO_EMPTY);
            $illustrated_bw_pages=preg_split('@,@', $illustrated_bw, NULL, PREG_SPLIT_NO_EMPTY);

            $mentioned_pages=preg_split('@,@', $mentioned, NULL, PREG_SPLIT_NO_EMPTY);
            $discussed_pages=preg_split('@,@', $discussed, NULL, PREG_SPLIT_NO_EMPTY);

            //print_r($illustrated_pages);
            //print_r($illustrated_bw_pages);
            //print_r($mentioned_pages);
            //print_r($discussed_pages);

            $pages_array_all_ill_col = $illustrated_pages;
            $pages_array_all_ill_bw = $illustrated_bw_pages;
            $pages_array_all_ref = array_merge($mentioned_pages, $discussed_pages);

            //print_r($pages_array_all);

            $options_ref=array();
            $options_ref['referece_text']="ref. p.";
            $pages_reference=admin_utils::order_pages($db,$pages_array_all_ref,$options_ref);
            if( empty($pages_reference) && ( $row['show_mentioned'] || $row['show_discussed'] ) ) $pages_reference="ref.";

            $options_ill_col=array();
            $options_ill_col['referece_text']="ill. p.";
            $options_ill_col['referece_text2']=" (col)";
            $pages_illustration_col=admin_utils::order_pages($db,$pages_array_all_ill_col,$options_ill_col);

            $options_ill_bw=array();
            $options_ill_bw['referece_text']="ill. p.";
            $options_ill_bw['referece_text2']=" (b/w)";
            $pages_illustration_bw=admin_utils::order_pages($db,$pages_array_all_ill_bw,$options_ill_bw);

            $pages_illustration="";
            $pages_illustration.=$pages_illustration_col;
            if( !empty($pages_illustration_col) && !empty($pages_illustration_bw) ) $pages_illustration.=", ";
            $pages_illustration.=$pages_illustration_bw;

            if( empty($pages_illustration_col) && $row['show_illustrated'] ) $pages_illustration="ill. col";
            if( empty($pages_illustration_bw) && $row['show_bw'] ) $pages_illustration="ill. b/w";
            //print_r($pages_array);
            //print $pages;

            $pages=array();
            $pages['pages_reference']=$pages_reference;
            $pages['pages_illustration']=$pages_illustration;

            //$db->query("UPDATE ".TABLE_RELATIONS_LITERATURE." SET pages='".$pages."' WHERE relationid_lit='".$relationid_lit."' LIMIT 1 ");

            if( !empty($options['relationid']) && !$options['debug'] )
            {
                $db->query("UPDATE ".TABLE_RELATIONS." SET pages_reference='".$pages_reference."', pages_illustration='".$pages_illustration."' WHERE relationid='".$options['relationid']."' LIMIT 1 ");
            }

            return $pages;

        }
    }

    public static function html2plaintext($db,$html) {

        $pattern = '~[^a-zA-Z0-9]+~';

        $html=preg_replace( $pattern, " ", rtrim(html_entity_decode(strip_tags($html))) );

       return $html;
    }


}

?>
