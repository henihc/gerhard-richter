<?php
session_start();

require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_search.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/languages/1-en-english.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/back-soon.php");

require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/utils.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/colors.inc.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/colors_convert.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/color.php");

# allowed ips
$ip=UTILS::get_ip();

if( !IS_DEV &&

    # 29-35 Lexington Street
    $ip!="212.140.254.194" &&
    
    # Christine
    $ip!="83.219.41.114" &&

    #Headchannel
    $ip!="84.10.30.182" &&
    $ip!="85.14.115.182"
    

){
    //print "not allowed";
    if( !$_SESSION['admin']['valid'] ) UTILS::redirect('/');
    //exit;
}
else
{
    //print "allowed";
    //exit;
}
# END allowed ips

class admin_html
{
    public function metainfo($options=array(),$google_analytics=1,$robots=true,$charset=DB_ENCODEING)
    {
        print "<!DOCTYPE html> \n";
        print "<html lang='en'>\n";
        print "<head>\n";
        print "  <meta http-equiv='Content-Type' content='text/html; charset=".strtolower($charset)."' />\n";
        print "  <meta name='copyright' content='copyright ".date("Y")." ".HTTP_SERVER."' />\n";
        if( (SERVER==DOMAIN || SERVER==WWW_DOMAIN) && $robots ) $robots_content="follow, index";
        else $robots_content="nofollow, noindex";
        print "  <meta name='robots' content='".$robots_content."' />\n";
        print "  <meta name='keywords' content='' />\n";
        print "  <meta name='description' content='' />\n";
        print "  <title>".$this->title."</title>\n";
        print "  <link rel='icon' href='".HTTP_SERVER."/favicon.png' type='image/png' />\n";
        print "  <link rel='apple-touch-icon' href='".HTTP_SERVER."/favicon.png' />\n";
        print "  <!--[if IE]><link rel='SHORTCUT ICON' href='".HTTP_SERVER."/favicon.png'/><![endif]--><!-- Internet Explorer-->";
        ### CSS files
        for ($i=0;$i<sizeof($this->css);$i++)
        {
            print "  <link href='".$this->css[$i]."' rel='stylesheet' type='text/css' media='screen' />\n";
        }
        #end
        ### JS files
        for ($i=0;$i<sizeof($this->js);$i++)
        {
            print "  <script src='".$this->js[$i]."' type='text/javascript'></script>\n";
        }
        #end

        print "</head>\n";
        ### adding BODY ONLOAD JS functions
        if (sizeof($this->onload)>0) $onload_tmp=" onload=\"";
        for ($i=0;$i<sizeof($this->onload);$i++)
        {
            $onload_tmp.=$this->onload[$i];
        }
        #end
        ### adding BODY ONUNLOAD JS functions
        if(sizeof($this->onload)>0)$onload_tmp.="\"";
        if (sizeof($this->onunload)>0) $onunload_tmp=" onunload='";
        for ($i=0;$i<sizeof($this->onunload);$i++)
        {
            $onunload_tmp.=$this->onunload[$i];
        }
        if(sizeof($this->onunload)>0)$onunload_tmp.="'";
        #end
        print "<body".$onload_tmp.$onunload_tmp." class='".$options['class']['body-login']." '>\n";

    }

    public function foot($db="")
    {
        ### adding JS files
        for ($i=0;$i<sizeof($this->js_footer);$i++)
        {
            print "\t<script src='".$this->js_footer[$i]."' type='text/javascript'></script>\n";
        }
        #end

        print "</body>\n";
        print "</html>";

        unset($db);
    }

    public static function admin_sign_in()
    {
        $html = new admin_html;
        $html->title="Gerhard Richter - Admin";
        $html->css[]="/admin/css/admin.css";
        $html->js[]="/admin/js/common.js";

        //$html->div_admin_container_start();
        //$html->div_admin_wrapper_start();
        $form_html="\t<div id='div_admin_sign_in' >\n";
        $form_html.="\t<form action='' method='post' >\n";
        $form_html.="\t<center><table class='table-sign-in' >";
        $form_html.="<tr><td>Login</td><td><input type='text' name='login' /></td></tr>";
        $form_html.="<tr><td>Password</td><td><input type='password' name='passwd' /></td></tr>";
        $form_html.="<tr><td></td><td><input type='submit' name='submit' value='Login' /></td></tr>";
        $form_html.="</table>";
        $form_html.=$wrong."</center>\n";
        $form_html.="\t</form>\n";
        $form_html.="\t</div>\n";
        //$html->div_admin_wrapper_end();
        //$html->div_admin_container_end();

        if( isset($_POST['login']) && isset($_POST['passwd']) )
        {
            $db=new dbCLASS;

            $_POST=$db->db_prepare_input($_POST);

            $results=$db->query("select userID,login,passwd,email from ".TABLE_ADMIN." where login = '".$_POST['login']."' ");
            $row=$db->mysql_array($results);

            #if password or username wrong
            if( !UTILS::validate_password($_POST['passwd'],$row['passwd']) )
            {
                admin_utils::admin_log($db,5,1,"",2);
                $wrong="Login or/and password wrong";

                $options_metainfo=array();
                $options_metainfo['class']['body-login']="body-login";
                $html->metainfo($options_metainfo);
                print $form_html;
                $html->foot();
                exit;
            }
            #if valid email and password
            else
            {
                $_SESSION['userID']=$row['userID'];
                admin_utils::admin_log($db,5,1);

                //if( stripos($_SERVER['REQUEST_URI'],"sign") ) $url="/admin";
                //else
                $url=getenv('HTTP_REFERER');
                UTILS::redirect($url);
            }

        }
        elseif( !$_SESSION['userID'] )
        {
            $options_metainfo=array();
            $options_metainfo['class']['body-login']="body-login";
            $html->metainfo($options_metainfo);
            print $form_html;
            $html->foot();
            exit;
        }

    }

    public static function div_admin_container_start($db="")
    {
        print "<div class='div-admin-container'>";
    }

    public static function div_admin_container_end($db="")
    {
        print "</div>";
    }

    public static function div_admin_wrapper_start($db="")
    {
        print "<div class='div-admin-wrapper'>";
    }

    public static function div_admin_wrapper_end($db="")
    {
        print "</div>";
    }

    public static function div_admin_header_start($db="")
    {
        print "<div class='div-admin-header'>";
    }

    public static function div_admin_header_end($db="")
    {
        print "</div>";
    }

    public function div_admin_header_content_start($db=0,$class=array())
    {
        print "\t<div class='div-admin-header-content'>\n";
        print "<div class='div-admin-header-content-left'>\n";
        print "<p class='p-header-richter-cms'>";
        print "<a href='/' title='Gerhard-Richter.com CMS'>Gerhard-Richter.com CMS</a>";
        print "</p>";
        print "</div>\n";
        print "<div class='div-admin-header-content-right'>\n";
        print "<a href='/admin/sign_out' title='Logout' >Logout</a>";
        print "</div>\n";
    }

    public function div_admin_header_content_end($db=0,$class=array())
    {
        print "<div class='clearer'></div>";
        print "\t</div>\n";
    }

    public static function div_admin_body_start($db="")
    {
        print "<div class='div-admin-body'>";
    }

    public static function div_admin_body_end($db="")
    {
        print "</div>";
    }

    public static function div_admin_body_content_start($db="")
    {
        print "<div class='div-admin-body-content'>";
    }

    public static function div_admin_body_content_end($db="")
    {
        print "</div>";
    }

    public static function div_admin_footer_start($db="")
    {
        print "<div class='div-admin-foter'>";
    }

    public static function div_admin_footer_end($db="")
    {
        print "</div>";
    }


    function menu_admin($db,$id,$class=array(),$values=array())
    {

        # Main menu admin home
        $menu_items['main_menu']['title']="Main menu";$menu_items['main_menu']['url']="/admin";$menu_items['main_menu']['class']="main_menu";

        # System
        /*
        $menu_items['system']['title']="System";
        $menu_items['system']['url']="/admin/system/configuration";
        $menu_items['system']['class']="system";
        $menu_items['system']['sub-menu']['configuration']['title']="Configuration";
        $menu_items['system']['sub-menu']['configuration']['url']="/admin/system/configuration";
        $menu_items['system']['sub-menu']['configuration']['class']="configuration";
        */

        # Paintings
        $menu_items['paintings']['title']="Paintings";
        $menu_items['paintings']['url']="#";
        $menu_items['paintings']['class']="paintings";
        $menu_items['paintings']['sub-menu']['add_painting']['title']="Add painting";
        $menu_items['paintings']['sub-menu']['add_painting']['url']="/admin/paintings/edit";
        $menu_items['paintings']['sub-menu']['add_painting']['class']="add_painting";
        # Photos
        $menu_items['paintings']['sub-menu']['photos']['title']="Photos";
        $menu_items['paintings']['sub-menu']['photos']['url']="/admin/paintings/photos";
        $menu_items['paintings']['sub-menu']['photos']['class']="photos";
        $menu_items['paintings']['sub-menu']['add_photo']['title']="Add photo";
        $menu_items['paintings']['sub-menu']['add_photo']['url']="/admin/paintings/photos/edit";
        $menu_items['paintings']['sub-menu']['add_photo']['class']="add_photo";
        # Types
        $menu_items['paintings']['sub-menu']['types']['title']="Artwork types";
        $menu_items['paintings']['sub-menu']['types']['url']="/admin/paintings/types";
        $menu_items['paintings']['sub-menu']['types']['class']="types";
        $menu_items['paintings']['sub-menu']['add_type']['title']="Add artwork type";
        $menu_items['paintings']['sub-menu']['add_type']['url']="/admin/paintings/types/edit";
        $menu_items['paintings']['sub-menu']['add_type']['class']="add_type";
        # Categories
        $menu_items['paintings']['sub-menu']['categories']['title']="Categories";
        $menu_items['paintings']['sub-menu']['categories']['url']="/admin/paintings/categories";
        $menu_items['paintings']['sub-menu']['categories']['class']="categories";
        $menu_items['paintings']['sub-menu']['add_category']['title']="Add category";
        $menu_items['paintings']['sub-menu']['add_category']['url']="/admin/paintings/categories/edit";
        $menu_items['paintings']['sub-menu']['add_category']['class']="add_category";
        # Mediums
        $menu_items['paintings']['sub-menu']['mediums']['title']="Mediums";
        $menu_items['paintings']['sub-menu']['mediums']['url']="/admin/paintings/mediums";
        $menu_items['paintings']['sub-menu']['mediums']['class']="mediums";
        $menu_items['paintings']['sub-menu']['add_medium']['title']="Add medium";
        $menu_items['paintings']['sub-menu']['add_medium']['url']="/admin/paintings/mediums/edit";
        $menu_items['paintings']['sub-menu']['add_medium']['class']="add_medium";
        # Colors
        $menu_items['paintings']['sub-menu']['colors']['title']="Colors";
        $menu_items['paintings']['sub-menu']['colors']['url']="/admin/paintings/colors";
        $menu_items['paintings']['sub-menu']['colors']['class']="colors";
        $menu_items['paintings']['sub-menu']['add_color']['title']="Add color";
        $menu_items['paintings']['sub-menu']['add_color']['url']="/admin/paintings/colors/edit";
        $menu_items['paintings']['sub-menu']['add_color']['class']="add_color";
        # Notes images
        $menu_items['paintings']['sub-menu']['notes_images']['title']="Notes images";
        $menu_items['paintings']['sub-menu']['notes_images']['url']="/admin/paintings/notes";
        $menu_items['paintings']['sub-menu']['notes_images']['class']="notes_images";
        $menu_items['paintings']['sub-menu']['add_notes_image']['title']="Add note image";
        $menu_items['paintings']['sub-menu']['add_notes_image']['url']="/admin/paintings/notes/edit";
        $menu_items['paintings']['sub-menu']['add_notes_image']['class']="add_notes_image";

        # SEARCH
        $menu_items['search']['title']="Search";
        $menu_items['search']['url']="/admin/search";
        $menu_items['search']['class']="search";

        # Microsites
        $menu_items['microsites']['title']="Microsites";
        $menu_items['microsites']['url']="/admin/microsites";
        $menu_items['microsites']['class']="microsites";
        # add Microsite
        $menu_items['microsites']['sub-menu']['add_microsite']['title']="Add microsite";
        $menu_items['microsites']['sub-menu']['add_microsite']['url']="/admin/microsites/edit";
        $menu_items['microsites']['sub-menu']['add_microsite']['class']="add_microsite";
        # Microsite versions
        $menu_items['microsites']['sub-menu']['microsite_versions']['title']="Microsite versions";
        $menu_items['microsites']['sub-menu']['microsite_versions']['url']="/admin/microsites/versions";
        $menu_items['microsites']['sub-menu']['microsite_versions']['class']="microsite_versions";
        /*
        # Add version
        $menu_items['paintings']['sub-menu']['add_version']['title']="Add version";
        $menu_items['paintings']['sub-menu']['add_version']['url']="/admin/microsites/versions/edit";
        $menu_items['paintings']['sub-menu']['add_version']['class']="add_version";
        */

        # Biography
        $menu_items['biography']['title']="Biography";
        $menu_items['biography']['url']="/admin/biography";
        $menu_items['biography']['class']="biography";
        # add biography
        $menu_items['biography']['sub-menu']['add_biography']['title']="Add biography";
        $menu_items['biography']['sub-menu']['add_biography']['url']="/admin/biography/edit";
        $menu_items['biography']['sub-menu']['add_biography']['class']="add_biography";
        # Photos
        $menu_items['biography']['sub-menu']['photos']['title']="Photos";
        $menu_items['biography']['sub-menu']['photos']['url']="/admin/photos";
        $menu_items['biography']['sub-menu']['photos']['class']="photos";
        # add Photo
        $menu_items['biography']['sub-menu']['add_photo']['title']="Add photo";
        $menu_items['biography']['sub-menu']['add_photo']['url']="/admin/photos/edit";
        $menu_items['biography']['sub-menu']['add_photo']['class']="add_photo";

        # Quotes
        $menu_items['quotes']['title']="Quotes";
        $menu_items['quotes']['url']="/admin/quotes";
        $menu_items['quotes']['class']="quotes";
        # add Quote
        $menu_items['quotes']['sub-menu']['add_quote']['title']="Add quote";
        $menu_items['quotes']['sub-menu']['add_quote']['url']="/admin/quotes/edit";
        $menu_items['quotes']['sub-menu']['add_quote']['class']="add_quote";
        # Quotes categories
        $menu_items['quotes']['sub-menu']['quotes_categories']['title']="Quotes categories";
        $menu_items['quotes']['sub-menu']['quotes_categories']['url']="/admin/quotes/categories";
        $menu_items['quotes']['sub-menu']['quotes_categories']['class']="quotes_categories";
        # Add category
        $menu_items['quotes']['sub-menu']['add_quotes_category']['title']="Add category";
        $menu_items['quotes']['sub-menu']['add_quotes_category']['url']="/admin/quotes/categories/edit";
        $menu_items['quotes']['sub-menu']['add_quotes_category']['class']="add_quotes_category";


        # Exhibitions
        $menu_items['exhibitions']['title']="Exhibitions";
        $menu_items['exhibitions']['url']="/admin/exhibitions";
        $menu_items['exhibitions']['class']="exhibitions";
        # add Exhibition
        $menu_items['exhibitions']['sub-menu']['add_exhibition']['title']="Add exhibition";
        $menu_items['exhibitions']['sub-menu']['add_exhibition']['url']="/admin/exhibitions/edit";
        $menu_items['exhibitions']['sub-menu']['add_exhibition']['class']="add_exhibition";
        # Installations
        $menu_items['exhibitions']['sub-menu']['installations']['title']="Installations";
        $menu_items['exhibitions']['sub-menu']['installations']['url']="/admin/exhibitions/installations";
        $menu_items['exhibitions']['sub-menu']['installations']['class']="installations";
        # add Installation
        $menu_items['exhibitions']['sub-menu']['add_installation']['title']="Add installation";
        $menu_items['exhibitions']['sub-menu']['add_installation']['url']="/admin/exhibitions/installations/edit";
        $menu_items['exhibitions']['sub-menu']['add_installation']['class']="add_installations";

        # Literature
        $menu_items['literature']['title']="Literature";
        $menu_items['literature']['url']="/admin/books";
        $menu_items['literature']['class']="literature";
        # add Literature
        $menu_items['literature']['sub-menu']['add_literature']['title']="Add literature";
        $menu_items['literature']['sub-menu']['add_literature']['url']="/admin/books/edit";
        $menu_items['literature']['sub-menu']['add_literature']['class']="add_literature";
        # Categories
        $menu_items['literature']['sub-menu']['categories']['title']="Categories";
        $menu_items['literature']['sub-menu']['categories']['url']="/admin/books/categories";
        $menu_items['literature']['sub-menu']['categories']['class']="categories";
        # Add categories
        $menu_items['literature']['sub-menu']['add_category']['title']="Add category";
        $menu_items['literature']['sub-menu']['add_category']['url']="/admin/books/categories/edit";
        $menu_items['literature']['sub-menu']['add_category']['class']="add_category";
        # Languages
        $menu_items['literature']['sub-menu']['languages']['title']="Languages";
        $menu_items['literature']['sub-menu']['languages']['url']="/admin/books/languages";
        $menu_items['literature']['sub-menu']['languages']['class']="languages";
        # Add language
        $menu_items['literature']['sub-menu']['add_language']['title']="Add language";
        $menu_items['literature']['sub-menu']['add_language']['url']="/admin/books/languages/edit";
        $menu_items['literature']['sub-menu']['add_language']['class']="add_language";
        # Sort featured titles
        $menu_items['literature']['sub-menu']['sort_featured_titles']['title']="Sort featured titles";
        $menu_items['literature']['sub-menu']['sort_featured_titles']['url']="/admin/books/edit/sort";
        $menu_items['literature']['sub-menu']['sort_featured_titles']['class']="sort_featured_titles";
        # Sort news publications
        $menu_items['literature']['sub-menu']['sort_news_publications']['title']="Sort news publications";
        $menu_items['literature']['sub-menu']['sort_news_publications']['url']="/admin/books/sort-publications";
        $menu_items['literature']['sub-menu']['sort_news_publications']['class']="sort_news_publications";
        # Broken links
        $menu_items['literature']['sub-menu']['broken_links']['title']="Broken links";
        $menu_items['literature']['sub-menu']['broken_links']['url']="/admin/books/broken-links";
        $menu_items['literature']['sub-menu']['broken_links']['class']="broken_links";
        # check links now
        $menu_items['literature']['sub-menu']['check_links_now']['title']="Check links now";
        $menu_items['literature']['sub-menu']['check_links_now']['url']="/admin/books/broken-links/check_link/check_link.php?redirect=1";
        $menu_items['literature']['sub-menu']['check_links_now']['class']="check_links_now";

        # Media
        $menu_items['media']['title']="Media";
        $menu_items['media']['url']="/admin/videos";
        $menu_items['media']['class']="media";
        # Videos
        $menu_items['media']['sub-menu']['videos']['title']="Videos";
        $menu_items['media']['sub-menu']['videos']['url']="/admin/videos";
        $menu_items['media']['sub-menu']['videos']['class']="videos";
        # add Video
        $menu_items['media']['sub-menu']['add_video']['title']="Add video";
        $menu_items['media']['sub-menu']['add_video']['url']="/admin/videos/edit";
        $menu_items['media']['sub-menu']['add_video']['class']="add_video";
        # Videos categories
        $menu_items['media']['sub-menu']['videos_categories']['title']="Video categories";
        $menu_items['media']['sub-menu']['videos_categories']['url']="/admin/videos/categories";
        $menu_items['media']['sub-menu']['videos_categories']['class']="videos_categories";
        # Add category
        $menu_items['media']['sub-menu']['add_videos_category']['title']="Add video category";
        $menu_items['media']['sub-menu']['add_videos_category']['url']="/admin/videos/categories/edit";
        $menu_items['media']['sub-menu']['add_videos_category']['class']="add_videos_category";
        # Audios
        $menu_items['media']['sub-menu']['audios']['title']="Audios";
        $menu_items['media']['sub-menu']['audios']['url']="/admin/audio";
        $menu_items['media']['sub-menu']['audios']['class']="audios";
        # add Audio
        $menu_items['media']['sub-menu']['add_audio']['title']="Add audio";
        $menu_items['media']['sub-menu']['add_audio']['url']="/admin/audio/edit";
        $menu_items['media']['sub-menu']['add_audio']['class']="add_audio";

        # News
        $menu_items['news']['title']="News";
        $menu_items['news']['url']="#";
        $menu_items['news']['class']="news";
        # Talks
        $menu_items['news']['sub-menu']['talks']['title']="Talks";
        $menu_items['news']['sub-menu']['talks']['url']="/admin/news";
        $menu_items['news']['sub-menu']['talks']['class']="talks";
        # add Talk
        $menu_items['news']['sub-menu']['add_talk']['title']="Add talk";
        $menu_items['news']['sub-menu']['add_talk']['url']="/admin/news/edit";
        $menu_items['news']['sub-menu']['add_talk']['class']="talks";

        # Pages
        $menu_items['pages']['title']="Pages";
        $menu_items['pages']['url']="#";
        $menu_items['pages']['class']="pages";
        # Credits page
        $menu_items['pages']['sub-menu']['credits']['title']="Credits";
        $menu_items['pages']['sub-menu']['credits']['url']="/admin/pages/edit/?textid=1";
        $menu_items['pages']['sub-menu']['credits']['class']="pageid_1";
        # Disclaimer page
        $menu_items['pages']['sub-menu']['disclaimer']['title']="Disclaimer";
        $menu_items['pages']['sub-menu']['disclaimer']['url']="/admin/pages/edit/?textid=2";
        $menu_items['pages']['sub-menu']['disclaimer']['class']="pageid_2";
        # Chronology page
        $menu_items['pages']['sub-menu']['chronology']['title']="Chronology";
        $menu_items['pages']['sub-menu']['chronology']['url']="/admin/pages/edit/?textid=3";
        $menu_items['pages']['sub-menu']['chronology']['class']="pageid_3";
        # Links/dealers page
        $menu_items['pages']['sub-menu']['links_dealers']['title']="Links/dealers";
        $menu_items['pages']['sub-menu']['links_dealers']['url']="/admin/pages/edit/?textid=4";
        $menu_items['pages']['sub-menu']['links_dealers']['class']="pageid_4";
        # Home page
        $menu_items['pages']['sub-menu']['home']['title']="Home";
        $menu_items['pages']['sub-menu']['home']['url']="/admin/pages/edit/?textid=5";
        $menu_items['pages']['sub-menu']['home']['class']="pageid_5";
        # Locations
        $menu_items['locations']['title']="Locations";
        $menu_items['locations']['url']="/admin/locations";
        $menu_items['locations']['class']="locations";
        # add Literature
        $menu_items['locations']['sub-menu']['add_location']['title']="Add location";
        $menu_items['locations']['sub-menu']['add_location']['url']="/admin/locations/edit/?typeid=1";
        $menu_items['locations']['sub-menu']['add_location']['class']="add_location";
        # Cities
        $menu_items['locations']['sub-menu']['cities']['title']="Cities";
        $menu_items['locations']['sub-menu']['cities']['url']="/admin/locations/cities";
        $menu_items['locations']['sub-menu']['cities']['class']="cities";
        # add city
        /*
        $menu_items['locations']['sub-menu']['add_city']['title']="Add city";
        $menu_items['locations']['sub-menu']['add_city']['url']="/admin/locations/cities/edit/?typeid=2";
        $menu_items['locations']['sub-menu']['add_city']['class']="add_city";
        */
        # Countries
        $menu_items['locations']['sub-menu']['countries']['title']="Countries";
        $menu_items['locations']['sub-menu']['countries']['url']="/admin/locations/countries";
        $menu_items['locations']['sub-menu']['countries']['class']="countries";
        # add country
        /*
        $menu_items['locations']['sub-menu']['add_country']['title']="Add Country";
        $menu_items['locations']['sub-menu']['add_country']['url']="/admin/locations/countries/edit/?typeid=3";
        $menu_items['locations']['sub-menu']['add_country']['class']="add_country";
        */

        # Auctions / Sales
        $menu_items['auctions_sales']['title']="Auctions / Sales";
        $menu_items['auctions_sales']['url']="/admin/auctions-sales/saleHistory";
        $menu_items['auctions_sales']['class']="auctions_sales";
        # add Sale
        $menu_items['auctions_sales']['sub-menu']['add_sale']['title']="Add sale";
        $menu_items['auctions_sales']['sub-menu']['add_sale']['url']="/admin/auctions-sales/saleHistory/edit";
        $menu_items['auctions_sales']['sub-menu']['add_sale']['class']="add_sale";
        # Auction houses
        $menu_items['auctions_sales']['sub-menu']['auction_houses']['title']="Auction houses";
        $menu_items['auctions_sales']['sub-menu']['auction_houses']['url']="/admin/auctions-sales/auctionhouse";
        $menu_items['auctions_sales']['sub-menu']['auction_houses']['class']="auction_houses";
        # add auction house
        $menu_items['auctions_sales']['sub-menu']['add_auction_house']['title']="Add auction house";
        $menu_items['auctions_sales']['sub-menu']['add_auction_house']['url']="/admin/auctions-sales/auctionhouse/edit";
        $menu_items['auctions_sales']['sub-menu']['add_auction_house']['class']="add_auction_house";
        # Countries
        $menu_items['auctions_sales']['sub-menu']['currencies']['title']="Currencies";
        $menu_items['auctions_sales']['sub-menu']['currencies']['url']="/admin/auctions-sales/currencys";
        $menu_items['auctions_sales']['sub-menu']['currencies']['class']="currencies";
        # add country
        $menu_items['auctions_sales']['sub-menu']['add_currency']['title']="Add currency";
        $menu_items['auctions_sales']['sub-menu']['add_currency']['url']="/admin/auctions-sales/currencys/add";
        $menu_items['auctions_sales']['sub-menu']['add_currency']['class']="add_currency";

        # Images
        $menu_items['images']['title']="Images";
        $menu_items['images']['url']="/admin/images";
        $menu_items['images']['class']="images";

        # MANAGEMENT
        $menu_items['management']['title']="Management";
        $menu_items['management']['url']="#";
        $menu_items['management']['class']="management";
        $menu_items['management']['sub-menu']['users']['title']="Users";
        $menu_items['management']['sub-menu']['users']['url']="/admin/management/users";
        $menu_items['management']['sub-menu']['users']['class']="users";
        $menu_items['management']['sub-menu']['add_user']['title']="Add user";
        $menu_items['management']['sub-menu']['add_user']['url']="/admin/management/users/add";
        $menu_items['management']['sub-menu']['add_user']['class']="add_users";








        print "\t<div id='cssmenu'>\n";
        print "\t<ul id='".$id."' class='".$class['admin-nav']." menu' >\n";
        foreach( $menu_items as $key => $value )
        {
            if( is_int($key) && $class['admin-nav']!="admin-nav-inline" )
            {
                //print "\t<li class='li-divider' style='".$value['style']."'>";
                //print $value['title'];
                //print "</li>\n";
            }
            elseif( $key=="main_menu" && $values['is_admin_root'] )
            {
            }
            else
            {

                if( $this->menu_admin_selected==$value['class'] ) $selected="menu_selected";
                else $selected="";

                $count_sub=count($value['sub-menu']);
                if( $count_sub>0 )
                {
                    $class_has_sub="has-sub";
                }
                else
                {
                    $class_has_sub="";
                }


                //if( !empty($value['target']) ) $target="target=\"".$value['target']."\"";
                //else $target="";

                print "\t<li class='$class_has_sub $selected'>";
                print"<a href='".$value['url']."' class='".$value['class']." a-admin-menu-main' title='".$value['title']."'>";
                print "<span>".$value['title']."</span>";
                print "</a>\n";

                if( $count_sub>0 )
                {
                    print "\t<ul class='ul-sub-menu'>\n";
                    foreach( $value['sub-menu'] as $value2 )
                    {
                        if( $this->menu_admin_sub_selected==$value2['class'] ) $selected_sub="menu_sub_selected";
                        else $selected_sub="";
                        if( $class['admin-nav']=="admin-nav-inline" )
                        {
                            if( $selected=="menu_selected" )
                            {
                                if( !empty($value2['onclick']) ) $onclick="onclick=\"".$value2['onclick']."\"";
                                else $onclick="";
                                if( !empty($value2['target']) ) $target="target=\"".$value2['target']."\"";
                                else $target="";
                                print "\t<li class='$selected_sub'><a href='".$value2['url']."' $onclick ".$target." class='".$value2['class']." ".$selected_sub." a-admin-menu-sub' title='".$value2['title']."'><span>".$value2['title']."</span></a></li>\n";
                            }
                        }
                        else
                        {
                            if( !empty($value2['onclick']) ) $onclick="onclick=\"".$value2['onclick']."\"";
                            else $onclick="";
                            if( !empty($value2['target']) ) $target="target=\"".$value2['target']."\"";
                            else $target="";
                            print "\t<li><a href='".$value2['url']."' $onclick ".$target." class='".$value2['class']." ".$selected_sub." a-admin-menu-sub' title='".$value2['title']."'>".$value2['title']."</a></li>\n";
                        }
                    }
                    print "\t</ul>\n";
                }
                print "\t</li>\n";

            }
        }


        print "\t</ul>\n";
        print "\t</div>\n";




    }


    # relations_table function
    public static function show_relations($db,$typeid,$itemid,$options=array())
    {
        print "\t<br />\n";
        print "\t<a name='relations'></a>\n";
        print "\t<ul id='ul_link_tabs' class='tabsadmin'>\n";

        $_GET=UTILS::ifemptyGET($_GET,20);
        $item=admin_html::relation($db,$typeid);
        $item_url=$item['url'].$itemid;
        # printing out relations
        $query_where_relations=" WHERE (itemid1='".$itemid."' AND typeid1='".$typeid."') OR (itemid2='".$itemid."' AND typeid2='".$typeid."') ";
        $query_relations=QUERIES::query_relations($db,$query_where_relations,$query_order);

        $results_relations=$db->query($query_relations['query']);
        $count_relations=$db->numrows($results_relations);
        if( $count_relations>0 )
        {
            # PAINTINGS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            if( $_GET['order-by']=="paintID" )
            {
                if( $_GET['order-how']=="asc") $order_how="desc";
                else $order_how="asc";
            }
            else $order_how="asc";
            $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";$table_columns[0]['href']=$item_url."&order-by=paintID&order-how=".$order_how."&p=1&relation_typeid=1#relations";
            $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
            $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
            $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
            if( $typei==1 || $typeid==12 )
            {
                $table_columns[6]['name']="additional";$table_columns[6]['style']="text-align:left;";
                $key_id=7;
            }
            else $key_id=6;
            $table_columns[$key_id]['name']="date_created";$table_columns[$key_id]['style']="text-align:center;font-size:60%;";
            $table_columns[($key_id+1)]['name']="&nbsp;";$table_columns[($key_id+1)]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/edit/?paintid=";
            if( $typeid==10 ) $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=1";
            $url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
            //$url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/includes/retrieve.image.php?size=o&paintID=";

            // The $limit used in SQL
            // if not set default to 20
            $limit=(isset($_REQUEST['sp']))?$_REQUEST['sp']:20;
            $query_painting="SELECT paintID AS itemid2,artworkID AS artwork,titleEN,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";

            $values_query_paint=array();
            $values_query_paint['columns']=",titleEN as title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table, r.relationid";
            $query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);
            $options_relation_table['query_relations']=1;
            $options_relation_table['query_relations_where']="SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1='".$typeid."' AND r.itemid1='".$itemid."' AND r.typeid2=1 ) OR ( r.typeid2='".$typeid."' AND r.itemid2='".$itemid."' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) ";
            if( !empty($_GET['order-by']) && !empty($_GET['order-how']) && $_GET['relation_typeid']==1 ) $options_relation_table['query_relations_order']=" ORDER BY p.".$_GET['order-by']." ".$_GET['order-how']." ";
            elseif( $typeid==10 ) $options_relation_table['query_relations_order']=" ORDER BY r.sort1 ASC, r.relationid DESC ";
            else $options_relation_table['query_relations_order']=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
            if( $_GET['relation_typeid']==1 ) $options_relation_table['query_relations_limit']=" LIMIT ".($limit*($_GET['p']-1)).",".$limit;
            else $options_relation_table['query_relations_limit']=" LIMIT 0,".$limit;
            //print_r($options_relation_table);
            admin_html::relations_table($db,$typeid,$itemid,1,"Paintings",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_painting,$limit,$options_relation_table);
            #END PAINTINGS

            # IMAGES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="imageid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="src_old";$table_columns[2]['style']="";
            $table_columns[3]['name']="notes_admin";$table_columns[3]['style']="";
            $table_columns[4]['name']="date_created";$table_columns[4]['style']="text-align:center;font-size:60%;";
            $table_columns[5]['name']="&nbsp;";$table_columns[5]['style']="text-align:center;";
            $url_admin_edit="/admin/images/edit/?imageid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=17";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_image="SELECT imageid,imageid AS itemid2,title_en,src_old as title_delete,title_de,notes_admin,src_old FROM ".TABLE_IMAGES." WHERE imageid='";
            admin_html::relations_table(
                $db,#1
                $typeid,#2
                $itemid,#3
                17,#4
                "Images",#5
                $table_columns,#6
                $url_admin_edit,#7
                $url_admin_sort,#8
                $url_image_src,#9
                $query_image,#10
                $limit,#11
                $options_rel_table_images#12
            );
            #END IMAGES


            # ATLAS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
            $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
            $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
            $table_columns[6]['name']="date_created";$table_columns[6]['style']="text-align:center;font-size:60%;";
            $table_columns[7]['name']="&nbsp;";$table_columns[7]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/edit/?paintid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=13";
            //$url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_atlas="SELECT paintID AS itemid2,artworkID AS artwork,titleEN,titleEN as title_delete,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";
            admin_html::relations_table($db,$typeid,$itemid,13,"Atlas",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_atlas,$limit,$options_relation_table);
            #END ATLAS

            # ATLAS ASSOCIATED WORKS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
            $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
            $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
            $table_columns[6]['name']="date_created";$table_columns[6]['style']="text-align:center;font-size:60%;";
            $table_columns[7]['name']="&nbsp;";$table_columns[7]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/edit/?paintid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=14";
            //$url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_editions_ind="SELECT paintID AS itemid2,artworkID AS artwork,titleEN, titleEN as title_delete,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";

            $values_query_paint=array();
            $values_query_paint['columns']=",titleEN as title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table, r.relationid";
            $query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);
            $options_relation_table['query_relations']=1;
            $options_relation_table['query_relations_where']="SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1='".$typeid."' AND r.itemid1='".$itemid."' AND r.typeid2=14 ) OR ( r.typeid2='".$typeid."' AND r.itemid2='".$itemid."' AND r.typeid1=14 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=14 ) OR ( r.itemid1=p.paintID AND r.typeid1=14 ) ) ";
            if( !empty($_GET['order-by']) && !empty($_GET['order-how']) && $_GET['relation_typeid']==14 ) $options_relation_table['query_relations_order']=" ORDER BY p.".$_GET['order-by']." ".$_GET['order-how']." ";
            //elseif( $typeid==10 ) $options_relation_table['query_relations_order']=" ORDER BY r.sort1 ASC, r.relationid DESC ";
            else $options_relation_table['query_relations_order']=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
            if( $_GET['relation_typeid']==14 ) $options_relation_table['query_relations_limit']=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$limit;
            else $options_relation_table['query_relations_limit']=" LIMIT 0,".$limit;

            admin_html::relations_table($db,$typeid,$itemid,14,"Associated Works",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_editions_ind,$limit,$options_relation_table);
            # END ATLAS ASSOCIATED WORKS

            # SALE HISTORY
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="saleid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="sale_date";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="auction_house";$table_columns[2]['style']="text-align:center;width:150px;";
            $table_columns[3]['name']="sale_name";$table_columns[3]['style']="text-align:center;width:200px;";
            $table_columns[4]['name']="lot_nr.";$table_columns[4]['style']="text-align:center;";
            $table_columns[5]['name']="estimate_currency_1";$table_columns[5]['style']="text-align:center;";
            $table_columns[6]['name']="estimate_currency_2";$table_columns[6]['style']="text-align:center;";
            $table_columns[7]['name']="sold_for_currency_1";$table_columns[7]['style']="text-align:center;";
            $table_columns[8]['name']="sold_for_currency_2";$table_columns[8]['style']="text-align:center;";
            $table_columns[9]['name']="sale_type";$table_columns[9]['style']="text-align:center;";
            $table_columns[10]['name']="date_created";$table_columns[10]['style']="text-align:center;font-size:60%;";
            //$table_columns[11]['name']="sort";$table_columns[11]['style']="text-align:center;";
            $table_columns[11]['name']="&nbsp;";$table_columns[11]['style']="text-align:center;";
            $url_admin_edit="/admin/auctions-sales/saleHistory/edit/?saleid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=2";
            $url_image_src="";
            $query_sale_history="SELECT a.house AS auction_house, s.saleName AS sale_name, s.saleName AS title_delete, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.saleID='";
            admin_html::relations_table($db,$typeid,$itemid,2,"Sale History",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_sale_history);
            #END SALE HISTORY

            # MICROSITES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="micrositeid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="titleEN";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="date_created";$table_columns[2]['style']="text-align:center;font-size:60%;";
            $table_columns[3]['name']="&nbsp;";$table_columns[3]['style']="text-align:center;";
            $url_admin_edit="/admin/microsites/edit/?micrositeid=";
            $url_admin_sort="";
            $url_image_src="";
            $query_microsites="SELECT micrositeid AS itemid2,title_en AS titleEN, title_en AS title_delete FROM ".TABLE_MICROSITES." WHERE micrositeid='";
            admin_html::relations_table($db,$typeid,$itemid,3,"Microsites",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_microsites);
            #END MICROSITES

            # EXHIBITIONS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="exhibitionid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="title_original";$table_columns[1]['style']="";
            $table_columns[2]['name']="date";$table_columns[2]['style']="";
            $table_columns[3]['name']="type";$table_columns[3]['style']="text-align:center;";
            $table_columns[4]['name']="image";$table_columns[4]['style']="text-align:center;";
            $table_columns[5]['name']="date_created";$table_columns[5]['style']="text-align:center;font-size:60%;";
            $table_columns[6]['name']="&nbsp;";$table_columns[6]['style']="text-align:center;";
            $url_admin_edit="/admin/exhibitions/edit/?exhibitionid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=4";
            //$url_image_src="/includes/retrieve.image.php?size=xs&exID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="10";

            $query_where_exhibitions=" WHERE exID='";
            $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions);
            $query_exhibitions=$query_exhibitions['query'];


            $values_query_exh=array();
            //$values_query_exh['columns_distinct']="DISTINCT(r.relationid), ";
            $values_query_exh['columns']=",title_original AS title_delete, r.relationid, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table";
            $query_exh=QUERIES::query_exhibition($db,"","","",$values_query_exh);
            $options_relation_table['query_relations']=1;
            $options_relation_table['query_relations_where']="SELECT ".$query_exh['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1='".$typeid."' AND r.itemid1='".$itemid."' AND r.typeid2=4 ) OR ( r.typeid2='".$typeid."' AND r.itemid2='".$itemid."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND exID!='".$itemid."' ";
            if( !empty($_GET['order-by']) && !empty($_GET['order-how']) && $_GET['relation_typeid']==4 ) $options_relation_table['query_relations_order']=" ORDER BY p.".$_GET['order-by']." ".$_GET['order-how']." ";
            else $options_relation_table['query_relations_order']=" ORDER BY e.date_from DESC ";
            if( $_GET['relation_typeid']==4 ) $options_relation_table['query_relations_limit']=" LIMIT ".($limit*($_GET['p']-1)).",".$limit;
            else $options_relation_table['query_relations_limit']=" LIMIT 0,".$limit;
            //$options_relation_table['query_relations_group']=" GROUP BY r.relationid ";

            admin_html::relations_table($db,$typeid,$itemid,4,"Exhibitions",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_exhibitions,$limit,$options_relation_table);
            #END EXHIBITIONS

            #EXHIBITIONS INSTALLATION
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="installationid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="add_images";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date_created";$table_columns[3]['style']="text-align:center;font-size:60%;";
            $table_columns[4]['name']="&nbsp;";$table_columns[4]['style']="text-align:center;";
            $url_admin_edit="/admin/exhibitions/installations/edit/?installationid=";
            $url_admin_sort="";
            //$url_image_src="/includes/retrieve.image.php?size=xs&installation_imageid=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $values_query_exh_inst=array();
            $values_query_exh_inst['columns']=", title_en AS title_delete";
            $query_where_installations=" WHERE installationid='";
            $query_installations=QUERIES::query_exhibitions_installations($db,$query_where_installations,"","","",$values_query_exh_inst);
            $query_installations=$query_installations['query'];
            admin_html::relations_table($db,$typeid,$itemid,5,"Exh. Installations",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_installations,10,$options_relation_table);

            #END EXHIBITIONS INSTALLATION

            # NEWS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="newsid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="type";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="date_from";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date_to";$table_columns[3]['style']="text-align:center;";
            $table_columns[4]['name']="titleEN";$table_columns[4]['style']="";
            $table_columns[5]['name']="locationEN";$table_columns[5]['style']="";
            $table_columns[6]['name']="date_created";$table_columns[6]['style']="text-align:center;font-size:60%;";
            $table_columns[7]['name']="&nbsp;";$table_columns[7]['style']="text-align:center;";
            $url_admin_edit="/admin/news/edit/?newsid=";
            $url_admin_sort="";
            $url_image_src="";
            $query_news="SELECT newsid AS itemid2,type,titleEN,titleEN AS title_delete,locationEN,DATE_FORMAT(datefrom ,'%d %b %Y') as datefrom_list_admin,DATE_FORMAT(dateto ,'%d %b %Y') as dateto_list_admin FROM ".TABLE_NEWS." WHERE newsID='";
            admin_html::relations_table($db,$typeid,$itemid,7,"News",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_news);
            #END NEWS

            #PHOTOS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="photoid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="year";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date_created";$table_columns[3]['style']="text-align:center;font-size:60%;";
            $table_columns[4]['name']="&nbsp;";$table_columns[4]['style']="text-align:center;";
            $url_admin_edit="/admin/photos/edit/?photoid=";
            $url_admin_sort="";
            //$url_image_src="/includes/retrieve.image.php?size=s&photoID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $query_photos="SELECT photoID,photoID AS itemid2,year,src FROM ".TABLE_PHOTOS." WHERE photoID='";
            admin_html::relations_table($db,$typeid,$itemid,8,"Photos",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_photos,10,$options_relation_table);
            #END PHOTOS

            #QUOTES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="quoteid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="year";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="text_en";$table_columns[2]['style']="";
            $table_columns[3]['name']="text_de";$table_columns[3]['style']="";
            $table_columns[4]['name']="source_en";$table_columns[4]['style']="";
            $table_columns[5]['name']="source_de";$table_columns[5]['style']="";
            $table_columns[6]['name']="date_created";$table_columns[6]['style']="text-align:center;font-size:60%;";
            $table_columns[7]['name']="&nbsp;";$table_columns[7]['style']="text-align:center;";
            $url_admin_edit="/admin/quotes/edit/?quoteid=";
            $url_admin_sort="";
            $url_image_src="";
            $query_quotes="SELECT quoteid,quoteid AS itemid2,year,text_en,text_de,source_en,source_de, text_en AS title_delete FROM ".TABLE_QUOTES." WHERE quoteid='";
            admin_html::relations_table($db,$typeid,$itemid,9,"Quotes",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_quotes);
            #END QUOTES

            # VIDEOS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="videoid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="titleEN";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date_created";$table_columns[3]['style']="text-align:center;font-size:60%;";
            $table_columns[4]['name']="&nbsp;";$table_columns[4]['style']="text-align:center;";
            $url_admin_edit="/admin/videos/edit/?videoid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=10";
            $url_image_src="/includes/retrieve.image.php?size=m&videoID=";
            $options_relation_table['url_image_href']="/includes/retrieve.image.php?size=m&videoID=";
            $query_video="SELECT videoID AS itemid2,titleEN, titleEN AS title_delete FROM ".TABLE_VIDEO." WHERE videoID='";
            admin_html::relations_table($db,$typeid,$itemid,10,"Videos",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_video,10,$options_relation_table);
            #END VIDEOS

            # AUDIOS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="audioid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="titleEN";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date";$table_columns[3]['style']="text-align:center;";
            $table_columns[4]['name']="date_created";$table_columns[4]['style']="text-align:center;font-size:60%;";
            $table_columns[5]['name']="&nbsp;";$table_columns[5]['style']="text-align:center;";
            $url_admin_edit="/admin/audio/edit/?audioid=";
            $url_admin_sort="";
            //$url_image_src="/includes/retrieve.image.php?thumb=1&maxwidth=60&maxheight=60&audioid=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $query_video="SELECT audioid AS itemid2,tracktitleen AS titleEN,tracktitleen AS title_delete,DATE_FORMAT(date ,'%Y-%m-%d') as date FROM ".TABLE_AUDIO." WHERE audioid='";
            admin_html::relations_table($db,$typeid,$itemid,11,"Audios",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_video,10,$options_relation_table);
            #END AUDIOS

            # LITERATURE
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            if( $_GET['order-by']=="id" )
            {
                if( $_GET['order-how']=="asc") $order_how="desc";
                else $order_how="asc";
            }
            else $order_how="asc";
            $table_columns[0]['name']="bookid";$table_columns[0]['style']="text-align:center;";$table_columns[0]['href']=$item_url."&order-by=id&order-how=".$order_how."&p=1&relation_typeid=12#relations";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="title";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="additional";$table_columns[3]['style']="text-align:left;";
            $table_columns[4]['name']="date_created";$table_columns[4]['style']="text-align:center;font-size:60%;";
            $table_columns[5]['name']="&nbsp;";$table_columns[5]['style']="text-align:center;";
            $url_admin_edit="/admin/books/edit/?bookid=";
            if( $typeid==10 ) $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=12";
            //$url_image_src="/includes/retrieve.image.php?size=s&bookimageID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $query_book="SELECT id AS itemid2,title,imageID FROM ".TABLE_BOOKS." WHERE id='";
            $limit="20";

            $options_relation_table['query_relations']=1;
            $values_query_lit=array();
            $query_literature=QUERIES::query_books($db,"","","","",$values_query_lit);
            $values_query_lit['columns']=", b.title AS title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table";

            /*
            $values_query_lit['columns']=", b.title AS title_delete, r.relationid, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table";
            $options_relation_table['query_relations_where']="SELECT ".$query_literature['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1='".$typeid."' AND r.itemid1='".$itemid."' AND r.typeid2=12 ) OR ( r.typeid2='".$typeid."' AND r.itemid2='".$itemid."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) ";
            */


            $options_relation_table['query_relations_where']="
                SELECT
                  ".$query_literature['query_columns'].",
                  b.title AS title_delete,
                  COALESCE(
                    r1.itemid2,
                    r2.itemid1
                  ) as itemid,
                  COALESCE(
                    r1.sort,
                    r2.sort
                  ) as sort_rel,
                  COALESCE(
                    r1.relationid,
                    r2.relationid
                  ) as relationid,
                  COALESCE(
                    r1.date_created,
                    r2.date_created
                  ) as date_created_admin_table
                FROM
                  ".TABLE_BOOKS." b
                LEFT JOIN
                  ".TABLE_RELATIONS." r1 ON r1.typeid1 = '".$typeid."' and r1.itemid1='".$itemid."' and r1.typeid2 = 12 and r1.itemid2 = b.id
                LEFT JOIN
                  ".TABLE_RELATIONS." r2 ON r2.typeid2 = '".$typeid."' and r2.itemid2='".$itemid."' and r2.typeid1 = 12 and r2.itemid1 = b.id
                WHERE r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL
            ";

            if( $typeid==10 ) $options_relation_table['query_relations_order']=" ORDER BY sort_rel ASC, relationid DESC ";
            elseif( !empty($_GET['order-by']) && !empty($_GET['order-how']) && $_GET['relation_typeid']==12 ) $options_relation_table['query_relations_order']=" ORDER BY b.".$_GET['order-by']." ".$_GET['order-how']." ";
            else $options_relation_table['query_relations_order']=" ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC ";

            if( $_GET['relation_typeid']==12 ) $options_relation_table['query_relations_limit']=" LIMIT ".($limit*($_GET['p']-1)).",".$limit;
            else $options_relation_table['query_relations_limit']=" LIMIT 0,".$limit;
            //print $options_relation_table['query_relations_where'];


            admin_html::relations_table($db,$typeid,$itemid,12,"Literature",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_book,$limit,$options_relation_table);
            #END LITERATURE

            # PAINTINGS NOTES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="noteid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="noteEN";$table_columns[2]['style']="";
            $table_columns[3]['name']="noteDE";$table_columns[3]['style']="";
            $table_columns[4]['name']="noteZH";$table_columns[4]['style']="";
            $table_columns[5]['name']="date_created";$table_columns[5]['style']="text-align:center;font-size:60%;";
            $table_columns[6]['name']="&nbsp;";$table_columns[6]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/notes/edit/?noteid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=15";
            //$url_image_src="/includes/retrieve.image.php?size=ll&thumb=1&noteid=";
            $url_image_src="/images/size_ll__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_note="SELECT noteid,noteid AS itemid2,src,noteEN,noteDE,noteZH, noteEN AS title_delete FROM ".TABLE_PAINTING_NOTE." WHERE noteid='";
            admin_html::relations_table($db,$typeid,$itemid,15,"Notes images",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_note,$limit,$options_relation_table);
            #END PAINTINGS NOTES

            # MUSEUMS
            /*
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="museumid";$table_columns[0]['style']="width:70px;text-align:center;";
            $table_columns[1]['name']="museum";$table_columns[1]['style']="";
            $table_columns[2]['name']="date_created";$table_columns[2]['style']="text-align:center;font-size:60%;";
            $table_columns[3]['name']="&nbsp;";$table_columns[3]['style']="text-align:center;";
            $url_admin_edit="/admin/museums/edit/?museumid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=16";
            $url_image_src="";
            $limit="20";
            $query_museum="SELECT muID AS museumid,muID AS itemid2, museum FROM ".TABLE_MUSEUM." WHERE muID='";
            admin_html::relations_table($db,$typeid,$itemid,16,"Museums",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_museum,$limit,$options_relation_table);
             */
            #END MUSEUMS

            # COLORS
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="colorid";$table_columns[0]['style']="width:70px;text-align:center;";
            $table_columns[1]['name']="color_code";$table_columns[1]['style']="";
            $table_columns[2]['name']="percentage";$table_columns[2]['style']="";
            $table_columns[3]['name']="title_en";$table_columns[3]['style']="";
            $table_columns[4]['name']="title_de";$table_columns[4]['style']="";
            $table_columns[5]['name']="date_created";$table_columns[5]['style']="text-align:center;font-size:60%;";
            $table_columns[6]['name']="&nbsp;";$table_columns[6]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/colors/edit/?colorid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=23";
            $url_image_src="";
            $limit="20";
            $query_colors="SELECT colorid AS itemid2, color_code,title_en,title_de,title_en AS title_delete FROM ".TABLE_PAINTING_COLORS." WHERE colorid='";
            admin_html::relations_table($db,$typeid,$itemid,23,"Colors",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_colors,$limit);
            #END COLORS

            # PAINTING PHOTOS TAB
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="painting_photoid";$table_columns[0]['style']="width:70px;text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="typeid";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="date_created";$table_columns[3]['style']="text-align:center;font-size:60%;";
            $table_columns[4]['name']="&nbsp;";$table_columns[4]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/photos/edit/?painting_photoid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=24";
            $url_image_src="/images/size_xs__paintingphotoid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_photos="SELECT painting_photoid AS itemid2, typeid FROM ".TABLE_PAINTING_PHOTOS." WHERE painting_photoid='";
            admin_html::relations_table($db,$typeid,$itemid,24,"Photos",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_photos,$limit,$options_relation_table);
            #END PAINTING PHOTOS TAB

            # CATEGORIES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="categoryid";$table_columns[0]['style']="width:70px;text-align:center;";
            $table_columns[1]['name']="categoryEN";$table_columns[1]['style']="";
            $table_columns[2]['name']="categoryDE";$table_columns[2]['style']="";
            $table_columns[3]['name']="date_created";$table_columns[3]['style']="text-align:center;font-size:60%;";
            $table_columns[4]['name']="&nbsp;";$table_columns[4]['style']="text-align:center;";
            $url_admin_edit="/admin/categorys/edit/?catid=";
            $url_admin_sort="";
            $url_image_src="";
            $limit="20";
            $query_category="SELECT catID as categoryid,catID AS itemid2, categoryEN, categoryDE, categoryEN AS title_delete FROM ".TABLE_CATEGORY." WHERE catID='";
            admin_html::relations_table($db,$typeid,$itemid,18,"Categories",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_category,$limit);
            #END CATEGORIES

            # QUOTES CATEGORIES
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="quotes_categoryid";$table_columns[0]['style']="width:70px;text-align:center;";
            $table_columns[1]['name']="image";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="title_en";$table_columns[2]['style']="";
            $table_columns[3]['name']="title_de";$table_columns[3]['style']="";
            $table_columns[4]['name']="date_created";$table_columns[4]['style']="text-align:center;font-size:60%;";
            $table_columns[5]['name']="&nbsp;";$table_columns[5]['style']="text-align:center;";
            $url_admin_edit="/admin/quotes/categories/edit/?quotes_categoryid=";
            $url_admin_sort="";
            $url_image_src="/images/size_s__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_quotes_category="SELECT quotes_categoryid,quotes_categoryid AS itemid2, title_en, title_de, title_en AS title_delete FROM ".TABLE_QUOTES_CATEGORIES." WHERE quotes_categoryid='";
            admin_html::relations_table($db,$typeid,$itemid,19,"Quotes-categories",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_quotes_category,$limit,$options_relation_table);
            #END QUOTES CATEGORIES

            # Edition
            if( $options['row']['edition_individual'] )
            {
                $options_relation_table=array();
                $options_relation_table['html']=$options['html'];
                $table_columns=array();
                $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";
                $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
                $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
                $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
                $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
                $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
                $url_admin_edit="/admin/paintings/edit/?paintid=";
                $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=13";
                //$url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
                $url_image_src="/images/size_xs__imageid_";
                $options_relation_table['url_image_href']="/images/size_o__imageid_";
                $limit="20";
                $results_edition=$db->query("SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=1 AND r.itemid2='".$itemid."' AND r.typeid2=25 ) OR ( r.typeid2=1 AND r.itemid1='".$itemid."' AND r.typeid1=25 ) ) AND ( ( r.itemid1=p.paintID AND r.typeid1=1 ) OR ( r.itemid2=p.paintID AND r.typeid2=1 ) ) LIMIT 1 ");
                //$count_editions_ind=$db->numrows($results_editions_ind);
                $row_edition=$db->mysql_array($results_edition,0);
                //print_R($row_edition);
                //$edition_paintid=UTILS::get_relation_id($db,1,$row_edition);
                //$query_edition="SELECT paintID AS itemid2,artworkID AS artwork,titleEN,titleEN as title_delete,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";
                //print $edition_paintid."----";
                //print $row_edition['paintID']."----";
                //admin_html::relations_table($db,$typeid,$itemid,25,"Edition",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$results_edition,$limit,$options_relation_table);

                print "\t<li class='Edition tab'>\n";
                print "\t<h4><a href='#Edition' name='Edition' class='Edition name ".$selected."'>Edition</a></h4>\n";
                print "\t<div class='tabsadmin-info'>\n";
                print "\t<div class='holder Edition-info' id='".$id."'>\n";
                print "\t<table class='table-links'>\n";
                print "\t<tr>\n";
                foreach( $table_columns as $column )
                {
                    $column_title=str_replace("_", " ", $column['name']);
                    print "\t<th style='".$column['style']."'>\n";
                    if( !empty($column['href']) ) print "<a href='".$column['href']."' style='text-decoration:underline;' >";
                    print $column_title;
                    if( !empty($column['href']) ) print "</a>";
                    print "\t</th>\n";
                }
                print "\t</tr>\n";
                print "\t<tr>\n";
                print "\t<td style='text-align:center;'><a href='".$url_admin_edit.$row_edition['paintID']."' title='edit this item'><u>".$row_edition['paintID']."</u></a></td>\n";
                print "\t<td style='text-align:center;'>".$row_edition['number']."</td>\n";
                print "\t<td style='text-align:center;'><img src='/includes/retrieve.image.php?size=xs&paintID=".$row_edition["paintID"]."' alt='' /></td>\n";
                print "\t<td>".$row_edition['titleEN']."</td>\n";
                print "\t<td>".$row_edition['titleDE']."</td>\n";
                $artwork=UTILS::get_artwork_info($db,$row_edition["artworkID"]);
                print "\t<td style='text-align:center;'>".$artwork['artworkEN']."</td>\n";
                print "\t</tr>\n";
                print "</table>";
                print "</div>";
                print "</div>";
                print "</li>";

            }
            #END EDITION

            # Editions – individual
            $options_relation_table=array();
            $options_relation_table['html']=$options['html'];
            $table_columns=array();
            $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";
            $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
            $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
            $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
            $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
            $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
            $table_columns[6]['name']="date_created";$table_columns[6]['style']="text-align:center;font-size:60%;";
            $table_columns[7]['name']="&nbsp;";$table_columns[7]['style']="text-align:center;";
            $url_admin_edit="/admin/paintings/edit/?paintid=";
            $url_admin_sort="/admin/relations/edit/sort/?typeid1=".$typeid."&itemid1=".$itemid."&typeid2=25";
            //$url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
            $url_image_src="/images/size_xs__imageid_";
            $options_relation_table['url_image_href']="/images/size_o__imageid_";
            $limit="20";
            $query_editions_ind="SELECT paintID AS itemid2,artworkID AS artwork,titleEN, titleEN as title_delete,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";

            $values_query_paint=array();
            $values_query_paint['columns']=",titleEN as title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table, r.relationid";
            $query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);
            $options_relation_table['query_relations']=1;
            $options_relation_table['query_relations_where']="SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1='".$typeid."' AND r.itemid1='".$itemid."' AND r.typeid2=25 ) OR ( r.typeid2='".$typeid."' AND r.itemid2='".$itemid."' AND r.typeid1=25 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=25 ) OR ( r.itemid1=p.paintID AND r.typeid1=25 ) ) ";
            if( !empty($_GET['order-by']) && !empty($_GET['order-how']) && $_GET['relation_typeid']==25 ) $options_relation_table['query_relations_order']=" ORDER BY p.".$_GET['order-by']." ".$_GET['order-how']." ";
            //elseif( $typeid==10 ) $options_relation_table['query_relations_order']=" ORDER BY r.sort1 ASC, r.relationid DESC ";
            else $options_relation_table['query_relations_order']=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
            if( $_GET['relation_typeid']==25 ) $options_relation_table['query_relations_limit']=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$limit;
            else $options_relation_table['query_relations_limit']=" LIMIT 0,".$limit;

            //admin_html::relations_table($db,$typeid,$itemid,25,"Editions – individual",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_editions_ind,$limit);
            admin_html::relations_table($db,$typeid,$itemid,25,"Editions – individual",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_editions_ind,$limit,$options_relation_table);
            #END Editions – individual

            # Collection artworks
            if( !empty($options['row']['paintID']) && !empty($options['row']['locationid']) )
            {
                $options_relation_table=array();
                $options_relation_table['html']=$options['html'];
                $options_relation_table['show_delete']=1;
                $table_columns=array();
                $table_columns[0]['name']="paintid";$table_columns[0]['style']="text-align:center;";
                $table_columns[1]['name']="number";$table_columns[1]['style']="text-align:center;";
                $table_columns[2]['name']="image";$table_columns[2]['style']="text-align:center;";
                $table_columns[3]['name']="titleEN";$table_columns[3]['style']="";
                $table_columns[4]['name']="titleDE";$table_columns[4]['style']="";
                $table_columns[5]['name']="artwork";$table_columns[5]['style']="";
                $url_admin_edit="/admin/paintings/edit/?paintid=";
                $url_admin_sort="";
                //$url_image_src="/includes/retrieve.image.php?size=xs&paintID=";
                $url_image_src="/images/size_xs__imageid_";
                $options_relation_table['url_image_href']="/images/size_o__imageid_";
                $limit="20";
                $query_editions_ind="SELECT paintID AS itemid2,artworkID AS artwork,titleEN, titleEN as title_delete,titleDE,number FROM ".TABLE_PAINTING." WHERE paintID='";

                $values_query_paint=array();
                $query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);
                $options_relation_table['query_relations']=1;
                $options_relation_table['query_relations_where']="SELECT ".$query_paintings['query_columns']." FROM ".TABLE_PAINTING." p WHERE locationid='".$options['row']['locationid']."' ";
                $options_relation_table['query_relations_order']=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
                $options_relation_table['query_relations_limit']=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$limit;

                admin_html::relations_table($db,$typeid,$itemid,"","Collection artworks",$table_columns,$url_admin_edit,$url_admin_sort,$url_image_src,$query_editions_ind,$limit,$options_relation_table);
            }
            # Collection artworks

        }
        #end


        print "<li><a></a></li>";
        print "\t</ul>\n";
        print "<div class='tabsadmin-info'>";
        print "<div class='holder'></div>";
        print "</div>\n\n";
    }
    # END relations_table function




    # relations_table function
    public static function relations_table(
        $db,#1
        $typeid1,#2
        $itemid1,#3
        $typeid2,#4
        $title,#5
        $table_columns,#6
        $url_admin_edit,#7
        $url_admin_sort,#8
        $url_image_src,#9
        $query,#10
        $limit=10,#11
        $values=array()#12
    )
    {
        //if( $typeid2==25 ) print_r($_GET);
        $_GET=UTILS::ifemptyGET($_GET,$limit);
        //print_r($_GET);
        //print $limit;
        $title_no_spaces=str_replace ("", " ", $title);

        if( $values['query_relations'] )
        {
            $results_relations=$db->query($values['query_relations_where']);
            $count_relations=$db->numrows($results_relations);
            $query_relations=$values['query_relations_where'].$values['query_relations_group'].$values['query_relations_order'].$values['query_relations_limit'];
            $results_relations=$db->query($query_relations);
            //if( $typeid1==4 && $typeid2==4 ) print $query_relations;
            //if( $typeid1==1 && $typeid2==25 ) print $query_relations;
            //if( $typeid1==1 && $typeid2==24 ) print $query_relations;
            //if( $typeid1==12 && $typeid2==12 ) print $query_relations;
            //if( $typeid1==1 && $typeid2==12 ) print $query_relations;
            //print "|".$typeid2."-".$count_relations;
            //if( $_SESSION['debug_page'] ) print $query_relations;
            //print $query_relations;
        }
        else
        {

            $query_where_relations=" WHERE (itemid1='".$itemid1."' AND typeid1='".$typeid1."' AND typeid2='".$typeid2."') OR (itemid2='".$itemid1."' AND typeid2='".$typeid1."' AND typeid1='".$typeid2."') ";
            $sort_column=UTILS::get_sort_column($db,$typeid2);
            $query_order_relations=" ORDER BY ".$sort_column." ASC, relationid DESC ";

            /*
            if( $_GET['relation_typeid']==$typeid2 ) $query_order_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
            else $query_order_limit=" LIMIT 0,".$_GET['sp'];
            */

            if( $_GET['relation_typeid']==$typeid2 ) $query_order_limit=" LIMIT ".($limit*($_GET['p']-1)).",".$limit;
            else $query_order_limit=" LIMIT 0,".$limit;

            $query_relations=QUERIES::query_relations($db,$query_where_relations,$query_order_relations,$query_order_limit);
            //if( $typeid1==4 && $typeid2==4 ) print $query_relations['query'];
            $results_relations=$db->query($query_relations['query_without_limit']);
            $count_relations=$db->numrows($results_relations);
            $results_relations=$db->query($query_relations['query']);
            //if( $_SESSION['debug_page'] ) print $query_relations['query'];
            //print $query_relations['query'];
        }
        if( $count_relations>0 )
        {
            if( $_GET['relation_typeid']==$typeid2 && !empty($_GET['relation_typeid']) )
            {
                $selected=" selected ";
                $id="this_type_selected";
            }
            else
            {
                $selected="";
                $id="";
            }

            print "\t<li class='".$title_no_spaces." tab'>\n";
            print "\t<h4><a href='#".$title_no_spaces."' name='".$title_no_spaces."' class='".$title_no_spaces." name ".$selected."'>".$title."</a></h4>\n";
            print "\t<div class='tabsadmin-info'>\n";
            print "\t<div class='holder ".$title_no_spaces."-info' id='".$id."'>\n";
            # sort items
            if( $count_relations>1 && !empty($url_admin_sort) )
            {
                print "\t<a href='".$url_admin_sort."' title='Sort items' target='_blank'><u>sort items</u></a>\n";
            }
            //else print $count_relations."-".$url_admin_sort;
            # end
            # items found
            print "&nbsp;items found: ".$count_relations;
            # end
            # page numbering
            //$pages=@ceil($count_relations/$_GET['sp']);

            $pages=@ceil($count_relations/$limit);
            $item=admin_html::relation($db,$typeid1);
            $url_pages="?".$item['id_name']."=".$itemid1;
            if( !empty($_GET['order-by']) && !empty($_GET['order-how']) ) $url_pages.="&order-by=".$_GET['order-by']."&order-how=".$_GET['order-how'];
            // Removed "p=" from $url_pages below.
            // Set in page_per_page_admin & page_numbering_admin.
            $url_pages.="&relation_typeid=".$typeid2;

            if( $typeid2==25 )
            {
                //print "<br />-----------pages=".$pages."|count_rel=".$count_relations."|get_sp=".$_GET['sp']."|limit=".$limit."----------------";
                //print "<br />".$query_relations;
            }

            // Allow per page select element
            print "<br/><br/>";
            admin_html::page_per_page_admin($db, $pages, $_REQUEST['sp'], $count, $url_pages);

            // Added last param $per_page_list set to 0.
            // This should turn on page_per_page_admin
            // however it is broken
            admin_html::page_numbering_admin($db,$pages,$_GET['p'],$limit,$url_pages,"",$class,true,"#relations",0);




            # end
            print "\t<table class='table-links'>\n";
            print "\t<tr>\n";
            foreach( $table_columns as $column )
            {
                $column_title=str_replace("_", " ", $column['name']);
                print "\t<th style='".$column['style']."'>\n";
                if( !empty($column['href']) ) print "<a href='".$column['href']."' style='text-decoration:underline;' >";
                print $column_title;
                if( !empty($column['href']) ) print "</a>";
                print "\t</th>\n";
            }
            print "\t</tr>\n";

            while( $row_relations=$db->mysql_array($results_relations) )
            {
                if( $values['query_relations'] )
                {
                    $item_tableid=$row_relations['itemid2'];
                    $count_item=$db->numrows($results_relations);
                    $row_item=$row_relations;
                }
                else
                {
                    $item_tableid=UTILS::get_relation_id($db,$typeid2,$row_relations);
                    $query_item=$query.$item_tableid."'";
                    $results_item=$db->query($query_item);
                    $count_item=$db->numrows($results_item);
                    $row_item=$db->mysql_array($results_item);
                }
                $row_item=UTILS::html_decode($row_item);
                if( $count_item>0 )
                {
                    print "\t<tr>\n";
                    $i=0;
                    foreach( $table_columns as $column )
                    {
                        $i++;
                        print "\t<td style='".$column['style']."'>\n";
                        if( $i==1 )
                        {
                            print "\t<a href='".$url_admin_edit.$item_tableid."' title='edit this item'><u>".$item_tableid."</u></a>\n";
                        }
                        elseif( $column['name']=="image" )
                        {
                            if( $typeid2==10 || $typeid2==1 )
                            {
                                $src=$url_image_src.$item_tableid;
                                $href_image=$values['url_image_href'].$item_tableid;
                            }
                            else
                            {
                                if( $typeid2==17 || $typeid2==24 )
                                {
                                    $src=$url_image_src.$row_item['itemid2'];
                                    $href_image=$values['url_image_href'].$row_item['itemid2'];
                                }
                                else
                                {
                                    if( $typeid2==13 || $typeid2==14 ) $typeid_for_image_use=$typeid1;
                                    elseif( $typeid2==25 ) $typeid_for_image_use=1;
                                    else $typeid_for_image_use=$typeid2;
                                    $query_related_image="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='".$typeid_for_image_use."' AND itemid2='".$row_item['itemid2']."' ) OR ( typeid2=17 AND typeid1='".$typeid_for_image_use."' AND itemid1='".$row_item['itemid2']."' ) ";
                                    $query_related_image.=" ORDER BY sort ASC, relationid DESC ";
                                    $results_related_image=$db->query($query_related_image);
                                    //print $query_related_image;
                                    $row_related_image=$db->mysql_array($results_related_image);
                                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);

                                    $src=$url_image_src.$imageid;
                                    $href_image=$values['url_image_href'].$url_image_src.$imageid;
                                }

                            }
                            # END books image
                            # END gallery image

                            if( !empty($values['url_image_href']) ) print "<a href='".$href_image."' title='View original image' target='_blank'>";
                            print "\t<img src='".$src."' alt='' />\n";
                            if( !empty($values['url_image_href']) ) print "</a>";
                        }
                        # date relation created
                        elseif( $column['name']=="date_created" )
                        {
                            print $row_relations['date_created_admin_table'];
                        }
                        # END date relation created
                        # delete relation column
                        elseif( $i==count($table_columns) && !$values['show_delete'] )
                        {
                            $title_delete_section=admin_html::relation($db,$typeid2);
                            $title_delete=$row_item['title_delete'];
                            $url="/admin/relations/edit/del.php?relationid=".$row_relations['relationid'];
                            if( empty($_GET['relation_typeid']) ) $url.="&relation_typeid=".$typeid2;
                            else $url.="&relation_typeid=".$_GET['relation_typeid'];
                            //print "\t<a href='#relations' title='delete this relation' onclick=\"delete_confirm2('".$url."','Relation - ".$title_delete_section['name']." - ".addslashes($title_delete)."','');\">\n";
                            //print "<img src='/g/delete.gif' alt='delete' />";
                            //print "\t</a>\n";

                            $options_delete=array();
                            $options_delete['url_del']=$url;
                            $options_delete['text1']="Relation - ".$title_delete_section['name']." - ".$title_delete;
                            //$options_delete['text2']="";
                            //$options_delete['html_return']=1;
                            admin_html::delete_button($db,$options_delete);

                        }
                        # END delete relation column
                        else
                        {
                            $value="";

                            # SALE HISTORY
                            # sale type
                            if( $column['name']=="sale_type" )
                            {
                                if( $row_item[$column['name']]==1 ) $sale_type="Premium";
                                elseif( $row_item[$column['name']]==2 ) $sale_type="Hammer";
                                elseif( $row_item[$column['name']]==3 ) $sale_type="Unknown";
                                else $sale_type="&nbsp;";
                                $value=$sale_type;
                            }
                            # end sale type

                            # estimate currency 1
                            if( $column['name']=="estimate_currency_1" )
                            {
                                $currency1=UTILS::get_currency($db,$row_item['estCurrID']);
                                $estimate1="";
                                if( !empty($row_item['estLow']) ) $estimate1=$currency1.number_format($row_item['estLow'],0);
                                if( !empty($row_item['estLow']) && !empty($row_item['estHigh']) ) $estimate1.="<br />-<br />";
                                if( !empty($row_item['estHigh']) ) $estimate1.=$currency1.number_format($row_item['estHigh'],0);
                                if( empty($estimate1) ) $estimate1="&nbsp;";
                                $value=$estimate1;
                            }
                            # end estimate currency 1

                            # estimate currency 2
                            if( $column['name']=="estimate_currency_2" )
                            {
                                $currency2=UTILS::get_currency($db,1);
                                $estimate2="";
                                if( !empty($row_item['estLowUSD']) ) $estimate2=$currency2.number_format($row_item['estLowUSD'],0);
                                if( !empty($row_item['estLowUSD']) && !empty($row_item['estHighUSD']) ) $estimate2.="<br />-<br />";
                                if( !empty($row_item['estHighUSD']) ) $estimate2.=$currency2.number_format($row_item['estHighUSD'],0);
                                if( empty($estimate2) ) $estimate2="&nbsp;";
                                $value=$estimate2;
                            }
                            # end estimate currency 2

                            # sold for currency 1
                            if( $column['name']=="sold_for_currency_1" )
                            {
                                if( !empty($row_item['soldFor']) ) $sold_for1=UTILS::get_currency($db,$row_item['soldForCurrID']).number_format($row_item['soldFor'],0);
                                else $sold_for1="&nbsp;";
                                $value=$sold_for1;
                            }
                            # end sold for currency 1

                            # sold for currency 2
                            if( $column['name']=="sold_for_currency_2" )
                            {
                                if( !empty($row_item['soldForUSD']) ) $sold_for2=UTILS::get_currency($db,1).number_format($row_item['soldForUSD'],0);
                                else $sold_for2="&nbsp;";
                                $value=$sold_for2;
                            }
                            # end sold for currency 2
                            # END SALE HISTORY

                            # EXHIBITIONS
                            if( $typeid2==4 )
                            {
                                # titleEN
                                if( $column['name']=="title_original" )
                                {
                                    $value=$row_item[$column['name']];
                                    $values_location=array();
                                    $values_location['row']=$row_item;
                                    $location=location($db,$values_location);
                                    if( !empty($location) )
                                    {
                                        $value.="\t<span style='color:#666;'>\n";
                                        $value.="<br />".$location;
                                        $value.="\t</span>\n";
                                    }
                                }
                                # end titleEN
                                # date
                                if( $column['name']=="date" )
                                {
                                    $value=exhibition_date($db,$row_item);
                                }
                                # end date
                                # type
                                if( $column['name']=="type" )
                                {
                                    if( $row_item['typeid']==1 ) $type="Solo";
                                    elseif( $row_item['typeid']==2 ) $type="Group";
                                    $value=$type;
                                }
                                # end type
                            }
                            # END EXHIBITIONS

                            # EXH. INSTALLATIONS
                            if( $typeid2==5 )
                            {
                                # add_images
                                if( $column['name']=="add_images" )
                                {
                                    $value="<a href='/admin/exhibitions/installations/images/edit/?installationid=".$item_tableid."' title='".$column['name']."' class='a_edit_item'>".$column['name']."</a>";
                                }
                                # end add_images
                            }
                            # END EXH. INSTALLATIONS

                            # PAINTINGS
                            /*
                            if( $typeid2==1 || $typeid2==13 || $typeid2==14 || $typeid2==25 )
                            {
                                if( $column['name']=="artwork" )
                                {
                                    $artwork=UTILS::get_artwork_info($db,$row_item["artwork"]);
                                    $value=$artwork['artworkEN'];
                                }
                            }
                            */
                            # END PAINTINGS

                            # NEWS
                            if( $typeid2==7 )
                            {
                                if( $column['name']=="date_from" )
                                {
                                    $value=$row_item['datefrom_list_admin'];
                                }
                                if( $column['name']=="date_to" )
                                {
                                    $value=$row_item['dateto_list_admin'];
                                }
                                if( $column['name']=="type" )
                                {
                                    $value=UTILS::get_news_type($db,$row_item['type']);;
                                }
                            }
                            # END NEWS

                            # COLORS
                            if( $typeid2==23 )
                            {
                                if( $column['name']=="color_code" )
                                {
                                    $value="<span style='background-color:#".$row_item[$column['name']].";'>".$row_item[$column['name']]."</span>";
                                }
                                elseif( $column['name']=="percentage" )
                                {
                                    $query_rel_percentage_where=" WHERE relationid='".$row_relations['relationid']."' ";
                                    $query_rel_percentage=QUERIES::query_relations_painting_colors($db,$query_rel_percentage_where);
                                    $results_rel_percentage=$db->query($query_rel_percentage['query']);
                                    $count_rel_percentage=$db->numrows($results_rel_percentage);
                                    if( $count_rel_percentage )
                                    {
                                        $row_rel_percentage=$db->mysql_array($results_rel_percentage,0);
                                        $value=$row_rel_percentage['percentage'];
                                    }
                                }
                            }
                            # END COLORS

                            # PAINTING PHOTOS
                            if( $typeid2==24 )
                            {
                                if( $column['name']=="typeid" )
                                {
                                    if( $row_item[$column['name']]==1 ) $paint_phot_type="Details";
                                    elseif( $row_item[$column['name']]==2 ) $paint_phot_type="Installation Shots";
                                    else $paint_phot_type="&nbsp;";
                                    $value=$paint_phot_type;
                                }
                            }
                            # END PAINTING PHOTOS

                            # ARTWORK
                            if( $column['name']=="artwork" )
                            {
                                //print_R($row_item);
                                if( !empty($row_item["artwork"]) ) $artworkid=$row_item["artwork"];
                                else $artworkid=$row_item["artworkID"];
                                $artwork2=UTILS::get_artwork_info($db,$artworkid);
                                $value=$artwork2['artworkEN'];
                            }
                            # END ARTWORK

                            # BOOKS
                            if( $typeid2==12 || $typeid2==1 )
                            {
                                if( $column['name']=="additional" )
                                {
                                    $query_rel_lit_where=" WHERE relationid='".$row_relations['relationid']."' ";
                                    $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
                                    $results_rel_lit=$db->query($query_rel_lit['query']);
                                    $count_rel_lit=$db->numrows($results_rel_lit);
                                    if( $count_rel_lit )
                                    {
                                        $row_rel_lit=$db->mysql_array($results_rel_lit,0);
                                        $value="";
                                        if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['show_mentioned']) )
                                        {
                                            $tmp=explode(",",$row_rel_lit['mentioned']);
                                            $value.=LANG_ARTWORK_BOOKS_REL_MENTIONED;
                                            if( empty($row_rel_lit['show_mentioned']) )
                                            {
                                                $value.=": ";
                                                if( count($tmp)>1 ) $value.=LANG_ARTWORK_BOOKS_REL_PP;
                                                else $value.=LANG_ARTWORK_BOOKS_REL_P;
                                                $value.=" ".$row_rel_lit['mentioned'];
                                                if( $_SESSION['lang']=="ch" )
                                                {
                                                    if( count($tmp)>1 ) $value.=" ".LANG_ARTWORK_BOOKS_REL_P2;
                                                    else $value.=" ".LANG_ARTWORK_BOOKS_REL_PP2;
                                                }
                                            }
                                            $value.="<br />";
                                        }
                                        if( !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_discussed']) )
                                        {
                                            $tmp=explode(",",$row_rel_lit['discussed']);
                                            $value.=LANG_ARTWORK_BOOKS_REL_DISCUSSED;
                                            if( empty($row_rel_lit['show_discussed']) )
                                            {
                                                $value.=": ";
                                                if( count($tmp)>1 ) $value.=LANG_ARTWORK_BOOKS_REL_PP;
                                                else $value.=LANG_ARTWORK_BOOKS_REL_P;
                                                $value.=" ".$row_rel_lit['discussed'];
                                                if( $_SESSION['lang']=="ch" )
                                                {
                                                    if( count($tmp)>1 ) $value.=" ".LANG_ARTWORK_BOOKS_REL_P2;
                                                    else $value.=" ".LANG_ARTWORK_BOOKS_REL_PP2;
                                                }
                                            }
                                            $value.="<br />";
                                        }
                                        if( !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) )
                                        {
                                            $tmp=explode(",",$row_rel_lit['illustrated_bw']);
                                            $tmp2=explode(",",$row_rel_lit['illustrated']);
                                            if( empty($row_rel_lit['show_illustrated']) && empty($row_rel_lit['show_bw']) )
                                            {
                                                $value.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED;
                                                $value.=": ";
                                                if( count($tmp)>1 || count($tmp2)>1 || stripos($row_rel_lit['illustrated_bw'],"-") || stripos($row_rel_lit['illustrated'],"-") || (!empty($row_rel_lit['illustrated_bw']) && !empty($row_rel_lit['illustrated'])) ) $value.=LANG_ARTWORK_BOOKS_REL_PP;
                                                else $value.=LANG_ARTWORK_BOOKS_REL_P;
                                                # black and white
                                                if( !empty($row_rel_lit['illustrated_bw']) )
                                                {
                                                    $value.=" ".$row_rel_lit['illustrated_bw'];
                                                    if( $_SESSION['lang']=="ch" )
                                                    {
                                                        if( count($tmp)>1 ) $value.=" ".LANG_ARTWORK_BOOKS_REL_P2;
                                                        else $value.=" ".LANG_ARTWORK_BOOKS_REL_PP2;
                                                    }
                                                    $value.=" (".LANG_ARTWORK_BOOKS_REL_BW.")";
                                                }
                                                # END black and white
                                                # color
                                                if( !empty($row_rel_lit['illustrated']) )
                                                {
                                                    if( !empty($row_rel_lit['illustrated_bw']) )
                                                    {
                                                        $value.=", ";
                                                    }
                                                    $value.=" ".$row_rel_lit['illustrated'];
                                                    if( $_SESSION['lang']=="ch" )
                                                    {
                                                        if( count($tmp)>1 ) $value.=" ".LANG_ARTWORK_BOOKS_REL_P2;
                                                        else $value.=" ".LANG_ARTWORK_BOOKS_REL_PP2;
                                                    }
                                                    $value.=" (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                                                }
                                                # END color
                                            }
                                            else
                                            {
                                                if( !empty($row_rel_lit['show_illustrated']) )
                                                {
                                                    $value.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." ".LANG_ARTWORK_BOOKS_REL_COLOUR;
                                                }
                                                if( !empty($row_rel_lit['show_bw']) )
                                                {
                                                    if( !empty($row_rel_lit['show_illustrated']) ) $value.="<br />";
                                                    $value.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED."&nbsp;".LANG_ARTWORK_BOOKS_REL_BW;
                                                }
                                            }
                                            //else $value.="here";
                                            //if( $row_rel_lit['illustrated_typeid'] ) $value.=" (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";

                                            //print_r($row_rel_lit);
                                            $value.="<br />";
                                        }
                                        $url_relationid_lit="/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$itemid1."\n";
                                    }
                                    else
                                    {
                                        $url_relationid_lit="/admin/relations/literature/edit/?relationid_lit=new&relationid=".$row_relations['relationid']."&bookid=".$itemid1."\n";
                                    }
                                    UTILS::admin_edit($url_relationid_lit);
                                    print "<br />";
                                }
                            }
                            # END BOOKS

                            if( empty($value) ) $value=$row_item[$column['name']];
                            print $value;
                        }
                        print "\t</td>\n";
                    }
                    print "\t</tr>\n";
                }
                else
                {
                    print "\t<tr>\n";
                    print "\t<td style='text-align:center;'>".$item_tableid."</td>\n";
                    print "\t<th colspan='".(count($table_columns)-3)."' class='error' style='text-align:center;'>\n";
                    print "ITEM NOT FOUND";
                    print "\t</th>\n";
                    print "\t<td style='text-align:center;'>\n";
                    print $row_relations['date_created_admin'];
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                    $url="/admin/relations/edit/del.php?relationid=".$row_relations['relationid'];
                    if( empty($_GET['relation_typeid']) ) $url.="&relation_typeid=".$typeid2;
                    else $url.="&relation_typeid=".$_GET['relation_typeid'];
                    print "\t<a href='#relations' title='delete this relation' onclick=\"delete_confirm2('".$url."','','');\">\n";
                    print "<img src='/g/delete.gif' alt='delete' />";
                    print "\t</a>\n";
                    print "\t</td>\n";
                    print "\t</tr>\n";
                }
            }
            print "\t</table>\n";
            # page numbering
            admin_html::page_numbering_admin($db,$pages,$_GET['p'],$limit,$url_pages,"",$class,true,"#relations");
            # end
            print "\t</div>\n"; #div holder end VIDEOS
            print "\t</div>\n"; #div tabs-info end VIDEOS
            print "\t</li>\n";

        }

    }
    # END relations_table function





    public static function relation($db,$typeid)
    {
        $relations[1]['name']="Paintings";
        $relations[1]['id_name']="paintid";
        $relations[1]['url']="/admin/paintings/edit/?paintid=";
        $relations[13]['name']="Atlas";
        $relations[13]['id_name']="paintid";
        $relations[13]['url']="/admin/paintings/edit/?paintid=";
        $relations[14]['name']="Associated Works";
        $relations[14]['id_name']="paintid";
        $relations[14]['url']="/admin/paintings/edit/?paintid=";
        $relations[2]['name']="Sales history";
        $relations[2]['id_name']="saleid";
        $relations[2]['url']="/admin/auctions-sales/saleHistory/edit/?saleid=";
        $relations[3]['name']="Microsites";
        $relations[3]['id_name']="micrositeid";
        $relations[3]['url']="/admin/microsites/edit/?micrositeid=";
        $relations[4]['name']="Exhibitions";
        $relations[4]['id_name']="exhibitionid";
        $relations[4]['url']="/admin/exhibitions/edit/?exhibitionid=";
        $relations[5]['name']="Installations";
        $relations[5]['id_name']="installationid";
        $relations[5]['url']="/admin/exhibitions/installations/edit/?installationid=";
        $relations[7]['name']="News";
        $relations[7]['id_name']="newsid";
        $relations[7]['url']="/admin/news/edit/?newsid=";
        $relations[8]['name']="Photos";
        $relations[8]['id_name']="photoid";
        $relations[8]['url']="/admin/photos/edit/?photoid=";
        $relations[9]['name']="Quotes";
        $relations[9]['id_name']="quoteid";
        $relations[9]['url']="/admin/quotes/edit/?quoteid=";
        $relations[10]['name']="Videos";
        $relations[10]['id_name']="videoid";
        $relations[10]['url']="/admin/videos/edit/?videoid=";
        //$relations[6]['name']="Related videos";
        //$relations[6]['id_name']="related_videoid";
        //$relations[6]['url']="/admin/related_videos/edit/?related_videoid=";
        $relations[11]['name']="Audios";
        $relations[11]['id_name']="audioid";
        $relations[11]['url']="/admin/audio/edit/?audioid=";
        $relations[12]['name']="Literature";
        $relations[12]['id_name']="bookid";
        $relations[12]['url']="/admin/books/edit/?bookid=";
        $relations[15]['name']="Notes images";
        $relations[15]['id_name']="noteid";
        $relations[15]['url']="/admin/paintings/notes/edit/?noteid=";
        //$relations[16]['name']="Museums";
        //$relations[16]['id_name']="museumid";
        //$relations[16]['url']="/admin/museums/edit/?museumid=";
        $relations[17]['name']="Images";
        $relations[17]['id_name']="imageid";
        $relations[17]['url']="/admin/images/edit/?imageid=";
        $relations[18]['name']="Categories";
        $relations[18]['id_name']="categoryid";
        $relations[18]['url']="/admin/categories/edit/?categoryid=";
        $relations[19]['name']="Quotes categories";
        $relations[19]['id_name']="quotes_categoryid";
        $relations[19]['url']="/admin/quotes/categories/edit/?quotes_categoryid=";
        $relations[20]['name']="Biography";
        $relations[20]['id_name']="biographyid";
        $relations[20]['url']="/admin/biography/edit/?biographyid=";
        $relations[21]['name']="Articles";
        //$relations[21]['id_name']="articleid";
        //$relations[21]['url']="/admin/articles/edit/?articleid=";
        $relations[23]['name']="Colors";
        $relations[23]['id_name']="colorid";
        $relations[23]['url']="/admin/paintings/colors/edit/?colorid=";
        $relations[24]['name']="Photos";
        $relations[24]['id_name']="painting_photoid";
        $relations[24]['url']="/admin/paintings/photos/edit/?painting_photoid=";
        $relations[25]['name']="Editions – individual";
        $relations[25]['id_name']="paintid";
        $relations[25]['url']="/admin/paintings/edit/?paintid=";
        $relations[26]['name']="Literature categories";
        $relations[26]['id_name']="books_catid";
        $relations[26]['url']="/admin/books/categories/edit/?books_catid=";

        return $relations[$typeid];
    }

    public static function delete_button($db,$options=array())
    {
        $html_print="";

        $options['text1']=addslashes($options['text1']);
        //$options['text1']=str_replace("&#39;", "‘", $options['text1']);
        //$options['text1']=str_replace("'", "‘", $options['text1']);
        $options['text1']=str_replace("&#34;", "”", $options['text1']);
        $options['text1']=str_replace('"', "”", $options['text1']);
        $options['text1']=str_replace('(', "", $options['text1']);
        $options['text1']=str_replace(')', "", $options['text1']);
        //$options['text1']=stripslashes($options['text1']);
        $options['text1']=str_replace("\'", "", $options['text1']);


        $options['text2']=addslashes($options['text2']);
        //$options['text2']=str_replace("&#39;", "‘", $options['text2']);
        //$options['text2']=str_replace("'", "‘", $options['text2']);
        $options['text2']=str_replace("&#34;", "”", $options['text2']);
        $options['text2']=str_replace('"', "”", $options['text2']);
        $options['text2']=str_replace('(', "", $options['text2']);
        $options['text2']=str_replace(')', "", $options['text2']);
        //$options['text2']=stripslashes($options['text2']);
        $options['text2']=str_replace("\'", "", $options['text2']);

        $html_print.="\t<input type='button' value='delete' title='' onclick='delete_confirm2(\"".$options['url_del']."\",\"".$options['text1']."\",\"".$options['text2']."\");' />\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    public static function page_numbering_admin($db,$pages,$selected,$show_per_page,$url1,$url,$class,$htaccess,$url_place="",$per_page_list=0)
    {
        // sp= is used a limit clause
        $url1=(isset($show_per_page))?$url1.="&sp=".$show_per_page."&p=":'';

        $mod_rewrite=explode("/", $_GET['mod_rewrite']);

        $values_url=array();
        $values_url['no_exclude']=1;
        $url_var_new="?";
        $url_var=UTILS::url_var($db,$_SERVER["QUERY_STRING"],$values_url);

        if( !empty($url_var) )
        {
            foreach( $url_var as $value )
            {
                $tmp=explode("=",$value);
                if( $tmp[0]!="p" ) $url_var_new.=$value."&amp;";
            }
        }
        $tmp="";

        if( $pages>1 )
        {
            print "\t<div class='page_numbering_admin'>\n";

            if( $per_page_list==1 )
            {
                @$class['select-per-page-admin']="select-per-page-admin";
                admin_html::page_per_page_admin($db,$pages,$show_per_page,$count,$url,$class);
            }
            #previous arrow
            if( $selected==1 ) $tmp=$pages;
            else $tmp=$selected-1;

            //if( !empty($url) ) $url_prev.="/".$url;
            //$url_prev=$url_var_new."p=".$tmp;
            $url_prev=$url1.$tmp;
            $url_prev.=$url_place;


            print "<a href='".$url_prev."' class='left-arrow' title='Previous page'>&#60;</a>\n";
            #end previous arrow

            #numbers
            if( $pages>10 && $selected>=7 ) print "<a>...</a>";

            for($i=1;$i<=$pages;$i++)
            {
                if($i==$selected) $class="class='selected'";
                else $class="";

                //$url_numb=$url_var_new."p=".$i;
                $url_numb=$url1.$i;

                //if( !empty($url) ) $url_numb.="/".$url;
                $url_numb.=$url_place;

                $link="<a href='".$url_numb."' $class title='Page ".$i."'>".$i."</a>";
                if( $pages!=$i ) $link.="&nbsp;&nbsp;";
                else $link.="&nbsp;";

                if( $pages>10 )
                {


                    $a=$selected-5;
                    $b=$selected+5;


                    if( $a<=0 )
                    {
                        $b=$b+abs($a);
                        $a=1;
                    }
                    else if( ($selected-$a)==5 )
                    {
                        $b=$selected+4;
                    }
                    else
                    {
                        $b=$selected+(10-$selected);
                    }
                    if( $b>=$pages )
                    {
                        $a=$a+($pages-$b);
                    }

                    if( $i>=$a && $i<=$b  )
                    {
                        print $link;
                        $tmp=$i;
                    }


                }
                else print $link;

            }

            if( $pages>10 && ($selected+1)<=$pages && $tmp!=$pages )     print "<a>...</a>";
            #end numbers

            #next arrow
            if( $selected==$pages ) $tmp=1;
            else $tmp=$selected+1;

            //$url_next=$url_var_new."p=".$tmp;
            $url_next=$url1.$tmp;

            $url_next.=$url_place;
            //if( !empty($url) ) $url_next.="/".$url;

            print "&nbsp;<a href='".$url_next."' class='right-arrow' title='Next page' >&#62;</a>\n";
            #end next arrow

            print "</div>\n";
        }
    }

    public static function page_per_page_admin($db,$pages,$show_per_page,$count,$url,$class=array())
    {
        if( $pages>1 || $show_per_page=="1000000" || $count>8  )
        {
            // Set page
            // Add it to select element below
            $p=$_GET['p'];

            // Uneeded concatentation characters removed
            print "<select name='sp' class='".@$class['select-per-page-admin']."' onchange=\"window.location='$url&p=$p&amp;sp='+this.value+'$relations'\">\n";
            if($show_per_page=="all") $select="selected='selected'";
            else $select="";
            if( !$show_all ) print "<option $select value='all'>".LANG_ALL."</option>\n";
            if($show_per_page==8) $select="selected='selected'";
            else $select="";
            print "<option $select value='8'>".LANG_SHOW." 8 ".LANG_PERPAGE."</option>\n";
            if($show_per_page==12) $select="selected='selected'";
            else $select="";
            print "<option $select value='12'>".LANG_SHOW." 12 ".LANG_PERPAGE."</option>\n";
            if($show_per_page==16) $select="selected='selected'";
            else $select="";
            print "<option $select value='16'>".LANG_SHOW." 16 ".LANG_PERPAGE."</option>\n";
            if($show_per_page==32) $select="selected='selected'";
            else $select="";
            print "<option $select value='32'>".LANG_SHOW." 32 ".LANG_PERPAGE."</option>\n";
            if($show_per_page==64) $select="selected='selected'";
            else $select="";
            print "<option $select value='64'>".LANG_SHOW." 64 ".LANG_PERPAGE."</option>\n";
            if( $show_all )
            {
                if($show_per_page==499) $select="selected='selected'";
                else $select="";
                print "<option $select value='499'>".LANG_SHOW." 499 ".LANG_PERPAGE."</option>\n";
                if($show_per_page==500) $select="selected='selected'";
                else $select="";
                print "<option $select value='500'>".LANG_SHOW." 500 ".LANG_PERPAGE."</option>\n";
            }
            print "</select>\n";
            #end
        }

    }

    public static function previous_next_item_admin($db,$id,$selectedid,$url,$query_next_prev,$print=1)
    {
        if( !empty($query_next_prev) )
        {
            $results_next_prev=$db->query($query_next_prev);
            $count=$db->numrows($results_next_prev);

            $i=0;
            while( $row_next_prev=$db->mysql_array($results_next_prev) )
            {
                if( $i==0 ) $first=$row_next_prev[$id];
                else $last=$row_next_prev[$id];
                $i++;
                $tmp[$i]=$row_next_prev;
                if( $row_next_prev[$id]==$selectedid ) $selected=$i;
            }

            for( $i=1;$i<=sizeof($tmp);$i++ )
            {
                if( $i==$selected-1 ) $previous=$tmp[$i][$id];
                if( $i==$selected+1 ) $next=$tmp[$i][$id];
            }

            if( empty($previous) ) $previous=$last;
            if( empty($next) ) $next=$first;

            if( !empty($selectedid) && !empty($selected) )
            {
                $buttons="\t<a href='".$url.$previous."' title='Go to previous item' class=''>\n";
                $buttons.="\t<img src='/g/base/arrow_left.gif' alt='' width='14' height='14' />\n";
                $buttons.="\t</a>\n";

                $buttons.="\t".$selected." of ".$count."\n";
                $buttons.="\t<a href='".$url.$next."' title='Go to next item' class=''>\n";
                $buttons.="\t<img src='/g/base/arrow_right.gif' alt='' width='14' height='14' />\n";
                $buttons.="\t</a>\n";

                //if( $id=="paintID" ) $buttons.="\t&nbsp;&nbsp;&nbsp;<a href='".str_replace("&", "&amp;", $_SESSION['search']['url'])."' class='a_edit_item'>Return to last search results</a>\n";
                if( $id=="paintID" ) $buttons.="\t&nbsp;&nbsp;&nbsp;<a href='".str_replace("&", "&amp;", $_SESSION['admin']['search_url'])."' class='a_edit_item'>Return to last search results</a>\n";
                if( $print ) print $buttons;
            }
            return $buttons;
        }
    }


    # SEARCH TEST

    public static function count_current_search_results($db,$values)
    {
        if( $values['categoryid']==1 )
        {
            if( !empty($values['keywords']) )
            {
                $keywords=explode(",", $values['keywords']);
                $values_search=array();
                $values_search['painting_search']=$values['painting_search'];
                $values_search['db']=$db;
                $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
                # prepare match
                $values_search_paintings=array();
                $values_search_paintings['search']=$prepear_search;
                $query_match_paintings=QUERIES_SEARCH::query_search_paintings($db,$values_search_paintings);
                # END prepare match
            }
            else $query_match_paintings="p.paintID=p.paintID";

            if( !empty($values['artworkid']) )
            {
                $query_artworkid=" AND p.artworkID='".$values['artworkid']."' ";
            }

            $query_paintings="SELECT p.paintID FROM ".TABLE_PAINTING." p WHERE p.enable=1 AND ".$query_match_paintings." ".$query_artworkid;
            $results_paintings=$db->query($query_paintings);
            $count_current=$db->numrows($results_paintings);
            while( $row=$db->mysql_array($results_paintings) )
            {
                $ids[]=$row['paintID'];
            }
            //print_r($ids);
            //print $query_paintings."<br /><br />";
        }

        if( $values['categoryid']==2 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_museums=array();
            $values_search_museums['search']=$prepear_search;
            $query_match_museums=QUERIES_SEARCH::query_search_museums($db,$values_search_museums);
            # END prepare match

            $query_museums="SELECT mu.muID FROM ".TABLE_MUSEUM." mu WHERE ".$query_match_museums." ";
            $results_museums=$db->query($query_museums);
            $count_current=$db->numrows($results_museums);
            while( $row=$db->mysql_array($results_museums) )
            {
                $ids[]=$row['muID'];
            }
        }

        if( $values['categoryid']==3 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_biography=array();
            $values_search_biography['search']=$prepear_search;
            $query_match_biography=QUERIES_SEARCH::query_search_biography($db,$values_search_biography);
            # END prepare match

            $query_biography="SELECT bi.biographyid FROM ".TABLE_BIOGRAPHY." bi WHERE ".$query_match_biography." ";
            $results_biography=$db->query($query_biography);
            $count_current=$db->numrows($results_biography);
            while( $row=$db->mysql_array($results_biography) )
            {
                $ids[]=$row['biographyid'];
            }
        }

        if( $values['categoryid']==4 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_quotes=array();
            $values_search_quotes['search']=$prepear_search;
            $query_match_quotes=QUERIES_SEARCH::query_search_quotes($db,$values_search_quotes);
            # END prepare match

            $query_quotes="SELECT q.quoteid FROM ".TABLE_QUOTES." q WHERE ".$query_match_quotes." ";
            $results_quotes=$db->query($query_quotes);
            $count_current=$db->numrows($results_quotes);
            while( $row=$db->mysql_array($results_quotes) )
            {
                $ids[]=$row['quoteid'];
            }
        }

        if( $values['categoryid']==5 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_exhibitions=array();
            $values_search_exhibitions['search']=$prepear_search;
            $query_match_exhibitions=QUERIES_SEARCH::query_search_exhibitions($db,$values_search_exhibitions);
            # END prepare match

            $query_exhibitions="SELECT e.exID FROM ".TABLE_EXHIBITIONS." e WHERE ".$query_match_exhibitions." ";
            $results_exhibitions=$db->query($query_exhibitions);
            $count_current=$db->numrows($results_exhibitions);
            while( $row=$db->mysql_array($results_exhibitions) )
            {
                $ids[]=$row['exID'];
            }
        }

        if( $values['categoryid']==6 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_locations=array();
            $values_search_locations['search']=$prepear_search;
            $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$values_search_locations);
            # END prepare match

            $query_locations="SELECT lo.locationID FROM ".TABLE_LOCATIONS." lo WHERE ".$query_match_locations." ";
            $results_locations=$db->query($query_locations);
            $count_current=$db->numrows($results_locations);
            while( $row=$db->mysql_array($results_locations) )
            {
                $ids[]=$row['locationID'];
            }
        }

        if( $values['categoryid']==7 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_lit_author=array();
            $values_search_lit_author['search']=$prepear_search;
            $query_match_lit_author=QUERIES_SEARCH::query_search_literature_author($db,$values_search_lit_author);
            # END prepare match

            $query_lit_author="SELECT b.id FROM ".TABLE_BOOKS." b WHERE ".$query_match_lit_author." ";
            $results_lit_author=$db->query($query_lit_author);
            $count_current=$db->numrows($results_lit_author);
            while( $row=$db->mysql_array($results_author) )
            {
                $ids[]=$row['id'];
            }
        }

        if( $values['categoryid']==8 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_lit_title=array();
            $values_search_lit_title['search']=$prepear_search;
            $query_match_lit_title=QUERIES_SEARCH::query_search_literature_title($db,$values_search_lit_title);
            # END prepare match

            $query_lit_title="SELECT b.id FROM ".TABLE_BOOKS." b WHERE ".$query_match_lit_title." ";
            $results_lit_title=$db->query($query_lit_title);
            $count_current=$db->numrows($results_lit_title);
            while( $row=$db->mysql_array($results_lit_title) )
            {
                $ids[]=$row['id'];
            }
        }

        if( $values['categoryid']==9 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_lit_keyword=array();
            $values_search_lit_keyword['search']=$prepear_search;
            $query_match_lit_keyword=QUERIES_SEARCH::query_search_literature_keyword($db,$values_search_lit_keyword);
            # END prepare match

            $query_lit_keyword="SELECT b.id FROM ".TABLE_BOOKS." b WHERE ".$query_match_lit_keyword." ";
            $results_lit_keyword=$db->query($query_lit_keyword);
            $count_current=$db->numrows($results_lit_keyword);
            while( $row=$db->mysql_array($results_lit_keyword) )
            {
                $ids[]=$row['id'];
            }
        }

        if( $values['categoryid']==13 )
        {
            $values_search=array();
            $values_search['db']=$db;
            $keywords=explode(",", $values['keywords']);
            $prepear_search=UTILS::prepear_search($keywords[0],$values_search);
            # prepare match
            $values_search_lit_keyword=array();
            $values_search_lit_keyword['search']=$prepear_search;
            $query_match_lit_keyword=QUERIES_SEARCH::query_search_literature($db,$values_search_lit_keyword);
            # END prepare match

            $query_lit_keyword="SELECT b.id FROM ".TABLE_BOOKS." b WHERE b.link IS NOT NULL AND b.link!='' AND (b.books_catid=6 OR b.books_catid=12 OR b.books_catid=13) AND b.enable=1 AND ".$query_match_lit_keyword." ";
            $results_lit_keyword=$db->query($query_lit_keyword);
            $count_current=$db->numrows($results_lit_keyword);
            while( $row=$db->mysql_array($results_lit_keyword) )
            {
                $ids[]=$row['id'];
            }
        }


        $return['count']=$count_current;
        $return['ids']=serialize($ids);

        return $return;

    }

    # end SEARCH TEST


    # location tr admin
    public static function draw_tr_location($db,$options=array())
    {
        print "\t<tr>\n";
        if( !empty($options['type_name']) ) $type_name=$options['type_name'];
        else $type_name="Collection/Location";
        print "\t<th class='th_align_left'>".$type_name."</th>\n";
        print "\t<td colspan='2'>\n";
        print "<a name='collection'></a>";
        print "\t<table class='admin-table' cellspacing='0'>\n";

        # country
        print "\t<tr>\n";
        print "\t<th class='th_align_left'>Country</th>\n";
        print "\t<td>\n";
        if( !empty($options['type_name']) )
        {
            $country_field_name1="countryid1";
            $country_field_name2="loan_countryid";
        }
        else
        {
            $country_field_name1="countryid";
            $country_field_name2="countryid";
        }
        if( $_GET['error'] ) $countryid=$_SESSION['new_record'][$country_field_name1];
        else $countryid=$options['row'][$country_field_name2];
        $values=array();
        $values['name']="countryid".$options['i'];
        $values['selectedid']=$countryid;
        $values['onchange']=" onchange=\"
                                                    $('cityid".$options['i']."').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                                    document.getElementById('tr-city".$options['i']."').style.display='';\" ";
        admin_html::select_locations_country($db,$values);
        if( $options['count'] && !empty($options['row']['countryid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=3&countryid=".$options['row']['countryid'])."\n";
        print "\t</td>\n";
        print "\t</tr>\n";

        # city
        if( empty($countryid) ) $style="display:none;";
        else $style="";
        print "\t<tr id='tr-city".$options['i']."' style='".$style."'>\n";
        print "\t<th class='th_align_left'>City</th>\n";
        print "\t<td>\n";
        if( !empty($options['type_name']) )
        {
            $city_field_name1="cityid1";
            $city_field_name2="loan_cityid";
        }
        else
        {
            $city_field_name1="cityid";
            $city_field_name2="cityid";
        }
        if( $_GET['error'] ) $cityid=$_SESSION['new_record'][$city_field_name1];
        else $cityid=$options['row'][$city_field_name2];
        $values=array();
        $values['name']="cityid".$options['i'];
        $values['selectedid']=$cityid;
        $values['onchange']=" onchange=\"
                                                    $('locationid".$options['i']."').load('/admin/locations/options_locations.php?countryid='+document.getElementById('countryid".$options['i']."').options[document.getElementById('countryid".$options['i']."').selectedIndex].value+'&cityid='+this.options[selectedIndex].value);
                                                    document.getElementById('tr-location".$options['i']."').style.display='';\" ";
        $values['where']=" WHERE countryid='".$countryid."' ";
        admin_html::select_locations_city($db,$values);
        if( $options['count'] && !empty($options['row']['cityid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=2&cityid=".$options['row']['cityid'])."\n";
        print "\t</td>\n";
        print "\t</tr>\n";

        # location
        if( empty($cityid) ) $style="display:none;";
        else $style="";
        print "\t<tr id='tr-location".$options['i']."' style='".$style."'>\n";
        print "\t<th class='th_align_left'>Location</th>\n";
        print "\t<td>\n";
        if( !empty($options['type_name']) )
        {
            $location_field_name1="locationid1";
            $location_field_name2="loan_locationid";
        }
        else
        {
            $location_field_name1="locationid";
            $location_field_name2="locationid";
        }
        if( $_GET['error'] ) $locationid=$_SESSION['new_record'][$location_field_name1];
        else $locationid=$options['row'][$location_field_name2];
        $values=array();
        $values['name']="locationid".$options['i'];
        $values['selectedid']=$locationid;
        $values['where']=" WHERE countryid='".$countryid."' AND cityid='".$cityid."' ";
        admin_html::select_locations($db,$values);
        if( $options['count'] && !empty($options['row']['locationid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=1&locationid=".$options['row']['locationid'])."\n";
        print "\t</td>\n";
        print "\t</tr>\n";

        # loan type - for museums
        if( $options['museums'] )
        {
            print "\t<tr id='tr-loan".$options['i']."' >\n";
            print "\t<th class='th_align_left'>Loan type</th>\n";
            print "\t<td>\n";
            if( $_GET['error'] ) $optionid_loan_type=$_SESSION['new_record']['optionid_loan_type'];
            else $optionid_loan_type=$options['row']['optionid_loan_type'];
            $options_select_loan_type=array();
            $options_select_loan_type['name']="";
            $options_select_loan_type['id']="";
            select_dropdown($db,1,$optionid_loan_type,$options_select_loan_type);
            print "\t</td>\n";
            print "\t</tr>\n";

            # additional collection information - for museums
            $loan_location=array();
            if( $_GET['error'] ) $loan_locationid=$_SESSION['new_record']['locationid1'];
            else $loan_locationid=$options['row']['loan_locationid'];
            $loan_location['loan_locationid']=$loan_locationid;
            if( $_GET['error'] ) $loan_cityid=$_SESSION['new_record']['cityid1'];
            else $loan_cityid=$options['row']['loan_cityid'];
            $loan_location['loan_cityid']=$loan_cityid;
            if( $_GET['error'] ) $loan_countryid=$_SESSION['new_record']['countryid1'];
            else $loan_countryid=$options['row']['loan_countryid'];
            $loan_location['loan_countryid']=$loan_countryid;
            $options_tr_location=array();
            $options_tr_location['row']=$loan_location;
            $options_tr_location['count']=$options['count'];
            $options_tr_location['html']=$options['html'];
            $options_tr_location['type_name']="Additional collection";
            $options_tr_location['preview']=1;
            $options_tr_location['i']=1;
            admin_html::draw_tr_location($db,$options_tr_location);
        }



        print "\t</table>\n";

        if( !$options['preview'] )
        {
            $options_location=array();
            $options_location['row']=$options['row'];
            $options_location['museum']=1;
            $options_location['html']=$options['html'];
            $location=location($db,$options_location);
            print $location;
        }

        print "\t</td>\n";
        print "\t</tr>\n";
    }
    # END location th admin


    # SELECT DROP DOWN LISTS

    public static function select_relations($db,$name,$selectedid,$onchange,$class=array(),$disable,$where="",$multiple="")
    {
        print "<select name='".$name."' id='".$name."' ".$onchange." ".$multiple." class='".@$class['select_books_catid']."' >\n";
        if( empty($multiple) ) print "<option value='0'>Select..</option>\n";
        if( $selectedid==14 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='14' $selected >Associated works - paintid</option>\n";
        if( $selectedid==13 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='13' $selected >Atlas - paintid</option>\n";
        if( $selectedid==11 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='11' $selected >Audios - audioid</option>\n";
        if( $selectedid==8 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='8' $selected >Biog. photos - photoid</option>\n";
        if( $selectedid==23 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='23' $selected >Colors - colorid</option>\n";
        if( $selectedid==25 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='25' $selected >Editions – individual - paintid</option>\n";
        if( $selectedid==4 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='4' $selected >Exhibitions - exhibitionid</option>\n";
        if( $selectedid==5 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='5' $selected >Exh. Installations - installationid</option>\n";
        if( $selectedid==17 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='17' $selected >Image - imageid</option>\n";
        if( $selectedid==12 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='12' $selected >Literature - bookid</option>\n";
        if( $selectedid==3 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='3' $selected >Microsites - micrositeid</option>\n";
        if( $selectedid==7 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='7' $selected >News - newsid</option>\n";
        if( $selectedid==1 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='1' $selected >Paintings - paintid</option>\n";
        if( $selectedid==15 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='15' $selected >Paintings notes images - noteid</option>\n";
        if( $selectedid==24 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='24' $selected >Painting photos - painting_photoid</option>\n";
        if( $selectedid==9 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='9' $selected >Quotes - quoteid</option>\n";
        if( $selectedid==2 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='2' $selected >Sales history - saleid</option>\n";
        if( $selectedid==10 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='10' $selected >Videos - videoid</option>\n";
        //if( $selectedid==6 ) $selected="selected='selected'";
        //else $selected="";
        //print "<option value='6' $selected >Rel. videos - related_videoid</option>\n";
        /*
        if( $selectedid==16 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='16' $selected >Museums - museumid</option>\n";
        if( $selectedid==18 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='18' $selected >Categories - categoryid</option>\n";
        if( $selectedid==19 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='19' $selected >Quotes categories - quotes_categoryid</option>\n";
        if( $selectedid==20 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='20' $selected >Biography - biographyid</option>\n";
        if( $selectedid==21 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='21' $selected >Links articles - articleid</option>\n";
        if( $selectedid==22 ) $selected="selected='selected'";
        else $selected="";
        print "<option value='22' $selected >Files - fileid</option>\n";
         */
        //if( $selectedid==26 ) $selected="selected='selected'";
        //else $selected="";
        //print "<option value='26' $selected >Literature categories - books_catid</option>\n";
        print "</select>\n";
    }

    public static function select_relations_sort($db,$name,$typeid1,$itemid1,$typeid2,$selectedid,$class=array(),$onchange="",$disable="")
    {
        //$results=$db->query("SELECT typeid1,itemid1,typeid2,itemid2,sort FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$typeid1."' AND itemid1='".$itemid1."' AND typeid2='".$typeid2."' ) OR ( typeid2='".$typeid1."' AND itemid2='".$itemid1."' AND typeid1='".$typeid2."' ) ORDER BY sort ASC, relationid DESC ");
        //$count=$db->numrows($results);
        print "<select name='".$name."' id='".$name."' ".$onchange." ".$multiple." class='".@$class['select_relations_sort']."' >\n";
        print "<option value=''>First</option>\n";
        /*
        $i=0;
        while( $row=$db->mysql_array($results) )
        {
            $i++;
            if( $i>1 )
            {
                if( $selectedid==1 ) $selected="selected='selected'";
                else $selected="";
                print "<option value='".$row['sort']."' $selected >Before";
                    if( $typeid2==1 )
                    {
                        $paintid=UTILS::get_relation_id($db,$typeid2,$row);
                        $results_painting=$db->query("SELECT artworkID, titleEN, titleDE, number FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' LIMIT 1");
                        $row_painting=$db->mysql_array($results_painting);
                        print " - ".$paintid;
                        if( !empty($row_painting['number']) ) print " - ".$row_painting['number'];
                        if( !empty($row_painting['titleEN']) ) print " - ".$row_painting['titleEN'];
                        if( !empty($row_painting['titleDE']) ) print " - ".$row_painting['titleDE'];
                    }
                    if( $typeid2==2 )
                    {
                        $saleid=UTILS::get_relation_id($db,$typeid2,$row);
                        $query_sale_history="SELECT a.house AS auction_house, s.saleName AS sale_name, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.saleID='".$saleid."'";
                        $results_sale=$db->query($query_sale_history);
                        $row_sale=$db->mysql_array($results_sale);
                        print " - ".$saleid;
                        if( !empty($row_sale['sale_date']) ) print " - ".$row_sale['sale_date'];
                        if( !empty($row_sale['auction_house']) ) print " - ".$row_sale['auction_house'];
                        if( !empty($row_sale['sale_name']) ) print " - ".$row_sale['sale_name'];
                    }
                    if( $typeid2==4 )
                    {
                        $exhibitionid=UTILS::get_relation_id($db,$typeid2,$row);
                        $results_exh=$db->query("SELECT title_original, locationEN FROM ".TABLE_EXHIBITIONS." WHERE exID='".$exhibitionid."' LIMIT 1");
                        $row_exh=$db->mysql_array($results_exh);
                        print " - ".$exhibitionid;
                        if( !empty($row_exh['title_original']) ) print " - ".$row_exh['title_original'];
                        if( !empty($row_exh['locationEN']) ) print " - ".$row_exh['locationEN'];
                    }
                print "</option>\n";
            }
        }
        if( $count>0 ) print "<option value='".($count+1)."'>Last</option>\n";
         */
        print "</select>\n";
    }

    public static function select_books_museums($db,$name,$selectedid,$onchange="",$class=array(),$disable="",$where="",$multiple="")
    {
        $query_select_museums="SELECT muID, museum FROM ".TABLE_MUSEUM." ORDER BY museum";
        $results_select_museums=$db->query($query_select_museums);
        print "<select name='".$name."' id='".$name."' $onchange class='".@$class['select_sub_categories']."' style='width:300px' >\n";
        print "<option value='0' class='select'>Select..</option>\n";
        while( $row=$db->mysql_array($results_select_museums) )
        {
            if( $row['muID']==$selectedid ) $selected="selected='selected'";
            else $selected="";
            print "<option class='".$class."' value='".$row['muID']."' $selected >".$row['museum']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_articles_categories($db,$name,$selectedid,$onchange="",$class=array(),$disable="",$where="",$multiple="")
    {
        $results=$db->query("SELECT articles_catid,title_en,title_de,title_zh FROM ".TABLE_ARTICLES_CATEGORIES." ORDER BY sort ");
        print "<select name='".$name."' id='".$name."' ".$onchange." ".$multiple." class='".@$class['select_books_catid']."' >\n";
        if( empty($multiple) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['articles_catid']==$selectedid || @in_array($row['articles_catid'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            $row=UTILS::html_decode($row);
            $title=UTILS::row_text_value($db,$row,"title");
            print "<option value='".$row['articles_catid']."' ".$selected." >".$title."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_books_type_ephemera($db,$values=array())
    {
        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_books_type_ephemera']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        if( $values['selectedid']==1 || @in_array(1, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='1' ".$selected." >Postcard</option>\n";
        if( $values['selectedid']==2 || @in_array(2, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='2' ".$selected." >Leaflet</option>\n";
        if( $values['selectedid']==3 || @in_array(3, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='3' ".$selected." >Invitation card</option>\n";
        if( $values['selectedid']==4 || @in_array(4, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='4' ".$selected." >Exhibition guide</option>\n";
        if( $values['selectedid']==5 || @in_array(5, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='5' ".$selected." >Press pack</option>\n";
        if( $values['selectedid']==6 || @in_array(6, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='6' ".$selected." >Program</option>\n";
        print "</select>\n";
    }

    public static function select_video_categories($db,$options=array())
    {
        $html_print="";

        $query="SELECT categoryid,title_en,title_de,title_zh FROM ".TABLE_VIDEO_CATEGORIES." ".$options['where']." ORDER BY sort_en ";
        $result=$db->query($query);
        $count=$db->numrows($result);

        $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." class='".$options['class']['select_categories']."' >\n";
        $html_print.="<option value='0' class='option_select'>".LANG_SELECT."</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( $options['sub_categories'] )
            {
                $query_sub="SELECT categoryid,title_en,title_de,title_zh FROM ".TABLE_VIDEO_CATEGORIES." WHERE sub_categoryid='".$row['categoryid']."' ORDER BY sort_en  ";
                $result_sub=$db->query($query_sub);
                $count_sub=$db->numrows($result_sub);
            }

            if( $row['categoryid']==$options['selectedid'] ) $selected="selected='selected'";
            else $selected="";
            if( $count_sub>0 )
            {
                $disabled="disabled='disabled'";
            }
            $html_print.="<option class='option_cat' value='".$row['categoryid']."' ".$selected." ".$disabled." >".$row['title_en']."</option>\n";

            if( $options['sub_categories'] )
            {
                while( $row_sub=$db->mysql_array($result_sub) )
                {
                    if( $row_sub['categoryid']==$options['selectedid'] ) $selected="selected='selected'";
                    else $selected="";
                    $html_print.="<option class='option_sub_cat' value='".$row_sub['categoryid']."' ".$selected." >".$row_sub['title_en']."</option>\n";
                }
            }
        }
        $html_print.="</select>\n";

        $return=array();
        $return['count']=$count;
        $return['count_sub']=$count_sub;
        $return['html_print']=$html_print;

        return $return;

    }


    public static function select_medium($db,$name,$selectedid,$onchange,$class=array(),$disable,$where="",$multiple="")
    {
        $result=$db->query("SELECT mID,mediumEN FROM ".TABLE_MEDIA." ORDER BY mediumEN ASC");
        print "<select name='".$name."' id='".$name."' $onchange $multiple class='".@$class['select_admin_medium_multi']."' >\n";
        if( empty($multiple) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( $row['mID']==$selectedid || @in_array($row['mID'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['mID']."' $selected >".$row['mediumEN']."</option>\n";
        }
        print "</select>\n";
    }


    public static function select_sub_categories($db,$name,$selectedid,$onchange,$class=array(),$disable,$where="")
    {
        $query="SELECT quotes_categoryid,sub_categoryid,title_en FROM ".TABLE_QUOTES_CATEGORIES." WHERE ( sub_categoryid='' || sub_categoryid IS NULL || sub_categoryid=0 ) ".$where." ORDER BY sort ";
        $result=$db->query($query);
        print "<select name='".$name."' id='".$name."' $onchange class='".@$class['select_sub_categories']."' >\n";
        print "<option value='0' class='select'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( $row['quotes_categoryid']==$selectedid ) $selected="selected='selected'";
            else $selected="";
            if( !empty($row['sub_categoryid']) ) $class="option_quote_sub_cat";
            else $class="";
            print "<option class='".$class."' value='".$row['quotes_categoryid']."' $selected >".$row['title_en']."</option>\n";

            $query_sub="SELECT quotes_categoryid,sub_categoryid,title_en FROM ".TABLE_QUOTES_CATEGORIES." WHERE sub_categoryid='".$row['quotes_categoryid']."' ".$where." ORDER BY sort ";
            $result_sub=$db->query($query_sub);
            while( $row_sub=$db->mysql_array($result_sub) )
            {
                if( $row_sub['quotes_categoryid']==$selectedid ) $selected="selected='selected'";
                else $selected="";
                if( !empty($row_sub['sub_categoryid']) ) $class="option_quote_sub_cat";
                else $class="";
                print "<option class='".$class."' value='".$row_sub['quotes_categoryid']."' $selected >".$row_sub['title_en']."</option>\n";
            }
        }
        print "</select>\n";
    }

    public static function select_categories_admin($db,$name,$selectedid,$onchange="",$class=array(),$disable="",$where="",$multiple="")
    {
        $query="SELECT catID,categoryEN FROM ".TABLE_CATEGORY." ".$where." ORDER BY sortEN ASC";
        $result=$db->query($query);
        print "\t<select name='".$name."' ".$multiple." id='".$name."' $onchange class='".@$class['select_category']." ".@$class['select_admin_cat_multi']."' >\n";
        if( empty($multiple) ) print "\t<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result,0) )
        {
            if( $selectedid==$row['catID'] || @in_array($row['catID'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            print "\t<option $selected value='".$row['catID']."'>".$row['categoryEN']."</option>\n";
        }
        print "\t</select>\n";
    }

    public static function select_artworks($db,$name,$selectedid,$onchange="",$class=array(),$disable="",$where="WHERE artworkID!=10 AND artworkID!=11",$multiple="",$show_paintings=1,$select_title="Select...")
    {
        $results=$db->query("SELECT artworkID,artworkEN FROM ".TABLE_ARTWORKS." ".$where." ORDER BY sort ASC");
        print "\t<select name='".$name."' id='".$name."' class='".$class."' $onchange $multiple >\n";
        if( empty($multiple) ) print "<option value='0'>".$select_title."</option>\n";
        if( $selectedid=="paintings" ) $selected=" selected='selected' ";
        else $selected="";
        if( $show_paintings ) print "<option $selected value='paintings'>Paintings</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['artworkID']==$selectedid || @in_array($row['artworkID'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            print "\t<option ".$selected." value='".$row['artworkID']."'>".$row['artworkEN']."</option>\t";
        }
        print "\t</select>\n";
    }


    public static function select_sale_type($db,$name,$selectedid,$onchange,$class=array(),$disable)
    {
        print "\t<select name='".$name."' id='".$name."' $onchange >\n";
        print "\t<option value='0'>Select..</option>\n";
        if( $selectedid ==1 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='1' $selected >Premium</option>\n";
        if( $selectedid==2 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='2' $selected >Hammer</option>\n";
        if( $selectedid==3 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='3' $selected >Unknown</option>\n";
        print "</select>\n";
    }

    public static function select_currencies($db,$name,$selectedid,$onchange,$class=array(),$disable,$exclude=array())
    {
        $result=$db->query("SELECT currID,symbol,currency FROM ".TABLE_CURRENCY." ");
        print "<select name='".$name."' id='".$name."' $onchange $disable >\n";
        print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( @!in_array($row['currID'], $exclude) )
            {
                if( $row['currID']==$selectedid ) $selected="selected='selected'";
                else $selected="";
                print "<option value='".$row['currID']."' $selected >".$row['symbol']."</option>\n";
            }
        }
        print "</select>\n";
    }

    public static function select_auction_houses($db,$name,$selectedid,$onchange="",$class=array(),$disable="",$multiple="")
    {
        $result=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." ORDER BY house ASC");
        print "<select name='".$name."' id='".$name."' class='".@$class['select_admin_auction_multi']."' ".$onchange." ".$multiple."  >\n";
        if( empty($multiple) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            $row=UTILS::html_decode($row);
            if( $row['ahID']==$selectedid || @in_array($row['ahID'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['ahID']."' $selected >";
            $values_location=array();
            $values_location['row']=$row;
            $location=location($db,$values_location);
            print $row['house'].", ".$location;
            print "</option>\n";
        }
        print "</select>\n";
    }


    public static function select_microsites($db,$name,$selectedid,$onchange,$class=array(),$disable)
    {
        $result=$db->query("SELECT micrositeid,title_en FROM ".TABLE_MICROSITES." ORDER BY sort ");
        print "<select name='".$name."' id='".$name."' $onchange >\n";
        print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( $row['micrositeid']==$selectedid ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['micrositeid']."' $selected >".$row['title_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_versions($db,$name,$selectedid,$onchange,$class=array(),$disable)
    {
        $result=$db->query("SELECT versionid,title_en FROM ".TABLE_MICROSITES_VERSIONS." ORDER BY sort ");
        print "<select name='".$name."' id='".$name."' $onchange >\n";
        print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($result) )
        {
            if( $row['versionid']==$selectedid ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['versionid']."' $selected >".$row['title_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function get_exhibitions($db,$id,$select,$onchange="")
    {
        $query=$db->query("SELECT * FROM ".TABLE_EXHIBITIONS." ORDER BY titleEN ASC");
        print "\t<select name='exID".$id."' id='exID".$id."' style='width:300px' ".$onchange." >\n";
        print "\t<option value=''>Select...</option>\n";
        while($row=$db->mysql_array($query))
        {
            if($row['exID']==$select)$selected="selected='selected'";
            else $selected="";
            print "\t<option $selected value='".$row['exID']."'>";
            print $row['titleEN'];
            if( !empty($row['locationEN']) )
            {
                print "  @  ";
                print $row['locationEN'];
                print "\t</option>\n";
            }
        }
        print "\t</select>\n";
    }

    public static function select_museums($db,$id,$selectedid,$onchange="",$class=array(),$disable="",$multiple="")
    {
        print "<select name='".$id."' id='".$id."' class='".@$class['select-museums']."' ".$multiple." ".$onchange." ".$disable." ".$multiple." >";
        if( empty($multiple) ) print "<option value=''>Select..</option>";
        $results=$db->query("SELECT muID,museum FROM ".TABLE_MUSEUM." ORDER BY museum ASC");
        while( $row=$db->mysql_array($results) )
        {
            $query_where_paintings=" WHERE ( typeid1=16 AND itemid1='".$row['muID']."' AND typeid2=1 ) OR ( typeid2=16 AND itemid2='".$row['muID']."' AND typeid1=1 ) ";
            $query_paintings=QUERIES::query_relations($db,$query_where_paintings);
            $results_paintings=$db->query($query_paintings['query_without_limit']);
            $count_paintings=$db->numrows($results_paintings);
            if( $row['muID']==$selectedid ) $selected="selected='selected'";
            else $selected="";
            $text_limit="130";
            if( !empty($row['museum']) )
            {
                if( strlen($row['museum'])>=$text_limit ) $dots="...";
                else $dots="";
                $museum=mb_substr($row['museum'], 0, $text_limit).$dots;
            }
            else $museum="";
            print "\t<option $selected value='".$row['muID']."'>".$museum." (".$count_paintings.")</option>\n";
        }
        print "</select>";
    }

    public static function get_medium($db,$id,$select)
    {
        print "<select name='mID".$id."' id='mID".$id."'>";
        print "<option value=''>Select..</option>";
        $results=$db->query("SELECT mID,mediumEN FROM ".TABLE_MEDIA." ORDER BY mediumEN ASC");
        while( $row=$db->mysql_array($results) )
        {
            $row=UTILS::html_decode($row);
            if( $row['mID']==$select ) $selected="selected='selected'";
            else $selected="";
            $text_limit="130";
            if( !empty($row['mediumEN']) )
            {
                if( strlen($row['mediumEN'])>=$text_limit ) $dots="...";
                else $dots="";
                $medium_en=mb_substr($row['mediumEN'], 0, $text_limit).$dots;
            }
            else $medium_en="";
            $medium=$medium_en;
            print "\t<option $selected value='".$row['mID']."'>".$medium."</option>\n";
        }
        print "</select>";
    }

    public static function get_years($id,$select,$class,$multiple="")
    {
        print "<select name='year".$id."' id='year".$id."' $class $multiple >";
        if( empty($multiple) ) print "<option value=''>Select..</option>";

        if( $select=='1949' ) $selected=" selected='selected' ";else $selected="";
        if( empty($selected) && is_array($select) )
        {
            if( in_array('1949', $select) ) $selected="selected='selected'"; else $selected="";
        }
        print "<option $selected  value='1949'>1949</option>\n";

        for($i=1962;$i<=date("Y");$i++)
        {
            if( $select==$i ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array($i, $select) ) $selected="selected='selected'"; else $selected="";
            }
            print "\t<option $selected value='$i'>$i</option>\n";


            #1962–1966
            if( $select=="1962–1966" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1962–1966", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1962="\t<option $selected value='1962–1966'>1962–1966</option>\n";
            if( $i=="1962" ) print $year_1962;
            elseif( is_array($select) )
            {
                if( in_array("1962", $select) ) $year_1962;
            }
            #1962–1968
            if( $select=="1962–1968" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1962–1968", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1962="\t<option $selected value='1962–1968'>1962–1968</option>\n";
            if( $i=="1962" ) print $year_1962;
            elseif( is_array($select) )
            {
                if( in_array("1962", $select) ) $year_1962;
            }
            #1962–2013
            if( $select=="1962–2013" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1962–2013", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1962="\t<option $selected value='1962–2013'>1962–2013</option>\n";
            if( $i=="1962" ) print $year_1962;
            elseif( is_array($select) )
            {
                if( in_array("1962", $select) ) $year_1962;
            }
            #1963-1966
            if( $select=="1963-1966" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1963-1966", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1963="\t<option $selected value='1963-1966'>1963-1966</option>\n";
            if( $i=="1963" ) print $year_1963;
            elseif( is_array($select) )
            {
                if( in_array("1963", $select) ) $year_1963;
            }
            #1963/1971
            if( $select=="1963/1971" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1963/1971", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1963="\t<option $selected value='1963/1971'>1963/1971</option>\n";
            if( $i=="1963" ) print $year_1963;
            elseif( is_array($select) )
            {
                if( in_array("1963", $select) ) $year_1963;
            }
            #1964-1967
            if( $select=="1964-1967" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1964-1967", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1964="\t<option $selected value='1964-1967'>1964-1967</option>\n";
            if( $i=="1964" ) print $year_1964;
            elseif( is_array($select) )
            {
                if( in_array("1964", $select) ) $year_1964;
            }
            #1965/68
            if( $select=="1965/68" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1965/68", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1965="\t<option $selected value='1965/68'>1965/68</option>\n";
            if( $i=="1965" ) print $year_1965;
            elseif( is_array($select) )
            {
                if( in_array("1965", $select) ) $year_1965;
            }

            # 1966/1996
            if( $select=="1966/1996" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1966/1996", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1966="\t<option $selected value='1966/1996'>1966/1996</option>\n";
            if( $i=="1966" ) print $year_1966;
            elseif( is_array($select) )
            {
                if( in_array("1966", $select) ) $year_1966;
            }

            # 1966/2005
            if( $select=="1966/2005" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1966/2005", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1966="\t<option $selected value='1966/2005'>1966/2005</option>\n";
            if( $i=="1966" ) print $year_1966;
            elseif( is_array($select) )
            {
                if( in_array("1966", $select) ) $year_1966;
            }

            # 1969/1970
            if( $select=="1969/1970" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1969/1970", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1969="\t<option $selected value='1969/1970'>1969/1970</option>\n";
            if( $i=="1969" ) print $year_1969;
            elseif( is_array($select) )
            {
                if( in_array("1969", $select) ) $year_1969;
            }

            # 1969/1971
            if( $select=="1969/1971" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1969/1971", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1969="\t<option $selected value='1969/1971'>1969/1971</option>\n";
            if( $i=="1969" ) print $year_1969;
            elseif( is_array($select) )
            {
                if( in_array("1969", $select) ) $year_1969;
            }

            # 1970-1975
            if( $select=="1970-1975" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1970-1975", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1970="\t<option $selected value='1970-1975'>1970-1975</option>\n";
            if( $i=="1970" ) print $year_1970;
            elseif( is_array($select) )
            {
                if( in_array("1970", $select) ) $year_1970;
            }

            # 1970-1976
            if( $select=="1970-1976" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1970-1976", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1970="\t<option $selected value='1970-1976'>1970-1976</option>\n";
            if( $i=="1970" ) print $year_1970;
            elseif( is_array($select) )
            {
                if( in_array("1970", $select) ) $year_1970;
            }

            # 1971/1987
            if( $select=="1971/1987" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1971/1987", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1971_1987="\t<option $selected value='1971/1987'>1971/1987</option>\n";
            if( $i=="1971" ) print $year_1971_1987;
            elseif( is_array($select) )
            {
                if( in_array("1971", $select) ) $year_1971_1987;
            }

            # 1971/72
            if( $select=="1971/72" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1971/72", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1971="\t<option $selected value='1971/72'>1971/72</option>\n";
            if( $i=="1971" ) print $year_1971;
            elseif( is_array($select) )
            {
                if( in_array("1971", $select) ) $year_1971;
            }

            # 1972/1973
            if( $select=="1972/1973" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1972/1973", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1972="\t<option $selected value='1972/1973'>1972/1973</option>\n";
            if( $i=="1972" ) print $year_1972;
            elseif( is_array($select) )
            {
                if( in_array("1972", $select) ) $year_1972;
            }

            # 1973/1974
            if( $select=="1973/1974" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1973/1974", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1973="\t<option $selected value='1973/1974'>1973/1974</option>\n";
            if( $i=="1973" ) print $year_1973;
            elseif( is_array($select) )
            {
                if( in_array("1973", $select) ) $year_1973;
            }



            # 1974/1984
            if( $select=="1974/1984" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1974/1984", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1974="\t<option $selected value='1974/1984'>1974/1984</option>\n";
            if( $i=="1974" ) print $year_1974;
            elseif( is_array($select) ) {
                if (in_array("1974", $select)) $year_1974;
            }


            # 1976/1991
            if( $select=="1976/1991" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1976/1991", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1976="\t<option $selected value='1976/1991'>1976/1991</option>\n";
            if( $i=="1976" ) print $year_1976;
            elseif( is_array($select) )
            {
                if( in_array("1976", $select) ) $year_1976;
            }
            # 1984-1988
            if( $select=="1984-1988" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1984-1988", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1984="\t<option $selected value='1984-1988'>1984-1988</option>\n";
            if( $i=="1984" ) print $year_1984;
            elseif( is_array($select) )
            {
                if( in_array("1984", $select) ) $year_1984;
            }

            # 1978/1984/1988
            if( $select=="1978/1984/1988" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1978/1984/1988", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1978="\t<option $selected value='1978/1984/1988'>1978/1984/1988</option>\n";
            if( $i=="1978" ) print $year_1978;
            elseif( is_array($select) )
            {
                if( in_array("1978", $select) ) $year_1978;
            }

            # 1984/1988
            if( $select=="1984/1988" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1984/1988", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1984="\t<option $selected value='1984-1988'>1984/1988</option>\n";
            if( $i=="1984" ) print $year_1984;
            elseif( is_array($select) )
            {
                if( in_array("1984", $select) ) $year_1984;
            }

            # 1986/88
            if( $select=="1986/88" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1986/88", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1986="\t<option $selected value='1986/88'>1986/88</option>\n";
            if( $i=="1986" ) print $year_1986;
            elseif( is_array($select) )
            {
                if( in_array("1986", $select) ) $year_1986;
            }

            # 1989/92
            if( $select=="1989/92" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1989/92", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1989="\t<option $selected value='1989/92'>1989/92</option>\n";
            if( $i=="1989" ) print $year_1989;
            elseif( is_array($select) )
            {
                if( in_array("1989", $select) ) $year_1989;
            }

            # c. 1992
            if( $select=="c. 1992" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("c. 1992", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1992="\t<option $selected value='c. 1992'>c. 1992</option>\n";
            if( $i=="1992" ) print $year_1992;
            elseif( is_array($select) )
            {
                if( in_array("1992", $select) ) $year_1992;
            }

            # 1992-1994
            if( $select=="1992-1994" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1992-1994", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1992="\t<option $selected value='1992-1994'>1992-1994</option>\n";
            if( $i=="1992" ) print $year_1992;
            elseif( is_array($select) )
            {
                if( in_array("1992", $select) ) $year_1992;
            }

            # 1990-1994
            if( $select=="1990-1994" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1990-1994", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1990="\t<option $selected value='1990-1994'>1990-1994</option>\n";
            if( $i=="1990" ) print $year_1990;
            elseif( is_array($select) )
            {
                if( in_array("1990", $select) ) $year_1990;
            }


            # 1993/1995
            if( $select=="1993/1995" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1993/1995", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1993="\t<option $selected value='1993/1995'>1993/1995</option>\n";
            if( $i=="1993" ) print $year_1993;
            elseif( is_array($select) )
            {
                if( in_array("1993", $select) ) $year_1993;
            }

            # 1995/1996
            if( $select=="1995/1996" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1995/1996", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1995="\t<option $selected value='1995/1996'>1995/1996</option>\n";
            if( $i=="1995" ) print $year_1995;
            elseif( is_array($select) )
            {
                if( in_array("1995", $select) ) $year_1995;
            }

            # 1997/1998
            if( $select=="1997/1998" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("1997/1998", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1997="\t<option $selected value='1997/1998'>1997/1998</option>\n";
            if( $i=="1997" ) print $year_1997;
            elseif( is_array($select) )
            {
                if( in_array("1997", $select) ) $year_1997;
            }

            # c. 1999
            if( $select=="c. 1999" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("c. 1999", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_1999="\t<option $selected value='c. 1999'>c. 1999</option>\n";
            if( $i=="1999" ) print $year_1999;
            elseif( is_array($select) )
            {
                if( in_array("1999", $select) ) $year_1999;
            }

            # 2002/2004
            if( $select=="2002/2004" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("2002/2004", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_2002="\t<option $selected value='2002/2004'>2002/2004</option>\n";
            if( $i=="2002" ) print $year_2002;
            elseif( is_array($select) )
            {
                if( in_array("2002", $select) ) $year_2002;
            }

            # 2002/2010
            if( $select=="2002/2010" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("2002/2010", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_2002="\t<option $selected value='2002/2010'>2002/2010</option>\n";
            if( $i=="2002" ) print $year_2002;
            elseif( is_array($select) )
            {
                if( in_array("2002", $select) ) $year_2002;
            }

            # 2002/2011
            if( $select=="2002/2011" ) $selected="selected='selected'"; else $selected="";
            if( empty($selected) && is_array($select) )
            {
                if( in_array("2002/2011", $select) ) $selected="selected='selected'"; else $selected="";
            }
            $year_2002="\t<option $selected value='2002/2011'>2002/2011</option>\n";
            if( $i=="2002" ) print $year_2002;
            elseif( is_array($select) )
            {
                if( in_array("2002", $select) ) $year_2002;
            }

        }

        # not dated
        if($select=='n.d.' )
        {
            $selected=" selected='selected' ";
        }
        elseif( empty($selected) && is_array($select) )
        {
            if( in_array('n.d.', $select) ) $selected="selected='selected'"; else $selected="";
        }
        else $selected="";
        print "<option $selected value='n.d.'>n.d.</option>";

        # not available
        if($select=='n.a.' )
        {
            $selected=" selected='selected' ";
        }
        elseif( empty($selected) && is_array($select) )
        {
            if( in_array('n.a.', $select) ) $selected="selected='selected'"; else $selected="";
        }
        else $selected="";
        print "<option $selected value='n.a.'>Not available(n.a.)</option>";

        print "</select>";
    }

    public static function get_artworks($db,$id,$select,$class,$javascript,$options=array())
    {
        //$query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID!=10 and artworkID!=11 ORDER BY artworkEN ASC");
        $query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID!=10 and artworkID!=11 ORDER BY sort ASC");
        echo "  <select name='artworkID".$id."' id='artworkID".$id."' $class $javascript style='".$options['style']."' >\n
            <option value=''>Select..</option>\n";
        while($row=$db->mysql_array($query))
        {
            if($row['artworkID']==$select)$selected="selected='selected'";
            else $selected="";
            //echo "<option $selected value='".$row['artworkID']."'>".$row['artworkEN']." - ".$row['artwork_dir_name']."</option>";
            echo "<option $selected value='".$row['artworkID']."'>".$row['artworkEN']."</option>";
        }
        echo "</select>\n";
    }


    public static function get_editions($db,$id,$select)
    {
        print "<select name='editionID".$id."' id='editionID".$id."' >";
        print "<option value=''>Select..</option>";
        $query=$db->query("SELECT * FROM ".TABLE_PAINTING." where artworkID=4 AND editionID IS NULL ORDER BY titleEN ASC");
        while($row=$db->mysql_array($query))
        {
            if($row['paintID']==$select)
            {
                $selected="selected='selected'";
            }
            else
            {
                $selected="";
            }
            $text_limit="60";
            if( !empty($row['titleEN']) && !empty($row['titleDE']) ) $dash=" - ";
            else $dash="";
            if( !empty($row['titleEN']) )
            {
                if( strlen($row['titleEN'])>=$text_limit ) $dots="...";
                else $dots="";
                $title_en=mb_substr($row['titleEN'], 0, $text_limit).$dots;
            }
            else $title_en="";
            if( !empty($row['titleDE']) )
            {
                if( strlen($row['titleDE'])>=$text_limit ) $dots="...";
                else $dots="";
                $title_de=mb_substr($row['titleDE'], 0, $text_limit).$dots;
            }
            else $title_de="";
            $title=$title_en.$dash.$title_de;
            print "\t<option $selected value='".$row['paintID']."'>".$title."</option>\n";
        }
        print "</select>";
    }

    public static function select_news_type($db,$name,$selectedid,$class=array())
    {
        print "\t<select name='".$name."' id='".$name."' class='".@$class['select_news_type']."' >\n";
        /*
        print "\t<option value=''>Select...</option>\n";
        if( $selectedid==1 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option ".$selected." value='1'>Exhibitions</option>\n";
        if( $selectedid==2 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option ".$selected." value='2'>Publications</option>\n";
        */
        if( $selectedid==3 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option ".$selected." value='3'>Talks</option>\n";
        print "\t</select>\n";
    }

    public static function select_videos($db,$name,$selectedid,$class=array())
    {
        print "\t<select name='".$name."' id='".$name."' class='".@$class['select_news_type']."' >\n";
        print "\t<option value=''>Select...</option>\n";
        $results=$db->query("SELECT * FROM ".TABLE_VIDEO." ORDER BY sort ASC ");
        while( $row=$db->mysql_array($results) )
        {
            if( $selectedid==$row['videoID'] ) $selected="selected='selected'";
            else $selected="";
            print "\t<option ".$selected." value='".$row['videoID']."'>".$row['titleEN']."</option>\n";
        }
        print "\t</select>\n";
    }

    public static function select_display_tab_images($db,$name,$selectedid,$class=array())
    {
        print "\t<select name='".$name."' id='".$name."' class='".@$class['select_display_tab_images']."' >\n";
        //print "\t<option value=''>Select...</option>\n";
        print "\t<option value='0'>Display 3 images per line text beneath image</option>\n";
        if( $selectedid==1 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option ".$selected." value='1'>Display 1 image per line text on right</option>\n";
        print "\t</select>\n";
    }

    public static function select_languages($db,$name,$selectedid,$class=array())
    {
        print "\t<select name='".$name."' id='".$name."' class='".@$class['select_languages']."' >\n";
        if( $selectedid==1 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='1' ".$selected.">English</option>\n";
        if( $selectedid==2 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='2' ".$selected.">German</option>\n";
        if( $selectedid==3 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='3' ".$selected.">French</option>\n";
        if( $selectedid==4 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='4' ".$selected.">Italian</option>\n";
        if( $selectedid==5 ) $selected="selected='selected'";
        else $selected="";
        print "\t<option value='5' ".$selected.">Chinese</option>\n";
        print "\t</select>\n";
    }


    public static function select_painting_photo_type($db,$values=array())
    {
        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_paint_phot']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        if( $values['selectedid']==1 || @in_array(1, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='1' ".$selected." >Details</option>\n";
        if( $values['selectedid']==2 || @in_array(2, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='2' ".$selected." >Installation Shots</option>\n";
        print "</select>\n";
    }

    public static function select_book_cover($db,$values=array())
    {
        print "<select name='".$values['name']."' id='".$values['id']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_book_cover']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        if( $values['selectedid']==1 || @in_array(1, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='1' ".$selected." >".LANG_LITERATURE_COVER_HARDBACK."</option>\n";
        if( $values['selectedid']==2 || @in_array(2, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='2' ".$selected." >".LANG_LITERATURE_COVER_SOFTCOVER."</option>\n";
        if( $values['selectedid']==3 || @in_array(3, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='3' ".$selected." >".LANG_LITERATURE_COVER_UNKNOWNBINDING."</option>\n";
        if( $values['selectedid']==4 || @in_array(4, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='4' ".$selected." >".LANG_LITERATURE_COVER_SPECIALBINDING."</option>\n";
        print "</select>\n";
    }

    public static function select_locations($db,$values=array())
    {
        $query_where_locations=$values['where'];
        $query_order=" ORDER BY location_en ";
        $query_locations=QUERIES::query_locations($db,$query_where_locations,$query_order,$query_limit);
        $results=$db->query($query_locations['query_without_limit']);

        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_location']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['locationid']==$values['selectedid'] || @in_array($row['locationid'], $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            $row=UTILS::html_decode($row);
            print "<option value='".$row['locationid']."' ".$selected." >".$row['location_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_locations_city($db,$values=array())
    {
        $query_where_city=$values['where'];
        $query_order=" ORDER BY city_en ";
        $query_locations=QUERIES::query_locations_city($db,$query_where_city,$query_order,$query_limit);
        $results=$db->query($query_locations['query_without_limit']);

        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_location_city']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['cityid']==$values['selectedid'] || @in_array($row['cityid'], $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            $row=UTILS::html_decode($row);
            print "<option value='".$row['cityid']."' ".$selected." >".$row['city_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_locations_country($db,$values=array())
    {
        $query_order=" ORDER BY country_en ";
        $query_locations=QUERIES::query_locations_country($db,$query_where_locations,$query_order,$query_limit);
        $results=$db->query($query_locations['query_without_limit']);

        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_location_country']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['countryid']==$values['selectedid'] || @in_array($row['countryid'], $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            $row=UTILS::html_decode($row);
            print "<option value='".$row['countryid']."' ".$selected." >".$row['country_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_search_test_categories($db,$values=array())
    {
        $values_search_category=array();
        $values_search_category['order']=" ORDER BY sort ";
        $query_search_cat=QUERIES::query_search_test_categories($db,$values_search_category);
        $results=$db->query($query_search_cat['query_without_limit']);

        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_search_categories']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['categoryid']==$values['selectedid'] || @in_array($row['categoryid'], $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['categoryid']."' ".$selected." >".$row['title']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_exhibition_typeid($db,$values=array())
    {
        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        if( $values['selectedid']==1 || @in_array(1, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='1' ".$selected." >".LANG_SOLO."</option>\n";
        if( $values['selectedid']==2 || @in_array(2, $values['selectedid']) ) $selected="selected='selected'";
        else $selected="";
        print "<option value='2' ".$selected." >".LANG_GROUP."</option>\n";
        print "</select>\n";
    }

    public static function select_painting_size_types($db,$values=array())
    {
        $values_search_category=array();
        $values_search_category['order']=" ORDER BY sort ";
        $query_search_cat=QUERIES::query_painting_size_types($db,$values_search_category);
        $results=$db->query($query_search_cat['query_without_limit']);

        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='".$values['class']['select_search_categories']."' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            if( $row['size_typeid']==$values['selectedid'] || @in_array($row['size_typeid'], $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$row['size_typeid']."' ".$selected." >".$row['title_en']."</option>\n";
        }
        print "</select>\n";
    }

    public static function select_size_parts($db,$values=array())
    {
        print "<select name='".$values['name']."' id='".$values['name']."' ".$values['onchange']." ".$values['multiple']." class='' >\n";
        if( empty($values['multiple']) ) print "<option value='0'>Select..</option>\n";
        for($i=2;$i<=120;$i++)
        {
            $parts=LANG_PAINTING_SIZE_PARTS;
            if( $i==$values['selectedid'] || @in_array($i, $values['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            if( $i<=10 || $i==12 || $i==16 || $i==18 || $i==48 || $i==100 || $i==120 )
            {
                if( $_SESSION['lang']=="ch" )
                {
                    if( $i==2 ) $i="两件";
                    elseif( $i==3 ) $i="三件";
                    elseif( $i==4 ) $i="四件";
                    elseif( $i==5 ) $i="五件";
                    elseif( $i==6 ) $i="六件";
                    elseif( $i==7 ) $i="七件";
                    elseif( $i==8 ) $i="八件";
                    elseif( $i==9 ) $i="九 件";
                    elseif( $i==10 ) $i="十件";
                }
                print "<option value='".$i."' ".$selected." >".$i.$parts."</option>\n";
            }
        }
        print "</select>\n";
    }

    public static function select_languages_new($db,$options=array())
    {
        $options_lang_files=array();
        $options_lang_files['not_load_lang_file']=1;
        $languages=UTILS::load_lang_file($db,$options_lang_files);

        print "<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".$options['class']['select_languages']."' >\n";
        //if( empty($options['multiple']) ) print "<option value='0'>Select..</option>\n";
        foreach($languages['languages'] as $lang)
        {
            if( $lang['lang']==$options['selectedid'] || @in_array($languages['languages'], $options['selectedid']) ) $selected="selected='selected'";
            else $selected="";
            print "<option value='".$lang['lang']."' ".$selected." >".$lang['title']."</option>\n";
        }
        print "</select>\n";
    }

    # END SELECT DROP DOWN LISTS







}
