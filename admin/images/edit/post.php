<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST);


### image upload
if( $_FILES['Filedata']['error']==0 )
{
    
    $fileName = strtolower(str_replace (" ", "_", $_FILES['Filedata']['name']));
    $tmpName  = $_FILES['Filedata']['tmp_name'];
    $fileSize = $_FILES['Filedata']['size'];
    $fileType = $_FILES['Filedata']['type'];

    if ( is_uploaded_file($tmpName) )
    {   

        if( $_POST['submit']=="update" ) 
        {   
            $imageid=$_POST['imageid'];
        }   
        else 
        {   
            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            admin_utils::admin_log($db,1,25,$imageid);
        }
        /*
        else
        {
            admin_utils::admin_log($db,"",25,$_POST['imageid'],2);
        }
        */

        $getimagesize = getimagesize($tmpName);

        switch( $getimagesize['mime'] )
        {   
            case 'image/gif' : $imgSrc = imagecreatefromgif($tmpName); $ext = ".gif"; break;
            case 'image/png' : $imgSrc = imagecreatefrompng($tmpName); $ext = ".png"; break;
            case 'image/jpeg' : $imgSrc = imagecreatefromjpeg($tmpName); $ext = ".jpg"; break;
            case 'image/bmp' : $imgSrc = UTILS::imagecreatefrombmp($tmpName); $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }

        $dir=DATA_PATH."/images_new";
        $new_file_name=$imageid.".jpg";
        $new_file_path=$dir."/original/".$imageid.$ext;

        UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,$dir.'/llarge/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name); 

        #edition
        UTILS::resize_image($tmpName,$fileName,SIZE_SNOWWHITE_HEIGHT,SIZE_SNOWWHITE_WIDTH,$dir.'/xlarge_snowwhite/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,SIZE_WARCUT_HEIGHT,SIZE_WARCUT_WIDTH,$dir.'/xlarge_warcut/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,SIZE_FLORENCE_HEIGHT,SIZE_FLORENCE_WIDTH,$dir.'/xlarge_florence/'.$new_file_name);

        # copy original
        if (!copy($tmpName, $new_file_path))
        {
            print "Error Uploading File.";
        }
        else
        {
            print "ok";
            $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");
            
            # create cache image
            $src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
            $src_cache_uri=$imageid.".jpg";
            UTILS::cache_uri($src,$src_cache_uri,1);
            #end

        }
        #end copy original

     }

}




if( isset($_POST['submit']) )
{
    
    if( !empty($_POST['imageid']) ) 
    {
        $db->query("UPDATE ".TABLE_IMAGES." SET 
                        title_en='".$_POST['title_en']."', 
                        title_de='".$_POST['title_de']."', 
                        title_fr='".$_POST['title_fr']."', 
                        title_it='".$_POST['title_it']."', 
                        title_zh='".$_POST['title_zh']."', 
                        notes_en='".$_POST['notes_en']."', 
                        notes_de='".$_POST['notes_de']."', 
                        notes_fr='".$_POST['notes_fr']."', 
                        notes_it='".$_POST['notes_it']."', 
                        notes_zh='".$_POST['notes_zh']."', 
                        notes_admin='".$_POST['notes_admin']."', 
                        date_modified=NOW() 
                    WHERE imageid='".$_POST['imageid']."'");

        admin_utils::admin_log($db,2,25,$_POST['imageid']);
    }
    
    UTILS::redirect("/admin/images/edit/?imageid=".$_POST['imageid']."&task=".$_POST['submit']);
}
# file upload end



