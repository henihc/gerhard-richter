<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# menu selected
$html->menu_admin_selected="images";
$html->menu_admin_sub_selected="add_image";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";


    $query_where_image=" WHERE imageid='".$_GET['imageid']."' ";
    $query_image=QUERIES::query_images($db,$query_where_image,$query_order,$query_limit);
    $results=$db->query($query_image['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";


                if( !empty($row['date_modified_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date modified</th>\n";
                        print "\t<td>".$row['date_modified_admin']."</td>\n";
                    print "\t</tr>\n";
                }   

                if( !empty($row['date_created_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date created</th>\n";
                        print "\t<td>".$row['date_created_admin']."</td>\n";
                    print "\t</tr>\n";
                } 

                
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Image title</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_en' id='title_en' value='".$row['title_en']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_de' id='title_de' value='".$row['title_de']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_fr' id='title_fr' value='".$row['title_fr']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_it' id='title_it' value='".$row['title_it']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<input type='text' name='title_zh' id='title_zh' value='".$row['title_zh']."' class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Notes</th>\n";
                    print "\t<td>\n";

                        print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_en' id='notes_en' cols='99' rows='6'>".$row['notes_en']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_de' id='notes_de' cols='99' rows='6'>".$row['notes_de']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_fr' id='notes_fr' cols='99' rows='6'>".$row['notes_fr']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_it' id='notes_it' cols='99' rows='6'>".$row['notes_it']."</textarea>\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                print "\t<textarea name='notes_zh' id='notes_zh' cols='99' rows='6'>".$row['notes_zh']."</textarea>\n";
                            print "\t</div>\n";
                        print "\t</div>\n";

                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<td>\n";
                    if( $count ) 
                    {
                        print "\t<a href='/images/imageid_".$row['imageid']."__size_o.jpg' target='blank' title='View image in full screen'>\n";
                            print "\t<img src='/images/imageid_".$row['imageid']."__size_s.jpg' alt='image".$row['imageid']."' />\n";
                        print "\t</a>\n";
                    }
                        else print "Images";
                    print "\t</td>\n";
                    print "\t<td>\n";
                        //print "\t<input type='file' name='Filedata' />\n";
                    print "\t</td>\n";
                print "\t</tr>\n";

                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Admin notes</td>\n";
                    print "\t<td>\n";
                        print "\t<textarea rows='7' cols='99' name='notes_admin' id='notes_admin' >".$row['notes_admin']."</textarea>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";


                if( $count )
                {
                    print "\t<tr>\n";
                        print "\t<td>";
                            if( $count )
                            {
                                //print "<a href='#' onclick=\"delete_confirm2('/admin/images/edit/del.php?imageid=".$row['imageid']."');\" title='delete this image'>delete</a>";
                                $options_delete=array();
                                $options_delete['url_del']="/admin/images/edit/del.php?imageid=".$row['imageid']."";
                                $options_delete['text1']="Images - ".$row['title_en'];
                                //$options_delete['text2']="";
                                //$options_delete['html_return']=1;
                                admin_html::delete_button($db,$options_delete);
                            }
                            else print "* Mandatory";
                        print "</td>\n";
                        if( $count ) $value="update";
                        else $value="add";
                        print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
                    print "\t</tr>\n";
                }



            print "\t</table>\n";
            print "\t<input type='hidden' name='imageid' value='".$row['imageid']."' />\n";
        print "\t</form>\n";

        # printing out relations
        if( $count>0 ) admin_html::show_relations($db,17,$row['imageid']);
        #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
