<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

if( !empty($_GET['imageid']) )
{
    $db=new dbCLASS;
    

    $_GET=$db->db_prepare_input($_GET);

    $results=$db->query("SELECT * FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND itemid1='".$_GET['imageid']."' AND typeid2=1 ) OR ( typeid2=17 AND itemid2='".$_GET['imageid']."' AND typeid1=1 ) ");
    $row=$db->mysql_array($results);

    # painting image
    if( ( $row['typeid1']==17 && $row['typeid2']==1 ) || ( $row['typeid1']==1 && $row['typeid2']==17 )  ) 
    {
        if( $row['typeid1']==17 && $row['typeid2']==1 ) 
        {    
            $paintid=$row['itemid2'];
        }    
        if( $row['typeid2']==17 && $row['typeid1']==1 )
        {    
            $paintid=$row['itemid1'];
        }        
        $db->query("UPDATE ".TABLE_PAINTING." SET imageid=NULL WHERE paintID='".$paintid."' LIMIT 1 ");
    }
    # END painting image

    $db->query("DELETE FROM ".TABLE_IMAGES." WHERE imageid='".$_GET['imageid']."' ");
    $db->query("DELETE FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND itemid1='".$_GET['imageid']."' ) OR ( typeid2=17 AND itemid2='".$_GET['imageid']."' ) ");

    admin_utils::admin_log($db,3,25,$_GET['imageid']);

    

    UTILS::redirect("/admin/images/?task=delete");
}
else 
{
	admin_utils::admin_log($db,3,25,$_GET['imageid'],2);

	UTILS::redirect("/");
}
