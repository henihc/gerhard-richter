<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="images";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY imageid DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_images=QUERIES::query_images($db,$query_where_images,$query_order,$query_limit);

    $results=$db->query($query_images['query_without_limit']);
    $count=$db->numrows($results);
    $results_images=$db->query($query_images['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) )
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<br /><br /><table class='admin-table' cellspacing='0'>\n";

            print "\t<thead>\n";
                print "\t<tr>\n";
                    print "\t<th>imageid</th>\n";
                    print "\t<th>image</th>\n";
                    print "\t<th>title en</th>\n";
                    print "\t<th>title de</th>\n";
                    print "\t<th>date modified</th>\n";
                    print "\t<th>date created</th>\n";
                print "\t</tr>\n";            
            print "\t</thead>\n";

            while( $row=$db->mysql_array($results_images) )
            {

                print "\t<tr>\n";
                    print "\t<td style='text-align:center;'><a href='/admin/images/edit/?imageid=".$row['imageid']."' title='Edit this image' class='a_edit_item' >".$row['imageid']."</a></td>\n";
                    print "\t<td style='text-align:center;'><img src='/images/imageid_".$row['imageid']."__size_s.jpg' alt='image".$row['imageid']."' alt='' /></td>\n";
                    print "\t<td>".$row['title_en']."</td>\n";
                    print "\t<td>".$row['title_de']."</td>\n";
                    print "\t<td style='text-align:center;'>".$row['date_modified_admin']."</td>\n";
                    print "\t<td style='text-align:center;'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";

            }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

    }
    else print "\t<p class='error' >No data found!</p>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
