<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# jquery ui
$html->css[]="/admin/js/jquery_ui/css/jquery-ui-1.10.3.custom.admin.css";
$html->js[]="/admin/js/jquery_ui/jquery-1.9.1.js";
$html->js[]="/admin/js/jquery_ui/jquery-ui-1.10.3.custom.min.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="pages";
$html->menu_admin_sub_selected="pageid_".$_GET['textid'];

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

$values_query_text=array();
$values_query_text['where']=" WHERE textid='".$_GET['textid']."' ";
$query_text=QUERIES::query_text($db,$values_query_text);
$results=$db->query($query_text['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);


print "\t<script type='text/javascript'>\n";
    print "\t$(document).ready(function() {\n";
        print "\tCKEDITOR.replace( 'text_en', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_de', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_fr', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_it', { toolbar : 'Large' } );\n";
        print "\tCKEDITOR.replace( 'text_zh', { toolbar : 'Large' } );\n";
        print "\t$(function() {\n";
            print "\t$( '#tabs-text, #tabs-title' ).tabs({\n";
                print "\tcache: false\n";
            print "\t});\n";
        print "\t});\n";
    print "\t});\n";
print "\t</script>\n";

    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            #error and update info
            if( $_GET['error'] ) print "<tr><td colspan='6' class='error'>*Please complete mandatory fields</td></tr>";
            if( $_GET['task']=="update" ) print "<tr><td colspan='6' class='valid'>Update successfully!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='6' class='valid'>Insert successfully!</td></tr>";
            #error and update info end

            if( !empty($row['date_modified_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</td>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<div id='tabs-title'>\n";
                        if( $_SESSION['new_record']['error']['title_en'] ) $class="error";
                        else $class="";
                        print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class."' href='#tabs-1' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-2' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-3' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-4' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-5' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div class='clearer'></div>\n";
                        print "\t<div id='tabs-1' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                            else $title_en=$row['title_en'];
                            print "\t<input type='text' name='title_en' id='title_en' value=\"".$title_en."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div id='tabs-2' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                            else $title_de=$row['title_de'];
                            print "\t<input type='text' name='title_de' id='title_de' value=\"".$title_de."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div id='tabs-3' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                            else $title_fr=$row['title_fr'];
                            print "\t<input type='text' name='title_fr' id='title_fr' value=\"".$title_fr."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div id='tabs-4' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                            else $title_it=$row['title_it'];
                            print "\t<input type='text' name='title_it' id='title_it' value=\"".$title_it."\" class='admin-input-large'>";
                        print "\t</div>\n";
                        print "\t<div id='tabs-5' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                            else $title_zh=$row['title_zh'];
                            print "\t<input type='text' name='title_zh' id='title_zh' value=\"".$title_zh."\" class='admin-input-large'>";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left' style='vertical-align:top;'>Text</th>\n";
                print "\t<td>\n";
                    print "\t<div id='tabs-text'>\n";
                        print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-1' >English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-2' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-3' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-4' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#tabs-5' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div class='clearer'></div>\n";
                        print "\t<div id='tabs-1' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_en=$_SESSION['new_record']['text_en'];
                            else $text_en=$row['text_en'];
                            print "\t<textarea name='text_en' id='text_en' rows='25' cols='120'>".$text_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div id='tabs-2' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_de=$_SESSION['new_record']['text_de'];
                            else $text_de=$row['text_de'];
                            print "\t<textarea name='text_de' id='text_de' rows='25' cols='120'>".$text_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div id='tabs-3' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_fr=$_SESSION['new_record']['text_fr'];
                            else $text_fr=$row['text_fr'];
                            print "\t<textarea name='text_fr' id='text_fr' rows='25' cols='120'>".$text_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div id='tabs-4' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_it=$_SESSION['new_record']['text_it'];
                            else $text_it=$row['text_it'];
                            print "\t<textarea name='text_it' id='text_it' rows='25' cols='120'>".$text_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div id='tabs-5' class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $text_zh=$_SESSION['new_record']['text_zh'];
                            else $text_zh=$row['text_zh'];
                            print "\t<textarea name='text_zh' id='text_zh' rows='25' cols='120'>".$text_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";
                print"\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</td>\n";
                print "\t<td colspan='5'>\n";
                    print "\t<textarea rows='7' cols='99' name='notes_admin' >".$row['notes_admin']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td></td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input type='submit' name='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='textid' value='".$row['textid']."' />\n";
        print "\t<input type='hidden' name='p' value='".$_GET['p']."' />\n";
    print "\t</form>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

unset($_SESSION['new_record']);
?>
