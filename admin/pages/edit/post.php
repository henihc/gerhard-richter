<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;



$text_en=$db->filter_parameters($_POST['text_en'],1);
$text_de=$db->filter_parameters($_POST['text_de'],1);
$text_fr=$db->filter_parameters($_POST['text_fr'],1);
$text_it=$db->filter_parameters($_POST['text_it'],1);
$text_zh=$db->filter_parameters($_POST['text_zh'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->filter_parameters($_POST);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['text_de'];
$text_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_fr'];
$text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['text_it'];
$text_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    //$db->query("INSERT INTO ".TABLE_TEXT."(date_created) VALUES(NOW())");  
    //$textid=$db->return_insert_id();

    //admin_utils::admin_log($db,1,24,$textid);
}
elseif( $_POST['submit']=="update" )
{
    $textid=$_POST['textid'];

    admin_utils::admin_log($db,2,24,$textid);
} 
else 
{
    admin_utils::admin_log($db,"",24,$_POST['textid'],2);

    UTILS::redirect("/");
}

$query="UPDATE ".TABLE_TEXT." SET 
            title_en=\"".$_POST['title_en']."\",
            title_de=\"".$_POST['title_de']."\",
            title_fr=\"".$_POST['title_fr']."\",
            title_it=\"".$_POST['title_it']."\",
            title_zh=\"".$_POST['title_zh']."\",
            text_en='".$text_en."',
            text_de='".$text_de."',
            text_fr='".$text_fr."',
            text_it='".$text_it."',
            text_zh='".$text_zh."',
            text_search_1='".$text_search_de['text_lower_converted']."',
            text_search_2='".$text_search_de['text_lower_german_converted']."',
            text_search_3='".$text_search_fr['text_lower_converted']."',
            text_search_4='".$text_search_it['text_lower_converted']."'";
if( $_POST['submit']=="update" ) $query.=" ,date_modified=NOW() ";
$query.=" WHERE textid='".$textid."' ";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);
}

unset($_SESSION['new_record']);

$url="/admin/pages/edit/?textid=".$textid."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
