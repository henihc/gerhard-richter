<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$keywords=$_POST['keywords'];
$_POST=$db->filter_parameters($_POST);

if( $_POST['submit']=="add" )
{
    $values_current_search_results=array();
    if( $_POST['categoryid']==1 ) $values_current_search_results['painting_search']=1;
    $values_current_search_results['categoryid']=$_POST['categoryid'];
    $values_current_search_results['artworkid']=$_POST['artworkID'];
    $values_current_search_results['keywords']=$keywords;
    $count_current=admin_html::count_current_search_results($db,$values_current_search_results);

    $db->query("INSERT INTO ".TABLE_SEARCH_TEST_KEYWORDS."(count_valid,ids_valid,count_test,ids_test,count_current,ids_current,date_created) VALUES('".$count_current['count']."','".$count_current['ids']."','".$count_current['count']."','".$count_current['ids']."','".$count_current['count']."','".$count_current['ids']."',NOW())");  
    $keywordid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $keywordid=$_POST['keywordid'];
} 
else UTILS::redirect("/");

$query="UPDATE ".TABLE_SEARCH_TEST_KEYWORDS." SET 
            artworkid='".$_POST['artworkID']."',
            categoryid='".$_POST['categoryid']."',
            keywords='".$keywords."' ";
if( $_POST['submit']=="update" ) $query.=" ,date_modified=NOW() ";
$query.=" WHERE keywordid='".$keywordid."' ";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);
}

unset($_SESSION['new_record']);

$url="/admin/search-test/edit/?keywordid=".$keywordid."&categoryid=".$_POST['categoryid']."&task=".$_POST['submit'];
UTILS::redirect($url);
?>
