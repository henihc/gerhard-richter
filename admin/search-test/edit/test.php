<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_GET=$db->filter_parameters($_GET);

if( !empty($_GET['categoryid']) )
{
    $values_search_keywords['where']=" WHERE sk.categoryid='".$_GET['categoryid']."' ";
    $query_search_keywords=QUERIES::query_search_test_keywords($db,$values_search_keywords);
    $results=$db->query($query_search_keywords['query_without_limit']);
    $count=$db->numrows($results);

    if( $count>0 )
    {
        while( $row=$db->mysql_array($results) )
        {
            $values_current_search_results=array();
            if( $row['categoryid']==1 ) $values_current_search_results['painting_search']=1;
            $values_current_search_results['categoryid']=$row['categoryid'];
            $values_current_search_results['artworkid']=$row['artworkid'];
            $values_current_search_results['keywords']=$row['keywords'];
            $count_current=admin_html::count_current_search_results($db,$values_current_search_results);

            # valid test
            $ids_test=unserialize($row['ids_test']);
            $ids_current=unserialize($count_current['ids']);
            //print_r($count_test);
            if( is_array($ids_current) && is_array($ids_test) ) 
            {   
                $difference=array_diff_assoc($ids_current,$ids_test);
                //print_r($difference);
                if( empty($difference) ) $valid_test=1;
                else $valid_test=0;
            }   
            else $valid_test=0;
            # END valid test

            $query="UPDATE ".TABLE_SEARCH_TEST_KEYWORDS." SET 
                        valid_test='".$valid_test."',
                        count_current='".$count_current['count']."',
                        ids_current='".$count_current['ids']."',
                        date_modified=NOW() 
                    WHERE keywordid='".$row['keywordid']."' ";

            $db->query($query);
        }
        //exit('here');
    }
}


$url="/admin/search-test/?categoryid=".$_GET['categoryid']."&task=test-successfull";
UTILS::redirect($url);
?>
