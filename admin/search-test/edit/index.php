<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/common.js";
$html->head('admin');

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);

if( !empty( $_GET['categoryid'] ) ) $url="?categoryid=".$_GET['categoryid'];

print "\t<div id='div_admin' >\n";


$values_query_keywords=array();
$values_query_keywords['where']=" WHERE sk.keywordid='".$_GET['keywordid']."' ";
$query=QUERIES::query_search_test_keywords($db,$values_query_keywords);
$results=$db->query($query['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);

print "\t<div id='div_admin' >\n";

    print "\t<a href='/admin' title=''><u>Main Menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search-test/".$url."' title='Search terms'><u>search terms</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search-test/edit/".$url."' title='Add search term'><u>add search term</u></a>&nbsp;&nbsp;\n";
    print "\t<br /><br />";

    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            #error and update info
            if( $_GET['error'] ) print "<tr><td colspan='6' class='error'>*Please complete mandatory fields</td></tr>";
            if( $_GET['task']=="update" ) print "<tr><td colspan='6' class='valid'>Update successfully!</td></tr>";
            if( $_GET['task']=="add" ) print "<tr><td colspan='6' class='valid'>Insert successfully!</td></tr>";
            #error and update info end


            if( !empty($row['date_modified_admin_sk']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td>".$row['date_modified_admin_sk']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created_admin_sk']) )
            {   
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td>".$row['date_created_admin_sk']."</td>\n";
                print "\t</tr>\n";
            } 

            if( $count )
            {
                print "\t<tr>\n";
                    if( $row['valid'] ) $class="td-search-test-valid";
                    else $class="td-search-test-not-valid";
                    print "\t<th class='th_align_left td-search-test-valid ".$class."'>Count valid</th>\n";
                    print "\t<td>\n";
                        $count_valid=$row['count_valid'];
                        if( !$count_valid ) print "0";
                        else
                        {
                            print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=1' title='View results'><u>";
                                print $row['count_valid'];
                            print "</u></a>";
                        }
                    print"\t</td>\n";
                print "\t</tr>\n";

                $count_test=$row['count_test'];
                if( $row['valid_test'] ) $class="td-search-test-valid";
                else $class="td-search-test-not-valid";
                print "\t<tr>\n";
                    print "\t<th class='th_align_left ".$class."'>Count test</th>\n";
                    print "\t<td>\n";
                        if( !$count_test ) print "0";
                        else
                        {
                            print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=2' title='View results'><u>";
                                print $row['count_test'];
                            print "</u></a>";
                        }
                    print"\t</td>\n";
                print "\t</tr>\n";

                $count_current=$row['count_current'];
                print "\t<tr>\n";
                    print "\t<th class='th_align_left '>Count current</th>\n";
                    print "\t<td>\n";
                        if( !$count_current ) print "0";
                        else
                        {
                            print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=3' title='View results'><u>";
                                print $row['count_current'];
                            print "</u></a>";
                        }
                    print"\t</td>\n";
                print "\t</tr>\n";

            }

            print "\t<tr>\n";
                if( !empty($_GET['categoryid']) ) $categoryid=$_GET['categoryid'];
                elseif( $_GET['error'] ) $categoryid=$_SESSION['new_record']['categoryid'];
                else $categoryid=$row['categoryid'];
                print "\t<th class='th_align_left'>Category</th>\n";
                print "\t<td>\n";
                    $class['select_sub_categories']="select_sub_categories";
                    $values_select_category=array();
                    $values_select_category['name']="categoryid";
                    $values_select_category['selectedid']=$categoryid;
                    $values_select_category['onchange']="onchange=\"
                        if( this.options[selectedIndex].value==1 ) 
                        {
                            document.getElementById('tr-artworkid').style.display='';
                        }
                        else document.getElementById('tr-artworkid').style.display='none';\"";
                    admin_html::select_search_test_categories($db,$values_select_category);
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $categoryid==1 ) $style="";
            else $style="display:none;";
            print "\t<tr style='".$style."' id='tr-artworkid'>\n";
                if( $_GET['error'] ) $artworkid=$_SESSION['new_record']['artworkid'];
                else $artworkid=$row['artworkid'];
                print "\t<th class='th_align_left'>Artwork</th>\n";
                print "\t<td>\n";
                    print admin_html::get_artworks($db,"",$artworkid,"","onchange=\"".$onchnage."\"");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                //print "\t<th class='th_align_left' style='vertical-align:top;'>Search-terms( comma ',' - seprated no spaces)</th>\n";
                print "\t<th class='th_align_left' style='vertical-align:top;'>Search-terms</th>\n";
                print "\t<td>\n";
                    if( $_GET['error'] ) $keywords=$_SESSION['new_record']['keywords'];
                    else $keywords=$row['keywords'];
                    print "\t<textarea name='keywords' id='keywords' rows='25' cols='120'>".$keywords."</textarea>\n";
                print"\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td></td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input type='submit' name='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='keywordid' value='".$row['keywordid']."' />\n";
    print "\t</form>\n";


print "\t</div>\n";


unset($_SESSION['new_record']);
$html->foot();

?>
