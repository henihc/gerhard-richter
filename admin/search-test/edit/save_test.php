<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_GET=$db->filter_parameters($_GET);

if( !empty($_GET['categoryid']) )
{
    $values_search_keywords=array();
    $values_search_keywords['where']=" WHERE sk.categoryid='".$_GET['categoryid']."' ";
    $query_search_keywords=QUERIES::query_search_test_keywords($db,$values_search_keywords);
    $results=$db->query($query_search_keywords['query_without_limit']);
    $count=$db->numrows($results);

    if( $count>0 )
    {   
        while( $row=$db->mysql_array($results) )
        {
            $values_current_search_results=array();
            if( $row['categoryid']==1 ) $values_current_search_results['painting_search']=1;
            $values_current_search_results['categoryid']=$row['categoryid'];
            $values_current_search_results['artworkid']=$row['artworkid'];
            $values_current_search_results['keywords']=$row['keywords'];
            $count_test=admin_html::count_current_search_results($db,$values_current_search_results);

            # valid test
            $ids_current=unserialize($row['ids_current']);
            $ids_test=unserialize($count_test['ids']);
            //print_r($count_test);
            if( is_array($ids_current) && is_array($ids_test) ) 
            {   
                $difference=array_diff_assoc($ids_current,$ids_test);
                //print_r($difference);
                if( empty($difference) ) $valid_test=1;
                else $valid_test=0;
            }   
            else $valid_test=0;
            # END valid test

            # valid
            $ids_valid=unserialize($row['ids_valid']);
            //print_r($count_valid);
            if( is_array($ids_test) && is_array($ids_valid) ) 
            {   
                $difference=array_diff($ids_test,$ids_valid);
                //print_r($difference);
                if( empty($difference) ) $valid=1;
                else $valid=0;
            }   
            else $valid=0;
            # END valid

            $query="UPDATE ".TABLE_SEARCH_TEST_KEYWORDS." SET 
                        valid='".$valid."',
                        valid_test='".$valid_test."',
                        count_test='".$count_test['count']."',
                        ids_test='".$count_test['ids']."',
                        date_modified=NOW() ";
            $query.=" WHERE keywordid='".$row['keywordid']."' ";
            //print $query;
            $db->query($query);

        }
    }
}


$url="/admin/search-test/?categoryid=".$_GET['categoryid']."&task=save-successfull";
UTILS::redirect($url);
?>
