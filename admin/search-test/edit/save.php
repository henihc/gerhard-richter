<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$_GET=$db->filter_parameters($_GET);

if( !empty($_GET['categoryid']) )
{
    $values_search_keywords=array();
    $values_search_keywords['where']=" WHERE sk.categoryid='".$_GET['categoryid']."' ";
    $query_search_keywords=QUERIES::query_search_test_keywords($db,$values_search_keywords);
    $results=$db->query($query_search_keywords['query_without_limit']);
    $count=$db->numrows($results);

    if( $count>0 )
    {   
        while( $row=$db->mysql_array($results) )
        {
            $values_current_search_results=array();
            if( $row['categoryid']==1 ) $values_current_search_results['painting_search']=1;
            $values_current_search_results['categoryid']=$row['categoryid'];
            $values_current_search_results['artworkid']=$row['artworkid'];
            $values_current_search_results['keywords']=$row['keywords'];
            $count_valid=admin_html::count_current_search_results($db,$values_current_search_results);


            $ids_test=unserialize($row['ids_test']);
            $ids_valid=unserialize($count_valid['ids']);
            //print_r($count_valid);
            if( is_array($ids_test) && is_array($ids_valid) ) 
            {
                $difference=array_diff_assoc($ids_test,$ids_valid);
                //print_r($difference);
                if( empty($difference) ) $valid=1;
                else $valid=0;
            }
            else $valid=0;

            $query="UPDATE ".TABLE_SEARCH_TEST_KEYWORDS." SET 
                        valid='".$valid."',
                        count_valid='".$count_valid['count']."',
                        ids_valid='".$count_valid['ids']."',
                        date_modified=NOW() ";
            $query.=" WHERE keywordid='".$row['keywordid']."' ";
            //print $query;
            $db->query($query);

        }
    }
}


$url="/admin/search-test/?categoryid=".$_GET['categoryid']."&task=save-successfull";
UTILS::redirect($url);
?>
