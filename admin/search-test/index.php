<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/common.js";
$html->head('admin');

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);
$_GET=UTILS::ifemptyGET($_GET,20);

if( !empty( $_GET['categoryid'] ) ) $url="?categoryid=".$_GET['categoryid'];

print "\t<div id='div_admin' >\n";
    
    print "\t<a href='/admin' title=''><u>Main Menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search-test/".$url."' title='Search terms'><u>search terms</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search-test/edit/".$url."' title='Add search term'><u>add search term</u></a>&nbsp;&nbsp;\n";
    print "\t<br /><br />";

    $values_select_category=array();;
    $values_select_category['name']="categoryid";
    $values_select_category['selectedid']=$_GET['categoryid'];
    $values_select_category['onchange']="onchange=\"location.href='?".$values_select_category['name']."='+document.getElementById('".$values_select_category['name']."').options[selectedIndex].value\"";
    admin_html::select_search_test_categories($db,$values_select_category);


    if( !empty($_GET['categoryid']) )
    {
        $values_search_keywords=array();
        $values_search_keywords['where']=" WHERE sk.categoryid='".$_GET['categoryid']."' ";
        $query_search_keywords=QUERIES::query_search_test_keywords($db,$values_search_keywords);
        $results=$db->query($query_search_keywords['query_without_limit']);
        $count=$db->numrows($results);

        //print $query_search_relations['query'];

        if( $count>0 )
        {

            print "\t<table class='admin-table' cellspacing='0' style='width:900px;'>\n";



                print "\t<thead>\n";
                    #error and update info
                    if( $_GET['task']=="test-successfull" ) print "<tr><td colspan='7' class='valid'>Test successfull!</td></tr>";
                    if( $_GET['task']=="save-successfull" ) print "<tr><td colspan='7' class='valid'>Save successfull!</td></tr>";
                    #error and update info end
                    print "\t<tr>\n";
                        print "\t<th>id</th>\n";
                        print "\t<th>search-terms</th>\n";
                        print "\t<th>count valid</th>\n";
                        print "\t<th>count test</th>\n";
                        print "\t<th>count current</th>\n";
                        print "\t<th>date modified</th>\n";
                    print "\t</tr>\n";            
                print "\t</thead>\n";

                while( $row=$db->mysql_array($results) )
                {
                    print "\t<tr>\n";
                        print "\t<td style='text-align:center;'><a href='/admin/search-test/edit/?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."' ><u>".$row['keywordid']."</u></a></td>\n";
                        print "\t<td>";
                            if( !empty($row['keywords']) )
                            {
                                $i=0;
                                $keywords=explode(",", $row['keywords']);
                                foreach( $keywords as $keyword )
                                {
                                    $i++;
                                    if( $row['categoryid']==1 ) $url="/art/search/?title=".$keyword."";
                                    elseif( $row['categoryid']==13 ) $url="/links/articles/search/?keyword=".$keyword."";
                                    else $url="#";
                                    print "<a href='".$url."' title='View on live page'><u>".$keyword."</u></a>";
                                    if( $i!=count($keywords) ) print ", ";
                                }
                            }
                            elseif( empty($row['keywords']) && !empty($row['artworkid']) )
                            {
                                $artwork=UTILS::get_artwork_name($db,$row['artworkid']);
                                print "<a href='/art/search/?artworkID=".$row['artworkid']."' title='View on live page'><u>".$artwork."</u></a>";
                            }
                        print "</td>\n";
                        if( $row['valid'] ) $class="td-search-test-valid";
                        else $class="td-search-test-not-valid";
                        print "\t<td style='text-align:center;' class='".$class."'>";
                            if( empty($row['count_valid']) ) print "0";
                            else
                            {
                                print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=1' title='View results'><u>";
                                    print $row['count_valid'];
                                print "</u></a>";
                            }
                        print "</td>\n";
                        if( $row['valid_test'] ) $class="td-search-test-valid";
                        else $class="td-search-test-not-valid";
                        print "\t<td style='text-align:center;' class='".$class."'>";
                            if( empty($row['count_test']) ) print "0";
                            else
                            {
                                print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=2' title='View results'><u>";
                                    print $row['count_test'];
                                print "</u></a>";
                            }
                        print "</td>\n";
                        print "\t<td style='text-align:center;'>";
                            if( empty($row['count_current']) ) print "0";
                            else
                            {
                                print "<a href='/admin/search-test/edit/view_results.php?keywordid=".$row['keywordid']."&categoryid=".$row['categoryid']."&typeid=3' title='View results'><u>";
                                    print $row['count_current'];
                                print "</u></a>";
                            }
                        print "</td>\n";
                        print "\t<td style='text-align:center;'>";
                            print $row['date_modified_admin_sk'];
                        print "</td>\n";
                    print "\t</tr>\n";

                }

            print "\t</table>\n";
            print "\t<input type='button' onclick=\"window.location='edit/test.php?categoryid=".$_GET['categoryid']."';\" value='Run test' />\n";
            print "\t<input type='button' onclick=\"if(confirm('Are you sure you wish to save count test?')){window.location='edit/save_test.php?categoryid=".$_GET['categoryid']."';}\" value='Save test' />\n";
            print "\t<input type='button' onclick=\"if(confirm('Are you sure you wish to save count valid?')){window.location='edit/save.php?categoryid=".$_GET['categoryid']."';}\" value='Save results' style='float:right;' />\n";

        }
        else print "\t<p class='error' >No data found!</p>\n";
    }
    else print "\t<p class='valid' >Please choose category!</p>\n";



print "\t</div>\n";



$html->foot();

?>
