<?php
header("Content-Type: text/html; charset=utf-8");
//header("Content-Type: text/plain");
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//$html = new html_elements;
//$html->head('artwork');

$db=new dbCLASS;


# PAINTINGS

function prepare_paint_title2($db,$medium)
{
    $medium=trim(strip_tags($medium));


    $medium=html_entity_decode($medium, ENT_QUOTES, 'UTF-8');

    $medium=$db->db_prepare_input($medium);

    return $medium;
}

$query_media="SELECT * FROM ".TABLE_PAINTING."  ";

$query_media.=" WHERE catID=79 ";

$results=$db->query($query_media);

$count=$db->numrows($results);

print $count."<br /><br />";

while( $row=$db->mysql_array($results,0) )
{

        $title_en=prepare_paint_title2($db,$row['titleEN']);
        $title_de=prepare_paint_title2($db,$row['titleDE']);
        $title_fr=prepare_paint_title2($db,$row['titleFR']);
        $title_it=prepare_paint_title2($db,$row['titleIT']);
        $title_zh=prepare_paint_title2($db,$row['titleZH']);

$title_tmp1=str_replace("(","",$title_en);
$title_tmp2=str_replace(")","",$title_tmp1);
$title_tmp3=explode(" ", $title_tmp2);
$title_new=$title_tmp3[1]." (".$title_tmp3[0].")";
$title_new_zh=$title_tmp3[1]." (".$title_tmp3[0]." 森林)";

$query_update="UPDATE ".TABLE_PAINTING." SET
            titleEN='".$title_new."',
            titleDE='".$title_new."',
            titleFR='".$title_new."',
            titleIT='".$title_new."',
            titleZH='".$title_new_zh."'
        WHERE paintID='".$row['paintID']."' LIMIT 1";

        print "paintid = ".$row['paintID']."<br />";
        print "NEW!!!!! = |".$title_new."|<br />";
        print "NEW!!!!! = |".$title_new_zh."|<br />";
        print "titleEN = |".$title_en."|<br />";
        print "titleDE = ".$title_de."<br />";
        print "titleFR = ".$title_fr."<br />";
        print "titleIT = ".$title_it."<br />";
        print "titleZH = ".$title_zh."<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);


}


exit;

# exhibitions

function prepare_item_value2($db,$string)
{
    $string=trim(strip_tags($string));

    $string=html_entity_decode($string, ENT_QUOTES, 'UTF-8');

    $string=$db->db_prepare_input($string);

    return $string;
}

$query_select="SELECT * FROM ".TABLE_EXHIBITIONS." ";

//$query_select.=" WHERE exID=3461 ";

//$query_select.=" LIMIT 10 ";

$results=$db->query($query_select);

while( $row=$db->mysql_array($results,0) )
{

        $title_original=prepare_item_value2($db,$row['title_original']);
        $title_en=prepare_item_value2($db,$row['titleEN']);
        $title_de=prepare_item_value2($db,$row['titleDE']);
        $title_fr=prepare_item_value2($db,$row['titleFR']);
        $title_it=prepare_item_value2($db,$row['titleIT']);
        $title_zh=prepare_item_value2($db,$row['titleZH']);
        //$notes=prepare_item_value2($db,$row['notes']);




$query_update="UPDATE ".TABLE_EXHIBITIONS." SET
            title_original='".$title_original."',
            titleEN='".$title_en."',
            titleDE='".$title_de."',
            titleFR='".$title_fr."',
            titleIT='".$title_it."',
            titleZH='".$title_zh."'
        WHERE exID='".$row['exID']."' LIMIT 1";

        print "exID = <a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."'>".$row['exID']."</a><br /><br />";
        print "title_original = |".$title_original."|<br />";
        print "title_en = |".$title_en."|<br />";
        print "title_de = |".$title_de."|<br />";
        print "title_fr = |".$title_fr."|<br />";
        print "title_it = |".$title_it."|<br />";
        print "title_zh = |".$title_zh."|<br />";
        //print "notes = |".$notes."|<br />";
        print "<br />\n".$query_update."\n";
        print "<br /><br /><hr />\n";

        //$db->query($query_update);

}

exit;

# literature

function prepare_item_value($db,$string)
{
    $string=trim(strip_tags($string));

    $string=html_entity_decode($string, ENT_QUOTES, 'UTF-8');

    $string=$db->db_prepare_input($string);

    return $string;
}

$query_select="SELECT * FROM ".TABLE_BOOKS." ";

//$query_select.=" WHERE id=1506 ";

//$query_select.=" LIMIT 100 ";

$results=$db->query($query_select);

while( $row=$db->mysql_array($results,0) )
{

        $title=prepare_item_value($db,$row['title']);
        $title_periodical=prepare_item_value($db,$row['title_periodical']);
        $title_display=prepare_item_value($db,$row['title_display']);
        $author=prepare_item_value($db,$row['author']);
        $author_essay=prepare_item_value($db,$row['author_essay']);
        $author_essay_title=prepare_item_value($db,$row['author_essay_title']);
        $publisher=prepare_item_value($db,$row['publisher']);
        $published_ephemera=prepare_item_value($db,$row['published_ephemera']);
        $occasion_ephemera=prepare_item_value($db,$row['occasion_ephemera']);


$query_update="UPDATE ".TABLE_BOOKS." SET
            title='".$title."',
            title_periodical='".$title_periodical."',
            title_display='".$title_display."',
            author='".$author."',
            author_essay='".$author_essay."',
            author_essay_title='".$author_essay_title."',
            publisher='".$publisher."',
            published_ephemera='".$published_ephemera."',
            occasion_ephemera='".$occasion_ephemera."'
        WHERE id='".$row['id']."' LIMIT 1";

        print "id = <a href='/admin/books/edit/?bookid=".$row['id']."'>".$row['id']."</a><br /><br />";
        print "title = |".$title."|<br />";
        print "title_periodical = |".$title_periodical."|<br />";
        print "title_display = |".$title_display."|<br />";
        print "author = |".$author."|<br />";
        print "author_essay = |".$author_essay."|<br />";
        print "author_essay_title = |".$author_essay_title."|<br />";
        print "publisher = |".$publisher."|<br />";
        print "published_ephemera = |".$published_ephemera."|<br />";
        print "occasion_ephemera = |".$occasion_ephemera."|<br />";
        print "<br />\n".$query_update."\n";
        print "<br /><br /><hr />\n";

        //$db->query($query_update);

}

exit;

# relations literure

    if( !empty($_GET['relationid']) ) $relationid=$_GET['relationid'];
    else $relationid="137315";

    $query_rel_lit_where=" WHERE relationid='".$relationid."' ";
    $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where,""," LIMIT 1 ");
    $results_rel_lit=$db->query($query_rel_lit['query']);
    $count=$db->numrows($results_rel_lit);

    while($row=$db->mysql_array($results_rel_lit))
    {
        $options_relation_lit=array();
        $options_relation_lit['relationid']=$row['relationid'];
        $options_relation_lit['debug']=1;
        $pages=admin_utils::order_literature_pages($db,$row['relationid_lit'],$options_relation_lit);
        print $pages['pages_reference']."<br />";
        print $pages['pages_illustration']."<br />";
    }

exit;

# AUCTIONHOUSES

function prepare_auction($db,$medium)
{
    $medium=trim(strip_tags($medium));

    $medium=html_entity_decode($medium, ENT_QUOTES, 'UTF-8');

    $medium=$db->db_prepare_input($medium);

    return $medium;
}

$query_media="SELECT * FROM ".TABLE_AUCTIONHOUSE." ";

//$query_media.=" WHERE ahID=4 ";

$results=$db->query($query_media);

while( $row=$db->mysql_array($results,0) )
{

        $house=prepare_auction($db,$row['house']);

$query_update="UPDATE ".TABLE_AUCTIONHOUSE." SET
            house='".$house."'
        WHERE ahID='".$row['ahID']."' LIMIT 1";

        print "ahID = ".$row['ahID']."<br />";
        print "house = |".$house."|<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);


}

exit;

# painting_opp

function prepare_item_title($db,$medium)
{
	$medium=trim(strip_tags($medium));

	$medium=html_entity_decode($medium, ENT_QUOTES, 'UTF-8');

	$medium=$db->db_prepare_input($medium);

	return $medium;
}

$query_media="SELECT * FROM painting_opp ";

//$query_media.=" WHERE oppinfoid=440 ";

$results=$db->query($query_media);

while( $row=$db->mysql_array($results,0) )
{

		$verso_signed_1_3=prepare_item_title($db,$row['verso_signed_1_3']);
		$verso_signed_2_3=prepare_item_title($db,$row['verso_signed_2_3']);
		$verso_dated_1_3=prepare_item_title($db,$row['verso_dated_1_3']);
		$verso_dated_2_3=prepare_item_title($db,$row['verso_dated_2_3']);
		$verso_numbered_3=prepare_item_title($db,$row['verso_numbered_3']);
		$verso_inscribed_3=prepare_item_title($db,$row['verso_inscribed_3']);

		$recto_signed_1_3=prepare_item_title($db,$row['recto_signed_1_3']);
		$recto_signed_2_3=prepare_item_title($db,$row['recto_signed_2_3']);
		$recto_dated_1_3=prepare_item_title($db,$row['recto_dated_1_3']);
		$recto_dated_2_3=prepare_item_title($db,$row['recto_dated_2_3']);
		$recto_numbered_3=prepare_item_title($db,$row['recto_numbered_3']);
		$recto_inscribed_3=prepare_item_title($db,$row['recto_inscribed_3']);

$query_update="UPDATE painting_opp SET
            verso_signed_1_3='".$verso_signed_1_3."',
            verso_signed_2_3='".$verso_signed_2_3."',
            verso_dated_1_3='".$verso_dated_1_3."',
            verso_dated_2_3='".$verso_dated_2_3."',
            verso_numbered_3='".$verso_numbered_3."',
            verso_inscribed_3='".$verso_inscribed_3."',
            recto_signed_1_3='".$recto_signed_1_3."',
            recto_signed_2_3='".$recto_signed_2_3."',
            recto_dated_1_3='".$recto_dated_1_3."',
            recto_dated_2_3='".$recto_dated_2_3."',
            recto_numbered_3='".$recto_numbered_3."',
            recto_inscribed_3='".$recto_inscribed_3."'
        WHERE oppinfoid='".$row['oppinfoid']."' LIMIT 1";

        print "oppinfoid = ".$row['oppinfoid']."<br />";
        print "verso_signed_1_3 = |".$verso_signed_1_3."|<br />";
        print "verso_signed_2_3 = ".$verso_signed_2_3."<br />";
        print "verso_dated_1_3 = ".$verso_dated_1_3."<br />";
        print "verso_dated_2_3 = ".$verso_dated_2_3."<br />";
        print "verso_numbered_3 = ".$verso_numbered_3."<br />";
        print "verso_inscribed_3 = ".$verso_inscribed_3."<br />";
        print "recto_signed_1_3 = ".$recto_signed_1_3."<br />";
        print "recto_signed_2_3 = ".$recto_signed_2_3."<br />";
        print "recto_dated_1_3 = ".$recto_dated_1_3."<br />";
        print "recto_dated_2_3 = ".$recto_dated_2_3."<br />";
        print "recto_numbered_3 = ".$recto_numbered_3."<br />";
        print "recto_inscribed_3 = ".$recto_inscribed_3."<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);


}


exit;

# PAINTINGS

function prepare_paint_title($db,$medium)
{
	$medium=trim(strip_tags($medium));


	$medium=html_entity_decode($medium, ENT_QUOTES, 'UTF-8');

	$medium=$db->db_prepare_input($medium);

	return $medium;
}

$query_media="SELECT * FROM ".TABLE_PAINTING." ";

//$query_media.=" WHERE paintID=14484 ";

$results=$db->query($query_media);

while( $row=$db->mysql_array($results,0) )
{

		$title_en=prepare_paint_title($db,$row['titleEN']);
		$title_de=prepare_paint_title($db,$row['titleDE']);
		$title_fr=prepare_paint_title($db,$row['titleFR']);
		$title_it=prepare_paint_title($db,$row['titleIT']);
		$title_zh=prepare_paint_title($db,$row['titleZH']);

$query_update="UPDATE ".TABLE_PAINTING." SET
            titleEN='".$title_en."',
            titleDE='".$title_de."',
            titleFR='".$title_fr."',
            titleIT='".$title_it."',
            titleZH='".$title_zh."'
        WHERE paintID='".$row['paintID']."' LIMIT 1";

        print "paintid = ".$row['paintID']."<br />";
        print "titleEN = |".$title_en."|<br />";
        print "titleDE = ".$title_de."<br />";
        print "titleFR = ".$title_fr."<br />";
        print "titleIT = ".$title_it."<br />";
        print "titleZH = ".$title_zh."<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);


}


exit;


# MEDIUMS

function prepare_medium($db,$medium)
{
	$medium=trim(strip_tags($medium));
	$medium=$db->db_prepare_input($medium);

	//$medium=UTILS::html_decode($medium);

	return $medium;
}

$query_media="SELECT * FROM ".TABLE_MEDIA." ";

//$query_media.=" WHERE mID=231 ";

$results=$db->query($query_media);

while( $row=$db->mysql_array($results,0) )
{

		$medium_en=prepare_medium($db,$row['mediumEN']);
		$medium_de=prepare_medium($db,$row['mediumDE']);
		$medium_fr=prepare_medium($db,$row['mediumFR']);
		$medium_it=prepare_medium($db,$row['mediumIT']);
		$medium_zh=prepare_medium($db,$row['mediumZH']);

$query_update="UPDATE ".TABLE_MEDIA." SET
            mediumEN='".$medium_en."',
            mediumDE='".$medium_de."',
            mediumFR='".$medium_fr."',
            mediumIT='".$medium_it."',
            mediumZH='".$medium_zh."'
        WHERE mID='".$row['mID']."' LIMIT 1";

        print "mediumid = ".$row['mID']."<br />";
        print "mediumEN = |".$medium_en."|<br />";
        print "mediumDE = ".$medium_de."<br />";
        print "mediumDE = ".$row['mediumDE']."<br />";
        print "mediumFR = ".$medium_fr."<br />";
        print "mediumIT = ".$medium_it."<br />";
        print "mediumZH = ".$medium_zh."<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);


}


exit;
# PAINTINGS

$query="SELECT * FROM painting2 ";
//$query.=" WHERE artworkID=5 AND titleDE LIKE 'Ohne Titel (%' ";
$query.=" WHERE artworkID=8 AND titleDE LIKE 'Ohne Titel (%' ";
$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

		$new_title=str_replace("Ohne Titel (", "", $row['titleDE']);
			$new_title=str_replace(")", "", $new_title);

			$titleurl=UTILS::convert_fortitleurl($db,TABLE_PAINTING,$new_title,"paintID",$row['paintID'],"titleurl_en","en");

        $query_update="UPDATE ".TABLE_PAINTING." SET ";
            $query_update.="titleEN='".$new_title."', ";
            $query_update.="titleDE='".$new_title."', ";
            $query_update.="titleFR='".$new_title."', ";
            $query_update.="titleIT='".$new_title."', ";
            $query_update.="titleZH='".$new_title."', ";
            $query_update.="titleurl='".$titleurl."' ";
        $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

        print "titleDE = ".$row['titleDE']."<br />";
        print "new_title = ".$new_title."<br />";
        print "titleurl = ".$titleurl."<br />";
        //print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);

}

exit;
# VIDEOS

$query="SELECT * FROM ".TABLE_VIDEO." ";
//$query.=" WHERE videoID=41";
//$query.=" WHERE videoID=242";

//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/videos/edit/?videoid=".$row['videoID']."' target='_blank'>".$row['videoID']."</a>-".$row['titleEN']."-".$row['title_en']."<br />";

	$query_relations="SELECT r.relationid AS relationid,p.paintID, p.paintID AS itemid2,p.number AS number FROM relations r, painting p
	WHERE ( ( r.typeid1=10 AND r.itemid1='".$row['videoID']."' AND r.typeid2=1 ) OR ( r.typeid2=10 AND r.itemid2='".$row['videoID']."' AND r.typeid1=1 ) )
	AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) )
	ORDER BY p.sort2 ASC, titleEN ASC";
	$results_relations=$db->query($query_relations);
	$count_relations=$db->numrows($results_relations);

	print "count=".$count_relations."<br /><br />";
	$i=0;
	while( $row_relations=$db->mysql_array($results_relations) )
	{
		//print $row_relations['relationid']." - ".$row_relations['itemid2']." - ".$row_relations['number']."<br />";

		$query_update=" UPDATE ".TABLE_RELATIONS." SET sort1='".$i."' WHERE relationid='".$row_relations['relationid']."' LIMIT 1 ";

		//print $query_update;

		//$db->query($query_update);


	    //print "<br />\n";
	    $i++;
	}

	print "<br /><br /><hr />\n";

}

?>
