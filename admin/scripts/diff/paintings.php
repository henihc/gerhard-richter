<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;
//$db=new dbCLASS(array(),DB_HOST, DB_USER, DB_PASSWORD, "richter_oppcheck2");

$html_print="";

$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
        	/*
        	$html_print.="<th>imageid</th>";
        	$html_print.="<th>image</th>";
        	*/
        	$html_print.="<th>paintid</th>";
        	$html_print.="<th>artwork</th>";
            $html_print.="<th>cr_number</th>";
            $html_print.="<th>titleDE</th>";
            $html_print.="<th>date</th>";
            $html_print.="<th>medium</th>";
            $html_print.="<th>width</th>";
            $html_print.="<th>height</th>";
            $html_print.="<th>pooptionid_verso_signed_1_1</th>";
            $html_print.="<th>pooptionid_verso_signed_1_2</th>";
            $html_print.="<th>poverso_signed_1_3</th>";
            $html_print.="<th>pooptionid_verso_signed_2_1</th>";
            $html_print.="<th>pooptionid_verso_signed_2_2</th>";
            $html_print.="<th>poverso_signed_2_3</th>";
            $html_print.="<th>pooptionid_verso_dated_1_1</th>";
            $html_print.="<th>pooptionid_verso_dated_1_2</th>";
            $html_print.="<th>poverso_dated_1_3</th>";
            $html_print.="<th>pooptionid_verso_dated_2_1</th>";
            $html_print.="<th>pooptionid_verso_dated_2_2</th>";
            $html_print.="<th>poverso_dated_2_3</th>";
            $html_print.="<th>pooptionid_verso_numbered_1</th>";
            $html_print.="<th>pooptionid_verso_numbered_2</th>";
            $html_print.="<th>poverso_numbered_3</th>";
            $html_print.="<th>pooptionid_verso_inscribed_1</th>";
            $html_print.="<th>pooptionid_verso_inscribed_2</th>";
            $html_print.="<th>poverso_inscribed_3</th>";
            $html_print.="<th>pooptionid_recto_signed_1_1</th>";
            $html_print.="<th>optionid_recto_signed_1_2</th>";
            $html_print.="<th>porecto_signed_1_3</th>";
            $html_print.="<th>pooptionid_recto_signed_2_1</th>";
            $html_print.="<th>optionid_recto_signed_2_2</th>";
            $html_print.="<th>porecto_signed_2_3</th>";
            $html_print.="<th>pooptionid_recto_dated_1_1</th>";
            $html_print.="<th>optionid_recto_dated_1_2</th>";
            $html_print.="<th>porecto_dated_1_3</th>";
            $html_print.="<th>pooptionid_recto_dated_2_1</th>";
            $html_print.="<th>optionid_recto_dated_2_2</th>";
            $html_print.="<th>porecto_dated_2_3</th>";
            $html_print.="<th>pooptionid_recto_numbered_1</th>";
            $html_print.="<th>pooptionid_recto_numbered_2</th>";
            $html_print.="<th>porecto_numbered_3</th>";
            $html_print.="<th>pooptionid_recto_inscribed_1</th>";
            $html_print.="<th>optionid_recto_inscribed_2</th>";
            $html_print.="<th>porecto_inscribed_3</th>";
            $html_print.="<th>location</th>";
            $html_print.="<th>city</th>";
            $html_print.="<th>country</th>";
            $html_print.="<th>loan_type</th>";
            $html_print.="<th>additional_location</th>";
            $html_print.="<th>additional_city</th>";
            $html_print.="<th>additional_country</th>";
            $html_print.="<th>pocopyright</th>";


        $html_print.="</tr>";
    $html_print.="</thead>";


    $query_export=" SELECT
		p.paintID AS paintid,
		p.artworkID AS artworkID,
		p.number AS cr_number,
		p.titleDE AS titleDE,
		p.date AS date,
		m.mediumEN AS medium,
		p.width AS width,
		p.height AS height,


	sdo2.title_en AS pooptionid_verso_signed_1_1, sdo3.title_en AS pooptionid_verso_signed_1_2, po.verso_signed_1_3 AS poverso_signed_1_3,
	sdo4.title_en AS pooptionid_verso_signed_2_1, sdo5.title_en AS pooptionid_verso_signed_2_2, po.verso_signed_2_3 AS poverso_signed_2_3,
	sdo6.title_en AS pooptionid_verso_dated_1_1, sdo7.title_en AS pooptionid_verso_dated_1_2, po.verso_dated_1_3 AS poverso_dated_1_3,
	sdo8.title_en AS pooptionid_verso_dated_2_1, sdo9.title_en AS pooptionid_verso_dated_2_2, po.verso_dated_2_3 AS poverso_dated_2_3,
	sdo10.title_en AS pooptionid_verso_numbered_1, sdo11.title_en AS pooptionid_verso_numbered_2, po.verso_numbered_3 AS poverso_numbered_3,
	sdo12.title_en AS pooptionid_verso_inscribed_1, sdo13.title_en AS pooptionid_verso_inscribed_2, po.verso_inscribed_3 AS poverso_inscribed_3,

	sdo14.title_en AS pooptionid_recto_signed_1_1, sdo15.title_en AS optionid_recto_signed_1_2, po.recto_signed_1_3 AS porecto_signed_1_3,
	sdo16.title_en AS pooptionid_recto_signed_2_1, sdo17.title_en AS optionid_recto_signed_2_2, po.recto_signed_2_3 AS porecto_signed_2_3,
	sdo18.title_en AS pooptionid_recto_dated_1_1, sdo19.title_en AS optionid_recto_dated_1_2, po.recto_dated_1_3 AS porecto_dated_1_3,
	sdo20.title_en AS pooptionid_recto_dated_2_1, sdo21.title_en AS optionid_recto_dated_2_2, po.recto_dated_2_3 AS porecto_dated_2_3,
	sdo22.title_en AS pooptionid_recto_numbered_1, sdo23.title_en AS pooptionid_recto_numbered_2, po.recto_numbered_3 AS porecto_numbered_3,
	sdo24.title_en AS pooptionid_recto_inscribed_1, sdo25.title_en AS optionid_recto_inscribed_2, po.recto_inscribed_3 AS porecto_inscribed_3,

	l.location_en AS location,ci.city_en AS city,co.country_en AS country,sdo1.title_en AS loan_type,l2.location_en AS additional_location,ci2.city_en AS additional_city,co2.country_en AS additional_country, po.copyright AS pocopyright

	FROM painting p
	LEFT JOIN painting_opp po ON p.paintID=po.paintid
	LEFT JOIN media m ON p.mID=m.mID
	LEFT JOIN locations l ON p.locationid=l.locationid
	LEFT JOIN locations_city ci ON p.cityid=ci.cityid
	LEFT JOIN locations_country co ON p.countryid=co.countryid
	LEFT JOIN select_dropdowns_options sdo1 ON p.optionid_loan_type=sdo1.optionid
	LEFT JOIN locations l2 ON p.loan_locationid=l2.locationid
	LEFT JOIN locations_city ci2 ON p.loan_cityid=ci2.cityid
	LEFT JOIN locations_country co2 ON p.loan_countryid=co2.countryid
	LEFT JOIN select_dropdowns_options sdo2 ON po.optionid_verso_signed_1_1=sdo2.optionid
	LEFT JOIN select_dropdowns_options sdo3 ON po.optionid_verso_signed_1_2=sdo3.optionid
	LEFT JOIN select_dropdowns_options sdo4 ON po.optionid_verso_signed_2_1=sdo4.optionid
	LEFT JOIN select_dropdowns_options sdo5 ON po.optionid_verso_signed_2_2=sdo5.optionid
	LEFT JOIN select_dropdowns_options sdo6 ON po.optionid_verso_dated_1_1=sdo6.optionid
	LEFT JOIN select_dropdowns_options sdo7 ON po.optionid_verso_dated_1_2=sdo7.optionid
	LEFT JOIN select_dropdowns_options sdo8 ON po.optionid_verso_dated_2_1=sdo8.optionid
	LEFT JOIN select_dropdowns_options sdo9 ON po.optionid_verso_dated_2_2=sdo9.optionid
	LEFT JOIN select_dropdowns_options sdo10 ON po.optionid_verso_numbered_1=sdo10.optionid
	LEFT JOIN select_dropdowns_options sdo11 ON po.optionid_verso_numbered_2=sdo11.optionid
	LEFT JOIN select_dropdowns_options sdo12 ON po.optionid_verso_inscribed_1=sdo12.optionid
	LEFT JOIN select_dropdowns_options sdo13 ON po.optionid_verso_inscribed_2=sdo13.optionid

	LEFT JOIN select_dropdowns_options sdo14 ON po.optionid_recto_signed_1_1=sdo14.optionid
	LEFT JOIN select_dropdowns_options sdo15 ON po.optionid_recto_signed_1_2=sdo15.optionid
	LEFT JOIN select_dropdowns_options sdo16 ON po.optionid_recto_signed_2_1=sdo16.optionid
	LEFT JOIN select_dropdowns_options sdo17 ON po.optionid_recto_signed_2_2=sdo17.optionid
	LEFT JOIN select_dropdowns_options sdo18 ON po.optionid_recto_dated_1_1=sdo18.optionid
	LEFT JOIN select_dropdowns_options sdo19 ON po.optionid_recto_dated_1_2=sdo19.optionid
	LEFT JOIN select_dropdowns_options sdo20 ON po.optionid_recto_dated_2_1=sdo20.optionid
	LEFT JOIN select_dropdowns_options sdo21 ON po.optionid_recto_dated_2_2=sdo21.optionid
	LEFT JOIN select_dropdowns_options sdo22 ON po.optionid_recto_numbered_1=sdo22.optionid
	LEFT JOIN select_dropdowns_options sdo23 ON po.optionid_recto_numbered_2=sdo23.optionid
	LEFT JOIN select_dropdowns_options sdo24 ON po.optionid_recto_inscribed_1=sdo24.optionid
	LEFT JOIN select_dropdowns_options sdo25 ON po.optionid_recto_inscribed_2=sdo25.optionid ";

	$query_export2=$query_export;

	$query_export_where=" WHERE (p.artworkID=6 && p.paintID NOT BETWEEN 15617 AND 15634) ";
	$query_export_order=" ORDER BY p.sort2 ASC, p.titleDE ASC ";
    //$query_export_limit=" LIMIT 0,5 ";

    $query_export.=$query_export_where.$query_export_order.$query_export_limit;

    //print $query_export;

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    print $count."<br />";





    //$i=0;
    while( $row=$db->mysql_array($results) )
    {
        //$i++;
        $row=UTILS::html_decode($row);

		$db2=new dbCLASS(array(),DB_HOST, DB_USER, DB_PASSWORD, "richter_oppcheck1");

		$query_export2_1=$query_export2." WHERE p.paintID='".$row['paintid']."' ";

		//print $query_export2;

	    $results2=$db2->query($query_export2_1);
	    $count2=$db2->numrows($results2);
	    if( $count2>0 )
        {
	    	$row2=$db2->mysql_array($results2);
	    	$row2=UTILS::html_decode($row2);

			if( $row['cr_number']!=$row2['cr_number'] ) { $diff=1; $td_style1="background:#ff9c9c;"; } else  { $td_style1=""; }
			if( $row['titleDE']!=$row2['titleDE'] ) { $diff=1; $td_style2="background:#ff9c9c;"; } else  { $td_style2=""; }
			if( $row['date']!=$row2['date'] ) { $diff=1; $td_style3="background:#ff9c9c;"; } else  { $td_style3=""; }
			if( $row['medium']!=$row2['medium'] ) { $diff=1; $td_style4="background:#ff9c9c;"; } else  { $td_style4=""; }
			if( $row['width']!=$row2['width'] ) { $diff=1; $td_style5="background:#ff9c9c;"; } else  { $td_style5=""; }
			if( $row['height']!=$row2['height'] ) { $diff=1; $td_style6="background:#ff9c9c;"; } else  { $td_style6=""; }
			if( $row['pooptionid_verso_signed_1_1']!=$row2['pooptionid_verso_signed_1_1'] ) { $diff=1; $td_style7="background:#ff9c9c;"; } else  { $td_style7=""; }
			if( $row['pooptionid_verso_signed_1_2']!=$row2['pooptionid_verso_signed_1_2'] ) { $diff=1; $td_style8="background:#ff9c9c;"; } else  { $td_style8=""; }
			if( $row['poverso_signed_1_3']!=$row2['poverso_signed_1_3'] ) { $diff=1; $td_style9="background:#ff9c9c;"; } else  { $td_style9=""; }
			if( $row['pooptionid_verso_signed_2_1']!=$row2['pooptionid_verso_signed_2_1'] ) { $diff=1; $td_style10="background:#ff9c9c;"; } else  { $td_style10=""; }
			if( $row['pooptionid_verso_signed_2_2']!=$row2['pooptionid_verso_signed_2_2'] ) { $diff=1; $td_style11="background:#ff9c9c;"; } else  { $td_style11=""; }
			if( $row['poverso_signed_2_3']!=$row2['poverso_signed_2_3'] ) { $diff=1; $td_style12="background:#ff9c9c;"; } else  { $td_style12=""; }
			if( $row['pooptionid_verso_dated_1_1']!=$row2['pooptionid_verso_dated_1_1'] ) { $diff=1; $td_style13="background:#ff9c9c;"; } else  { $td_style13=""; }
			if( $row['pooptionid_verso_dated_1_2']!=$row2['pooptionid_verso_dated_1_2'] ) { $diff=1; $td_style14="background:#ff9c9c;"; } else  { $td_style14=""; }
			if( $row['poverso_dated_1_3']!=$row2['poverso_dated_1_3'] ) { $diff=1; $td_style15="background:#ff9c9c;"; } else  { $td_style15=""; }
			if( $row['pooptionid_verso_dated_2_1']!=$row2['pooptionid_verso_dated_2_1'] ) { $diff=1; $td_style16="background:#ff9c9c;"; } else  { $td_style16=""; }
			if( $row['pooptionid_verso_dated_2_2']!=$row2['pooptionid_verso_dated_2_2'] ) { $diff=1; $td_style17="background:#ff9c9c;"; } else  { $td_style17=""; }
			if( $row['poverso_dated_2_3']!=$row2['poverso_dated_2_3'] ) { $diff=1; $td_style18="background:#ff9c9c;"; } else  { $td_style18=""; }
			if( $row['pooptionid_verso_numbered_1']!=$row2['pooptionid_verso_numbered_1'] ) { $diff=1; $td_style19="background:#ff9c9c;"; } else  { $td_style19=""; }
			if( $row['pooptionid_verso_numbered_2']!=$row2['pooptionid_verso_numbered_2'] ) { $diff=1; $td_style20="background:#ff9c9c;"; } else  { $td_style20=""; }
			if( $row['poverso_numbered_3']!=$row2['poverso_numbered_3'] ) { $diff=1; $td_style21="background:#ff9c9c;"; } else  { $td_style21=""; }
			if( $row['pooptionid_verso_inscribed_1']!=$row2['pooptionid_verso_inscribed_1'] ) { $diff=1; $td_style22="background:#ff9c9c;"; } else  { $td_style22=""; }
			if( $row['pooptionid_verso_inscribed_2']!=$row2['pooptionid_verso_inscribed_2'] ) { $diff=1; $td_style23="background:#ff9c9c;"; } else  { $td_style23=""; }
			if( $row['poverso_inscribed_3']!=$row2['poverso_inscribed_3'] ) { $diff=1; $td_style24="background:#ff9c9c;"; } else  { $td_style24=""; }
			if( $row['pooptionid_recto_signed_1_1']!=$row2['pooptionid_recto_signed_1_1'] ) { $diff=1; $td_style25="background:#ff9c9c;"; } else  { $td_style25=""; }
			if( $row['optionid_recto_signed_1_2']!=$row2['optionid_recto_signed_1_2'] ) { $diff=1; $td_style26="background:#ff9c9c;"; } else  { $td_style26=""; }
			if( $row['porecto_signed_1_3']!=$row2['porecto_signed_1_3'] ) { $diff=1; $td_style27="background:#ff9c9c;"; } else  { $td_style27=""; }
			if( $row['pooptionid_recto_signed_2_1']!=$row2['pooptionid_recto_signed_2_1'] ) { $diff=1; $td_style28="background:#ff9c9c;"; } else  { $td_style28=""; }
			if( $row['optionid_recto_signed_2_2']!=$row2['optionid_recto_signed_2_2'] ) { $diff=1; $td_style29="background:#ff9c9c;"; } else  { $td_style29=""; }
			if( $row['porecto_signed_2_3']!=$row2['porecto_signed_2_3'] ) { $diff=1; $td_style30="background:#ff9c9c;"; } else  { $td_style30=""; }
			if( $row['pooptionid_recto_dated_1_1']!=$row2['pooptionid_recto_dated_1_1'] ) { $diff=1; $td_style31="background:#ff9c9c;"; } else  { $td_style31=""; }
			if( $row['optionid_recto_dated_1_2']!=$row2['optionid_recto_dated_1_2'] ) { $diff=1; $td_style32="background:#ff9c9c;"; } else  { $td_style32=""; }
			if( $row['porecto_dated_1_3']!=$row2['porecto_dated_1_3'] ) { $diff=1; $td_style33="background:#ff9c9c;"; } else  { $td_style33=""; }
			if( $row['pooptionid_recto_dated_2_1']!=$row2['pooptionid_recto_dated_2_1'] ) { $diff=1; $td_style34="background:#ff9c9c;"; } else  { $td_style34=""; }
			if( $row['optionid_recto_dated_2_2']!=$row2['optionid_recto_dated_2_2'] ) { $diff=1; $td_style35="background:#ff9c9c;"; } else  { $td_style35=""; }
			if( $row['porecto_dated_2_3']!=$row2['porecto_dated_2_3'] ) { $diff=1; $td_style36="background:#ff9c9c;"; } else  { $td_style36=""; }
			if( $row['pooptionid_recto_numbered_1']!=$row2['pooptionid_recto_numbered_1'] ) { $diff=1; $td_style37="background:#ff9c9c;"; } else  { $td_style37=""; }
			if( $row['pooptionid_recto_numbered_2']!=$row2['pooptionid_recto_numbered_2'] ) { $diff=1; $td_style38="background:#ff9c9c;"; } else  { $td_style38=""; }
			if( $row['porecto_numbered_3']!=$row2['porecto_numbered_3'] ) { $diff=1; $td_style39="background:#ff9c9c;"; } else  { $td_style39=""; }
			if( $row['pooptionid_recto_inscribed_1']!=$row2['pooptionid_recto_inscribed_1'] ) { $diff=1; $td_style40="background:#ff9c9c;"; } else  { $td_style40=""; }
			if( $row['optionid_recto_inscribed_2']!=$row2['optionid_recto_inscribed_2'] ) { $diff=1; $td_style41="background:#ff9c9c;"; } else  { $td_style41=""; }
			if( $row['porecto_inscribed_3']!=$row2['porecto_inscribed_3'] ) { $diff=1; $td_style42="background:#ff9c9c;"; } else  { $td_style42=""; }
			if( $row['location']!=$row2['location'] ) { $diff=1; $td_style43="background:#ff9c9c;"; } else  { $td_style43=""; }
			if( $row['city']!=$row2['city'] ) { $diff=1; $td_style44="background:#ff9c9c;"; } else  { $td_style44=""; }
			if( $row['country']!=$row2['country'] ) { $diff=1; $td_style45="background:#ff9c9c;"; } else  { $td_style45=""; }
			if( $row['loan_type']!=$row2['loan_type'] ) { $diff=1; $td_style46="background:#ff9c9c;"; } else  { $td_style46=""; }
			if( $row['additional_location']!=$row2['additional_location'] ) { $diff=1; $td_style47="background:#ff9c9c;"; } else  { $td_style47=""; }
			if( $row['additional_city']!=$row2['additional_city'] ) { $diff=1; $td_style48="background:#ff9c9c;"; } else  { $td_style48=""; }
			if( $row['additional_country']!=$row2['additional_country'] ) { $diff=1; $td_style49="background:#ff9c9c;"; } else  { $td_style49=""; }
			if( $row['pocopyright']!=$row2['pocopyright'] ) { $diff=1; $td_style50="background:#ff9c9c;"; } else  { $td_style50=""; }

	        if( $diff ) $td_style_id="background:#ff9c9c;";
	        else $td_style_id="";

		}
        else
        {
            $diff=0;
        }

/*
        $query_related_images="SELECT
            COALESCE(
                r1.itemid1,
                r2.itemid2
            ) as itemid,
            COALESCE(
                r1.sort,
                r2.sort
            ) as sort_rel,
            COALESCE(
                r1.relationid,
                r2.relationid
            ) as relationid_rel
            FROM
                ".TABLE_PAINTING." p
            LEFT JOIN
                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
            LEFT JOIN
                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
            WHERE p.paintID='".$row['paintid']."'
            ORDER BY sort_rel ASC, relationid_rel DESC
            LIMIT 1
        ";
        $results_related_image=$db->query($query_related_images);
        $row_related_image=$db->mysql_array($results_related_image);

        $query_where_image=" WHERE imageid='".$row_related_image['itemid']."' ";
        $query_image=QUERIES::query_images($db,$query_where_image,$query_order,$query_limit);
        $results_image=$db->query($query_image['query']);
        $count_image=$db->numrows($results_image);
        $row_image=$db->mysql_array($results_image);

        $options_painting_url=array();
        $options_painting_url['paintid']=$row['paintid'];
        $painting_url=UTILS::get_painting_url($db,$options_painting_url);

        //if( empty($row_related_image['itemid']) ) $td_style="background:#ff9c9c;";
        //else $td_style="";
        */

        if( $diff )
        {
        $html_print.="<tr>";
        /*
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$i;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                if( !empty($row_related_image['itemid']) )
                {
                    $src="/images/size_m__imageid_".$row_related_image['itemid'].".jpg";
                    $link="/images/size_o__imageid_".$row_related_image['itemid'].".jpg";
                    $html_print.="\t<a href='".$link."' title='' target='blank' >\n";
                        $html_print.="\t<img src='".$src."' alt='' />\n";
                    $html_print.="\t</a>\n";
                }
                else
                {
                    $html_print.="NO IMAGE AVAILABLE";
                }
            $html_print.="</td>";
        */
       /*
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='/admin/images/edit/?imageid=".$row_related_image['itemid']."' title='' target='blank' >\n";
                    $html_print.=$row_related_image['itemid'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row_image['src_old'];
            $html_print.="</td>";
            */
            $html_print.="<td style='".$td_style_id."'>";
                $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['paintid']."' title='' target='blank' >\n";
                    $html_print.=$row['paintid'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=UTILS::get_artwork_name($db,$row['artworkID']);
            $html_print.="</td>";

            $html_print.="<td style='".$td_style1."'>";
                $html_print.=$row['cr_number'];
                if( !empty($td_style1) )
                {
                	$html_print.="<br /><hr />".$row2['cr_number'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style2."'>";
                $html_print.=$row['titleDE'];
                if( !empty($td_style2) )
                {
                	$html_print.="<br /><hr />".$row2['titleDE'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style3."'>";
                $html_print.=$row['date'];
                if( !empty($td_style3) )
                {
                	$html_print.="<br /><hr />".$row2['date'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style4."'>";
                $html_print.=$row['medium'];
                if( !empty($td_style4) )
                {
                	$html_print.="<br /><hr />".$row2['medium'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style5."'>";
                $html_print.=$row['width'];
                if( !empty($td_style5) )
                {
                	$html_print.="<br /><hr />".$row2['width'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style6."'>";
                $html_print.=$row['height'];
                if( !empty($td_style6) )
                {
                	$html_print.="<br /><hr />".$row2['height'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style7."'>";
                $html_print.=$row['pooptionid_verso_signed_1_1'];
                if( !empty($td_style7) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_signed_1_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style8."'>";
                $html_print.=$row['pooptionid_verso_signed_1_2'];
                if( !empty($td_style8) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_signed_1_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style9."'>";
                $html_print.=$row['poverso_signed_1_3'];
                if( !empty($td_style9) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_signed_1_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style10."'>";
                $html_print.=$row['pooptionid_verso_signed_2_1'];
                if( !empty($td_style10) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_signed_2_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style11."'>";
                $html_print.=$row['pooptionid_verso_signed_2_2'];
                if( !empty($td_style11) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_signed_2_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style12."'>";
                $html_print.=$row['poverso_signed_2_3'];
                if( !empty($td_style12) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_signed_2_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style13."'>";
                $html_print.=$row['pooptionid_verso_dated_1_1'];
                if( !empty($td_style13) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_dated_1_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style14."'>";
                $html_print.=$row['pooptionid_verso_dated_1_2'];
                if( !empty($td_style14) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_dated_1_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style15."'>";
                $html_print.=$row['poverso_dated_1_3'];
                if( !empty($td_style15) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_dated_1_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style16."'>";
                $html_print.=$row['pooptionid_verso_dated_2_1'];
                if( !empty($td_style16) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_dated_2_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style17."'>";
                $html_print.=$row['pooptionid_verso_dated_2_2'];
                if( !empty($td_style17) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_dated_2_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style18."'>";
                $html_print.=$row['poverso_dated_2_3'];
                if( !empty($td_style18) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_dated_2_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style19."'>";
                $html_print.=$row['pooptionid_verso_numbered_1'];
                if( !empty($td_style19) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_numbered_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style20."'>";
                $html_print.=$row['pooptionid_verso_numbered_2'];
                if( !empty($td_style20) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_numbered_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style21."'>";
                $html_print.=$row['poverso_numbered_3'];
                if( !empty($td_style21) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_numbered_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style22."'>";
                $html_print.=$row['pooptionid_verso_inscribed_1'];
                if( !empty($td_style22) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_inscribed_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style23."'>";
                $html_print.=$row['pooptionid_verso_inscribed_2'];
                if( !empty($td_style23) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_verso_inscribed_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style24."'>";
                $html_print.=$row['poverso_inscribed_3'];
                if( !empty($td_style24) )
                {
                	$html_print.="<br /><hr />".$row2['poverso_inscribed_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style25."'>";
                $html_print.=$row['pooptionid_recto_signed_1_1'];
                if( !empty($td_style25) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_signed_1_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style26."'>";
                $html_print.=$row['optionid_recto_signed_1_2'];
                if( !empty($td_style26) )
                {
                	$html_print.="<br /><hr />".$row2['optionid_recto_signed_1_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style27."'>";
                $html_print.=$row['porecto_signed_1_3'];
                if( !empty($td_style27) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_signed_1_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style28."'>";
                $html_print.=$row['pooptionid_recto_signed_2_1'];
                if( !empty($td_style28) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_signed_2_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style29."'>";
                $html_print.=$row['optionid_recto_signed_2_2'];
                if( !empty($td_style29) )
                {
                	$html_print.="<br /><hr />".$row2['optionid_recto_signed_2_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style30."'>";
                $html_print.=$row['porecto_signed_2_3'];
                if( !empty($td_style30) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_signed_2_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style31."'>";
                $html_print.=$row['pooptionid_recto_dated_1_1'];
                if( !empty($td_style31) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_dated_1_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style32."'>";
                $html_print.=$row['optionid_recto_dated_1_2'];
                if( !empty($td_style32) )
                {
                	$html_print.="<br /><hr />".$row2['optionid_recto_dated_1_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style33."'>";
                $html_print.=$row['porecto_dated_1_3'];
                if( !empty($td_style33) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_dated_1_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style34."'>";
                $html_print.=$row['pooptionid_recto_dated_2_1'];
                if( !empty($td_style34) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_dated_2_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style35."'>";
                $html_print.=$row['optionid_recto_dated_2_2'];
                if( !empty($td_style35) )
                {
                	$html_print.="<br /><hr />".$row2['optionid_recto_dated_2_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style36."'>";
                $html_print.=$row['porecto_dated_2_3'];
                if( !empty($td_style36) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_dated_2_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style37."'>";
                $html_print.=$row['pooptionid_recto_numbered_1'];
                if( !empty($td_style37) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_numbered_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style38."'>";
                $html_print.=$row['pooptionid_recto_numbered_2'];
                if( !empty($td_style38) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_numbered_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style39."'>";
                $html_print.=$row['porecto_numbered_3'];
                if( !empty($td_style39) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_numbered_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style40."'>";
                $html_print.=$row['pooptionid_recto_inscribed_1'];
                if( !empty($td_style40) )
                {
                	$html_print.="<br /><hr />".$row2['pooptionid_recto_inscribed_1'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style41."'>";
                $html_print.=$row['optionid_recto_inscribed_2'];
                if( !empty($td_style41) )
                {
                	$html_print.="<br /><hr />".$row2['optionid_recto_inscribed_2'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style42."'>";
                $html_print.=$row['porecto_inscribed_3'];
                if( !empty($td_style42) )
                {
                	$html_print.="<br /><hr />".$row2['porecto_inscribed_3'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style43."'>";
                $html_print.=$row['location'];
                if( !empty($td_style43) )
                {
                	$html_print.="<br /><hr />".$row2['location'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style44."'>";
                $html_print.=$row['city'];
                if( !empty($td_style44) )
                {
                	$html_print.="<br /><hr />".$row2['city'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style45."'>";
                $html_print.=$row['country'];
                if( !empty($td_style45) )
                {
                	$html_print.="<br /><hr />".$row2['country'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style46."'>";
                $html_print.=$row['loan_type'];
                if( !empty($td_style46) )
                {
                	$html_print.="<br /><hr />".$row2['loan_type'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style47."'>";
                $html_print.=$row['additional_location'];
                if( !empty($td_style47) )
                {
                	$html_print.="<br /><hr />".$row2['additional_location'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style48."'>";
                $html_print.=$row['additional_city'];
                if( !empty($td_style48) )
                {
                	$html_print.="<br /><hr />".$row2['additional_city'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style49."'>";
                $html_print.=$row['additional_country'];
                if( !empty($td_style49) )
                {
                	$html_print.="<br /><hr />".$row2['additional_country'];
                }
            $html_print.="</td>";
            $html_print.="<td style='".$td_style50."'>";
                $html_print.=$row['pocopyright'];
                if( !empty($td_style50) )
                {
                	$html_print.="<br /><hr />".$row2['pocopyright'];
                }
            $html_print.="</td>";

        $html_print.="</tr>";

    	}

        $diff=0;

        /*
        # updating dimensions
        if( empty($row['powidth']) || empty($row['poheight']) )
        {
            if( !empty($row_related_image['itemid']) )
            {
                $dir=DATA_PATH."/images_new/original/";
                $filename=$row_related_image['itemid'];

                if( file_exists($dir.$filename.".jpg") || file_exists($dir.$filename.".jpeg") )
                {
                    $ext=".jpg";
                }
                elseif( file_exists($dir.$filename.".png") )
                {
                    $ext=".png";
                }
                elseif( file_exists($dir.$filename.".gif") )
                {
                    $ext=".gif";
                }
                elseif( file_exists($dir.$filename.".bmp") )
                {
                    $ext=".bmp";
                }
                else
                {
                    exit("Unsupported file format! Supported formats: JPEG, GIF, PNG, BMP ");
                }

                $image_orginal_path=$dir.$filename.$ext;

                list($width, $height) = getimagesize($image_orginal_path);
                print "\t<a href='".$link."' title='' target='blank' >\n";
                    //print "\t<img src='".$src."' alt='' />\n";
                    print $image_orginal_path;
                print "\t</a>\n";
                print "<br />width-".$width."<br />";
                print "height-".$height."<br />";

                if( empty($width) ) $width="NULL";
                if( empty($height) ) $height="NULL";
                $query_update_dimensions="UPDATE ".TABLE_PAINTING_OPP." SET width=".$width.", height=".$height." WHERE oppinfoid='".$row['pooppinfoid']."' AND paintid='".$row['ppaintID']."' LIMIT 1 ";
                //$db->query($query_update_dimensions);
                //print $query_update_dimensions;

                print "<hr /><br /><br />";
            }
        }
        # END updating dimensions
        */

        # saving images
        /*
        if( !empty($row_related_image['itemid']) )
        {
            $dir=DATA_PATH."/images_new/original/";
            $filename=$row_related_image['itemid'];

            if( file_exists($dir.$filename.".jpg") || file_exists($dir.$filename.".jpeg") )
            {
                $ext=".jpg";
            }
            elseif( file_exists($dir.$filename.".png") )
            {
                $ext=".png";
            }
            elseif( file_exists($dir.$filename.".gif") )
            {
                $ext=".gif";
            }
            elseif( file_exists($dir.$filename.".bmp") )
            {
                $ext=".bmp";
            }
            else
            {
                exit("Unsupported file format! Supported formats: JPEG, GIF, PNG, BMP ");
            }

            $image_orginal_path=$dir.$filename.$ext;

            copy($image_orginal_path, DATA_PATH."/export/paintings/".$filename.$ext);
        }
        */
        # END saving images

    }

/*
}
*/

$html_print.="</table>";

print $html_print;

exit;


#SALE HISTORY
/*
$csv="paintID;saleID;auction_house;saleDate;lotNo;estLow;estHigh;estCurr;estLowUSD;estHighUSD;soldFor;soldForCurr;soldForUSD;boughtIn;saleName;saleType;number\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_SALEHISTORY." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $results_sale_relations=$db->query("SELECT relationid,typeid1,itemid1,typeid2,itemid2,sort FROM ".TABLE_RELATIONS." WHERE ( itemid1='".$row['saleID']."' AND typeid1=2 AND typeid2=1 ) OR ( itemid2='".$row['saleID']."' AND typeid2=2 AND typeid1=1 ) ORDER BY sort ASC, relationid DESC ");
        $row_sale_relations=$db->mysql_array($results_sale_relations);
        $paintid=UTILS::get_relation_id($db,1,$row_sale_relations);
        $results_house=$db->query("SELECT house FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row['ahID']."' ");
        $row_house=$db->mysql_array($results_house);
        $row_house=UTILS::html_decode($row_house);
        $results_esti_curr=$db->query("SELECT currency FROM ".TABLE_CURRENCY." WHERE currID='".$row['estCurrID']."' ");
        $row_esti_curr=$db->mysql_array($results_esti_curr);
        $results_sold_curr=$db->query("SELECT currency FROM ".TABLE_CURRENCY." WHERE currID='".$row['soldForCurrID']."' ");
        $row_sold_curr=$db->mysql_array($results_sold_curr);
        $csv.="\"".UTILS::prepare_for_csv($db,$paintid)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_house['house'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleDate'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['lotNo'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estLow'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estHigh'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_esti_curr['currency'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estLowUSD'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estHighUSD'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['soldFor'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_sold_curr['currency'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['soldForUSD'])."\";";
                            if( $row['boughtIn']==1 ) $boughtin="yes";
                            else $boughtin="no";
        $csv.="\"".UTILS::prepare_for_csv($db,$boughtin)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleName'])."\";";
                            if( $row['saleType']==1 ) $saleType="Premium";
                            elseif( $row['saleType']==2 ) $saleType="Hammer";
                            elseif( $row['saleType']==3 ) $saleType="Unknown";
                            else $saleType="";
        $csv.="\"".UTILS::prepare_for_csv($db,$saleType)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
*/

#EXHIBITIONS
/*
//$csv="exID;title_original;titleEN;titleDE;titleCH;descriptionEN;descriptionDE;descriptionCH;type;notes;locationEN;locationDE;locationCH;startDate;endDate\r\n";
$csv="exID|title_original|titleEN|titleFR|descriptionEN|descriptionFR|locationEN|locationFR\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_EXHIBITIONS." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['exID'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_original'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\"|";
        if( $row['descriptionEN']=="<br>" ) $row['descriptionEN']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\"|";
        //if( $row['descriptionDE']=="<br>" ) $row['descriptionDE']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\"|";
        if( $row['descriptionFR']=="<br>" ) $row['descriptionFR']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionFR'])."\"|";
        //if( $row['descriptionCH']=="<br>" ) $row['descriptionCH']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['type'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['startDate2'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['endDate2'])."\"|";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */
#EXHIBITIONS PAINTINGS
/*
$csv="paintID;exID;artworkID;editionID;titleEN;titleDE;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";

    $results_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." ");
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC ";
        $results_rel=$db->query($query);
        while( $row_rel=$db->mysql_array($results_rel) )
{
$paintid=UTILS::get_relation_id($db,"1",$row_rel);
$results=$db->query("SELECT
                paintID,
                artworkID,
                editionID,
                titleEN,
                titleDE,
                titleCH,
                year,
                number,
                ednumber,
                ednumberroman,
                ednumberap,
                citylifepage,
                nr_of_editions,
                catID,
                size,
                height,
                width,
                src,
                mID,
                muID,
                sort,
                artwork_notesEN,
                artwork_notesDE,
                artwork_notesCH,
                notes,
                image_zoomer,
                created,
                modified,
                enable
            FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");

$row=$db->mysql_array($results);
    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    $csv.="\"".UTILS::prepare_for_csv($db,$row['paintID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row_exh['exID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artworkID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['editionID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";

    //$csv.="<br />\r\n";
    $csv.="\r\n";

}
    }
$html_print.=$csv;
*/





# LITERATURE categories
/*
$csv="books_catid;title_en;title_de;title_ch\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_BOOKS_CATEGORIES." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['books_catid'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */

#BIOGRAPHY
/*
//$csv="biographyid;title_en;title_de;title_ch;text_en;text_de;text_ch\r\n";
$csv="biographyid|title_en\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_BIOGRAPHY." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="".UTILS::prepare_for_csv($db,$row['biographyid'])."|";
        $csv.="".UTILS::prepare_for_csv($db,$row['title_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['title_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['title_ch'])."\";";
        $csv.="".UTILS::prepare_for_csv($db,$row['text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */

#QUOTES
/*
//$csv="quoteid;quotes_categoryid;year;text_en;text_de;text_ch;source_en;source_de;source_ch;source_text_en;source_text_de;source_text_ch;\r\n";
//$csv="quoteid|text_en|source_en|source_text_en|\r\n";
$csv="quoteid|source_text_en\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_QUOTES." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="".UTILS::prepare_for_csv($db,$row['quoteid'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['quotes_categoryid'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
        //$csv.="".UTILS::prepare_for_csv($db,$row['text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_ch'])."\";";
        //$csv.="".UTILS::prepare_for_csv($db,$row['source_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_ch'])."\";";
        $csv.="".UTILS::prepare_for_csv($db,$row['source_text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_text_ch'])."\";";
        $csv.="\r\n";
    }
   $html_print.=$csv;
 */

# VIDEO
/*
$csv="videoID;titleEN;titleDE;titleCH;titlelong_en;titlelong_de;titlelong_ch;location_en;location_de;location_ch;descriptionEN;descriptionDE;descriptionCH;info_en;info_de;info_ch;\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_VIDEO." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['videoID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_ch'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_ch'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */


# MEDIUMS
/*
$query="SELECT * FROM ".TABLE_MEDIA." ORDER BY mID ASC ";
$results=$db->query($query);
$count=$db->numrows($results);

$csv="mediumEN;mediumDE;mediumFR;mediumCH;\r\n";
            while( $row=$db->mysql_array($results) )
            {
        $row=UTILS::html_decode($row);
                $results_used=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE mID='".$row['mID']."' ");
                $count_used=$db->numrows($results_used);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumFR'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumCH'])."\";";
        $csv.="\"".$count_used."\";";
        $csv.="\r\n";
            }
    $html_print.=$csv;
 */

#LITERATURE
/*
//$csv="id;books_catid;title;author;publisher;year;language;isbn;cover;NOpages;notesEN;notesDE;notesCH;infoEN;infoDE;infoCH\r\n";
$csv="id;title;author;publisher;year\r\n";
//$csv="bookid;infoEN;infoDE;infoFR;\r\n";

    //$results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE (infoEN IS NULL OR infoEN='') AND (books_catid=1 OR books_catid=2 OR books_catid=4) ");
    $results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE infoEN IS NOT NULL AND infoEN!='' ");
    //$results=$db->query("SELECT * FROM ".TABLE_BOOKS." ");
    $count=$db->numrows($results);
    $html_print.=$count;
    while( $row=$db->mysql_array($results) )
    {

        //$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=1 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        //$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        //$row_related_image=$db->mysql_array($results_related_image);
        //$count_image=$db->numrows($results_related_image);
        //$imageid=UTILS::get_relation_id($db,"17",$row_related_image);
        //$src="/images/size_xs__imageid_".$imageid.".jpg";

        //if( $count_image )
        //{
        $row=UTILS::html_decode($row);
        //$html_print.="<img src='".$src."' alt='' />";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['id'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['books_catid'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['author'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['publisher'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['language'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['isbn'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['cover'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['NOpages'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesEN'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesDE'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesCH'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoEN'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoDE'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoFR'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoCH'])."\";";
        $csv.="\r\n";
        //$csv.="<br />";
        //}
    }
    $html_print.=$csv;
 */
#PAINTINGS
//exit;
    //$query_where_painting=" WHERE ( p.artworkID=1 OR p.artworkID=2 OR p.artworkID=13 ) AND artwork_notesEN!='' AND artwork_notesEN IS NOT NULL ";
    //$query_where_painting=" WHERE ( p.artworkID=1 OR p.artworkID=2 OR p.artworkID=13 ) ";
    $query_where_painting=" WHERE ( p.artworkID=6 ) ";
    //$query_where_painting.=" AND ( titleEN LIKE '%Jan%' OR titleEN LIKE '%Feb%' OR titleEN LIKE '%March%' OR titleEN LIKE '%April%' OR titleEN LIKE '%May%' OR titleEN LIKE '%June%' OR titleEN LIKE '%July%' OR titleEN LIKE '%August%' OR titleEN LIKE '%Sep%' OR titleEN LIKE '%Oct%' OR titleEN LIKE '%Nov%' OR titleEN LIKE '%Dec%' ) ";
    //$query_where_painting=" WHERE p.catID=0 OR p.catID IS NULL OR p.catID='' ";
    //$query_where_painting=" WHERE paintID=5549 ";
    $query_order=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results=$db->query($query_painting['query']);


//$csv="paintID;artworkID;editionID;titleEN;titleDE;titleFR;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesFR;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";
//$csv="paintID|number|artwork_notesEN\r\n";
//$csv="paintID|artwork|titleEN|titleDE|number|year|paintings\r\n";
//$csv="paintID;artwork;number;year;titleEN;titleDE;titleFR;titleIT,titleCN\r\n";
$csv="paintID;artwork;year;date;number;titleEN;titleDE;titleFR;titleIT;titleZH\r\n";
$paintings_array=array();
while( $row=$db->mysql_array($results) )
{
    $test=1;
/*
    $tmp_en=explode("-",$row['titleurl_en']);
    foreach($tmp_en as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_en'];
            $html_print.=$count_length."=".$row['titleEN']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_de=explode("-",$row['titleurl_de']);
    foreach($tmp_de as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_de'];
            $html_print.=$count_length."=".$row['titleDE']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_fr=explode("-",$row['titleurl_fr']);
    foreach($tmp_fr as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_fr'];
            $html_print.=$count_length."=".$row['titleFR']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_it=explode("-",$row['titleurl_it']);
    foreach($tmp_it as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_it'];
            $html_print.=$count_length."=".$row['titleIT']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_ch=explode("-",$row['titleurl_ch']);
    foreach($tmp_ch as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_ch'];
            $html_print.=$count_length."=".$row['titleCH']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }
 */

    if($test==1)
    {


    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    /*
    $query_where_painting=" WHERE ( p.artworkID=5 OR p.artworkID=7 OR p.artworkID=8 ) AND p.titleDE=\"".$row['titleDE']."\" AND paintID!='".$row['paintID']."' ";
    $query_order=" ORDER BY p.sort2 ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results2=$db->query($query_painting['query']);

$count2=$db->numrows($results2);
if( $count2>=1 )
{

        if( $i==1 )
        {
            $artwork=UTILS::get_artwork_name($db,$row2['artworkID']);
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['paintID'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$artwork)."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleEN'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleDE'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['number'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['year'])."\"|";
        }
        else
        {
        $csv.=UTILS::prepare_for_csv($db,$row2['paintID']).",";
        }
        $paintings_array[$row['paintID']]=$row;
    //$csv.="\r\n";
}
     */

    $csv.="".UTILS::prepare_for_csv($db,$row['paintID']).";";
    $csv.="".UTILS::prepare_for_csv($db,UTILS::get_artwork_name($db,$row['artworkID'])).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['year']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['date']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['number']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleEN']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleDE']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleFR']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleIT']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleZH']).";";


    /*
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesFR'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";
     */
    //$csv.="<br />\r\n";
    $csv.="\r\n";

    }
}

/*
foreach( $paintings_array as $row2 )
{

    $query_where_painting=" WHERE ( p.artworkID=5 OR p.artworkID=7 OR p.artworkID=8 ) AND p.titleDE=\"".$row2['titleDE']."\" AND paintID!='".$row2['paintID']."' ";
    $query_order=" ORDER BY p.sort2 ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results2=$db->query($query_painting['query']);
$similar="";
while( $row=$db->mysql_array($results2) )
{
    $similar.=$row['paintID'].",";
}

            $artwork=UTILS::get_artwork_name($db,$row2['artworkID']);
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['paintID'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$artwork)."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleEN'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleDE'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['number'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['year'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$similar)."\"|";
    $csv.="\r\n";
}
 */
$html_print.=$csv;
#END PAINTINGS

//$html_print.=count($paintings_array);
//print_r($paintings_array);


$html->foot();
?>
