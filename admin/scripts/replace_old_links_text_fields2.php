<?php
header("Content-Type: text/html; charset=utf-8");
//header("Content-Type: text/plain");
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//$html = new html_elements;
//$html->head('artwork');

$db=new dbCLASS;

exit;

function replace_old_links($db,$text)
{
    if( !empty($text) )
    {
        $text=str_replace("&amp;","&",$text);
        $text=str_replace("../../..","",$text);
        $text=str_replace("http://www.gerhard-richter.com/","/",$text);

                //$pattern = '/(\/en\/)|(\/en$)/i';
                //$replacement = "/".$current_lang."/";
                //$link=preg_replace($pattern, $replacement, $link);

        $pattern='/href=["\']?([^"\'>]+)["\']?/';
        //$pattern="/<a href\s*=\s*\"(.+)\">/";
        $url = preg_match_all($pattern, $text, $match);
        $match[1] = array_unique($match[1]);

        //print_r($match);

        if( is_array($match[1]) )
        {
            foreach ($match[1] as $link) 
            {

                # /art
                if( preg_match("/\/art\//i", $link) )
                {
                    print "<strong>old link</strong> = ".$link."<br />";

                    $info_link = parse_url($link);

                    parse_str($info_link['query'], $link_params);

                    $url=array();
                    $link_change=0;
                    if( !empty($link_params) )
                    {
                        $i=0;
                        foreach ($link_params as $key => $value) 
                        {
                            $i++;
                            $paintid="";
                            $categoryid="";
                            $url=array();
                            $link_change=0;
                            if( is_numeric($key) )
                            {
                                $paintid=$key;
                            }
                            elseif ( $key=="paintid" ) 
                            {
                                $paintid=$value;
                            }
                            elseif ( $key=="catID" ) 
                            {
                                $categoryid=$value;
                            }

                            # art painting
                            if( !empty($paintid) )
                            {
                                $options_painting_url=array();
                                $options_painting_url['paintid']=$paintid;
                                $options_painting_url['lang']="en";
                                $url=UTILS::get_painting_url($db,$options_painting_url);
                                $link_change=1;
                                $section="paintng";
                            }
                            # art category links
                            elseif( !empty($categoryid) )
                            {
                                $url['url']="/en/art/paintings";

                                $query_where_category=" WHERE catID='".$categoryid."' AND enable=1 ";
                                $query_limit_category=" LIMIT 1 ";
                                $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                                $results_category=$db->query($query_category['query']);
                                $count_category=$db->numrows($results_category);
                                if( $count_category>0 )
                                {   
                                    $row_category=$db->mysql_array($results_category);
                            
                                    $query_artwork="SELECT artworkID,titleurl FROM ".TABLE_ARTWORKS." WHERE artworkID='".$row_category['artworkID']."' LIMIT 1 ";
                                    //print $query_artwork;
                                    $results_artwork=$db->query($query_artwork);
                                    $count_artwork=$db->numrows($results_artwork);
                                    if( $count_artwork )
                                    {
                                        $row_artwork=$db->mysql_array($results_artwork,0);
                                        $url['url'].="/".$row_artwork['titleurl'];
                                    }

                                    if( !empty($row_category['sub_catID']) )
                                    {   
                                        $query_where_category=" WHERE catID='".$row_category['sub_catID']."' AND artworkID='".$row_category['artworkID']."' AND enable=1 ";
                                        $query_limit_category=" LIMIT 1 ";
                                        $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                                        $results_sub_category=$db->query($query_category['query']);
                                        $count_sub_category=$db->numrows($results_sub_category);
                                        if( $count_sub_category>0 )
                                        {   
                                            $row_sub_category=$db->mysql_array($results_sub_category);
                                        }   
                                    }   
                                    if( !empty($row_sub_category['titleurl']) ) $url['url'].="/".$row_sub_category['titleurl'];
                                    if( !empty($row_category['titleurl']) ) $url['url'].="/".$row_category['titleurl'];
                                    $link_change=1;
                                    $section="category";
                                }
                            }
                            # all other art links
                            elseif( $i==1 )
                            {
                                # adding lang /en before link
                                $url['url']=str_replace("/art/","/en/art/",$link);
                                $link_change=1;
                                $section="other art section";
                            }

                            if( $link_change )
                            {
                                print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                                $text=str_replace($link,$url['url'],$text);
                            }
                            break;
                        }
                    }
                    else
                    {
                        $url['url']=str_replace("/art/","/en/art/",$link);
                        $link_change=1;
                        $section="new art section";      
                        print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                        $text=str_replace($link,$url['url'],$text);      
                    }
                }
                # END /art
                # /literature
                elseif( preg_match("/\/literature\/\?/i", $link) )
                {
                    $url=array();
                    print "<strong>old book seacrh link</strong> = ".$link."<br />";

                    $url['url']=str_replace("/literature/?","/en/literature/search/?",$link);

                    print "<strong style='color:green;'>book search</strong> = ".$url['url']."<br />";
                    $text=str_replace($link,$url['url'],$text);
                }
                elseif( preg_match("/\/literature\//i", $link) )
                {
                    print "<strong>old link literature</strong> = ".$link."<br />";

                    $info_link = parse_url($link);

                    parse_str($info_link['query'], $link_params);

                    $url=array();
                    $link_change=0;
                    if( !empty($link_params) )
                    {
                        $i=0;
                        foreach ($link_params as $key => $value) 
                        {
                            $i++;
                            $bookid="";
                            $url=array();
                            $link_change=0;
                            if( is_numeric($key) )
                            {
                                $bookid=$key;
                            }

                            # literature book
                            if( !empty($bookid) )
                            {
                                $options_book_url=array();
                                $options_book_url['bookid']=$bookid;
                                $options_book_url['lang']="en";
                                $url=UTILS::get_literature_url($db,$options_book_url);
                                $link_change=1;
                                $section="book";
                            }
                            # all other literature links
                            elseif( $i==1 )
                            {
                                # adding lang /en before link
                                $url['url']=str_replace("/literature/","/en/literature/",$link);
                                $link_change=1;
                                $section="other literature section";
                            }

                            if( $link_change )
                            {
                                print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                                $text=str_replace($link,$url['url'],$text);
                            }

                        }
                    }
                    else
                    {
                        $url['url']=str_replace("/literature/","/en/literature/",$link);
                        $link_change=1;
                        $section="new literature section";      
                        print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                        $text=str_replace($link,$url['url'],$text); 
                    }
                }
                # END /literature
                
            }

        }

    }

    //print $text;

    return $text;
}

# PAINTINGS
/*
$query="SELECT paintID,number,titleEN,titleDE,artwork_notesEN,artwork_notesDE,artwork_notesFR,artwork_notesIT,artwork_notesZH FROM ".TABLE_PAINTING." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleDE']."-".$row['titleEN']."-".$row['number']."<br />";

    $row['artwork_notesEN']=replace_old_links($db,$row['artwork_notesEN']);
    $row['artwork_notesDE']=replace_old_links($db,$row['artwork_notesDE']);
    $row['artwork_notesFR']=replace_old_links($db,$row['artwork_notesFR']);
    $row['artwork_notesIT']=replace_old_links($db,$row['artwork_notesIT']);
    $row['artwork_notesZH']=replace_old_links($db,$row['artwork_notesZH']);


    $row['artwork_notesEN']=$db->db_prepare_input($row['artwork_notesEN'],1);
    $row['artwork_notesDE']=$db->db_prepare_input($row['artwork_notesDE'],1);
    $row['artwork_notesFR']=$db->db_prepare_input($row['artwork_notesFR'],1);
    $row['artwork_notesIT']=$db->db_prepare_input($row['artwork_notesIT'],1);
    $row['artwork_notesZH']=$db->db_prepare_input($row['artwork_notesZH'],1);


    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="artwork_notesEN='".$row['artwork_notesEN']."', ";
        $query_update.="artwork_notesDE='".$row['artwork_notesDE']."', ";
        $query_update.="artwork_notesFR='".$row['artwork_notesFR']."', ";
        $query_update.="artwork_notesIT='".$row['artwork_notesIT']."', ";
        $query_update.="artwork_notesZH='".$row['artwork_notesZH']."' ";
    $query_update.="WHERE paintID='".$row['paintID']."' ";

    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# BIOGRAPHY
/*
$query="SELECT * FROM ".TABLE_BIOGRAPHY." ";
//$query.=" WHERE biographyid=9 ";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/biography/edit/?biographyid=".$row['biographyid']."' target='_blank'>".$row['biographyid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    //$row['text_en']=replace_old_links($db,$row['text_en']);
    //$row['text_de']=replace_old_links($db,$row['text_de']);
    //$row['text_fr']=replace_old_links($db,$row['text_fr']);
    //$row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_zh']=replace_old_links($db,$row['text_zh']);


    //$row['text_en']=$db->db_prepare_input($row['text_en'],1);
    //$row['text_de']=$db->db_prepare_input($row['text_de'],1);
    //$row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    //$row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_zh']=$db->db_prepare_input($row['text_zh'],1);

    $query_update="UPDATE ".TABLE_BIOGRAPHY." SET ";
        //$query_update.="text_en='".$row['text_en']."', ";
        //$query_update.="text_de='".$row['text_de']."', ";
        //$query_update.="text_fr='".$row['text_fr']."', ";
        //$query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_zh='".$row['text_zh']."' ";
    $query_update.="WHERE biographyid='".$row['biographyid']."' ";

    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    $db->query($query_update);
}
*/

# TEXT
/*
$query="SELECT * FROM ".TABLE_TEXT." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/pages/edit/?textid=".$row['textid']."' target='_blank'>".$row['textid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    $row['text_en']=replace_old_links($db,$row['text_en']);
    $row['text_de']=replace_old_links($db,$row['text_de']);
    $row['text_fr']=replace_old_links($db,$row['text_fr']);
    $row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_zh']=replace_old_links($db,$row['text_zh']);


    $row['text_en']=$db->db_prepare_input($row['text_en'],1);
    $row['text_de']=$db->db_prepare_input($row['text_de'],1);
    $row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    $row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_zh']=$db->db_prepare_input($row['text_zh'],1);

    $query_update="UPDATE ".TABLE_TEXT." SET ";
        $query_update.="text_en='".$row['text_en']."', ";
        $query_update.="text_de='".$row['text_de']."', ";
        $query_update.="text_fr='".$row['text_fr']."', ";
        $query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_zh='".$row['text_zh']."' ";
    $query_update.="WHERE textid='".$row['textid']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# QUOTES
/*
$query="SELECT * FROM ".TABLE_QUOTES." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/quotes/edit/?quoteid=".$row['quoteid']."' target='_blank'>".$row['quoteid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    $row['text_en']=replace_old_links($db,$row['text_en']);
    $row['text_de']=replace_old_links($db,$row['text_de']);
    $row['text_fr']=replace_old_links($db,$row['text_fr']);
    $row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_zh']=replace_old_links($db,$row['text_zh']);


    $row['text_en']=$db->db_prepare_input($row['text_en'],1);
    $row['text_de']=$db->db_prepare_input($row['text_de'],1);
    $row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    $row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_zh']=$db->db_prepare_input($row['text_zh'],1);

    $query_update="UPDATE ".TABLE_QUOTES." SET ";
        $query_update.="text_en='".$row['text_en']."', ";
        $query_update.="text_de='".$row['text_de']."', ";
        $query_update.="text_fr='".$row['text_fr']."', ";
        $query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_zh='".$row['text_zh']."' ";
    $query_update.="WHERE quoteid='".$row['quoteid']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/


# EXHIBITIONS
/*
$query="SELECT * FROM ".TABLE_EXHIBITIONS." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>".$row['exID']."</a>-".$row['title_original']."<br />";

    $row['descriptionEN']=replace_old_links($db,$row['descriptionEN']);
    $row['descriptionDE']=replace_old_links($db,$row['descriptionDE']);
    $row['descriptionFR']=replace_old_links($db,$row['descriptionFR']);
    $row['descriptionIT']=replace_old_links($db,$row['descriptionIT']);
    $row['descriptionZH']=replace_old_links($db,$row['descriptionZH']);


    $row['descriptionEN']=$db->db_prepare_input($row['descriptionEN'],1);
    $row['descriptionDE']=$db->db_prepare_input($row['descriptionDE'],1);
    $row['descriptionFR']=$db->db_prepare_input($row['descriptionFR'],1);
    $row['descriptionIT']=$db->db_prepare_input($row['descriptionIT'],1);
    $row['descriptionZH']=$db->db_prepare_input($row['descriptionZH'],1);

    $query_update="UPDATE ".TABLE_EXHIBITIONS." SET ";
        $query_update.="descriptionEN='".$row['descriptionEN']."', ";
        $query_update.="descriptionDE='".$row['descriptionDE']."', ";
        $query_update.="descriptionFR='".$row['descriptionFR']."', ";
        $query_update.="descriptionIT='".$row['descriptionIT']."', ";
        $query_update.="descriptionZH='".$row['descriptionZH']."' ";
    $query_update.="WHERE exID='".$row['exID']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# LITERATURE
/*
$query="SELECT * FROM ".TABLE_BOOKS." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/books/edit/?bookid=".$row['id']."' target='_blank'>".$row['id']."</a>-".$row['title']."<br />";

    $row['notesEN']=replace_old_links($db,$row['notesEN']);
    $row['notesDE']=replace_old_links($db,$row['notesDE']);
    $row['notesFR']=replace_old_links($db,$row['notesFR']);
    $row['notesIT']=replace_old_links($db,$row['notesIT']);
    $row['notesZH']=replace_old_links($db,$row['notesZH']);
    $row['notesEN']=$db->db_prepare_input($row['notesEN'],1);
    $row['notesDE']=$db->db_prepare_input($row['notesDE'],1);
    $row['notesFR']=$db->db_prepare_input($row['notesFR'],1);
    $row['notesIT']=$db->db_prepare_input($row['notesIT'],1);
    $row['notesZH']=$db->db_prepare_input($row['notesZH'],1);

    $row['infoEN']=replace_old_links($db,$row['infoEN']);
    $row['infoDE']=replace_old_links($db,$row['infoDE']);
    $row['infoFR']=replace_old_links($db,$row['infoFR']);
    $row['infoIT']=replace_old_links($db,$row['infoIT']);
    $row['infoZH']=replace_old_links($db,$row['infoZH']);
    $row['infoEN']=$db->db_prepare_input($row['infoEN'],1);
    $row['infoDE']=$db->db_prepare_input($row['infoDE'],1);
    $row['infoFR']=$db->db_prepare_input($row['infoFR'],1);
    $row['infoIT']=$db->db_prepare_input($row['infoIT'],1);
    $row['infoZH']=$db->db_prepare_input($row['infoZH'],1);

    $row['infosmall_en']=replace_old_links($db,$row['infosmall_en']);
    $row['infosmall_de']=replace_old_links($db,$row['infosmall_de']);
    $row['infosmall_fr']=replace_old_links($db,$row['infosmall_fr']);
    $row['infosmall_it']=replace_old_links($db,$row['infosmall_it']);
    $row['infosmall_zh']=replace_old_links($db,$row['infosmall_zh']);
    $row['infosmall_en']=$db->db_prepare_input($row['infosmall_en'],1);
    $row['infosmall_de']=$db->db_prepare_input($row['infosmall_de'],1);
    $row['infosmall_fr']=$db->db_prepare_input($row['infosmall_fr'],1);
    $row['infosmall_it']=$db->db_prepare_input($row['infosmall_it'],1);
    $row['infosmall_zh']=$db->db_prepare_input($row['infosmall_zh'],1);

    $query_update="UPDATE ".TABLE_BOOKS." SET ";
        $query_update.="notesEN='".$row['notesEN']."', ";
        $query_update.="notesDE='".$row['notesDE']."', ";
        $query_update.="notesFR='".$row['notesFR']."', ";
        $query_update.="notesIT='".$row['notesIT']."', ";
        $query_update.="notesZH='".$row['notesZH']."', ";

        $query_update.="infoEN='".$row['infoEN']."', ";
        $query_update.="infoDE='".$row['infoDE']."', ";
        $query_update.="infoFR='".$row['infoFR']."', ";
        $query_update.="infoIT='".$row['infoIT']."', ";
        $query_update.="infoZH='".$row['infoZH']."', ";

        $query_update.="infosmall_en='".$row['infosmall_en']."', ";
        $query_update.="infosmall_de='".$row['infosmall_de']."', ";
        $query_update.="infosmall_fr='".$row['infosmall_fr']."', ";
        $query_update.="infosmall_it='".$row['infosmall_it']."', ";
        $query_update.="infosmall_zh='".$row['infosmall_zh']."' ";

    $query_update.="WHERE id='".$row['id']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# VIDEOS
/*
$query="SELECT * FROM ".TABLE_VIDEO." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/videos/edit/?videoid=".$row['videoID']."' target='_blank'>".$row['videoID']."</a>-".$row['titleEN']."-".$row['title_en']."<br />";

    $row['info_en']=replace_old_links($db,$row['info_en']);
    $row['info_de']=replace_old_links($db,$row['info_de']);
    $row['info_fr']=replace_old_links($db,$row['info_fr']);
    $row['info_it']=replace_old_links($db,$row['info_it']);
    $row['info_zh']=replace_old_links($db,$row['info_zh']);


    $row['info_en']=$db->db_prepare_input($row['info_en'],1);
    $row['info_de']=$db->db_prepare_input($row['info_de'],1);
    $row['info_fr']=$db->db_prepare_input($row['info_fr'],1);
    $row['info_it']=$db->db_prepare_input($row['info_it'],1);
    $row['info_zh']=$db->db_prepare_input($row['info_zh'],1);

    $query_update="UPDATE ".TABLE_VIDEO." SET ";
        $query_update.="info_en='".$row['info_en']."', ";
        $query_update.="info_de='".$row['info_de']."', ";
        $query_update.="info_fr='".$row['info_fr']."', ";
        $query_update.="info_it='".$row['info_it']."', ";
        $query_update.="info_zh='".$row['info_zh']."' ";
    $query_update.="WHERE videoID='".$row['videoID']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/



//$html->foot();
?>
