<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;

$html_print="";

$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            $html_print.="<th>PaintID</th>";
            $html_print.="<th>sale id</th>";
            $html_print.="<th>sale date</th>";
            $html_print.="<th>auction house</th>";
            $html_print.="<th>sale name</th>";
            $html_print.="<th>estLow currency 1</th>";
            $html_print.="<th>estHigh currency 1</th>";
            $html_print.="<th>estLow currency 2</th>";
            $html_print.="<th>estHigh currency 2</th>";
            $html_print.="<th>sold for currency 1</th>";
            $html_print.="<th>sold for currency 2</th>";
            $html_print.="<th>notes EN</th>";
            $html_print.="<th>notes DE</th>";
        $html_print.="</tr>";
    $html_print.="</thead>";

    $query_export=" SELECT ";

    $query_export.=" p.paintID AS ppaintID ";

    //$query_export.=" p.paintID AS ppaintID, p.artworkID AS partworkID,p.titleurl AS ptitleurl, p.number AS pnumber, p.titleEN AS ptitleEN, p.titleDE AS ptitleDE, p.titleFR AS ptitleFR, p.titleIT AS ptitleIT, p.titleZH AS ptitleZH,m.mID AS mmID,m.mediumEN AS mmedium,p.width AS pwidth,p.height AS pheight,p.notes AS pnotes ";


    $query_export.=" FROM ".TABLE_PAINTING." p ";

    //$query_export.=" LEFT JOIN ".TABLE_PAINTING_OPP." po ON p.paintID=po.paintid ";
    //$query_export.=" LEFT JOIN ".TABLE_MEDIA." m ON p.mID=m.mID ";
    //$query_export.=" WHERE p.artworkID=6 OR ( p.artworkID=4 AND ( p.number_search LIKE '72%' OR p.number_search LIKE '78%' OR p.number_search LIKE '101%' OR p.number_search LIKE '110%' OR p.number_search LIKE '122%' OR p.number_search LIKE '136%' ) ) ";
    //$query_export.=" WHERE p.paintID='".$row_exh_painting_relation['paintID']."'  ";
    $query_export.=" WHERE p.artworkID=6  ";#opp
    //$query_export.=" WHERE p.artworkID=8  ";#oil on papper
    //$query_export.=" WHERE p.artworkID=5  ";# watercolor
    //$query_export.=" AND p.paintID=14699  ";

    $query_export.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    //$query_export.=" LIMIT 0,10 ";

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    while( $row=$db->mysql_array($results) )
    {  











    # exhibitions
        $query_sale_hist=QUERIES::query_sale_history($db);
        $results_sale_hist=$db->query("SELECT ".$query_sale_hist['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_SALEHISTORY." s WHERE ( ( r.typeid1=1 AND r.itemid1='".$row['ppaintID']."' AND r.typeid2=2 ) OR ( r.typeid2=1 AND r.itemid2='".$row['ppaintID']."' AND r.typeid1=2 ) ) AND ( ( r.itemid2=s.saleID AND r.typeid2=2 ) OR ( r.itemid1=s.saleID AND r.typeid1=2 ) ) AND s.enable=1 ORDER BY s.saleDate DESC ");

/*
    $query_where_exhibitions="";
    $query_order_exhibitions=" ORDER BY start_year_admin DESC, typeid DESC, start_month_admin DESC, start_day_admin DESC ";
    //$query_limit=" LIMIT 100 ";
    $query_limit_exhibitions=" ";
    $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions,$query_order_exhibitions,$query_limit_exhibitions);

    
    $results_exh=$db->query($query_exhibitions['query']);
    */
    $count_exh=$db->numrows($results_sale_hist);
    while( $row_exh=$db->mysql_array($results_sale_hist) )
    {
        $row_exh=UTILS::html_decode($row_exh);

                $results_auction_house=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row_exh['ahID']."'");
                $row_auction_house=$db->mysql_array($results_auction_house);
                $row_auction_house=UTILS::html_decode($row_auction_house);
        //print_r($row_exh);

        //$query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC LIMIT 1 ";
        //$results_rel=$db->query($query);
        //$count_exh_paint=$db->numrows($results_rel);

        //if( !$count_exh_paint )
        //{
            $html_print.="<tr>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['ppaintID']."' title='' target='blank' >\n";
                        $html_print.=$row['ppaintID'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/auctions-sales/saleHistory/edit/?saleid=".$row_exh['saleID']."' title='' target='blank' >\n";
                        $html_print.=$row_exh['saleID'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['sale_date'];
                $html_print.="</td>";


                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_auction_house['house'];
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['saleName'];
                $html_print.="</td>";

                    #estimate
                    $currency1=UTILS::get_currency($db,$row_exh['estCurrID']);
                    $html_print.="\t<td style='".$td_style."'>".$currency1.number_format($row_exh['estLow'],0)."</td>\n";
                    $html_print.="\t<td style='".$td_style."'>".$currency1.number_format($row_exh['estHigh'],0)."</td>\n";
                    $currency2=UTILS::get_currency($db,1);
                    $html_print.="\t<td style='".$td_style."'>".$currency2.number_format($row_exh['estLowUSD'],0)."</td>\n";
                    $html_print.="\t<td style='".$td_style."'>".$currency2.number_format($row_exh['estHighUSD'],0)."</td>\n";
                    # end estimate

                    #sold for
                    if( !empty($row_exh['soldFor']) ) $sold_for1=UTILS::get_currency($db,$row_exh['soldForCurrID']).number_format($row_exh['soldFor'],0);
                    else $sold_for1="";
                    $html_print.="\t<td style='".$td_style."'>".$sold_for1."</td>\n";
                    if( !empty($row_exh['soldForUSD']) ) $sold_for2=UTILS::get_currency($db,1).number_format($row_exh['soldForUSD'],0);
                    else $sold_for2="";
                    $html_print.="\t<td style='".$td_style."'>".$sold_for2."</td>\n";
                    # end sold for

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['notes_en'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['notes_de'];
                $html_print.="</td>";

            $html_print.="</tr>";
        //}


    }









    }

$html_print.="</table>";

print $html_print;

exit;


#EXHIBITIONS
/*
//$csv="exID;title_original;titleEN;titleDE;titleCH;descriptionEN;descriptionDE;descriptionCH;type;notes;locationEN;locationDE;locationCH;startDate;endDate\r\n";
$csv="exID|title_original|titleEN|titleFR|descriptionEN|descriptionFR|locationEN|locationFR\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_EXHIBITIONS." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {   
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['exID'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_original'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\"|";
        if( $row['descriptionEN']=="<br>" ) $row['descriptionEN']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\"|";
        //if( $row['descriptionDE']=="<br>" ) $row['descriptionDE']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\"|";
        if( $row['descriptionFR']=="<br>" ) $row['descriptionFR']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionFR'])."\"|";
        //if( $row['descriptionCH']=="<br>" ) $row['descriptionCH']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['type'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['startDate2'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['endDate2'])."\"|";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */
#EXHIBITIONS PAINTINGS
/*
$csv="paintID;exID;artworkID;editionID;titleEN;titleDE;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";

    $results_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." ");
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC ";
        $results_rel=$db->query($query);
        while( $row_rel=$db->mysql_array($results_rel) )
{
$paintid=UTILS::get_relation_id($db,"1",$row_rel);
$results=$db->query("SELECT 
                paintID,
                artworkID,
                editionID,
                titleEN,
                titleDE,
                titleCH,
                year,
                number,
                ednumber,
                ednumberroman,
                ednumberap,
                citylifepage,
                nr_of_editions,
                catID,
                size,
                height,
                width,
                src,
                mID,
                muID,
                sort,
                artwork_notesEN,
                artwork_notesDE,
                artwork_notesCH,
                notes,
                image_zoomer,
                created,
                modified,
                enable
            FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");

$row=$db->mysql_array($results);
    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    $csv.="\"".UTILS::prepare_for_csv($db,$row['paintID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row_exh['exID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artworkID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['editionID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";

    //$csv.="<br />\r\n";
    $csv.="\r\n";
   
}
    }
$html_print.=$csv;
*/




$html_print.=$csv;
#END PAINTINGS

//$html_print.=count($paintings_array);
//print_r($paintings_array);


$html->foot();
?>
