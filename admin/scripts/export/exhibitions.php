<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;

$html_print="";

$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            $html_print.="<th>PaintID</th>";
            $html_print.="<th>Artwork</th>";
            $html_print.="<th>exhibitionid</th>";
            $html_print.="<th>Title original</th>";
            $html_print.="<th>Location</th>";
            $html_print.="<th>Date</th>";
            $html_print.="<th>Type</th>";

        $html_print.="</tr>";
    $html_print.="</thead>";

    $query_export=" SELECT ";

    $query_export.=" p.paintID AS ppaintID, p.artworkID as partworkID ";

    //$query_export.=" p.paintID AS ppaintID, p.artworkID AS partworkID,p.titleurl AS ptitleurl, p.number AS pnumber, p.titleEN AS ptitleEN, p.titleDE AS ptitleDE, p.titleFR AS ptitleFR, p.titleIT AS ptitleIT, p.titleZH AS ptitleZH,m.mID AS mmID,m.mediumEN AS mmedium,p.width AS pwidth,p.height AS pheight,p.notes AS pnotes ";


    $query_export.=" FROM ".TABLE_PAINTING." p ";

    //$query_export.=" LEFT JOIN ".TABLE_PAINTING_OPP." po ON p.paintID=po.paintid ";
    //$query_export.=" LEFT JOIN ".TABLE_MEDIA." m ON p.mID=m.mID ";
    //$query_export.=" WHERE p.artworkID=6 OR ( p.artworkID=4 AND ( p.number_search LIKE '72%' OR p.number_search LIKE '78%' OR p.number_search LIKE '101%' OR p.number_search LIKE '110%' OR p.number_search LIKE '122%' OR p.number_search LIKE '136%' ) ) ";
    //$query_export.=" WHERE p.paintID='".$row_exh_painting_relation['paintID']."'  ";
    $query_export.=" WHERE p.artworkID=6 ";#opp
    $query_export.=" OR ( p.artworkID=4 && ( p.number=66 OR p.number=67 OR p.number=72 OR p.number=78 OR p.number=122 OR p.number=132 ) ) ";#editions
    //$query_export.=" WHERE p.artworkID=8  ";#oil on papper
    //$query_export.=" WHERE p.artworkID=5  ";# watercolor
    //$query_export.=" AND p.paintID=14699  ";

    $query_export.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    //$query_export.=" LIMIT 0,10 ";

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    while( $row=$db->mysql_array($results) )
    {  











    # exhibitions
    $query_exh=QUERIES::query_exhibition($db);
    $results_exh=$db->query("SELECT ".$query_exh['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1=1 AND r.itemid1='".$row['ppaintID']."' AND r.typeid2=4 ) OR ( r.typeid2=1 AND r.itemid2='".$row['ppaintID']."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND e.enable=1 ORDER BY e.date_from DESC ");

/*
    $query_where_exhibitions="";
    $query_order_exhibitions=" ORDER BY start_year_admin DESC, typeid DESC, start_month_admin DESC, start_day_admin DESC ";
    //$query_limit=" LIMIT 100 ";
    $query_limit_exhibitions=" ";
    $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions,$query_order_exhibitions,$query_limit_exhibitions);

    
    $results_exh=$db->query($query_exhibitions['query']);
    */
    $count_exh=$db->numrows($results_exh);
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $row_exh=UTILS::html_decode($row_exh);
        //print_r($row_exh);

        //$query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC LIMIT 1 ";
        //$results_rel=$db->query($query);
        //$count_exh_paint=$db->numrows($results_rel);

        //if( !$count_exh_paint )
        //{
            $html_print.="<tr>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['ppaintID']."' title='' target='blank' >\n";
                        $html_print.=$row['ppaintID'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.=UTILS::get_artwork_name($db,$row['partworkID']);
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/exhibitions/edit/?exhibitionid=".$row_exh['exID']."' title='' target='blank' >\n";
                        $html_print.=$row_exh['exID'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['title_original'];
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/locations/edit/?typeid=1&locationid=".$row_exh['locationid']."' title='' target='blank' >\n";
                        $values_location=array();
                        $values_location['row']=$row_exh;
                        $location=location($db,$values_location);
                        $html_print.=$location;
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $date=exhibition_date($db,$row_exh);
                    //print $row_exh['start_month'];
                    $html_print.=$date;
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    if( $row_exh['typeid']==1 ) $type="Solo";
                    elseif( $row_exh['typeid']==2 ) $type="Group";
                    $html_print.=$type;
                $html_print.="</td>";
            $html_print.="</tr>";
        //}


    }









    }

$html_print.="</table>";

print $html_print;

exit;


#EXHIBITIONS
/*
//$csv="exID;title_original;titleEN;titleDE;titleCH;descriptionEN;descriptionDE;descriptionCH;type;notes;locationEN;locationDE;locationCH;startDate;endDate\r\n";
$csv="exID|title_original|titleEN|titleFR|descriptionEN|descriptionFR|locationEN|locationFR\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_EXHIBITIONS." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {   
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['exID'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_original'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\"|";
        if( $row['descriptionEN']=="<br>" ) $row['descriptionEN']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\"|";
        //if( $row['descriptionDE']=="<br>" ) $row['descriptionDE']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\"|";
        if( $row['descriptionFR']=="<br>" ) $row['descriptionFR']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionFR'])."\"|";
        //if( $row['descriptionCH']=="<br>" ) $row['descriptionCH']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['type'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['startDate2'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['endDate2'])."\"|";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */
#EXHIBITIONS PAINTINGS
/*
$csv="paintID;exID;artworkID;editionID;titleEN;titleDE;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";

    $results_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." ");
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC ";
        $results_rel=$db->query($query);
        while( $row_rel=$db->mysql_array($results_rel) )
{
$paintid=UTILS::get_relation_id($db,"1",$row_rel);
$results=$db->query("SELECT 
                paintID,
                artworkID,
                editionID,
                titleEN,
                titleDE,
                titleCH,
                year,
                number,
                ednumber,
                ednumberroman,
                ednumberap,
                citylifepage,
                nr_of_editions,
                catID,
                size,
                height,
                width,
                src,
                mID,
                muID,
                sort,
                artwork_notesEN,
                artwork_notesDE,
                artwork_notesCH,
                notes,
                image_zoomer,
                created,
                modified,
                enable
            FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");

$row=$db->mysql_array($results);
    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    $csv.="\"".UTILS::prepare_for_csv($db,$row['paintID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row_exh['exID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artworkID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['editionID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";

    //$csv.="<br />\r\n";
    $csv.="\r\n";
   
}
    }
$html_print.=$csv;
*/




$html_print.=$csv;
#END PAINTINGS

//$html_print.=count($paintings_array);
//print_r($paintings_array);


$html->foot();
?>
