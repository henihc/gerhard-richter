<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;

$html_print="";

$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            //$html_print.="<th>Id</th>";
            //$html_print.="<th>Image</th>";
            $html_print.="<th>Imageid</th>";
            $html_print.="<th>Image src old</th>";
            $html_print.="<th>Artwork</th>";
            $html_print.="<th>PaintID</th>";
            $html_print.="<th>Url</th>";
            $html_print.="<th>Cr number</th>";
            $html_print.="<th>TitleEN</th>";
            $html_print.="<th>TitleDE</th>";
            $html_print.="<th>TitleFR</th>";
            $html_print.="<th>TitleIT</th>";
            $html_print.="<th>TitleZH</th>";
            $html_print.="<th>Medium</th>";
            $html_print.="<th>Location</th>";
            $html_print.="<th>Width</th>";
            $html_print.="<th>Height</th>";
            $html_print.="<th>Dim. Width</th>";
            $html_print.="<th>Dim. Height</th>";

            # opp additional
            $html_print.="<th>recto_signed_1</th>";
            $html_print.="<th>recto_signed_2</th>";
            $html_print.="<th>recto_signed_3</th>";
            $html_print.="<th>recto_dated_1</th>";
            $html_print.="<th>recto_dated_2</th>";
            $html_print.="<th>recto_dated_3</th>";
            $html_print.="<th>recto_numbered_1</th>";
            $html_print.="<th>recto_numbered_2</th>";
            $html_print.="<th>recto_numbered_3</th>";
            $html_print.="<th>recto_inscribed_1</th>";
            $html_print.="<th>recto_inscribed_2</th>";
            $html_print.="<th>recto_inscribed_3</th>";
            $html_print.="<th>verso_signed_1</th>";
            $html_print.="<th>verso_signed_2</th>";
            $html_print.="<th>verso_signed_3</th>";
            $html_print.="<th>verso_dated_1</th>";
            $html_print.="<th>verso_dated_2</th>";
            $html_print.="<th>verso_dated_3</th>";
            $html_print.="<th>verso_numbered_1</th>";
            $html_print.="<th>verso_numbered_2</th>";
            $html_print.="<th>verso_numbered_3</th>";
            $html_print.="<th>verso_inscribed_1</th>";
            $html_print.="<th>verso_inscribed_2</th>";
            $html_print.="<th>verso_inscribed_3</th>";
            $html_print.="<th>inscriptions</th>";
            $html_print.="<th>notations</th>";
            $html_print.="<th>city</th>";
            $html_print.="<th>country</th>";
            $html_print.="<th>collection</th>";
            $html_print.="<th>private collection</th>";
            $html_print.="<th>copyright / photographer</th>";
            $html_print.="<th>source</th>";
            $html_print.="<th>licence</th>";

            $html_print.="<th>Admin notes</th>";
        $html_print.="</tr>";
    $html_print.="</thead>";

exit;


    $query_exh_paint="SELECT * FROM relations r, painting p WHERE ( ( r.typeid1=4 AND r.itemid1='571' AND r.typeid2=1 ) OR ( r.typeid2=4 AND r.itemid2='571' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) AND p.enable=1 ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC";
    $results_exh_paint=$db->query($query_exh_paint);
    $count_exh_paint=$db->numrows($results_exh_paint);

    //print  $query_exh_paint."-".$count_exh_paint."<br />";

//exit;



while( $row_exh_paint=$db->mysql_array($results_exh_paint) )
{


    $query_export=" SELECT ";

    $query_export.=" p.paintID AS ppaintID, p.artworkID AS partworkID,p.titleurl AS ptitleurl, p.number AS pnumber, p.titleEN AS ptitleEN, p.titleDE AS ptitleDE,p.locationid,p.cityid,p.countryid,p.loan_locationid,p.loan_cityid,p.loan_countryid,m.mID AS mmID,m.mediumEN AS mmedium,p.width AS pwidth,p.height AS pheight,p.notes AS pnotes


        ";



    $query_export.=" FROM ".TABLE_PAINTING." p ";

    $query_export.=" LEFT JOIN ".TABLE_MEDIA." m ON p.mID=m.mID ";
    $query_export.=" WHERE p.artworkID=6  ";#opp
    $query_export.=" AND paintID='".$row_exh_paint['paintID']."'  ";
    //$query_export.=" OR ( p.artworkID=4 && ( p.number=66 OR p.number=67 OR p.number=72 OR p.number=78 OR p.number=122 OR p.number=132 ) ) ";#editions
    //$query_export.=" WHERE p.artworkID=8  ";#oil on papper
    //$query_export.=" WHERE p.artworkID=5  ";# watercolor

    $query_export.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    //$query_export.=" LIMIT 0,10 ";

    //print $query_export;
    //exit;

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    //print $count."<br />";





    //$i=0;
    while( $row=$db->mysql_array($results) )
    {
        //$i++;
        $row=UTILS::html_decode($row);
        $query_related_images="SELECT
            COALESCE(
                r1.itemid1,
                r2.itemid2
            ) as itemid,
            COALESCE(
                r1.sort,
                r2.sort
            ) as sort_rel,
            COALESCE(
                r1.relationid,
                r2.relationid
            ) as relationid_rel
            FROM
                ".TABLE_PAINTING." p
            LEFT JOIN
                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
            LEFT JOIN
                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
            WHERE p.paintID='".$row['ppaintID']."'
            ORDER BY sort_rel ASC, relationid_rel DESC
            LIMIT 1
        ";
        $results_related_image=$db->query($query_related_images);
        $row_related_image=$db->mysql_array($results_related_image);

        $query_where_image=" WHERE imageid='".$row_related_image['itemid']."' ";
        $query_image=QUERIES::query_images($db,$query_where_image,$query_order,$query_limit);
        $results_image=$db->query($query_image['query']);
        $count_image=$db->numrows($results_image);
        $row_image=$db->mysql_array($results_image);

        $options_painting_url=array();
        $options_painting_url['paintid']=$row['ppaintID'];
        $painting_url=UTILS::get_painting_url($db,$options_painting_url);

        if( empty($row_related_image['itemid']) ) $td_style="background:#ff9c9c;";
        else $td_style="";

        $html_print.="<tr>";
        /*
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$i;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                if( !empty($row_related_image['itemid']) )
                {
                    $src="/images/size_m__imageid_".$row_related_image['itemid'].".jpg";
                    $link="/images/size_o__imageid_".$row_related_image['itemid'].".jpg";
                    $html_print.="\t<a href='".$link."' title='' target='blank' >\n";
                        $html_print.="\t<img src='".$src."' alt='' />\n";
                    $html_print.="\t</a>\n";
                }
                else
                {
                    $html_print.="NO IMAGE AVAILABLE";
                }
            $html_print.="</td>";
        */
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='/admin/images/edit/?imageid=".$row_related_image['itemid']."' title='' target='blank' >\n";
                    $html_print.=$row_related_image['itemid'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row_image['src_old'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=UTILS::get_artwork_name($db,$row['partworkID']);
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['ppaintID']."' title='' target='blank' >\n";
                    $html_print.=$row['ppaintID'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='".$painting_url['url']."' title='' target='blank' >\n";
                    $html_print.=$row['ptitleurl'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pnumber'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ptitleEN'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ptitleDE'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ptitleFR'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ptitleIT'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ptitleZH'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                //$html_print.="\t<a href='/admin/paintings/mediums/edit/?mediumid=".$row['mmID']."' title='' target='blank' >\n";
                    $html_print.=$row['mmedium'];
                //$html_print.="\t</a>\n";
            $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/locations/edit/?typeid=1&locationid=".$row['locationid']."' title='' target='blank' >\n";
                        $values_location=array();
                        $values_location['row']=$row;
                        $values_location['museum']=1;
                        //$values_location['return_array']=1;
                        $location=location($db,$values_location);
                        $html_print.=$location;
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pwidth'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pheight'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                if( !empty($row['powidth']) ) $html_print.=$row['powidth'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                if( !empty($row['poheight']) ) $html_print.=$row['poheight'];
            $html_print.="</td>";

            # opp addition info
            $html_print.="<td style='".$td_style."'>";
                $rs1=admin_utils::recto_verso_location($db,2,$row['porecto_signed_1']);
                $html_print.=$rs1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $rs2=admin_utils::recto_verso_location($db,2,"",$row['porecto_signed_2']);
                $html_print.=$rs2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['porecto_signed_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $rd1=admin_utils::recto_verso_location($db,2,$row['porecto_dated_1']);
                $html_print.=$rd1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $rd2=admin_utils::recto_verso_location($db,2,"",$row['porecto_dated_2']);
                $html_print.=$rd2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['porecto_dated_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $rn1=admin_utils::recto_verso_location($db,2,$row['porecto_numbered_1']);
                $html_print.=$rn1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $rn2=admin_utils::recto_verso_location($db,2,"",$row['porecto_numbered_2']);
                $html_print.=$rn2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['porecto_numbered_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $ri1=admin_utils::recto_verso_location($db,2,$row['porecto_inscribed_1']);
                $html_print.=$ri1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $ri2=admin_utils::recto_verso_location($db,2,"",$row['porecto_inscribed_2']);
                $html_print.=$ri2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['porecto_inscribed_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vs1=admin_utils::recto_verso_location($db,1,$row['poverso_signed_1']);
                $html_print.=$vs1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vs2=admin_utils::recto_verso_location($db,1,"",$row['poverso_signed_2']);
                $html_print.=$vs2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poverso_signed_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vd1=admin_utils::recto_verso_location($db,1,$row['poverso_dated_1']);
                $html_print.=$vd1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vd2=admin_utils::recto_verso_location($db,1,"",$row['poverso_dated_2']);
                $html_print.=$vd2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poverso_dated_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vn1=admin_utils::recto_verso_location($db,1,$row['poverso_numbered_1']);
                $html_print.=$vn1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vn2=admin_utils::recto_verso_location($db,1,"",$row['poverso_numbered_2']);
                $html_print.=$vn2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poverso_numbered_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vi1=admin_utils::recto_verso_location($db,1,$row['poverso_inscribed_1']);
                $html_print.=$vi1;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $vi2=admin_utils::recto_verso_location($db,1,"",$row['poverso_inscribed_2']);
                $html_print.=$vi2;
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poverso_inscribed_3'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poinscriptions'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['ponotations'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pocity'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pocountry'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pocollection'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['poprivate_col'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pocopyright'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['posource'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['policence'];
            $html_print.="</td>";


            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['pnotes'];
            $html_print.="</td>";
        $html_print.="</tr>";


        /*
        # updating dimensions
        if( empty($row['powidth']) || empty($row['poheight']) )
        {
            if( !empty($row_related_image['itemid']) )
            {
                $dir=DATA_PATH."/images_new/original/";
                $filename=$row_related_image['itemid'];

                if( file_exists($dir.$filename.".jpg") || file_exists($dir.$filename.".jpeg") )
                {
                    $ext=".jpg";
                }
                elseif( file_exists($dir.$filename.".png") )
                {
                    $ext=".png";
                }
                elseif( file_exists($dir.$filename.".gif") )
                {
                    $ext=".gif";
                }
                elseif( file_exists($dir.$filename.".bmp") )
                {
                    $ext=".bmp";
                }
                else
                {
                    exit("Unsupported file format! Supported formats: JPEG, GIF, PNG, BMP ");
                }

                $image_orginal_path=$dir.$filename.$ext;

                list($width, $height) = getimagesize($image_orginal_path);
                print "\t<a href='".$link."' title='' target='blank' >\n";
                    //print "\t<img src='".$src."' alt='' />\n";
                    print $image_orginal_path;
                print "\t</a>\n";
                print "<br />width-".$width."<br />";
                print "height-".$height."<br />";

                if( empty($width) ) $width="NULL";
                if( empty($height) ) $height="NULL";
                $query_update_dimensions="UPDATE ".TABLE_PAINTING_OPP." SET width=".$width.", height=".$height." WHERE oppinfoid='".$row['pooppinfoid']."' AND paintid='".$row['ppaintID']."' LIMIT 1 ";
                //$db->query($query_update_dimensions);
                //print $query_update_dimensions;

                print "<hr /><br /><br />";
            }
        }
        # END updating dimensions
        */

        # saving images
        /*
        if( !empty($row_related_image['itemid']) )
        {
            $dir=DATA_PATH."/images_new/original/";
            $filename=$row_related_image['itemid'];

            if( file_exists($dir.$filename.".jpg") || file_exists($dir.$filename.".jpeg") )
            {
                $ext=".jpg";
            }
            elseif( file_exists($dir.$filename.".png") )
            {
                $ext=".png";
            }
            elseif( file_exists($dir.$filename.".gif") )
            {
                $ext=".gif";
            }
            elseif( file_exists($dir.$filename.".bmp") )
            {
                $ext=".bmp";
            }
            else
            {
                exit("Unsupported file format! Supported formats: JPEG, GIF, PNG, BMP ");
            }

            $image_orginal_path=$dir.$filename.$ext;

            copy($image_orginal_path, DATA_PATH."/export/paintings/".$filename.$ext);
        }
        */
        # END saving images

    }


}


$html_print.="</table>";

print $html_print;

exit;


#SALE HISTORY
/*
$csv="paintID;saleID;auction_house;saleDate;lotNo;estLow;estHigh;estCurr;estLowUSD;estHighUSD;soldFor;soldForCurr;soldForUSD;boughtIn;saleName;saleType;number\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_SALEHISTORY." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $results_sale_relations=$db->query("SELECT relationid,typeid1,itemid1,typeid2,itemid2,sort FROM ".TABLE_RELATIONS." WHERE ( itemid1='".$row['saleID']."' AND typeid1=2 AND typeid2=1 ) OR ( itemid2='".$row['saleID']."' AND typeid2=2 AND typeid1=1 ) ORDER BY sort ASC, relationid DESC ");
        $row_sale_relations=$db->mysql_array($results_sale_relations);
        $paintid=UTILS::get_relation_id($db,1,$row_sale_relations);
        $results_house=$db->query("SELECT house FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row['ahID']."' ");
        $row_house=$db->mysql_array($results_house);
        $row_house=UTILS::html_decode($row_house);
        $results_esti_curr=$db->query("SELECT currency FROM ".TABLE_CURRENCY." WHERE currID='".$row['estCurrID']."' ");
        $row_esti_curr=$db->mysql_array($results_esti_curr);
        $results_sold_curr=$db->query("SELECT currency FROM ".TABLE_CURRENCY." WHERE currID='".$row['soldForCurrID']."' ");
        $row_sold_curr=$db->mysql_array($results_sold_curr);
        $csv.="\"".UTILS::prepare_for_csv($db,$paintid)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_house['house'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleDate'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['lotNo'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estLow'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estHigh'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_esti_curr['currency'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estLowUSD'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['estHighUSD'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['soldFor'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row_sold_curr['currency'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['soldForUSD'])."\";";
                            if( $row['boughtIn']==1 ) $boughtin="yes";
                            else $boughtin="no";
        $csv.="\"".UTILS::prepare_for_csv($db,$boughtin)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['saleName'])."\";";
                            if( $row['saleType']==1 ) $saleType="Premium";
                            elseif( $row['saleType']==2 ) $saleType="Hammer";
                            elseif( $row['saleType']==3 ) $saleType="Unknown";
                            else $saleType="";
        $csv.="\"".UTILS::prepare_for_csv($db,$saleType)."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
*/

#EXHIBITIONS
/*
//$csv="exID;title_original;titleEN;titleDE;titleCH;descriptionEN;descriptionDE;descriptionCH;type;notes;locationEN;locationDE;locationCH;startDate;endDate\r\n";
$csv="exID|title_original|titleEN|titleFR|descriptionEN|descriptionFR|locationEN|locationFR\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_EXHIBITIONS." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['exID'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_original'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\"|";
        if( $row['descriptionEN']=="<br>" ) $row['descriptionEN']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\"|";
        //if( $row['descriptionDE']=="<br>" ) $row['descriptionDE']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\"|";
        if( $row['descriptionFR']=="<br>" ) $row['descriptionFR']="";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionFR'])."\"|";
        //if( $row['descriptionCH']=="<br>" ) $row['descriptionCH']="";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['type'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationEN'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationDE'])."\"|";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['locationFR'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['locationCH'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['startDate2'])."\"|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['endDate2'])."\"|";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */
#EXHIBITIONS PAINTINGS
/*
$csv="paintID;exID;artworkID;editionID;titleEN;titleDE;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";

    $results_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." ");
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC ";
        $results_rel=$db->query($query);
        while( $row_rel=$db->mysql_array($results_rel) )
{
$paintid=UTILS::get_relation_id($db,"1",$row_rel);
$results=$db->query("SELECT
                paintID,
                artworkID,
                editionID,
                titleEN,
                titleDE,
                titleCH,
                year,
                number,
                ednumber,
                ednumberroman,
                ednumberap,
                citylifepage,
                nr_of_editions,
                catID,
                size,
                height,
                width,
                src,
                mID,
                muID,
                sort,
                artwork_notesEN,
                artwork_notesDE,
                artwork_notesCH,
                notes,
                image_zoomer,
                created,
                modified,
                enable
            FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");

$row=$db->mysql_array($results);
    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    $csv.="\"".UTILS::prepare_for_csv($db,$row['paintID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row_exh['exID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artworkID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['editionID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['number'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";

    //$csv.="<br />\r\n";
    $csv.="\r\n";

}
    }
$html_print.=$csv;
*/





# LITERATURE categories
/*
$csv="books_catid;title_en;title_de;title_ch\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_BOOKS_CATEGORIES." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['books_catid'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */

#BIOGRAPHY
/*
//$csv="biographyid;title_en;title_de;title_ch;text_en;text_de;text_ch\r\n";
$csv="biographyid|title_en\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_BIOGRAPHY." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="".UTILS::prepare_for_csv($db,$row['biographyid'])."|";
        $csv.="".UTILS::prepare_for_csv($db,$row['title_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['title_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['title_ch'])."\";";
        $csv.="".UTILS::prepare_for_csv($db,$row['text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */

#QUOTES
/*
//$csv="quoteid;quotes_categoryid;year;text_en;text_de;text_ch;source_en;source_de;source_ch;source_text_en;source_text_de;source_text_ch;\r\n";
//$csv="quoteid|text_en|source_en|source_text_en|\r\n";
$csv="quoteid|source_text_en\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_QUOTES." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="".UTILS::prepare_for_csv($db,$row['quoteid'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['quotes_categoryid'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
        //$csv.="".UTILS::prepare_for_csv($db,$row['text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['text_ch'])."\";";
        //$csv.="".UTILS::prepare_for_csv($db,$row['source_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_ch'])."\";";
        $csv.="".UTILS::prepare_for_csv($db,$row['source_text_en'])."|";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_text_de'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['source_text_ch'])."\";";
        $csv.="\r\n";
    }
   $html_print.=$csv;
 */

# VIDEO
/*
$csv="videoID;titleEN;titleDE;titleCH;titlelong_en;titlelong_de;titlelong_ch;location_en;location_de;location_ch;descriptionEN;descriptionDE;descriptionCH;info_en;info_de;info_ch;\r\n";

    $results=$db->query("SELECT * FROM ".TABLE_VIDEO." ");
    $count=$db->numrows($results);
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['videoID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['titlelong_ch'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['location_ch'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['descriptionCH'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_en'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_de'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['info_ch'])."\";";
        $csv.="\r\n";
    }
    $html_print.=$csv;
 */


# MEDIUMS
/*
$query="SELECT * FROM ".TABLE_MEDIA." ORDER BY mID ASC ";
$results=$db->query($query);
$count=$db->numrows($results);

$csv="mediumEN;mediumDE;mediumFR;mediumCH;\r\n";
            while( $row=$db->mysql_array($results) )
            {
        $row=UTILS::html_decode($row);
                $results_used=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE mID='".$row['mID']."' ");
                $count_used=$db->numrows($results_used);
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumEN'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumDE'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumFR'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['mediumCH'])."\";";
        $csv.="\"".$count_used."\";";
        $csv.="\r\n";
            }
    $html_print.=$csv;
 */

#LITERATURE
/*
//$csv="id;books_catid;title;author;publisher;year;language;isbn;cover;NOpages;notesEN;notesDE;notesCH;infoEN;infoDE;infoCH\r\n";
$csv="id;title;author;publisher;year\r\n";
//$csv="bookid;infoEN;infoDE;infoFR;\r\n";

    //$results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE (infoEN IS NULL OR infoEN='') AND (books_catid=1 OR books_catid=2 OR books_catid=4) ");
    $results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE infoEN IS NOT NULL AND infoEN!='' ");
    //$results=$db->query("SELECT * FROM ".TABLE_BOOKS." ");
    $count=$db->numrows($results);
    $html_print.=$count;
    while( $row=$db->mysql_array($results) )
    {

        //$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=1 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        //$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        //$row_related_image=$db->mysql_array($results_related_image);
        //$count_image=$db->numrows($results_related_image);
        //$imageid=UTILS::get_relation_id($db,"17",$row_related_image);
        //$src="/images/size_xs__imageid_".$imageid.".jpg";

        //if( $count_image )
        //{
        $row=UTILS::html_decode($row);
        //$html_print.="<img src='".$src."' alt='' />";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['id'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['books_catid'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['title'])."\";";
        $csv.="\"".UTILS::prepare_for_csv($db,$row['author'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['publisher'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['language'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['isbn'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['cover'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['NOpages'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesEN'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesDE'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['notesCH'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoEN'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoDE'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoFR'])."\";";
        //$csv.="\"".UTILS::prepare_for_csv($db,$row['infoCH'])."\";";
        $csv.="\r\n";
        //$csv.="<br />";
        //}
    }
    $html_print.=$csv;
 */
#PAINTINGS
//exit;
    //$query_where_painting=" WHERE ( p.artworkID=1 OR p.artworkID=2 OR p.artworkID=13 ) AND artwork_notesEN!='' AND artwork_notesEN IS NOT NULL ";
    //$query_where_painting=" WHERE ( p.artworkID=1 OR p.artworkID=2 OR p.artworkID=13 ) ";
    $query_where_painting=" WHERE ( p.artworkID=6 ) ";
    //$query_where_painting.=" AND ( titleEN LIKE '%Jan%' OR titleEN LIKE '%Feb%' OR titleEN LIKE '%March%' OR titleEN LIKE '%April%' OR titleEN LIKE '%May%' OR titleEN LIKE '%June%' OR titleEN LIKE '%July%' OR titleEN LIKE '%August%' OR titleEN LIKE '%Sep%' OR titleEN LIKE '%Oct%' OR titleEN LIKE '%Nov%' OR titleEN LIKE '%Dec%' ) ";
    //$query_where_painting=" WHERE p.catID=0 OR p.catID IS NULL OR p.catID='' ";
    //$query_where_painting=" WHERE paintID=5549 ";
    $query_order=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results=$db->query($query_painting['query']);


//$csv="paintID;artworkID;editionID;titleEN;titleDE;titleFR;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesFR;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";
//$csv="paintID|number|artwork_notesEN\r\n";
//$csv="paintID|artwork|titleEN|titleDE|number|year|paintings\r\n";
//$csv="paintID;artwork;number;year;titleEN;titleDE;titleFR;titleIT,titleCN\r\n";
$csv="paintID;artwork;year;date;number;titleEN;titleDE;titleFR;titleIT;titleZH\r\n";
$paintings_array=array();
while( $row=$db->mysql_array($results) )
{
    $test=1;
/*
    $tmp_en=explode("-",$row['titleurl_en']);
    foreach($tmp_en as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_en'];
            $html_print.=$count_length."=".$row['titleEN']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_de=explode("-",$row['titleurl_de']);
    foreach($tmp_de as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_de'];
            $html_print.=$count_length."=".$row['titleDE']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_fr=explode("-",$row['titleurl_fr']);
    foreach($tmp_fr as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_fr'];
            $html_print.=$count_length."=".$row['titleFR']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_it=explode("-",$row['titleurl_it']);
    foreach($tmp_it as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_it'];
            $html_print.=$count_length."=".$row['titleIT']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }

    $tmp_ch=explode("-",$row['titleurl_ch']);
    foreach($tmp_ch as $word)
    {
        $count_length=strlen($word);
        if( $_GET['debug']==1 )
        {
            $tmp=$row['titleurl_ch'];
            $html_print.=$count_length."=".$row['titleCH']."=".$tmp."<br />";
        }
        if( $count_length>=19 ) $test=1;
    }
 */

    if($test==1)
    {


    $row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);

    //$row['titleDE']=translateUTF8ToWindowsCP1252($row['titleDE']);
    //$row['titleCH']=translateUTF8ToWindowsCP1252($row['titleCH']);
    //$row['titleCH']=_convert($row['titleCH']);

    /*
    $query_where_painting=" WHERE ( p.artworkID=5 OR p.artworkID=7 OR p.artworkID=8 ) AND p.titleDE=\"".$row['titleDE']."\" AND paintID!='".$row['paintID']."' ";
    $query_order=" ORDER BY p.sort2 ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results2=$db->query($query_painting['query']);

$count2=$db->numrows($results2);
if( $count2>=1 )
{

        if( $i==1 )
        {
            $artwork=UTILS::get_artwork_name($db,$row2['artworkID']);
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['paintID'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$artwork)."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleEN'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleDE'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['number'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['year'])."\"|";
        }
        else
        {
        $csv.=UTILS::prepare_for_csv($db,$row2['paintID']).",";
        }
        $paintings_array[$row['paintID']]=$row;
    //$csv.="\r\n";
}
     */

    $csv.="".UTILS::prepare_for_csv($db,$row['paintID']).";";
    $csv.="".UTILS::prepare_for_csv($db,UTILS::get_artwork_name($db,$row['artworkID'])).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['year']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['date']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['number']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleEN']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleDE']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleFR']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleIT']).";";
    $csv.="".UTILS::prepare_for_csv($db,$row['titleZH']).";";


    /*
    $csv.="\"".UTILS::prepare_for_csv($db,$row['titleCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['year'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumber'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberroman'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['ednumberap'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['citylifepage'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['nr_of_editions'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['catID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['size'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['height'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['width'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['src'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['mID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['muID'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['sort'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesFR'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['notes'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['image_zoomer'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['created'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['modified'])."\";";
    $csv.="\"".UTILS::prepare_for_csv($db,$row['enable'])."\";";
     */
    //$csv.="<br />\r\n";
    $csv.="\r\n";

    }
}

/*
foreach( $paintings_array as $row2 )
{

    $query_where_painting=" WHERE ( p.artworkID=5 OR p.artworkID=7 OR p.artworkID=8 ) AND p.titleDE=\"".$row2['titleDE']."\" AND paintID!='".$row2['paintID']."' ";
    $query_order=" ORDER BY p.sort2 ASC  ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results2=$db->query($query_painting['query']);
$similar="";
while( $row=$db->mysql_array($results2) )
{
    $similar.=$row['paintID'].",";
}

            $artwork=UTILS::get_artwork_name($db,$row2['artworkID']);
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['paintID'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$artwork)."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleEN'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['titleDE'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['number'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$row2['year'])."\"|";
            $csv.="\"".UTILS::prepare_for_csv($db,$similar)."\"|";
    $csv.="\r\n";
}
 */
$html_print.=$csv;
#END PAINTINGS

//$html_print.=count($paintings_array);
//print_r($paintings_array);


$html->foot();
?>
