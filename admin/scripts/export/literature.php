<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

require ($_SERVER["DOCUMENT_ROOT"]."/includes/table.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/relation_tabs.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/class.html.inc.php");

$html = new admin_html;
$html_front = new html_elements;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;

$html_print="";

exit;

$query_where_books=" WHERE books_catid=6 OR books_catid=17 OR books_catid=19 ";
$query_order_books=" ORDER BY year DESC,id DESC ";
//$query_limit_books=" LIMIT 10 ";
$query_books=QUERIES::query_books($db,$query_where_books,$query_order_books,$query_limit_books);
$results=$db->query($query_books['query']);

    $count=$db->numrows($results);

$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            $html_print.="<th>book id</th>";
            $html_print.="<th>image</th>";
            $html_print.="<th>date</th>";
            $html_print.="<th>author</th>";
            $html_print.="<th>title</th>";
            $html_print.="<th>publisher</th>";
            $html_print.="<th>pages</th>";
            $html_print.="<th>isbn + language</th>";
            $html_print.="<th>notesEN</th>";
            $html_print.="<th>notesDE</th>";
            $html_print.="<th>notesFR</th>";
            $html_print.="<th>notesIT</th>";
            $html_print.="<th>notesZH</th>";
            $html_print.="<th>notes_admin</th>";
            $html_print.="<th>keywords</th>";
            //$html_print.="<th>relations</th>";
        $html_print.="</tr>";
    $html_print.="</thead>";


    while( $row=$db->mysql_array($results) )
    {  

        $row=UTILS::html_decode($row);

        //if( !empty($row['infoEN']) || !empty($row['infoDE']) || !empty($row['infoFR']) || !empty($row['infoIT']) || !empty($row['infoZH']) )
        //{

            $options_relations=array();
            $options_relations['row']=$row;
            $options_relations['bookid']=$row['id'];
            $options_relations['typeid']=12;
            $options_relations['itemid']=$row['id'];
            $options_relations['return']=1;
            $options_relations['html_return']=1;
            $options_relations['html_class']=$html_front;
            $relations=div_relation_tabs($db,$options_relations);

            $html_print.="<tr>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/books/edit/?bookid=".$row['id']."' title='' target='blank' >\n";
                        $html_print.=$row['id'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                                        $html_print.="\t<a href='/admin/books/edit/?bookid=".$row['id']."' title='' target='blank' >\n";
                        
                    
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_s__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";
                    if( !empty($imageid) ) 
                    {
                        $html_print.="\t<img src='".$src."' alt='' border='0' />\n";
                    }
$html_print.="\t</a>\n";
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['date_display_year'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['author'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['title'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['publisher'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['NOpages'];
                $html_print.="</td>";


                    $query_isbn="SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$row['id']."' AND bookID!='' ORDER BY id ASC";
                    //print $query_isbn;
                    $results_isbn=$db->query($query_isbn);
                    $count_isbn=$db->numrows($results_isbn);
                    //print $count_isbn;
                    $isbn="";
                    $i=0;
                    while( $row_isbn=$db->mysql_array($results_isbn) )
                    {
                        $i++;
                        $isbn=$row_isbn['isbn'];
                        if( !empty($row_isbn['language']) || !empty($row_isbn['language2']) || !empty($row_isbn['language3']) )
                        {
                            if( !empty($row_isbn['isbn']) ) $isbn.=" - ";

                                $query_where_language=" WHERE languageid='".$row_isbn['language']."' ";
                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                $results_language=$db->query($query_language['query']);
                                $row_language=$db->mysql_array($results_language);
                                $row_language=UTILS::html_decode($row_language);
                                $title_language=UTILS::row_text_value($db,$row_language,"title");
                                $query_where_language=" WHERE languageid='".$row_isbn['language2']."' ";
                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                $results_language=$db->query($query_language['query']);
                                $row_language=$db->mysql_array($results_language);
                                $row_language=UTILS::html_decode($row_language);
                                $title_language2=UTILS::row_text_value($db,$row_language,"title");
                                $query_where_language=" WHERE languageid='".$row_isbn['language3']."' ";
                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                $results_language=$db->query($query_language['query']);
                                $row_language=$db->mysql_array($results_language);
                                $row_language=UTILS::html_decode($row_language);
                                $title_language3=UTILS::row_text_value($db,$row_language,"title");




                            if( !empty($row_isbn['language']) ) $isbn.=$title_language;
                            if( !empty($row_isbn['language2']) ) $isbn.=", ".$title_language2;
                            if( !empty($row_isbn['language3']) ) $isbn.=", ".$title_language3;
                        }
                        if( $i<$count_isbn )
                        {
                            $isbn.="<br />";
                        }
                    }

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$isbn;
                $html_print.="</td>";                

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notesEN'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notesDE'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notesFR'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notesIT'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notesZH'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['notes_admin'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row['keywords'];
                $html_print.="</td>";

                /*
                $html_print.="<td style='".$td_style."'>";
                    if( is_array($relations) )
                    {
                        foreach ($relations['tab'] as $key => $value) {
                            //$html_print.="<td style='".$td_style."'>";
                                $html_print.=$key;
                                $html_print.="<br />\n";
                            //$html_print.="</td>";
                        }
                    }
                $html_print.="</td>";
                */


            $html_print.="</tr>";
        //}

    }

$html_print.="</table>";

print $html_print;
exit;


/******** paintings with linked books *********/



$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            $html_print.="<th>PaintID</th>";
            $html_print.="<th>Artwork</th>";
            $html_print.="<th>book id</th>";
            $html_print.="<th>title</th>";
            $html_print.="<th>author</th>";
            $html_print.="<th>publisher</th>";
            $html_print.="<th>date</th>";
            $html_print.="<th>mentioned</th>";
            $html_print.="<th>discussed</th>";
            $html_print.="<th>illustrated</th>";
        $html_print.="</tr>";
    $html_print.="</thead>";

    $query_export=" SELECT ";

    $query_export.=" p.paintID AS ppaintID, p.artworkID AS partworkID ";

    //$query_export.=" p.paintID AS ppaintID, p.artworkID AS partworkID,p.titleurl AS ptitleurl, p.number AS pnumber, p.titleEN AS ptitleEN, p.titleDE AS ptitleDE, p.titleFR AS ptitleFR, p.titleIT AS ptitleIT, p.titleZH AS ptitleZH,m.mID AS mmID,m.mediumEN AS mmedium,p.width AS pwidth,p.height AS pheight,p.notes AS pnotes ";


    $query_export.=" FROM ".TABLE_PAINTING." p ";

    //$query_export.=" LEFT JOIN ".TABLE_PAINTING_OPP." po ON p.paintID=po.paintid ";
    //$query_export.=" LEFT JOIN ".TABLE_MEDIA." m ON p.mID=m.mID ";
    //$query_export.=" WHERE p.artworkID=6 OR ( p.artworkID=4 AND ( p.number_search LIKE '72%' OR p.number_search LIKE '78%' OR p.number_search LIKE '101%' OR p.number_search LIKE '110%' OR p.number_search LIKE '122%' OR p.number_search LIKE '136%' ) ) ";
    //$query_export.=" WHERE p.paintID='".$row_exh_painting_relation['paintID']."'  ";
    $query_export.=" WHERE p.artworkID=6  ";#opp
    $query_export.=" OR ( p.artworkID=4 && ( p.number=66 OR p.number=67 OR p.number=72 OR p.number=78 OR p.number=122 OR p.number=132 ) ) ";#editions
    //$query_export.=" WHERE p.artworkID=8  ";#oil on papper
    //$query_export.=" WHERE p.artworkID=5  ";# watercolor
    //$query_export.=" AND p.paintID=14699  ";

    $query_export.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    //$query_export.=" LIMIT 0,100 ";

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    while( $row=$db->mysql_array($results) )
    {  











    # exhibitions
        $query_literature=QUERIES::query_books($db);
        $results_sale_hist=$db->query("SELECT ".$query_literature['query_columns'].",relationid FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=1 AND r.itemid1='".$row['ppaintID']."' AND r.typeid2=12 ) OR ( r.typeid2=1 AND r.itemid2='".$row['ppaintID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC");

/*
    $query_where_exhibitions="";
    $query_order_exhibitions=" ORDER BY start_year_admin DESC, typeid DESC, start_month_admin DESC, start_day_admin DESC ";
    //$query_limit=" LIMIT 100 ";
    $query_limit_exhibitions=" ";
    $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions,$query_order_exhibitions,$query_limit_exhibitions);

    
    $results_exh=$db->query($query_exhibitions['query']);
    */
    $count_exh=$db->numrows($results_sale_hist);
    while( $row_exh=$db->mysql_array($results_sale_hist) )
    {
        $row_exh=UTILS::html_decode($row_exh);

                $results_auction_house=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row_exh['ahID']."'");
                $row_auction_house=$db->mysql_array($results_auction_house);
                $row_auction_house=UTILS::html_decode($row_auction_house);
        //print_r($row_exh);

        //$query="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exh['exID']."' AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row_exh['exID']."' AND typeid1=1 ) ORDER BY sort1 ASC, relationid DESC LIMIT 1 ";
        //$results_rel=$db->query($query);
        //$count_exh_paint=$db->numrows($results_rel);

        //if( !$count_exh_paint )
        //{
            $html_print.="<tr>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['ppaintID']."' title='' target='blank' >\n";
                        $html_print.=$row['ppaintID'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.=UTILS::get_artwork_name($db,$row['partworkID']);
                $html_print.="</td>";
                $html_print.="<td style='".$td_style."'>";
                    $html_print.="\t<a href='/admin/books/edit/?bookid=".$row_exh['id']."' title='' target='blank' >\n";
                        $html_print.=$row_exh['id'];
                    $html_print.="\t</a>\n";
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['title'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['author'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['publisher'];
                $html_print.="</td>";

                $html_print.="<td style='".$td_style."'>";
                    $html_print.=$row_exh['date_display_year'];
                $html_print.="</td>";

                //$html_print.="<td style='".$td_style."'>";

                    $query_columns_literature=QUERIES::query_books($db);
                    $query_literature_relations="SELECT 
                            ".$query_columns_literature['query_columns'].",
                            relationid 
                        FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b 
                        WHERE ( 
                            ( r.typeid1=1 AND r.itemid1='".$row['ppaintID']."' AND r.itemid2='".$row_exh['id']."' AND r.typeid2=12 ) 
                            OR ( r.typeid2=1 AND r.itemid2='".$row['ppaintID']."' AND r.itemid1='".$row_exh['id']."' AND r.typeid1=12 ) ) 
                            AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) 
                            AND b.enable=1 ";
                    $query_literature_relations.=" ORDER BY b.books_catid ASC, isnull ASC, b.author ASC ";
                    $results_literature=$db->query($query_literature_relations);
                    $count_rel_lit=$db->numrows($results_literature);

                    //$html_print.=$count_rel_lit;

                    if( $count_rel_lit )
                    {
                        $row_literature=$db->mysql_array($results_literature,0);

                        $query_rel_lit_where=" WHERE relationid='".$row_literature['relationid']."' ";
                        $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
                        $results_rel_lit=$db->query($query_rel_lit['query']);
                        $count_rel_lit=$db->numrows($results_rel_lit);
                        if( $count_rel_lit )
                        {
                            $row_rel_lit=$db->mysql_array($results_rel_lit,0);


                            $html_print.="<td style='".$td_style."'>";
                                # MENTIONED
                                if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['show_mentioned']) )
                                {
                                    $s="";
                                    $tmp=explode(",",$row_rel_lit['mentioned']);
                                    $s.= LANG_ARTWORK_BOOKS_REL_MENTIONED;
                                    if( empty($row_rel_lit['show_mentioned']) )
                                    {
                                        $s.= ": ";
                                        if( count($tmp)>1 || stripos($row_rel_lit['mentioned'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                        else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                        $s.= " ".$row_rel_lit['mentioned']; 
                                    }
                                    $options_admin_edit=array();
                                    $options_admin_edit['html_return']=1;
                                    //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                                    $html_print.=$s;
                                }
                                else
                                {
                                    $html_print.="&nbsp;";
                                }
                            $html_print.="</td>";

                            # DISCUSSED
                            $html_print.="<td style='".$td_style."'>";
                            if( !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_discussed']) )
                            {
                                $s="";
                                $s.= LANG_ARTWORK_BOOKS_REL_DISCUSSED;
                                if( empty($row_rel_lit['show_discussed']) )
                                {
                                    $s.= ": ";
                                    if( count($tmp)>1 || stripos($row_rel_lit['discussed'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                    else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                    $s.= " ".$row_rel_lit['discussed']; 
                                    if( $_GET['lang']=='zh' ) $s.= LANG_ARTWORK_BOOKS_REL_CHINESE;
                                }
                                $options_admin_edit=array();
                                $options_admin_edit['html_return']=1;
                                //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                                $html_print.=$s;
                            }
                            else
                            {
                                $html_print.="&nbsp;";
                            }
                            $html_print.="</td>";

                            # ILLUSTRATED
                            $html_print.="<td style='".$td_style."'>";
                            if( !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) )
                            {
                                $s="";
                                $tmp=explode(",",$row_rel_lit['illustrated_bw']);
                                $tmp2=explode(",",$row_rel_lit['illustrated']);
                                if( empty($row_rel_lit['show_illustrated']) && empty($row_rel_lit['show_bw']) )
                                {
                                    $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED;
                                    $s.= ": ";
                                    if( count($tmp)>1 || count($tmp2)>1 || stripos($row_rel_lit['illustrated_bw'],"-") || stripos($row_rel_lit['illustrated'],"-") || (!empty($row_rel_lit['illustrated_bw']) && !empty($row_rel_lit['illustrated'])) ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                    else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                    if( !empty($row_rel_lit['illustrated_bw']) )
                                    {
                                        $s.= " ".$row_rel_lit['illustrated_bw']; 
                                        $s.= " (".LANG_ARTWORK_BOOKS_REL_BW.")";
                                    }
                                    if( !empty($row_rel_lit['illustrated']) )
                                    {
                                        if( !empty($row_rel_lit['illustrated_bw']) )
                                        {
                                            $s.= ", ";
                                        }
                                        $s.= " ".$row_rel_lit['illustrated']; 
                                        $s.= " (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                                    }
                                }
                                # else if show illustrated or show black and white ticked
                                else
                                {
                                    if( !empty($row_rel_lit['show_illustrated']) ) 
                                    {
                                        $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                                    }
                                    if( !empty($row_rel_lit['show_bw']) ) 
                                    {
                                        if( !empty($row_rel_lit['show_illustrated']) ) $s.="<br />";
                                        $s.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_BW.")";
                                    }
                                }
                                $options_admin_edit=array();
                                $options_admin_edit['html_return']=1;
                                //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                                $html_print.=$s;
                            }
                            else
                            {
                                $html_print.="&nbsp;";
                            }
                            $html_print.="</td>";
                            # END ILLUSTRATED


                        }
                        else
                        {
                            $html_print.="<td></td>";
                            $html_print.="<td></td>";
                            $html_print.="<td></td>";                       
                        }
                    }
                    else
                    {
                        $html_print.="<td></td>";
                        $html_print.="<td></td>";
                        $html_print.="<td></td>";

                    }




                //$html_print.="</td>";

            $html_print.="</tr>";
        //}


    }









    }

$html_print.="</table>";

print $html_print;

exit;





$html->foot();
?>
