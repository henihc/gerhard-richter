<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

require ($_SERVER["DOCUMENT_ROOT"]."/includes/table.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/relation_tabs.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/class.html.inc.php");

$html = new admin_html;
$html_front = new html_elements;
$html->css[]="/admin/css/admin.css";
$html->metainfo();

$db=new dbCLASS;

$html_print="";


/******** paintings with linked books *********/



$html_print.="<table class='admin-table'>";

    $html_print.="<thead>";
        $html_print.="<tr>";
            $html_print.="<th>PaintID</th>";
            $html_print.="<th>Artwork</th>";
            $html_print.="<th>titleEN</th>";
            $html_print.="<th>titleDE</th>";
            $html_print.="<th>number</th>";
            $html_print.="<th>book id</th>";
            $html_print.="<th>mentioned</th>";
            $html_print.="<th>discussed</th>";
            $html_print.="<th>illustrated</th>";
        $html_print.="</tr>";
    $html_print.="</thead>";

    //$bookid="151";
    //$bookid="631";

    //$bookid="2299";
    //$bookid="2298";

    //$bookid="1510";
    $bookid="1402";





    $query_export=" SELECT 
            r.relationid,p.paintID, p.paintID AS itemid2, p.artworkID, p.artworkID2, p.editionID, p.edition_individual, p.titleEN, 
            p.titleDE, p.titleFR, p.titleIT, p.titleZH, p.titleurl, p.year, p.date, 
            DATE_FORMAT(date ,'%Y-%m-%d') AS date_admin, p.number, p.number_search, p.ednumber, p.ednumberroman, p.ednumberap, 
            p.citylifepage, p.nr_of_editions, p.catID, p.size, p.size_parts, p.size_typeid, p.height, p.width, p.src, p.mID, 
            p.muID, p.locationid, p.cityid, p.countryid, p.optionid_loan_type, p.loan_locationid, p.loan_cityid, p.loan_countryid, 
            p.collection_notes_en, p.collection_notes_de, p.collection_notes_fr, p.collection_notes_it, p.collection_notes_zh, 
            p.sort,p.sort2,p.sort3, p.artwork_notesEN, p.artwork_notesDE, p.artwork_notesFR, p.artwork_notesIT, p.artwork_notesZH, 
            p.notes, p.image_zoomer, p.created, p.modified, DATE_FORMAT(p.modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin, 
            DATE_FORMAT(p.created ,'%d %b %Y %H:%i:%s') AS date_created_admin, p.enable, p.keywords 
        FROM relations r, painting p 
        WHERE ( ( r.typeid1=12 AND r.itemid1='".$bookid."' AND r.typeid2=1 ) OR ( r.typeid2=12 AND r.itemid2='".$bookid."' AND r.typeid1=1 ) ) 
            AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) AND p.enable=1 
        ORDER BY p.sort2 ASC, titleEN ASC ";

    //$query_export.=" LIMIT 10 ";

    $results=$db->query($query_export);
    $count=$db->numrows($results);

    while( $row=$db->mysql_array($results) )
    {  
        $row=UTILS::html_decode($row);

        $html_print.="<tr>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' title='' target='blank' >\n";
                    $html_print.=$row['paintID'];
                $html_print.="\t</a>\n";
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=UTILS::get_artwork_name($db,$row['artworkID']);
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['titleEN'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['titleDE'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.=$row['number'];
            $html_print.="</td>";
            $html_print.="<td style='".$td_style."'>";
                $html_print.="\t<a href='/admin/books/edit/?bookid=".$bookid."' title='' target='blank' >\n";
                    $html_print.=$bookid;
                $html_print.="\t</a>\n";
            $html_print.="</td>";

            $query_rel_lit_where=" WHERE relationid='".$row['relationid']."' ";
            $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
            $results_rel_lit=$db->query($query_rel_lit['query']);
            $count_rel_lit=$db->numrows($results_rel_lit);
            if( $count_rel_lit )
            {
                $row_rel_lit=$db->mysql_array($results_rel_lit,0);


                $html_print.="<td style='".$td_style."'>";
                    # MENTIONED
                    if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['show_mentioned']) )
                    {
                        $s="";
                        $tmp=explode(",",$row_rel_lit['mentioned']);
                        $s.= LANG_ARTWORK_BOOKS_REL_MENTIONED;
                        if( empty($row_rel_lit['show_mentioned']) )
                        {
                            $s.= ": ";
                            if( count($tmp)>1 || stripos($row_rel_lit['mentioned'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                            else $s.= LANG_ARTWORK_BOOKS_REL_P;
                            $s.= " ".$row_rel_lit['mentioned']; 
                        }
                        $options_admin_edit=array();
                        $options_admin_edit['html_return']=1;
                        //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                        $html_print.=$s;
                    }
                    else
                    {
                        $html_print.="&nbsp;";
                    }
                $html_print.="</td>";

                # DISCUSSED
                $html_print.="<td style='".$td_style."'>";
                if( !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_discussed']) )
                {
                    $s="";
                    $s.= LANG_ARTWORK_BOOKS_REL_DISCUSSED;
                    if( empty($row_rel_lit['show_discussed']) )
                    {
                        $s.= ": ";
                        if( count($tmp)>1 || stripos($row_rel_lit['discussed'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                        else $s.= LANG_ARTWORK_BOOKS_REL_P;
                        $s.= " ".$row_rel_lit['discussed']; 
                        if( $_GET['lang']=='zh' ) $s.= LANG_ARTWORK_BOOKS_REL_CHINESE;
                    }
                    $options_admin_edit=array();
                    $options_admin_edit['html_return']=1;
                    //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                    $html_print.=$s;
                }
                else
                {
                    $html_print.="&nbsp;";
                }
                $html_print.="</td>";

                # ILLUSTRATED
                $html_print.="<td style='".$td_style."'>";
                if( !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) )
                {
                    $s="";
                    $tmp=explode(",",$row_rel_lit['illustrated_bw']);
                    $tmp2=explode(",",$row_rel_lit['illustrated']);
                    if( empty($row_rel_lit['show_illustrated']) && empty($row_rel_lit['show_bw']) )
                    {
                        $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED;
                        $s.= ": ";
                        if( count($tmp)>1 || count($tmp2)>1 || stripos($row_rel_lit['illustrated_bw'],"-") || stripos($row_rel_lit['illustrated'],"-") || (!empty($row_rel_lit['illustrated_bw']) && !empty($row_rel_lit['illustrated'])) ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                        else $s.= LANG_ARTWORK_BOOKS_REL_P;
                        if( !empty($row_rel_lit['illustrated_bw']) )
                        {
                            $s.= " ".$row_rel_lit['illustrated_bw']; 
                            $s.= " (".LANG_ARTWORK_BOOKS_REL_BW.")";
                        }
                        if( !empty($row_rel_lit['illustrated']) )
                        {
                            if( !empty($row_rel_lit['illustrated_bw']) )
                            {
                                $s.= ", ";
                            }
                            $s.= " ".$row_rel_lit['illustrated']; 
                            $s.= " (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                        }
                    }
                    # else if show illustrated or show black and white ticked
                    else
                    {
                        if( !empty($row_rel_lit['show_illustrated']) ) 
                        {
                            $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                        }
                        if( !empty($row_rel_lit['show_bw']) ) 
                        {
                            if( !empty($row_rel_lit['show_illustrated']) ) $s.="<br />";
                            $s.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_BW.")";
                        }
                    }
                    $options_admin_edit=array();
                    $options_admin_edit['html_return']=1;
                    //$s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$row_exh['id'],$options_admin_edit);
                    $html_print.=$s;
                }
                else
                {
                    $html_print.="&nbsp;";
                }
                $html_print.="</td>";
                # END ILLUSTRATED


            }






        $html_print.="</tr>";




    }

$html_print.="</table>";

print $html_print;

exit;





$html->foot();
?>
