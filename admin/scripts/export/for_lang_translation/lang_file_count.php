<?php
header("Content-Type: text/html; charset=utf-8");

set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//$word_count=1;

$file_url=$_SERVER["DOCUMENT_ROOT"]."/includes/languages/1-en-english.php";
$file=file_get_contents($file_url,FILE_USE_INCLUDE_PATH);

$file=str_replace ("<?php", "", $file);
$file=str_replace ("?>", "", $file);
$file=strip_tags($file);

$pattern = "/\".*?\"/";
preg_match_all($pattern, $file, $matches1);

//$pattern = "/\'.*?\'/";
//preg_match_all($pattern, $file, $matches2);

//print_r($matches1);
//print $file;

$text="";

foreach ($matches1[0] as $key => $value) {
	$text.=" ".$value;
}

$text=str_replace('"', "", $text);


if( $word_count )
{
    $word_count=admin_utils::str_word_count_unique($text);
    //print "Total unique words IN PAGE = ".$word_count;
    print $text;
}
else print $text;

?>