<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
//$html->metainfo();

$db=new dbCLASS;

//$word_count=1;
$options_prepare_csv=array();
$options_prepare_csv['strip_tags']=$word_count;

    $results=$db->query("SELECT * FROM ".TABLE_TEXT." ORDER BY textid ");

if( !$word_count ) $delimiter="|";

if( !$word_count ) $csv="textid".$delimiter."title_en".$delimiter."title_de".$delimiter."title_fr".$delimiter."title_it".$delimiter."title_zh".$delimiter."text_en".$delimiter."text_de".$delimiter."text_fr".$delimiter."text_it".$delimiter."text_zh".$delimiter."collection_notes_zh".$delimiter."artwork_notesEN".$delimiter."artwork_notesDE".$delimiter."artwork_notesFR".$delimiter."artwork_notesIT".$delimiter."artwork_notesZH".$delimiter."\r\n";
$paintings_array=array();
while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);

    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['textid'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['title_en'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['title_de'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['title_fr'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['title_it'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['title_zh'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['text_en'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['text_de'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['text_fr'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['text_it'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['text_zh'],$options_prepare_csv).$delimiter;

    $csv.="\r\n";

}

#END PAINTINGS

if( $word_count )
{
    $word_count=admin_utils::str_word_count_unique($csv);
    //print "Total unique words TEXT = ".$word_count;
    print $csv;
}
else print $csv;

//$html->foot();
?>
