<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
//$html->metainfo();

$db=new dbCLASS;

//$word_count=1;
$options_prepare_csv=array();
$options_prepare_csv['strip_tags']=$word_count;

    $results=$db->query("SELECT *,IF(author IS NULL OR author='', 1, 0) AS isnull FROM ".TABLE_BOOKS." ORDER BY DATE_FORMAT(link_date, '%Y') DESC, isnull ASC, books_catid ASC, author ASC ");

if( !$word_count ) $delimiter="|";

if( !$word_count ) $csv="bookid".$delimiter."notesEN".$delimiter."notesDE".$delimiter."notesFR".$delimiter."notesIT".$delimiter."notesZH".$delimiter."infosmall_en".$delimiter."infosmall_de".$delimiter."infosmall_fr".$delimiter."infosmall_it".$delimiter."infosmall_zh".$delimiter."infoEN".$delimiter."infoDE".$delimiter."infoFR".$delimiter."infoIT".$delimiter."infoZH"."\r\n";
$paintings_array=array();
while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);

    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['id'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['notesEN'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['notesDE'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['notesFR'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['notesIT'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['notesZH'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['infosmall_en'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infosmall_de'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infosmall_fr'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infosmall_it'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infosmall_zh'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['infoEN'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infoDE'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infoFR'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infoIT'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['infoZH'],$options_prepare_csv);




    $csv.="\r\n";

}

#END PAINTINGS

if( $word_count )
{
    $word_count=admin_utils::str_word_count_unique($csv);
    //print "Total unique words LITERATURE = ".$word_count;
    print $csv;
}
else print $csv;

//$html->foot();
?>
