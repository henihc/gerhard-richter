<?php
header("Content-Type: text/html; charset=utf-8");
//exit;
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$html = new admin_html;
$html->css[]="/admin/css/admin.css";
//$html->metainfo();

$db=new dbCLASS;

//$word_count=1;
$options_prepare_csv=array();
$options_prepare_csv['strip_tags']=$word_count;

    $query_order=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,"",$query_order);
    $results=$db->query($query_painting['query']);

if( !$word_count ) $delimiter="|";

if( !$word_count ) $csv="paintID".$delimiter."number".$delimiter."titleEN".$delimiter."titleDE".$delimiter."titleFR".$delimiter."titleIT".$delimiter."titleZH".$delimiter."collection_notes_en".$delimiter."collection_notes_de".$delimiter."collection_notes_fr".$delimiter."collection_notes_it".$delimiter."collection_notes_zh".$delimiter."artwork_notesEN".$delimiter."artwork_notesDE".$delimiter."artwork_notesFR".$delimiter."artwork_notesIT".$delimiter."artwork_notesZH".$delimiter."\r\n";
$paintings_array=array();
while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);

    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['paintID'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['number'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['titleEN'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['titleDE'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['titleFR'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['titleIT'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['titleZH'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['collection_notes_en'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['collection_notes_de'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['collection_notes_fr'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['collection_notes_it'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['collection_notes_zh'],$options_prepare_csv).$delimiter;
    $csv.=admin_utils::prepare_for_csv($db,$row['artwork_notesEN'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['artwork_notesDE'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['artwork_notesFR'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['artwork_notesIT'],$options_prepare_csv).$delimiter;
    if( !$word_count ) $csv.=admin_utils::prepare_for_csv($db,$row['artwork_notesZH'],$options_prepare_csv);

    $csv.="\r\n";

}

#END PAINTINGS

if( $word_count )
{
    $word_count=admin_utils::str_word_count_unique($csv);
    //print "Total unique words PAINTINGS = ".$word_count;
    print $csv;
}
else print $csv;

//$html->foot();
?>
