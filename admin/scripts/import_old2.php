<?php
header("Content-Type: text/html; charset=utf-8");
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

//$html = new html_elements;
//$html->head('artwork');

$db=new dbCLASS;



### importing title search for paintings
/*
$values_query_painting=array();
//$query_where_painting=" WHERE paintID=10523 ";
$query_painting=QUERIES::query_painting($db,$query_where_painting,"","","",$values_query_painting);
$results=$db->query($query_painting['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);

    $title_de = $row['titleDE'];
    $title_fr = $row['titleFR'];

    print $title_de."<br />";
    print $title_fr."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$title_de;
    $title_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$title_fr;
    $title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_painting="UPDATE ".TABLE_PAINTING." SET 
                title_search_1='".$title_search_de['text_lower_converted']."',
                title_search_2='".$title_search_de['text_lower_german_converted']."',
                title_search_3='".$title_search_fr['text_lower_converted']."' 
        WHERE paintID='".$row['paintID']."' LIMIT 1 ";
    $db->query($query_update_painting);
    
    print $query_update_painting."<br /><br />";

}
exit;
 */
# END

### importing search for biography
/*
$values_query_biography=array();
//$query_where_biography=" WHERE biographyid= ";
$query_biography=QUERIES::query_biography($db,$query_where_biography,"","","",$values_query_biography);
$results=$db->query($query_biography['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);

    $title_de = $row['title_de'];
    $title_fr = $row['title_fr'];
    $text_de = $row['text_de'];
    $text_fr = $row['text_fr'];

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$title_de;
    $title_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$title_fr;
    $title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_de;
    $text_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_fr;
    $text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_biography="UPDATE ".TABLE_BIOGRAPHY." SET 
                title_search_1=\"".$title_search_de['text_lower_converted']."\",
                title_search_2=\"".$title_search_de['text_lower_german_converted']."\",
                title_search_3=\"".$title_search_fr['text_lower_converted']."\",
                text_search_1=\"".$text_search_de['text_lower_converted']."\",
                text_search_2=\"".$text_search_de['text_lower_german_converted']."\",
                text_search_3=\"".$text_search_fr['text_lower_converted']."\"
        WHERE biographyid='".$row['biographyid']."' LIMIT 1 ";
    $db->query($query_update_biography);
    
    print $query_update_biography."<br /><br />";
}
exit;

# END

### importing search for text

$values_query_text=array();
$query_where_text=" WHERE textid=3 ";
$query_text=QUERIES::query_text($db,$query_where_text,"","","",$values_query_text);
$results=$db->query($query_text['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);

    $text_de = $row['text_de'];
    $text_fr = $row['text_fr'];

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$text_de;
    $text_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_fr;
    $text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_text="UPDATE ".TABLE_TEXT." SET 
                text_search_1='".$text_search_de['text_lower_converted']."',
                text_search_2='".$text_search_de['text_lower_german_converted']."',
                text_search_3='".$text_search_fr['text_lower_converted']."'
        WHERE textid='".$row['textid']."' LIMIT 1 ";
    $db->query($query_update_text);
    
    print $query_update_text."<br /><br />";
}
exit;

# END

### importing search for quotes

$values_query_quotes=array();
//$query_where_quotes=" WHERE quoteid= ";
$query_quotes=QUERIES::query_quotes($db,$query_where_quotes,"","","",$values_query_quotes);
$results=$db->query($query_quotes['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);

    $text_de = $row['text_de'];
    $text_fr = $row['text_fr'];
    $source_de = $row['source_de'];
    $source_fr = $row['source_fr'];

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$text_de;
    $text_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_fr;
    $text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$source_de;
    $source_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$source_fr;
    $source_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_quotes="UPDATE ".TABLE_QUOTES." SET 
                text_search_1='".$text_search_de['text_lower_converted']."',
                text_search_2='".$text_search_de['text_lower_german_converted']."',
                text_search_3='".$text_search_fr['text_lower_converted']."',
                source_search_1='".$source_search_de['text_lower_converted']."',
                source_search_2='".$source_search_de['text_lower_german_converted']."',
                source_search_3='".$source_search_fr['text_lower_converted']."'
        WHERE quoteid='".$row['quoteid']."' LIMIT 1 ";
    $db->query($query_update_quotes);
    
    print $query_update_quotes."<br /><br />";
}
//exit;

# END

### importing search for exhibitions

$values_query_exhibitions=array();
//$query_where_exhibitions=" WHERE exID=2855 OR exID=25 ";
$query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions,"","","",$values_query_exhibitions);
$results=$db->query($query_exhibitions['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);

    # good for saving to db
    
    //$title_original = mb_convert_encoding($row['title_original'],"HTML-ENTITIES","UTF-8");
    //$title_de = mb_convert_encoding($row['titleDE'],"HTML-ENTITIES","UTF-8");
    //$title_fr = mb_convert_encoding($row['titleFR'],"HTML-ENTITIES","UTF-8");


    $values_accented=array();
    $values_accented['text']=$row['title_original'];
    $title_original_search=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$row['titleDE'];
    $title_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$row['titleFR'];
    $title_search_fr=UTILS::replace_accented_letters($db, $values_accented);


    print $row['title_original']."<br />";
    print $row['titleDE']."<br />";
    print $row['titleFR']."<br />";

    $query_update_exhibitions="UPDATE ".TABLE_EXHIBITIONS." SET 
                title_original_search_1='".$title_original_search['text_lower_converted']."',
                title_original_search_2='".$title_original_search['text_lower_german_converted']."',
                title_search_1='".$title_search_de['text_lower_converted']."',
                title_search_2='".$title_search_de['text_lower_german_converted']."',
                title_search_3='".$title_search_fr['text_lower_converted']."'
        WHERE exID='".$row['exID']."' LIMIT 1 ";


    $db->query($query_update_exhibitions);
    
    print $query_update_exhibitions."<br /><br />";
}
//exit;

# END


### importing search for locations

$values_query_locations=array();
//$query_where_locations=" WHERE locationid= ";
$query_locations=QUERIES::query_locations($db,$query_where_locations,"","","",$values_query_locations);
$results=$db->query($query_locations['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $location_de = $row['location_de'];
    $location_fr = $row['location_fr'];
    print $location_de."<br />";
    print $location_fr."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$location_de;
    $location_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$location_fr;
    $location_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_locations="UPDATE ".TABLE_LOCATIONS." SET 
                location_search_1='".$location_search_de['text_lower_converted']."',
                location_search_2='".$location_search_de['text_lower_german_converted']."',
                location_search_3='".$location_search_fr['text_lower_converted']."'
        WHERE locationid='".$row['locationid']."' LIMIT 1 ";
    $db->query($query_update_locations);
    
    print $query_update_locations."<br /><br />";
}
//exit;

# END

### importing search for locations city

$values_query_locations_city=array();
//$query_where_locations_city=" WHERE cityid= ";
$query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city,"","","",$values_query_locations_city);
$results=$db->query($query_locations_city['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $city_de = $row['city_de'];
    $city_fr = $row['city_fr'];
    print $city_de."<br />";
    print $city_fr."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$city_de;
    $city_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$city_fr;
    $city_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_locations_city="UPDATE ".TABLE_LOCATIONS_CITY." SET 
                city_search_1='".$city_search_de['text_lower_converted']."',
                city_search_2='".$city_search_de['text_lower_german_converted']."',
                city_search_3='".$city_search_fr['text_lower_converted']."'
        WHERE cityid='".$row['cityid']."' LIMIT 1 ";
    $db->query($query_update_locations_city);
    
    print $query_update_locations_city."<br /><br />";
}
//exit;

# END

### importing search for locations country

$values_query_locations_country=array();
//$query_where_locations_country=" WHERE countryid= ";
$query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country,"","","",$values_query_locations_country);
$results=$db->query($query_locations_country['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $country_de = $row['country_de'];
    $country_fr = $row['country_fr'];
    print $country_de."<br />";
    print $country_fr."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$country_de;
    $country_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$country_fr;
    $country_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_locations_country="UPDATE ".TABLE_LOCATIONS_COUNTRY." SET 
                country_search_1='".$country_search_de['text_lower_converted']."',
                country_search_2='".$country_search_de['text_lower_german_converted']."',
                country_search_3='".$country_search_fr['text_lower_converted']."'
        WHERE countryid='".$row['countryid']."' LIMIT 1 ";
    $db->query($query_update_locations_country);
    
    print $query_update_locations_country."<br /><br />";
}
//exit;

# END


### importing search for literature

$values_query_literature=array();
//$query_where_literature=" WHERE id= ";
$query_literature=QUERIES::query_books($db,$query_where_literature,"","","",$values_query_literature);
$results=$db->query($query_literature['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $title = $row['title'];
    $author = $row['author'];
    $title_periodical = $row['title_periodical'];
    $info_search_de = $row['infoDE'];
    $info_search_fr = $row['infoFR'];
    $notes_search_de = $row['notesDE'];
    $notes_search_fr = $row['notesFR'];
    print $title."<br />";
    print $author."<br />";
    print $title_periodical."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$title;
    $title_search=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$author;
    $author_search=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$title_periodical;
    $title_periodical_search=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$info_search_de;
    $info_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$info_search_fr;
    $info_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$notes_search_de;
    $notes_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$notes_search_fr;
    $notes_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_literature="UPDATE ".TABLE_BOOKS." SET 
                title_search_1='".$title_search['text_lower_converted']."',
                title_search_2='".$title_search['text_lower_german_converted']."',
                author_search_1='".$author_search['text_lower_converted']."',
                author_search_2='".$author_search['text_lower_german_converted']."',
                title_periodical_search_1='".$title_periodical_search['text_lower_converted']."',
                title_periodical_search_2='".$title_periodical_search['text_lower_german_converted']."',
                info_search_1='".$info_search_de['text_lower_converted']."',
                info_search_2='".$info_search_de['text_lower_german_converted']."',
                info_search_3='".$info_search_fr['text_lower_converted']."',
                notes_search_1='".$notes_search_de['text_lower_converted']."',
                notes_search_2='".$notes_search_de['text_lower_german_converted']."',
                notes_search_3='".$notes_search_fr['text_lower_converted']."'
        WHERE id='".$row['id']."' LIMIT 1 ";
    $db->query($query_update_literature);
    
    print $query_update_literature."<br /><br />";
}
//exit;

# END

### importing search for video

$values_query_video=array();
//$query_where_video=" WHERE videoID= ";
$query_video=QUERIES::query_videos($db,$query_where_video,"","","",$values_query_video);
$results=$db->query($query_video['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $title_de = $row['titleDE'];
    $title_fr = $row['titleFR'];
    $name = $row['name'];
    $info_search_de = $row['infoDE'];
    $info_search_fr = $row['infoFR'];
    print $title_de."<br />";
    print $title_fr."<br />";
    print $name."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$title_de;
    $title_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$title_fr;
    $title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$name;
    $name_search=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$info_search_de;
    $info_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$info_search_fr;
    $info_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_video="UPDATE ".TABLE_VIDEO." SET 
                title_search_1='".$title_search_de['text_lower_converted']."',
                title_search_2='".$title_search_de['text_lower_german_converted']."',
                title_search_3='".$title_search_fr['text_lower_converted']."',
                name_search_1='".$name_search['text_lower_converted']."',
                name_search_2='".$name_search['text_lower_german_converted']."',
                info_search_1='".$info_search_de['text_lower_converted']."',
                info_search_2='".$info_search_de['text_lower_german_converted']."',
                info_search_3='".$info_search_fr['text_lower_converted']."'
        WHERE videoID='".$row['videoID']."' LIMIT 1 ";
    $db->query($query_update_video);
    
    print $query_update_video."<br /><br />";
}
//exit;

# END

### importing search for museum

$values_query_museum=array();
//$query_where_museum=" WHERE muID= ";
$query_museum=QUERIES::query_museums($db,$query_where_museum,"","","",$values_query_museum);
$results=$db->query($query_museum['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $museum = $row['museum'];
    print $museum."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$museum;
    $museum_search=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_museum="UPDATE ".TABLE_MUSEUM." SET 
                museum_search_1='".$museum_search['text_lower_converted']."',
                museum_search_2='".$museum_search['text_lower_german_converted']."'
        WHERE muID='".$row['muID']."' LIMIT 1 ";
    $db->query($query_update_museum);
    
    print $query_update_museum."<br /><br />";
}
//exit;

# END

### importing search for auctionHouse

$values_query_house=array();
//$values_query_house['where']=" WHERE ahID= ";
$query_house=QUERIES::query_auction_houses($db,$values_query_hous);
$results=$db->query($query_house['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $house = $row['house'];
    print $house."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$house;
    $house_search=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_house="UPDATE ".TABLE_AUCTIONHOUSE." SET 
                house_search_1='".$house_search['text_lower_converted']."',
                house_search_2='".$house_search['text_lower_german_converted']."'
        WHERE ahID='".$row['ahID']."' LIMIT 1 ";
    $db->query($query_update_house);
    
    print $query_update_house."<br /><br />";
}
//exit;

# END


### importing search for news

$values_query_news=array();
//$values_query_news['where']=" WHERE newsID= ";
$query_news=QUERIES::query_news($db,$values_query_hous);
$results=$db->query($query_news['query']);

while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);


    $title_de = $row['titleDE'];
    $title_fr = $row['titleFR'];
    $text_de = $row['textDE'];
    $text_fr = $row['textFR'];
    print $title_de."<br />";
    print $title_fr."<br />";

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$title_de;
    $title_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$title_fr;
    $title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_de;
    $text_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$text_fr;
    $text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $query_update_news="UPDATE ".TABLE_NEWS." SET 
                title_search_1='".$title_search_de['text_lower_converted']."',
                title_search_2='".$title_search_de['text_lower_german_converted']."',
                title_search_3='".$title_search_fr['text_lower_converted']."',
                text_search_1='".$text_search_de['text_lower_converted']."',
                text_search_2='".$text_search_de['text_lower_german_converted']."',
                text_search_3='".$text_search_fr['text_lower_converted']."'
        WHERE newsID='".$row['newsID']."' LIMIT 1 ";
    $db->query($query_update_news);
    
    print $query_update_news."<br /><br />";
}
exit;
*/
# END



/*
//$query="SELECT * FROM ".TABLE_BOOKS." WHERE id=181 ";
$query="SELECT * FROM ".TABLE_BOOKS." ";
$results=$db->query($query);
while( $row=$db->mysql_array($results,0) )
{
    $row=UTILS::html_decode($row);
    //$row=$db->filter_parameters($row);
    
    //print $row['title'];
    $title_search=$row['title'];
    //$title_search=utf8_decode($title_search);
    //$title_search=utf8_encode($title_search);
    $title_search=UTILS::replace_accented_letters($db, $title_search);
    print_r($title_search);

$query="UPDATE ".TABLE_BOOKS." SET 
    title_search1='".$title_search[0]."',
    title_search2='".$title_search[1]."',
    title_search3='".$title_search[4]."' ";

$query.=" WHERE id='".$row['id']."' ";

print $query."<br /><br />";
    $db->query($query);
}
exit;
*/
# re-create painting cache images with lighter bg
/*
//$query_where_installations=" WHERE installationid>24 ";
$query_installations=QUERIES::query_exhibitions_installations($db,$query_where_installations);
$results=$db->query($query_installations['query_without_limit']);
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=5 AND itemid2='".$row['installationid']."' ) OR ( typeid2=17 AND typeid1=5 AND itemid1='".$row['installationid']."' ) ORDER BY sort ASC, relationid DESC ");

    while( $row_related_image=$db->mysql_array($results_related_image))
    {
        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
        if( !empty($imageid) )
        {    
                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__bgcolor_ffffff.jpg";
                $src_cache_uri=$imageid.".jpg";
                $values_cache=array();
                $values_cache['cache_dir']="cache3";
                UTILS::cache_uri($src,$src_cache_uri,1,$values_cache);
                #end
            print "installationid-".$row['installationid'];
            print "imageid-".$imageid;
            print "<br />";
        }  
    }

}
exit;
 */
/*
# re-create painting cache images with lighter bg
$results=$db->query("SELECT paintID FROM ".TABLE_PAINTING." ");
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    if( !empty($imageid) )
    {    
                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__bgcolor_ffffff.jpg";
                $src_cache_uri=$imageid.".jpg";
                $values_cache=array();
                $values_cache['cache_dir']="cache3";
                UTILS::cache_uri($src,$src_cache_uri,1,$values_cache);
                #end
    }  

}
exit;
 */
/*
$results=$db->query("SELECT exID,title_original,locationid,cityid,countryid FROM ".TABLE_EXHIBITIONS."  ");
while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);
    $titleurl_en=UTILS::convert_fortitleurl($db,TABLE_EXHIBITIONS,$row['title_original'],"exID",$row['exID'],"titleurl_en","en");
    $query="UPDATE ".TABLE_EXHIBITIONS." SET 
            titleurl_en='".$titleurl_en."',
            titleurl_de='".$titleurl_en."',
            titleurl_fr='".$titleurl_en."',
            titleurl_ch='".$titleurl_en."'
        WHERE exID='".$row['exID']."' ";
    $db->query($query);

print $query."<br /><br />";

}
exit;
 */
/*
# resize images for exhibiton detail page exhibition poster
$results=$db->query("SELECT exID,locationid,cityid,countryid FROM ".TABLE_EXHIBITIONS."  ");
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    if( !empty($imageid) )
    {    

                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o__border_1__maxwidth_38__maxheight_45.jpg";
                $src_cache_uri=$imageid.".jpg";
                UTILS::cache_uri($src,$src_cache_uri,1);
                #end

    }  

}
exit;
*/
/*
    //$query_where_location=" WHERE locationid=6 ";
    $query_where_location=" WHERE locationdesearch1 LIKE '%&%' ";
    $query_location=QUERIES::query_locations($db,$query_where_location);
    $results_location=$db->query($query_location['query_without_limit']);
    $count=$db->numrows($results_location);
    while( $row=$db->mysql_array($results_location,0) )
    {
        //$row=UTILS::html_decode($row);
        //$row['location_de']=utf8_decode($row['location_de']);
        //$string=$row['location_de'];
        //$string = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $string);
        //$row['location_de']=html_entity_decode($string, ENT_COMPAT, "UTF-8");
        //print $string."<br />";
        //$row['location_de']=$string;

    $location_search_en=UTILS::replace_accented_letters($db, $row['location_en']);
    $location_search_de=UTILS::replace_accented_letters($db, $row['location_de']);
    $location_search_fr=UTILS::replace_accented_letters($db, $row['location_fr']);
    $location_search_ch=UTILS::replace_accented_letters($db, $row['location_ch']);

        # city info
        $query_where_city=" WHERE cityid='".$row['cityid']."' ";
        $query_city=QUERIES::query_locations_city($db,$query_where_city);
        $results_city=$db->query($query_city['query_without_limit']);
        $count_city=$db->numrows($results_city);
        $row_city=$db->mysql_array($results_city,0);
        $row_city=UTILS::html_decode($row_city);
        $city_search_en=UTILS::replace_accented_letters($db, $row_city['city_en']);
        $city_search_de=UTILS::replace_accented_letters($db, $row_city['city_de']);
        //print_r($city_search_de);
        $city_search_fr=UTILS::replace_accented_letters($db, $row_city['city_fr']);
        $city_search_ch=UTILS::replace_accented_letters($db, $row_city['city_ch']);
        if( $count_city )
        {
            $location_search_en[0].=" ".$city_search_en[0];
            $location_search_de[0].=" ".$city_search_de[0];
            $location_search_de[1].=" ".$city_search_de[1];
            $location_search_de[2].=" ".$city_search_de[2];
            $location_search_fr[0].=" ".$city_search_fr[0];
            $location_search_ch[0].=" ".$city_search_ch[0];
        }
        # end

        # country info
        $query_where_country=" WHERE countryid='".$row['countryid']."' ";
        $query_country=QUERIES::query_locations_country($db,$query_where_country);
        $results_country=$db->query($query_country['query_without_limit']);
        $count_country=$db->numrows($results_country);
        $row_country=$db->mysql_array($results_country,0);
        $row_country=UTILS::html_decode($row_country);
        $country_search_en=UTILS::replace_accented_letters($db, $row_country['country_en']);
        $country_search_de=UTILS::replace_accented_letters($db, $row_country['country_de']);
        $country_search_fr=UTILS::replace_accented_letters($db, $row_country['country_fr']);
        $country_search_ch=UTILS::replace_accented_letters($db, $row_country['country_ch']);
        if( $count_country )
        {
            $location_search_en[0].=" ".$country_search_en[0];
            $location_search_de[0].=" ".$country_search_de[0];
            $location_search_de[1].=" ".$country_search_de[1];
            $location_search_de[2].=" ".$country_search_de[2];
            $location_search_fr[0].=" ".$country_search_fr[0];
            $location_search_ch[0].=" ".$country_search_ch[0];
        }
        # end
    
        $query="UPDATE ".TABLE_LOCATIONS." SET
                    location_search_en_1='".$location_search_en[0]."',
                    locationdesearch1='".$location_search_de[0]."',
                    locationdesearch2='".$location_search_de[1]."',
                    locationdesearch3='".$location_search_de[2]."',
                    location_search_fr_1='".$location_search_fr[0]."',
                    location_search_ch_1='".$location_search_ch[0]."'
                WHERE locationid='".$row['locationid']."' ";

         $query_city="UPDATE ".TABLE_LOCATIONS_CITY." SET
                    citydesearch1='".$city_search_de[0]."',
                    citydesearch2='".$city_search_de[1]."',
                    citydesearch3='".$city_search_de[2]."',
                    city_search_fr_1='".$city_search_fr[0]."'
                WHERE cityid='".$row['cityid']."'";
         $query_country="UPDATE ".TABLE_LOCATIONS_COUNTRY." SET
                    countrydesearch1='".$country_search_de[0]."',
                    countrydesearch2='".$country_search_de[1]."',
                    countrydesearch3='".$country_search_de[2]."',
                    country_search_fr_1='".$country_search_fr[0]."'
                WHERE countryid='".$row['countryid']."'";


        //print $query."<br /><br />";
        //print $query_city."<br /><br />";
        //print $query_country."<br /><br />";
        //$db->query($query);
        //$db->query($query_city);
        //$db->query($query_country);

print "<a href='/admin/locations/edit/?typeid=1&locationid=".$row['locationid']."' title=''>".$row['locationid']."</a><br />";

    }


exit;
*/

/*
exit;
$results=$db->query("SELECT exID,locationid,cityid,countryid FROM ".TABLE_EXHIBITIONS."  ");
while( $row=$db->mysql_array($results) )
{
    $row=$db->filter_parameters($row,1);

    $query_where_locations=" WHERE locationid='".$row['locationid']."' AND cityid='".$row['cityid']."' AND countryid='".$row['countryid']."' AND locationid!=0 AND locationid!='' AND locationid IS NOT NULL ";
    $query_locations=QUERIES::query_locations($db,$query_where_locations,$query_order,$query_limit,$values_query);
    
    $results_loc=$db->query($query_locations['query_without_limit']);
    $count_loc=$db->numrows($results_loc);

    if( !$count_loc )
{
    print "<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>exhibitionid - ".$row['exID']."</a><br />\n";

    print "<br /><br /><hr />";
}
}
exit;
 */


/*
$results=$db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mediumDE LIKE '%Wasserfarbe%' AND mediumDE NOT LIKE '%Wasserfarben%'  ");
//$results=$db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mediumDE LIKE '%Wasserfarben%' ");
$count=$db->numrows($results);
print $count."<br />";
while( $row=$db->mysql_array($results) )
{
    //$row=$db->filter_parameters($row,1);
    //$row=UTILS::html_decode($row);
    print "<a href='/admin/medium/edit/?mediumid=".$row['mID']."'>".$row['mID']."</a><br />";
    print $row['mediumDE']."<br />";
    $new_medium_de=str_ireplace("Wasserfarbe","Wasserfarben", $row['mediumDE']);
    //$new_medium_de=$row['mediumDE'];
    print $new_medium_de;
    print "<hr /><br />";
    //$db->query("UPDATE ".TABLE_MEDIA." SET mediumDE='".$new_medium_de."' WHERE mID='".$row['mID']."' LIMIT 1 ");
}
 */

/*
$db->query("UPDATE ".TABLE_PAINTING." SET mID='63' WHERE mID='33'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='261' WHERE mID='257'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='197' WHERE mID='198'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='210' WHERE mID='223'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='187' WHERE mID='188'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='187' WHERE mID='194'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='187' WHERE mID='196'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='187' WHERE mID='203'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='191' WHERE mID='208'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='191' WHERE mID='216'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='166' WHERE mID='283'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='166' WHERE mID='184'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='166' WHERE mID='194'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='209' WHERE mID='212'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='151' WHERE mID='124'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='158' WHERE mID='39'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='1' WHERE mID='14'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='134' WHERE mID='179'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='27' WHERE mID='38'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='182' WHERE mID='185'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='182' WHERE mID='186'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='182' WHERE mID='199'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='107' WHERE mID='148'  ");
$db->query("UPDATE ".TABLE_PAINTING." SET mID='180' WHERE mID='171'  ");
 */

# EXHIITION location impots
/*
        $query_order=" ORDER BY locationid DESC ";
        $query_locations=QUERIES::query_locations($db,$query_where_locations,$query_order,$query_limit);
        $results=$db->query($query_locations['query_without_limit']);
        while( $row=$db->mysql_array($results) )
        {
            $row=$db->filter_parameters($row,1);
            $location_en=str_replace("  ", " ", $row['location_en']);
            $location_de=str_replace("  ", " ", $row['location_de']);
            $location_fr=str_replace("  ", " ", $row['location_fr']);
            $location_ch=str_replace("  ", " ", $row['location_ch']);
            
        $location_search_de=UTILS::replace_accented_letters($db, $location_de);
        $location_search_fr=UTILS::replace_accented_letters($db, $location_fr);

        $query="UPDATE ".TABLE_LOCATIONS." SET 
                        location_en='".$location_en."',
                        location_de='".$location_de."',
                        location_fr='".$location_fr."',
                        location_ch='".$location_ch."',
                        locationdesearch1='".$location_search_de[0]."',
                        locationdesearch2='".$location_search_de[1]."',
                        locationdesearch3='".$location_search_de[2]."',
                        location_search_fr_1='".$location_search_fr[0]."'
          WHERE locationid='".$row['locationid']."' ";

            $db->query($query);

        }
*/
/*
exit;

    function get_location($db,$values) 
    {
        $tmp=explode(",", $values['location']);
        if( is_array($tmp) )
        {
            krsort($tmp);
            //print_r($tmp);
            $i=0;
            $location="";
            foreach( $tmp as $value )
            {
                if( $i==0 ) $country=$value;
                elseif( $i==1 ) $city=$value;
                else 
                {
                    if( !empty($location) ) $value.=", ";
                    $location=$value.$location;
                }
                $i++;
            }


            $location="|||".$location."|||";
            $location=str_replace('||| ', "", $location);
            $location=str_replace(' |||', "", $location);
            $location=str_replace('|||', "", $location);
            $city="|||".$city."|||";
            $city=str_replace('||| ', "", $city);
            $city=str_replace(' |||', "", $city);
            $city=str_replace('|||', "", $city);
            $country="|||".$country."|||";
            $country=str_replace('||| ', "", $country);
            $country=str_replace(' |||', "", $country);
            $country=str_replace('|||', "", $country);
            print "<p style='color:red;margin:0 0 0 20px;padding:0;'>location - |".$location."|</p>\n";
            print "<p style='color:green;margin:0 0 0 20px;padding:0;'>city - |".$city."|</p>\n";
            print "<p style='color:blue;margin:0 0 0 20px;padding:0;'>country - |".$country."|</p>\n";
            $location_array['location']=$location;
            $location_array['city']=$city;
            $location_array['country']=$country;
        }
        return $location_array;
    }



$results=$db->query("SELECT exID,locationEN,locationDE,locationFR,locationCH FROM ".TABLE_EXHIBITIONS."  ");
while( $row=$db->mysql_array($results) )
{
    //$row=UTILS::html_decode($row);
    //$row=$db->db_prepare_input($row);
    $row=$db->filter_parameters($row,1);

    $tmp=explode(",", $row['locationEN']);
    //if( count($tmp)<=2 )
    //{
    print "<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>exhibitionid - ".$row['exID']."</a><br />\n";
    print "<strong>location EN - ".$row['locationEN']."</strong><br />\n";
        # location EN
        $values=array();
        $values['location']=$row['locationEN'];
        $location_en=get_location($db,$values);
    print "<strong>location DE - ".$row['locationDE']."</strong><br />\n";
        # location DE
        $values=array();
        $values['location']=$row['locationDE'];
        $location_de=get_location($db,$values);
    print "<strong>location FR - ".$row['locationFR']."</strong><br />\n";
        # location FR
        $values=array();
        $values['location']=$row['locationFR'];
        $location_fr=get_location($db,$values);
    print "<strong>location CH - ".$row['locationCH']."</strong><br />\n";
        # location CH
        $values=array();
        $values['location']=$row['locationCH'];
        $location_ch=get_location($db,$values);



#country
if( !empty($location_en['country']) )
{
    $query_where_country=" WHERE country_en='".$location_en['country']."' ";
    $query_country=QUERIES::query_locations_country($db,$query_where_country);
    $results_country=$db->query($query_country['query_without_limit']);
    $count_country=$db->numrows($results_country);
    $row_country=$db->mysql_array($results_country);
    $countryid=$row_country['countryid'];
    if( $count_country<=0 ) 
    {
        $location_search_de=UTILS::replace_accented_letters($db, $location_de['country']);
        $location_search_fr=UTILS::replace_accented_letters($db, $location_fr['country']);
        $db->query("INSERT INTO ".TABLE_LOCATIONS_COUNTRY."(
                        country_en,
                        country_de,
                        country_fr,
                        country_ch,
                        countrydesearch1,
                        countrydesearch2,
                        countrydesearch3,
                        country_search_fr_1,
                        date_created
                    ) VALUES(
                        '".$location_en['country']."',
                        '".$location_de['country']."',
                        '".$location_fr['country']."',
                        '".$location_ch['country']."',
                        '".$location_search_de[0]."',
                        '".$location_search_de[1]."',
                        '".$location_search_de[2]."',
                        '".$location_search_fr[0]."',
                        NOW()
        ) ");
        $countryid=$db->return_insert_id();
        print "<strong class='valid'>COUNTRY - INSERT - <a href='/admin/locations/edit/?typeid=3&countryid=".$countryid."'>".$countryid."</a></strong><br />\n";
    }
    else
    {
        print "<strong style='color:orange;'>COUNTRY - EXISTING - <a href='/admin/locations/edit/?typeid=3&countryid=".$countryid."'>".$countryid."</a></strong><br />\n";
    }
}



#city
if( !empty($location_en['city']) )
{
    $query_where_city=" WHERE city_en='".$location_en['city']."' ";
    $query_city=QUERIES::query_locations_city($db,$query_where_city);
    $results_city=$db->query($query_city['query_without_limit']);
    $count_city=$db->numrows($results_city);
    $row_city=$db->mysql_array($results_city);
    $cityid=$row_city['cityid'];
    if( !$count_city ) 
    {
        $location_search_de=UTILS::replace_accented_letters($db, $location_de['city']);
        $location_search_fr=UTILS::replace_accented_letters($db, $location_fr['city']);
        $db->query("INSERT INTO ".TABLE_LOCATIONS_CITY."(
                        countryid,
                        city_en,
                        city_de,
                        city_fr,
                        city_ch,
                        citydesearch1,
                        citydesearch2,
                        citydesearch3,
                        city_search_fr_1,
                        date_created
                    ) VALUES(
                        '".$countryid."',
                        '".$location_en['city']."',
                        '".$location_de['city']."',
                        '".$location_fr['city']."',
                        '".$location_ch['city']."',
                        '".$location_search_de[0]."',
                        '".$location_search_de[1]."',
                        '".$location_search_de[2]."',
                        '".$location_search_fr[0]."',
                        NOW()
        ) ");
        $cityid=$db->return_insert_id();
        print "<strong class='valid'>CITY - INSERT - <a href='/admin/locations/edit/?typeid=2&cityid=".$cityid."'>".$cityid."</a></strong><br />\n";
    }
    else
    {
        print "<strong style='color:orange;'>CITY - EXISTING - <a href='/admin/locations/edit/?typeid=2&cityid=".$cityid."'>".$cityid."</a></strong><br />\n";
    }
}



#location
if( !empty($location_en['location']) )
{
    $query_where_location=" WHERE location_en='".$location_en['location']."' ";
    $query_location=QUERIES::query_locations($db,$query_where_location);
    $results_location=$db->query($query_location['query_without_limit']);
    $count_location=$db->numrows($results_location);
    $row_location=$db->mysql_array($results_location);
    $locationid=$row_location['locationid'];
    if( !$count_location ) 
    {
        $location_search_de=UTILS::replace_accented_letters($db, $location_de['location']);
        $location_search_fr=UTILS::replace_accented_letters($db, $location_fr['location']);
        $db->query("INSERT INTO ".TABLE_LOCATIONS."(
                        countryid,
                        cityid,
                        location_en,
                        location_de,
                        location_fr,
                        location_ch,
                        locationdesearch1,
                        locationdesearch2,
                        locationdesearch3,
                        location_search_fr_1,
                        date_created
                    ) VALUES(
                        '".$countryid."',
                        '".$cityid."',
                        '".$location_en['location']."',
                        '".$location_de['location']."',
                        '".$location_fr['location']."',
                        '".$location_ch['location']."',
                        '".$location_search_de[0]."',
                        '".$location_search_de[1]."',
                        '".$location_search_de[2]."',
                        '".$location_search_fr[0]."',
                        NOW()
        ) ");
        $locationid=$db->return_insert_id();
        print "<strong class='valid'>LOCATION - INSERT - <a href='/admin/locations/edit/?typeid=1&locationid=".$locationid."'>".$locationid."</a></strong><br />\n";
    }
    else
    {
        print "<strong style='color:orange;'>LOCATION - EXISTING - <a href='/admin/locations/edit/?typeid=1&locationid=".$locationid."'>".$locationid."</a></strong><br />\n";
    }
}

    $query_exh_update="UPDATE ".TABLE_EXHIBITIONS." SET locationid='".$locationid."', cityid='".$cityid."', countryid='".$countryid."' WHERE exID='".$row['exID']."' ";
    $db->query($query_exh_update);
    print "<br />".$query_exh_update."<br />";

    print "<br /><br /><hr />";
    //}
}
#END EXHIITION location impots

exit;
 */
/*
$query="SELECT * FROM ".TABLE_BOOKS;
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{   
    if( empty($row['coverid']) ) $coverid="NULL";
    else
    {
        if( $row['coverid']=="Hardback" ) $coverid=1;
        elseif( $row['coverid']=="Softcover" ) $coverid=2;
        elseif( $row['coverid']=="Unknown binding" ) $coverid=3;
        elseif( $row['coverid']=="Special binding" ) $coverid=4;
    }
            $query="UPDATE ".TABLE_BOOKS." SET coverid=".$coverid." WHERE id='".$row['id']."' LIMIT 1 ";
            print $query."<br /><br />";
}
*/


/*
$query="SELECT * FROM ".TABLE_BOOKS;
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{   
    if( !empty($row['title_periodical']) )
    {
    $row=UTILS::html_decode($row);
    $row['title_periodical']=str_replace('"', "", $row['title_periodical']);
    $row['title_periodical']=str_replace("'", "", $row['title_periodical']);   
    $title_periodical_search=UTILS::replace_accented_letters($db, $row['title_periodical']);
    
            $query="UPDATE ".TABLE_BOOKS." SET 
    title_periodical_search1='".$title_periodical_search[0]."',
    title_periodical_search2='".$title_periodical_search[1]."',
    title_periodical_search3='".$title_periodical_search[4]."' 
                        WHERE id='".$row['id']."' LIMIT 1 ";
            $db->query($query); 
    print $query."<br /><br />";
    }
}
 */

/*
$query="SELECT saleID FROM ".TABLE_SALEHISTORY." ";
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{
    $saleid=$row['saleID'];
    $results_sale_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 from ".TABLE_RELATIONS." WHERE ( typeid1=2 AND itemid1='".$saleid."' AND typeid2=1 ) OR ( typeid2=2 AND itemid2='".$saleid."' AND typeid1=1 ) ");
    $count_rel=$db->numrows($results_sale_relations);
    if( $count_rel )
    {
        while( $row_sale_relations=$db->mysql_array($results_sale_relations) )
        {    
            $paintid=UTILS::get_relation_id($db,"1",$row_sale_relations);
            print $saleid."-".$paintid."<br />";
            $db->query("UPDATE ".TABLE_SALEHISTORY." SET paintID='".$paintid."' WHERE saleID='".$saleid."' ");
        } 
    }
    else print $saleid."-not found <br />";
}

exit;
*/
/*
$query="SELECT paintID,keywords FROM ".TABLE_PAINTING." where catID=92 and artworkID=6 ";
    $results=$db->query($query);
while( $row=$db->mysql_array($results) )
{


    //$db->query("UPDATE ".TABLE_PAINTING." SET keywords='Museum Visit' WHERE paintID='".$row['paintID']."' LIMIT 1 ");
    //print $row['paintID']."-".strip_tags($row['keywords'])."<br />\n";

}
exit;
 */
/*
$query="SELECT * FROM ".TABLE_BOOKS;
    $results=$db->query($query);
while( $row=$db->mysql_array($results) )
{   
    $field="infoCH";
        if( $row[$field]=="<br />\n" || $row[$field]=="<br />" || $row[$field]==addslashes("<br />") || $row[$field]=="<br/>" || $row[$field]=="<br>" )
        {
            $db->query("UPDATE ".TABLE_BOOKS." SET ".$field."=NULL WHERE id='".$row['id']."' LIMIT 1 ");
            print "<span style='color:green;'>".$row['id']." - |".$row[$field]."|</span><br /><br />\n\n";
        }
        else
        {
            //print "<span style='color:red;'>".$row['id']." - |".$row[$field]."|</span><br /><br />\n\n";
        }
}
 
exit;
 */
/*
$query="SELECT * FROM ".TABLE_PAINTING." WHERE editionID='12813' ORDER BY sort ASC ";
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
 */
    # create cache image
    /*
    $values=array();
    $values['cache_dir']="cache_microsites";
    $src="/images/imageid_".$imageid."__thumb_1__size_o__maxwidth_146__maxheight_102__quality_100.jpg";
    $src_cache_uri=$imageid.".jpg";
    UTILS::cache_uri($src,$src_cache_uri,1,$values);
     */
    #end

    //print "<img src='".$src."' alt='' /><br />";

    /*
    $image_src=DATA_PATH."/images_new/xlarge_snowwhite/".$imageid.".jpg";
    $image_src_new=DATA_PATH."/test/".$imageid.".jpg";
    if (!copy($image_src, $image_src_new)) exit("Error file copy!");
     */
//}

/*
$query="SELECT newsID FROM ".TABLE_NEWS." ";
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{
    $newsid=$row['newsID'];
    $results_sale_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 from ".TABLE_RELATIONS." WHERE ( typeid1=7 AND itemid1='".$newsid."' AND typeid2=4 ) OR ( typeid2=7 AND itemid2='".$newsid."' AND typeid1=4 ) ");
    $count_rel=$db->numrows($results_sale_relations);
    if( $count_rel )
    {
        $row_news_relations=$db->mysql_array($results_sale_relations);
        $exhibitionid=UTILS::get_relation_id($db,"4",$row_news_relations);
        print $newsid."-".$exhibitionid."<br />";
        $db->query("UPDATE ".TABLE_EXHIBITIONS." SET newsid='".$newsid."' WHERE exID='".$exhibitionid."' ");
    }
    else print $newsid."- 0 exh found <br />";
}
*/


# GERMAN LANg SEARCH museums
/*
$results=$db->query("SELECT muID,museum FROM ".TABLE_MUSEUM." ");

while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);
    $row=$db->db_prepare_input($row);

    $row['museum']=str_replace('"', "", $row['museum']);
    $row['museum']=str_replace("'", "", $row['museum']);

    $museum_search=UTILS::replace_accented_letters($db, $row['museum']);

    $query="UPDATE ".TABLE_MUSEUM." SET museumDEsearch1='".$museum_search['0']."', museumDEsearch2='".$museum_search[1]."' WHERE muID='".$row['muID']."' ";
    $db->query($query);
    print $query."<br /><br />";
}
 */
# END GERMAN LANg SEARCH museums



/*
$query="SELECT p.paintID AS paintid,p.artworkID,p.titleEN,p.number,DATE_FORMAT(p.date, '%Y-%m-%d') AS date,p.year,p.sort AS psort,a.sort AS asort FROM ".TABLE_PAINTING." p, ".TABLE_ARTWORKS." a WHERE p.artworkID=a.artworkID ";
$query.=" AND ( p.paintID=16951 OR p.paintID=14777 ) ";
$query.=" ORDER BY p.sort ASC, p.titleEN ASC ";
//$query.=" LIMIT 5 ";
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{

    $values=array();
    if( empty($row['number']) ) 
    {   
        #checking if date is not empty
        if( !empty($row['date']) && $row['date']!="0000-00-00" ) $values['date']=$row['date'];
        elseif( !empty($row['year']) ) $values['year']=$row['year'];
    }   
    else $values['number']=$row['number'];
    $values['year_drawings']=$row['year'];
    $row_artwork=UTILS::get_artwork_info($db,$row['artworkID']);
    if( $row_artwork['artworkID']==1 || $row_artwork['artworkID']==2 || $row_artwork['artworkID']==13 ) $values['artwork_sort']=1;
    else $values['artwork_sort']=$row_artwork['sort'];
    $values['artworkid']=$row_artwork['artworkID'];
    $values['title_en']=$row['titleEN'];
    $values['paintid']=$row['paintid'];
    $sort=UTILS::painting_sort_number($db,$values);

    print "paintID - <a href='/admin/paintings/edit/?paintid=".$row['paintid']."' target='_blank'>".$row['paintid']."</a><br />\n";
    if( !empty($row['titleEN']) ) print "<span style='color:orange;'>titleEN - ".$row['titleEN']."</span><br />\n";
    if( !empty($row['number']) ) print "<span style='color:red;'>number - ".$row['number']."</span><br />\n";
    if( !empty($values['date']) && $values['date']!="0000-00-00" ) print "<span style='color:green;'>date - ".$values['date']."</span><br />\n";
    if( !empty($values['year']) ) print "<span style='color:yellow;'>year - ".$values['year']."</span><br />\n";
    print "sort - ".$row['psort']."<br />\n";
    print "new sort - ".$sort."<br /><br />\n";
    //$db->query("UPDATE ".TABLE_PAINTING." SET sort2='".$sort."' WHERE paintID='".$row['paintid']."' LIMIT 1 ");
    unset($row);
}
 */
exit; 

function month_replace1($date)
{
    $date=str_replace("January",".01.", $date);
    $date=str_replace("Jan",".01.", $date);
    $date=str_replace("February",".02.", $date);
    $date=str_replace("Feb",".02.", $date);
    $date=str_replace("March",".03.", $date);
    $date=str_replace("Mar",".03.", $date);
    $date=str_replace("April",".04.", $date);
    $date=str_replace("Apr",".04.", $date);
    $date=str_replace("May",".05.", $date);
    $date=str_replace("June",".06.", $date);
    $date=str_replace("Jun",".06.", $date);
    $date=str_replace("July",".07.", $date);
    $date=str_replace("Jul",".07.", $date);
    $date=str_replace("August",".08.", $date);
    $date=str_replace("Aug",".08.", $date);
    $date=str_replace("September",".09.", $date);
    $date=str_replace("Sept",".09.", $date);
    $date=str_replace("Sep",".09.", $date);
    $date=str_replace("October",".10.", $date);
    $date=str_replace("Oct",".10.", $date);
    $date=str_replace("Okt",".10.", $date);
    $date=str_replace("November",".11.", $date);
    $date=str_replace("Nov",".11.", $date);
    $date=str_replace("December",".12.", $date);
    $date=str_replace("Dec",".12.", $date);
    return $date;
}

function month_replace($date)
{
    $date=month_replace1($date);
    /*
    $date=str_replace("Jan","01", $date);
    $date=str_replace("Feb","02", $date);
    $date=str_replace("March","03", $date);
    $date=str_replace("Mar","03", $date);
    $date=str_replace("April","04", $date);
    $date=str_replace("Apr","04", $date);
    $date=str_replace("May","05", $date);
    $date=str_replace("June","06", $date);
    $date=str_replace("Jun","06", $date);
    $date=str_replace("July","07", $date);
    $date=str_replace("Jul","07", $date);
    $date=str_replace("August","08", $date);
    $date=str_replace("Aug","08", $date);
    $date=str_replace("Sept","09", $date);
    $date=str_replace("Sep","09", $date);
    $date=str_replace("Oct","10", $date);
    $date=str_replace("Okt","10", $date);
    $date=str_replace("November","11", $date);
    $date=str_replace("Nov","11", $date);
    $date=str_replace("Dec","12", $date);
     */
    return $date;
}
/*
$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=5  ";
//$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=6  ";
//$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=8  ";
$results=$db->query($query);
while( $row=$db->mysql_array($results) )
{
    $title_en=$row['titleEN'];
    $date_array1_1=explode("(",$title_en);
    $count1=count($date_array1_1);
    $tmp=$date_array1_1[$count1-1];
    $date_array1_2=explode(")",$tmp);
    $date_cutted=str_replace(" ","",$date_array1_2[0]);
    $date_cutted=str_replace("Houses","",$date_cutted);
    $date_cutted=str_replace("BR","",$date_cutted);
    $date_cutted=str_replace("-",".",$date_cutted);
$date_cutted=month_replace($date_cutted);
    $date_cutted=str_replace("..",".",$date_cutted);
    $date_cutted=str_replace("'","",$date_cutted);

    # type 1
    $date_type1_array=explode(".",$date_cutted);
    $date_type1=(int)$date_type1_array[0];
    $month_type1=(int)$date_type1_array[1];
    if( $date_type1_array[2]=="00") $year_type1="20".$year_type1;
    else $year_type1=(int)$date_type1_array[2];
    if( strlen($year_type1)==1 ) $year_type1="200".$year_type1;
    elseif( strlen($year_type1)==2 ) $year_type1="19".$year_type1;
    if( $year_type1=="202000" ) $year_type1="2000";
    if( !empty($date_type1) && !empty($month_type1) && !empty($year_type1) )
    {
        $day=$date_type1;
        $month=$month_type1;
        $year=$year_type1;
    }
    # END type 1


    $valid=0;
    if( !empty($day) && !empty($month) && !empty($year) )
    {
        $style="background-color:green;";
        $valid=1;
    }
    else 
    {
        $style="background-color:red;"; 
        $valid=0;
    }

        print "<div style='".$style."'>";
        print "PaintID - <a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'><u>".$row['paintID']."</u></a><br />";
        print "Title - ".$title_en."<br />";
        print "Date from title - ".$date_cutted."<br />";
        print "Day - ".$day."<br />";
        print "Month - ".$month."<br />";
        print "Year - ".$year."<br />";
        $date=$year."-".$month."-".$day;
        print "DATE - <strong>".$date."</strong><br />";
        print "</div>";
        print "<br />";

    if( $valid ) $db->query("UPDATE ".TABLE_PAINTING." SET date='".$date."' WHERE paintID='".$row['paintID']."' LIMIT 1 ");

    $date="";
    $day="";
    $month="";
    $year="";
}
*/


//$html->foot();
?>
