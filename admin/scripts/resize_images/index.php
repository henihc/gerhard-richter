<?php
ini_set('display_errors','1');
ini_set('display_startup_errors','1');
error_reporting (E_ALL);
set_time_limit(0);

require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/tooltip.js";
$html->js[]="/js/common.js";
$html->head('admin');

$db=new dbCLASS;


$artwork_dirs=UTILS::fileTree(DATA_PATH."/images");

foreach( $artwork_dirs as $dir_artwork )
{
	$artwork_path=DATA_PATH."/images/".$dir_artwork;
	if( $dir_artwork!=".DS_Store" && $dir_artwork!="paintings" && $dir_artwork!="other" && $dir_artwork!="works"  )
    {
        //print $dir_artwork;
	$artwork_path.="/original";
	$files=UTILS::fileTree($artwork_path);

		foreach( $files as $file )
        {
        $path=$artwork_path."/".$file;
        
        
	    	if( $dir_artwork=="books" )
            {
            //print $path."<br />";
	    	//UTILS::resize_image($path,$file,THUMB_XS_HEIGHT_BOOKS,THUMB_XS_WIDTH_BOOKS,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_S_HEIGHT_BOOKS,THUMB_S_WIDTH_BOOKS,DATA_PATH."/images/".$dir_artwork."/small/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_M_HEIGHT_BOOKS,THUMB_M_WIDTH_BOOKS,DATA_PATH."/images/"".$dir_artwork."/medium/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_L_HEIGHT_BOOKS,THUMB_L_WIDTH_BOOKS,DATA_PATH."/images/".$dir_artwork."/large/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_XL_HEIGHT_BOOKS,THUMB_XL_WIDTH_BOOKS,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
	    	}
	    	
	    ######## there is new folder paintings in which I moved photo_paintings and abstracts so before launch this check ho wit works	
	
        	
    	    elseif( $dir_artwork=="atlas" )
	    	{
            //print $path."<br />";    
            //UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,DATA_PATH."/images/".$dir_artwork."/llarge/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
	    	}  
	    	
	    	elseif( $dir_artwork=="photos" )
	    	{
	    	//print $path."<br />";
	    	//UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_LLL_HEIGHT,THUMB_LLL_WIDTH,DATA_PATH."/images/".$dir_artwork."/lllarge/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
	    	}    

            elseif( $dir_artwork=="exhibitions" )
            {
            //print $path."<br />";
            //UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
            //UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
            //UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
            //UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
            }

            elseif( $dir_artwork=="editions" )
            {
            //print $path."<br />";
            //UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
            //UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
            //UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
            //UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
            //UTILS::resize_image($path,$file,SIZE_WARCUT_HEIGHT,SIZE_WARCUT_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge_warcut/".$file);
            //UTILS::resize_image($path,$file,SIZE_SNOWWHITE_HEIGHT,SIZE_SNOWWHITE_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge_snowwhite/".$file);
            //UTILS::resize_image($path,$file,SIZE_FLORENCE_HEIGHT,SIZE_FLORENCE_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge_florence/".$file);
            }
            elseif( $dir_artwork=="no_cr" )
            {
            //print $path."<br />";
            //UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
            //UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
            //UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
            //UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_LL_HEIGHT,THUMB_LL_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
            //UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
            }

            else
	    	{
	    	//UTILS::resize_image($path,$file,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,DATA_PATH."/images/".$dir_artwork."/xsmall/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_S_HEIGHT,THUMB_S_WIDTH,DATA_PATH."/images/".$dir_artwork."/small/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_M_HEIGHT,THUMB_M_WIDTH,DATA_PATH."/images/".$dir_artwork."/medium/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_L_HEIGHT,THUMB_L_WIDTH,DATA_PATH."/images/".$dir_artwork."/large/".$file);
	    	//UTILS::resize_image($path,$file,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,DATA_PATH."/images/".$dir_artwork."/xlarge/".$file);
	    	}    
        list($width, $height, $type, $attr) = getimagesize($path);
       	if( $width1<$width ) $width1=$width;
       	if( $height1<$height ) $height1=$height;
        
        
        
        }
				
	
	}
}
 
print "max_Width-".$width1."<br />";
print "max_Height-".$height1."<br />";
/*
	    	print "
	    	<p>
	    	<strong>Resize BOOK images:</strong>
	    	<ol>
	    	<li>1. xsmall - ".THUMB_XS_HEIGHT_BOOKS." x ".THUMB_XS_WIDTH_BOOKS."</li>
	    	<li>2. small - ".THUMB_S_HEIGHT_BOOKS." x ".THUMB_S_WIDTH_BOOKS."</li>
	    	<li>3. medium - ".THUMB_M_HEIGHT_BOOKS." x ".THUMB_M_WIDTH_BOOKS."</li>
	    	<li>4. large - ".THUMB_L_HEIGHT_BOOKS." x ".THUMB_L_WIDTH_BOOKS."</li>
	    	<li>5. xlarge - ".THUMB_XL_HEIGHT_BOOKS." x ".THUMB_XL_WIDTH_BOOKS."</li>
	    	</ol>	
	    	</p>
	    	";
 */

	    	print "
	    	<br />
	    	<p>
	    	<strong>Resize ARTWORK images:</strong>
	    	<ol>
	    	<li>1. xsmall - ".THUMB_XS_HEIGHT." x ".THUMB_XS_WIDTH."</li>
	    	<li>2. small - ".THUMB_S_HEIGHT." x ".THUMB_S_WIDTH."</li>
	    	<li>3. medium - ".THUMB_M_HEIGHT." x ".THUMB_M_WIDTH."</li>
	    	<li>4. large - ".THUMB_L_HEIGHT." x ".THUMB_L_WIDTH."</li>
	    	<li>5. xlarge - ".THUMB_XL_HEIGHT." x ".THUMB_XL_WIDTH."</li>
	    	</ol>	
	    	</p>
	    	";




$html->foot();
?>
