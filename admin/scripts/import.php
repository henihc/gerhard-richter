<?php
header("Content-Type: text/html; charset=utf-8");
//header("Content-Type: text/plain");
set_time_limit(0);
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

//$html = new html_elements;
//$html->head('artwork');

$db=new dbCLASS;

$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=2 AND number LIKE '839-%' AND number != '839-1' AND number != '839-2' ";
//$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=2 AND number='839-2' ";
//$query.=" WHERE paintID=4735";
$query_order=" ORDER BY sort2 ASC ";
//$query_limit=" LIMIT 0,10 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";


function add_relation_literature($db,$relationid,$relationid_lit1)
{

	$query_lit_rel="SELECT * FROM ".TABLE_RELATIONS_LITERATURE." WHERE relationid='".$relationid_lit1."' ";
	$results_lit_rel=$db->query($query_lit_rel);
	$row_lit_rel=$db->mysql_array($results_lit_rel);

   if( !empty($relationid) && ( !empty($row_lit_rel['illustrated']) || !empty($row_lit_rel['illustrated_bw']) || !empty($row_lit_rel['mentioned']) || !empty($row_lit_rel['discussed']) || !empty($row_lit_rel['show_illustrated']) || !empty($row_lit_rel['show_bw']) || !empty($row_lit_rel['show_mentioned']) || !empty($row_lit_rel['show_discussed']) ) )
    {
        $db->query("INSERT INTO ".TABLE_RELATIONS_LITERATURE."(relationid,illustrated,illustrated_bw,show_illustrated,show_bw,mentioned,show_mentioned,discussed,show_discussed,date_created) VALUES('".$relationid['relationid']."','".$row_lit_rel['illustrated']."','".$row_lit_rel['illustrated_bw']."','".$row_lit_rel['show_illustrated']."','".$row_lit_rel['show_bw']."','".$row_lit_rel['mentioned']."','".$row_lit_rel['show_mentioned']."','".$row_lit_rel['discussed']."','".$row_lit_rel['show_discussed']."',NOW()) ");

        $relationid_lit=$db->return_insert_id();
        $options_relation_lit=array();
        $options_relation_lit['relationid']=$relationid['relationid'];
        admin_utils::order_literature_pages($db,$relationid_lit,$options_relation_lit);

        admin_utils::admin_log($db,1,10,$relationid['relationid']);
    }
}


while( $row=$db->mysql_array($results) )
{

    //$query_update="DELETE FROM ".TABLE_PAINTING." ";
    //$query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";


$titleen=str_replace("  "," ",$row['titleEN']);
$titlede=str_replace("  "," ",$row['titleDE']);
$titlefr=str_replace("  "," ",$row['titleFR']);
$titleit=str_replace("  "," ",$row['titleIT']);
$titlezh=str_replace("  "," ",$row['titleZH']);
/*
$titleen=str_replace(" (Halifax 1978),",", Halifax 1978",$titleen);
$titlede=str_replace(" (Halifax 1978),",", Halifax 1978",$titlede);
$titlefr=str_replace(" (Halifax 1978),",", Halifax 1978",$titlefr);
$titleit=str_replace(" (Halifax 1978),",", Halifax 1978",$titleit);
$titlezh=str_replace(" (Halifax 1978),",", Halifax 1978",$titlezh);

$titleen=str_replace("(Halifax 1978),",", Halifax 1978",$titleen);
$titlede=str_replace("(Halifax 1978),",", Halifax 1978",$titlede);
$titlefr=str_replace("(Halifax 1978),",", Halifax 1978",$titlefr);
$titleit=str_replace("(Halifax 1978),",", Halifax 1978",$titleit);
$titlezh=str_replace("(Halifax 1978),",", Halifax 1978",$titlezh);
*/

$titleen=str_replace("(","[",$titleen);
$titleen=str_replace(")","]",$titleen);
$titlede=str_replace("(","[",$titlede);
$titlede=str_replace(")","]",$titlede);
$titlefr=str_replace("(","[",$titlefr);
$titlefr=str_replace(")","]",$titlefr);
$titleit=str_replace("(","[",$titleit);
$titleit=str_replace(")","]",$titleit);
$titlezh=str_replace("(","[",$titlezh);
$titlezh=str_replace(")","]",$titlezh);

$titleen=$db->db_prepare_input($titleen,1);
$titlede=$db->db_prepare_input($titlede,1);
$titlefr=$db->db_prepare_input($titlefr,1);
$titleit=$db->db_prepare_input($titleit,1);
$titlezh=$db->db_prepare_input($titlezh,1);


    $query_update="UPDATE ".TABLE_PAINTING." SET ";
    /*
        $query_update.="mID=350, ";
        $query_update.="height='18.6', ";
        $query_update.="width='12.6, ";
        $query_update.="size='18.6 cm x 12.6 cm' ";
        */

        $query_update.="titleEN='".$titleen."', ";
        $query_update.="titleDE='".$titlede."', ";
        $query_update.="titleFR='".$titlefr."', ";
        $query_update.="titleIT='".$titleit."', ";
        $query_update.="titleZH='".$titlezh."' ";

    $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

    //print "paintID = ".$row['paintID']."<br />";
    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['paintID']."<br />";
    print "enable = ".$row['enable']."<br />";
    print "titleEN = ".$row['titleEN']."<br />";
    //print "<strong>titleEN NEW = ".$titleen."<br /></strong>";
    print "titleDE = ".$row['titleDE']."<br />";
    //print "<strong>titleDE NEW = ".$titlede."<br /></strong>";
    //print "<strong>titleFR NEW = ".$titlefr."<br /></strong>";
    //print "<strong>titleIT NEW = ".$titleit."<br /></strong>";
    //print "<strong>titleZH NEW = ".$titlezh."<br /></strong>";
    print "number = ".$row['number']."<br />";
    print "ednumber = ".$row['ednumber']."<br />";
    print "catID = ".$row['catID']."<br />";
    $row_artwork=UTILS::get_artwork_info($db,$row['artworkID']);
    print "artwork = ".$row_artwork['artworkEN']."<br />";
    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);







/*
    $relationid=admin_utils::add_relation($db,12,275,1,$row['paintID'],$_POST['relations_sort']);
    add_relation_literature($db,$relationid,228430);
    $relationid=admin_utils::add_relation($db,12,494,1,$row['paintID'],$_POST['relations_sort']);
    add_relation_literature($db,$relationid,228346);
    $relationid=admin_utils::add_relation($db,12,88,1,$row['paintID'],$_POST['relations_sort']);
    add_relation_literature($db,$relationid,129515);
    $relationid=admin_utils::add_relation($db,12,90,1,$row['paintID'],$_POST['relations_sort']);
    add_relation_literature($db,$relationid,129399);
*/

}

exit;


$query="SELECT * FROM ".TABLE_PAINTING." WHERE titleEN LIKE '%.08 (Grauwald)' AND artworkID=6 ";
//$query.=" WHERE paintID=4735";
$query_order=" ORDER BY sort2 ASC ";
//$query_limit=" LIMIT 0,10 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";


while( $row=$db->mysql_array($results) )
{

    //$query_update="DELETE FROM ".TABLE_PAINTING." ";
    //$query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
    /*
        $query_update.="mID=350, ";
        $query_update.="height='18.6', ";
        $query_update.="width='12.6, ";
        $query_update.="size='18.6 cm x 12.6 cm' ";
        */

        $query_update.="mID=350, ";
        $query_update.="height='18.6', ";
        $query_update.="width='13, ";
        $query_update.="size='18.6 cm x 13 cm' ";

    $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

    //print "paintID = ".$row['paintID']."<br />";
    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['paintID']."<br />";
    print "enable = ".$row['enable']."<br />";
    print "titleEN = ".$row['titleEN']."<br />";
    print "titleDE = ".$row['titleDE']."<br />";
    print "number = ".$row['number']."<br />";
    print "ednumber = ".$row['ednumber']."<br />";
    print "catID = ".$row['catID']."<br />";
    $row_artwork=UTILS::get_artwork_info($db,$row['artworkID']);
    print "artwork = ".$row_artwork['artworkEN']."<br />";
    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);

    //admin_utils::add_relation($db,4,725,1,$row['paintID'],$_POST['relations_sort']);

}

exit;


$query="SELECT * FROM ".TABLE_PAINTING." WHERE artworkID=4 and ( ( titleEN LIKE 'Snow White (%' and titleEN LIKE '%/100)%' ) ) ";
//$query.=" WHERE paintID=4735";
$query_order="  ";
//$query_limit=" LIMIT 0,10 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";


while( $row=$db->mysql_array($results) )
{

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="editionID=12813 ";
    $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

    //print "paintID = ".$row['paintID']."<br />";
    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['paintID']."<br />";
    print "enable = ".$row['enable']."<br />";
    print "titleEN = ".$row['titleEN']."<br />";
    print "titleDE = ".$row['titleDE']."<br />";
    print "number = ".$row['number']."<br />";
    print "catID = ".$row['catID']."<br />";
    $row_artwork=UTILS::get_artwork_info($db,$row['artworkID']);
    print "artwork = ".$row_artwork['artworkEN']."<br />";
    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);


}


exit;

#VIDEOS
$query="SELECT * FROM ".TABLE_VIDEO."  ";
//$query.=" WHERE videoID=249 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<br /><br /><br /><br /><a href='/admin/videos/edit/?videoid=".$row['videoID']."' target='_blank'>".$row['videoID']."</a><hr />";



$query_relations_literature="
SELECT b.id,b.year as year, DATE_FORMAT(b.link_date, '%Y') AS date_display_en, b.id AS itemid2, IF(b.author IS NULL OR b.author='', 1, 0) AS isnull,
  COALESCE( r1.itemid2, r2.itemid1 ) as itemid,
  COALESCE( r1.sort, r2.sort ) as sort_rel,
  COALESCE( r1.relationid, r2.relationid ) as relationid
  FROM books b LEFT JOIN relations r1 ON r1.typeid1 = '10' and r1.itemid1='".$row['videoID']."' and r1.typeid2 = 12 and r1.itemid2 = b.id
  LEFT JOIN relations r2 ON r2.typeid2 = '10' and r2.itemid2='".$row['videoID']."' and r2.typeid1 = 12 and r2.itemid1 = b.id
  WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL )
  ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC
  ";

    $results_literature=$db->query($query_relations_literature);
    $count_literature=$db->numrows($results_literature);
    $i=0;
    while( $row_literature=$db->mysql_array($results_literature) )
    {
        print "<br />bookid=".$row_literature['id']." - year=".$row_literature['date_display_en']." - relationid=".$row_literature['relationid'];
        $query_relations_literature_sort_update="UPDATE ".TABLE_RELATIONS." SET sort='".$i."' WHERE relationid='".$row_literature['relationid']."' LIMIT 1";

        print "<br />".$query_relations_literature_sort_update;
        $db->query($query_relations_literature_sort_update);
        $i++;
    }
}
exit;


exit;

$query="SELECT * FROM ".TABLE_PAINTING." ";
//$query.=" WHERE paintID=4735";
$query_order="  ";
//$query_limit=" LIMIT 0,10 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";


while( $row=$db->mysql_array($results) )
{


                        $query_related_images="SELECT
                            COALESCE(
                                r1.itemid1,
                                r2.itemid2
                            ) as itemid,
                            COALESCE(
                                r1.sort,
                                r2.sort
                            ) as sort_rel,
                            COALESCE(
                                r1.relationid,
                                r2.relationid
                            ) as relationid_rel
                            FROM
                                ".TABLE_PAINTING." p
                            LEFT JOIN
                                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
                            LEFT JOIN
                                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
                            WHERE p.paintID=".$row['paintID']."
                            ORDER BY sort_rel ASC, relationid_rel DESC
                            LIMIT 1
                        ";
                        $results_related_image=$db->query($query_related_images);
                        $row_related_image=$db->mysql_array($results_related_image);


                    if( !empty($row_related_image['itemid']) )
                    {
                        //print $row_related_image['itemid'];
                        $db->query("UPDATE ".TABLE_PAINTING." SET imageid='".$row_related_image['itemid']."' WHERE paintID='".$row['paintID']."' LIMIT 1 ");
                    }

}



exit;
$query="SELECT * FROM ".TABLE_PAINTING." ";
$query.=" WHERE artworkID=3";
$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_order.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{



        $query_update="UPDATE ".TABLE_PAINTING." SET ";
            $query_update.="locationid=102, ";
            $query_update.="cityid=6, ";
            $query_update.="countryid=1 ";
        $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

        print "number = ".$row['number']."<br />";
        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);

}

exit;


# serach replace &copy; to photograph by in paintings artwork_notes
$query="SELECT * FROM ".TABLE_PAINTING." ";
//$query.=" WHERE paintID=4914";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
$search = array("©", "&copy;", "&amp;copy;");
$pattern="/©|\&amp\;copy\;|\&copy\;/i";
$i=0;
while( $row=$db->mysql_array($results) )
{

    if( preg_match($pattern, $row['artwork_notesEN'])
        || preg_match($pattern, $row['artwork_notesDE'])
        || preg_match($pattern, $row['artwork_notesFR'])
        || preg_match($pattern, $row['artwork_notesIT'])
        || preg_match($pattern, $row['artwork_notesZH']) )
    {
        $i++;
        $artwork_notesEN=str_replace($search, "photograph by ", $row['artwork_notesEN']);
        $artwork_notesDE=str_replace($search, "Fotografie ", $row['artwork_notesDE']);
        $artwork_notesFR=str_replace($search, "Photographie ", $row['artwork_notesFR']);
        $artwork_notesIT=str_replace($search, "Fotograffia ", $row['artwork_notesIT']);
        $artwork_notesZH=str_replace($search, "photograph by ", $row['artwork_notesZH']);
        $artwork_notesEN=$db->db_prepare_input($artwork_notesEN,1);
        $artwork_notesDE=$db->db_prepare_input($artwork_notesDE,1);
        $artwork_notesFR=$db->db_prepare_input($artwork_notesFR,1);
        $artwork_notesIT=$db->db_prepare_input($artwork_notesIT,1);
        $artwork_notesZH=$db->db_prepare_input($artwork_notesZH,1);

        $query_update="UPDATE ".TABLE_PAINTING." SET ";
            $query_update.="artwork_notesEN='".$artwork_notesEN."', ";
            $query_update.="artwork_notesDE='".$artwork_notesDE."', ";
            $query_update.="artwork_notesFR='".$artwork_notesFR."', ";
            $query_update.="artwork_notesIT='".$artwork_notesIT."', ";
            $query_update.="artwork_notesZH='".$artwork_notesZH."' ";
        $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);
    }
}
print "TOTAL FOUND=".$i."<br />";
# END ***** serach replace &copy; to photograph by in paintings artwork_notes
exit;

$query="SELECT * FROM ".TABLE_PAINTING_OPP." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    if( !empty($row['optionid_verso_signed_1']) || !empty($row['optionid_verso_signed_2']) || !empty($row['verso_signed_3']) ||
        !empty($row['optionid_verso_dated_1']) || !empty($row['optionid_verso_dated_2']) || !empty($row['verso_dated_3']) ||
        !empty($row['optionid_verso_numbered_1']) || !empty($row['optionid_verso_numbered_2']) || !empty($row['verso_numbered_3']) ||
        !empty($row['optionid_verso_inscribed_1']) || !empty($row['optionid_verso_inscribed_2']) || !empty($row['verso_inscribed_3']) )
    {
        print "<a href='/admin/paintings/edit/?paintid=".$row['paintid']."' target='_blank'>".$row['paintid']."</a>-".$row['paintid']."<br />";

        $query_update="UPDATE ".TABLE_PAINTING_OPP." SET ";
            $query_update.="verified_verso=1 ";
        $query_update.="WHERE oppinfoid='".$row['oppinfoid']."' LIMIT 1 ";

        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);
    }
    if( !empty($row['optionid_recto_signed_1']) || !empty($row['optionid_recto_signed_2']) || !empty($row['recto_signed_3']) ||
        !empty($row['optionid_recto_dated_1']) || !empty($row['optionid_recto_dated_2']) || !empty($row['recto_dated_3']) ||
        !empty($row['optionid_recto_numbered_1']) || !empty($row['optionid_recto_numbered_2']) || !empty($row['recto_numbered_3']) ||
        !empty($row['optionid_recto_inscribed_1']) || !empty($row['optionid_recto_inscribed_2']) || !empty($row['recto_inscribed_3']) )
    {
        print "<a href='/admin/paintings/edit/?paintid=".$row['paintid']."' target='_blank'>".$row['paintid']."</a>-".$row['paintid']."<br />";

        $query_update="UPDATE ".TABLE_PAINTING_OPP." SET ";
            $query_update.="verified_recto=1 ";
        $query_update.="WHERE oppinfoid='".$row['oppinfoid']."' LIMIT 1 ";

        print "\n".$query_update."\n";
        print "<br /><br /><hr />\n";
        //$db->query($query_update);
    }

}


exit;
# EXHIBITIONS
#3146
$query="SELECT exID AS exhibitionid FROM ".TABLE_EXHIBITIONS." LIMIT 3000,500  ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{


            $values_query_paint=array();
            $values_query_paint['columns']=",titleEN as title_delete, DATE_FORMAT(r.date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table, r.relationid";
            $query_paintings=QUERIES::query_painting($db,"","","","",$values_query_paint);



    $results_related_paint=$db->query("SELECT r.typeid1,r.itemid1,r.typeid2,r.itemid2,".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=4 AND r.itemid1='".$row['exhibitionid']."' AND r.typeid2=1 ) OR ( r.typeid2=4 AND r.itemid2='".$row['exhibitionid']."' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ");
    $count_related_paint=$db->numrows($results_related_paint);
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exhibitionid']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exhibitionid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid1=UTILS::get_relation_id($db,"17",$row_related_image);
    if( empty($imageid1) && $count_related_paint>0 )
    {
        $row_related_paint=$db->mysql_array($results_related_paint);
        $paintid=UTILS::get_relation_id($db,"1",$row_related_paint);
        $results_related_image2=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$paintid."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$paintid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        $row_related_image2=$db->mysql_array($results_related_image2);
        $imageid2=UTILS::get_relation_id($db,"17",$row_related_image2);
        if( !empty($imageid2) )
        {
            $fileName=$imageid2.".jpg";
            $tmpName=DATA_PATH."/images_new/original/".$fileName;
            $src_display="/images/size_o__imageid_".$imageid2.".jpg";
            print "paint imageid=<a href='".$src_display."'>".$imageid2."</a> - ";
            print "paintid=".$paintid." - count paintings=".$count_related_paint." - ";
            print "\t<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exhibitionid']."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$row['exhibitionid']."</a>\n";
            print "<br />";

            //exit;
            $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
            $imageid=$db->return_insert_id();

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.".jpg";

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,4,$row['exhibitionid'],$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o__border_1__maxwidth_38__maxheight_45.jpg";
                $src="/images/imageid_".$imageid."__thumb_1__size_o__quality_100__border_1__maxwidth_38__maxheight_45.png";
                //$src_cache_uri=$imageid.".jpg";
                $src_cache_uri=$imageid.".png";
                UTILS::cache_uri($src,$src_cache_uri,1);
                #end

                $db->query("UPDATE ".TABLE_EXHIBITIONS." SET image_enable=0 WHERE exID = '".$row['exhibitionid']."'");
                //exit;
            }
        }
    }
}


exit;

$query="SELECT paintID,year FROM ".TABLE_PAINTING." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="year_search='".substr($row['year'],0,4)."' ";
    $query_update.="WHERE paintID='".$row['paintID']."' LIMIT 1 ";

    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    $db->query($query_update);
}

exit;

$query="SELECT * FROM ".TABLE_BOOKS_ISBN." bi ";
$query.=" LEFT JOIN ".TABLE_BOOKS." b ON bi.bookID = b.id ";
$query.=" WHERE ( bi.isbn IS NULL OR bi.isbn = '' OR bi.isbn =0 )";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 10 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    $query2="SELECT * FROM ".TABLE_BOOKS_ISBN." bi WHERE bi.bookID='".$row['bookID']."' ";
    //$query2.=" WHERE AND ( bi.language IS NULL OR bi.language = '' OR bi.language =0 ) AND ( bi.language2 IS NULL OR bi.language2 = '' OR bi.language2 =0 ) AND ( bi.language3 IS NULL OR bi.language3 = '' OR bi.language3 =0 )";
    //$query2_order=" ORDER BY sort2 ";
    //$query2_limit=" LIMIT 0,1000 ";
    //$query2_limit.=" LIMIT 1 ";
    $results2=$db->query($query2.$query2_limit);
    $count2=$db->numrows($results2);
    $results_total2=$db->query($query2);
        if( $count2>1 )
        {
            print "<a href='/admin/books/edit/?bookid=".$row['id']."' target='_blank'>".$row['id']."</a>-".$row['title']."<br />";
        }
    while( $row2=$db->mysql_array($results2) )
    {
        if( $count2>1 )
        {
            //print "<a href='/admin/books/edit/?bookid=".$row['id']."' target='_blank'>".$row['id']."</a>-".$row['title']."<br />";
            /*
            $query_update="UPDATE ".TABLE_BOOKS." SET ";
                $query_update.="notesEN='".$row['notesEN']."', ";
                $query_update.="notesDE='".$row['notesDE']."', ";
                $query_update.="notesFR='".$row['notesFR']."', ";
                $query_update.="notesIT='".$row['notesIT']."', ";
                $query_update.="notesCN='".$row['notesCN']."', ";



            $query_update.="WHERE id='".$row['id']."' ";

            //print "\n".$query_update."\n";
            print "<br /><br /><hr />\n";
            //$db->query($query_update);
            */
        }
    }
}


exit;

function replace_old_links($db,$text)
{
    if( !empty($text) )
    {
        $text=str_replace("&amp;","&",$text);
        $text=str_replace("../../..","",$text);
        $text=str_replace("http://www.gerhard-richter.com/","/",$text);

                //$pattern = '/(\/en\/)|(\/en$)/i';
                //$replacement = "/".$current_lang."/";
                //$link=preg_replace($pattern, $replacement, $link);

        $pattern='/href=["\']?([^"\'>]+)["\']?/';
        //$pattern="/<a href\s*=\s*\"(.+)\">/";
        $url = preg_match_all($pattern, $text, $match);
        $match[1] = array_unique($match[1]);

        //print_r($match);

        if( is_array($match[1]) )
        {
            foreach ($match[1] as $link)
            {

                # /art
                if( preg_match("/\/art\//i", $link) )
                {
                    print "<strong>old link</strong> = ".$link."<br />";

                    $info_link = parse_url($link);

                    parse_str($info_link['query'], $link_params);

                    $url=array();
                    $link_change=0;
                    if( !empty($link_params) )
                    {
                        $i=0;
                        foreach ($link_params as $key => $value)
                        {
                            $i++;
                            $paintid="";
                            $categoryid="";
                            $url=array();
                            $link_change=0;
                            if( is_numeric($key) )
                            {
                                $paintid=$key;
                            }
                            elseif ( $key=="paintid" )
                            {
                                $paintid=$value;
                            }
                            elseif ( $key=="catID" )
                            {
                                $categoryid=$value;
                            }

                            # art painting
                            if( !empty($paintid) )
                            {
                                $options_painting_url=array();
                                $options_painting_url['paintid']=$paintid;
                                $options_painting_url['lang']="en";
                                $url=UTILS::get_painting_url($db,$options_painting_url);
                                $link_change=1;
                                $section="paintng";
                            }
                            # art category links
                            elseif( !empty($categoryid) )
                            {
                                $url['url']="/en/art/paintings";

                                $query_where_category=" WHERE catID='".$categoryid."' AND enable=1 ";
                                $query_limit_category=" LIMIT 1 ";
                                $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                                $results_category=$db->query($query_category['query']);
                                $count_category=$db->numrows($results_category);
                                if( $count_category>0 )
                                {
                                    $row_category=$db->mysql_array($results_category);

                                    $query_artwork="SELECT artworkID,titleurl FROM ".TABLE_ARTWORKS." WHERE artworkID='".$row_category['artworkID']."' LIMIT 1 ";
                                    //print $query_artwork;
                                    $results_artwork=$db->query($query_artwork);
                                    $count_artwork=$db->numrows($results_artwork);
                                    if( $count_artwork )
                                    {
                                        $row_artwork=$db->mysql_array($results_artwork,0);
                                        $url['url'].="/".$row_artwork['titleurl'];
                                    }

                                    if( !empty($row_category['sub_catID']) )
                                    {
                                        $query_where_category=" WHERE catID='".$row_category['sub_catID']."' AND artworkID='".$row_category['artworkID']."' AND enable=1 ";
                                        $query_limit_category=" LIMIT 1 ";
                                        $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                                        $results_sub_category=$db->query($query_category['query']);
                                        $count_sub_category=$db->numrows($results_sub_category);
                                        if( $count_sub_category>0 )
                                        {
                                            $row_sub_category=$db->mysql_array($results_sub_category);
                                        }
                                    }
                                    if( !empty($row_sub_category['titleurl']) ) $url['url'].="/".$row_sub_category['titleurl'];
                                    if( !empty($row_category['titleurl']) ) $url['url'].="/".$row_category['titleurl'];
                                    $link_change=1;
                                    $section="category";
                                }
                            }
                            # all other art links
                            elseif( $i==1 )
                            {
                                # adding lang /en before link
                                $url['url']=str_replace("/art/","/en/art/",$link);
                                $link_change=1;
                                $section="other art section";
                            }

                            if( $link_change )
                            {
                                print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                                $text=str_replace($link,$url['url'],$text);
                            }
                            break;
                        }
                    }
                    else
                    {
                        $url['url']=str_replace("/art/","/en/art/",$link);
                        $link_change=1;
                        $section="new art section";
                        print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                        $text=str_replace($link,$url['url'],$text);
                    }
                }
                # END /art
                # /literature
                elseif( preg_match("/\/literature\/\?/i", $link) )
                {
                    $url=array();
                    print "<strong>old book seacrh link</strong> = ".$link."<br />";

                    $url['url']=str_replace("/literature/?","/en/literature/search/?",$link);

                    print "<strong style='color:green;'>book search</strong> = ".$url['url']."<br />";
                    $text=str_replace($link,$url['url'],$text);
                }
                elseif( preg_match("/\/literature\//i", $link) )
                {
                    print "<strong>old link literature</strong> = ".$link."<br />";

                    $info_link = parse_url($link);

                    parse_str($info_link['query'], $link_params);

                    $url=array();
                    $link_change=0;
                    if( !empty($link_params) )
                    {
                        $i=0;
                        foreach ($link_params as $key => $value)
                        {
                            $i++;
                            $bookid="";
                            $url=array();
                            $link_change=0;
                            if( is_numeric($key) )
                            {
                                $bookid=$key;
                            }

                            # literature book
                            if( !empty($bookid) )
                            {
                                $options_book_url=array();
                                $options_book_url['bookid']=$bookid;
                                $options_book_url['lang']="en";
                                $url=UTILS::get_literature_url($db,$options_book_url);
                                $link_change=1;
                                $section="book";
                            }
                            # all other literature links
                            elseif( $i==1 )
                            {
                                # adding lang /en before link
                                $url['url']=str_replace("/literature/","/en/literature/",$link);
                                $link_change=1;
                                $section="other literature section";
                            }

                            if( $link_change )
                            {
                                print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                                $text=str_replace($link,$url['url'],$text);
                            }

                        }
                    }
                    else
                    {
                        $url['url']=str_replace("/literature/","/en/literature/",$link);
                        $link_change=1;
                        $section="new literature section";
                        print "<strong style='color:green;'>".$section."</strong> = ".$url['url']."<br />";
                        $text=str_replace($link,$url['url'],$text);
                    }
                }
                # END /literature

            }

        }

    }

    //print $text;

    return $text;
}

# PAINTINGS
/*
$query="SELECT paintID,number,titleEN,titleDE,artwork_notesEN,artwork_notesDE,artwork_notesFR,artwork_notesIT,artwork_notesCN FROM ".TABLE_PAINTING." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleDE']."-".$row['titleEN']."-".$row['number']."<br />";

    $row['artwork_notesEN']=replace_old_links($db,$row['artwork_notesEN']);
    $row['artwork_notesDE']=replace_old_links($db,$row['artwork_notesDE']);
    $row['artwork_notesFR']=replace_old_links($db,$row['artwork_notesFR']);
    $row['artwork_notesIT']=replace_old_links($db,$row['artwork_notesIT']);
    $row['artwork_notesCN']=replace_old_links($db,$row['artwork_notesCN']);


    $row['artwork_notesEN']=$db->db_prepare_input($row['artwork_notesEN'],1);
    $row['artwork_notesDE']=$db->db_prepare_input($row['artwork_notesDE'],1);
    $row['artwork_notesFR']=$db->db_prepare_input($row['artwork_notesFR'],1);
    $row['artwork_notesIT']=$db->db_prepare_input($row['artwork_notesIT'],1);
    $row['artwork_notesCN']=$db->db_prepare_input($row['artwork_notesCN'],1);

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="artwork_notesEN='".$row['artwork_notesEN']."', ";
        $query_update.="artwork_notesDE='".$row['artwork_notesDE']."', ";
        $query_update.="artwork_notesFR='".$row['artwork_notesFR']."', ";
        $query_update.="artwork_notesIT='".$row['artwork_notesIT']."', ";
        $query_update.="artwork_notesCN='".$row['artwork_notesCN']."' ";
    $query_update.="WHERE paintID='".$row['paintID']."' ";

    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# BIOGRAPHY
/*
$query="SELECT * FROM ".TABLE_BIOGRAPHY." ";
//$query.=" WHERE biographyid=9 ";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/biography/edit/?biographyid=".$row['biographyid']."' target='_blank'>".$row['biographyid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    //$row['text_en']=replace_old_links($db,$row['text_en']);
    //$row['text_de']=replace_old_links($db,$row['text_de']);
    //$row['text_fr']=replace_old_links($db,$row['text_fr']);
    //$row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_zh']=replace_old_links($db,$row['text_zh']);


    //$row['text_en']=$db->db_prepare_input($row['text_en'],1);
    //$row['text_de']=$db->db_prepare_input($row['text_de'],1);
    //$row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    //$row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_zh']=$db->db_prepare_input($row['text_zh'],1);

    $query_update="UPDATE ".TABLE_BIOGRAPHY." SET ";
        //$query_update.="text_en='".$row['text_en']."', ";
        //$query_update.="text_de='".$row['text_de']."', ";
        //$query_update.="text_fr='".$row['text_fr']."', ";
        //$query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_zh='".$row['text_zh']."' ";
    $query_update.="WHERE biographyid='".$row['biographyid']."' ";

    print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    $db->query($query_update);
}
*/

# TEXT
/*
$query="SELECT * FROM ".TABLE_TEXT." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/pages/edit/?textid=".$row['textid']."' target='_blank'>".$row['textid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    $row['text_en']=replace_old_links($db,$row['text_en']);
    $row['text_de']=replace_old_links($db,$row['text_de']);
    $row['text_fr']=replace_old_links($db,$row['text_fr']);
    $row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_cn']=replace_old_links($db,$row['text_cn']);


    $row['text_en']=$db->db_prepare_input($row['text_en'],1);
    $row['text_de']=$db->db_prepare_input($row['text_de'],1);
    $row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    $row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_cn']=$db->db_prepare_input($row['text_cn'],1);

    $query_update="UPDATE ".TABLE_TEXT." SET ";
        $query_update.="text_en='".$row['text_en']."', ";
        $query_update.="text_de='".$row['text_de']."', ";
        $query_update.="text_fr='".$row['text_fr']."', ";
        $query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_cn='".$row['text_cn']."' ";
    $query_update.="WHERE textid='".$row['textid']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# QUOTES
/*
$query="SELECT * FROM ".TABLE_QUOTES." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/quotes/edit/?quoteid=".$row['quoteid']."' target='_blank'>".$row['quoteid']."</a>-".$row['title_de']."-".$row['title_en']."<br />";

    $row['text_en']=replace_old_links($db,$row['text_en']);
    $row['text_de']=replace_old_links($db,$row['text_de']);
    $row['text_fr']=replace_old_links($db,$row['text_fr']);
    $row['text_it']=replace_old_links($db,$row['text_it']);
    $row['text_cn']=replace_old_links($db,$row['text_cn']);


    $row['text_en']=$db->db_prepare_input($row['text_en'],1);
    $row['text_de']=$db->db_prepare_input($row['text_de'],1);
    $row['text_fr']=$db->db_prepare_input($row['text_fr'],1);
    $row['text_it']=$db->db_prepare_input($row['text_it'],1);
    $row['text_cn']=$db->db_prepare_input($row['text_cn'],1);

    $query_update="UPDATE ".TABLE_QUOTES." SET ";
        $query_update.="text_en='".$row['text_en']."', ";
        $query_update.="text_de='".$row['text_de']."', ";
        $query_update.="text_fr='".$row['text_fr']."', ";
        $query_update.="text_it='".$row['text_it']."', ";
        $query_update.="text_cn='".$row['text_cn']."' ";
    $query_update.="WHERE quoteid='".$row['quoteid']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/


# EXHIBITIONS
/*
$query="SELECT * FROM ".TABLE_EXHIBITIONS." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>".$row['exID']."</a>-".$row['title_original']."<br />";

    $row['descriptionEN']=replace_old_links($db,$row['descriptionEN']);
    $row['descriptionDE']=replace_old_links($db,$row['descriptionDE']);
    $row['descriptionFR']=replace_old_links($db,$row['descriptionFR']);
    $row['descriptionIT']=replace_old_links($db,$row['descriptionIT']);
    $row['descriptionCN']=replace_old_links($db,$row['descriptionCN']);


    $row['descriptionEN']=$db->db_prepare_input($row['descriptionEN'],1);
    $row['descriptionDE']=$db->db_prepare_input($row['descriptionDE'],1);
    $row['descriptionFR']=$db->db_prepare_input($row['descriptionFR'],1);
    $row['descriptionIT']=$db->db_prepare_input($row['descriptionIT'],1);
    $row['descriptionCN']=$db->db_prepare_input($row['descriptionCN'],1);

    $query_update="UPDATE ".TABLE_EXHIBITIONS." SET ";
        $query_update.="descriptionEN='".$row['descriptionEN']."', ";
        $query_update.="descriptionDE='".$row['descriptionDE']."', ";
        $query_update.="descriptionFR='".$row['descriptionFR']."', ";
        $query_update.="descriptionIT='".$row['descriptionIT']."', ";
        $query_update.="descriptionCN='".$row['descriptionCN']."' ";
    $query_update.="WHERE exID='".$row['exID']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# LITERATURE
/*
$query="SELECT * FROM ".TABLE_BOOKS." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/books/edit/?bookid=".$row['id']."' target='_blank'>".$row['id']."</a>-".$row['title']."<br />";

    $row['notesEN']=replace_old_links($db,$row['notesEN']);
    $row['notesDE']=replace_old_links($db,$row['notesDE']);
    $row['notesFR']=replace_old_links($db,$row['notesFR']);
    $row['notesIT']=replace_old_links($db,$row['notesIT']);
    $row['notesCN']=replace_old_links($db,$row['notesCN']);
    $row['notesEN']=$db->db_prepare_input($row['notesEN'],1);
    $row['notesDE']=$db->db_prepare_input($row['notesDE'],1);
    $row['notesFR']=$db->db_prepare_input($row['notesFR'],1);
    $row['notesIT']=$db->db_prepare_input($row['notesIT'],1);
    $row['notesCN']=$db->db_prepare_input($row['notesCN'],1);

    $row['infoEN']=replace_old_links($db,$row['infoEN']);
    $row['infoDE']=replace_old_links($db,$row['infoDE']);
    $row['infoFR']=replace_old_links($db,$row['infoFR']);
    $row['infoIT']=replace_old_links($db,$row['infoIT']);
    $row['infoCN']=replace_old_links($db,$row['infoCN']);
    $row['infoEN']=$db->db_prepare_input($row['infoEN'],1);
    $row['infoDE']=$db->db_prepare_input($row['infoDE'],1);
    $row['infoFR']=$db->db_prepare_input($row['infoFR'],1);
    $row['infoIT']=$db->db_prepare_input($row['infoIT'],1);
    $row['infoCN']=$db->db_prepare_input($row['infoCN'],1);

    $row['infosmall_en']=replace_old_links($db,$row['infosmall_en']);
    $row['infosmall_de']=replace_old_links($db,$row['infosmall_de']);
    $row['infosmall_fr']=replace_old_links($db,$row['infosmall_fr']);
    $row['infosmall_it']=replace_old_links($db,$row['infosmall_it']);
    $row['infosmall_cn']=replace_old_links($db,$row['infosmall_cn']);
    $row['infosmall_en']=$db->db_prepare_input($row['infosmall_en'],1);
    $row['infosmall_de']=$db->db_prepare_input($row['infosmall_de'],1);
    $row['infosmall_fr']=$db->db_prepare_input($row['infosmall_fr'],1);
    $row['infosmall_it']=$db->db_prepare_input($row['infosmall_it'],1);
    $row['infosmall_cn']=$db->db_prepare_input($row['infosmall_cn'],1);

    $query_update="UPDATE ".TABLE_BOOKS." SET ";
        $query_update.="notesEN='".$row['notesEN']."', ";
        $query_update.="notesDE='".$row['notesDE']."', ";
        $query_update.="notesFR='".$row['notesFR']."', ";
        $query_update.="notesIT='".$row['notesIT']."', ";
        $query_update.="notesCN='".$row['notesCN']."', ";

        $query_update.="infoEN='".$row['infoEN']."', ";
        $query_update.="infoDE='".$row['infoDE']."', ";
        $query_update.="infoFR='".$row['infoFR']."', ";
        $query_update.="infoIT='".$row['infoIT']."', ";
        $query_update.="infoCN='".$row['infoCN']."', ";

        $query_update.="infosmall_en='".$row['infosmall_en']."', ";
        $query_update.="infosmall_de='".$row['infosmall_de']."', ";
        $query_update.="infosmall_fr='".$row['infosmall_fr']."', ";
        $query_update.="infosmall_it='".$row['infosmall_it']."', ";
        $query_update.="infosmall_cn='".$row['infosmall_cn']."' ";

    $query_update.="WHERE id='".$row['id']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

# VIDEOS
/*
$query="SELECT * FROM ".TABLE_VIDEO." ";
//$query.=" WHERE paintID=17677";
//$query_order=" ORDER BY sort2 ";
//$query_limit=" LIMIT 0,1000 ";
//$query_limit.=" LIMIT 1 ";
$results=$db->query($query.$query_limit);
$count=$db->numrows($results);
$results_total=$db->query($query);
$count_total=$db->numrows($results_total);
print "count_total=".$count_total."<br />";
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/videos/edit/?videoid=".$row['videoID']."' target='_blank'>".$row['videoID']."</a>-".$row['titleEN']."-".$row['title_en']."<br />";

    $row['info_en']=replace_old_links($db,$row['info_en']);
    $row['info_de']=replace_old_links($db,$row['info_de']);
    $row['info_fr']=replace_old_links($db,$row['info_fr']);
    $row['info_it']=replace_old_links($db,$row['info_it']);
    $row['info_cn']=replace_old_links($db,$row['info_cn']);


    $row['info_en']=$db->db_prepare_input($row['info_en'],1);
    $row['info_de']=$db->db_prepare_input($row['info_de'],1);
    $row['info_fr']=$db->db_prepare_input($row['info_fr'],1);
    $row['info_it']=$db->db_prepare_input($row['info_it'],1);
    $row['info_cn']=$db->db_prepare_input($row['info_cn'],1);

    $query_update="UPDATE ".TABLE_VIDEO." SET ";
        $query_update.="info_en='".$row['info_en']."', ";
        $query_update.="info_de='".$row['info_de']."', ";
        $query_update.="info_fr='".$row['info_fr']."', ";
        $query_update.="info_it='".$row['info_it']."', ";
        $query_update.="info_cn='".$row['info_cn']."' ";
    $query_update.="WHERE videoID='".$row['videoID']."' ";

    //print "\n".$query_update."\n";
    print "<br /><br /><hr />\n";
    //$db->query($query_update);
}
*/

exit;
function replace2($db,$text)
{
    //$text=str_replace("/intro.php","",$text);
    //$text=str_replace("editions/snow-white","microsites/snow-white",$text);
    //$text=str_replace("editions/war-cut","microsites/war-cut",$text);
    $text=str_replace("paintings/sinbad","microsites/sinbad",$text);

    //$text = htmlspecialchars($text);

    return $text;
}

# PAINTINGS
$query="SELECT *  FROM `richter_live_fixed_encodings`.`painting` WHERE (CONVERT(`paintID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artworkID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artworkID2` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`editionID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`edition_individual` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleEN` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleDE` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleFR` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleIT` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleCN` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`titleurl` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`title_search_1` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`title_search_2` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`title_search_3` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`title_search_4` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`year` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`date` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`number` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`number_search` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`number_search2` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`ednumber` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`ednumberroman` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`ednumberap` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`citylifepage` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`nr_of_editions` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`catID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`size` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`size_parts` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`size_typeid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`height` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`width` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`src` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`mID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`muID` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`locationid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`cityid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`countryid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`loan_typeid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`loan_locationid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`loan_cityid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`loan_countryid` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`sort` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`sort2` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`sort3` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artwork_notesEN` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artwork_notesDE` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artwork_notesFR` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artwork_notesIT` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`artwork_notesCN` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`notes` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`image_zoomer` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`keywords` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`created` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`modified` USING utf8) LIKE '%%paintings/sinbad%%' OR CONVERT(`enable` USING utf8) LIKE '%%paintings/sinbad%%')";
//$query="SELECT * FROM ".TABLE_PAINTING." WHERE ";
//$query.=" artwork_notesEN LIKE '%intro.php%' AND artwork_notesEN LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
//$query.=" ORDER BY sort2 ";
//$query.=" LIMIT 0,1000 ";
//$query.=" LIMIT 1 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleDE']."-".$row['titleEN']."-".$row['number']."<br />";

    $row['artwork_notesEN']=replace2($db,$row['artwork_notesEN']);
    $row['artwork_notesDE']=replace2($db,$row['artwork_notesDE']);
    $row['artwork_notesFR']=replace2($db,$row['artwork_notesFR']);
    $row['artwork_notesIT']=replace2($db,$row['artwork_notesIT']);
    $row['artwork_notesCN']=replace2($db,$row['artwork_notesCN']);


    $row['artwork_notesEN']=$db->db_prepare_input($row['artwork_notesEN'],1);
    $row['artwork_notesDE']=$db->db_prepare_input($row['artwork_notesDE'],1);
    $row['artwork_notesFR']=$db->db_prepare_input($row['artwork_notesFR'],1);
    $row['artwork_notesIT']=$db->db_prepare_input($row['artwork_notesIT'],1);
    $row['artwork_notesCN']=$db->db_prepare_input($row['artwork_notesCN'],1);

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="artwork_notesEN='".$row['artwork_notesEN']."', ";
        $query_update.="artwork_notesDE='".$row['artwork_notesDE']."', ";
        //$query_update.="artwork_notesDE='".$row['artwork_notesDE']."' ";
        $query_update.="artwork_notesFR='".$row['artwork_notesFR']."', ";
        $query_update.="artwork_notesIT='".$row['artwork_notesIT']."', ";
        $query_update.="artwork_notesCN='".$row['artwork_notesCN']."' ";
    $query_update.="WHERE paintID='".$row['paintID']."' ";

//print $query_update."<br /><br />";
    $db->query($query_update);
}

exit;


function replace($db,$text)
{

    # style color
    /*
    $text = str_replace('style="color: rgb(90, 90, 89);"','',$text);
    $pattern='/<span style="color: rgb(90, 90, 89);">(.*?)<\/span>/';
    $replace="$1";
    $text = preg_replace($pattern, $replace, $text);
*/

    # italic and underline together
    $pattern='/<span style="font-style: italic; text-decoration: underline;">(.*?)<\/span>/';
    $replace="<u><em>$1</em></u>";
    $text = preg_replace($pattern, $replace, $text);

    # italic
    $pattern='/<span style="font-style: italic;">(.*?)<\/span>/';
    $replace="<em>$1</em>";
    $text = preg_replace($pattern, $replace, $text);

    # bold
    $pattern='/<span style="font-weight: bold;">(.*?)<\/span>/';
    $replace="<strong>$1</strong>";
    $text = preg_replace($pattern, $replace, $text);

    # take out underlining if link
    //$text_search=$db->db_prepare_input($text,1);
    $text_search=$text;
    //$regexp="<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    //preg_match_all('/$regexp/siU', $text_search, $matche);
    //$pattern='/<a[^>]+href\s*=\s*["\']([^"\']+)["\'][^>]*>(.*?)<\/a>/';
    $pattern='~<a\s+.*?</a>~is';
    preg_match_all($pattern, $text_search, $matche);
    foreach( $matche[0] as $key => $link )
    {
        # remove targets from inside links
        if( stripos($link,"gerhard-richter.com") || stripos($link,"detail.php") || stripos($link,"/art/") )
        {
            $old_link_with_target=$matche[0][$key];
            $matche[0][$key]=str_replace('target="_blank"',"",$matche[0][$key]);
            $matche[0][$key]=str_replace('target="blank"',"",$matche[0][$key]);
            $text=str_replace($old_link_with_target,$matche[0][$key],$text);
        }
        # END remove targets from inside links
        $pattern='#<\s*?a\b[^>]*>(.*?)</a\b[^>]*>#s';
        preg_match($pattern, $link, $matche2);
        foreach($matche2 as $key => $link)
        {
            if($key==1)
            {
                //print_r($link);
                # underline take out from links
                $pattern2='/<span style="text-decoration: underline;">(.*?)<\/span>/';
                $replace2="$1";
                $new = preg_replace($pattern2, $replace2, $link);
                $text=str_replace('<span style="text-decoration: underline;">'.$new.'</span>',$new,$text);

                # underline take out from links in i tag underlined with style attr
                $pattern3='/<i style="text-decoration: underline;">(.*?)<\/i>/';
                $replace3="<i>$1</i>";
                $new = preg_replace($pattern3, $replace3, $link);
                $text=str_replace($link,$new,$text);
            }
            if($key==0)
            {
                # underline take out from links
                //$new=str_replace('style="text-decoration: underline;"',"",$link);
                //$new=str_replace('underline',"",$link);
                //print $link."-".$new."|";
                //$text=str_replace(stripslashes($link),stripslashes($new),$text);
                //$text=str_replace('style="text-decoration: underline;"',"",$text);
                //$text=str_replace("text-decoration: underline;","tttttt",$text);
                //$text=str_replace('style="tttttt"',"",$text);

            }
        }
    }
    //print_r($matche);
    # END take out underlining if link

    # underline
    $pattern='/<span style="text-decoration: underline;">(.*?)<\/span>/';
    $replace="<u>$1</u>";
    $text = preg_replace($pattern, $replace, $text);

    # underline i
    $pattern='/<i style="text-decoration: underline;">(.*?)<\/i>/';
    $replace="<i><u>$1</u></i>";
    //$text = preg_replace($pattern, $replace, $text);

    # a style
    $pattern='/(<a]+) style=".*?"/i';
    $replace="$1";
    //$text = preg_replace($pattern, $replace, $text);

    # remove font tag
    $pattern='/<font[^>]*>/';
    $replace="";
    $text = preg_replace($pattern, $replace, $text);
    $pattern='/<\/font[^>]*>/';
    $replace="";
    $text = preg_replace($pattern, $replace, $text);
    # END remove font tag

    $text=str_replace("http://www.gerhard-richter.com","",$text);
    $text=str_replace("../../..","",$text);

    //$text = htmlspecialchars($text);

    return $text;
}

exit;
#VIDEOS
$query="SELECT * FROM ".TABLE_VIDEO." WHERE ";
$query.=" info_en LIKE '%_blank%' AND info_en LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
//$query.=" LIMIT 0,1 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<br /><br /><br /><br /><a href='/admin/videos/edit/?videoid=".$row['videoID']."' target='_blank'>".$row['videoID']."</a>";

    $text_en=replace($db,$row['info_en']);
    $text_de=replace($db,$row['info_de']);
    $text_fr=replace($db,$row['info_fr']);
    $text_it=replace($db,$row['info_it']);
    $text_ch=replace($db,$row['info_ch']);
    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    print htmlspecialchars($text_en);
    //print $text_en;

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $query_update="UPDATE ".TABLE_VIDEO." SET ";
        $query_update.="info_en='".$text_en."', ";
        $query_update.="info_de='".$text_de."', ";
        $query_update.="info_fr='".$text_fr."', ";
        $query_update.="info_it='".$text_it."', ";
        $query_update.="info_ch='".$text_ch."' ";
    $query_update.="WHERE videoID='".$row['videoID']."' ";

    //$db->query($query_update);
}
exit;


#LITERATURE
$query="SELECT * FROM ".TABLE_BOOKS." WHERE ";
$query.=" ( infosmall_en LIKE '%_blank%' AND infosmall_en LIKE '%gerhard-richter.com%' ) OR ( infoEN LIKE '%_blank%' AND infoEN LIKE '%gerhard-richter.com%' ) OR ( notesEN LIKE '%_blank%' AND notesEN LIKE '%gerhard-richter.com%' ) ";
//$query.=" id=32";
//$query.=" LIMIT 0,1 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<br /><br /><br /><br /><a href='/admin/books/edit/?bookid=".$row['id']."' target='_blank'>".$row['id']."</a>";

    $text_en=replace($db,$row['infoEN']);
    $text_de=replace($db,$row['infoDE']);
    $text_fr=replace($db,$row['infoFR']);
    $text_it=replace($db,$row['infoIT']);
    $text_ch=replace($db,$row['infoCH']);

    $infosmall_en=replace($db,$row['infosmall_en']);
    $infosmall_de=replace($db,$row['infosmall_de']);
    $infosmall_fr=replace($db,$row['infosmall_fr']);
    $infosmall_it=replace($db,$row['infosmall_it']);
    $infosmall_ch=replace($db,$row['infosmall_ch']);

    $notesEN=replace($db,$row['notesEN']);
    $notesDE=replace($db,$row['notesDE']);
    $notesFR=replace($db,$row['notesFR']);
    $notesIT=replace($db,$row['notesIT']);
    $notesCH=replace($db,$row['notesCH']);

    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    print "<br /><br />".htmlspecialchars($text_en);
    print "<br /><br />".htmlspecialchars($infosmall_en);
    print "<br /><br />".htmlspecialchars($notesEN);
    //print $text_en;

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $infosmall_en=$db->db_prepare_input($infosmall_en,1);
    $infosmall_de=$db->db_prepare_input($infosmall_de,1);
    $infosmall_fr=$db->db_prepare_input($infosmall_fr,1);
    $infosmall_it=$db->db_prepare_input($infosmall_it,1);
    $infosmall_ch=$db->db_prepare_input($infosmall_ch,1);

    $notesEN=$db->db_prepare_input($notesEN,1);
    $notesDE=$db->db_prepare_input($notesDE,1);
    $notesFR=$db->db_prepare_input($notesFR,1);
    $notesIT=$db->db_prepare_input($notesIT,1);
    $notesCH=$db->db_prepare_input($notesCH,1);

    $query_update="UPDATE ".TABLE_BOOKS." SET ";
        $query_update.="infoEN='".$text_en."', ";
        $query_update.="infoDE='".$text_de."', ";
        $query_update.="infoFR='".$text_fr."', ";
        $query_update.="infoIT='".$text_it."', ";
        $query_update.="infoCH='".$text_ch."', ";
        $query_update.="infosmall_en='".$infosmall_en."', ";
        $query_update.="infosmall_de='".$infosmall_de."', ";
        $query_update.="infosmall_fr='".$infosmall_fr."', ";
        $query_update.="infosmall_it='".$infosmall_it."', ";
        $query_update.="infosmall_ch='".$infosmall_ch."', ";
        $query_update.="notesEN='".$notesEN."', ";
        $query_update.="notesDE='".$notesDE."', ";
        $query_update.="notesFR='".$notesFR."', ";
        $query_update.="notesIT='".$notesIT."', ";
        $query_update.="notesCH='".$notesCH."' ";
    $query_update.="WHERE id='".$row['id']."' ";

    //$db->query($query_update);
}
exit;

#EXHIBITIONS
$query="SELECT * FROM ".TABLE_EXHIBITIONS." WHERE ";
$query.=" descriptionEN LIKE '%_blank%' AND descriptionEN LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
//$query.=" LIMIT 0,1 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<br /><br /><br /><br /><a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>".$row['exID']."</a>";

    $text_en=replace($db,$row['descriptionEN']);
    $text_de=replace($db,$row['descriptionDE']);
    $text_fr=replace($db,$row['descriptionFR']);
    $text_it=replace($db,$row['descriptionIT']);
    $text_ch=replace($db,$row['descriptionCH']);
    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    print htmlspecialchars($text_en);
    //print $text_en;

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $query_update="UPDATE ".TABLE_EXHIBITIONS." SET ";
        $query_update.="descriptionEN='".$text_en."', ";
        $query_update.="descriptionDE='".$text_de."', ";
        $query_update.="descriptionFR='".$text_fr."', ";
        $query_update.="descriptionIT='".$text_it."', ";
        $query_update.="descriptionCH='".$text_ch."' ";
    $query_update.="WHERE exID='".$row['exID']."' ";

    //$db->query($query_update);
}
exit;

#QUOTES
$query="SELECT * FROM ".TABLE_QUOTES." WHERE ";
//$query.=" text_en LIKE '%_blank%' AND text_en LIKE '%gerhard-richter.com%' ";
$query.=" text_en LIKE '%rgb(%' ";
//$query.=" paintID=14854";
//$query.=" LIMIT 0,1 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<br /><br /><br /><br /><a href='/admin/quotes/edit/?quoteid=".$row['quoteid']."' target='_blank'>".$row['quoteid']."</a>";

    $text_en=replace($db,$row['text_en']);
    $text_de=replace($db,$row['text_de']);
    $text_fr=replace($db,$row['text_fr']);
    $text_it=replace($db,$row['text_it']);
    $text_ch=replace($db,$row['text_ch']);
    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    print htmlspecialchars($text_en);
    //print $text_en;

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $query_update="UPDATE ".TABLE_QUOTES." SET ";
        $query_update.="text_en='".$text_en."', ";
        $query_update.="text_de='".$text_de."', ";
        $query_update.="text_fr='".$text_fr."', ";
        $query_update.="text_it='".$text_it."', ";
        $query_update.="text_ch='".$text_ch."' ";
    $query_update.="WHERE quoteid='".$row['quoteid']."' ";

    //$db->query($query_update);
}
exit;

#TEXT
$query="SELECT * FROM ".TABLE_TEXT." WHERE ";
$query.=" text_en LIKE '%_blank%' AND text_en LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
//$query.=" LIMIT 0,100 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<a href='/admin/text/edit/?textid=".$row['textid']."' target='_blank'>".$row['textid']."</a><br /><br /><br /><br />";

    $text_en=replace($db,$row['text_en']);
    $text_de=replace($db,$row['text_de']);
    $text_fr=replace($db,$row['text_fr']);
    $text_it=replace($db,$row['text_it']);
    $text_ch=replace($db,$row['text_ch']);
    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    //print htmlspecialchars($text_en);
    print $text_en;

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $query_update="UPDATE ".TABLE_TEXT." SET ";
        $query_update.="text_en='".$text_en."', ";
        $query_update.="text_de='".$text_de."', ";
        $query_update.="text_fr='".$text_fr."', ";
        $query_update.="text_it='".$text_it."', ";
        $query_update.="text_ch='".$text_ch."' ";
    $query_update.="WHERE textid='".$row['textid']."' ";

    //$db->query($query_update);
}
exit;

#BIOGRAPHY
$query="SELECT * FROM ".TABLE_BIOGRAPHY." WHERE ";
$query.=" text_en LIKE '%_blank%' AND text_en LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
//$query.=" LIMIT 0,100 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{
    print "<a href='/admin/biography/edit/?biographyid=".$row['biographyid']."' target='_blank'>".$row['biographyid']."</a><br />";

    $text_en=replace($db,$row['text_en']);
    $text_de=replace($db,$row['text_de']);
    $text_fr=replace($db,$row['text_fr']);
    $text_it=replace($db,$row['text_it']);
    $text_ch=replace($db,$row['text_ch']);
    //$row['text_en']=htmlspecialchars($row['text_en']);
    //print $row['text_en'];
    print htmlspecialchars($text_en);

    $text_en=$db->db_prepare_input($text_en,1);
    $text_de=$db->db_prepare_input($text_de,1);
    $text_fr=$db->db_prepare_input($text_fr,1);
    $text_it=$db->db_prepare_input($text_it,1);
    $text_ch=$db->db_prepare_input($text_ch,1);

    $query_update="UPDATE ".TABLE_BIOGRAPHY." SET ";
        $query_update.="text_en='".$text_en."', ";
        $query_update.="text_de='".$text_de."', ";
        $query_update.="text_fr='".$text_fr."', ";
        $query_update.="text_it='".$text_it."', ";
        $query_update.="text_ch='".$text_ch."' ";
    $query_update.="WHERE biographyid='".$row['biographyid']."' ";

    //$db->query($query_update);
}
exit;


# PAINTINGS
$query="SELECT * FROM ".TABLE_PAINTING." WHERE ";
$query.=" artwork_notesEN LIKE '%_blank%' AND artwork_notesEN LIKE '%gerhard-richter.com%' ";
//$query.=" paintID=14854";
$query.=" ORDER BY sort2 ";
//$query.=" LIMIT 0,1000 ";
$results=$db->query($query);
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleDE']."-".$row['titleEN']."-".$row['number']."<br />";

    $row['artwork_notesEN']=replace($db,$row['artwork_notesEN']);
    $row['artwork_notesDE']=replace($db,$row['artwork_notesDE']);
    $row['artwork_notesFR']=replace($db,$row['artwork_notesFR']);
    $row['artwork_notesIT']=replace($db,$row['artwork_notesIT']);
    $row['artwork_notesCH']=replace($db,$row['artwork_notesCH']);
    //$row['artwork_notesEN']=htmlspecialchars($row['artwork_notesEN']);
    //print $row['artwork_notesEN'];
    //$row['artwork_notesDE']=htmlspecialchars($row['artwork_notesDE']);
    //print $row['artwork_notesDE'];
    //$row['artwork_notesFR']=htmlspecialchars($row['artwork_notesFR']);
    //print $row['artwork_notesFR'];
    //$row['artwork_notesIT']=htmlspecialchars($row['artwork_notesIT']);
    //print $row['artwork_notesIT'];
    //$row['artwork_notesCH']=htmlspecialchars($row['artwork_notesCH']);
    //print $row['artwork_notesCH'];

    $row['artwork_notesEN']=$db->db_prepare_input($row['artwork_notesEN'],1);
    $row['artwork_notesDE']=$db->db_prepare_input($row['artwork_notesDE'],1);
    $row['artwork_notesFR']=$db->db_prepare_input($row['artwork_notesFR'],1);
    $row['artwork_notesIT']=$db->db_prepare_input($row['artwork_notesIT'],1);
    $row['artwork_notesCH']=$db->db_prepare_input($row['artwork_notesCH'],1);

    $query_update="UPDATE ".TABLE_PAINTING." SET ";
        $query_update.="artwork_notesEN='".$row['artwork_notesEN']."', ";
        $query_update.="artwork_notesDE='".$row['artwork_notesDE']."', ";
        //$query_update.="artwork_notesDE='".$row['artwork_notesDE']."' ";
        $query_update.="artwork_notesFR='".$row['artwork_notesFR']."', ";
        $query_update.="artwork_notesIT='".$row['artwork_notesIT']."', ";
        $query_update.="artwork_notesCH='".$row['artwork_notesCH']."' ";
    $query_update.="WHERE paintID='".$row['paintID']."' ";

    //$db->query($query_update);
}
exit;
/*
$results=$db->query("SELECT paintID,titleDE,titleEN,number,artworkID,catID FROM ".TABLE_PAINTING." WHERE titleDE LIKE 'Blech%' AND artworkID=2 ORDER BY sort2 ");
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleDE']."-".$row['titleEN']."-".$row['number']."<br />";
    //$db->query("UPDATE ".TABLE_PAINTING." SET artworkID=1, catID=101 WHERE paintID='".$row['paintID']."' ");
}
exit;
*/
/*
$results1=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='8197' ");
$row1=$db->mysql_array($results1);
    $row1['artwork_notesEN']=$db->db_prepare_input($row1['artwork_notesEN'],1);
    $row1['artwork_notesDE']=$db->db_prepare_input($row1['artwork_notesDE'],1);
    $row1['artwork_notesFR']=$db->db_prepare_input($row1['artwork_notesFR'],1);
    $row1['artwork_notesIT']=$db->db_prepare_input($row1['artwork_notesIT'],1);
    $row1['artwork_notesCH']=$db->db_prepare_input($row1['artwork_notesCH'],1);
$results=$db->query("SELECT paintID,titleEN,number FROM ".TABLE_PAINTING." WHERE number LIKE '%838-%' ORDER BY sort2 ");
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleEN']."-".$row['number']."<br />";
    //$db->query("UPDATE ".TABLE_PAINTING." SET artwork_notesEN='".$row1['artwork_notesEN']."', artwork_notesDE='".$row1['artwork_notesDE']."', artwork_notesFR='".$row1['artwork_notesFR']."', artwork_notesIT='".$row1['artwork_notesIT']."', artwork_notesCH='".$row1['artwork_notesCH']."' WHERE paintID='".$row['paintID']."' ");
}
exit;
 */
/*
$results=$db->query("SELECT paintID,titleEN, titleIT FROM ".TABLE_PAINTING." WHERE titleIT LIKE '%Striscia%' ");
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleEN']."-".$row['titleIT']."-".$row['number']."<br />";
    $db->query("UPDATE ".TABLE_PAINTING." SET titleIT='".$row['titleEN']."' WHERE paintID='".$row['paintID']."' ");
}
exit;
 */
/*
//$results=$db->query("SELECT * FROM ".TABLE_MUSEUM." WHERE muID=363 ");
$results=$db->query("SELECT * FROM ".TABLE_MUSEUM."");
while( $row=$db->mysql_array($results) )
{

    //if( empty($row['locationid']) || empty($row['cityid']) || empty($row['countryid']) ) print "<a href='/admin/museums/edit/?museumid=".$row['muID']."'>".$row['muID']."</a><br />";
    //if( !empty($row['collection_desc_en']) ) print "<a href='/admin/museums/edit/?museumid=".$row['muID']."'>".$row['muID']."</a><br />";

    $results_relations=$db->query("SELECT r.relationid,r.typeid1,r.itemid1,r.typeid2,r.itemid2 FROM ".TABLE_RELATIONS." r WHERE ( r.typeid1=16 AND r.itemid1='".$row['muID']."' AND r.typeid2=1 ) OR ( r.typeid2=16 AND r.itemid2='".$row['muID']."' AND r.typeid1=1 ) ");
    $count_relations=$db->numrows($results_relations);
    if( $count_relations>0 )
    {
        while($row_relations=$db->mysql_array($results_relations))
        {
            $paintid=UTILS::get_relation_id($db,"1",$row_relations);


            $query="UPDATE ".TABLE_PAINTING." SET locationid='".$row['locationid']."', cityid='".$row['cityid']."', countryid='".$row['countryid']."', loan_typeid='".$row['loan_typeid']."', loan_locationid='".$row['loan_locationid']."', loan_cityid='".$row['loan_cityid']."', loan_countryid='".$row['loan_countryid']."'  WHERE paintID='".$paintid."' LIMIT 1  ";
            //$db->query($query);
            //print $query."<br />";

        }
    }


}
exit;
*/


//exit;
/*
//$editionid="12662";
//$like="May 1990)";

//$editionid="12770";
//$like="Kassel (";

//$editionid="12813";
//$like="Snow White (";

//$editionid="15642";
//$like="Strip (";

//$editionid="12672";
//$like="War Cut II (";

$like="Fuji";
////$like="Green-Blue-Red";

//$results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID=4 AND titleEN LIKE '%".$like."%' ");
$results1=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='10522' ");
$row1=$db->mysql_array($results1);
    $row1['artwork_notesEN']=$db->db_prepare_input($row1['artwork_notesEN'],1);
    $row1['artwork_notesDE']=$db->db_prepare_input($row1['artwork_notesDE'],1);
    $row1['artwork_notesFR']=$db->db_prepare_input($row1['artwork_notesFR'],1);
    $row1['artwork_notesIT']=$db->db_prepare_input($row1['artwork_notesIT'],1);
    $row1['artwork_notesCH']=$db->db_prepare_input($row1['artwork_notesCH'],1);
$results=$db->query("SELECT paintID,titleEN,number FROM ".TABLE_PAINTING." WHERE titleEN LIKE '%".$like."%' AND number LIKE '%839-%' ORDER BY sort2 ");
//$results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE titleEN LIKE '%".$like."%' AND titleEN LIKE '%/100)%' ");
$count=$db->numrows($results);
print "count=".$count."<br /><br />";
while( $row=$db->mysql_array($results) )
{

    print "<a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a>-".$row['titleEN']."-".$row['number']."<br />";
    //admin_utils::add_relation($db,1,$editionid,25,$row['paintID'],"");
    //$db->query("UPDATE ".TABLE_PAINTING." SET enable=1 WHERE paintID='".$row['paintID']."' ");
    //$db->query("UPDATE ".TABLE_PAINTING." SET edition_individual=1 WHERE paintID='".$row['paintID']."' ");
    //$db->query("UPDATE ".TABLE_PAINTING." SET artwork_notesEN='".$row1['artwork_notesEN']."', artwork_notesDE='".$row1['artwork_notesDE']."', artwork_notesFR='".$row1['artwork_notesFR']."', artwork_notesIT='".$row1['artwork_notesIT']."', artwork_notesCH='".$row1['artwork_notesCH']."' WHERE paintID='".$row['paintID']."' ");
}
*/


/*
exit;
$bookid=123;
$results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE id='".$bookid."' ");
while( $row=$db->mysql_array($results) )
{
    $results_literature=$db->query("SELECT r.relationid,r.typeid1,r.itemid1,r.typeid2,r.itemid2 FROM ".TABLE_RELATIONS." r WHERE ( r.typeid1=12 AND r.itemid1='".$row['id']."' AND r.typeid2=1 ) OR ( r.typeid2=12 AND r.itemid2='".$row['id']."' AND r.typeid1=1 ) ");
    $count_literature=$db->numrows($results_literature);
    if( $count_literature>0 )
    {
        while($row_literature=$db->mysql_array($results_literature))
        {
            $paintid=UTILS::get_relation_id($db,"1",$row_literature);

            $query_rel_lit_where=" WHERE relationid='".$row_literature['relationid']."' ";
            $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
            $results_rel_lit=$db->query($query_rel_lit['query']);
            $count_rel_lit=$db->numrows($results_rel_lit);
            $row_rel_lit=$db->mysql_array($results_rel_lit,0);
            //print_r($row_rel_lit);



    # RELATION adding
    $relationid=admin_utils::add_relation($db,12,1378,1,$paintid,$_POST['relations_sort']);
    #end RELATION adding

    if( !empty($relationid) && ( !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) || !empty($row_rel_lit['show_mentioned']) || !empty($row_rel_lit['show_discussed']) ) )
    {
        $db->query("INSERT INTO ".TABLE_RELATIONS_LITERATURE."(relationid,illustrated,illustrated_bw,show_illustrated,show_bw,mentioned,show_mentioned,discussed,show_discussed,date_created) VALUES('".$relationid['relationid']."','".$row_rel_lit['illustrated']."','".$row_rel_lit['illustrated_bw']."','".$row_rel_lit['show_illustrated']."','".$row_rel_lit['show_bw']."','".$row_rel_lit['mentioned']."','".$row_rel_lit['show_mentioned']."','".$row_rel_lit['discussed']."','".$row_rel_lit['show_discussed']."',NOW()) ");
    }




        }

    }

}
*/

/*
$error_count=0;;
//$query=$db->query("SELECT * FROM ".TABLE_MUSEUM." WHERE cityid='' OR cityid IS NULL OR cityid=0 ");
$query=$db->query("SELECT * FROM ".TABLE_MUSEUM."  ");
$count=$db->numrows($query);
while($row=$db->mysql_array($query))
{
    $error=0;
    $error1=0;
    $error2=0;
    $error3=0;
    $row=UTILS::html_decode($row);
    $house=str_replace(" ,",",",$row['museum']);
    $house=str_replace(", ",",",$house);
    $house=str_replace(" , ",",",$house);
    $tmp=explode(",",$house);

    if( count($tmp)==4 )
    {
        $house=$tmp[0].", ".$tmp[1];
        $city=$tmp[2];
        $country=$tmp[3];
    }
    elseif( count($tmp)==3 )
    {
        $house=$tmp[0];
        $city=$tmp[1];
        $country=$tmp[2];
    }
    else
    {
        //print_r($tmp);
        $error=1;
        $house=$tmp[0].", ".$tmp[1];
        $city=$tmp[2];
        $country=$tmp[3];
    }

    $query_where_location=" WHERE location_en='".addslashes($house)."' ";
    $query_location=QUERIES::query_locations($db,$query_where_location);
    $results_location=$db->query($query_location['query_without_limit']);
    $count_location=$db->numrows($results_location);
    $row_location=$db->mysql_array($results_location);

    //if( !$count_location )
    if( empty($row['locationid']) )
    {
        $error=1;
        $error1=1;
    }

    $query_where_city=" WHERE city_en='".$city."' ";
    $query_city=QUERIES::query_locations_city($db,$query_where_city);
    $results_city=$db->query($query_city['query_without_limit']);
    $count_city=$db->numrows($results_city);
    $row_city=$db->mysql_array($results_city);

    //if( !$count_city )
    if( empty($row['cityid']) )
    {
        $error=1;
        $error2=1;
    }

    $query_where_country=" WHERE country_en='".$country."' ";
    $query_country=QUERIES::query_locations_country($db,$query_where_country);
    $results_country=$db->query($query_country['query_without_limit']);
    $count_country=$db->numrows($results_country);
    $row_country=$db->mysql_array($results_country);

    //if( !$count_country )
    if( empty($row['countryid']) )
    {
        $error=1;
        $error3=1;
    }

    if( $error>0 )
    {
        $style="color:red;";
        $error_count++;
        if( $error1!=1 )
        {
            //$db->query("UPDATE ".TABLE_MUSEUM." SET locationid='".$row_location['locationid']."' WHERE muID='".$row['muID']."' LIMIT 1 ");
        }
        if( $error2!=1 )
        {
            //$db->query("UPDATE ".TABLE_MUSEUM." SET cityid='".$row_city['cityid']."' WHERE muID='".$row['muID']."' LIMIT 1 ");
        }
        if( $error3!=1 )
        {
            //$db->query("UPDATE ".TABLE_MUSEUM." SET countryid='".$row_country['countryid']."' WHERE muID='".$row['muID']."' LIMIT 1 ");
        }
    }
    else
    {
        $style="";
        //$db->query("UPDATE ".TABLE_MUSEUM." SET locationid='".$row_location['locationid']."', cityid='".$row_city['cityid']."', countryid='".$row_country['countryid']."' WHERE muID='".$row['muID']."' LIMIT 1 ");
    }

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$house;
    $house_search=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $house=$db->db_replace_quotes($db,$house);

    //$db->query("UPDATE ".TABLE_MUSEUM." SET house='".$house."' WHERE ahID='".$row['ahID']."' LIMIT 1 ");

    //print "<p style='".$style."'>";
    print "full - <a href='/admin/museums/edit/?museumid=".$row['muID']."' target='_blank' >".$row['museum']."</a><br />";
    if( $error1==1 ) $style_1="color:red;";
    else $style_1="";
    print "<span style='".$style_1."'>museum</span> - ".$house."<br />";
    print "locationid - ".$row_location['locationid']."<br />";
    if( $error2==1 ) $style_2="color:red;";
    else $style_2="";
    print "<span style='".$style_2."'>city</span> - ".$city."<br />";
    print "cityid - ".$row_city['cityid']."<br />";
    if( $error3==1 ) $style_3="color:red;";
    else $style_3="";
    print "<span style='".$style_3."'>country</span> - ".$country."<br />";
    print "countryid - ".$row_country['countryid']."<br />";
    print "count - ".count($tmp)."<br />";
    print "museumid - ".$row['muID']."<br />";
    //print "</p>";
    print "<hr style='".$style."' /><br />";

}
print "error_count=".$error_count."<br />";
print "total_count=".$count."<br />";



exit;
 */

# to extract size -each, featured
/*
function searcharray($value, $key, $array) {
       foreach ($array as $k => $val) {
                  if ($val[$key] == $value) {
                                 return $k;
                                        }
                     }
          return null;
}

$query_paintings="SELECT paintID,height,width,size,size_typeid FROM ".TABLE_PAINTING." WHERE size_typeid!=''  ";
//$query_paintings.=" WHERE paintID=4683 OR paintID=6677  ";
$query_paintings_total=$query_paintings;
//$query_paintings.="LIMIT 600";
$results=$db->query($query_paintings);
$results_total=$db->query($query_paintings_total);
$count=$db->numrows($results_total);
$count_limit=$db->numrows($results);
print "count=".$count."<br />";
print "count limit=".$count_limit."<br />";
$sizes_clean=array();
$i=0;
$count_total=0;
while( $row=$db->mysql_array($results) )
{
    $size=$row['size'];
    $size_clean=strtolower($size);
    $size_clean=str_replace(" ","",$size_clean);
    $size_clean=str_replace("x","",$size_clean);
    $size_clean=str_replace("cm","",$size_clean);
    $size_clean=preg_replace('/[^a-z]/i', '', $size_clean);

    $size_type=UTILS::painting_size_info($db,$row['size_typeid']);
    $size_clean=$size_type;
    $size.=" (".$size_type.")";

    //if( !empty($size_clean) )
    if( !empty($size_type) )
    {
        if( $sizes_clean[$size_clean]['count']==0 )
        {
            $sizes_clean[$size_clean]['size']=$size;
            $sizes_clean[$size_clean]['size_clean']=$size_clean;
            $sizes_clean[$size_clean]['count']++;
            $count_total++;
            $sizes_clean[$size_clean]['paintings'][]=$row['paintID'];
            $i++;
        }
        else
        {
            //print "|-".$found_key."-|";
            $sizes_clean[$size_clean]['count']++;
            $count_total++;
            $sizes_clean[$size_clean]['paintings'][]=$row['paintID'];

        }
    }
}

print "found versions=".count($sizes_clean)."<br />";
print "found version paintings=".$count_total."<br /><br />";

//print_r($sizes_clean);

foreach($sizes_clean AS $size)
{
    $search="Framed: ";
    if( strpos($size['size'],$search) !== false )
    {
        $style="style='color:green;'";
        $update=1;
    }
    else
    {
        $style="";
        $update=0;
    }
    //print "<strong>paintid-</strong><a href='/admin/artworks/edit/?paintid=".$row['paintID']."' target='_blank'>".$row['paintID']."</a><br />";
    //print "<strong>height-</strong>".$row['height']."<br />";
    //print "<strong>width-</strong>".$row['width']."<br />";
    //print "<strong>size-</strong>".$row['size']."<br />";
    print "<strong>size-</strong>".$size['size']."<br />";
    print "<strong>count-</strong>".$size['count']."<br />";

    //print "<strong>size clean-</strong>|".$size['size_clean']."|<br />";


    print "<br />___________________ START - ".$size['size']." _________________________<br />";
    foreach( $size['paintings'] as $paintid )
    {
        print "<strong ".$style.">paintid-</strong><a href='/admin/artworks/edit/?paintid=".$paintid."' target='_blank'>".$paintid."</a><br />";
        if( $update==1 )
        {
            $values_query_painting=array();
            $query_where_painting=" WHERE paintID='".$paintid."' ";
            $query_painting=QUERIES::query_painting($db,$query_where_painting,"","","",$values_query_painting);
            $results_painting=$db->query($query_painting['query']);
            $row_painting=$db->mysql_array($results_painting,0);

            $new_size=str_replace($search,"",$row_painting['size']);
            print "<strong>new_size-</strong>".$new_size."<br />";

            //$db->query("UPDATE ".TABLE_PAINTING." SET size_typeid=3, size='".$new_size."' WHERE paintID='".$paintid."' LIMIT 1 ");
        }
    }
    print "___________________ END - ".$size['size']." _________________________<br />";


    print "<hr /><br />";
}
//print_r($sizes_clean);
exit;
 */

/*
$count_found=0;
$results=$db->query("SELECT exID,src_guide FROM ".TABLE_EXHIBITIONS." ");
while( $row_exhibition=$db->mysql_array($results) )
{
    $results_literature=$db->query("SELECT r.relationid,r.typeid1,r.itemid1,r.typeid2,r.itemid2 FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibition['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibition['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ");
    $count_literature=$db->numrows($results_literature);
    if( $count_literature>0 )
    {
        while( $row_literature=$db->mysql_array($results_literature) )
        {
            $bookid=UTILS::get_relation_id($db,"12",$row_literature);
            $results_related_exh_relations=$db->query("SELECT e.exID FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibition['exID']."' AND r.typeid2=4 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibition['exID']."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND e.exID!='".$row_exhibition['exID']."' ORDER BY e.date_from DESC ");
            $count_related_exh_relations=$db->numrows($results_related_exh_relations);
            if( $count_related_exh_relations>0 )
            {
                while( $row_exh=$db->mysql_array($results_related_exh_relations) )
                {
                    //$exhibitionid=UTILS::get_relation_id($db,"4",$row_exh);
                    $exhibitionid=$row_exh['exID'];

                    if( $exhibitionid==$bookid && !empty($exhibitionid) && !empty($bookid) )
                    {
                        print "Exhibition - <a href='/admin/exhibitions/edit/?exhibitionid=".$exhibitionid."' traget='_blank'>".$exhibitionid."</a><br />";
                        print "Book - <a href='/admin/books/edit/?bookid=".$bookid."' traget='_blank'>".$bookid."</a>";
                        print "<hr /><br /><br />";
                        //exit;
                        $count_found++;
                    }
                    //print $exhibitionid."-".$bookid."<br />";
                }

            }
        }
    }
}
print "found faulty = ".$count_found;
*/

# update rekation column for exhibition table

/*
$results=$db->query("SELECT exID,src_guide FROM ".TABLE_EXHIBITIONS." ");
$count=0;
while( $row=$db->mysql_array($results) )
{
    # check if exhibtion has some relation
    $values_exhibition_rel=array();
    $values_exhibition_rel['exhibitionid']=$row['exID'];
    $values_exhibition_rel['src_guide']=$row['src_guide'];
    $relations=UTILS::check_relations($db,$values_exhibition_rel);
    if( $relations>=5 )
    {
        $count++;
        print "<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' target='_blank'>".$row['exID']."</a><br />";
    }
    # end
}
print $count;
exit;

*/


/*
exit;
$results=$db->query("SELECT exID,src_guide FROM ".TABLE_EXHIBITIONS." ");
while( $row=$db->mysql_array($results) )
{
    # check if exhibtion has some relation
    $values_exhibition_rel=array();
    $values_exhibition_rel['exhibitionid']=$row['exID'];
    $values_exhibition_rel['src_guide']=$row['src_guide'];
    $relations=UTILS::check_relations($db,$values_exhibition_rel);
    if( $relations )
    {
        $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=1 WHERE exID = '".$row['exID']."'");
    }
    else
    {
        $db->query("UPDATE ".TABLE_EXHIBITIONS." SET relations=0 WHERE exID = '".$row['exID']."'");
    }
    # end
}
 */

/*
$error_count=0;;
//$query=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." WHERE cityid='' OR cityid IS NULL OR cityid=0 ");
$query=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." ");
$count=$db->numrows($query);
while($row=$db->mysql_array($query))
{
    $error=0;
    $row=UTILS::html_decode($row);
    $house=str_replace(" ,",",",$row['house']);
    $house=str_replace(", ",",",$house);
    $house=str_replace(" , ",",",$house);
    $tmp=explode(",",$house);
    if( count($tmp)!=3 )
    {
        print_r($tmp);
        $error=1;
    }
    $house=$tmp[0];
    $city=$tmp[1];
    $country=$tmp[2];


    $query_where_city=" WHERE city_en='".$city."' ";
    $query_city=QUERIES::query_locations_city($db,$query_where_city);
    $results_city=$db->query($query_city['query_without_limit']);
    $count_city=$db->numrows($results_city);
    $row_city=$db->mysql_array($results_city);

    if( !$count_city ) $error=1;

    $query_where_country=" WHERE country_en='".$country."' ";
    $query_country=QUERIES::query_locations_country($db,$query_where_country);
    $results_country=$db->query($query_country['query_without_limit']);
    $count_country=$db->numrows($results_country);
    $row_country=$db->mysql_array($results_country);

    if( !$count_country ) $error=1;

    if( $error )
    {
        $style="color:red;";
        $error_count++;
    }
    else
    {
        $style="";
        //$db->query("UPDATE ".TABLE_AUCTIONHOUSE." SET cityid='".$row_city['cityid']."', countryid='".$row_country['countryid']."' WHERE ahID='".$row['ahID']."' LIMIT 1 ");
    }

    # prepare search values
    $values_accented=array();
    $values_accented['text']=$house;
    $house_search=UTILS::replace_accented_letters($db, $values_accented);
    # END prepare search values

    $house=$db->db_replace_quotes($db,$house);

    //$db->query("UPDATE ".TABLE_AUCTIONHOUSE." SET house='".$house."' WHERE ahID='".$row['ahID']."' LIMIT 1 ");

    print "<p style='".$style."'>";
    print "full - <a href='/admin/auctionhouse/edit/?auctionhouseid=".$row['ahID']."' >".$row['house']."</a><br />";
    print "house - ".$house."<br />";
    print "city - ".$city."<br />";
    print "cityid - ".$row_city['cityid']."<br />";
    print "country - ".$country."<br />";
    print "countryid - ".$row_country['countryid']."<br />";
    print "</p>";
    print "<hr /><br />";

}
print "error_count=".$error_count."<br />";
print "total_count=".$count."<br />";
 */
exit;
    $query_paintings=QUERIES::query_painting($db);
    $results_atlas=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." r WHERE ( ( r.typeid1=1 AND r.typeid2=14 ) OR ( r.typeid2=1 AND r.typeid1=14 ) ) ");
        $count_atlas=$db->numrows($results_atlas);
    print "total_associated_paintings=".$count_atlas."<br /><br />";
    while( $row=$db->mysql_array($results_atlas) )
    {
        $paintid=UTILS::get_relation_id($db,"1", $row);
        $atlas_paintid=UTILS::get_relation_id($db,"14", $row);
        $results_atlas_assoc=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." r WHERE ( ( r.typeid1=1 AND r.itemid1='".$atlas_paintid."' AND r.typeid2=13 AND  r.itemid2='".$paintid."' ) OR ( r.typeid2=1 AND r.itemid2='".$atlas_paintid."' AND r.typeid1=13 AND r.itemid1='".$paintid."' ) ) ");
        $count_atlas_assoc=$db->numrows($results_atlas_assoc);
        if( $count_atlas_assoc<=0 )
        {
            $style="color:red;font-weight:700;";
            //else $style="color:green;";
            print "<span style='".$style.";'>count-".$count_atlas_assoc."</span><br />";
            print "paintid=<a href='/admin/artworks/edit/?paintid=".$paintid."&relation_typeid=14'>".$paintid."</a> | atlas_paintid=<a href='/admin/artworks/edit/?paintid=".$atlas_paintid."&relation_typeid=13'>".$atlas_paintid."</a><br />";
            //admin_utils::add_relation($db,1,$atlas_paintid,13,$paintid,$_POST['relations_sort']);

        }
        //exit;
    }
exit;

    $query_paintings=QUERIES::query_painting($db);
    $results_atlas=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." r WHERE ( ( r.typeid1=1 AND r.typeid2=13 ) OR ( r.typeid2=1 AND r.typeid1=13 ) ) ");
        $count_atlas=$db->numrows($results_atlas);
    print "total_atlas=".$count_atlas."<br /><br />";
    while( $row=$db->mysql_array($results_atlas) )
    {
        $paintid=UTILS::get_relation_id($db,"1", $row);
        $atlas_paintid=UTILS::get_relation_id($db,"13", $row);
        $results_atlas_assoc=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." r WHERE ( ( r.typeid1=1 AND r.itemid1='".$atlas_paintid."' AND r.typeid2=14 AND  r.itemid2='".$paintid."' ) OR ( r.typeid2=1 AND r.itemid2='".$atlas_paintid."' AND r.typeid1=14 AND r.itemid1='".$paintid."' ) ) ");
        $count_atlas_assoc=$db->numrows($results_atlas_assoc);
        if( $count_atlas_assoc<=0 )
        {
            $style="color:red;font-weight:700;";
            //else $style="color:green;";
            print "<span style='".$style.";'>count-".$count_atlas_assoc."</span><br />";
            print "paintid=<a href='/admin/artworks/edit/?paintid=".$paintid."&relation_typeid=13'>".$paintid."</a> | atlas_paintid=<a href='/admin/artworks/edit/?paintid=".$atlas_paintid."&relation_typeid=14'>".$atlas_paintid."</a><br />";
            //admin_utils::add_relation($db,1,$atlas_paintid,14,$paintid,$_POST['relations_sort']);

        }
        //exit;
    }

exit;


    //$query_rel_lit_where=" WHERE relationid_lit='10613' ";
    $query_rel_lit=QUERIES::query_relations_literature($db,$query_rel_lit_where);
    $results_rel_lit=$db->query($query_rel_lit['query']);
    $count=$db->numrows($results_rel_lit);
    while($row=$db->mysql_array($results_rel_lit))
{
    if( $row['illustrated_typeid'] )
    {
        $illustrated_bw="NULL";
        $illustrated="'".$row['illustrated']."'";
    }
    else
    {
        $illustrated_bw="'".$row['illustrated']."'";
        $illustrated="NULL";
    }
    $query_update="UPDATE ".TABLE_RELATIONS_LITERATURE." SET illustrated=".$illustrated.", illustrated_bw=".$illustrated_bw." WHERE relationid_lit='".$row['relationid_lit']."' ";
    print $query_update."<br /><br />";
    $db->query($query_update);
}


exit;
$results=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE artworkID=6 ");
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    if( !empty($imageid) )
    {
        $values_query_painting_opp=array();
        $values_query_painting_opp['where']=" WHERE paintid='".$row['paintID']."' ";
        $query_painting_opp=QUERIES::query_painting_opp($db,$values_query_painting_opp);
        $results_opp=$db->query($query_painting_opp['query']);
        $count_opp=$db->numrows($results_opp);
        $row_opp=$db->mysql_array($results_opp,0);

        if( !$count_opp )
        {
            $query_opp_add=" INSERT INTO ".TABLE_PAINTING_OPP."(paintid,date_created) VALUES('".$row['paintID']."',NOW()); ";
            $db->query($query_opp_add);
            $oppinfoid=$db->return_insert_id();
        }
        else $oppinfoid=$row_opp['oppinfoid'];

        $src_original=HTTP_SERVER."/images/size_o__imageid_".$imageid.".jpg";
        list($width, $height) = getimagesize($src_original);
        if( empty($width) ) $width="NULL";
        if( empty($height) ) $height="NULL";

        $query="UPDATE ".TABLE_PAINTING_OPP." SET width='".$width."', height='".$height."' WHERE oppinfoid='".$oppinfoid."' ";
        $db->query($query);
        //print $query."-".$row['paintID']."-<br /><br />";
    }
}


exit;
$results=$db->query("SELECT paintID,number FROM ".TABLE_PAINTING." ");
while( $row=$db->mysql_array($results) )
{
    $values=array();
    if( empty($row['number']) )
    {
        //$values['number']=$maxsort;
        #checking if date is not empty
        if( !empty($row['date']) && $row['date']!="0000-00-00" ) $values['date']=$row['date'];
        elseif( !empty($row['year']) ) $values['year']=$row['year'];
    }
    else $values['number']=$row['number'];
    $values['year_drawings']=$row['year'];
    $row_artwork=UTILS::get_artwork_info($db,$row['artworkID']);
    if( $row_artwork['artworkID']==1 || $row_artwork['artworkID']==2 || $row_artwork['artworkID']==13 ) $values['artwork_sort']=1;
    else $values['artwork_sort']=$row_artwork['sort'];
    $values['artworkid']=$row_artwork['artworkID'];
    $values['title_en']=$row['titleEN'];
    $values['paintid']=$row['paintID'];
    $values['return_array']=1;
    $sort=UTILS::painting_sort_number($db,$values);
    $number_search=$sort[0]%10000;
    $number_search2=$sort[1];


    //if( empty($number_search2) ) $number_search2="NULL";
    if( empty($number_search2) ) $number_search2=$number_search;
    else $number_search2="'".$number_search2."'";
    //$db->query("UPDATE painting SET number_search2=".$number_search2." WHERE paintID='".$row['paintID']."' ");
}

exit;
$results=$db->query("SELECT * FROM ".TABLE_BOOKS." WHERE books_catid=2 OR books_catid=3 ");

while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);
    $row=$db->db_prepare_input($row);

    $results_lit_rel_exh=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." r WHERE ( r.typeid1=12 AND r.itemid1='".$row['id']."' AND r.typeid2=4 ) OR ( r.typeid2=12 AND r.itemid2='".$row['id']."' AND r.typeid1=4 ) ");
    //$count_lit_rel_exh=$db->numrows($results_lit_rel_exh);
    $exhibitions=array();
    while( $row_lit_rel_exh=$db->mysql_array($results_lit_rel_exh) )
    {
        $exhibitionid=UTILS::get_relation_id($db,"4",$row_lit_rel_exh);
        $exhibitions[]=$exhibitionid;
    }

    foreach( $exhibitions as $exhibitionid )
    {
        print "<a href='/admin/exhibitions/edit/?exhibitionid=".$exhibitionid."' target='_blank'>".$exhibitionid."</a><br />";
        foreach( $exhibitions as $exhibitionid2 )
        {
            # linking this exhibtiion to the book catalogu exhibitions
            //admin_utils::add_relation($db,4,$exhibitionid,4,$exhibitionid2,$_POST['relations_sort']);
        }
    }
    print "<hr />";

}






exit;
$results=$db->query("SELECT id,title,title_periodical,title_display FROM ".TABLE_BOOKS." ");

while( $row=$db->mysql_array($results) )
{
    $row=UTILS::html_decode($row);
    $row=$db->db_prepare_input($row);


$options_make_slugs=array();
$options_make_slugs['delimiter']="-";
$options_make_slugs['maxlen']=55;
if( !empty($row['title']) ) $title_slugs=$row['title'];
elseif( !empty($row['title_periodical']) ) $title_slugs=$row['title_periodical'];
elseif( !empty($row['title_display']) ) $title_slugs=$row['title_display'];
$options_make_slugs['text']=$title_slugs;
$titleurl=UTILS::replace_accented_letters($db,$options_make_slugs);
if( !empty($titleurl['text_lower_converted']) ) $titleurl['text_lower_converted'].=$options_make_slugs['delimiter'];
$titleurl=$titleurl['text_lower_converted'].$row['id'];


    $query="UPDATE ".TABLE_BOOKS." SET titleurl='".$titleurl."' WHERE id='".$row['id']."' LIMIT 1 ";
    $db->query($query);
    print $query."<br /><br />";
}

exit;

# update exhibition catalogues with linked literature exh catalog images
$results=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." ");
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    if( empty($imageid) )
    {
        $query_literature=QUERIES::query_books($db);
        $results_literature=$db->query("SELECT ".$query_literature['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=4 AND r.itemid1='".$row['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ORDER BY DATE_FORMAT(b.link_date, '%Y') AND (b.books_catid=2 OR b.books_catid=3) DESC, isnull ASC, b.author ASC LIMIT 1 ");
        $count_literature=$db->numrows($results_literature);
        if( $count_literature>0 )
        {
            $row_lit=$db->mysql_array($results_literature);
            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row_lit['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row_lit['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1");
            $count_images=$db->numrows($results_related_image);
            if( $count_images>0 )
            {
                $row_related_image=$db->mysql_array($results_related_image);
                $imageid_lit=UTILS::get_relation_id($db,"17", $row_related_image);
                $src_xs="/images/size_xs__imageid_".$imageid_lit.".jpg";
                $src_orginal=DATA_PATH."/images_new/original/".$imageid_lit.".jpg";
                print "\t<a href='/admin/exhibitions/edit/?exhibitionid=".$row['exID']."' title=''>".$row['exID']."</a> - <a href='/admin/books/edit/?bookid=".$row_lit['id']."' title=''>".$row_lit['id']."</a>\n";
                print "\t<img src='".$src_xs."' alt=''><br />\n";


                # adding imgae
                $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
                $imageid=$db->return_insert_id();

                $fileName=basename($src_orginal);
                $tmpName=$src_orginal;
                $getimagesize = getimagesize($tmpName);

                switch( $getimagesize['mime'] )
                {
                    case 'image/gif'  : $ext = ".gif"; break;
                    case 'image/png'  : $ext = ".png"; break;
                    case 'image/jpeg' : $ext = ".jpg"; break;
                    case 'image/bmp'  : $ext = ".bmp"; break;
                    default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
                }

                $dir=DATA_PATH."/images_new";
                $new_file_name=$imageid.".jpg";
                $new_file_path=$dir."/original/".$imageid.$ext;

                UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
                UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
                UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
                UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
                UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

                #copy original
                if (!copy($tmpName, $new_file_path))
                {
                    exit("Error Uploading File.");
                }
                else
                {
                    $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                    # RELATION adding
                    admin_utils::add_relation($db,17,$imageid,4,$row['exID'],$_POST['relations_sort']);
                    #end RELATION adding

                    # create cache image
                    $src="/images/imageid_".$imageid."__thumb_1__size_o__border_1__maxwidth_38__maxheight_45.jpg";
                    $src_cache_uri=$imageid.".jpg";
                    UTILS::cache_uri($src,$src_cache_uri,1);
                    #end
                }
                #END
            }
        }
    }

}

exit;


# resize images for exhibiton detail page exhibition poster
$results=$db->query("SELECT exID,locationid,cityid,countryid FROM ".TABLE_EXHIBITIONS." ");
while( $row=$db->mysql_array($results) )
{
    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    if( !empty($imageid) )
    {
                # create cache image
                $src="/images/imageid_".$imageid."__thumb_1__size_o__border_1__maxwidth_38__maxheight_45.jpg";
                $src_cache_uri=$imageid.".jpg";
                UTILS::cache_uri($src,$src_cache_uri,1);
                #end

        print "<img src='".$src."' alt='NOIMAGE-".$row['exID']."' />";
        //print $row['exID']."<br />";
    }

}


//$html->foot();
?>
