<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="main_menu";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();
?>



<div id="div-admin-home">
    <div class="div-admin-home-col">

        <div class="admin-option">
        	<h2>Paintings</h2>
        	<ul>
        			<li><a href="/admin/paintings/types" title="Types" >Types</a></li>
        			<li><a href="/admin/paintings/edit" title="Add Artworks" >add new</a></li>
        			<!--<li><a href="/admin/paintings/edit/sort" title="Sort Artworks" >sort</a><br /></li>-->
        			<!--<li><a href="/admin/search/paintings.php" title="Edit Artworks" >edit</a></li>-->
        			<li><a href="/admin/paintings/categories" title="Categories" >categories</a></li>
        			<li><a href="/admin/paintings/photos/" title="Painting photos" >photos (detail photographs)</a></li>
        			<li><a href="/admin/paintings/photos/edit" title="Add photo" >add photo</a></li>
        			<li><a href="/admin/paintings/mediums" title="Mediums" >mediums</a></li>
        			<li><a href="/admin/paintings/colors" title="Colors" >colors</a></li>
                    <li><a href="/admin/paintings/colors/edit" title="Add colors" >add color</a></li>
        			<li><a href="/admin/paintings/notes" title="Artwork notes images" >Artwork notes images</a></li>
        			<li><a href="/admin/paintings/notes/edit/" title="Add artwork note image" >Add note image</a></li>
        	</ul>
        </div>

        <div class="admin-option">
        	<h2>Microsites</h2>
        		<ul>
        			<li><a href="/admin/microsites" title="Microsites" >Microsites</a></li>
        			<li><a href="/admin/microsites/versions" title="Microsites versions" >Microsites versions</a></li>
        <!--
        			<li><a href="/admin/microsites/4900_colours" title="4900 Colours" >4900 Colours</a></li>
        			<li><a href="/admin/microsites/sinbad" title="Sinbad" >Sinbad</a></li>
        			<li><a href="/admin/microsites/elbe" title="Elbe" >Elbe</a></li>-->
        			<!--<li><a href="/admin/microsites/" title="" >&nbsp;</a></li>
        			<li><a href="/admin/microsites/" title="" >&nbsp;</a></li>-->
        		</ul>
        </div>       
        
        <div class="admin-option">
        	<h2>Biography</h2>
        		<ul>
        			<li><a href="/admin/biography/edit" title="Add biography" >add new</a></li>
        			<li><a href="/admin/biography" title="Modify biography" >edit</a></li>
                </ul>
        </div>
        
        <div class="admin-option">
        	<h2>Photos</h2>
        	<ul>
        			<li><a href="/admin/photos/edit/" title="Add Photos" >add new</a></li>
        			<li><a href="/admin/photos" title="Add Photos" >edit</a></li>
        	</ul>
        </div>
        
        <div class="admin-option">
        	<h2>Quotes</h2>
        	<ul>
        		<li><a href="/admin/quotes/edit" title="Add quote" >add new quote</a></li>
        		<li><a href="/admin/quotes" title="Modify quote" >edit quotes</a></li>
        		<li><a href="/admin/quotes/categories/edit" title="Add quote category" >add new category</a></li>
        		<li><a href="/admin/quotes/categories" title="Modify quote category" >edit categories</a></li>
            </ul>
        </div>

    </div><!-- div-admin-home-col -->
    <div class="div-admin-home-col">

        <div class="admin-option">
        	<h2>Exhibitions</h2>
        		<ul>
        			<li><a href="/admin/exhibitions" title="Modify exhibitions" >exhibitions</a></li>
        			<li><a href="/admin/exhibitions/edit" title="Add exhibition to database" >add new</a></li>
                </ul>
        	<h2>Exhibitions installations</h2>
        		<ul>
        			<li><a href="/admin/exhibitions/installations/" title="Exhibitions installations" >exhibitions installations</a></li>
        			<li><a href="/admin/exhibitions/installations/edit" title="Add exhibitions installations images" >add new</a></li>
                </ul>
        </div>
        
        <div class="admin-option">
        	<h2>Literature</h2>
        		<ul>
        			<li><a href="/admin/books/edit" title="Add new book" >add new</a></li>
        			<li><a href="/admin/books" title="Modifie literature info" >edit</a></li>
                    <li><a href="/admin/books/categories/edit" title="Add books category" >add new category</a></li>
                    <li><a href="/admin/books/categories" title="Modify books category" >edit categories</a></li>
        			<li><a href="/admin/books/edit/sort" title="Sort literature" >sort selected titles</a></li>
        			<li><a href="/admin/books/sort-publications" title="Sort news publications" >sort news publications</a></li>
        			<li><a href="/admin/books/languages/" title="Literature languages" >languages</a></li>
        			<li><a href="/admin/books/languages/edit/" title="Add literature language" >add literature language</a></li>
        			<li><a href="/admin/books/broken-links/" title="Check for broken links" >broken links</a></li>
        		</ul>
        </div>
        
        <div class="admin-option">
        	<h2>Videos</h2>
        		<ul>
        			<li><a href="/admin/videos/edit" title="Add video to site" >add new</a></li>
        			<li><a href="/admin/videos" title="Modify videos" >edit</a></li>
        		</ul>
<!--
        	<h2>Related Videos</h2>
        		<ul>
        			<li><a href="/admin/related_videos/edit" title="Add related video to site" >add new</a></li>
        			<li><a href="/admin/related_videos" title="Modify related videos" >edit</a></li>
        		</ul>-->
        </div>
        
        <div class="admin-option">
        	<h2>Audio</h2>
        		<ul>
        			<li><a href="/admin/audio/edit" title="Add audio to site" >add new</a></li>
        			<li><a href="/admin/audio" title="Modify audio" >edit</a></li>
        		</ul>
        </div>
        
        
        <div class="admin-option">
        	<h2>Talks</h2>
        		<ul>
        			<li><a href="/admin/news/edit" title="Add news to database" >add new</a></li>
        			<li><a href="/admin/news" title="Modify news" >edit</a></li>
        		</ul>
        </div>

    </div><!-- div-admin-home-col -->
    <div class="div-admin-home-col">

        <div class="admin-option">
        	<h2>Search</h2>
        	<ul>
        			<!--<li><a href="/admin/search/paintings.php" title="Serch artworks" >Search artworks</a></li>-->
        			<li><a href="/admin/search" title="Serch database" >Search</a></li>
        			<!--<li><a href="/admin/search-test" title="Serch test" >Search test</a></li>-->
        	</ul>
        </div>
        
        <div class="admin-option">
        	<h2>Locations and Collections</h2>
        		<ul>
        			<!--<li><a href="/admin/museums" title="Museums" >museums</a></li>
                    <li><a href="/admin/museums/edit" title="Add museum" >add museum</a></li>-->
        			<li><a href="/admin/locations" title="Locations" >locations</a></li>
        			<li><a href="/admin/locations/edit/?typeid=1" title="Add location" >add location</a></li>
        		</ul>
        </div>
        
        <div class="admin-option">
        	<h2>Auctions | Sales</h2>
        	<ul>
        			<li><a href="/admin/auctions-sales/saleHistory" title="Sale history" >Sales history</a></li>
        			<li><a href="/admin/auctions-sales/saleHistory/edit/" title="Add sale" >Add sale</a><br /></li>
                    <li><a href="/admin/auctions-sales/auctionhouse" title="Auction houses" >auction houses</a></li>
                    <li><a href="/admin/auctions-sales/currencys" title="" >currencies</a></li>
        	</ul>
        </div>
        
        <div class="admin-option">
        	<h2>Texts</h2>
        	<ul>
        			<li><a href="/admin/pages/edit/?textid=1" title="Credits" >credits</a></li>
        			<li><a href="/admin/pages/edit/?textid=2" title="Disclaimer" >disclaimer</a></li>
        			<li><a href="/admin/pages/edit/?textid=3" title="Chronology" >chronology</a></li>
        			<li><a href="/admin/pages/edit/?textid=4" title="Dealers" >links/dealers</a></li>
                    <li><a href="/admin/pages/edit/?textid=5" title="Home page" >home page</a></li>
        	</ul>
        </div>
        
        <div class="admin-option">
        	<h2>All Images</h2>
        	<ul>
        			<li><a href="/admin/images" title="Images" >Images</a></li>
        	</ul>
        </div>
        
        <!--
        <div class="admin-option">
        	<h2>Zoomer</h2>
        	<ul>
        			<li><a href="/admin/zoomer" title="View zoomer images" >zoomer images</a></li>
        			<li><a href="/admin/zoomer/edit" title="Add Photos" >add zoomer images</a></li>
        	</ul>
        </div>
        -->
        
        <div class="admin-option">
        	<h2>Site</h2>
        	<ul>
        			<li><a href="/admin/management/users" title="Users" >Manage users</a></li>
                    <!--<li><a href="/admin/users" title="Users" >View admin user site access stats</a></li>-->
                    <!--<li><a href="/admin/stats/reports" title="Users" >View site access stats</a></li>-->
        	</ul>
        </div>
        
        <div class="admin-option">
        	<h2>LOGOUT</h2>
        	<ul>
        			<li><a href="/admin/sign_out" title="Logout" >Logout</a></li>
        	</ul>
        </div>


    </div><!-- div-admin-home-col -->
</div><!-- div-admin-home -->


<?php
        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
