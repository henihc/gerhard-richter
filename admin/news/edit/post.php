<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

//$_SESSION['new_record']=$_POST;
//if( empty($_POST['titleEN']) && empty($_POST['titleDE']) ) $_SESSION['new_record']['error']['title']=true;
//if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/news/edit/?newsid=".$_POST['newsid']."&error=true");

$db=new dbCLASS;


$textEN=$db->db_prepare_input($_POST['textEN'],1);
$textDE=$db->db_prepare_input($_POST['textDE'],1);
$textFR=$db->db_prepare_input($_POST['textFR'],1);
$textIT=$db->db_prepare_input($_POST['textIT'],1);
$textZH=$db->db_prepare_input($_POST['textZH'],1);

$_POST=UTILS::html_decode($_POST); 
$_POST=$db->db_prepare_input($_POST);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['titleDE'];
$title_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleFR'];
$title_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['titleIT'];
$title_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['textDE'];
$text_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['textFR'];
$text_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['textIT'];
$text_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_NEWS."(created) VALUES(NOW())");  
    $newsid=$db->return_insert_id();
}
elseif( $_POST['submit']=="update" )
{
    $newsid=$_POST['newsid'];
} 
else UTILS::redirect("/");

if( empty($_POST['datefrom']) ) $datefrom="NULL";
else $datefrom="'".$_POST['datefrom']." 23:59:59'";
if( empty($_POST['dateto']) ) $dateto="NULL";
else $dateto="'".$_POST['dateto']." 23:59:59'";

$query="UPDATE ".TABLE_NEWS." SET 
                type='".$_POST['type']."', 
                datefrom=".$datefrom.",
                dateto=".$dateto.",
                time='".$_POST['time']."',
                showdate='".$_POST['showdate']."',
                title_original='".$_POST['title_original']."',
                titleEN='".$_POST['titleEN']."',
                titleDE='".$_POST['titleDE']."',
                titleFR='".$_POST['titleFR']."',
                titleIT='".$_POST['titleIT']."',
                titleZH='".$_POST['titleZH']."',
                title_search_1='".$title_search_de['text_lower_converted']."',
                title_search_2='".$title_search_de['text_lower_german_converted']."',
                title_search_3='".$title_search_fr['text_lower_converted']."',
                title_search_4='".$title_search_it['text_lower_converted']."',
                person='".$_POST['person']."',
                locationid='".$_POST['locationid']."',
                cityid='".$_POST['cityid']."',
                countryid='".$_POST['countryid']."',
                textEN='".$textEN."',
                textDE='".$textDE."', 
                textFR='".$textFR."',
                textIT='".$textIT."',
                textZH='".$textZH."',
                text_search_1='".$text_search_de['text_lower_converted']."',
                text_search_2='".$text_search_de['text_lower_german_converted']."',
                text_search_3='".$text_search_fr['text_lower_converted']."',
                text_search_4='".$text_search_it['text_lower_converted']."'
";
//if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
$query.=" WHERE newsID='".$newsid."' ";

if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,29,$newsid);
    }
    elseif ( $_POST['submit']=="update" ) 
    {
        admin_utils::admin_log($db,2,29,$newsid);
    }
    
    # RELATION adding
    admin_utils::add_relation($db,7,$newsid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding
}
else
{
    admin_utils::admin_log($db,"",29,$_POST['newsid'],2);
}

#image upload
if($_FILES['image']['error']==0)
{

	$fileName = str_replace (" ", "_", $_FILES['image']['name']);
	$tmpName  = $_FILES['image']['tmp_name'];
	$fileSize = $_FILES['image']['size'];
	$fileType = $_FILES['image']['type'];

	if (is_uploaded_file($tmpName))
	{

        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

        $getimagesize = getimagesize($tmpName);
         
        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }
                       
            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT,THUMB_S_WIDTH,$dir.'/small/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT,THUMB_M_WIDTH,$dir.'/medium/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT,THUMB_L_WIDTH,$dir.'/large/'.$new_file_name);
            UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT,THUMB_XL_WIDTH,$dir.'/xlarge/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,7,$newsid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
                //$src_cache_uri=$imageid.".jpg";
                //UTILS::cache_uri($src,$src_cache_uri,1);
                #end
            }

	}

}
#end image upload



# leaflet upload
if($_FILES['leaflet']['error']==0)
{

    $fileName = str_replace (" ", "_", $_FILES['leaflet']['name']);
    $tmpName  = $_FILES['leaflet']['tmp_name'];
    $fileSize = $_FILES['leaflet']['size'];
    $fileType = $_FILES['leaflet']['type'];

    $new_file_path=DATA_PATH."/files/leaflet/".$fileName;

    if( is_uploaded_file($tmpName) )
    {   
        if (!copy($tmpName, $new_file_path))
        {
            exit("Error Uploading File.");
        }
        else $db->query("UPDATE ".TABLE_NEWS." SET leaflet='".$fileName."' WHERE newsID = '".$newsid."'");
    }
}
#end leaflet upload

unset($_SESSION['new_record']);

$url="/admin/news/edit/?newsid=".$newsid."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
