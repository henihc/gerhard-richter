<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";


# menu selected
$html->menu_admin_selected="news";
$html->menu_admin_sub_selected="add_talk";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




$query_where_news=" WHERE newsID='".$_GET['newsid']."' ";
$query_news=QUERIES::query_news($db,$query_where_news,$query_order,$query_limit);
$results=$db->query($query_news['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results,0);
if( $count>0 ) $row['newsid']=$row['newsID'];

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "new DatePicker($$('.datefrom'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_start' });\n";
        print "new DatePicker($$('.dateto'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_end' });\n";
        print "\tCKEDITOR.replace('textEN', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('textDE', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('textFR', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('textIT', { toolbar : 'Large' });\n";
        print "\tCKEDITOR.replace('textZH', { toolbar : 'Large' });\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-title li a','#div-admin-tabs-info-title .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-text li a','#div-admin-tabs-info-text .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";
print "\t</script>\n";


    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            if( $count )
            {   
                print "\t<tr>\n";
                    print "\t<td style='text-align:right;' colspan='2'>\n";
                        $values_url=array();
                        $values_url['newsid']=$row['newsID'];
                        $values_url['location']=1;
                        $news_url=UTILS::get_news_url($db,$values_url);
                        print "\t<a href='".$news_url['url']."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                    print "\t</td>\n";

                print "\t</tr>\n";
            }  

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Type</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $type=$_SESSION['new_record']['type'];
                    else $type=$row['type'];
                    admin_html::select_news_type($db,"type",$type);
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Date</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $datefrom=$_SESSION['new_record']['datefrom'];
                    else $datefrom=$row['datefrom_admin'];
                    print "\t<input id='datefrom' class='datefrom dashboard' name='datefrom' type='text' value='".$datefrom."' />";
                    print "\t<button type='button' class='calendar-icon date_toggler_start'></button>\n";
                    print " - ";
                    if( $_GET['error'] ) $dateto=$_SESSION['new_record']['dateto'];
                    else $dateto=$row['dateto_admin'];
                    print "\t<input id='dateto' class='dateto dashboard' name='dateto' type='text' value='".$dateto."' />";
                    print "\t<button type='button' class='calendar-icon date_toggler_end'></button>\n";
                    if( $_GET['error'] ) $time=$_SESSION['new_record']['time'];
                    else $time=$row['time'];
                    print "\t<input name='time' type='text' value='".$time."' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Show date</th>\n";
                print "\t<td>";
                    if( $row['showdate'] || $_SESSION['new_record']['error']['showdate'] ) $checked=" checked='checked' ";
                    else $checked=""; 
                    print "\t<input id='showdate' name='showdate' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title original</th>\n";
                print "\t<td colspan='2'>";
                    print "\t<textarea id='title_original' name='title_original' cols='109' rows='1'>".$row['title_original']."</textarea>";                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>Title</td>\n";
                print "\t<td>\n";

                    print "\t<ul id='ul-admin-tabs-title' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-title' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleEN=$_SESSION['new_record']['titleEN'];
                            else $titleEN=$row['titleEN'];
                            print "\t<textarea name='titleEN' cols='121' rows='2'>".$titleEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleDE=$_SESSION['new_record']['titleDE'];
                            else $titleDE=$row['titleDE'];
                            print "\t<textarea name='titleDE' cols='121' rows='2'>".$titleDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleFR=$_SESSION['new_record']['titleFR'];
                            else $titleFR=$row['titleFR'];
                            print "\t<textarea name='titleFR' cols='121' rows='2'>".$titleFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleIT=$_SESSION['new_record']['titleIT'];
                            else $titleIT=$row['titleIT'];
                            print "\t<textarea name='titleIT' cols='121' rows='2'>".$titleIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $titleZH=$_SESSION['new_record']['titleZH'];
                            else $titleZH=$row['titleZH'];
                            print "\t<textarea name='titleZH' cols='121' rows='2'>".$titleZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Person</th>\n";
                print "\t<td>";
                    if( $_GET['error'] ) $person=$_SESSION['new_record']['person'];
                    else $person=$row['person'];
                    print "\t<input name='person' type='text' value='".$person."' />";
                print "</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<table class='admin-table' cellspacing='0'>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Country</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="countryid";
                                $values['selectedid']=$row['countryid'];
                                $values['onchange']=" onchange=\"
                                                        $('cityid').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-city').style.display='';\" ";
                                admin_html::select_locations_country($db,$values);
                                if( $count ) UTILS::admin_edit("/admin/locations/edit/?typeid=3&countryid=".$row['countryid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-city' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>City</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="cityid";
                                $values['selectedid']=$row['cityid'];
                                $values['onchange']=" onchange=\"
                                                        $('locationid').load('/admin/locations/options_locations.php?countryid='+document.getElementById('countryid').options[document.getElementById('countryid').selectedIndex].value+'&cityid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-location').style.display='';\" ";
                                $values['where']=" WHERE countryid='".$row['countryid']."' ";
                                admin_html::select_locations_city($db,$values);
                                if( $count && !empty($row['cityid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=2&cityid=".$row['cityid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['cityid']) && empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-location' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>Location</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="locationid";
                                $values['selectedid']=$row['locationid'];
                                $values['where']=" WHERE countryid='".$row['countryid']."' AND cityid='".$row['cityid']."' ";
                                admin_html::select_locations($db,$values);
                                if( $count && !empty($row['locationid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=1&locationid=".$row['locationid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";

                print "\t</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<th class='th_align_left'>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=7 AND itemid2='".$row['newsid']."' ) OR ( typeid2=17 AND typeid1=7 AND itemid1='".$row['newsid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_xs__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";

                    if( !empty($imageid) ) print "<a href='".$link."' ><img src='".$src."' alt='' /></a>";
                    else print "Image";

                print "\t</th>\n";
                print "\t<td>";
                    print "\t<input type='file' name='image' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Text</th>\n";
                print "\t<td>";

                    print "\t<ul id='ul-admin-tabs-text' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-text' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $textEN=$_SESSION['new_record']['textEN'];
                            else $textEN=$row['textEN'];
                            print "\t<textarea name='textEN' id='textEN' cols='90' rows='10'>".$textEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $textDE=$_SESSION['new_record']['textDE'];
                            else $textDE=$row['textDE'];
                            print "\t<textarea name='textDE' id='textDE' cols='90' rows='10'>".$textDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $textFR=$_SESSION['new_record']['textFR'];
                            else $textFR=$row['textFR'];
                            print "\t<textarea name='textFR' id='textFR' cols='90' rows='10'>".$textFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $textIT=$_SESSION['new_record']['textIT'];
                            else $textIT=$row['textIT'];
                            print "\t<textarea name='textIT' id='textIT' cols='90' rows='10'>".$textIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $textZH=$_SESSION['new_record']['textZH'];
                            else $textZH=$row['textZH'];
                            print "\t<textarea name='textZH' id='textZH' cols='90' rows='10'>".$textZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "</td>\n";
            print "\t</tr>\n";

            /*
            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( !empty($row['leaflet']) ) $leaflet="<a href='/includes/retrieve.file.php?type=l&filename=".$row['leaflet']."' title='' ><u>Leaflet</u></a>";
                    else $leaflet="Leaflet";
                    print $leaflet;
                print "</th>\n";
                print "\t<td>";
                    print "\t<input type='file' name='leaflet' />\n";
                print "</td>\n";
            print "\t</tr>\n";
            */

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>";
                    if( $count )
                    {
                        //print "\t<input type='button' name='delete' value='delete' onclick=\"delete_confirm2('del.php?newsid=".$row['newsid']."','".addslashes($row['title_original'])."','')\" />\n";
                        $options_delete=array();
                        $options_delete['url_del']="del.php?newsid=".$row['newsid']."";
                        $options_delete['text1']="News - ".$row['title_original'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</th>\n";
                print "\t<td>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

        print "\t</table>\n";
        print "\t<input type='hidden' name='newsid' value='".$row['newsid']."' />\n";
    print "\t</form>\n";

    # printing out relations
    admin_html::show_relations($db,7,$row['newsid']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
