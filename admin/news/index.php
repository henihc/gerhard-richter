<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="news";
$html->menu_admin_sub_selected="talks";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


    $query_where_news=" WHERE type=3 ";
    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY datefrom DESC ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_news=QUERIES::query_news($db,$query_where_news,$query_order,$query_limit);

    $results=$db->query($query_news['query_without_limit']);
    $count=$db->numrows($results);
    $results_news=$db->query($query_news['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {   
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) 
        {
            $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        }
        else $url="?p=";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    if( $_GET['sort_by']=='newsID' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=newsID&sort_how=$sort_how' title='Order by newsid'><u>newsid</u></a></th>\n";

                    if( $_GET['sort_by']=='type' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th><a href='?sort_by=type&sort_how=$sort_how' title='Order by type'><u>type</u></a></th>\n";

                    if( $_GET['sort_by']=='datefrom' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:90px;'><a href='?sort_by=datefrom&sort_how=$sort_how' title='Order by date from'><u>date from</u></a></th>\n";

                    if( $_GET['sort_by']=='dateto' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                        else $sort_how='asc';
                    }
                    else $sort_how='asc';
                    print "\t<th style='width:90px;'><a href='?sort_by=dateto&sort_how=$sort_how' title='Order by date to'><u>date to</u></a></th>\n";

                    print "\t<th>title EN</th>\n";
                    print "\t<th>location</th>\n";
                    print "\t<th>&nbsp;</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";

            while( $row=$db->mysql_array($results_news) )
            {
                print "\t<tr>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print "\t<a href='/admin/news/edit/?newsid=".$row['newsID']."' title='Edit this news'><u>".$row['newsID']."</u></a>\n";
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print UTILS::get_news_type($db,$row['type']);
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print $row['datefrom_list_admin'];
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print $row['dateto_list_admin'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['title_original'];
                        $title=UTILS::row_text_value2($db,$row,"title");
                        if( !empty($row['title_original']) ) print "<span class='span-news-title-exh'>";
                            print $title;
                        if( !empty($row['title_original']) ) print "</span>";
                    print "\t</td>\n";
                    print "\t<td>\n";
                        $values_location=array();
                        $values_location['row']=$row;
                        $location=location($db,$values_location);
                        print $location;
                    print "\t</td>\n";
                    print "\t<td>\n";
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=7 AND itemid2='".$row['newsID']."' ) OR ( typeid2=17 AND typeid1=7 AND itemid1='".$row['newsID']."' ) ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        $src="/images/size_xs__imageid_".$imageid.".jpg";
                        $link="/images/size_l__imageid_".$imageid.".jpg";
                        if( !empty($imageid) ) print "<a href='".$link."' ><img src='".$src."' alt='' /></a>";
                        else print "&nbsp;";
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No news found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
