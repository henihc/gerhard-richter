<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=20;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="literature";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    print "\t<h1>Literature</h1><br />\n";

    print "Category - ";

    $options_select_book_cat=array();
    $options_select_book_cat=$options;
    $options_select_book_cat['select_books_catid']="select_books_catid";
    $options_select_book_cat['class']['select-field']="select-field";
    $options_select_book_cat['class']['select-large']="select-large";
    $options_select_book_cat['where']=" AND enable=1 ";
    $options_select_book_cat['id']="books_catid";
    $options_select_book_cat['name']="books_catid";
    $options_select_book_cat['sub']=1;
    $options_select_book_cat['onchange']="onchange=\"location.href='?books_catid='+document.getElementById('books_catid').options[selectedIndex].value\"";
    select_books_categories($db,$_GET['books_catid'],$options_select_book_cat);

    $query="SELECT * FROM ".TABLE_BOOKS;
    if( !empty($_GET['books_catid']) ) $query.=" WHERE books_catid='".$_GET['books_catid']."' ";
    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query.=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query.=" ORDER BY year DESC,id DESC ";

    $query_books=$query." LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];

    $results=$db->query($query);
    $count=$db->numrows($results);
    $results_books=$db->query($query_books);
    $pages=@ceil($count/$_GET['sp']);
    
    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $url="?books_catid=".$_GET['books_catid']."&sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        else $url="?books_catid=".$_GET['books_catid']."&p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

echo "
<table class='admin-table' cellspacing='0'>
<tr>";

//1
if( $_GET['sort_by']=="id" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=id&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by id'><u>ID</u></a></strong></th>";
//2
if( $_GET['sort_by']=="title" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=title&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by title'><u>TITLE</u></a></strong></th>";
//3
if( $_GET['sort_by']=="selectedTitle" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=selectedTitle&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by selected title'><u>S.T.</u></a></strong></th>";
//4
if( $_GET['sort_by']=="author" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=author&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by author'><u>AUTHOR</u></a></strong></th>";
//5
if( $_GET['sort_by']=="publisher" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=publisher&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by publisher'><u>PUBLISHER</u></a></strong></th>";
//6
if( $_GET['sort_by']=="year" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=year&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by year'><u>YEAR</u></a></strong></th>";
//7
if( $_GET['sort_by']=="cover" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=coverid&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by cover type'><u>COVER</u></a></strong></th>";
//8
if( $_GET['sort_by']=="books_catid" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=books_catid&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by books category'><u>CATEGORY</u></a></strong></th>";

//9
if( $_GET['sort_by']=="NOpages" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=NOpages&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by number of pages'><u>Nr.P.</u></a></strong></th>";
//10
/*
if( $_GET['sort_by']=="notesEN" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=notesEN&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by notes'><u>NOTES</u></a></strong></th>";
 */
//11
//echo "<th>OLDIM</th>";
echo "<th>IMAGE</th>";
//echo "<th>size</th>";



echo "</tr>";

    while( $row=$db->mysql_array($results_books) )
    {
        echo "
        <tr>
        <td><a href='/admin/books/edit/?bookid=".$row['id']."' style='text-decoration:underline;color:blue;' title='Edit this book'>".$row['id']."</a></td>
        <td>".$row['title']."</td>
        <td style='text-align:center;'>";
        if($row['selectedTitle']==1){echo "yes";}else echo "no";
        echo "
        </td>
        <td>".$row['author']."</td>
        <td>".$row['publisher']."</td>
        <td>".$row['year']."</td>";
        print "\t<td>\n";
            $values_cover=array();
            $values_cover['coverid']=$row['coverid'];
            $cover=UTILS::get_cook_cover($db,$values_cover);
            print $cover;
        print "\t</td>\n";
          $books_category=UTILS::get_books_category($db,$row['books_catid']);
        echo "<td>".$books_category."</td>
        <td style='text-align:center;'>".$row['NOpages']."</td>";
        //print "<td>".$row['notesEN']."</td>";
        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ");
        $row_related_image=$db->mysql_array($results_related_image);
        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);

        /*
    $query_where_image=" WHERE imageid='".$imageid."' ";
            $query_image=QUERIES::query_images($db,$query_where_image,$query_order,$query_limit);
            $results3=$db->query($query_image['query']);
                $row3=$db->mysql_array($results3);
         */

        //print "<td style='text-align:center;'><img src='/includes/retrieve.image_old.php?bookimageID=".$row['imageID']."&size=s' alt='' /></td>";
        print "<td style='text-align:center;'><img src='/images/size_s__imageid_".$imageid.".jpg' alt='' /></td>";
        /*
        $src_old_img_original=DATA_PATH."/images/books/original/".$row2['src'];
        $src_new_img_original=DATA_PATH."/images_new/original/".$row3['src'];
        $filesize_old=@filesize($src_old_img_original);
        $filesize_new=@filesize($src_new_img_original);
        if( $filesize_old!=$filesize_new && !empty($row3['src']) ) $style="background:red;";
        else $style="background:green;";
        print "<td style='".$style."'>";
            print "old:".$filesize_old."<br />";
            print "new:".$filesize_new;
        print "</td>";
         */
        print "</tr>";
    }


        print "\t</table>\n";

        $pages=@ceil($count/$_GET['sp']);
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No books found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
