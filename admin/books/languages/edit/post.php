<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) ) $_SESSION['new_record']['error']['title_en']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/books/languages/edit/?languageid=".$_POST['languageid']."&error=true");

$db=new dbCLASS;


//$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST,1);

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort_en",TABLE_BOOKS_LANGUAGES,$where);
    $db->query("INSERT INTO ".TABLE_BOOKS_LANGUAGES."(sort_en,sort_de,sort_fr,sort_zh,date_created) VALUES('".$maxsort."','".$maxsort."','".$maxsort."','".$maxsort."',NOW())");  
    $languageid=$db->return_insert_id();
    admin_utils::admin_log($db,1,13,$languageid);
}
elseif( $_POST['submit']=="update" )
{
    $languageid=$_POST['languageid'];
    admin_utils::admin_log($db,2,13,$languageid);
}
else
{
    admin_utils::admin_log($db,"",13,$_POST['languageid'],2);
}

    $query="UPDATE ".TABLE_BOOKS_LANGUAGES." SET
        title_en='".$_POST['title_en']."',
        title_de='".$_POST['title_de']."',
        title_it='".$_POST['title_it']."',
        title_fr='".$_POST['title_fr']."',
        title_zh='".$_POST['title_zh']."',
        enable='".$_POST['enable']."'";
    if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
    $query.=" WHERE languageid='".$languageid."' ";


if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);
}




unset($_SESSION['new_record']);
UTILS::redirect("/admin/books/languages/edit/?languageid=".$languageid."&task=".$_POST['submit']);

?>
