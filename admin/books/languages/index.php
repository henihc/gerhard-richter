<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="literature";
$html->menu_admin_sub_selected="languages";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( empty($_GET['lang']) ) $_GET['lang']="en";
$sort_by=UTILS::select_lang($_GET['lang'],"sort");
$sort_how="asc";

    print "Language - ";
    $options_select_languages=array();
    $options_select_languages['name']='lang';
    $options_select_languages['id']='lang';
    $options_select_languages['selectedid']=$_GET['lang'];
    $options_select_languages['onchange']="onchange=\"location.href='?lang='+this.options[selectedIndex].value\"";
    admin_html::select_languages_new($db,$options_select_languages);




    $results=$db->query("SELECT * FROM ".TABLE_BOOKS_LANGUAGES." ORDER BY sort_".$_GET['lang']." ");
    $count=$db->numrows($results);

    if( $count>=1 )
    {

        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>sort</th>\n";
                print "\t<th>id</th>\n";
                print "\t<th>title_en</th>\n";
                print "\t<th>title_de</th>\n";
                print "\t<th>title_fr</th>\n";
                print "\t<th>title_it</th>\n";
                print "\t<th>title_zh</th>\n";
            print "\t</tr>\n";
        print "\t</table>\n";

        $ii=1;
        print "\t<ul id='sortlist".$ii."' class='sortlist'>\n";

        while( $row=$db->mysql_array($results) )
        {
            $row=UTILS::html_decode($row);
            print "\t<li id='item_".$row['languageid']."'  >";
                print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                    print "\t<tr>\n";
                        print "\t<td class='td_move'>sort</td>\n";
                        print "\t<td style='text-align:center;width:30px;' >";
                            print "<a name='".$row['languageid']."' href='/admin/books/languages/edit/?languageid=".$row['languageid']."' title='edit this language' class='link_edit'><u>".$row['languageid']."</u></a>";
                        print "</td>\n";
                        print "\t<td style='text-align:center;width:150px;'>\n";
                            print $row['title_en'];
                        print "</td>\n";
                        print "\t<td style='text-align:center;width:150px;'>\n";
                            print $row['title_de'];
                        print "</td>\n";
                        print "\t<td style='text-align:center;width:150px;'>\n";
                            print $row['title_fr'];
                        print "</td>\n";
                        print "\t<td style='text-align:center;width:150px;'>\n";
                            print $row['title_it'];
                        print "</td>\n";
                        print "\t<td style='text-align:center;width:150px;'>\n";
                            print $row['title_zh'];
                        print "</td>\n";
                    print "\t</tr>\n";
                print "\t</table>\n";
            print "</li>\n";
        }

        print "\t</ul>\n";
        
        for( $i=1;$i<=$ii;$i++ )
        {
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('edit/sort.php?count=".$ii."&lang=".$_GET['lang']."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }

    }
    else print "\t<p class='error'>No data found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
