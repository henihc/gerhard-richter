<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['title_en']) ) $_SESSION['new_record']['error']['title_en']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/books/categories/edit/?books_catid=".$_POST['books_catid']."&error=true");

$db=new dbCLASS;


$_POST=$db->db_prepare_input($_POST);

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_BOOKS_CATEGORIES,"");
    $db->query("INSERT INTO ".TABLE_BOOKS_CATEGORIES."(sort,date_created) VALUES('".$maxsort."',NOW()) ");
    $books_catid=$db->return_insert_id();
    admin_utils::admin_log($db,1,12,$books_catid);
}
elseif( $_POST['submit']=="update" ) 
{
    $books_catid=$_POST['books_catid'];
    admin_utils::admin_log($db,2,12,$books_catid);
}
else
{
    admin_utils::admin_log($db,"",12,$_POST['books_catid'],2);
}

$titleurl_en=UTILS::convert_fortitleurl($db,TABLE_BOOKS_CATEGORIES,$_POST['title_en'],"books_catid","","titleurl","en");

//print $titleurl_en;
//print_r($_POST);
//exit();

    $query="UPDATE ".TABLE_BOOKS_CATEGORIES." SET 
        sub_books_catid='".$_POST['sub_books_catid']."',
        title_en='".$_POST['title_en']."',
        titleurl='".$titleurl_en."',
        title_de='".$_POST['title_de']."',
        title_fr='".$_POST['title_fr']."',
        title_it='".$_POST['title_it']."',
        title_zh='".$_POST['title_zh']."',
        enable='".$_POST['enable']."'";
    if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
    $query.=" WHERE books_catid='".$books_catid."' ";


if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);
}


### image upload
if($_FILES['image']['error']==0)
{

    $fileName = str_replace (" ", "_", $_FILES['image']['name']);
    $tmpName  = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];

    if ( is_uploaded_file($tmpName) )
    {   
        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

            $getimagesize = getimagesize($tmpName);
       
            switch( $getimagesize['mime'] )
            {   
                case 'image/gif'  : $ext = ".gif"; break;
                case 'image/png'  : $ext = ".png"; break;
                case 'image/jpeg' : $ext = ".jpg"; break;
                case 'image/bmp'  : $ext = ".bmp"; break;
                default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
            }

            $dir=DATA_PATH."/images_new";
            $new_file_name=$imageid.".jpg";
            $new_file_path=$dir."/original/".$imageid.$ext;

            UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT,THUMB_XS_WIDTH,$dir.'/xsmall/'.$new_file_name);

            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,26,$books_catid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
                //$src_cache_uri=$imageid.".jpg";
                //UTILS::cache_uri($src,$src_cache_uri,1);
                #end
            }

     }

}




unset($_SESSION['new_record']);


UTILS::redirect("/admin/books/categories/edit/?books_catid=".$books_catid."&task=".$_POST['submit']);
