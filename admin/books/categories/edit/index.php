<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="literature";
$html->menu_admin_sub_selected="add_category";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    $query_where_book=" WHERE books_catid='".$_GET['books_catid']."' ";
    $query_book=QUERIES::query_books_categories($db,$query_where_book,$query_order,$query_limit);
    $results=$db->query($query_book['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);


    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='table_admin_newrecord' cellspacing='0'>\n";

            if( !empty($row['date_modified2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date modified</td>\n";
                    print "\t<td>".$row['date_modified2']."</td>\n";
                print "\t</tr>\n";
            }   

            if( !empty($row['date_created2']) )
            {   
                print "\t<tr>\n";
                    print "\t<td>Date Created</td>\n";
                    print "\t<td>".$row['date_created2']."</td>\n";
                print "\t</tr>\n";
            } 

            print "\t<tr>\n";
                if( $_GET['error'] ) $sub_categoryid=$_SESSION['new_record']['sub_books_catid'];
                else $sub_categoryid=$row['sub_books_catid'];
                print "\t<td>Sub Category</td>\n";
                print "\t<td data-sub_books_catid='".$sub_categoryid."'>\n";
                    $options_select_book_cat=array();
                    $options_select_book_cat['select_books_catid']="select_books_catid";
                    $options_select_book_cat['class']['select-field']="select-field";
                    $options_select_book_cat['class']['select-large']="select-large";
                    $options_select_book_cat['where']=" AND books_catid!='".$row['books_catid']."' ";
                    $options_select_book_cat['id']="sub_books_catid";
                    $options_select_book_cat['name']="sub_books_catid";
                    $options_select_book_cat['optgroup']=1;
                    $html_print.=select_books_categories($db,$sub_categoryid,$options_select_book_cat);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_GET['error'] && $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                elseif( $row['enable'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td>Enable</td>\n";
                print "\t<td><input name='enable' $checked type='checkbox' value='1' class='' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['title_en'] ) $class="error";
                else $class="";
                print "\t<td class='".$class."'>*Title EN</td>\n";
                if( $_GET['error'] ) $title_en=$_SESSION['new_record']['title_en'];
                else $title_en=$row['title_en'];
                print "\t<td>";
                    print "<input name='title_en' type='text' value=\"".$title_en."\" class='input_field_admin' />";
                    $url="/literature";
                    if( !empty($row['sub_books_catid']) )
                    { 
                        $results_category=$db->query("SELECT titleurl FROM ".TABLE_BOOKS_CATEGORIES." WHERE books_catid='".$row['sub_books_catid']."'");
                        $row_category=$db->mysql_array($results_category);

                        $url.="/".$row_category['titleurl'];
                    }
                    $url.="/".$row['titleurl'];
                    print "&nbsp;<a href='".$url."' title='".$row['title_en']."'><u>".$row['titleurl']."</u></a>";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title DE</td>\n";
                if( $_GET['error'] ) $title_de=$_SESSION['new_record']['title_de'];
                else $title_de=$row['title_de'];
                print "\t<td>";
                    print "<input name='title_de' type='text' value=\"".$title_de."\" class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title FR</td>\n";
                if( $_GET['error'] ) $title_fr=$_SESSION['new_record']['title_fr'];
                else $title_fr=$row['title_fr'];
                print "\t<td>";
                    print "<input name='title_fr' type='text' value=\"".$title_fr."\" class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title IT</td>\n";
                if( $_GET['error'] ) $title_it=$_SESSION['new_record']['title_it'];
                else $title_it=$row['title_it'];
                print "\t<td>";
                    print "<input name='title_it' type='text' value=\"".$title_it."\" class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title CN</td>\n";
                if( $_GET['error'] ) $title_zh=$_SESSION['new_record']['title_zh'];
                else $title_zh=$row['title_zh'];
                print "\t<td>";
                    print "<input name='title_zh' type='text' value=\"".$title_zh."\" class='input_field_admin' />";
                print "</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<td>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=26 AND itemid2='".$row['books_catid']."' ) OR ( typeid2=17 AND typeid1=26 AND itemid1='".$row['books_catid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_o__imageid_".$imageid.".jpg";
                    $link="/images/size_o__imageid_".$imageid.".jpg";

                    if( !empty($imageid) ) print "\t<img src='".$src."' alt='' width='148' />\n";
                    else print "Category image";
                    print "<br />Height = 98px<br />Width = 148px";
                print "\t</td>\n";
                print "\t<td>";
                    print "<input type='file' name='image' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/books/categories/edit/del.php?books_catid=".$row['books_catid']."','".addslashes($row['title_en'])."','');\" title='delete this category'>delete</a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/books/categories/edit/del.php?books_catid=".$row['books_catid']."";
                        $options_delete['text1']="Literature - Category - ".$row['title_en'];
                        //$options_delete['text2']="";
                        //$options_delete['html_return']=1;
                        admin_html::delete_button($db,$options_delete);
                    }
                    else print "* Mandatory";
                print "</td>\n";
                if( $count ) $value="update";
                else $value="add";
                print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
            print "\t</tr>\n";



        print "\t</table>\n";
        print "\t<input type='hidden' name='books_catid' value='".$row['books_catid']."' />\n";
    print "\t</form>\n";

        # printing out relations
        //if( $count>0 ) admin_html::show_relations($db,26,$row['books_catid']);
        #end


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
