<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=20;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="literature";
$html->menu_admin_sub_selected="broken_links";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    print "\t<h1>Broken links</h1><br />\n";

    $query="SELECT * FROM ".TABLE_BOOKS;
    $query.=" WHERE link_broken=1 AND link_validated=0 && url IS NOT NULL && url!='' ";
    $query.=" ORDER BY year DESC,id DESC ";

    $query_books=$query." LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];

    $results=$db->query($query);
    $count=$db->numrows($results);
    $results_books=$db->query($query_books);
    $pages=@ceil($count/$_GET['sp']);
    
    if( $count>0 )
    {
        if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $url="?books_catid=".$_GET['books_catid']."&sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
        else $url="?books_catid=".$_GET['books_catid']."&p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

echo "
<table class='admin-table' cellspacing='0'>
<tr>";

//1
if( $_GET['sort_by']=="id" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=id&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by id'><u>ID</u></a></strong></th>";
//2
if( $_GET['sort_by']=="link" )
{
    if( $_GET['sort_how']=="asc") $sort="desc";
    else $sort="asc";
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=link&sort_how=".$sort."&books_catid=".$_GET['books_catid']."' title='Order by link'><u>LINK</u></a></strong></th>";

echo "</tr>";

    while( $row=$db->mysql_array($results_books) )
    {
        echo "
        <tr>
        <td><a href='/admin/books/edit/?bookid=".$row['id']."' style='text-decoration:underline;color:blue;' title='Edit this book'>".$row['id']."</a></td>
        <td><a href='".$row['link']."' target='_blank'>".$row['link']."</a></td>
        <td><a href='#' onclick=\"delete_confirm2('/admin/books/broken-links/mark_valid.php?bookid=".$row['id']."','','Do you really want to mark this as valid?')\" ><u>mark as valid</u></a></td>";
        print "</tr>";
    }


        print "\t</table>\n";

        $pages=@ceil($count/$_GET['sp']);
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    else print "\t<p class='p_admin_no_data_found'>No books found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
