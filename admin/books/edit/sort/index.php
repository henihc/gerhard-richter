<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# sort
$html->js[]="/admin/js/sort/prototype.js";
$html->js[]="/admin/js/sort/scriptaculous.js";

# menu selected
$html->menu_admin_selected="literature";
$html->menu_admin_sub_selected="sort_featured_titles";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


    $results=$db->query("SELECT id,title,author,year,imageID
        FROM ".TABLE_BOOKS." 
        WHERE selectedTitle=1 
        ORDER BY sort ");
    $count=$db->numrows($results);

    if( $count>0 )
    {
        $ii=1;
        print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list_head'>\n";
            print "\t<tr>\n";
                print "\t<th>image</th>\n";
                print "\t<th>bookid</th>\n";
                print "\t<th>title</th>\n";
                print "\t<th>author</th>\n";
                print "\t<th>year</th>\n";
            print "\t</tr>\n";    
        print "\t</table>\n"; 

        print "\t<ul id='sortlist$ii' class='sortlist'>\n";

            while($row=$db->mysql_array($results))
            {
                print "\t<li id='item_".$row['id']."'  >";
                    print "\t<table cellspacing='0' cellpadding='0' class='table_sort_list'>\n";
                        print "\t<tr>\n";
                            print "\t<td class='td_move' ".$style.">\n";
                                $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                $row_related_image=$db->mysql_array($results_related_image);
                                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                $src="/images/size_s__imageid_".$imageid.".jpg";
                                print "\t<img src='".$src."' alt='' />\n";
                            print "\t</td>\n";
                            print "\t<td>";
                                print "<a name='".$row['id']."' href='/admin/books/edit/?bookid=".$row['id']."' title='edit book' class='link_edit'>".$row['title']."</a>";
                            print "\t</td>";
                            print "\t<td>".$row['author']."</td>\n";
                            print "\t<td>".$row['year']."</td>\n";
                        print "\t</tr>\n";
                    print "\t</table>\n";
                print "</li>\n";
            }

        print "\t</ul>\n";

        for( $i=1;$i<=$ii;$i++ )
        {    
            print "\t<script type='text/javascript'>\n";
                print "\tSortable.create('sortlist".$i."',\n";
                print "\t{\n";
                    print "\tonUpdate: function()\n";
                    print "\t{\n";
                        print "\tnew Ajax.Request('sort.php?count=".$ii."',\n";
                        print "\t{\n";
                            print "\tmethod: 'post',\n";
                            print "\tparameters: { data: Sortable.serialize('sortlist".$i."') }\n";
                        print "\t});\n";
                    print "\t}\n";
                print "\t});\n";
            print "\t</script>\n";
        }    


    }
    else print "\t<p class='error'>No data found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

?>
