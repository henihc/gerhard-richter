<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";
$html->js_footer[]="/admin/js/common_footer.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# relation sort
$html->js[]="/admin/js/relations_sort.php?relation_typeid=12&itemid=".$_GET['bookid'];

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="literature";
$html->menu_admin_sub_selected="add_literature";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$query_where_books=" WHERE id='".$_GET['bookid']."' ";
$query_books=QUERIES::query_books($db,$query_where_books);
$results=$db->query($query_books['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results,0);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        print "\tCKEDITOR.replace( 'notesEN', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notesDE', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notesFR', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notesIT', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notesZH', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infoEN', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infoDE', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infoFR', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infoIT', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infoZH', { toolbar : 'Large', width : '950px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'infosmall_en', { toolbar : 'Small', width : '950px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'infosmall_de', { toolbar : 'Small', width : '950px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'infosmall_fr', { toolbar : 'Small', width : '950px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'infosmall_it', { toolbar : 'Small', width : '950px', height : '100px' } );\n";
        print "\tCKEDITOR.replace( 'infosmall_zh', { toolbar : 'Small', width : '950px', height : '100px' } );\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-info li a','#div-admin-tabs-info-info .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-infosmall li a','#div-admin-tabs-info-infosmall .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
    print "\t};\n";

    print "\twindow.addEvent('domready', function() {";
        print "new DatePicker($$('#date'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_start' });\n";

    print "\t});\n";
print "\t</script>\n";

    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";
        print "\t<table width='900px'  class='admin-table'>\n";

            #error and update info
            if( $_GET['task']=="update" ) print "\t<tr><td colspan='3' class='valid'>Update successful!</td></tr>\n";
            if( $_GET['task']=="add" ) print "\t<tr><td colspan='3' class='valid'>Insert successful!</td></tr>\n";
            if( $_GET['task']=="delete_image" ) print "\t<tr><td colspan='3' class='valid'>Image deleted successfully!</td></tr>\n";
            #end error and update info

            #next previous buttons
            if( $count>0 )
            {
                $query_next_previous_book="SELECT id FROM ".TABLE_BOOKS." ORDER BY year DESC,id DESC";
                $previous_next_item_admin=admin_html::previous_next_item_admin($db,"id",$row['id'],"/admin/books/edit/?bookid=",$query_next_previous_book,0);
                print "\t<tr>\n";
                    print "\t<td colspan='3' style='text-align:center;'>\n";
                        print $previous_next_item_admin;
                    print "\t</td>\n";
                print "\t</tr>\n";
            }
            #end next previous buttons

            $tr_delete="";
            $tr_delete.="\t<tr>\n";
                $tr_delete.="\t<td>\n";
                    $options_delete=array();
                    $options_delete['url_del']="/admin/books/edit/del.php?bookid=".$row['id']."";
                    $options_delete['text1']="Literature - ".$row['title'];
                    //$options_delete['text2']="";
                    $options_delete['html_return']=1;
                    $tr_delete.=admin_html::delete_button($db,$options_delete);
                    //print "\t<a href='#' onclick=\"delete_confirm2('/admin/books/edit/del.php?bookid=".$row['id']."','".addslashes($row['title'])."','')\"><u>delete</u></a>\n";
                $tr_delete.="\t</td>\n";
                $tr_delete.="\t<td colspan='2'>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    $tr_delete.="\t<input type='submit' name='submit' value='".$value."' />\n";
                $tr_delete.="\t</td>\n";
            $tr_delete.="\t</tr>\n";

            print $tr_delete;

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td colspan='2'>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td colspan='2'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<td>Category</td>\n";
                print "\t<td>\n";
                    $onchange="onchange=\"
                        if(this.options[selectedIndex].value==6 || this.options[selectedIndex].value==12 || this.options[selectedIndex].value==13 || this.options[selectedIndex].value==19 || this.options[selectedIndex].value==20)
                        {
                            document.getElementById('tr-link-category').style.display='';
                            document.getElementById('tr-title-periodical').style.display='';
                            document.getElementById('span-link-date').style.display='';
                            document.getElementById('span-article-pages').style.display='';
                            document.getElementById('tr-link').style.display='';
                        }
                        else if(this.options[selectedIndex].value==14)
                        {
                            document.getElementById('tr-type-ephemera').style.display='';
                            document.getElementById('tr-published-ephemera').style.display='';
                            document.getElementById('tr-occasion-ephemera').style.display='';
                        }
                        else
                        {
                            document.getElementById('tr-link-category').style.display='none';
                            document.getElementById('tr-title-periodical').style.display='none';
                            document.getElementById('span-link-date').style.display='none';
                            document.getElementById('span-article-pages').style.display='none';
                            document.getElementById('tr-link').style.display='none';
                            document.getElementById('tr-type-ephemera').style.display='none';
                            document.getElementById('tr-published-ephemera').style.display='none';
                            document.getElementById('tr-occasion-ephemera').style.display='none';
                        }
                        \"";
                    if( $_GET['error'] ) $books_catid=$_SESSION['new_record']['books_catid'];
                    else $books_catid=$row['books_catid'];
                    $options_select_book_cat=array();
                    $options_select_book_cat=$options;
                    $options_select_book_cat['select_books_catid']="select_books_catid";
                    $options_select_book_cat['class']['select-field']="select-field";
                    $options_select_book_cat['class']['select-large']="select-large";
                    $options_select_book_cat['where']=" AND enable=1 ";
                    $options_select_book_cat['id']="books_catid";
                    $options_select_book_cat['name']="books_catid";
                    $options_select_book_cat['sub']=1;
                    $options_select_book_cat['onchange']=$onchange;
                    $html_print.=select_books_categories($db,$books_catid,$options_select_book_cat);
                print "\t</td>\n";
                print "\t<td style='text-align:right;'>\n";
                    $values=array();
                    $values['bookid']=$row['id'];
                    $url_literature=UTILS::get_literature_url($db,$values);
                    if( $count ) print "\t<a href='".$url_literature['url']."' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Enable</td>\n";
                print "\t<td colspan='2'>";
                    if( $row['enable'] || $_SESSION['new_record']['enable'] ) $checked="checked='checked'";
                    elseif( $count && !$row['enable'] ) $checked="";
                    else $checked="checked='checked'";
                    print "\t<input name='enable' type='checkbox' value='1' ".$checked." />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>News literature</td>\n";
                print "\t<td colspan='2'>";
                    if( !empty($row['newsid']) ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' id='news_literature' ".$checked." name='news_literature' value='1' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Do not show in <br />recently added</td>\n";
                print "\t<td colspan='2'>";
                    if( !empty($row['not_show_recently']) ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' id='not_show_recently' ".$checked." name='not_show_recently' value='1' />";
                print "</td>\n";
            print "\t</tr>\n";

            if( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 || $row['books_catid']==20 ) $style="";
            else $style="display:none;";
            print "\t<tr style='".$style."' id='tr-link-category'>\n";
                print "\t<th class='".$class."' style='text-align:left;width:103px;'>Link category</th>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $articles_catid=$_SESSION['new_record']['articles_catid'];
                    else $articles_catid=$row['articles_catid'];
                    admin_html::select_articles_categories($db,"articles_catid",$articles_catid,"",$class=array(),$disable,$where="",$multiple="");
                print "\t</td>\n";
            print "\t</tr>\n";


            if( $row['books_catid']==14 ) $style="";
            else $style="display:none;";
            print "\t<tr style='".$style."' id='tr-type-ephemera'>\n";
                print "\t<th class='".$class."' style='text-align:left;width:128px;'>Type</th>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $type_ephemera=$_SESSION['new_record']['type_ephemera'];
                    else $type_ephemera=$row['type_ephemera'];
                    $options_type_emp=array();
                    $options_type_emp['name']="type_ephemera";
                    $options_type_emp['selectedid']=$type_ephemera;
                    admin_html::select_books_type_ephemera($db,$options_type_emp);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr style='".$style."' id='tr-published-ephemera'>\n";
                print "\t<th class='th_align_left'>Published by</th>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $published_ephemera=$_SESSION['new_record']['published_ephemera'];
                    else $published_ephemera=$row['published_ephemera'];
                    print "\t<textarea name='published_ephemera' cols='79' rows='1'>".$published_ephemera."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr style='".$style."' id='tr-occasion-ephemera'>\n";
                print "\t<th class='th_align_left'>On the occasion of</th>\n";
                print "\t<td colspan='2'>\n";
                    if( $_GET['error'] ) $occasion_ephemera=$_SESSION['new_record']['occasion_ephemera'];
                    else $occasion_ephemera=$row['occasion_ephemera'];
                    print "\t<textarea name='occasion_ephemera' cols='79' rows='1'>".$occasion_ephemera."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Url</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='url' cols='79' rows='1'>".$row['url']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='title' cols='79' rows='1'>".$row['title']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Title featured</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='title_display' cols='79' rows='1'>".$row['title_display']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 || $row['books_catid']==20 ) $style="";
            else $style="display:none;";
            print "\t<tr style='".$style."' id='tr-title-periodical'>\n";
                print "\t<th style='text-align:left;'>Title periodical</th>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='title_periodical' cols='79' rows='1'>".$row['title_periodical']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Author</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='author' cols='79' rows='1'>".$row['author']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Editor</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='editor' cols='79' rows='1'>".$row['editor']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Essay author</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='author_essay' cols='79' rows='1'>".$row['author_essay']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Essay author title</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='author_essay_title' cols='79' rows='1'>".$row['author_essay_title']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Publisher</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='publisher' id='publisher' cols='79' rows='1'>".$row['publisher']."</textarea>\n";
                    //print "\t<script type='text/javascript' language='javascript' charset='utf-8'>\n";
                        //print "\tnew Ajax.Autocompleter('publisher','ac2update','/admin/search_ajax.php?for=publisher');\n";
                    //print "\t</script>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Date</td>\n";
                print "\t<td style='width:150px;'>\n";
                    if( $_GET['error'] ) $date=$_SESSION['new_record']['link_date'];
                    else $date=$row['link_date_admin'];
                    //print "\t<input type='text' name='year' value='".$row['year']."' class='input_field_admin_small' />\n";
                    print "\t<input type='text' name='link_date' id='date' class='date dashboard' value='".$date."' class='admin-input-large'>";
                    print "\t<button type='button' class='calendar-icon date_toggler_start'></button>\n";
                print "\t</td>\n";

                print "\t<th style='text-align:left;'>\n";
                    if( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 || $row['books_catid']==20 ) $style="";
                    else $style="display:none;";
                    print "\t<span id='span-link-date' style='".$style."'>\n";
                        if( $_GET['error'] ) $link_date_year_full=$_SESSION['new_record']['link_date_year_full'];
                        else $link_date_year_full=$row['link_date_year_full'];
                        print "Month";
                        print "\t<input type='text' name='link_date_year_full' id='link_date_year_full' value='".$link_date_year_full."' style='width:196px;'>";
                        /*
                        if( $row['link_date_month'] ) $checked=" checked='checked' ";
                        else $checked="";
                        print "\t Show month/year <input type='checkbox' name='link_date_month' value='1' ".$checked.">";
                        if( $row['link_date_year'] ) $checked=" checked='checked' ";
                        else $checked="";
                        print "\t Show year <input type='checkbox' name='link_date_year' value='1' ".$checked.">";
                         */
                        print "\t&nbsp;<span style='font-weight:700;'>Vol.No</span>\n";
                        if( $_GET['error'] ) $vol_no=$_SESSION['new_record']['vol_no'];
                        else $vol_no=$row['vol_no'];
                        print "\t<input type='text' name='vol_no' value='".$vol_no."' class='input_field_admin_small'>";
                        print "\t&nbsp;<span style='font-weight:700;'>Issue.No</span>\n";
                        if( $_GET['error'] ) $issue_no=$_SESSION['new_record']['issue_no'];
                        else $issue_no=$row['issue_no'];
                        print "\t<input type='text' name='issue_no' value='".$issue_no."' class='input_field_admin_small'>";
                    print "\t</span>\n";
                print "</th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    print "\t<a class='add_icn' href=\"javascript:AddInput('Insert2Clone','InsertInsert','isbn');\"><u>ISBN/ISSN</u></a>\n";
                print "\t</td>\n";
                print "\t<td colspan='2'>\n";
                    #DO not remove this because this is from where I take ISBN clone
                    print "\t<div id='Insert2Clone' style='display:none;'>\n";
                        print "\t<input type='text' name='isbn' id='isbn' value='' />\n";
                        print "\tLanguage\n";
                        #1
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="language";
                        $options_books_lang['id']="language";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);
                        #2
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="2language";
                        $options_books_lang['id']="2language";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);
                        #3
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="3language";
                        $options_books_lang['id']="3language";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);

                        print "\t<a class='delete_icn' onclick=\"this.parentNode.parentNode.removeChild(this.parentNode);\" href='javascript:void(0)'>Rem</a>\n";
                        print "\t<a class='add_icn' href=\"javascript:AddInput('Insert2Clone','InsertInsert','isbn');\">Add</a>\n";
                    print "\t</div>\n";
                    #END
	                $query_isbn="SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$row['id']."' AND bookID!='' ORDER BY id ASC";
                    //print $query_isbn;
                    $results_isbn=$db->query($query_isbn);
	                $count_isbn=$db->numrows($results_isbn);
                    //print $count_isbn;
	                if( $count_isbn==0 )
                    {
                        print "\t<div>\n";
                            print "\t<div id='' style='display: block;'>\n";
                                print "\t<input type='text' name='isbn1' id='isbn1' value='' />\n";
                                print "\tLanguage\n";

                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="language1";
                        $options_books_lang['id']="language1";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);
                        #2
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="2language1";
                        $options_books_lang['id']="2language1";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);
                        #3
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="3language1";
                        $options_books_lang['id']="3language1";
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$selectedid,$options_books_lang);

                                print "\t<a class='delete_icn' onclick=\"this.parentNode.parentNode.removeChild(this.parentNode);\" href='javascript:void(0)'>Rem</a>\n";
                                print "\t<a class='add_icn' href=\"javascript:AddInput('Insert2Clone','InsertInsert','isbn');\">Add</a>\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                    }
                    $i=1;
                    while( $row_isbn=$db->mysql_array($results_isbn) )
                    {
                        print "\t<div>\n";
                            print "\t<div id='' style='display: block;'>\n";
                                print "\t<input type='text' name='isbn".$i."' id='isbn".$i."' value='".$row_isbn['isbn']."' />\n";
                                print "\tLanguage\n";

                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="language".$i;
                        $options_books_lang['id']="language".$i;
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$row_isbn['language'],$options_books_lang);
                        #2
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="2language".$i;
                        $options_books_lang['id']="2language".$i;
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$row_isbn['language2'],$options_books_lang);
                        #3
                        $options_books_lang=array();
                        $options_books_lang=$options;
                        $options_books_lang['class']['select_books_languageid']="select_medium";
                        $options_books_lang['name']="3language".$i;
                        $options_books_lang['id']="3language".$i;
                        //$options_books_lang['where']=" WHERE languageid!=11 ";
                        $options_books_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE;
                        select_books_languages($db,$row_isbn['language3'],$options_books_lang);

                                print "\t<a class='delete_icn' onclick=\"this.parentNode.parentNode.removeChild(this.parentNode);\" href='javascript:void(0)'>Rem</a>\n";
                                print "\t<a class='add_icn' href=\"javascript:AddInput('Insert2Clone','InsertInsert','isbn');\">Add</a>\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                        $i++;
                    }
                    print "\t<div id='InsertInsert'></div>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Cover</td>\n";
                print "\t<td colspan='2'>\n";
                    $values=array();
                    $values['name']="coverid[]";
                    $values['id']="coverid";
                    $coverids=explode(",", $row['coverid']);
                    foreach( $coverids AS $coverid )
                    {
                        $values['selectedid'][]=$coverid;
                    }
                    $values['multiple']="multiple='multiple'";
                    admin_html::select_book_cover($db,$values);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Pages</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='NOpages' value='".$row['NOpages']."' />\n";
                print "\t</td>\n";
                print "\t<th style='text-align:left;'>\n";
                    if( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 || $row['books_catid']==20 ) $style="";
                    else $style="display:none;";
                    print "\t<span id='span-article-pages' style='".$style."'>\n";
                        print "Article pages";
                        if( $_GET['error'] ) $article_pages_from=$_SESSION['new_record']['article_pages_from'];
                        else $article_pages_from=$row['article_pages_from'];
                        if( $_GET['error'] ) $article_pages_till=$_SESSION['new_record']['article_pages_till'];
                        else $article_pages_till=$row['article_pages_till'];
                        print "\t<input type='text' name='article_pages_from' value='".$article_pages_from."'>";
                        print "\t - <input type='text' name='article_pages_till' value='".$article_pages_till."'>";
                    print "\t</span>\n";
                print "</th>\n";
            print "\t</tr>\n";

            if( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 || $row['books_catid']==20 ) $style="";
            else $style="display:none;";
            print "\t<tr id='tr-link' style='".$style."'>\n";
                if( $_SESSION['new_record']['error']['link'] ) $class="error";
                else $class="";
                print "\t<th class='".$class."' style='text-align:left;'>Link</th>\n";
                if( $_GET['error'] ) $link=$_SESSION['new_record']['link'];
                else $link=$row['link'];
                print "\t<td colspan='2'>";
                    print "\t<input type='text' name='link' id='link' value='".$link."' class='admin-input-xlarge'>";
                print "\t</td>\n";
            print "\t</tr>\n";

            /*
            print "\t<tr>\n";
                print "\t<td>Exh. catalogue</td>\n";
                print "\t<td>\n";
                    if( $row['exhi_catalogue'] ) $checked=" checked='checked' ";
                    else $checked="";
                    print "\t<input type='checkbox' $checked name='exhi_catalogue' value='1' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";
             */

            print "\t<tr>\n";
                print "\t<td>Notes</td>\n";
                print "\t<td colspan='2'>\n";

                    print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notesEN=$_SESSION['new_record']['notesEN'];
                            else $notesEN=$row['notesEN'];
                            print "\t<textarea class='form' name='notesEN' cols='99' rows='2'>".$notesEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notesDE=$_SESSION['new_record']['notesDE'];
                            else $notesDE=$row['notesDE'];
                            print "\t<textarea class='form' name='notesDE' cols='99' rows='2'>".$notesDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notesFR=$_SESSION['new_record']['notesFR'];
                            else $notesFR=$row['notesFR'];
                            print "\t<textarea class='form' name='notesFR' cols='99' rows='2'>".$notesFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notesIT=$_SESSION['new_record']['notesIT'];
                            else $notesIT=$row['notesIT'];
                            print "\t<textarea class='form' name='notesIT' cols='99' rows='2'>".$notesIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notesZH=$_SESSION['new_record']['notesZH'];
                            else $notesZH=$row['notesZH'];
                            print "\t<textarea class='form' name='notesZH' cols='99' rows='2'>".$notesZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Information small</td>\n";
                print "\t<td colspan='2'>\n";

                    print "\t<ul id='ul-admin-tabs-infosmall' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";

                    print "\t<div id='div-admin-tabs-info-infosmall' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infosmall_en=$_SESSION['new_record']['infosmall_en'];
                            else $infosmall_en=$row['infosmall_en'];
                            print "\t<textarea class='form' name='infosmall_en' cols='99' rows='7'>".$infosmall_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infosmall_de=$_SESSION['new_record']['infosmall_de'];
                            else $infosmall_de=$row['infosmall_de'];
                            print "\t<textarea class='form' name='infosmall_de' cols='99' rows='7'>".$infosmall_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infosmall_fr=$_SESSION['new_record']['infosmall_fr'];
                            else $infosmall_fr=$row['infosmall_fr'];
                            print "\t<textarea class='form' name='infosmall_fr' cols='99' rows='7'>".$infosmall_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infosmall_it=$_SESSION['new_record']['infosmall_it'];
                            else $infosmall_it=$row['infosmall_it'];
                            print "\t<textarea class='form' name='infosmall_it' cols='99' rows='7'>".$infosmall_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infosmall_zh=$_SESSION['new_record']['infosmall_zh'];
                            else $infosmall_zh=$row['infosmall_zh'];
                            print "\t<textarea class='form' name='infosmall_zh' cols='99' rows='7'>".$infosmall_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                    print "\t</ul>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Information</td>\n";
                print "\t<td colspan='2'>\n";

                    print "\t<ul id='ul-admin-tabs-info' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";

                    print "\t<div id='div-admin-tabs-info-info' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infoEN=$_SESSION['new_record']['infoEN'];
                            else $infoEN=$row['infoEN'];
                            print "\t<textarea class='form' name='infoEN' cols='99' rows='7'>".$infoEN."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infoDE=$_SESSION['new_record']['infoDE'];
                            else $infoDE=$row['infoDE'];
                            print "\t<textarea class='form' name='infoDE' cols='99' rows='7'>".$infoDE."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infoFR=$_SESSION['new_record']['infoFR'];
                            else $infoFR=$row['infoFR'];
                            print "\t<textarea class='form' name='infoFR' cols='99' rows='7'>".$infoFR."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infoIT=$_SESSION['new_record']['infoIT'];
                            else $infoIT=$row['infoIT'];
                            print "\t<textarea class='form' name='infoIT' cols='99' rows='7'>".$infoIT."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $infoZH=$_SESSION['new_record']['infoZH'];
                            else $infoZH=$row['infoZH'];
                            print "\t<textarea class='form' name='infoZH' cols='99' rows='7'>".$infoZH."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                    print "\t</ul>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Selected title</td>\n";
                print "\t<td colspan='2'>\n";
                    if( $row['selectedTitle']==1 ) $checked="checked";
                    else $checked="";
                    print "\t<input type='checkbox' ".$checked." name='selTitle' value='1' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_s__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";
                    if( !empty($imageid) )
                    {
                        print "\t<a href='".$link."'><img src='".$src."' alt='' border='0' /></a>\n";
                    }
                    else print "Image";
                print "\t</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<input type='file' name='book_img' value='book_img' />&nbsp;&nbsp;\n";

                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Keywords</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<textarea name='keywords' cols='100' rows='2'>".$row['keywords']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</th>\n";
                print "\t<td colspan='2'>\n";
                    $onchange="onchange=\"if(this.options[selectedIndex].value==1){\$\$('.tr-rel-illustrated').setStyle('display','');document.getElementById('tr-rel-mentioned').style.display='';document.getElementById('tr-rel-discussed').style.display='';}else{\$\$('.tr-rel-illustrated').setStyle('display','none');document.getElementById('tr-rel-mentioned').style.display='none';document.getElementById('tr-rel-discussed').style.display='none';}\"";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='' class='input-relation-itemid-sort' />\n";
                    print "&nbsp;";
                    $class['select_relations_sort']="select_relations_sort";
                    print admin_html::select_relations_sort($db,"relations_sort",12,$row['id'],1,$selected,$class);
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $_GET['relation_typeid']==1 ) $style="";
            else $style="display:none;";

            print "\t<tr id='tr-rel-mentioned' style='".$style."'>\n";
                print "\t<th class='th_align_left'>Rel. mentioned</th>\n";
                print "\t<td colspan='2'>\n";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_MENTIONED."</b> <input type='checkbox' name='show_mentioned' id=''show_mentioned' value='1' /><br />\n";
                    //print "\t<input type='text' name='mentioned' id='mentioned' value='' />\n";
                    print "\t<textarea name='mentioned' id='mentioned' cols='90' rows='1'></textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr id='tr-rel-discussed' style='".$style."'>\n";
                print "\t<th class='th_align_left'>Rel. discussed</th>\n";
                print "\t<td colspan='2'>\n";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_DISCUSSED."</b> <input type='checkbox' name='show_discussed' id=''show_discussed' value='1' /><br />\n";
                    //print "\t<input type='text' name='discussed' id='discussed' value='' />\n";
                    print "\t<textarea name='discussed' id='discussed' cols='90' rows='1'></textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr class='tr-rel-illustrated' style='".$style."'>\n";
                print "\t<th class='th_align_left' rowspan='4'>Rel. illustrated</th>\n";
                print "\t<td colspan='2'>\n";
                    //print "<input type='checkbox' name='illustrated_typeid' id='illustrated_typeid' value='1' checked='checked' /><b>(color)</b>\n";
                    print "\tBlack and white illustrations<br />";
                    //print "\t<input type='text' name='illustrated_bw' id='illustrated_bw' value='' />&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_BW.")</b>\n";
                    print "\t<textarea name='illustrated_bw' id='illustrated_bw' cols='90' rows='1'></textarea></textarea>&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_BW.")\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr class='tr-rel-illustrated' style='".$style."'>\n";
                print "\t<td colspan='2'>\n";
                    print "\tShow  <b>".LANG_ARTWORK_BOOKS_REL_ILLUSTRATED."&nbsp;".LANG_ARTWORK_BOOKS_REL_BW."</b> <input type='checkbox' name='show_bw' id=''show_bw' value='1' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr class='tr-rel-illustrated' style='".$style."'>\n";
                print "\t<td colspan='2'>\n";
                    print "\tColor illustrations<br />";
                    //print "\t<input type='text' name='illustrated' id='illustrated' value='' />&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_COLOUR.")</b>\n";
                    print "\t<textarea name='illustrated' id='illustrated' cols='90' rows='1'></textarea>&nbsp;<b>(".LANG_ARTWORK_BOOKS_REL_COLOUR.")</b>\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr class='tr-rel-illustrated' style='".$style."'>\n";
                print "\t<td colspan='2'>\n";
                    print "\tShow <b>".LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." ".LANG_ARTWORK_BOOKS_REL_COLOUR."</b> <input type='checkbox' name='show_illustrated' id=''show_illustrate' value='1' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Admin notes</td>\n";
                print "\t<td colspan='5'>\n";
                    $row['notes_admin']=UTILS::html_decode($row['notes_admin']);
                    print "\t<textarea rows='8' cols='126' name='notes_admin' >".$row['notes_admin']."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            /*
            print "\t<tr>\n";
                print "\t<td>\n";
                    $options_delete=array();
                    $options_delete['url_del']="/admin/books/edit/del.php?bookid=".$row['id']."";
                    $options_delete['text1']="Literature - ".$row['title'];
                    //$options_delete['text2']="";
                    //$options_delete['html_return']=1;
                    admin_html::delete_button($db,$options_delete);
                    //print "\t<a href='#' onclick=\"delete_confirm2('/admin/books/edit/del.php?bookid=".$row['id']."','".addslashes($row['title'])."','')\"><u>delete</u></a>\n";
                print "\t</td>\n";
                print "\t<td colspan='2'>\n";
                    if( $count>0 ) $value="update";
                    else $value="add";
                    print "\t<input type='submit' name='submit' value='".$value."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";
            */

            print $tr_delete;

            if( $count>0 )
            {
                #next previous buttons
                print "\t<tr>\n";
                    print "\t<td colspan='3' style='text-align:center;'>\n";
                        print $previous_next_item_admin;
                    print "\t</td>\n";
                print "\t</tr>\n";
                #end next previous buttons
            }

        print "\t</table>\n";
        print "\t<input type='hidden' name='bookid' value='".$row['id']."' />\n";
        print "\t<input type='hidden' name='book_img_old' value='".$row['src']."' />\n";
    print "\t</form>\n";

    # printing out relations
    admin_html::show_relations($db,12,$row['id']);
    #end

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
