<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;

if( empty($_POST['infosmall_en']) )
{
    $infosmall_en="";
    $text="";
    $tmp=str_replace("<br>"," ",$_POST['infoEN']);
    $info_text_no_html=strip_tags($tmp);
    $words=explode(" ", $info_text_no_html);
    for( $i=0;$i<=60;$i++ )
    {
        $text.=$words[$i]." ";
        $count_text_length=strlen($text);
        if( $count_text_length<200 ) $infosmall_en=$text;
    }
}
else $infosmall_en=$db->filter_parameters($_POST['infosmall_en'],1);

if( empty($_POST['infosmall_de']) )
{
    $infosmall_de="";
    $text="";
    $tmp=str_replace("<br>"," ",$_POST['infoDE']);
    $info_text_no_html=strip_tags($tmp);
    $words=explode(" ", $info_text_no_html);
    for( $i=0;$i<=60;$i++ )
    {
        $text.=$words[$i]." ";
        $count_text_length=strlen($text);
        if( $count_text_length<200 ) $infosmall_de=$text;
    }
}
else $infosmall_de=$db->filter_parameters($_POST['infosmall_de'],1);

if( empty($_POST['infosmall_fr']) )
{
    $infosmall_fr="";
    $text="";
    $tmp=str_replace("<br>"," ",$_POST['infoFR']);
    $info_text_no_html=strip_tags($tmp);
    $words=explode(" ", $info_text_no_html);
    for( $i=0;$i<=60;$i++ )
    {
        $text.=$words[$i]." ";
        $count_text_length=strlen($text);
        if( $count_text_length<200 ) $infosmall_fr=$text;
    }
}
else $infosmall_fr=$db->filter_parameters($_POST['infosmall_fr'],1);

if( empty($_POST['infosmall_it']) )
{
    $infosmall_it="";
    $text="";
    $tmp=str_replace("<br>"," ",$_POST['infoIT']);
    $info_text_no_html=strip_tags($tmp);
    $words=explode(" ", $info_text_no_html);
    for( $i=0;$i<=60;$i++ )
    {
        $text.=$words[$i]." ";
        $count_text_length=strlen($text);
        if( $count_text_length<200 ) $infosmall_it=$text;
    }
}
else $infosmall_it=$db->filter_parameters($_POST['infosmall_it'],1);

if( empty($_POST['infosmall_zh']) )
{
    $infosmall_zh="";
    $text="";
    $tmp=str_replace("<br>"," ",$_POST['infoZH']);
    $info_text_no_html=strip_tags($tmp);
    $words=explode(" ", $info_text_no_html);
    for( $i=0;$i<=60;$i++ )
    {
        $text.=$words[$i]." ";
        $count_text_length=strlen($text);
        if( $count_text_length<200 ) $infosmall_zh=$text;
    }
}
else $infosmall_zh=$db->filter_parameters($_POST['infosmall_zh'],1);


$_POST=$db->filter_parameters($_POST,1);


# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['title'];
$title_search=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['author'];
$author_search=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['title_periodical'];
$title_periodical_search=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['infoDE'];
$info_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['infoFR'];
$info_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['infoIT'];
$info_search_it=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['notesDE'];
$notes_search_de=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['notesFR'];
$notes_search_fr=UTILS::replace_accented_letters($db, $values_accented);
$values_accented=array();
$values_accented['text']=$_POST['notesIT'];
$notes_search_it=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

$i=0;
foreach( $_POST['coverid'] AS $coverid )
{
    if( $i>0 ) $coverids.=",";
    $coverids.=$coverid;
    $i++;
}
$_POST['coverid']=$coverids;

//print $coverids;
//print $title;
//print_r($title_search);
//print_r($_POST);
//exit;

if( empty($_POST['year']) ) $year="NULL";
else $year="'".$_POST['year']."'";
if( empty($_POST['link_date']) ) $link_date="NULL";
else $link_date="'".$_POST['link_date']."'";
if( !empty($_POST['link']) )
{
    $test_link=strstr($_POST['link'],"http");
    if( !empty($test_link) ) $link=$_POST['link'];
    else $link="http://".$_POST['link'];
}

if( $_POST['submit']=="add" )
{
    $maxsort=UTILS::max_table_value($db,"sort",TABLE_BOOKS,"");
    $db->query("INSERT INTO ".TABLE_BOOKS."(sort,date_created) VALUES('".$maxsort."',NOW()) ");
    $bookid=$db->return_insert_id();
    admin_utils::admin_log($db,1,11,$bookid);
}
elseif( $_POST['submit']=="update" )
{
    $bookid=$_POST['bookid'];
    admin_utils::admin_log($db,2,11,$bookid);
}
else
{
    admin_utils::admin_log($db,"",11,$_POST['bookid'],2);
}

$options_make_slugs=array();
$options_make_slugs['delimiter']="-";
$options_make_slugs['maxlen']=55;
if( !empty($_POST['title']) ) $title_slugs=$_POST['title'];
elseif( !empty($_POST['title_periodical']) ) $title_slugs=$_POST['title_periodical'];
elseif( !empty($_POST['title_display']) ) $title_slugs=$_POST['title_display'];
$options_make_slugs['text']=$title_slugs;
$titleurl=UTILS::replace_accented_letters($db,$options_make_slugs);
if( !empty($titleurl['text_lower_converted']) ) $titleurl['text_lower_converted'].=$options_make_slugs['delimiter'];
$titleurl=$titleurl['text_lower_converted'].$bookid;

# all the stuff with added items
$db->query("DELETE FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$bookid."'");

//print_r($_POST);

$isbn=array();
$languages="";
for( $i=1;$i<100;$i++ )
{
    //if($i==0)$i='';
    if( array_key_exists("isbn".$i, $_POST) && ( !empty($_POST['isbn'.$i]) || !empty($_POST['language'.$i]) || !empty($_POST['2language'.$i]) || !empty($_POST['3language'.$i]) ) )
    {
        $isbn[]=$_POST['isbn'.$i];
        if( !empty($languages) ) $comma=", ";
        else $comma="";
        $values_language=array();
        $values_language['languageid']=$_POST['language'.$i];
        $language_1=UTILS::get_language_info($db,$values_language);
        $languages.=$comma.$language_1;
        $values_language=array();
        $values_language['languageid']=$_POST['2language'.$i];
        $language_2=UTILS::get_language_info($db,$values_language);
        if( !empty($_POST['2language'.$i]) ) $languages.=", ".$language_2;
        $values_language=array();
        $values_language['languageid']=$_POST['3language'.$i];
        $language_3=UTILS::get_language_info($db,$values_language);
        if( !empty($_POST['3language'.$i]) ) $languages.=", ".$language_3;
        $query_insert_isbn="INSERT INTO ".TABLE_BOOKS_ISBN."(bookID,isbn,language,language2,language3) values('".$bookid."','".$_POST['isbn'.$i]."','".$_POST['language'.$i]."','".$_POST['2language'.$i]."','".$_POST['3language'.$i]."')";
        //print $query_insert_isbn."<br />";
        $db->query($query_insert_isbn);
    }
}
//exit;
#end

$query="UPDATE ".TABLE_BOOKS." SET
    newsid='".$_POST['news_literature']."',
    books_catid='".$_POST['books_catid']."',
    enable='".$_POST['enable']."',
    not_show_recently='".$_POST['not_show_recently']."',
    articles_catid='".$_POST['articles_catid']."',
    type_ephemera='".$_POST['type_ephemera']."',
    published_ephemera='".$_POST['published_ephemera']."',
    occasion_ephemera='".$_POST['occasion_ephemera']."',
    url='".$_POST['url']."',
    title='".$_POST['title']."',
    title_display='".$_POST['title_display']."',
    title_periodical='".$_POST['title_periodical']."',
    titleurl='".$titleurl."',
    title_search_1='".$title_search['text_lower_converted']."',
    title_search_2='".$title_search['text_lower_german_converted']."',
    title_periodical_search_1='".$title_periodical_search['text_lower_converted']."',
    title_periodical_search_2='".$title_periodical_search['text_lower_german_converted']."',
    author='".$_POST['author']."',
    editor='".$_POST['editor']."',
    author_essay='".$_POST['author_essay']."',
    author_essay_title='".$_POST['author_essay_title']."',
    author_search_1='".$author_search['text_lower_converted']."',
    author_search_2='".$author_search['text_lower_german_converted']."',
    publisher='".$_POST['publisher']."',
    year=".$year.",
    link_date=".$link_date.",";

//$query.="link_date_month='".$_POST['link_date_month']."',
//    link_date_year='".$_POST['link_date_year']."', ";

$query.="link_date_year_full='".$_POST['link_date_year_full']."',
    vol_no='".$_POST['vol_no']."',
    issue_no='".$_POST['issue_no']."',
    link='".$link."',
    link_broken=0,
    link_validated=0,
    coverid='".$coverids."',
    NOpages='".$_POST['NOpages']."',
    article_pages_from='".$_POST['article_pages_from']."',
    article_pages_till='".$_POST['article_pages_till']."',
    selectedTitle='".$_POST['selTitle']."',
    notesEN='".$_POST['notesEN']."',
    notesDE='".$_POST['notesDE']."',
    notesFR='".$_POST['notesFR']."',
    notesIT='".$_POST['notesIT']."',
    notesZH='".$_POST['notesZH']."',
    notes_search_1='".$notes_search_de['text_lower_converted']."',
    notes_search_2='".$notes_search_de['text_lower_german_converted']."',
    notes_search_3='".$notes_search_fr['text_lower_converted']."',
    notes_search_4='".$notes_search_it['text_lower_converted']."',
    infoEN='".$_POST['infoEN']."',
    infoDE='".$_POST['infoDE']."',
    infoFR='".$_POST['infoFR']."',
    infoIT='".$_POST['infoIT']."',
    infoZH='".$_POST['infoZH']."',
    infosmall_en='".$infosmall_en."',
    infosmall_de='".$infosmall_de."',
    infosmall_fr='".$infosmall_fr."',
    infosmall_it='".$infosmall_it."',
    infosmall_zh='".$infosmall_zh."',
    info_search_1='".$info_search_de['text_lower_converted']."',
    info_search_2='".$info_search_de['text_lower_german_converted']."',
    info_search_3='".$info_search_fr['text_lower_converted']."',
    info_search_4='".$info_search_it['text_lower_converted']."',
    keywords='".$_POST['keywords']."',
    notes_admin='".$_POST['notes_admin']."' ";

if( $_POST['submit']=="update" ) $query.=" ,date_modified=NOW() ";
$query.=" WHERE id='".$bookid."' ";


#image upload
if( $_FILES['book_img']['error']==0 )
{
	$fileName = str_replace (" ", "_", $_FILES['book_img']['name']);
	$tmpName  = $_FILES['book_img']['tmp_name'];
	$fileSize = $_FILES['book_img']['size'];
	$fileType = $_FILES['book_img']['type'];

    if (is_uploaded_file($tmpName))
	{

        $db->query("INSERT INTO ".TABLE_IMAGES."(enable,date_created) VALUES(1,NOW()) ");
        $imageid=$db->return_insert_id();

        $getimagesize = getimagesize($tmpName);

        switch( $getimagesize['mime'] )
        {
            case 'image/gif'  : $ext = ".gif"; break;
            case 'image/png'  : $ext = ".png"; break;
            case 'image/jpeg' : $ext = ".jpg"; break;
            case 'image/bmp'  : $ext = ".bmp"; break;
            default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
        }

        $dir=DATA_PATH."/images_new";
        $new_file_name=$imageid.".jpg";
        $new_file_path=$dir."/original/".$imageid.$ext;


        UTILS::resize_image($tmpName,$fileName,THUMB_XS_HEIGHT_BOOKS,THUMB_XS_WIDTH_BOOKS,$dir.'/xsmall/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_S_HEIGHT_BOOKS,THUMB_S_WIDTH_BOOKS,$dir.'/small/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_M_HEIGHT_BOOKS,THUMB_M_WIDTH_BOOKS,$dir.'/medium/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_L_HEIGHT_BOOKS,THUMB_L_WIDTH_BOOKS,$dir.'/large/'.$new_file_name);
        UTILS::resize_image($tmpName,$fileName,THUMB_XL_HEIGHT_BOOKS,THUMB_XL_WIDTH_BOOKS,$dir.'/xlarge/'.$new_file_name);


            #copy original
            if (!copy($tmpName, $new_file_path))
            {
                exit("Error Uploading File.");
            }
            else
            {
                $db->query("UPDATE ".TABLE_IMAGES." SET src_old='".$imageid."-".$fileName."', src='".$new_file_name."' WHERE imageid='".$imageid."'");

                # RELATION adding
                admin_utils::add_relation($db,17,$imageid,12,$bookid,$_POST['relations_sort']);
                #end RELATION adding

                # create cache image
                //$src="/images/imageid_".$imageid."__thumb_1__size_o.jpg";
                //$src_cache_uri=$imageid.".jpg";
                //UTILS::cache_uri($src,$src_cache_uri,1);
                #end

            }


	}
}
#image upload end



if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    $db->query($query);

    if( $_POST['submit']=="add" )
    {
        admin_utils::admin_log($db,1,11,$bookid);
    }
    if( $_POST['submit']=="update" )
    {
        admin_utils::admin_log($db,2,11,$bookid);
    }


    # RELATION adding
    $relationid=admin_utils::add_relation($db,12,$bookid,$_POST['relation_typeid'],$_POST['itemid'],$_POST['relations_sort']);
    #end RELATION adding

    if( !empty($relationid) && ( !empty($_POST['illustrated']) || !empty($_POST['illustrated_bw']) || !empty($_POST['mentioned']) || !empty($_POST['discussed']) || !empty($_POST['show_illustrated']) || !empty($_POST['show_bw']) || !empty($_POST['show_mentioned']) || !empty($_POST['show_discussed']) ) )
    {
        $db->query("INSERT INTO ".TABLE_RELATIONS_LITERATURE."(relationid,illustrated,illustrated_bw,show_illustrated,show_bw,mentioned,show_mentioned,discussed,show_discussed,date_created) VALUES('".$relationid['relationid']."','".$_POST['illustrated']."','".$_POST['illustrated_bw']."','".$_POST['show_illustrated']."','".$_POST['show_bw']."','".$_POST['mentioned']."','".$_POST['show_mentioned']."','".$_POST['discussed']."','".$_POST['show_discussed']."',NOW()) ");

        $relationid_lit=$db->return_insert_id();
        $options_relation_lit=array();
        $options_relation_lit['relationid']=$relationid['relationid'];
        admin_utils::order_literature_pages($db,$relationid_lit,$options_relation_lit);

        admin_utils::admin_log($db,1,10,$relationid['relationid']);
    }
}
else
{
    admin_utils::admin_log($db,"",11,$bookid,2);
}


unset($_SESSION['new_record']);

$url="/admin/books/edit/?bookid=".$bookid."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
