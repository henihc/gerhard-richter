<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="locations";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( $_GET['sp']=="1000000" )
{
    if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
    {
        $_GET['sort_by']="co.country_en";
        $_GET['sort_how']="asc";
    }
}
else
{
    if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
    {
        $_GET['sort_by']="lo.location_en";
        $_GET['sort_how']="asc";
    }
}


    print "\t<a href='/admin/locations/?sp=all' title='Show all' class='a_admin_edit'>Show all</a>&nbsp;\n";

    print "Country&nbsp;";
    $values=array();
    $values['name']="countryid";
    $values['selectedid']=$_GET['countryid'];
    $values['onchange']=" onchange=\"location.href='?countryid='+this.options[selectedIndex].value\" ";
    admin_html::select_locations_country($db,$values);

    if( !empty($_GET['countryid']) )
    {
        print "&nbsp;City&nbsp;";
        $values=array();
        $values['name']="cityid";
        $values['selectedid']=$_GET['cityid'];
        $values['onchange']=" onchange=\"location.href='?countryid=".$_GET['countryid']."&cityid='+this.options[selectedIndex].value\" ";
        $values['where']=" WHERE countryid='".$_GET['countryid']."' ";
        admin_html::select_locations_city($db,$values);
    }

    if( ( !empty($_GET['countryid']) && !empty($_GET['cityid']) ) || $_GET['sp']=="1000000" )
    {
        $values_query=array();
        if( $_GET['sp']=="1000000" )
        {
            $query_where_locations=", ".TABLE_LOCATIONS_CITY." ci, ".TABLE_LOCATIONS_COUNTRY." co WHERE lo.cityid=ci.cityid AND ci.countryid=co.countryid ";
            $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
            $values_query['columns']=", ci.city_en, co.country_en";
        }
        else
        {
            $query_where_locations=" WHERE countryid='".$_GET['countryid']."' && cityid='".$_GET['cityid']."' ";
            $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
        }
        if( $_GET['sp']!="1000000" ) $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
        $query_locations=QUERIES::query_locations($db,$query_where_locations,$query_order,$query_limit,$values_query);

        $results=$db->query($query_locations['query_without_limit']);
        $count=$db->numrows($results);
        $results_locations=$db->query($query_locations['query']);
        $pages=@ceil($count/$_GET['sp']);
    }

    if( $count>0 )
    {

    	//$url="&countryid=".$_GET['countryid']."&cityid=".$_GET['cityid']."&sp=".$_GET['sp']."&p=".$_GET['p'];
    	$url="?countryid=".$_GET['countryid']."&cityid=".$_GET['cityid'];

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,0);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    # locationid
                    if( $_GET['sort_by']=='lo.locationid' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=lo.locationid&sort_how=".$sort.$url."' title='Order by locationid'><u>locationid</u></a></th>";

                    if( $_GET['sp']=="1000000" )
                    {
                        # country_en
                        if( $_GET['sort_by']=='co.country_en' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=co.country_en&sort_how=".$sort.$url."' title='Order by country'><u>Country</u></a></th>";

                        # city_en
                        if( $_GET['sort_by']=='ci.city_en' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=ci.city_en&sort_how=".$sort.$url."' title='Order by city'><u>City</u></a></th>";
                    }

                    # location_en
                    if( $_GET['sort_by']=='lo.location_en' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=lo.location_en&sort_how=".$sort.$url."' title='Order by location_en'><u>location_en</u></a></th>";


                        # location_de
                        if( $_GET['sort_by']=='lo.location_de' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=lo.location_de&sort_how=".$sort.$url."' title='Order by location_de'><u>location_de</u></a></th>";

                        # location_it
                        if( $_GET['sort_by']=='lo.location_fr' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=lo.location_fr&sort_how=".$sort.$url."' title='Order by location_fr'><u>location_fr</u></a></th>";

                        # location_it
                        if( $_GET['sort_by']=='lo.location_it' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=lo.location_it&sort_how=".$sort.$url."' title='Order by location_it'><u>location_it</u></a></th>";

                        # location_it
                        if( $_GET['sort_by']=='lo.location_zh' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=lo.location_zh&sort_how=".$sort.$url."' title='Order by location_zh'><u>location_zh</u></a></th>";

                    //print "\t<th><a title='Count used on site'>c</a></th>\n";
                    print "\t<th>date created</th>\n";

                print "\t</tr>\n";
            print "\t</thead>\n";


        while( $row=$db->mysql_array($results_locations) )
        {
            $row=UTILS::html_decode($row);

            //$results_used=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE locationid='".$row['locationid']."'");
            //$count_exhibitions=$db->numrows($results_used);

            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/locations/edit/?typeid=1&locationid=".$row['locationid']."' style='text-decoration:underline;color:blue;' title='edit this location'>".$row['locationid']."</a>\n";
                print "\t</td>";
                if( $_GET['sp']=="1000000" )
                {
                    print "\t<td style='text-align:center;'>\n";
                        print "<a href='/admin/locations/edit/?typeid=3&countryid=".$row['countryid']."' title='' class='a_admin_edit' >".$row['country_en']."</a>";
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print "<a href='/admin/locations/edit/?typeid=2&cityid=".$row['cityid']."' title='' class='a_admin_edit' >".$row['city_en']."</a>";
                    print "\t</td>\n";
                }
                print "\t<td style='text-align:center;'>\n";
                    print $row['location_en'];
                print "\t</td>\n";

                    print "\t<td style='text-align:center;'>\n";
                        print $row['location_de'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['location_fr'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['location_it'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['location_zh'];
                    print "\t</td>\n";
                //print "\t<td style='text-align:center;'>\n";
                    //print $count_exhibitions;
                //print "\t</td>\n";
                print "\t<td>\n";
                    print $row['date_created_admin'];
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    elseif( !empty($_GET['countryid']) && !empty($_GET['cityid']) ) print "\t<p class='p_admin_no_data_found'>No locations found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
