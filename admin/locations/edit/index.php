<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# menu selected
$html->menu_admin_selected="locations";
$html->menu_admin_sub_selected="add_location";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



if( empty($_GET['typeid']) ) UTILS::redirect('/admin/locations');

if( $_GET['typeid']==1 )
{
    $query_where_location=" WHERE locationid='".$_GET['locationid']."' ";
    $values_query_location['columns']=",location_search_1, location_search_2, location_search_3, location_search_4";
    $query_location=QUERIES::query_locations($db,$query_where_location,"","",$values_query_location);
    $results_location=$db->query($query_location['query_without_limit']);
    $count=$db->numrows($results_location);
    $row=$db->mysql_array($results_location);
}
if( $_GET['typeid']==2 )
{
    $query_where_city=" WHERE cityid='".$_GET['cityid']."' ";
    $query_city=QUERIES::query_locations_city($db,$query_where_city);
    $results_city=$db->query($query_city['query_without_limit']);
    $count=$db->numrows($results_city);
    $row=$db->mysql_array($results_city);
}
if( $_GET['typeid']==3 )
{
    $query_where_country=" WHERE countryid='".$_GET['countryid']."' ";
    $query_country=QUERIES::query_locations_country($db,$query_where_country);
    $results_country=$db->query($query_country['query_without_limit']);
    $count=$db->numrows($results_country);
    $row=$db->mysql_array($results_country);
}

$row=UTILS::html_decode($row);

print "\t<script type='text/javascript'>\n";
    print "\twindow.onload = function()\n";
    print "\t{\n";
        if( $_GET['typeid']==1 )
        {
            /*
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-location li a','#div-admin-tabs-info-location .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        */
        }
        if( $_GET['typeid']==2 )
        {
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-city li a','#div-admin-tabs-info-city .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        }
        if( $_GET['typeid']==3 )
        {
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-country li a','#div-admin-tabs-info-country .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        }

        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-location-new li a','#div-admin-tabs-info-location-new .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";

        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-city-new li a','#div-admin-tabs-info-city-new .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";

        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-country-new li a','#div-admin-tabs-info-country-new .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";

    print "\t};\n";
print "\t</script>\n";


    print "\t<form action='post.php' method='post' enctype='multipart/form-data' >\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

                if( !empty($row['date_modified_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date modified</th>\n";
                        print "\t<td>".$row['date_modified_admin']."</td>\n";
                    print "\t</tr>\n";
                }   

                if( !empty($row['date_created_admin']) )
                {   
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Date created</th>\n";
                        print "\t<td>".$row['date_created_admin']."</td>\n";
                    print "\t</tr>\n";
                } 

                if( $_GET['typeid']==1 )
                {

                print "\t<tr>\n";
                    if( $_SESSION['new_record']['error']['countryid'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."' style='width:100px;'>*Country</th>\n";
                    print "\t<td>\n";
                        $values=array();
                        $values['name']="countryid";
                        if( !empty($_GET['countryid']) ) $countryid=$_GET['countryid'];
                        elseif( $_GET['error'] ) $countryid=$_SESSION['new_record']['countryid'];
                        else $countryid=$row['countryid'];
                        $values['selectedid']=$countryid;
                        $values['onchange']=" onchange=\"
                                     $('cityid').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                    if( this.options[selectedIndex].value!=0 ) {document.getElementById('tr-city').style.display='';document.getElementById('tr-country-new').style.display='none';}
                                    else {document.getElementById('tr-city').style.display='none';document.getElementById('tr-country-new').style.display='';} \" ";
                        admin_html::select_locations_country($db,$values);
                    print "\t</td>\n";
                print "\t</tr>\n";

                if( empty($row['countryid']) ) $style="display:none;";
                else $style="";
                print "\t<tr id='tr-city' style='".$style."'>\n";
                    if( $_SESSION['new_record']['error']['cityid'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."'>*City</th>\n";
                    print "\t<td>\n";
                        $values=array();
                        $values['name']="cityid";
                        if( !empty($_GET['cityid']) ) $cityid=$_GET['cityid'];
                        elseif( $_GET['error'] ) $cityid=$_SESSION['new_record']['cityid'];
                        else $cityid=$row['cityid'];
                        $values['selectedid']=$cityid;
                        $values['where']=" WHERE countryid='".$countryid."' ";
                        $values['onchange']=" onchange=\"";
                        $values['onchange'].="if( this.options[selectedIndex].value!=0 ) {document.getElementById('tr-city-new').style.display='none';}";
                        $values['onchange'].="else {document.getElementById('tr-city-new').style.display='';} ";
                        if( !empty($_GET['locationid']) && empty($_GET['cityid']) && empty($_GET['countryid'])  ) $values['onchange'].=" document.getElementById('tr-location').style.display='';";
                        $values['onchange'].="\" ";
                        admin_html::select_locations_city($db,$values);
                    print "\t</td>\n";
                print "\t</tr>\n";

/*
                if( empty($row['cityid']) || empty($row['countryid']) ) $style="display:none;";
                else $style="";
                print "\t<tr id='tr-location' style='".$style."'>\n";
                    if( $_SESSION['new_record']['error']['location_en'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left'>Location</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-location' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-location' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_en=$_SESSION['new_record']['location_en'];
                                else $location_en=$row['location_en'];
                                print "\t<input type='text' name='location_en' id='location_en' value=\"".$location_en."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_de=$_SESSION['new_record']['location_de'];
                                else $location_de=$row['location_de'];
                                print "\t<input type='text' name='location_de' id='location_de' value=\"".$location_de."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_fr=$_SESSION['new_record']['location_fr'];
                                else $location_fr=$row['location_fr'];
                                print "\t<input type='text' name='location_fr' id='location_fr' value=\"".$location_fr."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_it=$_SESSION['new_record']['location_it'];
                                else $location_it=$row['location_it'];
                                print "\t<input type='text' name='location_it' id='location_it' value=\"".$location_it."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_zh=$_SESSION['new_record']['location_zh'];
                                else $location_zh=$row['location_zh'];
                                print "\t<input type='text' name='location_zh' id='location_zh' value=\"".$location_zh."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                        if( $_SESSION['debug_page'] ) print "<br />Title search = ".$row['location_search_1']." | ".$row['location_search_2']." | ".$row['location_search_3'];
                    print "\t</td>\n";
                print "\t</tr>\n";
*/

                }

                if( $_GET['typeid']==2 )
                {

                print "\t<tr>\n";
                    if( $_SESSION['new_record']['error']['countryid'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."'>*Country</th>\n";
                    print "\t<td>\n";
                        $values=array();
                        $values['name']="countryid";
                        if( $_GET['error'] ) $values['selectedid']=$_SESSION['new_record']['countryid'];
                        else $values['selectedid']=$row['countryid'];
                        admin_html::select_locations_country($db,$values);
                    print "\t</td>\n";
                print "\t</tr>\n";



                print "\t<tr>\n";
                    if( $_SESSION['new_record']['error']['city_en'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left'>City</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-city' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-city' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_en=$_SESSION['new_record']['city_en'];
                                else $city_en=$row['city_en'];
                                print "\t<input type='text' name='city_en' id='city_en' value=\"".$city_en."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_de=$_SESSION['new_record']['city_de'];
                                else $city_de=$row['city_de'];
                                print "\t<input type='text' name='city_de' id='city_de' value=\"".$city_de."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_fr=$_SESSION['new_record']['city_fr'];
                                else $city_fr=$row['city_fr'];
                                print "\t<input type='text' name='city_fr' id='city_fr' value=\"".$city_fr."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_it=$_SESSION['new_record']['city_it'];
                                else $city_it=$row['city_it'];
                                print "\t<input type='text' name='city_it' id='city_it' value=\"".$city_it."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_zh=$_SESSION['new_record']['city_zh'];
                                else $city_zh=$row['city_zh'];
                                print "\t<input type='text' name='city_zh' id='city_zh' value=\"".$city_zh."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";
                }

                if( $_GET['typeid']==3 )
                {
                print "\t<tr>\n";
                    if( $_SESSION['new_record']['error']['country_en'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left'>Country</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-country' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-country' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_en=$_SESSION['new_record']['country_en'];
                                else $country_en=$row['country_en'];
                                print "\t<input type='text' name='country_en' id='country_en' value=\"".$country_en."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_de=$_SESSION['new_record']['country_de'];
                                else $country_de=$row['country_de'];
                                print "\t<input type='text' name='country_de' id='country_de' value=\"".$country_de."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_fr=$_SESSION['new_record']['country_fr'];
                                else $country_fr=$row['country_fr'];
                                print "\t<input type='text' name='country_fr' id='country_fr' value=\"".$country_fr."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_it=$_SESSION['new_record']['country_it'];
                                else $country_it=$row['country_it'];
                                print "\t<input type='text' name='country_it' id='country_it' value=\"".$country_it."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_zh=$_SESSION['new_record']['country_zh'];
                                else $country_zh=$row['country_zh'];
                                print "\t<input type='text' name='country_zh' id='country_zh' value=\"".$country_zh."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";
                }



                if( !$count )
                {
                print "\t<tr id='tr-country-new'>\n";
                    if( $_SESSION['new_record']['error']['countryid'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."'>Country</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-country-new' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-country-new' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_en=$_SESSION['new_record']['country_en'];
                                else $country_en=$row['country_en'];
                                print "\t<input type='text' name='country_en' id='country_en' value=\"".$country_en."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_de=$_SESSION['new_record']['country_de'];
                                else $country_de=$row['country_de'];
                                print "\t<input type='text' name='country_de' id='country_de' value=\"".$country_de."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_fr=$_SESSION['new_record']['country_fr'];
                                else $country_fr=$row['country_fr'];
                                print "\t<input type='text' name='country_fr' id='country_fr' value=\"".$country_fr."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_it=$_SESSION['new_record']['country_it'];
                                else $country_it=$row['country_it'];
                                print "\t<input type='text' name='country_it' id='country_it' value=\"".$country_it."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $country_zh=$_SESSION['new_record']['country_zh'];
                                else $country_zh=$row['country_zh'];
                                print "\t<input type='text' name='country_zh' id='country_zh' value=\"".$country_zh."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";


                print "\t<tr id='tr-city-new'>\n";
                    if( $_SESSION['new_record']['error']['cityid'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."'>City</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-city-new' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-city-new' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_en=$_SESSION['new_record']['city_en'];
                                else $city_en=$row['city_en'];
                                print "\t<input type='text' name='city_en' id='city_en' value=\"".$city_en."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_de=$_SESSION['new_record']['city_de'];
                                else $city_de=$row['city_de'];
                                print "\t<input type='text' name='city_de' id='city_de' value=\"".$city_de."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_fr=$_SESSION['new_record']['city_fr'];
                                else $city_fr=$row['city_fr'];
                                print "\t<input type='text' name='city_fr' id='city_fr' value=\"".$city_fr."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_it=$_SESSION['new_record']['city_it'];
                                else $city_it=$row['city_it'];
                                print "\t<input type='text' name='city_it' id='city_it' value=\"".$city_it."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $city_zh=$_SESSION['new_record']['city_zh'];
                                else $city_zh=$row['city_zh'];
                                print "\t<input type='text' name='city_zh' id='city_zh' value=\"".$city_zh."\" class='input_field_admin' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                    print "\t</td>\n";
                print "\t</tr>\n";
                }

                if( $_GET['typeid']!=1 ) $style="display:none;";
                else $style="";
                print "\t<tr id='tr-location' style='".$style."'>\n";
                    if( $_SESSION['new_record']['error']['location_en'] ) $class_error="error";
                    else $class_error="";
                    print "\t<th class='th_align_left ".$class_error."'>Location</th>\n";
                    print "\t<td>\n";
                        print "\t<ul id='ul-admin-tabs-location-new' class='ul-admin-tabs'>\n";
                            print "\t<li><a class='li-admin-tab ".$class_error."' href='#' >*English</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                            print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                        print "\t</ul>\n";
                        print "\t<div id='div-admin-tabs-info-location-new' class='div-admin-tabs-info'>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_en=$_SESSION['new_record']['location_en'];
                                else $location_en=$row['location_en'];
                                print "\t<input type='text' name='location_en' id='location_en' value=\"".$location_en."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_de=$_SESSION['new_record']['location_de'];
                                else $location_de=$row['location_de'];
                                print "\t<input type='text' name='location_de' id='location_de' value=\"".$location_de."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_fr=$_SESSION['new_record']['location_fr'];
                                else $location_fr=$row['location_fr'];
                                print "\t<input type='text' name='location_fr' id='location_fr' value=\"".$location_fr."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_it=$_SESSION['new_record']['location_it'];
                                else $location_it=$row['location_it'];
                                print "\t<input type='text' name='location_it' id='location_it' value=\"".$location_it."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                            print "\t<div class='div-admin-tab-info'>\n";
                                if( $_GET['error'] ) $location_zh=$_SESSION['new_record']['location_zh'];
                                else $location_zh=$row['location_zh'];
                                print "\t<input type='text' name='location_zh' id='location_zh' value=\"".$location_zh."\" class='input_field_admin_large' />\n";
                            print "\t</div>\n";
                        print "\t</div>\n";
                        if( $_SESSION['debug_page'] ) print "<br />Title search = ".$row['location_search_1']." | ".$row['location_search_2']." | ".$row['location_search_3'];
                    print "\t</td>\n";
                print "\t</tr>\n";

                //if( $count>0 && $row['cityid']!=666 && $row['countryid']!=82 && (!empty($row['locationid']) || !empty($row['cityid']) || !empty($row['countryid']) ) )
                if( $count>0 && (!empty($row['locationid']) || !empty($row['cityid']) || !empty($row['countryid']) ) )
                {
                    print "\t<tr>\n";
                        print "\t<th class='th_align_left'>Display</th>\n";
                        print "\t<td>\n";
                            $options_location=array();
                            $options_location['row']=$row;
                            print location($db,$options_location);
                        print "\t</td>\n";
                    print "\t</tr>\n";
                }

                print "\t<tr>\n";
                    print "\t<td>";
                        if( $count )
                        {
                            //print "<a href='#' onclick=\"delete_confirm2('del.php?typeid=".$_GET['typeid']."&locationid=".$row['locationid']."&cityid=".$row['cityid']."&countryid=".$row['countryid']."','".addslashes($row['location_en']." ".$row['city_en']." ".$row['country_en'])."','');\" title='delete this location'>delete</a>";
                            $options_delete=array();
                            $options_delete['url_del']="del.php?typeid=".$_GET['typeid']."&locationid=".$row['locationid']."&cityid=".$row['cityid']."&countryid=".$row['countryid']."";
                            $options_delete['text1']="Location - ".$row['location_en']." ".$row['city_en']." ".$row['country_en'];
                            //$options_delete['text2']="";
                            //$options_delete['html_return']=1;
                            admin_html::delete_button($db,$options_delete);
                        }
                        else print "* Mandatory";
                    print "</td>\n";
                    if( $count ) $value="update";
                    else $value="add";
                    print "\t<td><input name='submit' type='submit' value='".$value."' /></td>\n";
                print "\t</tr>\n";

            print "\t</table>\n";
            if( $_GET['typeid']==1 ) print "\t<input type='hidden' name='locationid' value='".$row['locationid']."' />\n";
            if( $_GET['typeid']==2 ) print "\t<input type='hidden' name='cityid' value='".$row['cityid']."' />\n";
            if( $_GET['typeid']==3 ) print "\t<input type='hidden' name='countryid' value='".$row['countryid']."' />\n";
            print "\t<input type='hidden' name='typeid' value='".$_GET['typeid']."' />\n";
        print "\t</form>\n";

    $query_where_paintings=" WHERE locationid='".$row['locationid']."' OR loan_locationid='".$row['locationid']."' ";
    $query_paintings=QUERIES::query_painting($db,$query_where_paintings,$query_order,$query_limit);
    $results_used_paintings=$db->query($query_paintings['query']);
    $count_used_paintings=$db->numrows($results_used_paintings);

    $query_where_exhibition=" WHERE locationid='".$row['locationid']."' ";
    $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
    $results_used=$db->query($query_exhibition['query']);
    $count_used=$db->numrows($results_used);

    if( ( $count_used>0 || $count_used_paintings>0 ) && $count && $_GET['typeid']==1 )
    {
        print "\t<ul class='tabsadmin' id='ul_link_tabs'>\n";
            if( $count_used_paintings>0 )
            {
                $selected="selected";
                print "\t<li class='Paintings tab'>\n";
                    print "\t<h4 style='top: 0px;'><a class='Paintings name ".$selected."' name='Paintings' href='#Paintings'>Paintings</a></h4>\n";
                print "\t</li>\n";
            }
            if( $count_used>0 )
            {
                if( empty($selected) ) $selected="selected";
                print "\t<li class='Exhibitions tab'>\n";
                    print "\t<h4 style='top: 0px;'><a class='Exhibitions name ".$selected."' name='Exhibitions' href='#Exhibitions'>Exhibitions</a></h4>\n";
                print "\t</li>\n";
            }
        print "\t</ul>\n";
        print "\t<br class='clear'>\n";
        if( $count_used_paintings>0 )
        {
            print "\t<div class='tabsadmin-info'>\n";
                print "\t<div id='' class='holder Paintings-info'>\n";
                    print "\t&nbsp;items found: ".$count_used_paintings."\n";
                    print "\t<table class='table-links'>\n";
                        print "\t<tbody>\n";
                            print "\t<tr>\n";
                                print "\t<th style='width:70px;text-align:center;'>paintid</th>\n";
                                print "\t<th style=''>number</th>\n";
                                print "\t<th style=''>image</th>\n";
                                print "\t<th style=''>titleEN</th>\n";
                                print "\t<th style=''>titleDE</th>\n";
                                print "\t<th style=''>artwork</th>\n";
                            print "\t</tr>\n";
                            while( $row_used_paintings=$db->mysql_array($results_used_paintings,0) )
                            {
                                print "\t<tr>\n";
                                    print "\t<td style='width:70px;text-align:center;'>\n";
                                        print "\t<a title='edit this item' href='/admin/paintings/edit/?paintid=".$row_used_paintings['paintID']."#collection'><u>".$row_used_paintings['paintID']."</u></a>\n";
                                    print "\t</td>\n";
                                    print "\t<td style='text-align:left;'>\n";
                                        print $row_used_paintings['number'];
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        print "<img src='/includes/retrieve.image.php?size=xs&paintID=".$row_used_paintings['paintID']."' alt='' />";
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        print $row_used_paintings['titleEN'];
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        print $row_used_paintings['titleDE'];
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        $artwork=UTILS::get_artwork_info($db,$row_item["artworkID"]);
                                        print $artwork['artworkEN'];
                                    print "\t</td>\n";
                                print "\t</tr>\n";
                            }
                        print "\t</tbody>\n";
                    print "\t</table>\n";
                print "\t</div>\n";
            print "\t</div>\n";
        }

        if( $count_used>0 )
        {
            print "\t<div class='tabsadmin-info'>\n";
                print "\t<div id='' class='holder Exhibitions-info hide'>\n";
                    print "\t&nbsp;items found: ".$count_used."\n";
                    print "\t<table class='table-links'>\n";
                        print "\t<tbody>\n";
                            print "\t<tr>\n";
                                print "\t<th style='width:70px;text-align:center;'>exhibitionid</th>\n";
                                print "\t<th style=''>title original</th>\n";
                                print "\t<th style=''>date</th>\n";
                                print "\t<th style=''>type</th>\n";
                                print "\t<th style=''>image</th>\n";
                            print "\t</tr>\n";
                            while( $row_used=$db->mysql_array($results_used,0) )
                            {
                                print "\t<tr>\n";
                                    print "\t<td style='width:70px;text-align:center;'>\n";
                                        print "\t<a title='edit this item' href='/admin/exhibitions/edit/?exhibitionid=".$row_used['exID']."'><u>".$row_used['exID']."</u></a>\n";
                                    print "\t</td>\n";
                                    print "\t<td style='text-align:left;'>\n";
                                        print $row_used['title_original'];
                                        $values_location=array();
                                        $values_location['row']=$row_used;
                                        $location=location($db,$values_location);
                                        print "<span style='color:#666;'><br>".$location."</span>";
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        $date=exhibition_date($db,$row_used);
                                        print $date;
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        print $row_used['type'];
                                    print "\t</td>\n";
                                    print "\t<td style=''>\n";
                                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row_used['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row_used['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");                
                                        $row_related_image=$db->mysql_array($results_related_image);
                                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                        print "<img alt='' src='/images/size_xs__imageid_".$imageid."'>";
                                    print "\t</td>\n";
                                print "\t</tr>\n";
                            }
                        print "\t</tbody>\n";
                    print "\t</table>\n";
                print "\t</div>\n";
            print "\t</div>\n";
        }
    }


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);

?>
