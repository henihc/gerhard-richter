<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

$db=new dbCLASS;


$_POST=$db->filter_parameters($_POST,1);
$_SESSION['new_record']=$_POST;

if( empty($_POST['typeid']) ) UTILS::redirect('/admin/locations');

//print_r($_POST);
//exit;
if( $_POST['typeid']==1 )
{

    $values_accented=array();
    $values_accented['text']=$_POST['location_de'];
    $location_search_de=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$_POST['location_fr'];
    $location_search_fr=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$_POST['location_it'];
    $location_search_it=UTILS::replace_accented_letters($db, $values_accented);

    //if( empty($_POST['cityid']) && empty($_POST['city_en']) ) $_SESSION['new_record']['error']['cityid']=true;
    if( empty($_POST['countryid']) && empty($_POST['country_en'])  ) $_SESSION['new_record']['error']['countryid']=true;
    //if( empty($_POST['location_en']) ) $_SESSION['new_record']['error']['location_en']=true;
    if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/locations/edit/?typeid=".$_POST['typeid']."&locationid=".$_POST['locationid']."&error=true");
    if( $_POST['submit']=="add" )
    {
        if( !empty($_POST['location_en']) ) $db->query("INSERT INTO ".TABLE_LOCATIONS."(date_created) VALUES(NOW()) ");
        $locationid=$db->return_insert_id();
    }
    elseif( $_POST['submit']=="update" )
    {
        $locationid=$_POST['locationid'];
    }



    # country info
    if( empty($_POST['countryid']) && !empty($_POST['country_en']) )
    {
        $values_accented=array();
        $values_accented['text']=$_POST['country_de'];
        $country_search_de=UTILS::replace_accented_letters($db, $values_accented);
        $values_accented=array();
        $values_accented['text']=$_POST['country_fr'];
        $country_search_fr=UTILS::replace_accented_letters($db, $values_accented);
        $values_accented=array();
        $values_accented['text']=$_POST['country_it'];
        $country_search_it=UTILS::replace_accented_letters($db, $values_accented);

        $db->query("INSERT INTO ".TABLE_LOCATIONS_COUNTRY."(date_created) VALUES(NOW()) ");
        $countryid=$db->return_insert_id();

        $query="UPDATE ".TABLE_LOCATIONS_COUNTRY." SET 
            country_en='".$_POST['country_en']."',
            country_de='".$_POST['country_de']."',
            country_fr='".$_POST['country_fr']."',
            country_it='".$_POST['country_it']."',
            country_zh='".$_POST['country_zh']."',
            country_search_1='".$country_search_de['text_lower_converted']."',
            country_search_2='".$country_search_de['text_lower_german_converted']."',
            country_search_3='".$country_search_fr['text_lower_converted']."',
            country_search_4='".$country_search_it['text_lower_converted']."' 
    ";
        if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
        $query.=" WHERE countryid='".$countryid."'";
         $db->query($query);

        if( $_POST['submit']=="add" || $_POST['submit']=="update" ) 
        {           
            if( $_POST['submit']=="add" )
            { 
                admin_utils::admin_log($db,1,18,$locationid);
            }
            elseif( $_POST['submit']=="update" )
            { 
                admin_utils::admin_log($db,2,18,$locationid);
            }
        }
        else
        {
            admin_utils::admin_log($db,"",18,$_POST['locationid'],2);
        }
    }
    elseif( empty($_POST['country_en']) ) $countryid=$_POST['countryid'];

    $query_where_country=" WHERE countryid='".$countryid."' ";
    $query_country=QUERIES::query_locations_country($db,$query_where_country);
    $results_country=$db->query($query_country['query_without_limit']);
    $count_country=$db->numrows($results_country);
    $row_country=$db->mysql_array($results_country);

    $values_accented=array();
    $values_accented['text']=$row_country['country_de'];
    $country_search_de=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$row_country['country_fr'];
    $country_search_fr=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$row_country['country_it'];
    $country_search_it=UTILS::replace_accented_letters($db, $values_accented);

    if( $count_country ) 
    {
        $location_search_de['text_lower_converted'].=" ".$country_search_de['text_lower_converted'];
        $location_search_de['text_lower_german_converted'].=" ".$country_search_de['text_lower_german_converted'];
        $location_search_fr['text_lower_converted'].=" ".$country_search_fr['text_lower_converted'];
        $location_search_it['text_lower_converted'].=" ".$country_search_it['text_lower_converted'];
    }
    # end

    # city info
    if( empty($_POST['cityid']) && !empty($_POST['city_en']) )
    {
        $values_accented=array();
        $values_accented['text']=$_POST['city_de'];
        $city_search_de=UTILS::replace_accented_letters($db, $values_accented);
        $values_accented=array();
        $values_accented['text']=$_POST['city_fr'];
        $city_search_fr=UTILS::replace_accented_letters($db, $values_accented);
        $values_accented=array();
        $values_accented['text']=$_POST['city_it'];
        $city_search_it=UTILS::replace_accented_letters($db, $values_accented);

        $db->query("INSERT INTO ".TABLE_LOCATIONS_CITY."(date_created) VALUES(NOW()) ");
        $cityid=$db->return_insert_id();

        $query="UPDATE ".TABLE_LOCATIONS_CITY." SET 
            countryid='".$countryid."',
            city_en='".$_POST['city_en']."',
            city_de='".$_POST['city_de']."',
            city_fr='".$_POST['city_fr']."',
            city_it='".$_POST['city_it']."',
            city_zh='".$_POST['city_zh']."',
            city_search_1='".$city_search_de['text_lower_converted']."',
            city_search_2='".$city_search_de['text_lower_german_converted']."',
            city_search_3='".$city_search_fr['text_lower_converted']."',
            city_search_4='".$city_search_it['text_lower_converted']."'

    ";
        if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
        $query.=" WHERE cityid='".$cityid."'";
        $db->query($query);

        if( $_POST['submit']=="add" || $_POST['submit']=="update" ) 
        {           
            if( $_POST['submit']=="add" )
            { 
                admin_utils::admin_log($db,1,19,$cityid);
            }
            elseif( $_POST['submit']=="update" )
            { 
                admin_utils::admin_log($db,2,19,$cityid);
            }
        }
        else
        {
            admin_utils::admin_log($db,"",19,$_POST['cityid'],2);
        }
    }
    elseif( empty($_POST['city_en']) ) $cityid=$_POST['cityid'];

    $query_where_city=" WHERE cityid='".$cityid."' ";
    $query_city=QUERIES::query_locations_city($db,$query_where_city);
    $results_city=$db->query($query_city['query_without_limit']);
    $count_city=$db->numrows($results_city);
    $row_city=$db->mysql_array($results_city,0);


    $values_accented=array();
    $values_accented['text']=$row_city['city_de'];
    $city_search_de=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$row_city['city_fr'];
    $city_search_fr=UTILS::replace_accented_letters($db, $values_accented);

    $values_accented=array();
    $values_accented['text']=$row_city['city_it'];
    $city_search_it=UTILS::replace_accented_letters($db, $values_accented);

    if( $count_city ) 
    {
        $location_search_de['text_lower_converted'].=" ".$city_search_de['text_lower_converted'];
        $location_search_de['text_lower_german_converted'].=" ".$city_search_de['text_lower_german_converted'];
        $location_search_fr['text_lower_converted'].=" ".$city_search_fr['text_lower_converted'];
        $location_search_it['text_lower_converted'].=" ".$city_search_it['text_lower_converted'];
    }
    # end


    if( !empty($_POST['location_en']) ) 
    {
        $query="UPDATE ".TABLE_LOCATIONS." SET 
            countryid='".$countryid."',
            cityid='".$cityid."',
            location_en='".$_POST['location_en']."',
            location_de='".$_POST['location_de']."',
            location_fr='".$_POST['location_fr']."',
            location_it='".$_POST['location_it']."',
            location_zh='".$_POST['location_zh']."',
            location_search_1='".$location_search_de['text_lower_converted']."',
            location_search_2='".$location_search_de['text_lower_german_converted']."',
            location_search_3='".$location_search_fr['text_lower_converted']."',
            location_search_4='".$location_search_it['text_lower_converted']."'
";

        if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
        $query.=" WHERE locationid='".$locationid."'";
        $url="/admin/locations/edit/?typeid=".$_POST['typeid']."&locationid=".$locationid."&task=".$_POST['submit'];
    }
    else 
    {
        if( !empty($_POST['cityid']) || !empty($_POST['city_en']) ) $url="/admin/locations/edit/?typeid=2&cityid=".$cityid."&task=".$_POST['submit'];
        elseif( !empty($_POST['countryid']) || !empty($_POST['country_en']) ) $url="/admin/locations/edit/?typeid=3&countryid=".$countryid."&task=".$_POST['submit'];
    }


        if( $_POST['submit']=="add" || $_POST['submit']=="update" ) 
        {           
            if( $_POST['submit']=="add" )
            { 
                admin_utils::admin_log($db,1,20,$countryid);
            }
            elseif( $_POST['submit']=="update" )
            { 
                admin_utils::admin_log($db,2,20,$countryid);
            }
        }
        else
        {
            admin_utils::admin_log($db,"",20,$_POST['countryid'],2);
        }
}
elseif( $_POST['typeid']==2 )
{
    $values_accented=array();
    $values_accented['text']=$_POST['city_de'];
    $city_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$_POST['city_fr'];
    $city_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$_POST['city_it'];
    $city_search_it=UTILS::replace_accented_letters($db, $values_accented);


    if( empty($_POST['city_en']) ) $_SESSION['new_record']['error']['city_en']=true;
    if( empty($_POST['countryid']) ) $_SESSION['new_record']['error']['countryid']=true;
    if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/locations/edit/?typeid=".$_POST['typeid']."&cityid=".$_POST['cityid']."&error=true");
    if( $_POST['submit']=="add" )
    {
        $db->query("INSERT INTO ".TABLE_LOCATIONS_CITY."(date_created) VALUES(NOW()) ");
        $cityid=$db->return_insert_id();
    }
    elseif( $_POST['submit']=="update" )
    {
        $cityid=$_POST['cityid'];
    }

    $query="UPDATE ".TABLE_LOCATIONS_CITY." SET 
        countryid='".$_POST['countryid']."',
        city_en='".$_POST['city_en']."',
        city_de='".$_POST['city_de']."',
        city_fr='".$_POST['city_fr']."',
        city_it='".$_POST['city_it']."',
        city_zh='".$_POST['city_zh']."',
        city_search_1='".$city_search_de['text_lower_converted']."',
        city_search_2='".$city_search_de['text_lower_german_converted']."',
        city_search_3='".$city_search_fr['text_lower_converted']."',
        city_search_4='".$city_search_it['text_lower_converted']."'

";
    if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
    $query.=" WHERE cityid='".$cityid."'";

    $url="/admin/locations/edit/?typeid=".$_POST['typeid']."&cityid=".$cityid."&task=".$_POST['submit'];

}
elseif( $_POST['typeid']==3 )
{
    $values_accented=array();
    $values_accented['text']=$_POST['country_de'];
    $country_search_de=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$_POST['country_fr'];
    $country_search_fr=UTILS::replace_accented_letters($db, $values_accented);
    $values_accented=array();
    $values_accented['text']=$_POST['country_it'];
    $country_search_it=UTILS::replace_accented_letters($db, $values_accented);

    if( empty($_POST['country_en']) ) $_SESSION['new_record']['error']['country_en']=true;
    if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/locations/edit/?typeid=".$_POST['typeid']."&countryid=".$_POST['countryid']."&error=true");
    if( $_POST['submit']=="add" )
    {
        $db->query("INSERT INTO ".TABLE_LOCATIONS_COUNTRY."(date_created) VALUES(NOW()) ");
        $countryid=$db->return_insert_id();
    }
    elseif( $_POST['submit']=="update" )
    {
        $countryid=$_POST['countryid'];
    }

    $query="UPDATE ".TABLE_LOCATIONS_COUNTRY." SET 
        country_en='".$_POST['country_en']."',
        country_de='".$_POST['country_de']."',
        country_fr='".$_POST['country_fr']."',
        country_it='".$_POST['country_it']."',
        country_zh='".$_POST['country_zh']."',
        country_search_1='".$country_search_de['text_lower_converted']."',
        country_search_2='".$country_search_de['text_lower_german_converted']."',
        country_search_3='".$country_search_fr['text_lower_converted']."',
        country_search_4='".$country_search_it['text_lower_converted']."' 
";
    if( $_POST['submit']=="update" ) $query.=", date_modified=NOW() ";
    $query.=" WHERE countryid='".$countryid."'";

    $url="/admin/locations/edit/?typeid=".$_POST['typeid']."&countryid=".$countryid."&task=".$_POST['submit'];

}


if( $_POST['submit']=="add" || $_POST['submit']=="update" ) 
{   
     if( !empty($_POST['location_en']) || $_POST['typeid']==2 ||$_POST['typeid']==3 ) $db->query($query);
}
   


unset($_SESSION['new_record']);

UTILS::redirect($url);
