<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

if( !empty($_GET['typeid']) && ( !empty($_GET['locationid']) || !empty($_GET['cityid']) || !empty($_GET['countryid']) ) )
{
    $db=new dbCLASS;
    

    $_GET=$db->db_prepare_input($_GET);

    if( $_GET['typeid']==1 )
    {
        $db->query("DELETE FROM ".TABLE_LOCATIONS." WHERE locationid='".$_GET['locationid']."' ");

        $url_redirect="/admin/locations/?countryid=".$_GET['countryid']."&cityid=".$_GET['cityid']."&task=delete";
        
        admin_utils::admin_log($db,3,18,$_GET['locationid']);
    }
    else
    {
        admin_utils::admin_log($db,3,18,$_GET['locationid'],2);
    }
    
    if( $_GET['typeid']==2 )
    {
        $db->query("DELETE FROM ".TABLE_LOCATIONS_CITY." WHERE cityid='".$_GET['cityid']."' ");

        $url_redirect="/admin/locations/cities/?countryid=".$_GET['countryid']."&task=delete";

        admin_utils::admin_log($db,3,19,$_GET['cityid']);
    }
    else
    {
        admin_utils::admin_log($db,3,19,$_GET['cityid'],2);
    }

    if( $_GET['typeid']==3 )
    {
        $db->query("DELETE FROM ".TABLE_LOCATIONS_COUNTRY." WHERE countryid='".$_GET['countryid']."' ");

        $url_redirect="/admin/locations/countries/?task=delete";

        admin_utils::admin_log($db,3,20,$_GET['countryid']);
    }
    else
    {
        admin_utils::admin_log($db,3,20,$_GET['countryid'],2);
    }


    
    UTILS::redirect($url_redirect);
}
else UTILS::redirect("/");
