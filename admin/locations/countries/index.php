<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="locations";
$html->menu_admin_sub_selected="countries";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
{
    $_GET['sort_by']="country_en";
    $_GET['sort_how']="asc";
}


    $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    if( $_GET['sp']!="all" ) $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_countries=QUERIES::query_locations_country($db,$query_where_countries,$query_order,$query_limit);

    
    $results=$db->query($query_countries['query_without_limit']);
    $count=$db->numrows($results);
    $results_countries=$db->query($query_countries['query']);
    $pages=@ceil($count/$_GET['sp']);

    if( $count>0 )
    {
        $url_page_numbering="&sort_by=".$_GET['sort_by']."ffff&sort_how=".$_GET['sort_how'];
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url_page_numbering,"",$class,0);
        print "\t<a href='/admin/locations/countries/?sp=all' title='Show all' class='a_admin_edit'>Show all</a>&nbsp;\n";

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    $url="&sp=".$_GET['sp']."&p=".$_GET['p'];

                    # locationid
                    if( $_GET['sort_by']=='countryid' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=countryid&sort_how=".$sort.$url."' title='Order by countryid'><u>countryid</u></a></th>";

                    # country_en
                    if( $_GET['sort_by']=='country_en' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=country_en&sort_how=".$sort.$url."' title='Order by country_en'><u>country_en</u></a></th>";

                    # country_de
                    if( $_GET['sort_by']=='country_de' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=country_de&sort_how=".$sort.$url."' title='Order by country_de'><u>country_de</u></a></th>";

                    # country_fr
                    if( $_GET['sort_by']=='country_fr' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=country_fr&sort_how=".$sort.$url."' title='Order by country_fr'><u>country_fr</u></a></th>";

                    # country_it
                    if( $_GET['sort_by']=='country_it' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=country_it&sort_how=".$sort.$url."' title='Order by country_it'><u>country_it</u></a></th>";

                    # country_zh
                    if( $_GET['sort_by']=='country_zh' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=country_zh&sort_how=".$sort.$url."' title='Order by country_zh'><u>country_zh</u></a></th>";

                    //print "\t<th><a title='Count used on site'>c</a></th>\n";
                    print "\t<th>date created</th>\n";
                print "\t</tr>\n";
            print "\t</thead>\n";

        while( $row=$db->mysql_array($results_countries) )
        {
            $row=UTILS::html_decode($row);

            //$results_used=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE countryid='".$row['countryid']."'");
            //$count_exhibitions=$db->numrows($results_used);

            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/locations/edit/?typeid=3&countryid=".$row['countryid']."' style='text-decoration:underline;color:blue;' title='edit this country'>".$row['countryid']."</a>\n";
                print "\t</td>";
                print "\t<td style='text-align:center;'>\n";
                    print $row['country_en'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['country_de'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['country_fr'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['country_it'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['country_zh'];
                print "\t</td>\n";
                //print "\t<td style='text-align:center;'>\n";
                    //print $count_exhibitions;
                //print "\t</td>\n";
                print "\t<td>\n";
                    print $row['date_created_admin'];
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,0);
    }
    else print "\t<p class='p_admin_no_data_found'>No country found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
