<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="locations";
$html->menu_admin_sub_selected="cities";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

if( $_GET['sp']=="all" )
{
    if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
    {   
        $_GET['sort_by']="co.country_en";
        $_GET['sort_how']="asc";
    }   
}
else
{
    if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
    {   
        $_GET['sort_by']="ci.city_en";
        $_GET['sort_how']="asc";
    }   
}


    print "\t<a href='/admin/locations/cities/?sp=all' title='Show all' class='a_admin_edit'>Show all</a>&nbsp;\n";

    print "Country&nbsp;";
    $values=array();
    $values['name']="countryid";
    $values['selectedid']=$_GET['countryid'];
    $values['onchange']=" onchange=\"location.href='?countryid='+this.options[selectedIndex].value\" ";
    admin_html::select_locations_country($db,$values);


    //if( !empty($_GET['countryid']) || $_GET['sp']=="all" )
    //{
        $values_query=array();
        if( $_GET['sp']=="all" )
        {
            $query_where_cities=", ".TABLE_LOCATIONS_COUNTRY." co WHERE ci.countryid=co.countryid ";
            $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
            $values_query['columns']=", co.country_en";
        }
        elseif( !empty($_GET['countryid']) )
        {
            $query_where_cities=" WHERE countryid='".$_GET['countryid']."' ";
            $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
            $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
        }
        $query_cities=QUERIES::query_locations_city($db,$query_where_cities,$query_order,$query_limit,$values_query);
    
        $results=$db->query($query_cities['query_without_limit']);
        $count=$db->numrows($results);
        $results_cities=$db->query($query_cities['query']);
        $pages=@ceil($count/$_GET['sp']);
    //}

    if( $count>0 )
    {
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    $url="&countryid=".$_GET['countryid']."&sp=".$_GET['sp']."&p=".$_GET['p'];

                    # cityid
                    if( $_GET['sort_by']=='ci.cityid' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.cityid&sort_how=".$sort.$url."' title='Order by cityid'><u>cityid</u></a></th>";

                    if( $_GET['sp']=="all" )
                    {
                        # country_en
                        if( $_GET['sort_by']=='co.country_en' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort='desc';
                            else $sort='asc';
                        }
                        else $sort='asc';
                        print "<th><a href='?sort_by=co.country_en&sort_how=".$sort.$url."' title='Order by country'><u>Country</u></a></th>";
                    }

                    # city_en
                    if( $_GET['sort_by']=='ci.city_en' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.city_en&sort_how=".$sort.$url."' title='Order by city_en'><u>city_en</u></a></th>";

                    # city_de
                    if( $_GET['sort_by']=='ci.city_de' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.city_de&sort_how=".$sort.$url."' title='Order by city_de'><u>city_de</u></a></th>";

                    # city_fr
                    if( $_GET['sort_by']=='ci.city_fr' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.city_fr&sort_how=".$sort.$url."' title='Order by city_fr'><u>city_fr</u></a></th>";

                    # city_it
                    if( $_GET['sort_by']=='ci.city_it' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.city_it&sort_how=".$sort.$url."' title='Order by city_it'><u>city_it</u></a></th>";

                    # city_zh
                    if( $_GET['sort_by']=='ci.city_zh' )
                    {
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }
                    else $sort='asc';
                    print "<th><a href='?sort_by=ci.city_zh&sort_how=".$sort.$url."' title='Order by city_zh'><u>city_zh</u></a></th>";

                    //print "\t<th><a title='Count used on site'>c</a></th>\n";
                    print "\t<th>date created</th>\n";
                print "\t</tr>\n";
            print "\t</thead>\n";

        while( $row=$db->mysql_array($results_cities) )
        {
            $row=UTILS::html_decode($row);

            //$results_used=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE cityid='".$row['cityid']."'");
            //$count_exhibitions=$db->numrows($results_used);

            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>\n";
                    print "\t<a href='/admin/locations/edit/?typeid=2&cityid=".$row['cityid']."' style='text-decoration:underline;color:blue;' title='edit this city'>".$row['cityid']."</a>\n";
                print "\t</td>";
                if( $_GET['sp']=="all" )
                {
                    print "\t<td style='text-align:center;'>\n";
                        print "<a href='/admin/locations/edit/?typeid=3&countryid=".$row['countryid']."' title='' class='a_admin_edit' >".$row['country_en']."</a>";
                    print "\t</td>\n";
                }

                print "\t<td style='text-align:center;'>\n";
                    print $row['city_en'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['city_de'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['city_fr'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['city_it'];
                print "\t</td>\n";
                print "\t<td style='text-align:center;'>\n";
                    print $row['city_zh'];
                print "\t</td>\n";
                //print "\t<td style='text-align:center;'>\n";
                    //print $count_exhibitions;
                //print "\t</td>\n";
                print "\t<td>\n";
                    print $row['date_created_admin'];
                print "\t</td>\n";
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,false);
    }
    //elseif( !empty($_GET['locationid']) ) print "\t<p class='p_admin_no_data_found'>No locations found!</p>\n";
    elseif( !empty($_GET['countryid']) ) print "\t<p class='p_admin_no_data_found'>No city found!</p>\n";
    //elseif( !empty($_GET['countryid']) ) print "\t<p class='p_admin_no_data_found'>No country found!</p>\n";
    //else print "\t<p>Please select country!</p>\n";
    //else print "\t<p class='p_admin_no_data_found'>No city found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
