<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/tooltip.js";
$html->head('admin');

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);
$_GET=UTILS::ifemptyGET($_GET,20);


print "\t<div id='div_admin'>\n";

    for($i=0;$i<sizeof($_GET['year']);$i++) $years.="&year[]=".$_GET['year'][$i];

    $url="/admin/search/results.php?".$_SERVER["QUERY_STRING"];


    function display_search_keys($found,$search)
    {
        $new_look = str_replace($search, "<font style='font-weight:bold'>".$search."</font>", $found);
        $new_look = str_replace(ucwords($search), "<font style='font-weight:bold'>".ucwords($search)."</font>", $found);
        return $new_look;
    }

    # menu admin
    print "\t<a href='/admin' title=''><u>main menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search/' title=''><u>search</u></a><br /><br />\n";
    #end menu admin



#this is to keep searched values if user goes back to form
unset($_SESSION['search']);

$_SESSION['search']['url_sale']=$url;


$_SESSION['search']['show_year']=$_GET['show_year'];
$_SESSION['search']['show_category']=$_GET['show_category'];
$_SESSION['search']['show_artwork']=$_GET['show_artwork'];
$_SESSION['search']['show_size']=$_GET['show_size'];
$_SESSION['search']['show_media']=$_GET['show_media'];
$_SESSION['search']['show_saleDate']=$_GET['show_saleDate'];
$_SESSION['search']['show_saleName']=$_GET['show_saleName'];
$_SESSION['search']['show_auctionHouse']=$_GET['show_auctionHouse'];
$_SESSION['search']['show_lotNo']=$_GET['show_lotNo'];
$_SESSION['search']['show_estimate']=$_GET['show_estimate'];


$_SESSION['search']['artwork']=$_GET['artworkID'];
$_SESSION['search']['title']=$_GET['title'];
for($i=0;$i<sizeof($_GET['year']);$i++){$_SESSION['search']['year'][]=$_GET['year'][$i];}
$_SESSION['search']['number']=$_GET['number'];
$_SESSION['search']['catID']=$_GET['catID'];
$_SESSION['search']['height']=$_GET['height'];
$_SESSION['search']['width']=$_GET['width'];
$_SESSION['search']['mID']=$_GET['mID'];
$_SESSION['search']['sale_from']=$_GET['sale_from'];
$_SESSION['search']['sale_to']=$_GET['sale_to'];
$_SESSION['search']['ahID']=$_GET['ahID'];
$_SESSION['search']['sale_name']=$_GET['sale_name'];
$_SESSION['search']['lotNo']=$_GET['lotNo'];
$_SESSION['search']['estCurrID']=$_GET['estCurrID'];
$_SESSION['search']['estLow']=$_GET['estLow'];
$_SESSION['search']['estHigh']=$_GET['estHigh'];
$_SESSION['search']['soldForCurrID']=$_GET['soldForCurrID'];
$_SESSION['search']['soldMin']=$_GET['soldMin'];
$_SESSION['search']['soldMax']=$_GET['soldMax'];
$_SESSION['search']['bought_in']=$_GET['bought_in'];

#end

#change date format 
$_GET['sale_from']=UTILS::dates_interconv("d-m-Y","Y-m-d", $_GET['sale_from']);
$_GET['sale_to']=UTILS::dates_interconv("d-m-Y","Y-m-d", $_GET['sale_to']);
#end


#prepeare variables
$_GET=$db->db_prepare_input($_GET);
#end



#Building query

#this sets wheather to select one artwork type or all of them
if(empty($_GET['artwork'])) {$artwork_from=1;$artwork_till=15;}
else {$artwork_from=$_GET['artwork'];$artwork_till=$_GET['artwork'];}
#end


/*
	$query="
	SELECT 
	p.paintID,p.artworkID,p.number,p.titleEN,p.titleDE,p.year,p.atlasID,p.catID,p.size,p.height,p.width,p.src,p.mID,
	saleID,DATE_FORMAT(s.saleDate, '%D %b %Y') AS saleDate,s.ahID,s.saleName,s.soldFor,s.soldForCurrID,s.soldForUSD,s.boughtIn,s.lotNo,s.estLow,s.estHigh,s.estCurrID
	FROM 
	".TABLE_PAINTING." p , 
	".TABLE_SALEHISTORY." s
	WHERE 
	p.artworkID = s.artworkID AND 
	p.paintID = s.paintID";
 */

	$query="
	SELECT 
	p.paintID,p.artworkID,p.number,p.titleEN,p.titleDE,p.year,p.atlasID,p.catID,p.size,p.height,p.width,p.src,p.mID
	FROM 
	".TABLE_PAINTING." p 
	WHERE p.paintID=p.paintID ";





//ARTWORK

	#ARTWODKID
	if(!empty($_GET['artworkID']) && $_GET['artworkID']=="paintings" ) $query.=" and ( p.artworkID = 1 or p.artworkID = 2 or p.artworkID = 12 or p.artworkID = 13 or p.artworkID = 14  ) ";
	else if(!empty($_GET['artworkID'])) $query.=" and p.artworkID = '".$_GET['artworkID']."' ";

	#TITLE
	if(!empty($_GET['title'])) $query.=" and IF('".strlen($_GET['title'])."'>0, MATCH(p.titleEN,p.titleDE) AGAINST ('".UTILS::prepear_search($_GET['title'])."' IN BOOLEAN MODE), p.titleEN=p.titleEN ) ";
	
	#YEAR
	if(!empty($_GET['year'][0])) $query.=" and (p.year = '".$_GET['year'][0]."' ";
	for($i=1;$i<sizeof($_GET['year']);$i++) $query.=" or p.year = '".$_GET['year'][$i]."' ";
	if(!empty($_GET['year'][0]))$query.=" )";
	
	#NUMBER
	if(!empty($_GET['number'])) 
	{
	
	//$query.=" and p.number LIKE '".$_GET['number']."%' ";
	
	
		$search_array=UTILS::search_array($_GET['number']);

        if( sizeof($search_array)<=1 ) $numer_search.=" p.number = '".$search_array[0]."' ";
        else
        {
            $i=0;
            foreach( $search_array as $value )
            {
                $i++;
                $numer_search.=" p.number = '".$value."' ";
                if( $i!=sizeof($search_array) ) $numer_search.=" OR ";
            }
        }

	
	if( sizeof($search_array)==0 )
	{
		list($_GET['number'], $tmp) = split('[-]', $_GET['number']);
		$numer_search.=" p.number LIKE '".str_replace("-", "", $_GET['number'])."-%' ";
		$numer_search.=" or p.number LIKE '".$_GET['number']." -%' ";
		$numer_search.=" or p.number LIKE '".$_GET['number']." - %' ";
		$numer_search.=" or p.number LIKE '".$_GET['number']."/%' ";
		
	    $numer_search.=" or p.number LIKE '".$_GET['number']."a' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."a%' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."b' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."b%' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."c' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."c%' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."d' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."d%' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."e' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."e%' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."f' ";
	    $numer_search.=" or p.number LIKE '".$_GET['number']."f%' ";
		if( strlen($_GET['number'])>=3 ) $numer_search.=" or p.number LIKE '".$_GET['number']."%' ";
	}
	
	
	
	$query.=" and IF('".strlen($_GET['number'])."'>0,$numer_search, p.number LIKE '%') ";
	
	}
	
	#SERIES(catID)
	if(!empty($_GET['catID'])) $query.=" and p.catID = '".$_GET['catID']."' ";

	#HEIGHT
	if(!empty($_GET['height'])) $query.=" and p.height='".$_GET['height']."' ";

	#WIDTH
	if(!empty($_GET['width'])) $query.=" and p.width='".$_GET['width']."' ";

	#MID(mediaID)
	if(!empty($_GET['mID'])) $query.=" and p.mID='".$_GET['mID']."' ";



//END

//SALES HISTORY
/*
	#SALE DATE
	if(!empty($_GET['sale_from']) && !empty($_GET['sale_to'])) $query.=" and s.saleDate BETWEEN '".$_GET['sale_from']."' AND '".$_GET['sale_to']."' ";
	elseif(empty($_GET['sale_from']) && !empty($_GET['sale_to'])) $query.=" and s.saleDate<='".$_GET['sale_to']."' ";
	elseif(!empty($_GET['sale_from']) && empty($_GET['sale_to'])) $query.=" and s.saleDate>='".$_GET['sale_from']."' ";

	#AUCTIONHOUSE(ahID)
	if(!empty($_GET['ahID'])) $query.=" and s.ahID='".$_GET['ahID']."' ";

	#SALE NAME
	if(!empty($_GET['sale_name'])) $query.=" and s.saleName LIKE '%".$_GET['sale_name']."%' ";

	#AUCTIONHOUSE(ahID)
	if(!empty($_GET['lotNo'])) $query.=" and s.lotNo='".$_GET['lotNo']."' ";

	#Estimate(estCurrID,estLow,estHigh)
	if(!empty($_GET['estLow']) && !empty($_GET['estHigh'])) $query.=" and s.estLow >= '".$_GET['estLow']."' and s.estHigh <= '".$_GET['estHigh']."' and s.estCurrID = '".$_GET['estCurrID']."' ";
	elseif(empty($_GET['estLow']) && !empty($_GET['estHigh'])) $query.=" and s.estHigh <= '".$_GET['estHigh']."' and s.estCurrID = '".$_GET['estCurrID']."' ";
	elseif(!empty($_GET['estLow']) && empty($_GET['estHigh'])) $query.=" and s.estLow >= '".$_GET['estLow']."' and s.estCurrID = '".$_GET['estCurrID']."' ";
	elseif(!empty($_GET['estCurrID']))
	{
	//if($_GET['estCurrID']!=1)
	$query.=" and s.estCurrID = '".$_GET['estCurrID']."' ";
	}





	#SOLD FOR(soldFor,bought_in)
	if($_GET['bought_in']==true) $query.=" and s.boughtIn=1  ";
	elseif(!empty($_GET['soldMin']) && !empty($_GET['soldMax']))
	{
	  //if($_GET['soldForCurrID']==1) $query.=" and s.soldForUSD BETWEEN '".$_GET['soldMin']."' AND '".$_GET['soldMax']."'";
	  //else 
	  $query.=" and s.soldFor BETWEEN '".$_GET['soldMin']."' AND '".$_GET['soldMax']."' and s.soldForCurrID='".$_GET['soldForCurrID']."'";
	}
	elseif(empty($_GET['soldMin']) && !empty($_GET['soldMax']))
	{
	  //if($_GET['soldForCurrID']==1) $query.=" and s.soldForUSD <= '".$_GET['soldMax']."' ";
	  //else 
	  $query.=" and s.soldFor <= '".$_GET['soldMax']."' and s.soldForCurrID = '".$_GET['soldForCurrID']."' ";
	}
	elseif(!empty($_GET['soldMin']) && empty($_GET['soldMax']))
	{
	  //if($_GET['soldForCurrID']==1) $query.=" and s.soldForUSD >= '".$_GET['soldMin']."' ";
	  //else 
	  $query.=" and s.soldFor >= '".$_GET['soldMin']."' and s.soldForCurrID = '".$_GET['soldForCurrID']."' ";
	}
	elseif( !empty($_GET['soldForCurrID']) )
	{
	  $query.=" and s.soldForCurrID = '".$_GET['soldForCurrID']."' ";
	}






 */

	
//END


 	$query.=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort'];
	
	
	
	$result_total=$db->query($query);
	
	if($_GET['page_selected']==1) $page_selected=($_GET['page_selected']-1);
	else $page_selected=($_GET['page_selected']-1)*16;
	
	$query.=" LIMIT ".$page_selected.",16";
	//print $query;
	$_SESSION['search']['query_sale']=$query;
	$result=$db->query($query);


#end building query


	print "<table class='admin-table'>\n";


#PAGE NUMBERING
	  $count=$db->numrows($result_total);
	  $how_much_pages=@ceil($count/16);
	  if($how_much_pages>1)
	  {
	  if($_GET['page_selected']!=1) $link=$url."&page_selected=".($_GET['page_selected']-1);
	  else $link="#";
	  print "<tr><td style='text-align: center' colspan='20'><a href='".$link."'>&#060;</a>";
	  	for($i=1;$i<=$how_much_pages;$i++)
	  	{
	  	if($_GET['page_selected']==$i) $selected=" style='font-weight:bold;'";
	  	else $selected="";
	  	print " <a href='".$url."&page_selected=$i' $selected>".$i."</a> ";
	  	}
	  if($_GET['page_selected']!=$how_much_pages) $link=$url."&page_selected=".($_GET['page_selected']+1);
	  else $link="#";
	  print "<a href='".$link."'>&#062;</a></td></tr>";
	  }

#end
	


#SORT LINKS

	  print "<tr>";

	//NUMBER
	    if($_GET['show_number']==false)
	    {
	      if($_GET['sort_by']=="number" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="number" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=number' title='Order by Number $sort_text'><u>Number</u> $sort_img</a></th>";
	    }


	//IMAGE
	    if($_GET['show_image']==false) print "<th>Image</th>";



	//TITLE
	    if($_GET['show_title']==false)
	    {
	      if($_GET['sort_by']=="titleEN" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="titleEN" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=titleEN' title='Order by Title $sort_text'><u>Title</u> $sort_img</a></th>";
	    }


	//YEAR
	    if($_GET['show_year']==true)
	    {
	      if($_GET['sort_by']=="year" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="year" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=year' title='Order by Year $sort_text'><u>Year</u> $sort_img</a></th>";
	    }


	//CATEGORY
	    if($_GET['show_category']==true)
	    {
	      if($_GET['sort_by']=="catID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="catID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=catID' title='Order by catID $sort_text'><u>Category</u> $sort_img</a></th>";
	    }

	//CATEGORY
	    if($_GET['show_artwork']==true)
	    {
	      if($_GET['sort_by']=="artworkID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="artworkID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=artworkID' title='Order by Artwork $sort_text'><u>Artwork</u> $sort_img</a></th>";
	    }

	//SIZE
	    if($_GET['show_size']==true) print "<th>Size</th>";



	//MEDIUM
	    if($_GET['show_media']==true)
	    {
	      if($_GET['sort_by']=="mID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="mID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=mID' title='Order by mID $sort_text'><u>Medium</u> $sort_img</a></th>";
	    }

	//SALE DATE
	    if($_GET['show_saleDate']==true)
	    {
	      if($_GET['sort_by']=="saleDate" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="saleDate" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=saleDate' title='Order by saleDate $sort_text'><u>Sale Date</u> $sort_img</a></th>";
	    }

	//SALE NAME
	    if($_GET['show_saleName']==true)
	    {
	      if($_GET['sort_by']=="saleName" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="saleName" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=saleName' title='Order by saleName $sort_text'><u>Sale Name</u> $sort_img</a></th>";
	    }

	//AUCTION HOUSE
	    if($_GET['show_auctionHouse']==true)
	    {
	      if($_GET['sort_by']=="ahID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="ahID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=ahID' title='Order by ahID $sort_text'><u>Auction House</u> $sort_img</a></th>";
	    }

	//LOT NO
	    if($_GET['show_lotNo']==true)
	    {
	      if($_GET['sort_by']=="lotNo" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="lotNo" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=lotNo' title='Order by lotNo $sort_text'><u>Lot No</u> $sort_img</a></th>";
	    }

	//ESTIMATE 
	    if($_GET['show_estimate']==true) print "<th>Estimate L/H</th>";

	//SALE TYPE
//	    if($_GET['show_saleType']==true)
//	    {
//	      if($_GET['sort_by']=="saleType" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
//	      elseif($_GET['sort_by']=="saleType" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
//	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
//	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=saleType' title='Order by saleType $sort_text'><u>Sale Type</u> $sort_img</a></th>";
//	    }
	  
	  	//SOLD FOR
	    if($_GET['show_soldFor']==false)
	    {
	      if($_GET['sort_by']=="soldFor" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="soldFor" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_text="Descending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=soldFor' title='Order by soldFor $sort_text'><u>Sold For</u> $sort_img</a></th>";
	    }
	  
	  print "</tr>";


#end - sort links


	    $tmp=array();
	    while($row=$db->mysql_array($result))
	    {
		$artwork_image_dir=UTILS::get_artwork_image_dir($db,$row['artworkID']);
		$artwork_dir_admin=UTILS::get_artwork_dir_admin($db,$row['artworkID']);


	    #changing text with bold text to indicate found places
	    $row['titleEN2'] = display_search_keys($row['titleEN'],$_GET['title']);
	    $row['titleDE2'] = display_search_keys($row['titleDE'],$_GET['title']);
	    $row['year2'] = display_search_keys($row['year'],$_GET['year']);
	    $row['atlasID2'] = display_search_keys($row['atlasID'],$_GET['atlas']);
	    $row['height2'] = display_search_keys($row['height'],$_GET['height']);
	    $row['width2'] = display_search_keys($row['width'],$_GET['width']);
	    $row['saleName2'] = display_search_keys($row['saleName'],$_GET['sale_name']);
	    #end



	    #building data table
	      
	      #adding odd and evan row
	      if(empty($style) && @!array_search($row['paintID'],$tmp)) $style=" style='background:#A2A2A2'";
	      elseif(!empty($style) && @!array_search($row['paintID'],$tmp)) $style="";
	      //else $style="";
	      #end
	    
	    print "<tr $style>";


		if(@!array_search($row['paintID'],$tmp))
		{
		  //IMAGE
	    	  if($_GET['show_number']==false)
	    	  {
		  print "\t<td>".$row['number']."</td>\n";
		  }

		  //IMAGE
	    	  if($_GET['show_image']==false)
	    	  {
		  print "\t<td>";
		    

		    
		    
		    #If image doesn't exist showing image not available



  $src="/includes/retrieve.image.php?paintID=".$row['paintID']."&size=m";



		    print "<a href='/admin/saleHistory/?".$row['paintID']."'><img src='".$src."' alt='".$row['titleEN']."' /></a>";
		    #end
		    
		  $tmp[]=$row['paintID'];
		  if($row['artworkID']!=3)$paintid=$row['paintID'];
		  else $paintid=$row['atlasID'];
		  print "<ul class='painting-details-brief'>";
		  print "<li><a title='Edit Sales History' href='/admin/saleHistory/view.php?".$row['saleID']."' class='info-sales'>Edit Sales History</a></li>";
		  print "<li><a title='Edit Painting Info' href='/admin/".$artwork_dir_admin."/?".$paintid."' class='info-museum'>Edit Painting Info</a></li>";
		  print "</ul>";
		  print "</td>\n";
		  }
		}
		else
		{
		print "\t<td colspan='2' >"; 
		  print "<ul class='painting-details-brief'>";
		  print "<li><a title='Edit Sales History' href='/admin/saleHistory/view.php?".$row['saleID']."' class='info-sales'>Edit Sales History</a></li>";
		  print "</ul>";
		print "</td>\n";
		}


	    //TITLE
	    if($_GET['show_title']==false)
	    {
	    	print "\t<td><p>".$row['titleEN2']."</p><p>".$row['titleDE2']."</p></td>\n";
	    }

	    //YEAR
	    if($_GET['show_year']==true)
	    {
	    print "\t<td>".$row['year2']."</td>\n";
	    }

	    //ATLAS
	    if($_GET['show_atlas']==true)
	    {
	    print "\t<td>";
	    if(!empty($row['atlasID'])) print "<a href='#' title=\"header=[Atlas - ".$row['atlasID']."] body=[<img src='http://www.gr.soho-dev.local.soho/art/images/atlas/thumbs/".$row['atlasID'].".jpg' alt='' />]\">".$row['atlasID2']."</a>";
	    else print "&nbsp;";
	    print "</td>\n";
	    }

	    //CATEGORY
	    if($_GET['show_category']==true)
	    {
	    $result2=$db->query("select * from ".TABLE_CATEGORY." where catID='".$row['catID']."'");
	    $row2=$db->mysql_array($result2);
	    print "\t<td>".$row2['categoryEN']."</td>\n";
	    }
	    
	    if($_GET['show_artwork']==true)
	    {
	    $result2=$db->query("select * from ".TABLE_ARTWORKS." where artworkID='".$row['artworkID']."'");
	    $row2=$db->mysql_array($result2);
	    print "\t<td>".$row2['artworkEN']."</td>\n";
	    }

	    //HEIGHT & WIDTH (size)
	    if($_GET['show_size']==true)
	    {
	    print "\t<td><p>".$row['height2']."</p><p>".$row['width2'].$row['size']."</p></td>\n";
	    }



	    //MEDIA
	    if($_GET['show_media']==true)
	    {
	    $result3=$db->query("select * from ".TABLE_MEDIA." where mID='".$row['mID']."'");
	    $row3=$db->mysql_array($result3);
	    print "\t<td>".$row3['mediumEN']."</td>\n";
	    }

	    //SALE DATE
	    if($_GET['show_saleDate']==true)
	    {
	    print "\t<td>".$row['saleDate']."</td>\n";
	    }

	    //SALE NAME
	    if($_GET['show_saleName']==true)
	    {
	    print "\t<td>".$row['saleName2']."</td>\n";
	    }

	    //AUCTIONHOUSE
	    if($_GET['show_auctionHouse']==true)
	    {
	    $result4=$db->query("select * from ".TABLE_AUCTIONHOUSE." where ahID='".$row['ahID']."'");
	    $row4=$db->mysql_array($result4);
	    print "\t<td>".$row4['house']."</td>\n";
	    }

	    //Lot No
	    if($_GET['show_lotNo']==true)
	    {
	    print "\t<td>".$row['lotNo']."</td>\n";
	    }

	    //ESTIMATE
	    if($_GET['show_estimate']==true)
	    {
	    print "\t<td>".UTILS::get_currency($db,$row['estCurrID'])."&nbsp;".number_format($row['estLow'],0)."<br />";
	    print UTILS::get_currency($db,$row['estCurrID'])."&nbsp;".number_format($row['estHigh'],0)."</td>\n";
	    }

	    //ESTIMATE
	    if($_GET['show_saleType']==true)
	    {
	    print "\t<td>".$row['saleType']."</td>\n";
	    }


	    //SOLD FOR
	    if($_GET['show_soldFor']==false)
	    {
	    	print "\t<td>";
			if( $row['boughtIn'] ) print "Bought In";
			else
			{
				if( !empty($row['soldForCurrID']) && !empty($row['soldFor']) )
				{
		    	print UTILS::get_currency($db,number_format($row['soldForCurrID'],0))."&nbsp;".number_format($row['soldFor'],0)."<br />";
		    	}
		    	if( !empty($row['soldForUSD']) )
				{
		    	print UTILS::get_currency($db,1)."&nbsp;".number_format($row['soldForUSD'],0);
		    	}
	    	}
	    	print "\t</td>\n";
	    }

	    print "</tr>";
	    #end
	    }
	    
	    
#PAGE NUMBERING
	  if($how_much_pages>1)
	  {
	  if($_GET['page_selected']!=1) $link=$url."&page_selected=".($_GET['page_selected']-1);
	  else $link="#";
	  print "<tr><td style='text-align: center' colspan='20'><a href='".$link."'>&#060;</a>";
	  	for($i=1;$i<=$how_much_pages;$i++)
	  	{
	  	if($_GET['page_selected']==$i) $selected=" style='font-weight:bold;'";
	  	else $selected="";
	  	print " <a href='".$url."&page_selected=$i' $selected>".$i."</a> ";
	  	}
	  if($_GET['page_selected']!=$how_much_pages) $link=$url."&page_selected=".($_GET['page_selected']+1);
	  else $link="#";
	  print "<a href='".$link."'>&#062;</a></td></tr>";
	  }

#end
	    
	print "</table>\n";






print "\t</div>\n";


$html->foot();
?>
