<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/tooltip.js";
$html->head('admin');

$db=new dbCLASS;


$utils=new UTILS;

for($i=0;$i<sizeof($_GET['year']);$i++) $years.="&year[]=".$_GET['year'][$i];


$url="/admin/search/paintings.results.php?".$_SERVER["QUERY_STRING"];






function display_search_keys($found,$search)
{
$new_look = str_replace($search, "<font style='font-weight:bold'>".$search."</font>", $found);
$new_look = str_replace(ucwords($search), "<font style='font-weight:bold'>".ucwords($search)."</font>", $found);
return $new_look;
}

print "<a href='/admin' title=''><u>Main Menu</u></a>&nbsp";
print "<a href='/admin/search/paintings.php' title=''><u>Search</u></a><br /><br />";

#this is to keep searched values if user goes back to form
unset($_SESSION['search']);

$_SESSION['search']['url']=$url;

$_SESSION['search']['show_artwork']=$_GET['show_artwork'];
$_SESSION['search']['show_number']=$_GET['show_number'];
$_SESSION['search']['show_image']=$_GET['show_image'];
$_SESSION['search']['show_title']=$_GET['show_title'];
$_SESSION['search']['show_ednumber']=$_GET['show_ednumber'];
$_SESSION['search']['show_year']=$_GET['show_year'];
$_SESSION['search']['show_category']=$_GET['show_category'];
$_SESSION['search']['show_size']=$_GET['show_size'];
$_SESSION['search']['show_media']=$_GET['show_media'];
$_SESSION['search']['show_museum']=$_GET['show_museum'];
$_SESSION['search']['artworkID']=$_GET['artworkID'];
$_SESSION['search']['title']=$_GET['title'];
for($i=0;$i<sizeof($_GET['year']);$i++){$_SESSION['search']['year'][]=$_GET['year'][$i];}
$_SESSION['search']['number']=$_GET['number'];
$_SESSION['search']['catID']=$_GET['catID'];
$_SESSION['search']['height']=$_GET['height'];
$_SESSION['search']['width']=$_GET['width'];
$_SESSION['search']['mID']=$_GET['mID'];
$_SESSION['search']['muID']=$_GET['muID'];
#end



$search_array=UTILS::search_array($_GET['number']);

if( sizeof($search_array)<=1 ) $numer_search.=" number = '".$search_array[0]."' ";
else
{
    $i=0;
    foreach( $search_array as $value )
    {
        $i++;
        $numer_search.=" number = '".$value."' ";
        if( $i!=sizeof($search_array) ) $numer_search.=" OR ";
    }
}

	

#Building query

#this sets wheather to select one artwork type or all of them
if(empty($_GET['artwork'])) {$artwork_from=1;$artwork_till=15;}
else {$artwork_from=$_GET['artwork'];$artwork_till=$_GET['artwork'];}
#end




	$query="SELECT * FROM ".TABLE_PAINTING." WHERE ";
	
	//artworkID
	if($_GET['artworkID']=="paintings") $query.=" IF('".strlen($_GET['artworkID'])."'>0,artworkID='1' or artworkID='2' or artworkID = 12 or artworkID = 13 or artworkID = 14 ,artworkID LIKE '%') and ";
	else  $query.=" IF('".strlen($_GET['artworkID'])."'>0,artworkID='".$_GET['artworkID']."',artworkID LIKE '%') and ";
	//end
	
	
	
    //title
    if( strlen($_GET['title'])>0 ) $query.=" MATCH(titleEN,titleDE) AGAINST ('".UTILS::prepear_search($_GET['title'])."' IN BOOLEAN MODE) and ";
	//end
	
	
	
	
	//number
	//if(sizeof($number)!=0)
	//{
	$query.=" IF('".strlen($_GET['number'])."'>0,$numer_search, number LIKE '%') and ";
    //}
	//endfgf
	
	
	
	
	//year
	if(sizeof($_GET['year'])!=0)
	{
		$query.=" IF('".sizeof($_GET['year'])."'>0, ";
		for($i=0;$i<sizeof($_GET['year']);$i++)
		{
		if($i!=0) $or=" OR ";
		else $or="";
		$query.=" $or year='".$_GET['year'][$i]."' ";
		}
		$query.=" ,year>'1800' OR year=0 OR year IS NULL) and ";
	}
	//end
	
	if( strlen($_GET['catID'])>0 ) $query.=" catID = '".$_GET['catID']."' and ";
	
	$query.=" 
	IF('".strlen($_GET['height']).">0', height='".$_GET['height']."', height=0 OR height IS NULL OR height>0) and
	IF('".strlen($_GET['width']).">0', width='".$_GET['width']."', width=0 OR width IS NULL OR width>0)  ";
    
    if( strlen($_GET['muID'])>0 ) $query.=" AND muID = '".$_GET['muID']."' ";
        
    if( strlen($_GET['mID'])>0 ) $query.=" AND mID = '".$_GET['mID']."' ";



	if($_GET['sort_by']=="number")$_GET['sort_by']=$_GET['sort_by']."+0";
	if( !empty($_GET['sort_by2']) ) $sortby2=",".$_GET['sort_by2'];
 	$query.=" ORDER BY ".$_GET['sort_by'].$sortby2." ".$_GET['sort'];
	
	//print $query;
	
	$result_total=$db->query($query);
	
	if( empty($_GET['show_per_page']) ) $_GET['show_per_page']=64;
	if($_GET['page_selected']==1) $page_selected=($_GET['page_selected']-1);
	else $page_selected=($_GET['page_selected']-1)*$_GET['show_per_page'];

    $_SESSION['search']['query']=$query;

	if( $_GET['show_per_page']!="all" )$query.=" LIMIT ".$page_selected.",".$_GET['show_per_page'];
	//print $query;


	$result=$db->query($query);


#end building query








    print "<table class='admin-table'>\n";


#PAGE NUMBERING
	  
	  
	  $count=$db->numrows($result_total);
	  $how_much_pages=@ceil($count/$_GET['show_per_page']);

	  
	  
	  if($how_much_pages>1)
	  {
	  print "<tr><td style='text-align: center' colspan='20'>";
	  $html->page_numbering($how_much_pages,$_GET['page_selected'],$_GET['show_per_page'],$url,$count," class='pages-form top' ");
	  print "</td></tr>";
	  }

#end
	


#SORT LINKS

	  print "<tr>";


	//artwork
	    if($_GET['show_artwork']==true)
	    {
	      if($_GET['sort_by']=="artworkID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="artworkID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=artworkID' title='Order by Artwork $sort_text'><u>Artwork</u> $sort_img</a></th>";
	    }

	//NUMBER
	    if($_GET['show_number']==true)
	    {
	      if($_GET['sort_by']=="number" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="number" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=number' title='Order by Number $sort_text'><u>Number</u> $sort_img</a></th>";
	    }




	//IMAGE
	    if($_GET['show_image']==true) print "<th>Image</th>";



	//TITLE
	    if($_GET['show_title']==true)
	    {
	      if($_GET['sort_by']=="titleEN" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="titleEN" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=titleEN' title='Order by Title $sort_text'><u>Title</u> $sort_img</a></th>";
	    }

	//ednumber
	    if($_GET['show_ednumber']==true)
	    {
	      if($_GET['sort_by']=="ednumber" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="ednumber" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=ednumber' title='Order by ednumber $sort_text'><u>EDnumber</u> $sort_img</a></th>";
	    }




	//YEAR
	    if($_GET['show_year']==true)
	    {
	      if($_GET['sort_by']=="year" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="year" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=year' title='Order by Year $sort_text'><u>Year</u> $sort_img</a></th>";
	    }


	//CATEGORY
	    if($_GET['show_category']==true)
	    {
	      if($_GET['sort_by']=="catID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="catID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=catID' title='Order by catID $sort_text'><u>Category</u> $sort_img</a></th>";
	    }



	//SIZE
	    if($_GET['show_size']==true) print "<th>Size</th>";



	//MEDIUM
	    if($_GET['show_media']==true)
	    {
	      if($_GET['sort_by']=="mID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="mID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=mID' title='Order by mID $sort_text'><u>Medium</u> $sort_img</a></th>";
	    }
        
//MUSEUM

        if($_GET['show_museum']==true)
	    {
	      if($_GET['sort_by']=="muID" && $_GET['sort']=="ASC") {$sort="DESC";$sort_text="Descending";$sort_img="<img src='/g/up.gif' alt='' />";}
	      elseif($_GET['sort_by']=="muID" && $_GET['sort']=="DESC"){ $sort="ASC";$sort_text="Ascending";$sort_img="<img src='/g/down.gif' alt='' />";}
	      else { $sort="ASC";$sort_text="Ascending";$sort_img="";}
	    print "<th><a href='?".$_SERVER['QUERY_STRING']."&sort=$sort&sort_by=muID' title='Order by muID $sort_text'><u>Museum</u> $sort_img</a></th>";
	    }



	  print "</tr>";


#end - sort links


        if( $count==0 )
        {
            print "<tr><td colspan=4>".LANG_SEARCH_NORESULTS."</td></tr>";
        }
        else
        {

        $tmp=array();
	    while($row=$db->mysql_array($result))
	    {
		$artwork_image_dir=$utils->get_artwork_image_dir($db,$row['artworkID']);
		$artwork_dir_admin=$utils->get_artwork_dir_admin($db,$row['artworkID']);


	    #changing text with bold text to indicate found places
	    $row['titleEN2'] = display_search_keys($row['titleEN'],$_GET['title']);
	    $row['titleDE2'] = display_search_keys($row['titleDE'],$_GET['title']);
	    $row['year2'] = display_search_keys($row['year'],$_GET['year']);
	    $row['atlasID2'] = display_search_keys($row['atlasID'],$_GET['atlas']);
	    $row['height2'] = display_search_keys($row['height'],$_GET['height']);
	    $row['width2'] = display_search_keys($row['width'],$_GET['width']);
	    #end



	    #building data table
	      
	      #adding odd and evan row
	      if(empty($style) && @!array_search($row['paintID'],$tmp)) $style=" style='background:#A2A2A2'";
	      elseif(!empty($style) && @!array_search($row['paintID'],$tmp)) $style="";
	      //else $style="";
	      #end
	    
	    print "<tr $style>";


		if(@!array_search($row['paintID'],$tmp))
		{
		
		
				  
		  	    if($_GET['show_artwork']==true)
	    {
	    $result2=$db->query("select * from ".TABLE_ARTWORKS." where artworkID='".$row['artworkID']."'");
	    $row2=$db->mysql_array($result2);
	    print "\t<td>".$row2['artworkEN']."</td>\n";
	    }
		  
		
		
		  //IMAGE
	    	  if($_GET['show_number']==true)
	    	  {
		  print "\t<td>".$row['number']."</td>\n";
		  }
		  


		  //IMAGE
	    	  if($_GET['show_image']==true)
	    	  {
		  print "\t<td>";
		    

		    
		    
		    #If image doesn't exist showing image not available



  $src="/includes/retrieve.image.php?paintID=".$row['paintID']."&size=m";



		    print "<a href='/admin/".$artwork_dir_admin."/?".$row['paintID']."'><img src='".$src."' alt='".$row['titleEN']."' /></a>";
		    #end
		    
		  $tmp[]=$row['paintID'];
		  $paintid=$row['paintID'];
		  print "<ul class='painting-details-brief'>";
		  $result2=$db->query("SELECT * FROM ".TABLE_SALEHISTORY." WHERE paintID='".$row['paintID']."'");
		  $count2=$db->numrows($result2);
		  $row2=$db->mysql_array($result2);
		  if($count2>0) print "<li><a title='Edit Sales History' href='/admin/saleHistory/?".$row['paintID']."' class='info-sales'>Edit Sales History</a></li>";
		  print "<li><a title='Edit Painting Info' href='/admin/".$artwork_dir_admin."/?".$paintid."' class='info-museum'>Edit Painting Info</a></li>";
		  print "<li><a title='Edit Sales History' href='/admin/saleHistory/?".$row['paintID']."' >Edit Sales History</a></li>";
		  print "</ul>";
		  print "</td>\n";
		  }
		}
		else
		{
		print "\t<td colspan='2' >"; 
		  print "<ul class='painting-details-brief'>";
		  print "<li><a title='Edit Sales History' href='/admin/".$artwork_dir_admin."/?".$row['saleID']."' class='info-sales'>Edit Sales History</a></li>";
		  print "</ul>";
		print "</td>\n";
		}




	    //TITLE
	    if($_GET['show_title']==true)
	    {
	    	print "\t<td><p>".$row['titleEN2']."</p><p>".$row['titleDE2']."</p></td>\n";
	    }

	    //YEAR
	    if($_GET['show_ednumber']==true)
	    {
	    print "\t<td>".$row['ednumber']."</td>\n";
	    }



	    //YEAR
	    if($_GET['show_year']==true)
	    {
	    print "\t<td>".$row['year2']."</td>\n";
	    }

	    //ATLAS
	    if($_GET['show_atlas']==true)
	    {
	    print "\t<td>";
	    if(!empty($row['atlasID'])) print "<a href='#' title=\"header=[Atlas - ".$row['atlasID']."] body=[<img src='http://www.gr.soho-dev.local.soho/art/images/atlas/thumbs/".$row['atlasID'].".jpg' alt='' />]\">".$row['atlasID2']."</a>";
	    else print "&nbsp;";
	    print "</td>\n";
	    }

	    //CATEGORY
	    if($_GET['show_category']==true)
	    {
	    $result2=$db->query("select * from ".TABLE_CATEGORY." where catID='".$row['catID']."'");
	    $row2=$db->mysql_array($result2);
	    print "\t<td>".$row2['categoryEN']."</td>\n";
	    }
	    


	    //HEIGHT & WIDTH (size)
	    if($_GET['show_size'] && $row['artworkID']!=3 )
	    {
	    print "\t<td><p>".$row['height']." cm X ".$row['width']." cm</p></td>\n";
	    }
	    else if($_GET['show_size'] && $row['artworkID']==3 )
	    {
	    print "\t<td><p>".$row['iheight']." cm X ".$row['iwidth']." cm</p></td>\n";
	    }



	    //MEDIA
	    if($_GET['show_media']==true)
	    {
	    $result3=$db->query("select * from ".TABLE_MEDIA." where mID='".$row['mID']."'");
	    $row3=$db->mysql_array($result3);
	    print "\t<td>".$row3['mediumEN']."</td>\n";
	    }

        //MUSEUM
	    if($_GET['show_museum']==true)
	    {
	    $result3=$db->query("select * from ".TABLE_MUSEUM." where muID='".$row['muID']."'");
	    $row3=$db->mysql_array($result3);
	    print "\t<td><a href='/admin/museum/edit/?".$row3['muID']."' title=''>".$row3['museum']."</a></td>\n";
	    }



	    print "</tr>";
	    #end
	    }
	    
	    
#PAGE NUMBERING
	  if($how_much_pages>1)
	  {
	  print "<tr><td style='text-align: center' colspan='20'>";
	  $html->page_numbering($how_much_pages,$_GET['page_selected'],$_GET['show_per_page'],$url,$count," class='pages-form bottom' ");
	  print "</td></tr>";
	  }

#end
}//if $count>0 
	print "</table>\n";








$html->foot();

?>
