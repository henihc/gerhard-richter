<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->js[]="/js/tooltip.js";
$html->js[]="/js/common.js";
$html->head('admin');

$db=new dbCLASS;


$url_var=UTILS::url_var($db,$_SERVER["QUERY_STRING"]);

if($url_var[0]=='reset')
{
    unset($_SESSION['search']);
    UTILS::redirect("/admin/search/paintings.php");
}

print "\t<div id='div_admin' >\n";

?>

<a href='/admin' title=''><u>Main Menu</u></a><br /><br />


<h1>Search Database</h1>



<table class="admin-table">

<?php
if($_GET['task']=="delete") print "<tr><td colspan='2' style='color:green'>Delete successful!</td></tr>";
if($_GET['task']=="error") print "<tr><td colspan='2' style='color:red'>Nothing to delete!</td></tr>";
?>


  <tr>
    <td colspan="15"><font style='font-weight:bold;font-size:17px;'>Display</font></td>
  </tr>
  <tr>
  
<form method="get" action="paintings.results.php">


    

    <td>Artwork<br /><input type="checkbox"  checked='checked' name="show_artwork" value="1" /></td> 
   
    <td>Number<br /><input type="checkbox"  checked='checked' name="show_number" value="1" /></td> 
    
    <td>Image<br /><input type="checkbox" checked='checked' name="show_image" value="1" /></td> 

    <td>Title<br /><input type="checkbox" checked='checked' name="show_title" value="1" /></td> 

<?php if($_SESSION['search']['show_ednumber']==1){$checked=" checked='checked' ";}else $checked=""; ?>
    <td>EDnumber<br /><input type="checkbox" <?=$checked?> name="show_ednumber" value="1" /></td> 
<?php if($_SESSION['search']['show_year']==1){$checked=" checked='checked' ";}else $checked=""; ?>
    <td>Year<br /><input type="checkbox" <?=$checked?> name="show_year" value="1" /></td> 
<?php if($_SESSION['search']['show_category']==1){$checked=" checked='checked' ";}else $checked=""; ?>
    <td>Category<br /><input type="checkbox" <?=$checked?> name="show_category" value="1" /></td>
<?php if($_SESSION['search']['show_size']==1){$checked=" checked='checked' ";}else $checked=""; ?>
    <td>Size<br /><input type="checkbox" <?=$checked?> name="show_size" value="1" /></td>
<?php if($_SESSION['search']['show_media']==1){$checked=" checked='checked' ";}else $checked=""; ?>
    <td>Medium<br /><input type="checkbox" <?=$checked?> name="show_media" value="1" /></td>
<?php if($_SESSION['search']['show_museum']==1){$checked=" checked='checked' ";}else $checked=""; ?>
<td>Museum<br /><input type="checkbox" <?=$checked?> name="show_museum" value="1" /></td>
  </tr>
</table>



<table class="admin-table">



<tr>
  <td colspan="2"><font style='font-weight:bold;font-size:17px;'>Artwork</font></td>
</tr>



<tr>
  <td>Artwork</td>
  <td>
  <?php
  	$query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID!=10 and artworkID!=11  ORDER BY artworkEN ASC");
	echo "	<select name='artworkID' id='artworkID' >\n";
		if($_SESSION['search']['artworkID']=="paintings") $selected=" selected='selected' ";
		else $selected="";
		print "<option $selected value=''>Select...</option>\n";
		
		print "<option $selected value='paintings'>Paintings</option>\n";
		while($row=$db->mysql_array($query))
		{
			if($row['artworkID']==$_SESSION['search']['artworkID'])$selected="selected='selected'";
			else $selected="";
			echo "<option $selected value='".$row['artworkID']."'>".$row['artworkEN'];
		}
	echo "</select>\n";
	?>
  </td>
</tr>



<tr>
  <td>Title</td>
  <td>
  <input type="text" name="title" value="<?=$_SESSION['search']['title']?>" />
  </td>
</tr>



<tr>
  <td>Year</td>
  <td>
    <select name="year[]" id="year" size="5" multiple="multiple" style="width:100px;">
      <option value="">Select..</option>
      <?php
      for($i=1962;$i<=date("Y");$i++)
      {
      	if(in_array($i, $_SESSION['search']['year'])) $selected="selected='selected'";
      	else $selected="";
      print "\t<option $selected value='$i'>$i</option>\n";
      }
      ?>
    </select>
  </td>
</tr>



<tr>
  <td>Number <a href="#" title='header=[Search help] body=[CR or <br />some other kind of Indexing number]' >?</a> </td>
  <td>
  <input type="text" name="number" value="<?=$_SESSION['search']['number']?>" />
  </td>
</tr>





<tr>
  <td>Category</td>
  <td>
    <select name="catID">
      <option value="">Select..</option>
      <?php
      $query=$db->query("SELECT * from ".TABLE_CATEGORY." ORDER BY categoryEN ASC");
      while($row=$db->mysql_array($query))
      {
      //$query2=$db->query("SELECT catID from ".TABLE_CATEGORY." where categoryEN='".$row['categoryEN']."' ORDER BY categoryEN ASC");
      //$row2=$db->mysql_array($query2);
      if( $_SESSION['search']['catID']==$row['catID'] && !empty($_SESSION['search']['catID']) ) $selected="selected='selected'";
      else $selected="";
      print "\t<option $selected value='".$row['catID']."'>".$row['categoryEN']."</option>\n";
      }
      ?>
    </select>
  </td>
</tr>



<tr>
  <td>Height x Width</td>
  <td>
  <input type="text" name="height" value="<?=$_SESSION['search']['height']?>" /> - <input type="text" name="width" value="<?=$_SESSION['search']['width']?>" />
  </td>
</tr>



<tr>
  <td>Medium</td>
  <td>
    <select name="mID">
      <option value="">Select..</option>
      <?php
      $query=$db->query("SELECT * from ".TABLE_MEDIA." ORDER BY mediumEN ASC");
      while($row=$db->mysql_array($query))
      {
      if($_SESSION['search']['mID']==$row['mID']) $selected="selected='selected'";
      else $selected="";
      print "\t<option $selected value='".$row['mID']."'>".$row['mediumEN']."</option>\n";
      }
      ?>
    </select>
  </td>
</tr>

<tr>
  <td>Museum</td>
  <td>
    <select name="muID">
      <option value="">Select..</option>
      <?php
      $query=$db->query("SELECT * from ".TABLE_MUSEUM." ORDER BY museum ASC");
      while($row=$db->mysql_array($query))
      {
      if($_SESSION['search']['muID']==$row['muID']) $selected="selected='selected'";
      else $selected="";
      print "\t<option $selected value='".$row['muID']."'>".$row['museum']."</option>\n";
      }
      ?>
    </select>
  </td>
</tr>




<tr>
  <td>
  <a href="?reset" title="Reset Form data" >reset</a>
  </td>
  <td>
  <input type="hidden" name="page_selected" value="1" />
  <input type="hidden" name="sort" value="ASC" />
  <input type="hidden" name="sort_by" value="sort" />
  <input type="submit" name="search" value="Search" />
  </td>
</tr>



</table>
</form>
<?
print "\t</div>\n";


$html->foot();
?>
