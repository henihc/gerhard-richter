<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");

admin_html::admin_sign_in();

$html = new html_elements;
$html->title="Gerhard Richter - Admin";
$html->css[]="/css/admin.css";
$html->css[]="/css/calendar.css";
$html->js[]="/js/calendar/mootools-1.3.1.js";
$html->js[]="/js/calendar/mootools-more.js";
$html->js[]="/js/calendar/Picker.js";
$html->js[]="/js/calendar/Picker.Attach.js";
$html->js[]="/js/calendar/Picker.Date.js";
$html->js[]="/js/common.js";
$html->head('admin');

$db=new dbCLASS;


if( $_GET['task']=="reset" )
{
    unset($_SESSION['search_admin']);
    UTILS::redirect("/admin/search/");
}

print "\t<script type='text/javascript'>\n";
    print "\twindow.addEvent('domready', function() {";
        print "new Picker.Date('saleDate_from', { pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
        print "new Picker.Date('saleDate_to', { pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
        print "new Picker.Date('date_bought_for_from', { pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
        print "new Picker.Date('date_bought_for_to', { pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
    print "\t});\n";
print "\t</script>\n";

print "\t<div id='div_admin'>\n";

    # menu admin
    print "\t<a href='/admin' title=''><u>main menu</u></a>&nbsp;&nbsp;\n";
    print "\t<a href='/admin/search' title='Search'><u>search</u></a><br /><br />\n";
    #end menu admin

    print "\t<h1>Search paintings</h1>\n";

    print "\t<form action='search.php' method='get'>\n";

        print "\t<table class='admin-table'>\n";
            print "\t<tr>\n";
                print "\t<th colspan='15' style='text-align:center;'>Fields to display</th>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                $display_array[0]['name']="paintID";$display_array[0]['title']="paintid";$display_array[0]['disabled']="disabled='disabled'";$display_array[0]['checked']="checked='checked'";
                $display_array[1]['name']="number";$display_array[1]['title']="Number";$display_array[1]['disabled']="disabled='disabled'";$display_array[1]['checked']="checked='checked'";
                $display_array[2]['name']="image";$display_array[2]['title']="Image";$display_array[2]['disabled']="disabled='disabled'";$display_array[2]['checked']="checked='checked'";
                $display_array[3]['name']="titleEN";$display_array[3]['title']="TitleEN";$display_array[3]['disabled']="disabled='disabled'";$display_array[3]['checked']="checked='checked'";
                $display_array[4]['name']="titleDE";$display_array[4]['title']="TitleDE";$display_array[4]['disabled']="disabled='disabled'";$display_array[4]['checked']="checked='checked'";
                $display_array[5]['name']="titleCH";$display_array[5]['title']="TitleCN";$display_array[5]['disabled']="disabled='disabled'";$display_array[5]['checked']="checked='checked'";
                $display_array[6]['name']="year";$display_array[6]['title']="Year";$display_array[6]['disabled']="disabled='disabled'";$display_array[6]['checked']="checked='checked'";
                $display_array[7]['name']="category";$display_array[7]['title']="Category";
                $display_array[8]['name']="artwork";$display_array[8]['title']="Artwork";
                $display_array[9]['name']="size";$display_array[9]['title']="Size";
                $display_array[10]['name']="media";$display_array[10]['title']="Medium";
                /*
                $display_array[8]['name']="saleDate";$display_array[8]['title']="Sale date";
                $display_array[9]['name']="saleName";$display_array[9]['title']="Sale name";
                $display_array[10]['name']="auctionHouse";$display_array[10]['title']="Auction house";
                $display_array[11]['name']="lotNo";$display_array[11]['title']="Lot No";
                $display_array[12]['name']="estimate";$display_array[12]['title']="Estimate L/H";
                $display_array[13]['name']="soldFor";$display_array[13]['title']="Sold For";$display_array[13]['disabled']="disabled='disabled'";$display_array[13]['checked']="checked='checked'";
                 */
                foreach( $display_array as $key => $value )
                {
                    if( $_SESSION['search_admin']['show_'.$value['name']]==1 )
                    {
                        $checked=" checked='checked' ";
                    }
                    else 
                    {
                        $checked=$value['checked'];
                    }
                    print "\t<td>\n";
                        print $value['title'];
                        print "<br /><input type='checkbox' ".$value['disabled']." ".$checked." name='show_".$value['name']."' value='1' />\n";
                    print "\t</td>\n";
                }
            print "\t</tr>\n";
        print "\t</table>\n";


        print "\t<table class='admin-table'>\n";
            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Artwork info<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Artwork</td>\n";
                print "\t<td>\n";
                    admin_html::select_artworks($db,"artworkID",$_SESSION['search_admin']['artworkID'],$onchange,$class=array(),$disable,$where=" WHERE artworkID!=10 and artworkID!=11 ");
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Title</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='title' value='".$_SESSION['search_admin']['title']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Year</td>\n";
                print "\t<td>\n";
                    admin_html::get_years($id,$_SESSION['search_admin']['year'],"class='select_admin_years_multi'"," multiple='multiple' ");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Number</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='number' value='".$_SESSION['search_admin']['number']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Category</td>\n";
                print "\t<td>\n";
                    $class['select_admin_cat_multi']="select_admin_cat_multi";
                    admin_html::select_categories_admin($db,"catID",$_SESSION['search_admin']['catID'],$onchange,$class,$disable,$where="",$multiple=" multiple='multiple' ");
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Height</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='height_from' value='".$_SESSION['search_admin']['height_from']."' class='input_field_admin_small' />\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "\t<input type='text' name='height_to' value='".$_SESSION['search_admin']['height_to']."' class='input_field_admin_small' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Width</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='width_from' value='".$_SESSION['search_admin']['width_from']."' class='input_field_admin_small' />\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "\t<input type='text' name='width_to' value='".$_SESSION['search_admin']['width_to']."' class='input_field_admin_small' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Medium</td>\n";
                print "\t<td>\n";
                    $class['select_admin_medium_multi']="select_admin_medium_multi";
                    admin_html::select_medium($db,"mID",$_SESSION['search_admin']['mID'],$onchange,$class,$disable,$where="",$multiple="");
                print "\t</td>\n";
            print "\t</tr>\n";

            #**********     SALE HISTORY    **************#

            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Sales history<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Sales Date</td>\n";
                print "\t<td>\n";
                    print "\t<span><input type='text' name='saleDate_from' id='saleDate_from' class='dashboard' value='".$_SESSION['search_admin']['saleDate_from']."' /></span>\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "\t<input type='text' name='saleDate_to' id='saleDate_to' class='dashboard' value='".$_SESSION['search_admin']['saleDate_to']."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Auction House</td>\n";
                print "\t<td>\n";
                    $class['select_admin_medium_multi']="select_admin_medium_multi";
                    admin_html::select_auction_houses($db,"ahID",$_SESSION['search_admin']['ahID'],$onchange,$class,$disable,$multiple="multiple='multiple'");
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Sale Name</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='sale_name' value='".$_SESSION['search_admin']['sale_name']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Lot No</td>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='lotNo' value='".$_SESSION['search_admin']['lotNo']."' class='' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Estimate currency 1</td>\n";
                if( !empty($_SESSION['search_admin']['']) ) $estLow=$_SESSION['search_admin']['estLow'];
                else $estLow="";
                if( !empty($_SESSION['search_admin']['']) ) $estHigh=$_SESSION['search_admin']['estHigh'];
                else $estHigh="";
                print "\t<td>";
                    if( !empty($_SESSION['search_admin']['']) ) $estCurrID=$_SESSION['search_admin']['estCurrID'];
                    else $estCurrID="";
                    admin_html::select_currencies($db,"estCurrID",$estCurrID,$onchange,$class=array(),$disable);
                    print "<input type='text' name='estLow' id='estLow' value='".$estLow."' class='input_field_admin_medium' />";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "<input type='text' name='estHigh' id='estHigh' value='".$estHigh."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Estimate currency 2</td>\n";
                if( !empty($_SESSION['search_admin']['']) ) $estLowUSD=$_SESSION['search_admin']['estLowUSD'];
                else $estLowUSD="";
                if( !empty($_SESSION['search_admin']['']) ) $estHighUSD=$_SESSION['search_admin']['estHighUSD'];
                else $estHighUSD="";
                print "\t<td>";
                    admin_html::select_currencies($db,"estCurrID",1,$onchange,$class=array(),"disabled='disabled'");
                    print "<input type='text' name='estLowUSD' id='estLowUSD' value='".$estLowUSD."' class='input_field_admin_medium' />";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "<input type='text' name='estHighUSD' id='estHighUSD' value='".$estHighUSD."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Sold for currency 1</td>\n";
                if( !empty($_SESSION['search_admin']['soldfor_from']) ) $soldfor_from=$_SESSION['search_admin']['soldfor_from'];
                else $soldfor_from="";
                if( !empty($_SESSION['search_admin']['soldfor_to']) ) $soldfor_to=$_SESSION['search_admin']['soldfor_to'];
                else $soldfor_to="";
                if( !empty($_SESSION['search_admin']['soldForCurrID']) ) $soldForCurrID=$_SESSION['search_admin']['soldForCurrID'];
                else $soldForCurrID="";
                print "\t<td>";
                    admin_html::select_currencies($db,"soldForCurrID",$soldForCurrID,$onchange,$class=array(),"");
                    print "<input type='text' name='soldFor_from' id='soldFor_from' value='".$soldFor_from."' class='input_field_admin_medium' />";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "<input type='text' name='soldFor_to' id='soldFor_to' value='".$soldFor_to."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>Sold for currency 2</td>\n";
                if( !empty($_SESSION['search_admin']['soldForUSD_from']) ) $soldForUSD_from=$_SESSION['search_admin']['soldForUSD_from'];
                else $soldForUSD_from="";
                if( !empty($_SESSION['search_admin']['soldForUSD_to']) ) $soldForUSD_to=$_SESSION['search_admin']['soldForUSD_to'];
                else $soldForUSD_to="";
                print "\t<td>";
                    admin_html::select_currencies($db,"estCurrID",1,$onchange,$class=array(),"disabled='disabled'");
                    print "<input type='text' name='soldForUSD_from' id='soldForUSD_from' value='".$soldForUSD_from."' class='input_field_admin_medium' />";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "<input type='text' name='soldForUSD_to' id='soldForUSD_to' value='".$soldForUSD_to."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Bought In</td>\n";
                print "\t<td>\n";
                    if( $_SESSION['search_admin']['bought_in'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' ".$checked." name='bought_in' id='bought_in' value='1' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";




            #**********     PRIVATE DATA    **************#

            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Private data<br /><br /></th>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<td>Owner</td>\n";
                if( !empty($_SESSION['search_admin']['owner']) ) $owner=$_SESSION['search_admin']['owner'];
                else $owner="";
                print "\t<td>";
                    print "<input type='text' name='owner' value='".$owner."' class='admin-input' />\n";
                    //if( !empty($_SESSION['search_admin']['owner_truth']) ) $owner_truth=$_SESSION['search_admin']['owner_truth'];
                    //else $owner_truth="";
                    //$html->select_owner_truth($db,"owner_truth",$owner_truth,$onchange,$class=array(),$disable);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Bought for</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['bought_for_currencyid']) ) $bought_for_currencyid=$_SESSION['search_admin']['bought_for_currencyid'];
                    else $bought_for_currencyid="";
                    admin_html::select_currencies($db,"bought_for_currencyid",$bought_for_currencyid,$onchange,$class=array(),$disable);
                    if( !empty($_SESSION['search_admin']['bought_for_price']) ) $bought_for_price=$_SESSION['search_admin']['bought_for_price'];
                    else $bought_for_price="";
                    print "\t<input type='text' name='bought_for_price' value='".$bought_for_price."' />\n";
                    //if( !empty($_SESSION['search_admin']['bought_for_truth']) ) $bought_for_truth=$_SESSION['search_admin']['bought_for_truth'];
                    //else $bought_for_truth="";
                    //$html->select_owner_truth($db,"bought_for_truth",$bought_for_truth,$onchange,$class=array(),$disable);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Date bought for</td>\n";
                print "\t<td>";
                    if( !empty($_SESSION['search_admin']['date_bought_for_from']) ) $date_bought_for_from=$_SESSION['search_admin']['date_bought_for_from'];
                    else $date_bought_for_from="";
                    print "<span><input type='text' name='date_bought_for_from' id='date_bought_for_from' class='dashboard' value='".$date_bought_for_from."' /></span>\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    if( !empty($_SESSION['search_admin']['date_bought_for_to']) ) $date_bought_for_to=$_SESSION['search_admin']['date_bought_for_to'];
                    else $date_bought_for_to="";
                    print "\t<input type='text' name='date_bought_for_to' id='date_bought_for_to' class='dashboard' value='".$date_bought_for_to."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Source</td>\n";
                if( !empty($_SESSION['search_admin']['bought_for_source']) ) $bought_for_source=$_SESSION['search_admin']['bought_for_source'];
                else $bought_for_source="";
                print "\t<td><input type='text' name='bought_for_source' value='".$bought_for_source."' class='admin-input' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Asking price</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['asking_currencyid']) ) $asking_currencyid=$_SESSION['search_admin']['asking_currencyid'];
                    else $asking_currencyid="";
                    admin_html::select_currencies($db,"asking_currencyid",$asking_currencyid,$onchange,$class=array(),$disable);
                    if( !empty($_SESSION['search_admin']['asking_price']) ) $asking_price=$_SESSION['search_admin']['asking_price'];
                    else $asking_price="";
                    print "\t<input type='text' name='asking_price' value='".$asking_price."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Sold for</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['sold_for_currencyid']) ) $sold_for_currencyid=$_SESSION['search_admin']['sold_for_currencyid'];
                    else $sold_for_currencyid="";
                    admin_html::select_currencies($db,"sold_for_currencyid",$sold_for_currencyid,$onchange,$class=array(),$disable);
                    if( !empty($_SESSION['search_admin']['sold_for_price']) ) $sold_for_price=$_SESSION['search_admin']['sold_for_price'];
                    else $sold_for_price="";
                    print "\t<input type='text' name='sold_for_price' value='".$sold_for_price."' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Location</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['location']) ) $location=$_SESSION['search_admin']['location'];
                    else $location="";
                    print "\t<input type='text' name='location' value='".$location."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Number of Times<br />sold At auction</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['sold_at_auction']) ) $sold_at_auction=$_SESSION['search_admin']['sold_at_auction'];
                    else $sold_at_auction="";
                    print "\t<input type='text' name='sold_at_auction' id='sold_at_auction' value='".$sold_at_auction."' class='input_field_admin_small' /></td>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>Number of Times<br />sold Privately</td>\n";
                print "\t<td>\n";
                    if( !empty($_SESSION['search_admin']['sold_privately']) ) $sold_privately=$_SESSION['search_admin']['sold_privately'];
                    else $sold_privately="";
                    print "\t<input type='text' name='sold_privately' id='sold_privately' value='".$sold_privately."' class='input_field_admin_small' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";






            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br /><br /><br /></th>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<td>";
                    print "<a href='/admin/search/?task=reset' title='Reset Form data' >reset</a>";
                print "</td>\n";
                print "\t<td>\n";
                    print "\t<input type='submit' name='submit' value='search' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


        print "\t</table>\n";
        print "\t<input type='hidden' name='sort_how' value='ASC' />\n";
        print "\t<input type='hidden' name='sort_by' value='sort' />\n";
    print "\t</form>\n";
print "\t</div>\n";


$html->foot();
?>
