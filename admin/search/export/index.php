<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/ods.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html2text/html2text.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/htmlpurifier/HTMLPurifier.auto.php");

/*
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
*/

header('Content-Type: text/html; charset='.strtolower(DB_ENCODEING));

set_time_limit(0);

//$html = new html_elements;
//$html->metainfo();

$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);

//print_r($_SESSION);
//print_r($_GET);
//exit;

if( !empty($_SESSION['admin']['search_query']) )
{
    $object = newOds(); //create a new ods file

    $i_cell=0;

    if( !empty($_GET['columns_painting']) || !empty($_GET['columns_painting_opp']) )
    {
        $csv="";
        if( !empty($_GET['columns_painting']) )
        {
            foreach ($_GET['columns_painting'] as $column_name)
            {
                $csv.="p.".$column_name.";";
                $object->addCell(0,0,$i_cell,"p.".$column_name,'string');
                $i_cell++;
            }
        }
        if( !empty($_GET['columns_painting_opp']) )
        {
            foreach ($_GET['columns_painting_opp'] as $column_name)
            {
                $csv.="po.".$column_name.";";
                $object->addCell(0,0,$i_cell,"po.".$column_name,'string');
                $i_cell++;
            }
        }
        $csv.="\r\n";
    }
    else
    {
        $csv="paintID;artworkID;editionID;titleEN;titleDE;titleCH;year;number;ednumber;ednumberroman;ednumberap;citylifepage;nr_of_editions;catID;size;height;width;src;mID;muID;sort;artwork_notesEN;artwork_notesDE;artwork_notesCH;notes;image_zoomer;created;modified;enable\r\n";

        $object->addCell(0,0,$i_cell,"paintID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"artworkID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"editionID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"titleEN",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"titleDE",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"titleCH",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"year",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"number",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"ednumber",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"ednumberroman",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"ednumberap",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"citylifepage",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"nr_of_editions",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"catID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"size",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"height",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"width",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"src",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"mID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"muID",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"sort",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"artwork_notesEN",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"artwork_notesDE",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"artwork_notesCH",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"notes",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"image_zoomer",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"created",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"modified",'string');
        $i_cell++;
        $object->addCell(0,0,$i_cell,"enable",'string');
    }

    $results=$db->query($_SESSION['admin']['search_query']."  ");
    $count=$db->numrows($results);

    $i_row=1;
    while( $row=$db->mysql_array($results) )
    {
        $row=UTILS::html_decode($row);

        $i_cell=0;

        if( !empty($_GET['columns_painting']) || !empty($_GET['columns_painting_opp']) )
        {
            if( !empty($_GET['columns_painting']) )
            {
                foreach ($_GET['columns_painting'] as $column_name)
                {
            		if( $column_name=="artwork_notesEN" || $column_name=="artwork_notesDE" || $column_name=="notes" )
            		{
            			$row[$column_name]=admin_utils::html2plaintext($db,$row[$column_name]);
		        	}

                    $csv.="\"".admin_utils::prepare_for_csv($db,$row[$column_name])."\";";
                    //row,cell,value,float
                    $object->addCell(0,$i_row,$i_cell,$row[$column_name],'string');
                    $i_cell++;
                }
            }
            if( !empty($_GET['columns_painting_opp']) )
            {
                foreach ($_GET['columns_painting_opp'] as $column_name)
                {
                	//$row[$column_name]=admin_utils::html2plaintext($db,$row[$column_name]);
                    $csv.="\"".admin_utils::prepare_for_csv($db,$row[$column_name])."\";";
                    $object->addCell(0,$i_row,$i_cell,$row[$column_name],'string');
                    $i_cell++;
                }
            }

        }
        else
        {
            $object->addCell(0,$i_row,$i_cell,$row['paintID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['artworkID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['editionID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['titleEN'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['titleDE'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['titleCH'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['year'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['number'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['ednumber'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['ednumberroman'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['ednumberap'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['citylifepage'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['nr_of_editions'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['catID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['size'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['height'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['width'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['src'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['mID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['muID'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['sort'],'string');
            $i_cell++;
            //if( !empty($row['artwork_notesEN']) ) $row['artwork_notesEN']=convert_html_to_text($row['artwork_notesEN']);
            //$row['artwork_notesEN'] = $purifier->purify($row['artwork_notesEN']);
            //$row['artwork_notesEN']=str_replace("&", "%26", $row['artwork_notesEN']);
            //$object->addCell(0,$i_row,$i_cell,$row['artwork_notesEN'],'string');
            //$row['artwork_notesEN']=UTILS::convert_fortitleurl($db,TABLE_PAINTING,$row['artwork_notesEN'],"paintID",$paintid,"titleurl_en","en",0," ");
			//$arr = array("&",";",",",".",":","%");
			//$repl = array("%26"," "," "," ");
			//$row['artwork_notesEN']=str_replace($arr, " ", $row['artwork_notesEN']);
			//$row['artwork_notesEN'] = preg_replace( "/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($row['artwork_notesEN']))) );
			$row['artwork_notesEN']=admin_utils::html2plaintext($db,$row['artwork_notesEN']);
            $object->addCell(0,$i_row,$i_cell,$row['artwork_notesEN'],'string');
            $i_cell++;
            $row['artwork_notesDE']=admin_utils::html2plaintext($db,$row['artwork_notesDE']);
            $object->addCell(0,$i_row,$i_cell,$row['artwork_notesDE'],'string');
            $i_cell++;
            $row['artwork_notesCH']=admin_utils::html2plaintext($db,$row['artwork_notesCH']);
            $object->addCell(0,$i_row,$i_cell,$row['artwork_notesCH'],'string');
            $i_cell++;
            $row['notes']=admin_utils::html2plaintext($db,$row['notes']);
            $object->addCell(0,$i_row,$i_cell,$row['notes'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['image_zoomer'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['created'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['modified'],'string');
            $i_cell++;
            $object->addCell(0,$i_row,$i_cell,$row['enable'],'string');


            $csv.="\"".admin_utils::prepare_for_csv($db,$row['paintID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['artworkID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['editionID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['titleEN'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['titleDE'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['titleCH'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['year'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['number'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['ednumber'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['ednumberroman'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['ednumberap'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['citylifepage'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['nr_of_editions'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['catID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['size'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['height'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['width'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['src'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['mID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['muID'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['sort'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['artwork_notesEN'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['artwork_notesDE'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['artwork_notesCH'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['notes'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['image_zoomer'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['created'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['modified'])."\";";
            $csv.="\"".admin_utils::prepare_for_csv($db,$row['enable'])."\";";
        }
        $i_row++;

        $csv.="\r\n";
    }

    //header('Content-transfer-encoding: binary');
    //header('Content-Type: text/csv; charset=UTF-8');
    //header('Content-disposition: attachment;filename=paintings_export_'.date('d_m_Y_G_i_s').'.csv');
    //header('Pragma: public');
    //print $csv;
    //exit;


    $ods_url=$_SERVER["DOCUMENT_ROOT"].'/admin/search/export/paintings.ods';
    saveOds($object,$ods_url); //save the object to a ods file

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($ods_url));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($ods_url));
    readfile($ods_url);
    exit;

}

//$html->footer();
?>
