<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# menu selected
$html->menu_admin_selected="search";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




if( $_GET['task']=="reset" )
{
    unset($_SESSION['search_admin']);
    UTILS::redirect("/admin/search/");
}

print "\t<script type='text/javascript'>\n";
    print "\twindow.addEvent('domready', function() {";
        print "new DatePicker($$('.sale_date_from'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
        print "new DatePicker($$('.sale_date_to'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d' });\n";
    print "\t});\n";
print "\t</script>\n";

    print "\t<h1>Search paintings</h1>\n";

    print "\t<form action='search.php' method='get'>\n";

        print "\t<table class='admin-table'>\n";
            print "\t<tr>\n";
                print "\t<th colspan='15' style='text-align:center;'>Fields to display</th>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                print "\t<td>\n";
                    print "Table Paintings";
                print "\t</td>\n";
                print "\t<td>\n";
                    print "Table Paintings OPP";
                print "\t</td>\n";
            print "\t</tr>\n";
            print "\t<tr>\n";
                print "\t<td>\n";
                    # Table painting columns
                    $results_columns_painting=$db->query("SHOW COLUMNS FROM ".TABLE_PAINTING);

                    print "<select name='columns_painting[]' multiple='multiple' style='height:250px;width:150px;'>";
                        while($row_columns_painting=$db->mysql_array($results_columns_painting) )
                        {
                            print "<option value='".$row_columns_painting['Field']."' style='padding:5px;font-size:13px;'>".$row_columns_painting['Field']."</option>";
                        }
                    print "</select>";
                print "\t</td>\n";
                print "\t<td>\n";
                    # Table painting OPP columns
                    $results_columns_painting=$db->query("SHOW COLUMNS FROM ".TABLE_PAINTING_OPP);
                    print "<select name='columns_painting_opp[]' multiple='multiple' style='height:250px;width:150px;'>";
                        while($row_columns_painting=$db->mysql_array($results_columns_painting) )
                        {
                            print "<option value='".$row_columns_painting['Field']."' style='padding:5px;font-size:13px;'>".$row_columns_painting['Field']."</option>";
                        }
                    print "</select>";
                print "\t</td>\n";
            print "\t</tr>\n";
        print "\t</table>\n";


        print "\t<table class='admin-table'>\n";
            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Artwork info<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Disabled</th>\n";
                print "\t<td>\n";
                    if( $_SESSION['search_admin']['enable'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' name='enable' value='1' ".$checked." class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";



            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Artwork</th>\n";
                print "\t<td>\n";
                    admin_html::select_artworks($db,"artworkID",$_SESSION['search_admin']['artworkID'],$onchange,$class=array(),$disable,$where=" WHERE artworkID!=10 and artworkID!=11 ");
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Title</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='title' value='".$_SESSION['search_admin']['title']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>paintID</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='paintid' value='".$_SESSION['search_admin']['paintid']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Year</th>\n";
                print "\t<td>\n";
                    admin_html::get_years("[]",$_SESSION['search_admin']['year'],"class='select_admin_years_multi'"," multiple='multiple' ");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Number</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='number' value='".$_SESSION['search_admin']['number']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Category</th>\n";
                print "\t<td>\n";
                    $class['select_admin_cat_multi']="select_admin_cat_multi";
                    admin_html::select_categories_admin($db,"catID",$_SESSION['search_admin']['catID'],$onchange,$class,$disable,$where="",$multiple=" multiple='multiple' ");
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Height</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='height_from' value='".$_SESSION['search_admin']['height_from']."' class='input_field_admin_small' />\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "\t<input type='text' name='height_to' value='".$_SESSION['search_admin']['height_to']."' class='input_field_admin_small' />\n";
                    print " Have no dimensions ";
                    if( $_SESSION['search_admin']['no_dimensions'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' name='no_dimensions' value='1' ".$checked." />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Width</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='width_from' value='".$_SESSION['search_admin']['width_from']."' class='input_field_admin_small' />\n";
                    print "&nbsp;&nbsp;-&nbsp;&nbsp;";
                    print "\t<input type='text' name='width_to' value='".$_SESSION['search_admin']['width_to']."' class='input_field_admin_small' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Medium</th>\n";
                print "\t<td>\n";
                    $class['select_admin_medium_multi']="select_admin_medium_multi";
                    admin_html::select_medium($db,"mID",$_SESSION['search_admin']['mID'],$onchange,$class,$disable,$where="",$multiple="");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Museum</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='museum' value='".$_SESSION['search_admin']['museum']."' class='admin-input' />\n";
                    print " Show paintings which are in museum ";
                    if( $_SESSION['search_admin']['in_museum'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' name='in_museum' value='1' ".$checked." />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Sales data<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Auction house</th>\n";
                print "\t<td colspan='3'>\n";
                    $class['select_admin_auction_multi']="select_admin_auction_multi";
                    admin_html::select_auction_houses($db,"ahID[]",$_SESSION['search_admin']['ahID'],$onchange,$class,$disable,"multiple='multiple'");
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sale date</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='sale_date_from' id='sale_date_from' value='".$_SESSION['search_admin']['sale_date_from']."' class='sale_date_from dashboard' />\n";
                    print "\t - <input type='text' name='sale_date_to' id='sale_date_to' value='".$_SESSION['search_admin']['sale_date_to']."' class='sale_date_to dashboard' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Exclude<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Exhibition ID</th>\n";
                print "\t<td>\n";
                    print "\t<input type='text' name='exclude_exhibitionid' value='".$_SESSION['search_admin']['exclude_exhibitionid']."' class='admin-input' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th colspan='2' style='text-align:center;'><br />Relations<br /><br /></th>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Has relations</th>\n";
                print "\t<td>\n";
                    print "Literature <br />";
                    if( $_SESSION['search_admin']['relations_literature'] ) $checked="checked='checked'";
                    else $checked="";
                    print "\t<input type='checkbox' name='relations_literature' value='1' ".$checked." />\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<td>";
                    print "<a href='/admin/search/?task=reset' title='Reset Form data' ><u>reset</u></a>";
                print "</td>\n";
                print "\t<td>\n";
                    print "\t<input type='submit' name='submit' value='search' />\n";
                print "\t</td>\n";
            print "\t</tr>\n";


        print "\t</table>\n";
        //print "\t<input type='hidden' name='sort_how' value='ASC' />\n";
        //print "\t<input type='hidden' name='sort_by' value='sort' />\n";
    print "\t</form>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
