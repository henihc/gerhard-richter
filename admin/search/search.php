<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
//if( $_GET['sp']!="all" ) $options_get['get']['sp']=32;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="search";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();

    if( !empty($_GET['ahID']) )
    {
        $count_sales=0;
        foreach( $_GET['ahID'] as $ahid )
        {
            $results_sales=$db->query("SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE ahID='".$ahid."' ");
            $count_sale=$db->numrows($results_sales);
            if( $count_sale>0 ) $count_sales++;
        }
    }


    if( !empty($_GET['sale_date_from']) || !empty($_GET['sale_date_to']) )
    {
        if(  !empty($_GET['sale_date_from']) && !empty($_GET['sale_date_to']))
        {
            $query_sale_date="SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE saleDate BETWEEN '".$_GET['sale_date_from']."' AND '".$_GET['sale_date_to']."' ";
        }
        elseif( !empty($_GET['sale_date_from']) && empty($_GET['sale_date_to']) )
        {
            $query_sale_date="SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE saleDate>='".$_GET['sale_date_from']."'";
        }
        else if( empty($_GET['sale_date_from']) && !empty($_GET['sale_date_to']) )
        {
            $query_sale_date="SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE saleDate<='".$_GET['sale_date_to']."'";
        }
        $results_sales_date=$db->query($query_sale_date);
        $count_sales_date=$db->numrows($results_sales_date);
    }

    $query_from_paintings="";
    if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) || !empty($_GET['in_museum']) || $count_sales>0 || $count_sales_date>0 || !empty($_GET['exclude_exhibitionid']) ) $query_where_paintings=" WHERE ";

    # enable
    if( !empty($_GET['enable']) ) $query_where_paintings.=" enable=0 ";
    # END enable
    # artworkID
    if( !empty($_GET['artworkID']) )
    {
        if( !empty($_GET['enable']) ) $and=" AND ";
        else $and="";
        if( $_GET['artworkID']=='paintings' ) $query_where_paintings.=$and." ( artworkID='1' OR artworkID='2' OR artworkID='13' ) ";
        else $query_where_paintings.=$and." artworkID='".$_GET['artworkID']."' ";
    }
    # END artworkID
    # title
    if( !empty($_GET['title']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) ) $and=" AND ";
        else $and="";

        # prepare search keyword
        $values_search=array();
        $values_search['painting_search']=1;
        $values_search['db']=$db;
        $title_search=UTILS::prepear_search($_GET['title'],$values_search);
        # END prepare search keyword

        # prepare match
        $values_search_paintings=array();
        $values_search_paintings['search']=$title_search;
        $query_match=QUERIES_SEARCH::query_search_paintings($db,$values_search_paintings);
        # END prepare match

        $query_where_paintings.=$and." ".$query_match;

    }
    # title
    # paintid
    if( !empty($_GET['paintid']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) ) $and=" AND ";
        else $and="";

        $query_where_paintings.=$and." paintID='".$_GET['paintid']."'";

    }
    # paintid
    # year
    if( !empty($_GET['year']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and." ( ";
        for($i=0;$i<sizeof($_GET['year']);$i++)
        {
            if( $i==0 ) $or="";
            else $or=" OR ";
            $query_where_paintings.=$or." year='".$_GET['year'][$i]."'";
        }
        $query_where_paintings.=" ) ";
    }
    # END year
    # number
    if( !empty($_GET['number']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and;

        $options_search_array_number=array();
        $options_search_array_number['number']=$_GET['number'];
        $number_search=UTILS::prepare_cr_number_search($db,$options_search_array_number);
        //$search_array=UTILS::search_array($number);
        //print_r($search_array);
        //exit;

        //print $number_search;


        $query_where_paintings.=$number_search;

    }
    # END number
    # catID
    if( !empty($_GET['catID']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) ) $and=" AND ";
        else $and="";
        if( is_array($_GET['catID']) )
        {
            $query_where_paintings.=$and." ( ";
            for( $i=0;$i<sizeof($_GET['catID']);$i++ )
            {
                if( $i==0 ) $or="";
                else $or=" OR ";
                $query_where_paintings.=$or." catID='".$_GET['catID'][$i]."'";
            }
            $query_where_paintings.=" ) ";
        }
        else $query_where_paintings.=$and." catID='".$_GET['catID']."'";
    }
    # END catID

    if( !empty($_GET['no_dimensions']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and."";
        $query_where_paintings.=" ( ( height='' AND width='' ) OR ( height IS NULL AND width IS NULL ) OR ( height=0 AND width=0 ) ) ";
    }
    else
    {
        # height
        if( !empty($_GET['height_from']) || !empty($_GET['height_to']) )
        {
            if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) ) $and=" AND ";
            else $and="";
            $query_where_paintings.=$and."";
            if( !empty($_GET['height_from']) && empty($_GET['height_to']) ) $query_where_paintings.=" height>='".$_GET['height_from']."' ";
            if( empty($_GET['height_from']) && !empty($_GET['height_to']) ) $query_where_paintings.=" height<='".$_GET['height_to']."' ";
            if( !empty($_GET['height_from']) && !empty($_GET['height_to']) ) $query_where_paintings.=" height BETWEEN '".$_GET['height_from']."' AND '".$_GET['height_to']."' ";
        }
        # END height
        # width
        if( !empty($_GET['width_from']) || !empty($_GET['width_to']) )
        {
            if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title'])  || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) ) $and=" AND ";
            else $and="";
            $query_where_paintings.=$and."";
            if( !empty($_GET['width_from']) && empty($_GET['width_to']) ) $query_where_paintings.=" width>='".$_GET['width_from']."' ";
            if( empty($_GET['width_from']) && !empty($_GET['width_to']) ) $query_where_paintings.=" width<='".$_GET['width_to']."' ";
            if( !empty($_GET['width_from']) && !empty($_GET['width_to']) ) $query_where_paintings.=" width BETWEEN '".$_GET['width_from']."' AND '".$_GET['width_to']."' ";
        }
        # END width
    }
    # medium
    if( !empty($_GET['mID']) )
    {
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title'])  || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and." mID='".$_GET['mID']."' ";
    }
    # END medium
    # museum
    if( !empty($_GET['museum']) )
    {
        $results_museum=$db->query("SELECT muID FROM ".TABLE_MUSEUM." WHERE ( museum LIKE '%".$_GET['museum']."%' OR museumDEsearch1 LIKE '%".$_GET['museum']."%' OR museumDEsearch2 LIKE '%".$_GET['museum']."%' ) ");
        $count_museum=$db->numrows($results_museum);
        if( $count_museum>0 )
        {
            $k=0;
            while( $row_museum=$db->mysql_array($results_museum) )
            {
                $k++;
                if( $k!=1 )
                {
                    $query_museums1.=" OR ";
                    $query_museums2.=" OR ";
                }
                $query_museums1.=" r.itemid1='".$row_museum['muID']."' ";
                $query_museums2.=" r.itemid2='".$row_museum['muID']."' ";
            }

            $query_from_paintings=", ".TABLE_RELATIONS." r ";
            if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) ) $and=" AND ";
            else $and="";
            $query_where_paintings.=$and." (( p.paintID=r.itemid1 AND r.typeid1=1 ) OR ( p.paintID=r.itemid2 AND r.typeid2=1 )) AND (( r.typeid1=16 AND ( ".$query_museums1." ) AND r.typeid2=1 ) OR ( r.typeid2=16 AND ( ".$query_museums2." ) AND r.typeid1=1 )) ";
        }
    }
    # END museum
    # in_museum
    if( !empty($_GET['in_museum']) )
    {
        $query_from_paintings=", ".TABLE_RELATIONS." r ";
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and." (( p.paintID=r.itemid1 AND r.typeid1=1 ) OR ( p.paintID=r.itemid2 AND r.typeid2=1 )) AND (( r.typeid1=16 AND r.itemid1>0 AND r.typeid2=1 ) OR ( r.typeid2=16 AND r.itemid2>0 AND r.typeid1=1 )) ";
    }
    # END in_museum
    # relations
    if( !empty($_GET['relations_literature']) )
    {
        $query_from_paintings=", ".TABLE_RELATIONS." r ";
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and." (( p.paintID=r.itemid1 AND r.typeid1=1 ) OR ( p.paintID=r.itemid2 AND r.typeid2=1 )) AND (( r.typeid1=12 AND r.itemid1>0 AND r.typeid2=1 ) OR ( r.typeid2=12 AND r.itemid2>0 AND r.typeid1=1 )) ";
    }
    # END relations
    # auction house
    if( !empty($_GET['ahID']) && $count_sales>0 )
    {
        if( $count_sales>0 ) $query_auction_paint1=" ( ";
        $k=0;
        foreach( $_GET['ahID'] as $ahid )
        {
            $results_sales=$db->query("SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE ahID='".$ahid."' ");
            $count_sales2=$db->numrows($results_sales);
            if( $count_sales2>0 )
            {
                while( $row_sales=$db->mysql_array($results_sales) )
                {
                    $results_sales_rel_paint=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=2 AND itemid1='".$row_sales['saleID']."' AND typeid2=1 ) OR ( typeid2=2 AND itemid2='".$row_sales['saleID']."' AND typeid1=1 ) ");
                    $row_sales_rel_paint=$db->mysql_array($results_sales_rel_paint);
                    $paintid=UTILS::get_relation_id($db,"1",$row_sales_rel_paint);
                    $k++;
                    if( $k!=1 )
                    {
                        $query_auction_paint1.=" OR ";
                    }
                    $query_auction_paint1.=" p.paintID='".$paintid."' ";
                }

                if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) || !empty($_GET['in_museum']) || !empty($_GET['relations_literature']) ) $and=" AND ";
                else $and="";
            }
        }
        if( $count_sales>0 ) $query_where_paintings.=$and." ".$query_auction_paint1." ) ";
    }
    # END auction house
    # sale date
    if( $count_sales_date>0 )
    {
        $k=0;
        $query_saledate_paint1=" ( ";
        while( $row_sales_date=$db->mysql_array($results_sales_date) )
        {
            $results_sales_rel_paint=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=2 AND itemid1='".$row_sales_date['saleID']."' AND typeid2=1 ) OR ( typeid2=2 AND itemid2='".$row_sales_date['saleID']."' AND typeid1=1 ) ");
            $row_sales_rel_paint=$db->mysql_array($results_sales_rel_paint);
            $paintid=UTILS::get_relation_id($db,"1",$row_sales_rel_paint);
            $k++;
            if( $k!=1 )
            {
                $query_saledate_paint1.=" OR ";
            }
            $query_saledate_paint1.=" p.paintID='".$paintid."' ";
        }
        $query_saledate_paint1.=" ) ";

        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) || !empty($_GET['in_museum']) || !empty($_GET['relations_literature']) || $count_sales>0 ) $and=" AND ";
        else $and="";
        $query_where_paintings.=$and." ".$query_saledate_paint1;
    }
    # END sale date
    # exclude_exhibitionid
    if( !empty($_GET['exclude_exhibitionid']) )
    {
        $exclude_exhibitionid = preg_replace('/\s+/', '', $_GET['exclude_exhibitionid']);
        $exhibitions=explode(",", $exclude_exhibitionid);
        $query_exclude_exhibitionid="(";
        $i=0;
        foreach ($exhibitions as $key => $exhibitionid)
        {
            $query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
            $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
            $results_exhibition=$db->query($query_exhibition['query']);
            $count_exhibition=$db->numrows($results_exhibition);
            if( $count_exhibition )
            {
                $row_exhibition=$db->mysql_array($results_exhibition,0);
                $query_artworks="SELECT p.paintID
                    FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p
                    WHERE
                        ( ( r.typeid1=4 AND r.itemid1='".$exhibitionid."' AND r.typeid2=1 )
                            OR ( r.typeid2=4 AND r.itemid2='".$exhibitionid."' AND r.typeid1=1 ) )
                        AND ( ( r.itemid2=p.paintID AND r.typeid2=1 )
                            OR ( r.itemid1=p.paintID AND r.typeid1=1 ) )
                         ";
                    $results_artworks=$db->query($query_artworks);
                    $count_artworks=$db->numrows($results_artworks);
                    if( $count_artworks )
                    {
                        //print "count=".$count_artworks;
                        while( $row_artworks=$db->mysql_array($results_artworks) )
                        {
                            if( $i>0 ) $query_exclude_exhibitionid.=" AND ";
                            $query_exclude_exhibitionid.=" paintID!='".$row_artworks['paintID']."'";
                            $i++;
                        }
                    }
            }
        }
        $query_exclude_exhibitionid.=")";
        if( !empty($_GET['enable']) || !empty($_GET['artworkID']) || !empty($_GET['title']) || !empty($_GET['paintid']) || !empty($_GET['year']) || !empty($_GET['number']) || !empty($_GET['catID']) || !empty($_GET['no_dimensions']) || !empty($_GET['height_from']) || !empty($_GET['height_to']) || !empty($_GET['width_from']) || !empty($_GET['width_to']) || !empty($_GET['mID']) || !empty($_GET['museum']) || !empty($_GET['in_museum']) || !empty($_GET['relations_literature']) || $count_sales>0 || $count_sales_date>0 ) $and=" AND ";
        else $and="";
        if( $query_exclude_exhibitionid!="()" ) $query_where_paintings.=$and." ".$query_exclude_exhibitionid;
    }
    # END exclude_exhibitionid


//print $query_where_paintings;

    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
    if( $_GET['sp']!="all" ) $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];

    if( !empty($_GET['columns_painting']) || !empty($_GET['columns_painting_opp']) )
    {
        $query_paintings['query_without_limit']="SELECT p.paintID,p.enable";
        $i=0;
        if( is_array($_GET['columns_painting']) )
        {
            foreach ($_GET['columns_painting'] as $column_name)
            {
                $i++;
                //if( $i==1 ) $comma="";
                //else $comma=",";
                $comma=",";
                $query_paintings['query_without_limit'].=$comma."p.".$column_name;
            }
        }
        if( is_array($_GET['columns_painting_opp']) )
        {
            foreach ($_GET['columns_painting_opp'] as $column_name)
            {
                $i++;
                if( $i==1 && !empty($_GET['columns_painting']) ) $comma="";
                else $comma=",";
                $query_paintings['query_without_limit'].=$comma."po.".$column_name;
            }
        }
        $query_paintings['query_without_limit'].=" FROM ".TABLE_PAINTING." p ".$query_from_paintings;
        # left join pai ting opp
        if( !empty($_GET['columns_painting_opp']) )
        {
            $query_paintings['query_without_limit'].=" LEFT JOIN ".TABLE_PAINTING_OPP." po ON p.paintID=po.paintid ";
        }
        # END left join pai ting opp
        $query_paintings['query_without_limit'].=$query_where_paintings.$query_order;
        $query_paintings['query']=$query_paintings['query_without_limit']." ".$query_limit;
    }
    else
    {
        $query_paintings=QUERIES::query_painting($db,$query_where_paintings,$query_order,$query_limit,$query_from_paintings);
    }

    //print $query_paintings['query'];
    //exit;

    $results=$db->query($query_paintings['query_without_limit']);
    $_SESSION['admin']['search_columns_painting']=$_GET['columns_painting'];
    $_SESSION['admin']['search_columns_painting_opp']=$_GET['columns_painting_opp'];
    $_SESSION['admin']['search_query']=$query_paintings['query_without_limit'];
    $_SESSION['admin']['search_url']="/admin/search/search.php"."?".$_SERVER["QUERY_STRING"];
    if( (!empty($_GET['ahID']) && $count_sales==0) || ( (!empty($_GET['sale_date_from']) || !empty($_GET['sale_date_to'])) && $count_sales_date==0 ) ) $count=0;
    else $count=$db->numrows($results);
    $results_paintings=$db->query($query_paintings['query']);
    $pages=@ceil($count/$_GET['sp']);

    /*
    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) )
    {
        $url="?sort_by=".$_GET['sort_by']."&sort_how=".$_GET['sort_how']."&p=";
    }
    else*/
    $i=0;
    $url1="?";
    foreach( $_GET as $key => $value )
    {
        if( $i!=0 ) $sign="&";
        else $sign="";
        if( $key!="p" && $key!="sp" && $key!="page_selected" && $key!="show_per_page" && $key!="pp" && $key!="l" )
        {
            if( is_array($value) )
            {
                foreach ($value as $value2) {
                    if( $i!=0 ) $sign="&";
                    else $sign="";
                    $url1.=$sign.$key."[]=".$value2;
                    $i++;
                }

            }
            else
            {
                $url1.=$sign.$key."=".$value;
            }
        }
        $i++;
    }
    $url2=$url1;
    $url1.="&sp=".$_GET['sp']."&p=";
    //print $url1;
    //print "<br />".$url2;

    # menu admin
    if( $count>0 )
    {
        //print "\t<a href='#' onclick=\"popup('/admin/search/export/".$url1."',800,400,'no');\" title='Export to csv file'><u>export</u></a>&nbsp;&nbsp;\n";
        print "\t<a href='/admin/search/export/".$url1."' title='Export to csv file'><u>export</u></a>&nbsp;&nbsp;\n";
        print "\t<a href='#' onclick=\"popup('/admin/search/print',950,600,'yes');\" title='Print search results'><u>print</u></a><br /><br />\n";
    }
    else print "<br /><br />";
    #end menu admin

    if( $count>0 )
    {
        print "<p>Total found = ".$count."</p>";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url1,$url1,$class,true,"",1);
        if( $_GET['sp']=="1000000" )
        {
            print "<div class='page_numbering_admin'>";
            admin_html::page_per_page_admin($db,$pages,$_GET['sp'],$count,$url2,$class);
            print "</div>";
        }

        print "\t<table class='admin-table'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";

                    function print_th($url1,$column_name,$get_sort_by,$get_sort_how,$sortable=1)
                    {
                        if( $get_sort_by==$column_name )
                        {
                            if( $get_sort_how=='asc' ) $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';

                        print "\t<th>";
                            if( $sortable )
                            {
                                print "<a href='".$url1."&sort_by=".$column_name."&sort_how=".$sort_how."' title=''><u>";
                            }
                                    print $column_name;
                            if( $sortable )
                            {
                                print "</u></a>";
                            }
                        print "</th>\n";
                    }

                    if( !empty($_GET['columns_painting']) || !empty($_GET['columns_painting_opp']) )
                    {
                        print_th($url1,"p.paintID",$_GET['sort_by'],$_GET['sort_how']);
                        print_th($url1,"image",$_GET['sort_by'],$_GET['sort_how'],0);
                        if( !empty($_GET['columns_painting']) )
                        {
                            foreach ($_GET['columns_painting'] as $column_name)
                            {
                                print_th($url1,"p.".$column_name,$_GET['sort_by'],$_GET['sort_how']);
                            }
                        }
                        if( !empty($_GET['columns_painting_opp']) )
                        {
                            foreach ($_GET['columns_painting_opp'] as $column_name)
                            {
                                print_th($url1,"po.".$column_name,$_GET['sort_by'],$_GET['sort_how']);
                            }
                        }
                    }
                    else
                    {
                        if( $_GET['sort_by']=='paintid' )
                        {
                            if( $_GET['sort_how']=='asc' ) $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th><a href='".$url1."&sort_by=p.paintid&sort_how=$sort_how' title='Order by paint id'><u>paintid</u></a></th>\n";

                        print "\t<th>image</th>\n";

                        if( $_GET['sort_by']=='number' )
                        {
                            if($_GET['sort_how']=='asc') $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th><a href='".$url1."&sort_by=p.number&sort_how=$sort_how' title='Order by number'><u>number</u></a></th>\n";

                        if($_GET['sort_by']=='titleEN')
                        {
                            if($_GET['sort_how']=='asc') $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th style='width:240px;'><a href='".$url1."&sort_by=p.titleEN&sort_how=$sort_how' title='Order by titleEN'><u>titleEN</u></a></th>\n";

                        if($_GET['sort_by']=='titleDE')
                        {
                            if($_GET['sort_how']=='asc') $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th style='width:240px;'><a href='".$url1."&sort_by=p.titleDE&sort_how=$sort_how' title='Order by titleDE'><u>titleDE</u></a></th>\n";

                        if($_GET['sort_by']=='titleZH')
                        {
                            if($_GET['sort_how']=='asc') $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th style='width:240px;'><a href='".$url1."&sort_by=p.titleZH&sort_how=$sort_how' title='Order by titleZH'><u>titleZH</u></a></th>\n";

                        if($_GET['sort_by']=='year')
                        {
                            if($_GET['sort_how']=='asc') $sort_how='desc';
                            else $sort_how='asc';
                        }
                        else $sort_how='asc';
                        print "\t<th><a href='".$url1."&sort_by=p.year&sort_how=$sort_how' title='Order by year'><u>year</u></a></th>\n";
                    }
                print "\t</tr>\n";
            print "\t</thead>\n";



        while( $row=$db->mysql_array($results_paintings) )
        {
            $row=UTILS::html_decode($row);
            if( $row['enable']==0 ) $style="background:red;";
            else $style="";
            print "\t<tr>\n";
                print "\t<td style='text-align:center;".$style."'>\n";
                    print "\t<a href='/admin/paintings/edit/?paintid=".$row['paintID']."' style='text-decoration:underline;color:blue;' title='edit this exhibition'>".$row['paintID']."</a>\n";
                print "\t</td>";
                print "\t<td style='text-align:center;'>\n";
                    //print "\t<img src='/includes/retrieve.image.php?size=xs&paintID=".$row['paintID']."' alt='' />\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_s__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";
                    print "\t<img src='".$src."' alt='' />\n";
                print "\t</td>\n";
                if( !empty($_GET['columns_painting']) || !empty($_GET['columns_painting_opp']) )
                {
                    if( !empty($_GET['columns_painting']) )
                    {
                        for ($i=0; $i < count($_GET['columns_painting']); $i++)
                        {
                            print "\t<td>\n";
                                if( $_GET['columns_painting'][$i]=="locationid" )
                                {
                                    $options_location=array();
                                    $options_location['row']=$row;
                                    $location=location($db,$options_location);
                                    print $location;
                                    //print_r($row);
                                }
                                else
                                {
                                    print $row[$_GET['columns_painting'][$i]];
                                }
                            print "\t</td>\n";
                        }
                    }
                    if( !empty($_GET['columns_painting_opp']) )
                    {
                        for ($i=0; $i < count($_GET['columns_painting_opp']); $i++)
                        {
                            print "\t<td>\n";
                                if( $_GET['columns_painting_opp'][$i]=="verified_verso" || $_GET['columns_painting_opp'][$i]=="verified_recto" )
                                {
                                    if( $row[$_GET['columns_painting_opp'][$i]] ) $value="Yes";
                                    else $value="No";
                                    print $value;
                                }
                                else
                                {
                                    print $row[$_GET['columns_painting_opp'][$i]];
                                }
                            print "\t</td>\n";
                        }
                    }
                }
                else
                {
                    print "\t<td style='text-align:center;'>\n";
                        print $row['number'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['titleEN'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['titleDE'];
                    print "\t</td>\n";
                    print "\t<td>\n";
                        print $row['titleZH'];
                    print "\t</td>\n";
                    print "\t<td style='text-align:center;'>\n";
                        print $row['year'];
                    print "\t</td>\n";
                }
            print "\t</tr>\n";
        }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url1,$url1,$class,false,"",1);
        if( $_GET['sp']=="1000000" )
        {
            print "<div class='page_numbering_admin'>";
            admin_html::page_per_page_admin($db,$pages,$_GET['sp'],$count,$url2,$class);
            print "</div>";
        }
    }
    else print "\t<p class='p_admin_no_data_found'>No paintings found!</p>\n";

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
