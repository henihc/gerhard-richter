<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

set_time_limit(0);

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->js[]="/admin/js/common.js";

$html->metainfo();



if( !empty($_SESSION['admin']['search_query']) )
{

    $results=$db->query($_SESSION['admin']['search_query']);
    $count=$db->numrows($results);
    //print_r($_SESSION['admin']);
    print "\t<table cellspacing='0' cellpadding='0' class='table-search-print'>\n";
        print "\t<thead>\n";
            print "\t<tr>\n";
                print "\t<th>paintid</th>\n";
                print "\t<th>image</th>\n";
                if( !empty($_SESSION['admin']['search_columns_painting']) || !empty($_SESSION['admin']['search_columns_painting_opp']) )
                {
                    for ($i=0; $i < count($_SESSION['admin']['search_columns_painting']); $i++) 
                    {
                        if( $_SESSION['admin']['search_columns_painting'][$i]=="locationid" )
                        {
                            $column_name="p.location";
                            unset($_SESSION['admin']['search_columns_painting'][$i+1]);
                            unset($_SESSION['admin']['search_columns_painting'][$i+2]);
                        }
                        else $column_name="p.".$_SESSION['admin']['search_columns_painting'][$i];
                        print "\t<th>".$column_name."</th>\n";
                    }
                    if( is_array($_SESSION['admin']['search_columns_painting_opp']) )
                    {
                        foreach ($_SESSION['admin']['search_columns_painting_opp'] as $column_name) 
                        {
                            print "\t<th>po.".$column_name."</th>\n";
                        }
                    }
                }
                else
                {
                    print "\t<th>number</th>\n";
                    print "\t<th>titleEN</th>\n";
                    print "\t<th>titleDE</th>\n";
                    print "\t<th>titleCN</th>\n";
                    print "\t<th>year</th>\n";
                }
            print "\t</tr>\n";
        print "\t</thead>\n";
        while( $row=$db->mysql_array($results) )
        {
            //$row=UTILS::html_decode($row);
            print "\t<tr>\n";
                print "\t<td style='text-align:center;'>".$row['paintID']."</td>\n";
                print "\t<td style='text-align:center;'>";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row['paintID']."' ) ORDER BY sort ASC, relationid DESC ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    $src="/images/size_m__imageid_".$imageid.".jpg";
                    $link="/images/size_l__imageid_".$imageid.".jpg";
                    print "\t<img src='".$src."' alt='' />\n";
                print "\t</td>\n";
                if( !empty($_SESSION['admin']['search_columns_painting']) || !empty($_SESSION['admin']['search_columns_painting_opp']) )
                {
                    for ($i=0; $i < count($_SESSION['admin']['search_columns_painting']); $i++) 
                    {
                        if( $_SESSION['admin']['search_columns_painting'][$i]=="locationid" )
                        {
                            $options_location=array();
                            $options_location['row']=$row;
                            $location=location($db,$options_location);
                            //print $location;
                            print "\t<td>".$location."</td>\n";
                            //print_r($row);
                        }
                        else
                        {
                            print "\t<td>".$row[$_SESSION['admin']['search_columns_painting'][$i]]."</td>\n";
                        }
                    }
                    if( is_array($_SESSION['admin']['search_columns_painting_opp']) )
                    {
                        foreach ($_SESSION['admin']['search_columns_painting_opp'] as $column_name) 
                        {
                            print "\t<td>".$row[$column_name]."</td>\n";
                        }
                    }
                }
                else
                {
                    print "\t<td style='text-align:center;'>".$row['number']."</td>\n";
                    print "\t<td>".$row['titleEN']."</td>\n";
                    print "\t<td>".$row['titleDE']."</td>\n";
                    print "\t<td>".$row['titleCH']."</td>\n";
                    print "\t<td style='text-align:center;'>".$row['year']."</td>\n";
                }
            print "\t</tr>\n";
        }
    print "\t</table>\n";

        
}


$html->foot();
?>
