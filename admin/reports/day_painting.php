<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/utils.php");

require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/gapi/gapi.class.php");
require_once ($_SERVER["DOCUMENT_ROOT"].'/includes/php_mailer/PHPMailerAutoload.php');

$db=new dbCLASS;


if( !empty($_GET['date']) )
{
        $time=strtotime($_GET['date']);
        $date_display=date("jS F", $time);
        $start_date=$_GET['date'];
        $end_date=$_GET['date'];
}
else
{
    $date_from_year=date("Y");
    $date_from_month=date("m");
    $date_from_day=date("d")-2;
    $date_display=date("jS F", mktime(0, 0, 0, $date_from_month, $date_from_day, $date_from_year));
    $start_date=date("Y-m-d",mktime(0, 0, 0, $date_from_month, $date_from_day, $date_from_year));

    $date_to_year=date("Y");
    $date_to_month=date("m");
    $date_to_day=date("d")-2;
    $end_date=date("Y-m-d",mktime(0, 0, 0, $date_to_month, $date_to_day, $date_to_year));
    //print $start_date." | ".$end_date;
    //$end_date=date("Y-m-d");
}

    $html="
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
    <title>Gerhard-Richter.com daily report - ".$date_display."</title>
    <style type=\"text/css\">

    .title {
        margin: 20px 0 0 5px;
        width: 600px;
        text-align: center;
    }

    .item {
        color: #559DE6;
    }

    .item-artwork {
        color: #E86857;
    }

    .item-value {
        font-weight: bold;
    }

    .admin-option {
        width:646px;
        margin:0 0 0 5px;
    }

    .admin-option-h2 {
        background: #747474;
        font-weight: normal;
        padding: 8px;
        font-size: 1.1em;
        color: #fff;
        width: 540px;
    }

    .clear {
        clear: both;
    }

    .admin-option-small {
        width: 370px;
        margin: 0 0 0 5px;
        float: left;
    }

    .h1-orange {
        color: orange;
    }

    </style>
    </head>
        <body>";



/* Set your Google Analytics credentials */
define('ga_account'     ,'163175018466-itogrkgi8stbmva06n06jvql6ua693uk@developer.gserviceaccount.com');
define('ga_password'    ,'gerhard_richter_analytics-65f6037c5b5c.p12');
define('ga_profile_id'  ,'35533302');

$ga = new gapi(ga_account,ga_password);

/* We are using the 'source' dimension and the 'visits' metrics */
//$dimensions = array('source');
//$dimensions = array('pagepath','country');
$dimensions = array('pagepath');
$metrics    = array('uniquePageviews');
//$filter="pagePath=@overpainted-photographs/";



$filter="pagePath=@art/paintings/photo-paintings/ ||
        pagePath=@art/paintings/abstracts/ ||
        pagePath=@art/atlas/ ||
        pagePath=@art/editions/ ||
        pagePath=@art/drawings/ ||
        pagePath=@art/overpainted-photographs/ ||
        pagePath=@art/oils-on-paper/ ||
        pagePath=@art/watercolours/ ||
        pagePath=@art/prints/ ||
        pagePath=@art/other/ ||
        pagePath=@exhibitions/ &&
        pagePath!@videos/exhibitions/ &&
        pagePath!@detail.php &&
        pagePath!@atlas.php &&
        pagePath!@category.php
        ";



//$filter="";

$html.="
            <h2 class='title'>Gerhard Richter Report: <span class='h1-orange'>".$date_display."</span></h2>
            <div class='admin-option'>
                <h2 class='admin-option-h2'>Daily site usage</h2>
                <table cellspacing='0' style='width: 540px'>
                <thead>
                <tr>
                <th>Image</th>
                <th>Info</th>
                <!--<th style='width: 130px;'>Country</th>-->
                <th>Total Unique Visists</th>
                </tr>
                </thead>
    ";



/* We will sort the result be desending order of visits,
    and hence the '-' sign before the 'visits' string */
$ga->requestReportData(ga_profile_id, $dimensions, $metrics,'-uniquePageviews',$filter,$start_date,$end_date,1,1000);

$gaResults = $ga->getResults();

$painting_array=array();
foreach($gaResults as $result)
{
    $unique_pageviews=$result->getUniquePageviews();
    $page_path=$result->getPagePath();
    //$page_path="/en/exhibitions/a-new-spirit-in-painting-253/annunciation-after-titian-5996";
    //$page_path="/fr/exhibitions/gerhard-richter-bilder-serien-3180/man-shot-down-2-7692/?p=1&sp=32";
    //$country=$result->getCountry();
           //$result->getSource(),
           //print $result->getPageviews()."-";
           //print $result->getVisits()."-";
           //print $page_path." : ".$unique_pageviews."<br />";
    $mod_rewrite_vars=explode("/", $page_path);

    foreach ($mod_rewrite_vars as $key => $value)
    {
        if( empty($value) || preg_match("/\?p\=/i", $value) || preg_match("/\&sp\=/i", $value) || preg_match("/\?year\=/i", $value) )
        {
            unset($mod_rewrite_vars[$key]);
        }
    }
    $mod_rewrite_vars=array_values($mod_rewrite_vars);

    //print_r($mod_rewrite_vars);

    # collecting paintings viewed via exhibition page
    if( preg_match("/\/exhibitions\//i", $page_path) )
    {
        if( count($mod_rewrite_vars)==4 )
        {
            //print $page_path." : ".$unique_pageviews."<br />";
            //print count($mod_rewrite_vars)."<br /><br /><hr />";
            $tmp=explode("-", $mod_rewrite_vars[3]);
            $paintid=$tmp[(count($tmp)-1)];
            //print $paintid."<br />";
        }
    }
    # paintings detail view from /art section
    else
    {
        if( preg_match("/\/art\/paintings\//i", $page_path) )
        {
            if( count($mod_rewrite_vars)==6 )
            {
                $tmp=explode("-", $mod_rewrite_vars[5]);
                $paintid=$tmp[(count($tmp)-1)];
                //print $paintid."<br />";
                //print $page_path." : ".$unique_pageviews."<br />";
                //print count($mod_rewrite_vars)."<br /><br /><hr />";
            }
        }
        if( preg_match("/\/art\/overpainted-photographs\//i", $page_path) )
        {
            if( count($mod_rewrite_vars)==5 )
            {
                $tmp=explode("-", $mod_rewrite_vars[4]);
                $paintid=$tmp[(count($tmp)-1)];
                //print $paintid."<br />";
                //print $page_path." : ".$unique_pageviews."<br />";
                //print count($mod_rewrite_vars)."<br /><br /><hr />";
            }
        }
        else
        {
            if( count($mod_rewrite_vars)==4 )
            {
                $tmp=explode("-", $mod_rewrite_vars[3]);
                $paintid=$tmp[(count($tmp)-1)];
                //print $paintid."<br />";
                //print $page_path." : ".$unique_pageviews."<br />";
                //print count($mod_rewrite_vars)."<br /><br /><hr />";
            }
        }
    }
    //print $page_path." : <strong>".$mod_rewrite_vars[4]."</strong><br />";

    if( !empty($paintid) )
    {
        $results_painting=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."'");
        $count_painting=$db->numrows($results_painting);

        if( $count_painting )
        {
            //print $page_path." : ".$unique_pageviews." : ".$paintid."<br />";

            $painting_array[$paintid]=$painting_array[$paintid]+$unique_pageviews;
            //$painting_array[$paintid]['unique_pageviews']=$painting_array[$paintid]['unique_pageviews']+$unique_pageviews;
            //$painting_array[$paintid]['page_path'][]=$page_path." : ".$unique_pageviews;
        }
    }
    unset($paintid);
    unset($paintID);
}

arsort($painting_array);

//print_r($painting_array);
//print count($painting_array);

$i=0;
foreach( $painting_array as $paintid => $unique_pageviews )
{
    $i++;
    if( $i<=30 )
    {
        //print $page_path." : ".$unique_pageviews." : ".$paintid."<br />";

        $results_painting=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."'");
        $row_painting=$db->mysql_array($results_painting);
        //$row_painting=UTILS::html_decode($row_painting);

        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='1' AND itemid2='".$paintid."' ) OR ( typeid2=17 AND typeid1='1' AND itemid1='".$paintid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        $row_related_image=$db->mysql_array($results_related_image);
        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
        $src="https://www.gerhard-richter.com/images/size_o__thumb_1__imageid_".$imageid.".jpg";
        //$src="/images/size_o__thumb_1__imageid_".$imageid.".jpg";
        //$src="/datadir/images_new/cache/".$imageid.".jpg";

        if( $row_painting['artworkID']==4 ) $nr="Editions CR";
        elseif( $row_painting['artworkID']==3 ) $nr="Atlas Sheet";
        elseif( !empty($row_painting['number']) ) $nr="CR";
        else $nr="";

        $values=array();
        $values['artworkid']=$row_painting['artworkID'];
        $values['paintid']=$row_painting['paintID'];
        $painting_url=UTILS::get_painting_url($db,$values);

        $html.="<tr>
                    <td style='width: 100px;border-bottom:1px solid #CCC;padding-bottom: 5px;'>
                        ".$i.".<br /><a href='".$painting_url['url']."' style='display:block;'>
                            <img src=\"".$src."\" style='float:left;'  alt='Painting ".$paintid." IMG' />
                        </a>
                    </td>
                    <td style='padding-left: 10px;border-bottom:1px solid #CCC;'>
                    <span class='item'>";
            if( !empty($row_painting['titleDE']) ) $html.=$row_painting['titleDE'];
            if( !empty($row_painting['titleEN']) ) $html.="<br />".$row_painting['titleEN'];
            $html.="<br />".$row_painting['year'];
            if( !empty($row_painting['number']) ) $html.="<br />".$nr.":".$row_painting['number'];
                    $html.="</span>
                        <br />
                        <span class='item-artwork'>".UTILS::get_artwork_name($db,$row_painting['artworkID'])."</span>
                    </td>
                    <!--<td style='border-bottom:1px solid #CCC;'>
                    ".$country." - ".$unique_pageviews_country."

                    </td>-->
                    <td style='border-bottom:1px solid #CCC;text-align:center;'>".$unique_pageviews."</td>
                    </tr>
                    <!--<tr>
                     <td colspan='3'>".$page_path."</td>
                    </tr>-->

                    ";
    }
}

//echo "<br />Total Results : ".$ga->getTotalResults();
//echo '<p>Total pageviews: ' . $ga->getPageviews() . ' total visits: ' . $ga->getVisits() . '</p>';





        $html.="
                    </table>
                </div>
            </body>
            </html>
        ";

print  $html;

                $mail = new PHPMailer();
                $mail->IsSMTP();
                //$mail->SMTPDebug  = 2;

                $mail->Host       = EMAIL_ADMIN_HOST;
                $mail->Port       = EMAIL_ADMIN_PORT;
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = "tls";
                $mail->Username   = EMAIL_ADMIN;
                $mail->Password   = EMAIL_ADMIN_PASSWORDS;

                //$mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_NAME);
                $mail->AddAddress( "j.au@heni.com" , "Juliane Au");
                /*$mail->AddAddress( "joe@byorl.com" , "Joe Hage");*/

                $mail->SetFrom(EMAIL_ADMIN, EMAIL_ADMIN_NAME);
                $mail->Subject = "GR.com daily stats - ".$date_display;
                $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
                $mail->MsgHTML($html);
                $mail->Send();


?>
