<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/utils.php");

require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/gapi/gapi.class.php");
require_once ($_SERVER["DOCUMENT_ROOT"].'/includes/php_mailer/PHPMailerAutoload.php');

$db=new dbCLASS;


if( !empty($_GET['date']) )
{
        $time=strtotime($_GET['date']);
        $date_display=date("jS F", $time);
        $start_date=$_GET['date'];
        $end_date=$_GET['date'];
}
else
{
    $date_from_year=date("Y");
    $date_from_month=date("m");
    $date_from_day=date("d")-2;
    $date_display=date("jS F", mktime(0, 0, 0, $date_from_month, $date_from_day, $date_from_year));
    $start_date=date("Y-m-d",mktime(0, 0, 0, $date_from_month, $date_from_day, $date_from_year));

    $date_to_year=date("Y");
    $date_to_month=date("m");
    $date_to_day=date("d")-2;
    $end_date=date("Y-m-d",mktime(0, 0, 0, $date_to_month, $date_to_day, $date_to_year));
    //print $start_date." | ".$end_date;
    //$end_date=date("Y-m-d");
}

    $html="
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
    <title>Gerhard-Richter.com daily video report - ".$date_display."</title>
    <style type=\"text/css\">

    .title {
        margin: 20px 0 0 5px;
        width: 600px;
        text-align: center;
    }

    .item {
        color: #559DE6;
    }

    .item-artwork {
        color: #E86857;
    }

    .item-value {
        font-weight: bold;
    }

    .admin-option {
        width:646px;
        margin:0 0 0 5px;
    }

    .admin-option-h2 {
        background: #747474;
        font-weight: normal;
        padding: 8px;
        font-size: 1.1em;
        color: #fff;
        width: 540px;
    }

    .clear {
        clear: both;
    }

    .admin-option-small {
        width: 370px;
        margin: 0 0 0 5px;
        float: left;
    }

    .h1-orange {
        color: orange;
    }

    </style>
    </head>
        <body>";



/* Set your Google Analytics credentials */
define('ga_account'     ,'163175018466-itogrkgi8stbmva06n06jvql6ua693uk@developer.gserviceaccount.com');
define('ga_password'    ,'gerhard_richter_analytics-65f6037c5b5c.p12');
define('ga_profile_id'  ,'35533302');

$ga = new gapi(ga_account,ga_password);

/* We are using the 'source' dimension and the 'visits' metrics */
//$dimensions = array('source');
//$dimensions = array('pagepath','country');
$dimensions = array('pagepath');
$metrics    = array('uniquePageviews','avgTimeOnPage');
//$filter="pagePath=@overpainted-photographs/";


$filter="pagePath=@/videos/ && pagePath!@/search/";

//$filter="";

$html.="
            <h2 class='title'>Gerhard Richter videos Report: <span class='h1-orange'>".$date_display."</span></h2>
            <div class='admin-option'>
                <h2 class='admin-option-h2'>Daily site usage</h2>
                <table cellspacing='0' style='width: 540px'>
                <thead>
                <tr>
                <th>Image</th>
                <th>Info</th>
                <!--<th style='width: 130px;'>Country</th>-->
                <th>Avg. Time on Page</th>
                <th>Total Unique Visists</th>
                </tr>
                </thead>
    ";



/* We will sort the result be desending order of visits,
    and hence the '-' sign before the 'visits' string */
$ga->requestReportData(ga_profile_id, $dimensions, $metrics,'-uniquePageviews',$filter,$start_date,$end_date,1,240);

$gaResults = $ga->getResults();

$i=0;

$videos_array=array();
foreach($gaResults as $result)
{
    //print_r($result);
    $unique_pageviews=$result->getUniquePageviews();
    $avg_time_on_page=$result->getavgTimeOnPage();
    $page_path=$result->getPagePath();
    $mod_rewrite_vars=explode("/", $page_path);

    foreach ($mod_rewrite_vars as $key => $value)
    {
        if( empty($value) || preg_match("/\?p\=/i", $value) || preg_match("/\&sp\=/i", $value) || preg_match("/\?year\=/i", $value) )
        {
            unset($mod_rewrite_vars[$key]);
        }
    }
    $mod_rewrite_vars=array_values($mod_rewrite_vars);

    if( count($mod_rewrite_vars)==4 )
    {

        $tmp=explode("-", $mod_rewrite_vars[3]);
        $videoid=$tmp[(count($tmp)-1)];
        //print $page_path." : ".$unique_pageviews." : ".$videoid." : ".$avg_time_on_page."<br />";
        //$tmp=explode("-", $mod_rewrite_vars[count($mod_rewrite_vars)-1]);
        //if( !empty($tmp[(count($tmp)-1)]) )
        //{
            //$videoid=$tmp[(count($tmp)-1)];
            $videos_array[$videoid]['unique_page_views']=$videos_array[$videoid]['unique_page_views']+$unique_pageviews;
            $videos_array[$videoid]['avg_time_on_page']=$videos_array[$videoid]['avg_time_on_page']+$avg_time_on_page;
        //}
    }
    //print $page_path." : ".$unique_pageviews." : ".$videoid." : ".$avg_time_on_page."<br />";
}
arsort($videos_array);

foreach( $videos_array as $videoid => $video )
{
        $i++;
        $results_video=$db->query("SELECT * FROM ".TABLE_VIDEO." WHERE videoID='".$videoid."'");
        $row_video=$db->mysql_array($results_video);
        //$row_video=UTILS::html_decode($row_video);


        //$src="/includes/retrieve.image.php?videoID=".$row_video['videoID']."&size=m&thumb=1&border=1&maxwidth=100&maxheight=100";
        //$src="/datadir/images/video/medium/".$row_video['videoID'].".jpg";
        $src="https://www.gerhard-richter.com/datadir/images/video/medium/".$row_video['videoID'].".jpg";

        $values=array();
        $values['videoid']=$row_video['videoID'];
        $video_url=UTILS::get_video_url($db,$values);
        $url="https://www.gerhard-richter.com".$video_url['url'].".html";

        $html.="<tr>
                    <td style='width: 100px;border-bottom:1px solid #CCC;padding-bottom: 5px;'>
                        ".$i.".<br /><a href='".$url."' style='display:block;'>
                            <img src='".$src."' style='float:left;'  alt='Video ".$row_video['videoID']." IMG' />
                        </a>
                    </td>
                    <td style='padding-left: 10px;border-bottom:1px solid #CCC;'>
                    <span class='item'>";
            if( !empty($row_video['titleEN']) ) $html.=$row_video['titleEN'];
            if( !empty($row_video['name']) ) $html.="<br />".$row_video['name'];
            if( !empty($row_video['location_en']) ) $html.=$row_video['location_en'];
            $html.="<br />".$row_video['year'];
                    $html.="</span>
                        <br />
                    </td>
                    <td style='border-bottom:1px solid #CCC;text-align:center;'>".gmdate('H:i:s',$video['avg_time_on_page'])."</td>
                    <td style='border-bottom:1px solid #CCC;text-align:center;'>".$video['unique_page_views']."</td>
                    </tr>
                    <!--
                    <tr>
                     <td colspan='3'>".$page_path."</td>
                    </tr>
                    -->
                    ";


}






        $html.="
                    </table>
                </div>
            </body>
            </html>
        ";

print  $html;

                $mail = new PHPMailer();
                $mail->IsSMTP();
                //$mail->SMTPDebug  = 2;

                $mail->Host       = EMAIL_ADMIN_HOST;
                $mail->Port       = EMAIL_ADMIN_PORT;
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = "tls";
                $mail->Username   = EMAIL_ADMIN;
                $mail->Password   = EMAIL_ADMIN_PASSWORDS;

                //$mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_NAME);
                $mail->AddAddress( "j.au@heni.com" , "Juliane Au");
                /*$mail->AddAddress( "joe@byorl.com" , "Joe Hage");*/

                $mail->SetFrom(EMAIL_ADMIN, EMAIL_ADMIN_NAME);
                $mail->Subject = "GR.com daily video stats - ".$date_display;
                $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
                $mail->MsgHTML($html);
                $mail->Send();


?>
