<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="auctions_sales";
//$html->menu_admin_sub_selected="";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


if( empty($_GET['order_by']) || empty($_GET['order_how']) )
{
    $_GET['order_by']="saleDate";
    $_GET['order_how']="DESC";
} 


    if( !empty($_GET['sort_by']) && !empty($_GET['sort_how']) ) $query_order=" ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how'];
    else $query_order=" ORDER BY ".$_GET['order_by']." ".$_GET['order_how']." ";
    $query_limit=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];
    $query_sales=QUERIES::query_sale_history($db,$query_where_sales,$query_order,$query_limit);
    
    $results=$db->query($query_sales['query_without_limit']);
    $count=$db->numrows($results);
    $results_sales=$db->query($query_sales['query']);
    $pages=@ceil($count/$_GET['sp']);

    print "\t<h1>Sale history - ".$count."</h1><br />\n";

    if( $count>0 )
    {

        if( !empty($_GET['order_by']) && !empty($_GET['order_how']) ) $url="?order_by=".$_GET['order_by']."&order_how=".$_GET['order_how']."&p=";
        else $url="?p=";
        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);

        print "\t<table class='table_admin table_sale_history'>\n";
            print "\t<thead>\n";
                print "\t<tr>\n";
                    if( $_GET['sort_by']=='saleID' )
                    {   
                        if( $_GET['sort_how']=='asc' ) $sort='desc';
                        else $sort='asc';
                    }   
                    else $sort='asc';
                    print "<th><a href='?order_by=saleID&order_how=".$sort."' title='Order by sale ID'><u>id</u></a></th>";
                    print "\t<th>sale date</th>\n";
                    print "\t<th>auction house</th>\n";
                    print "\t<th>sale name</th>\n";
                    print "\t<th>lot<br />nr.</th>\n";
                    print "\t<th>estimate <br /> currency 1</th>\n";
                    print "\t<th>estimate <br /> currency 2</th>\n";
                    print "\t<th>sold for <br /> currency 1</th>\n";
                    print "\t<th>sold for <br /> currency 2</th>\n";
                    print "\t<th>sale<br />type</th>\n";
                print "\t</tr>\n";
            print "\t</thead>\n";

            while( $row=$db->mysql_array($results_sales) )
            {
                $row=UTILS::html_decode($row);

                $results_auction_house=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row['ahID']."'");
                $row_auction_house=$db->mysql_array($results_auction_house);
                $row_auction_house=UTILS::html_decode($row_auction_house);

                print "\t<tr>\n";
                    print "\t<td><a href='/admin/auctions-sales/saleHistory/edit/?saleid=".$row['saleID']."' title='Edit this sale' class='a_admin_edit'>".$row['saleID']."</a></td>\n";
                    print "\t<td>";
                        print $row['sale_date'];
                    print "</td>\n";
                    print "<td>\n";
                        print $row_auction_house['house'];
                    print "</td>\n";
                    print "\t<td>";
                        print $row['saleName'];
                    print "</td>\n";
                    print "\t<td>".$row['lotNo']."</td>\n";

                    #estimate
                    $currency1=UTILS::get_currency($db,$row['estCurrID']);
                    $estimate1="";
                    if( !empty($row['estLow']) ) $estimate1.=$currency1.number_format($row['estLow'],0);
                    if( !empty($row['estLow']) && !empty($row['estHigh']) ) $estimate1.="<br />-<br />";
                    if( !empty($row['estHigh']) ) $estimate1.=$currency1.number_format($row['estHigh'],0);
                    if( empty($estimate1) ) $estimate1="";
                    print "\t<td>".$estimate1."</td>\n";
                    $currency2=UTILS::get_currency($db,1);
                    $estimate2="";
                    if( !empty($row['estLowUSD']) ) $estimate2.=$currency2.number_format($row['estLowUSD'],0);
                    if( !empty($row['estLowUSD']) && !empty($row['estHighUSD']) ) $estimate2.="<br />-<br />";
                    if( !empty($row['estHighUSD']) ) $estimate2.=$currency2.number_format($row['estHighUSD'],0);
                    if( empty($estimate2) ) $estimate2="";
                    print "\t<td>".$estimate2."</td>\n";
                    # end estimate

                    #sold for
                    if( !empty($row['soldFor']) ) $sold_for1=UTILS::get_currency($db,$row['soldForCurrID']).number_format($row['soldFor'],0);
                    else $sold_for1="";
                    print "\t<td>".$sold_for1."</td>\n";
                    if( !empty($row['soldForUSD']) ) $sold_for2=UTILS::get_currency($db,1).number_format($row['soldForUSD'],0);
                    else $sold_for2="";
                    print "\t<td>".$sold_for2."</td>\n";
                    # end sold for

                    # sale type
                    if( $row['saleType']==1 ) $saleType="Premium";
                    elseif( $row['saleType']==2 ) $saleType="Hammer";
                    elseif( $row['saleType']==3 ) $saleType="Unknown";
                    else $saleType="";
                    print "\t<td>".$saleType."</td>\n";
                    # end sale type
                print "\t</tr>\n";
           
            }

        print "\t</table>\n";

        admin_html::page_numbering_admin($db,$pages,$_GET['p'],$_GET['sp'],$url,"",$class,true);
    }
    else
    {
        print "<p class='error'>No sale data found!</p>";
    }

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
