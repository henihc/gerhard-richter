<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['notes_admin']) ) $_SESSION['new_record']['error']['notes_admin']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/auctions-sales/saleHistory/edit/?saleid=".$_POST['saleid']."&error=true");

$db=new dbCLASS;


$notes_en=$db->db_prepare_input($_POST['notes_en'],1);
$notes_de=$db->db_prepare_input($_POST['notes_de'],1);
$notes_fr=$db->db_prepare_input($_POST['notes_fr'],1);
$notes_it=$db->db_prepare_input($_POST['notes_it'],1);
$notes_zh=$db->db_prepare_input($_POST['notes_zh'],1);
$news_auction_text_en=$db->db_prepare_input($_POST['news_auction_text_en'],1);
$news_auction_text_de=$db->db_prepare_input($_POST['news_auction_text_de'],1);
$news_auction_text_fr=$db->db_prepare_input($_POST['news_auction_text_fr'],1);
$news_auction_text_it=$db->db_prepare_input($_POST['news_auction_text_it'],1);
$news_auction_text_zh=$db->db_prepare_input($_POST['news_auction_text_zh'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST);

$lots = preg_replace('/\s+/', '', $_POST['lotNo']);
$lots=explode(",", $lots);

foreach ($lots as $lot_no) {

    if( $_POST['submit']=="add" )
    {
        $db->query("INSERT INTO ".TABLE_SALEHISTORY."(date_created) VALUES(NOW()) ");
        $saleid=$db->return_insert_id();
    }
    elseif( $_POST['submit']=="update" )
    {
        $saleid=$_POST['saleid'];
    }

    if( empty($_POST['saleDate']) ) $saleDate="NULL";
    else $saleDate="'".$_POST['saleDate']."'";

    $notes_admin=$db->db_replace_quotes($db,$_POST['notes_admin']);
    $notes_auction=$db->db_replace_quotes($db,$_POST['notes_auction']);
    $sale_name=$db->db_replace_quotes($db,$_POST['saleName']);

    $query="UPDATE ".TABLE_SALEHISTORY." SET
                ahID='".$_POST['ahID']."',
                saleDate=".$saleDate.",
                saleName='".$_POST['saleName']."',
                lotNo='".$lot_no."',
                upon_request='".$_POST['upon_request']."',
                tbannounced='".$_POST['tbannounced']."',
                estCurrID='".$_POST['estCurrID']."',
                estLow='".$_POST['estLow']."',
                estHigh='".$_POST['estHigh']."',
                estLowUSD='".$_POST['estLowUSD']."',
                estHighUSD='".$_POST['estHighUSD']."',
                soldForCurrID='".$_POST['soldForCurrID']."',
                soldFor='".$_POST['soldFor']."',
                soldForUSD='".$_POST['soldForUSD']."',
                boughtIn='".$_POST['boughtIn']."',
                withdrawn='".$_POST['withdrawn']."',
                notes_admin='".$notes_admin."',
                notes_auction='".$notes_auction."',
                saleName='".$sale_name."',
                sale_number='".$_POST['sale_number']."',
                saleType='".$_POST['saleType']."',
                number='".$_POST['number']."',
                news_auction='".$_POST['news_auction']."',
                notes_en='".$notes_en."',
                notes_de='".$notes_de."',
                notes_fr='".$notes_fr."',
                notes_it='".$notes_it."',
                notes_zh='".$notes_zh."',
                news_auction_text_en='".$news_auction_text_en."',
                news_auction_text_de='".$news_auction_text_de."',
                news_auction_text_fr='".$news_auction_text_fr."',
                news_auction_text_it='".$news_auction_text_it."',
                news_auction_text_zh='".$news_auction_text_zh."',
                enable='".$_POST['enable']."'";
                if( $_POST['submit']=="update" ) $query.=", date_modified=NOW()";

    $query.=" WHERE saleID='".$saleid."'";


    if( $_POST['submit']=="add" )
    {
        $db->query($query);

        admin_utils::admin_log($db,1,21,$saleid);
    }
    elseif( $_POST['submit']=="update" )
    {
        $db->query($query);

        admin_utils::admin_log($db,2,21,$saleid);
    }
    else
    {
        admin_utils::admin_log($db,"",21,$_POST['saleid'],2);
    }

}



if( $_POST['submit']=="add" || $_POST['submit']=="update" )
{
    # RELATION adding
    admin_utils::add_relation($db,2,$saleid,$_POST['relation_typeid'],$_POST['itemid']);
    #end RELATION adding
}

unset($_SESSION['new_record']);

$url="/admin/auctions-sales/saleHistory/edit/?saleid=".$saleid."&relation_typeid=".$_POST['relation_typeid']."&task=".$_POST['submit'];
if( !empty($_POST['relation_typeid']) && !empty($_POST['itemid']) ) $url.="#relations";
UTILS::redirect($url);
?>
