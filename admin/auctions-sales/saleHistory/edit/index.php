<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->css[]="/admin/js/autocompleter/autocompleter.css";
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";
$html->js[]="/admin/js/autocompleter/observer.js";
$html->js[]="/admin/js/autocompleter/autocompleter.js";
$html->js[]="/admin/js/autocompleter/autocompleter.request.js";
$html->js[]="/admin/js/autocompleter/autocompleter.load.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";

# calendar
$html->css[]="/admin/js/calendar/calendar.css";
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";
$html->js[]="/admin/js/calendar/datepicker.js";

# tabs
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# ckeditor
$html->js[]="/admin/js/ckeditor/ckeditor.js";
$html->js[]="/admin/js/ckeditor/ckeditor_config.js";

# menu selected
$html->menu_admin_selected="auctions_sales";
$html->menu_admin_sub_selected="add_sale";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();



print "\t<script type='text/javascript'>\n";
    print "\twindow.addEvent('domready', function() {";
        print "new DatePicker($$('.saleDate'), {  pickerClass: 'datepicker_dashboard', format: '%Y-%m-%d', toggleElements: '.date_toggler_start' });\n";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-notes li a','#div-admin-tabs-info-notes .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "var tabs = new MGFX.Tabs('#ul-admin-tabs-news li a','#div-admin-tabs-info-news .div-admin-tab-info',{";
            print "hash:false,";
            print "transitionDuration:0,";
            print "startIndex:0,";
            print "hover:true";
        print "});";
        print "\tCKEDITOR.replace( 'notes_en', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notes_de', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notes_fr', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notes_it', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'notes_zh', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'news_auction_text_en', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'news_auction_text_de', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'news_auction_text_fr', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'news_auction_text_it', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
        print "\tCKEDITOR.replace( 'news_auction_text_zh', { toolbar : 'Small', width : '700px', height : '200px' } );\n";
    print "\t});\n";
print "\t</script>\n";


    $query_where_sale=" WHERE saleID='".$_GET['saleid']."' ";
    $query_sale=QUERIES::query_sale_history($db,$query_where_sale,$query_order,$query_limit);
    $results=$db->query($query_sale['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results,0);
    if( $count>0 ) $row['saleid']=$row['saleID'];


    print "\t<form action='post.php' method='post' enctype='multipart/form-data'>\n";

        print "\t<table class='admin-table'>\n";

            if( !empty($_GET['task']) || !empty($_GET['error']) || $count>0 )
            {
                print "\t<tr>\n";
                    print "\t<td>";
                        if( $_GET['error'] ) print "<span class='error'>*Please complete mandatory fields</span>";
                        if( $_GET['task']=="update" ) print "<span class='valid'>Update successful!</span>";
                        if( $_GET['task']=="add" ) print "<span class='valid'>Insert successful!</span>";
                    print "\t</td>";
                    print "\t<td colspan='2' style='text-align:right;'>";
                        if( $count>0 )
                        {
                            $query_related_painting="SELECT
                                COALESCE(
                                    r1.itemid1,
                                    r2.itemid2
                                ) as paintid,
                                COALESCE(
                                    r1.sort,
                                    r2.sort
                                ) as sort_rel,
                                COALESCE(
                                    r1.relationid,
                                    r2.relationid
                                ) as relationid_rel
                                FROM
                                    ".TABLE_SALEHISTORY." s
                                LEFT JOIN
                                    ".TABLE_RELATIONS." r1 ON r1.typeid1 = 1 and r1.typeid2 = 2 and r1.itemid2 = s.saleID
                                LEFT JOIN
                                    ".TABLE_RELATIONS." r2 ON r2.typeid2 = 1 and r2.typeid1 = 2 and r2.itemid1 = s.saleID
                                WHERE s.saleID=".$row['saleID']."
                                ORDER BY sort_rel ASC, relationid_rel DESC
                                LIMIT 1
                            ";
                            $results_related_painting=$db->query($query_related_painting);
                            $count_related_painting=$db->numrows($results_related_painting);
                            if( $count_related_painting )
                            {
                                $row_related_painting=$db->mysql_array($results_related_painting);
                                $options_painting_url=array();
                                $options_painting_url['paintid']=$row_related_painting['paintid'];
                                $painting_url=UTILS::get_painting_url($db,$options_painting_url);
                                print "\t<a href='".$painting_url['url']."?&p=1&sp=32&tab=sales-history-tabs' title='View in page' class='a-view-on-page'><u>View in page</u></a>\n";
                            }
                        }
                    print "</td>\n";
                print "\t</tr>\n";
            }

            $tr_delete="";
            $tr_delete.="\t<tr>\n";
                $tr_delete.="\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/auctions-sales/saleHistory/edit/del.php?saleid=".$row['saleID']."&amp;paintid=".$row['paintID']."','".addslashes($row['saleName'])."','');\" title='delete this sale history record'><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/auctions-sales/saleHistory/edit/del.php?saleid=".$row['saleID']."&amp;paintid=".$row['paintID']."";
                        $options_delete['text1']="Sale history - ".$row['saleName'];
                        //$options_delete['text2']="";
                        $options_delete['html_return']=1;
                        $tr_delete.=admin_html::delete_button($db,$options_delete);
                    }
                    else $tr_delete.="* Mandatory";
                    $tr_delete.="<input type='reset' value='reset' />";
                $tr_delete.="</td>\n";
                if( $count ) $value="update";
                else $value="add";
                $tr_delete.="\t<td colspan='4'><input name='submit' type='submit' value='".$value."' /></td>\n";
            $tr_delete.="\t</tr>\n";

            print $tr_delete;

            if( !empty($row['date_modified_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date modified</th>\n";
                    print "\t<td colspan='4'>".$row['date_modified_admin']."</td>\n";
                print "\t</tr>\n";
            }

            if( !empty($row['date_created_admin']) )
            {
                print "\t<tr>\n";
                    print "\t<th class='th_align_left'>Date created</th>\n";
                    print "\t<td colspan='4'>".$row['date_created_admin']."</td>\n";
                print "\t</tr>\n";
            }

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Enable</th>\n";
                if( $_GET['error'] && !$_SESSION['new_record']['enable'] ) $checked="";
                elseif( !$row['enable'] && $count ) $checked="";
                else $checked="checked='checked'";
                print "\t<td colspan='4'><input type='checkbox' name='enable' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sale date</th>\n";
                if( $_GET['error'] ) $saleDate=$_SESSION['new_record']['saleDate'];
                else $saleDate=$row['sale_date_admin'];
                print "\t<td colspan='4'>";
                    print "\t<input type='text' class='saleDate dashboard' name='saleDate' id='saleDate' value='".$saleDate."' />";
                    print "\t<button type='button' class='calendar-icon date_toggler_start'></button>\n";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Painting Number(cr-45 etc.)</td>\n";
                print "\t<td colspan='4'>\n";
                    if( $_GET['error'] ) $number=$_SESSION['new_record']['number'];
                    else $number=$row['number'];
                    print "\t<textarea class='form' name='number' cols='98' rows='2'>".$number."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left' style='width:140px;'>Auction house</th>\n";
                print "\t<td colspan='4'>\n";
                    if( $_GET['error'] ) $ahid=$_SESSION['new_record']['ahID'];
                    else $ahid=$row['ahID'];
                    admin_html::select_auction_houses($db,"ahID",$ahid,$onchange,$class=array(),$disable);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sale name</th>\n";
                if( $_GET['error'] ) $sale_name=$_SESSION['new_record']['saleName'];
                else $sale_name=$row['saleName'];
                print "\t<td colspan='4'><input type='text' name='saleName' value='".$sale_name."' class='admin-input-large' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sale number</td>\n";
                print "\t<td colspan='4'>\n";
                    if( $_GET['error'] ) $sale_number=$_SESSION['new_record']['sale_number'];
                    else $sale_number=$row['sale_number'];
                    print "\t<textarea class='form' name='sale_number' cols='98' rows='2'>".$sale_number."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Lot No</th>\n";
                if( $_GET['error'] ) $lot_no=$_SESSION['new_record']['lotNo'];
                else $lot_no=$row['lotNo'];
                print "\t<td colspan='4'><input type='text' name='lotNo' id='lotNo' value='".$lot_no."' class='input_field_admin_large' /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Auction notes</td>\n";
                print "\t<td colspan='4'>\n";
                    if( $_GET['error'] ) $notes_auction=$_SESSION['new_record']['notes_auction'];
                    else $notes_auction=$row['notes_auction'];
                    print "\t<textarea name='notes_auction' cols='98' rows='1'>".$notes_auction."</textarea>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Estimate</th>\n";
                if( $_GET['error'] ) $estLow=$_SESSION['new_record']['estLow'];
                else $estLow=$row['estLow'];
                if( $_GET['error'] ) $estHigh=$_SESSION['new_record']['estHigh'];
                else $estHigh=$row['estHigh'];
                print "\t<td style='text-align:center;width:83px;'>";
                    if( $_GET['error'] ) $estCurrID=$_SESSION['new_record']['estCurrID'];
                    else $estCurrID=$row['estCurrID'];
                    $exclude=array();
                    $exclude[]=1;
                    admin_html::select_currencies($db,"estCurrID",$estCurrID,$onchange,$class=array(),$disable,$exclude);
                print "</td>\n";
                print "\t<td style='width:329px;'>";
                    print "<input type='text' name='estLow' id='estLow' value='".$estLow."' class='input_field_admin_medium' /> - ";
                    print "<input type='text' name='estHigh' id='estHigh' value='".$estHigh."' class='input_field_admin_medium' />";
                print "</td>\n";
                print "\t<th class='th_align_left' style='width:100px;'>Upon Request</th>\n";
                if( $_GET['error'] && $_SESSION['new_record']['upon_request'] ) $checked="checked='checked'";
                elseif( $row['upon_request'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td><input type='checkbox' name='upon_request' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Estimate USD</th>\n";
                if( $_GET['error'] ) $estLowUSD=$_SESSION['new_record']['estLowUSD'];
                else $estLowUSD=$row['estLowUSD'];
                if( $_GET['error'] ) $estHighUSD=$_SESSION['new_record']['estHighUSD'];
                else $estHighUSD=$row['estHighUSD'];
                print "\t<td style='text-align:center;'>";
                    admin_html::select_currencies($db,"estCurrID",1,$onchange,$class=array(),"disabled='disabled'");
                print "</td>\n";
                print "\t<td style='width:329px;'>";
                    print "<input type='text' name='estLowUSD' id='estLowUSD' value='".$estLowUSD."' class='input_field_admin_medium' /> - ";
                    print "<input type='text' name='estHighUSD' id='estHighUSD' value='".$estHighUSD."' class='input_field_admin_medium' />";
                print "</td>\n";
                print "\t<th class='th_align_left' style='width:100px;'>To be announced</th>\n";
                if( $_GET['error'] && $_SESSION['new_record']['tbannounced'] ) $checked="checked='checked'";
                elseif( $row['tbannounced'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td><input type='checkbox' name='tbannounced' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sold for</th>\n";
                if( $_GET['error'] ) $soldFor=$_SESSION['new_record']['soldFor'];
                else $soldFor=$row['soldFor'];
                if( $_GET['error'] ) $soldForCurrID=$_SESSION['new_record']['soldForCurrID'];
                else $soldForCurrID=$row['soldForCurrID'];
                print "\t<td style='text-align:center;'>";
                    admin_html::select_currencies($db,"soldForCurrID",$soldForCurrID,$onchange,$class=array(),"",$exclude);
                print "</td>\n";
                print "\t<td colspan='3'>";
                    print "<input type='text' name='soldFor' id='soldFor' value='".$soldFor."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";


            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sold for USD</th>\n";
                if( $_GET['error'] ) $soldForUSD=$_SESSION['new_record']['soldForUSD'];
                else $soldForUSD=$row['soldForUSD'];
                print "\t<td style='text-align:center;'>";
                    admin_html::select_currencies($db,"estCurrID",1,$onchange,$class=array(),"disabled='disabled'");
                print "</td>\n";
                print "\t<td colspan='3'>";
                    print "<input type='text' name='soldForUSD' id='soldForUSD' value='".$soldForUSD."' class='input_field_admin_medium' />";
                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Sale type</th>\n";
                print "\t<td colspan='4'>\n";
                    if( $_GET['error'] ) $saleType=$_SESSION['new_record']['saleType'];
                    else $saleType=$row['saleType'];
                    admin_html::select_sale_type($db,"saleType",$saleType,$onchange,$class=array(),$disable);
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Bought in</th>\n";
                if( $_GET['error'] && $_SESSION['new_record']['boughtIn'] ) $checked="checked='checked'";
                elseif( $row['boughtIn'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td colspan='4'><input type='checkbox' name='boughtIn' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Withdrawn</th>\n";
                if( $_GET['error'] && $_SESSION['new_record']['withdrawn'] ) $checked="checked='checked'";
                elseif( $row['withdrawn'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td colspan='4'><input type='checkbox' name='withdrawn' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>News auction</th>\n";
                if( $_GET['error'] && $_SESSION['new_record']['news_auction'] ) $checked="checked='checked'";
                elseif( $row['news_auction'] ) $checked="checked='checked'";
                else $checked="";
                print "\t<td colspan='4'><input type='checkbox' name='news_auction' value='1' $checked /></td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Notes</th>\n";
                print "\t<td colspan='4'>";

                    print "\t<ul id='ul-admin-tabs-notes' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-notes' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notes_en=$_SESSION['new_record']['notes_en'];
                            else $notes_en=$row['notes_en'];
                            print "\t<textarea name='notes_en' id='notes_en' cols='90' rows='10'>".$notes_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notes_de=$_SESSION['new_record']['notes_de'];
                            else $notes_de=$row['notes_de'];
                            print "\t<textarea name='notes_de' id='notes_de' cols='90' rows='10'>".$notes_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notes_fr=$_SESSION['new_record']['notes_fr'];
                            else $notes_fr=$row['notes_fr'];
                            print "\t<textarea name='notes_fr' id='notes_fr' cols='90' rows='10'>".$notes_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notes_it=$_SESSION['new_record']['notes_it'];
                            else $notes_it=$row['notes_it'];
                            print "\t<textarea name='notes_it' id='notes_it' cols='90' rows='10'>".$notes_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $notes_zh=$_SESSION['new_record']['notes_zh'];
                            else $notes_zh=$row['notes_zh'];
                            print "\t<textarea name='notes_zh' id='notes_zh' cols='90' rows='10'>".$notes_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>News auction text</th>\n";
                print "\t<td colspan='4'>";

                    print "\t<ul id='ul-admin-tabs-news' class='ul-admin-tabs'>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >English</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >German</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >French</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Italian</a></li>\n";
                        print "\t<li><a class='li-admin-tab' href='#' >Chinese</a></li>\n";
                    print "\t</ul>\n";
                    print "\t<div id='div-admin-tabs-info-news' class='div-admin-tabs-info'>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $news_auction_text_en=$_SESSION['new_record']['news_auction_text_en'];
                            else $news_auction_text_en=$row['news_auction_text_en'];
                            print "\t<textarea name='news_auction_text_en' id='news_auction_text_en' cols='90' rows='10'>".$news_auction_text_en."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $news_auction_text_de=$_SESSION['new_record']['news_auction_text_de'];
                            else $news_auction_text_de=$row['news_auction_text_de'];
                            print "\t<textarea name='news_auction_text_de' id='news_auction_text_de' cols='90' rows='10'>".$news_auction_text_de."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $news_auction_text_fr=$_SESSION['new_record']['news_auction_text_fr'];
                            else $news_auction_text_fr=$row['news_auction_text_fr'];
                            print "\t<textarea name='news_auction_text_fr' id='news_auction_text_fr' cols='90' rows='10'>".$news_auction_text_fr."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $news_auction_text_it=$_SESSION['new_record']['news_auction_text_it'];
                            else $news_auction_text_it=$row['news_auction_text_it'];
                            print "\t<textarea name='news_auction_text_it' id='news_auction_text_it' cols='90' rows='10'>".$news_auction_text_it."</textarea>\n";
                        print "\t</div>\n";
                        print "\t<div class='div-admin-tab-info'>\n";
                            if( $_GET['error'] ) $news_auction_text_zh=$_SESSION['new_record']['news_auction_text_zh'];
                            else $news_auction_text_zh=$row['news_auction_text_zh'];
                            print "\t<textarea name='news_auction_text_zh' id='news_auction_text_zh' cols='90' rows='10'>".$news_auction_text_zh."</textarea>\n";
                        print "\t</div>\n";
                    print "\t</div>\n";

                print "</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Relation</td>\n";
                print "\t<td colspan='5'>\n";
                    print admin_html::select_relations($db,"relation_typeid",$_GET['relation_typeid'],$onchange,"",$disable);
                    print "&nbsp;";
                    print "\t<input type='text' name='itemid' id='itemid' value='".$_GET['paintid']."' class='input-relation-itemid' />\n";
                    print "&nbsp;";
                    print "<img src='/g/loading.gif' class='autocompleter-loading' id='loading' />";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['notes_admin'] ) $class="error";
                else $class="";
                print "\t<th class='".$class." th_align_left'>*Admin notes</th>\n";
                $row['notes_admin']=UTILS::html_decode($row['notes_admin']);
                if( $_GET['error'] ) $notes_admin=$_SESSION['new_record']['notes_admin'];
                else $notes_admin=$row['notes_admin'];
                print "\t<td colspan='4'><textarea rows='7' cols='99' name='notes_admin'>".$notes_admin."</textarea></td>\n";
            print "\t</tr>\n";

            print $tr_delete;

        print "\t</table>\n";

        print "\t<input name='saleid' type='hidden' value='".$row['saleID']."' />\n";
        print "\t<input name='p' type='hidden' value='".$_GET['p']."' />\n";

    print "\t</form>\n";

    # printing out relations
    admin_html::show_relations($db,2,$row['saleID']);
    #end


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
unset($_SESSION['new_record']);
?>
