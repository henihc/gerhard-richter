<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$_SESSION['new_record']=$_POST;
if( empty($_POST['house']) ) $_SESSION['new_record']['error']['house']=true;
if( count($_SESSION['new_record']['error'])>0 ) UTILS::redirect("/admin/auctions-sales/auctionhouse/edit/?auctionhouseid=".$_POST['auctionhouseid']."&error=true");

$db=new dbCLASS;

$house=$db->db_prepare_input($_POST['house'],1);

$_POST=UTILS::html_decode($_POST);
$_POST=$db->db_prepare_input($_POST,1);

# prepare search values
$values_accented=array();
$values_accented['text']=$_POST['house'];
$house_search=UTILS::replace_accented_letters($db, $values_accented);
# END prepare search values

if( $_POST['submit']=="add" )
{
    $db->query("INSERT INTO ".TABLE_AUCTIONHOUSE."(house) VALUES(NULL)");
    $auctionhouseid=$db->return_insert_id();

    admin_utils::admin_log($db,1,22,$auctionhouseid);
}
elseif( $_POST['submit']=="update" )
{
    $auctionhouseid=$_POST['auctionhouseid'];

    admin_utils::admin_log($db,2,22,$auctionhouseid);
}
else
{
	admin_utils::admin_log($db,"",22,$_POST['auctionhouseid'],2);
}

$query="UPDATE ".TABLE_AUCTIONHOUSE." SET 
        house='".$house."',
        house_search_1='".$house_search['text_lower_converted']."',
        house_search_2='".$house_search['text_lower_german_converted']."',
        cityid='".$_POST['cityid']."',
        countryid='".$_POST['countryid']."'
WHERE ahID='".$auctionhouseid."'";
$db->query($query);

unset($_SESSION['new_record']);

UTILS::redirect("/admin/auctions-sales/auctionhouse/edit/?auctionhouseid=".$auctionhouseid."&task=update");
?>
