<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# autocompleter
$html->js[]="/admin/js/autocompleter/mootools-1.2.5-core.js";

# calendar
$html->js[]="/admin/js/calendar/mootools-1.2.5.1-more.js";

# tabs
$html->js[]="/admin/js/tabs/mootools-tabs-admin.js";
$html->js[]="/admin/js/tabs/rotater.js";
$html->js[]="/admin/js/tabs/tabs.js";

# menu selected
$html->menu_admin_selected="auctions_sales";
$html->menu_admin_sub_selected="add_auction_house";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();




$db=new dbCLASS;


$_GET=$db->db_prepare_input($_GET);

$results=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$_GET['auctionhouseid']."'");
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);



    print "\t<form action='post.php' method='post'>\n";
        print "\t<table class='admin-table' cellspacing='0'>\n";

            $tr_delete="";
            $tr_delete.="\t<tr>\n";
                $tr_delete.="\t<td>";
                    if( $count )
                    {
                        //print "<a href='#' onclick=\"delete_confirm2('/admin/auctions-sales/saleHistory/edit/del.php?saleid=".$row['saleID']."&amp;paintid=".$row['paintID']."','".addslashes($row['saleName'])."','');\" title='delete this sale history record'><u>delete</u></a>";
                        $options_delete=array();
                        $options_delete['url_del']="/admin/auctions-sales/auctionhouse/edit/del.php?auctionhouseid=".$row['ahID']."";
                        $options_delete['text1']="Auction house - ".$row['house'];
                        //$options_delete['text2']="";
                        $options_delete['html_return']=1;
                        $tr_delete.=admin_html::delete_button($db,$options_delete);
                    }
                    else $tr_delete.="* Mandatory";
                    $tr_delete.="<input type='reset' value='reset' />"; 
                $tr_delete.="</td>\n";
                if( $count ) $value="update";
                else $value="add";
                $tr_delete.="\t<td colspan='4'><input name='submit' type='submit' value='".$value."' /></td>\n";
            $tr_delete.="\t</tr>\n";

            print $tr_delete;

            print "\t<tr>\n";
                if( $_SESSION['new_record']['error']['house'] ) $class="error";
                else $class="";
                print "\t<th class='".$class."' style='text-align:left;'>*Title</th>\n";
                if( $_GET['error'] ) $house=$_SESSION['new_record']['house'];
                else $house=$row['house'];
                print "\t<td>";
                    print "\t<textarea name='house' id='house' cols='90' rows='2'>".$house."</textarea>";
                print "\t</td>\n";
            print "\t</tr>\n";

            print "\t<tr>\n";
                print "\t<th class='th_align_left'>Location</td>\n";
                print "\t<td colspan='2'>\n";
                    print "\t<table class='admin-table' cellspacing='0'>\n";
                        print "\t<tr>\n";
                            print "\t<th class='th_align_left'>Country</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="countryid";
                                $values['selectedid']=$row['countryid'];
                                $values['onchange']=" onchange=\"
                                                        $('cityid').load('/admin/locations/cities/options_cities.php?countryid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-city').style.display='';\" ";
                                admin_html::select_locations_country($db,$values);
                                if( $count ) UTILS::admin_edit("/admin/locations/edit/?typeid=3&countryid=".$row['countryid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";

                        if( empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-city' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>City</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="cityid";
                                $values['selectedid']=$row['cityid'];
                                /*
                                $values['onchange']=" onchange=\"
                                                        $('locationid').load('/admin/locations/options_locations.php?countryid='+document.getElementById('countryid').options[document.getElementById('countryid').selectedIndex].value+'&cityid='+this.options[selectedIndex].value);
                                                        document.getElementById('tr-location').style.display='';\" ";
                                */
                                $values['where']=" WHERE countryid='".$row['countryid']."' ";
                                admin_html::select_locations_city($db,$values);
                                if( $count && !empty($row['cityid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=2&cityid=".$row['cityid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";
                        /*
                        if( empty($row['cityid']) && empty($row['countryid']) ) $style="display:none;";
                        else $style="";
                        print "\t<tr id='tr-location' style='".$style."'>\n";
                            print "\t<th class='th_align_left'>Location</th>\n";
                            print "\t<td>\n";
                                $values=array();
                                $values['name']="locationid";
                                $values['selectedid']=$row['locationid'];
                                $values['where']=" WHERE countryid='".$row['countryid']."' AND cityid='".$row['cityid']."' ";
                                admin_html::select_locations($db,$values);
                                if( $count && !empty($row['locationid']) ) UTILS::admin_edit("/admin/locations/edit/?typeid=1&locationid=".$row['locationid'])."\n";
                            print "\t</td>\n";
                        print "\t</tr>\n";
                        */
                    print "\t</table>\n";
                print "\t</td>\n";
            print "\t</tr>\n";

            if( $count )
            {
                print "\t<tr>\n";
                    print "\t<th class='' style='text-align:left;'>Display</th>\n";
                    print "\t<td>";
                        $values_location=array();
                        $values_location['row']=$row;
                        $location=location($db,$values_location);
                        print $house.", ".$location;
                    print "\t</td>\n";
                print "\t</tr>\n";
            }

            print $tr_delete;

        print "\t</table>\n";
        print "\t<input type='hidden' name='auctionhouseid' value='".$row['ahID']."' />\n";
    print "\t</form>\n";

    $query_sale_history="SELECT a.house AS auction_house, s.saleID AS saleID,s.saleName AS sale_name, s.saleName AS title_delete, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.ahID='".$row['ahID']."'";
    $results_used=$db->query($query_sale_history);
    $count_used=$db->numrows($results_used);

    if( $count_used>0 && $count )
    {
        print "\t<ul class='tabsadmin' id='ul_link_tabs'>\n";
            print "\t<li class='Images tab'>\n";
                print "\t<h4 style='top: 0px;'><a class='Sales history' name='Sales history' href='#Sales history'>Sales history</a></h4>\n";
            print "\t</li>\n";
        print "\t</ul>\n";
        print "\t<br class='clear'>\n";
        print "\t<div class='tabsadmin-info'>\n";
            print "\t<div id='' class='holder Colors-info hide'>\n";
                print "\t&nbsp;items found: ".$count_used."\n";
                print "\t<table class='table-links' style=\"width:900px;\">\n";
                    print "\t<tbody>\n";
                        print "\t<tr>\n";
                            print "\t<th style='width:70px;text-align:center;'>saleid</th>\n";
                            print "\t<th style=''>sale date</th>\n";
                            //print "\t<th style=''>auction house</th>\n";
                            print "\t<th style=''>sale name</th>\n";
                            print "\t<th style=''>lot nr.</th>\n";
                            print "\t<th style=''>estimate currency 1</th>\n";
                            print "\t<th style=''>estimate currency 2</th>\n";
                            print "\t<th style=''>sold for currency 1</th>\n";
                            print "\t<th style=''>sold for currency 2</th>\n";
                            print "\t<th style=''>sale type</th>\n";
                        print "\t</tr>\n";
                        while( $row_used=$db->mysql_array($results_used,0) )
                        {
                            print "\t<tr>\n";
                                print "\t<td style='width:70px;text-align:center;'>\n";
                                    print "\t<a title='edit this item' href='/admin/auctions-sales/saleHistory/edit/?saleid=".$row_used['saleID']."'><u>".$row_used['saleID']."</u></a>\n";
                                print "\t</td>\n";
                                print "\t<td style='text-align:left;'>\n";
                                    print $row_used['sale_date'];
                                print "\t</td>\n";
                                //print "\t<td style=''>\n";
                                    //print $row_used['auction_house'];
                                //print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    print $row_used['sale_name'];
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    print $row_used['lot_nr.'];
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    $currency1=UTILS::get_currency($db,$row_used['estCurrID']);
                                    $estimate1="";
                                    if( !empty($row_used['estLow']) ) $estimate1=$currency1.number_format($row_used['estLow'],0);
                                    if( !empty($row_used['estLow']) && !empty($row_used['estHigh']) ) $estimate1.="<br />-<br />";
                                    if( !empty($row_used['estHigh']) ) $estimate1.=$currency1.number_format($row_used['estHigh'],0);
                                    if( empty($estimate1) ) $estimate1="&nbsp;";
                                    print $estimate1;
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    $currency2=UTILS::get_currency($db,1);
                                    $estimate2="";
                                    if( !empty($row_used['estLowUSD']) ) $estimate2=$currency2.number_format($row_used['estLowUSD'],0);
                                    if( !empty($row_used['estLowUSD']) && !empty($row_used['estHighUSD']) ) $estimate2.="<br />-<br />";
                                    if( !empty($row_used['estHighUSD']) ) $estimate2.=$currency2.number_format($row_used['estHighUSD'],0);
                                    if( empty($estimate2) ) $estimate2="&nbsp;";
                                    print $estimate2;
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    print $row_used['sold_for_currency_1'];
                                    if( !empty($row_used['soldFor']) ) $sold_for1=UTILS::get_currency($db,$row_used['soldForCurrID']).number_format($row_used['soldFor'],0);
                                    else $sold_for1="&nbsp;";
                                    print $sold_for1;
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    if( !empty($row_used['soldForUSD']) ) $sold_for2=UTILS::get_currency($db,1).number_format($row_used['soldForUSD'],0);
                                    else $sold_for2="&nbsp;";
                                    print $sold_for2;
                                print "\t</td>\n";
                                print "\t<td style=''>\n";
                                    if( $row_used['sale_type']==1 ) $sale_type="Premium";
                                    elseif( $row_used['sale_type']==2 ) $sale_type="Hammer";
                                    elseif( $row_used['sale_type']==3 ) $sale_type="Unknown";
                                    else $sale_type="";
                                    print $sale_type;
                                print "\t</td>\n";
                            print "\t</tr>\n";
                        }
                    print "\t</tbody>\n";
                print "\t</table>\n";
            print "\t</div>\n";
        print "\t</div>\n";
    }

        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();

unset($_SESSION['new_record']);
?>
