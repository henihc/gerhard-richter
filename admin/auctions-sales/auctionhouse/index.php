<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/admin/inc/html.php");

admin_html::admin_sign_in();

$db=new dbCLASS;


$html = new admin_html;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( $_GET['sp']!="all" ) $options_get['get']['sp']=40;
$_GET=admin_utils::if_empty_get($db,$options_get);

$html = new admin_html;
$html->title=DOMAIN." - Admin";

# js & css files
$html->css[]="/admin/css/admin.css";
$html->css[]="/admin/css/menu.css";
$html->js[]="/admin/js/common.js";

# menu selected
$html->menu_admin_selected="auctions_sales";
$html->menu_admin_sub_selected="auction_houses";

$html->metainfo();

$html->div_admin_container_start();
$html->div_admin_wrapper_start();
    $html->div_admin_header_start();
        $html->div_admin_header_content_start();
        $html->div_admin_header_content_end();
        $html->menu_admin($db,"ul-menu-nav");
    $html->div_admin_header_end();
    $html->div_admin_body_start();
        $html->div_admin_body_content_start();


$var_array=UTILS::url_var($db,$_SERVER["QUERY_STRING"]);

if( empty($_GET['sort_by']) || empty($_GET['sort_how']) )
{
    $_GET['sort_by']="ahID";
    $_GET['sort_how']="asc";
} 


echo "<table class='admin-table'><tr>";

//1
if($_GET['sort_by']=='ahID')
{
    if( $_GET['sort_how']=='asc' ) $sort='desc';
    else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=ahID&sort_how=$sort' title='Order by ahID'><u>ahID</u></a></strong></th>";
//3
if($_GET['sort_by']=='house')
{
    if( $_GET['sort_how']=='asc' )$sort='desc';
    else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=house&sort_how=$sort' title='Order by house'><u>house</u></a></strong></th>";
//city
if($_GET['sort_by']=='cityid')
{
    if( $_GET['sort_how']=='asc' )$sort='desc';
    else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=cityid&sort_how=$sort' title='Order by city'><u>city</u></a></strong></th>";
//country
if($_GET['sort_by']=='countryid')
{
    if( $_GET['sort_how']=='asc' )$sort='desc';
    else $sort='asc';
}
else $sort='asc';
echo "<th><strong><a href='?sort_by=countryid&sort_how=$sort' title='Order by country'><u>country</u></a></strong></th>";
//used
echo "<th><strong>used</strong></th>";



echo "</tr>\n";



$query=$db->query("SELECT * FROM ".TABLE_AUCTIONHOUSE." ORDER BY ".$_GET['sort_by']." ".$_GET['sort_how']);
while($row=$db->mysql_array($query)){
    $row=UTILS::html_decode($row);

    $query_sale_history="SELECT a.house AS auction_house,a.cityid AS acityid,a.countryid AS acountryid, s.saleID AS saleID,s.saleName AS sale_name, s.saleName AS title_delete, DATE_FORMAT(s.saleDate ,'%D %M %Y') AS sale_date, s.lotNo AS `lot_nr.`, s.estCurrID, s.estLow, s.estHigh, s.estLowUSD, s.estHighUSD, s.soldForCurrID, s.soldFor, s.soldForUSD, s.saleType AS sale_type FROM ".TABLE_SALEHISTORY." s, ".TABLE_AUCTIONHOUSE." a WHERE a.ahID=s.ahID AND s.ahID='".$row['ahID']."'";
    $results_used=$db->query($query_sale_history);
    $count_used=$db->numrows($results_used);

//if( !$count_used ) $db->query("DELETE FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row['ahID']."' ");

echo "
<tr>
<td><a href='/admin/auctions-sales/auctionhouse/edit/?auctionhouseid=".$row['ahID']."' style='text-decoration:underline;color:blue;' title='edit this category'>".$row['ahID']."</a></td>";


echo "
    <td>";
    print $row['house'];
    print "</td>";
    $values_location=array();
    $values_location['row']=$row;
    $values_location['return_array']=1;
    $location=location($db,$values_location);
print "<td>".$location['city']."</td>";
print "<td>".$location['country']."</td>";
print "<td>".$count_used."</td>";

echo "</tr>\n";

}
echo "</table>\n";


        $html->div_admin_body_content_end();
    $html->div_admin_body_end();
    $html->div_admin_footer_start();
    $html->div_admin_footer_end();
$html->div_admin_wrapper_end();
$html->div_admin_container_end();

$html->foot();
?>
