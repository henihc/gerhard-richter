<?php

function redirect($url,$type="")
{   
    session_write_close();

    switch( $type )
    {   
        // 301 Moved Permanently
        case 301 : header("Location: ".$url,TRUE,$type);break;
        // 302 Found
        case 302 : header("Location: ".$url,TRUE,$type);
                   header("Location: ".$url); 
                   break;
        // 303 See Other
        case 303 : header("Location: ".$url,TRUE,$type);break;
        // 307 Temporary Redirect
        case 307 : header("Location: ".$url,TRUE,$type);break;
        default :  header("Location: ".$url);
    }   

    exit;
} 

redirect("https://sixstops.6stops.com:2096/",301);

?>