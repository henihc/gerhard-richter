<?php
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");

# COUNTRY LOCALE LANGUAGE
$browser_language = get_browsers_default_language($_SERVER['HTTP_ACCEPT_LANGUAGE']);

if( !empty($_GET['lang']) ) $browser_language=$_GET['lang'];

$options_lang_file=array();
$options_lang_file['get_lang']=$browser_language;
$language=UTILS::load_lang_file($db,$options_lang_file);
# END COUNTRY LOCALE LANGUAGE

?>
$(document).ready(function(){
    $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none',
        loop : false,
        closeBtn  : false,
        helpers : {
            title : {
                type : 'inside'
            },
            buttons : {}
        },
        afterLoad : function() {
            var link_url=$(this.element).find('img').attr('alt');
            window.link_url=$(this.element).find('img').attr('alt');
            if( link_url!='' )
            {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + '<div id="div-fancybox-title-path" class="div-fancybox-title-path"><a class="a-copy-to-clipboard" onclick="copyTextToClipboard(link_url);" ><?php print LANG_COPY_TO_CLIPBOARD; ?></a></div><div class="clearer"></div>';
            }
            else
            {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' );
            }
        }
    });
    //$('#test').trigger('click');
});
