$(document).ready(function() {
    $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none',
        loop : false,
        closeBtn  : false,
        helpers : {
            title : {
                type : 'inside'
            },
            buttons : {}
        },
        afterLoad : function() {
            attr_alt=$(this.element).find('img').attr('alt');
            if( typeof attr_alt === 'undefined' ) attr_alt="";
            //this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + '<div class="div-fancybox-title-path">' + attr_alt + '</div>';
        }
    });
    //$('#test').trigger('click');
});
