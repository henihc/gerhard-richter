$(document).ready(function() {
    $('.fancybox-html').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none',
        loop : false,
        closeBtn  : true,
        type : 'iframe',
        beforeLoad : function() {         
            this.width  = parseInt(this.element.data('fancybox-width'));  
            this.height = parseInt(this.element.data('fancybox-height'));
        }
    });
    //$('#test').trigger('click');
});
