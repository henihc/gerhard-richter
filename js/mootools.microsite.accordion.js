window.addEvent('domready', function(){

	var hook = $$('a.about-hook');

	var accordion = new Accordion(hook,'div.accordion',{
		start:'all-closed',
		alwaysHide:true,
		onActive: function(){
			hook.setStyle('background-position', 'right -236px');
		},
		onBackground: function(){
			hook.setStyle('background-position', 'right 3px');
		}
	}, $('about'));

});