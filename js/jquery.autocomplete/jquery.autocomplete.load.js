/* allows to focus coursor at the end of input field */
$.fn.setCursorPosition = function(position){
    if(this.length == 0) return this;
    return $(this).setSelection(position, position);
}

$.fn.setSelection = function(selectionStart, selectionEnd) {
    if(this.length == 0) return this;
    input = this[0];

    if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    } else if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }

    return this;
}

$.fn.focusEnd = function(){
    this.setCursorPosition(this.val().length);
            return this;
}

/* autocompleter function load */
$(function() {
    $("#input-basic-search").autocomplete('/includes/search/autocomplete.php?output=json', {
        remoteDataType: 'json',
        processData: function(data) {
            var i, processed = [];
            for (i=0; i < data.length; i++) {
                //processed.push([data[i][0] + " - " + data[i][1]]);
                processed.push([data[i][0]]);
            }
            return processed;
        },
        onItemSelect: function(item) {
            var val = item.value;
            //pageurl = "/search/?search="+encodeURIComponent(val);
            pageurl = "/search/?search="+encodeURI(val);
            $('#div-search-loader').html("<img src='/g/loading.gif' alt='' class='img-search-loader' />");
            //$('#div-basic-search-results').load('search.php?search='+encodeURIComponent(val), function() {
            $('#div-basic-search-results').load('/includes/search/search.php?search='+encodeURI(val), function() {
                $('#div-search-loader').html("");
            });
            //to change the browser URL to 'pageurl'
            if( pageurl!=window.location && val!='' ) 
            {
                var lang=$('.lang-on').data('lang');
                window.history.pushState({path:'/'+lang+pageurl},'','/'+lang+pageurl);
                //console.debug('1-'+lang);
                for (i = 0; i < $('#language a').length; i++) {
                    $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                };
            }
            else 
            {
                var lang=$('.lang-on').data('lang');
                window.history.pushState({path:'/'+lang+'/search'},'','/'+lang+'/search');
                //console.debug('2-'+lang);
                for (i = 0; i < $('#language a').length; i++) {
                    $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                };
            }
            return false;
        }
    });

$( "#button-submit-basic-search" ).click(function() {
    var val = $("#input-basic-search")[0].value;
    //console.debug(val.length);
    if( val.length>0 )
    {
        //pageurl = "/search/?search="+encodeURIComponent(val);
        pageurl = "/search/?search="+encodeURI(val);
        $('#div-search-loader').html("<img src='/g/loading.gif' alt='' class='img-search-loader' />");
        //$('#div-basic-search-results').load('search.php?search='+encodeURIComponent(val), function() {
        $('#div-basic-search-results').load('/includes/search/search.php?search='+encodeURI(val), function() {
            $('#div-search-loader').html("");
        });
        //to change the browser URL to 'pageurl'
        if( pageurl!=window.location && val!='' ) 
        {
            var lang=$('.lang-on').data('lang');
            window.history.pushState({path:'/'+lang+pageurl},'','/'+lang+pageurl);
            //console.debug('3-'+lang);
            for (i = 0; i < $('#language a').length; i++) {
                $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
            };
        }
        else 
        {
            var lang=$('.lang-on').data('lang');
            window.history.pushState({path:'/'+lang+'/search'},'','/'+lang+'/search');
            //console.debug('4-'+lang);
            for (i = 0; i < $('#language a').length; i++) {
                $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
            };
        }
    }
});

    $(function() {
      var timer;
      $("#input-basic-search").keypress(function(event) {
        if ( event.keyCode == 13 ) 
        {
            var val = this.value;
            //pageurl = "/search/?search="+encodeURIComponent(val);
            pageurl = "/search/?search="+encodeURI(val);
            $('#div-search-loader').html("<img src='/g/loading.gif' alt='' class='img-search-loader' />");
            //$('#div-basic-search-results').load('search.php?search='+encodeURIComponent(val), function() {
            $('#div-basic-search-results').load('/includes/search/search.php?search='+encodeURI(val), function() {
                $('#div-search-loader').html("");
            });
            //to change the browser URL to 'pageurl'
            if( pageurl!=window.location && val!='' ) 
            {
                var lang=$('.lang-on').data('lang');
                window.history.pushState({path:'/'+lang+pageurl},'','/'+lang+pageurl);
                //console.debug('5-'+lang);
                //console.debug($('.lang-on'));
                for (i = 0; i < $('#language a').length; i++) {
                    $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                };
            }
            else 
            {
                var lang=$('.lang-on').data('lang');
                window.history.pushState({path:'/'+lang+'/search'},'','/'+lang+'/search');
                //console.debug('6-'+lang);
                for (i = 0; i < $('#language a').length; i++) {
                    $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                };
            }
            return false;
        }
        else
        {
            clearTimeout(timer);
            $('#div-search-loader').html("<img src='/g/loading.gif' alt='' class='img-search-loader' />");
            //var ms = 700; // milliseconds
            var ms = 2000; // milliseconds
            timer = setTimeout(function() {
                var val = document.getElementById("input-basic-search").value;
                //pageurl = "/search/?search="+encodeURIComponent(val);
                pageurl = "/search/?search="+encodeURI(val);
                //$('#div-basic-search-results').load('search.php?search='+encodeURIComponent(val), function() {
                $('#div-basic-search-results').load('/includes/search/search.php?search='+encodeURI(val), function() {
                    $('#div-search-loader').html("");
                    //to change the browser URL to 'pageurl'
                    if( pageurl!=window.location && val!='' ) 
                    {
                        var lang=$('.lang-on').data('lang');
                        window.history.pushState({path:'/'+lang+pageurl},'','/'+lang+pageurl);    
                        //console.debug('7-'+lang);
                        for (i = 0; i < $('#language a').length; i++) {
                            $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                        };
                    }
                    else 
                    {
                        var lang=$('.lang-on').data('lang');
                        window.history.pushState({path:'/'+lang+'/search'},'','/'+lang+'/search');
                        //console.debug('8-'+lang);
                        for (i = 0; i < $('#language a').length; i++) {
                            $('#language a')[i].search=replaceQueryParam('search', encodeURI(val), $('#language a')[0].search);
                        };
                    }
                    return false;
                });
            }, ms);
        }
      });
    });

    $.urlParam = function(name){
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    /* the below code is to override back button to get the ajax content without reload*/
    $(window).bind('popstate', function() {
        $('#div-search-loader').html("<img src='/g/loading.gif' alt='' class='img-search-loader' />");
        $('#input-basic-search').val(decodeURI($.urlParam('search')));
        $('#div-basic-search-results').load('/includes/search/search.php?search='+encodeURI($.urlParam('search')), function() {
        $('#div-search-loader').html("");
            return false;
        });
    });

});
