window.addEvent('domready', function(){

	//hide the element when the page loads
	//$$('.show-hide').addClass('hide')
	
	//add the cursor style for the anchor
	$$('.toggle-show-hide').setStyle('cursor', 'pointer');
	
	//onclick toggle the hide class	
	$$('.toggle-show-hide').addEvent('click', function(e)
    {
		//$$('.show-hide').toggleClass('hide');
        //document.getElementById('divtest2').classList.toggle('hide');
        //document.getElementById("div-number-quick-search").className += " hide";
        //document.getElementById("div-number-advanced-search").className += " MyCla";
        
        changeClassToggle(document.getElementById("div-number-quick-search"), "hide");
        changeClassToggle(document.getElementById("div-number-advanced-search"), "hide");

        if( this.id=="a-right-side-advanced" )
        {
            $('number1').value="";
        }
        else if( this.id=="a-right-side-quick" )
        {
            $('input-number-from').value="From";
            $('input-number-to').value="To";
            var inputs = document.getElementsByTagName('input');
            for (var i = 0; i < inputs.length; i++)
            {
                if( inputs[i].name=="number_list[]" ) inputs[i].value="";
            }
        }
	});

});
