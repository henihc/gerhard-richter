window.addEvent('domready', function() {

	
	
	//create the tabs
	var ulTabs = new Element('ul').setProperty('class', 'tabs');
	var brClear = new Element('br').setProperty('class', 'clear');
	var divTabsInfo = new Element('div').setProperty('class', 'tabs-info');
	
	//add the tabs to the dom
	var ulTabsElement = ulTabs.injectAfter('details-image');
	var brClearElement = brClear.injectAfter(ulTabsElement);
	var divTabsInfoElement = divTabsInfo.injectAfter(brClearElement);
	
	//add the tab items
	var salesTab = "<li class=\"sales\"><h4><a href=\"#\" class=\"sales\">Sales History</a></h4></li>";
	var collectionTab = "<li class=\"collection\"><h4><a href=\"#\" class=\"collection\">Collection</a></h4></li>";
	var relatedartTab = "<li class=\"related-art\"><h4><a href=\"#\">Related Art</a></h4></li>";
	var exhibitionsTab = "<li class=\"exhibitions\"><h4><a href=\"#\">Exhibitions</a></h4></li>";
	var notesTab = "<li class=\"notes\"><h4><a href=\"#\">Notes</a></h4></li>";
	
	ulTabsElement.setHTML(salesTab+collectionTab+relatedartTab+exhibitionsTab+notesTab);
	$$('.tabs li a')[0].addClass('selected');	
	
	//add the tab-info content
	var salesInfo = "<p class=\"sales-info\">All sales info would go here ansd sdfk jnn dlg;dlfgj dlbk dl;b dl; bmlcv;b mnld;ffb nmldf;b nmdfl;b kndfgl;b ndflb </p>";
	var collectionInfo = "<p class=\"collection-info\">St&auml;dtische Galerie Wolfsburg</p>";
	var relatedartInfo = "<p class=\"related-art-info\">All related art would go here</p>";
	var exhibitionsInfo = "<p class=\"exhibitions-info\">All exhibition info would go here</p>";
	var notesInfo = "<p class=\"notes-info\">All notes would go here</p>";
	
	divTabsInfoElement.setHTML(salesInfo+collectionInfo+relatedartInfo+exhibitionsInfo+notesInfo);
	$$('.tabs-info p').addClass('hide');
	$$('.tabs-info p')[0].removeClass('hide');

	var tabs = $$('.tabs li');
	var links = $$('.tabs li a');
	var info = $$('.tabs-info p');

	tabs.setStyles('float: left; margin-right: 5px;');
	$$('.tabs li h4').setStyle('top', '0'); 
	
	links.each(function(items) {
		items.addEvent('click', function(e) {
		
			e = new Event(e).stop();
			links.removeClass('selected');
			items.addClass('selected');
			
			var tabClass = items.getParent().getParent().getProperty('class');
			
			info.each(function(el) {
				var infoClass = el.getProperty('class');
				if (!infoClass.test(tabClass)) {
					el.addClass('hide');
				}
				else {
					el.removeClass('hide');
				}
			});
		});

	});

});