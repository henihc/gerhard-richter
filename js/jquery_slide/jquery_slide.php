<?php
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
/*
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
*/

# COUNTRY LOCALE LANGUAGE
$browser_language = get_browsers_default_language($_SERVER['HTTP_ACCEPT_LANGUAGE']);

$options_lang_file=array();
$options_lang_file['get_lang']=$browser_language;
$language=UTILS::load_lang_file($db,$options_lang_file);
# END COUNTRY LOCALE LANGUAGE
?>
(function($) {
    $(document).ready(
        function()
        {
            $('#a_form_opp_read_more').on('click',function() {
               //console.log('pressed');
               if( $(this).html()=='[<?php print LANG_CATEGORY_DESC_MORE; ?>]' ) $(this).html('[<?php print LANG_CATEGORY_DESC_CLOSE; ?>]');
               else $(this).html('[<?php print LANG_CATEGORY_DESC_MORE; ?>]');
               $('#div-brief-desc').toggleClass('p-opened');
            });
        }
    );
})(jQuery);
