$(function() {
    //$('#p-exh-search-right-side-info').tooltip({
    $('#tooltip-videos').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        extraClass: "tooltip-videos",
        left: -200,
        fade: 250
    });
    $('.help').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        left: -250,
        fade: 250
    });
    $('.help-quote').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        extraClass: "tooltip-quotes",
        left: -200,
        fade: 250
    });

});
