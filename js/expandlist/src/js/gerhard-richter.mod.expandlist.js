
// | gerhard-richter.mod.expandlist.js
// | 
// | Description: Tables with expandable rows for www.gerhard-richter.com (JavaScript)
// | Dependencies: gerhard-richter.mod.expandlist.php, gerhard-richter.mod.expandlist.css, jquery.js
// | Author: Eric Kuhnert
// | Date: 2014-01-06
// |

// .......................................................................................
var expandlist = {
	obj : [],
	css : [ '.sort_down', '.sort_up' ],
	debug : false,																				// -- 2014-01-06: attr added

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	log : function(_message)																	// -- 2014-01-06: function added
	{
		if (this.debug && console && (typeof console.log == 'function'))
		{
			console.log('expandlist: ' + _message);
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	expand_row : function(_row)
	{
		var gap = 17;
		var expand_box_height = $($(_row.expand_box).find('.expand-box')[0]).height();			// -- 2014-01-06: var added
		this.log( "selected row's expand-box height: " + expand_box_height );					// -- 2014-01-06: function call added
		//_row.style.height = (_row.original_height + _row.expand_box.height + gap) + 'px';		// -- 2014-01-06: commented
		_row.style.height = (_row.original_height + expand_box_height + gap) + 'px';			// -- 2014-01-06: row added
		$(_row.thumb).hide();
		$(_row.thumb_placeholder).show();
		_row.thumb_placeholder.style.visibility = 'visible';
		$(_row.thumb_placeholder).height(_row.original_height - 8);
		$(_row).addClass('selected');
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	collapse_row : function(_row)
	{
		$(_row).removeClass('selected')
		$(_row.thumb).show();
		$(_row.thumb_placeholder).hide();
		_row.style.height = _row.original_height + 'px';
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	parentHasClass : function(_node, _class)
	{
		do
		{
			if ($(_node).hasClass(_class)) return true;
			_node = _node.parentNode;
		}
		while (_node.parentNode != undefined);
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	onHeaderClick : function(_col)
	{
		var sortable = $(_col).attr('data-sortable');
		if ( (_col.table.options.onheaderclick) && (typeof sortable != 'undefined') && (sortable == 1) )
		{
			direction = $(_col).find(expandlist.css[0]).length - $(_col).find(expandlist.css[1]).length;
			document.location = _col.table.options.onheaderclick + _col.colId  + ',' + direction;
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init_table : function(_table)
	{
		var rows = $(_table).find('tr');
		var cols = $(rows).find('th');
		for (var i = 0; i < cols.length; i+=2)
		{
			cols[i].table = _table;
			cols[i].colId = i/2;
			cols[i].onclick = function (_e) 
			{
				expandlist.onHeaderClick(this);
			}
	}
		for (var i = 1; i < rows.length; i++)
		{
			var expand_box = $(rows[i]).find('.expand-box-wrapper')[0];
			//var div_book_overlay_content = $(rows[i]).find('.div-book-overlay-content')[0];
			if (expand_box)
			{
				rows[i].expand_box = expand_box;
				rows[i].expand_box.visible = false;
				//rows[i].expand_box.height = $($(rows[i].expand_box).find('.expand-box')[0]).height();		// -- 2014-01-06: commented
				rows[i].original_height = $(rows[i]).height();
				rows[i].thumb = $(rows[i]).find('.thumb')[0];
				rows[i].thumb_placeholder = $(rows[i]).find('.thumb_placeholder')[0];
				$(rows[i]).click( function (e)
				{
					//console.debug(e);
					if (!expandlist.parentHasClass(e.target, 'expand-box'))
					{
						this.expand_box.visible = !this.expand_box.visible;
						(this.expand_box.visible) ? expandlist.expand_row(this) : expandlist.collapse_row(this);
					}
				} );
			}
			/*
			else if( div_book_overlay_content )
			{
				rows[i].expand_box = div_book_overlay_content;
				$(rows[i]).click( function (e)
				{
					var bookid=$(e.currentTarget).data('tr');
					var overlay_content=$('.overlay-book-'+bookid).html();
					//console.debug(e);
					//console.debug(overlay_content);

                    $.fancybox({
                        type: 'inline',
                        padding: 0,
                        helpers : {
                            overlay: {
                                css : {
                                    //'background' : 'rgba(58, 42, 45, 2)'
                                }
                            }
                        },
                        content: overlay_content,
					    beforeShow: function () {
					        // Disable right click
					        $.fancybox.wrap.bind("contextmenu", function (e) {
					        return false;
					        });
					        // Disable drag
					        $.fancybox.wrap.bind("dragstart", function (e) {
					        return false;
					        });
					    }
                    });

				} );
			}
			*/
		}
		this.obj.push(_table);
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function()
	{
		var tables = $('.expandlist');
		for (var i = 0; i < tables.length; i++) this.init_table(tables[i]);
	}
	
}

// .......................................................................................
$(document).ready(function() {
  expandlist.init();
});