/*
Complex Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

var offsetfrommouse=[15,15]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 300;	// maximum image size.



if (document.getElementById || document.all){
	document.write('<div id="trailimageid">');
	document.write('</div>');
}

function gettrailobj(){
if (document.getElementById)
return document.getElementById("trailimageid").style
else if (document.all)
return document.all.trailimagid.style
}

function gettrailobjnostyle(){
if (document.getElementById)
return document.getElementById("trailimageid")
else if (document.all)
return document.all.trailimagid
}


function truebody(){
return (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

//function showtrail(imagename,title,description,ratingaverage,ratingnumber,showthumb,height,filetype){
function showtrail(imagename,title,height, year, number){
	if (height > 0){
		currentimageheight = height;
	}
//alert(height);
	document.onmousemove=followmouse;

	//cameraHTML = '';

	//if ( !ratingnumber ){
	//	ratingnumber = 0;
	//	ratingaverage = 0;
	//}
	//
	//for(x = 1; x <= 5; x++){
	//
	//	if (ratingaverage >= 1){
	//		cameraHTML = cameraHTML + '<img src="/images/camera_1.gif">';
	//	} else if (ratingaverage >= 0.5){
	//		cameraHTML = cameraHTML + '<img src="/images/camera_05.gif">';
	//	} else {
	//		cameraHTML = cameraHTML + '<img src="/images/camera_0.gif">';
	//	}
	//
	//	ratingaverage = ratingaverage - 1;
	//}
	//
	//cameraHTML = cameraHTML + ' (' + ratingnumber + ' Review';
	//if ( ratingnumber > 1 ) cameraHTML += 's';
	//cameraHTML = cameraHTML + ')';
	newHTML = '<div class="painting-hover">';
	newHTML = newHTML + '<img src="' + imagename + '" />';
	//newHTML = newHTML + '<span class="painting-title">' + title + '</span><br />';
	//newHTML = newHTML + year + ' - CR:' + number + '</div>';
	//newHTML = newHTML + 'Rating: ' + cameraHTML + '<br/>';
//	if (showthumb > 0){
//		newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;">';
//		if(filetype == 8) { // Video
//			newHTML = newHTML +	'<object width="380" height="285" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">';
//			newHTML = newHTML + '<param name="movie" value="video_loupe.swf">';
//			newHTML = newHTML + '<param name="quality" value="best">';
//			newHTML = newHTML + '<param name="loop" value="true">';
//
//			newHTML = newHTML + '<param name="FlashVars" value="videoLocation=' + imagename + '&bufferPercent=25">';
//			newHTML = newHTML + '<EMBED SRC="video_loupe.swf" LOOP="true" QUALITY="best" FlashVars="videoLocation=' + imagename + '&bufferPercent=25" WIDTH="380" HEIGHT="285">';
//			newHTML = newHTML + '</object></div>';
//		} else {

//		}
//	}

	//newHTML = newHTML + '</div>';
	gettrailobjnostyle().innerHTML = newHTML;
	gettrailobj().display="inline";
}

//function showtrailBatch(imagename,title,filetype){
//	document.onmousemove=followmouseBatch;
//
//	cameraHTML = '';
//
//	newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;" id="trailInnerDiv">';
//	newHTML = newHTML + '<h2>' + title + '</h2>';
//
//	newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;">';
//	if(filetype == 8) { // Video
//		newHTML = newHTML +	'<object width="380" height="285" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">';
//		newHTML = newHTML + '<param name="movie" value="video_loupe.swf">';
//		newHTML = newHTML + '<param name="quality" value="best">';
//		newHTML = newHTML + '<param name="loop" value="true">';
//
//		newHTML = newHTML + '<param name="FlashVars" value="videoLocation=' + imagename + '">';
//		newHTML = newHTML + '<EMBED SRC="video_loupe.swf" LOOP="true" QUALITY="best" FlashVars="videoLocation=' + imagename + '" WIDTH="380" HEIGHT="285">';
//		newHTML = newHTML + '</object></div>';
//	} else {
//		newHTML = newHTML + '<img src="' + imagename + '" border="0"></div>';
//	}
//
//	newHTML = newHTML + '</div>';
//	gettrailobjnostyle().innerHTML = newHTML;
//	gettrailobj().display="inline";
//	gettrailobj().position="absolute";

//	currentimageheight = $('trailInnerDiv').offsetHeight;
//	
//}
//////////////dont touch below//////////////
function hidetrail(){
	gettrailobj().innerHTML = " ";
	gettrailobj().display="none"
	document.onmousemove=""
	gettrailobj().left="-500px"

}

function followmouse(e){

	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

	//if (document.all){
	//	gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
	//} else {
	//	gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}
	
	var thisHeight = 70;
	
	//if (currentimageheight>300)
	//{
	//	thisHeight = 110;
	//}
	//else
	//{
	//	thisHeight = 50;
	//}
	
	if (typeof e != "undefined"){
		if (docwidth - e.pageX < 380){
			xcoord = e.pageX - xcoord - 400; // Move to the left side of the cursor
		} else {
			xcoord += e.pageX;
		}
		if (docheight - e.pageY < (currentimageheight + thisHeight)){
			ycoord += e.pageY - Math.max(0,(thisHeight + currentimageheight + e.pageY - docheight - truebody().scrollTop));
		} else {
			ycoord += e.pageY;
		}

	} else if (typeof window.event != "undefined"){
		if (docwidth - event.clientX < 380){
			xcoord = event.clientX + truebody().scrollLeft - xcoord - 400; // Move to the left side of the cursor
		} else {
			xcoord += truebody().scrollLeft+event.clientX
		}
		if (docheight - event.clientY < (currentimageheight + thisHeight)){
			ycoord += event.clientY + truebody().scrollTop - Math.max(0,(thisHeight + currentimageheight + event.clientY - docheight));
		} else {
			ycoord += truebody().scrollTop + event.clientY;
		}
	}

	if(ycoord < 0) { ycoord = ycoord*-1; }
	gettrailobj().left=xcoord+"px"
	gettrailobj().top=ycoord+"px"

}

function followmouseBatch(e){
	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

	var trailInnerDiv = $('trailInnerDiv');
	var currentimageheight = trailInnerDiv.offsetHeight;
	var currentimagewidth = trailInnerDiv.offsetWidth;

	scrollPos = Position.realOffset(truebody());
	
	if (typeof e != "undefined"){
		if (docwidth - e.pageX < 380){
			xcoord = e.pageX - xcoord - 400; // Move to the left side of the cursor
		} else {
			xcoord += e.pageX;
		}
		if ((e.pageY - scrollPos[1]) + currentimageheight > docheight){
			ycoord = -ycoord + (e.pageY - currentimageheight);
		} else {
			ycoord += e.pageY;
		}
	} else if (typeof window.event != "undefined"){
		if (event.clientX + currentimagewidth > docwidth){
			xcoord = -xcoord + ((event.clientX + scrollPos[0]) - currentimagewidth); // Move to the left side of the cursor
		} else {
			xcoord += (event.clientX + scrollPos[0]);
		}
		if (event.clientY + currentimageheight > docheight){
			ycoord = -ycoord + ((event.clientY + scrollPos[1]) - currentimageheight);
		} else {
			ycoord += (event.clientY + scrollPos[1]);
		}
	}

	if(ycoord < 0) { ycoord = ycoord*-1; }

	gettrailobj().left=xcoord+"px"
	gettrailobj().top=ycoord+"px"

}