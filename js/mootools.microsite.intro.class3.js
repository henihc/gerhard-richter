var IntroFades = new Class({

	options:{
		siteName:"Site Name",
		sitePathName:"",
		siteAddress:"",
		imagesPath:"",
		images:[]
	},

	initialize: function(options){

		this.setOptions(options);

		//if sitePathName is empty
		if(this.options.sitePathName == ""){
			this.options.sitePathName = this.options.siteName.toLowerCase().replace(/ /g, "-");
		}
		else{
			this.options.sitePathName = this.options.sitePathName.toLowerCase().replace(/ /g, "-");
		}

		//if siteAddress is empty
		if(this.options.siteAddress == ""){
			this.options.siteAddress = "/art/editions/"+this.options.sitePathName+"/";
		}
		else{
			this.options.siteAddress = this.options.siteAddress;
		}
		
		//if imagesPath is empty
		if(this.options.imagesPath == ""){
			//this.options.imagesPath = "/g/microsite/"+this.options.sitePathName+"-";
			this.options.imagesPath = "/g/microsite/";
		}
		else{
			this.options.imagesPath = this.options.imagesPath;
		}

		this.addImages(this.options.images,this.options.siteName,this.options.siteAddress, this.options.imagesPath);

	},
	
	addImages: function(images,siteName,siteAddress,imagesPath){
		

		var introHolder = new Element('div').setProperty('id', 'intro-holder').injectInside(document.body);

		var enterHolder = new Element('div').setProperty('id', 'enter').injectInside(introHolder);

		var a_intro_image = new Element('a').setProperty('id', 'a_intro_image').injectInside(enterHolder);
		a_intro_image.setProperty('class', 'a_intro_image');
		a_intro_image.setProperty('title', 'Gerhard Richter '+siteName);
		a_intro_image.setProperty('href', siteAddress);

		var enterHolder = new Element('p').setProperty('class', 'p_richter');
        enterHolder.innerHTML="Gerhard Richter";
        enterHolder.injectInside(a_intro_image);

		var enterHolder = new Element('p').setProperty('class', 'p_colours');
        enterHolder.innerHTML=siteName;
        enterHolder.injectInside(a_intro_image);

		var enterHolder = new Element('p').setProperty('class', 'p_enter');
        enterHolder.innerHTML="Enter";
        enterHolder.injectInside(a_intro_image);








		//create a holder for the images
		var imageHolder = new Element('div').setProperty('id', 'image-holder').injectInside(introHolder);

		//add the array of images to the page
		images.each(function addImages(items, index){

			var tempLink = new Element('a').setProperties({'class': 'close', 'title': 'Enter '+siteName, 'href': siteAddress}).injectInside(imageHolder);

			//wrap the image in a link
			new Element('img').setProperties({
				'src': imagesPath+items,
				'class': 'fade',
				'id': 'image-' + (index+1),
				'alt': siteName+' image ' + (index+1)
			}).injectInside(tempLink).setOpacity(0);

		});

		var imageFade = $$('.close img.fade');
		var myChain = new Chain();
		imageFade.each(function(element){
			myChain.chain(function(){
				var fx = new Fx.Styles(element, {duration:500, wait:false});
				fx.start({
					'opacity':1
				});
			});
		});

		var runChain = function(){
			myChain.callChain();
			if (myChain.chains.length==0){runChain = $clear(timer);};
		}

		var timer = runChain.periodical(700);





	}

});

IntroFades.implement(new Options, new Events);
