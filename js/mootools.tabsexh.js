window.addEvent('domready', function() {

	var tabs = $$('.tabsexh li.tab');
	var links = $$('.tabsexh li a.name');
	var info = $$('.tabsexh-info .holder');
	
	var clear = new Element('br').addClass('clear');
	
	if($('top-tabsexh')){
	info.addClass('hide');
	info[0].removeClass('hide');


    if( document.getElementById('this_year_selected') )
    {
        var divs = document.getElementsByTagName('div');
        for (var i = 0; i < divs.length; i++) 
        {
            var div = divs[i];
            if( div.className.indexOf('holder')==0 ) div.addClass('hide');
            document.getElementById('this_year_selected').removeClass('hide');
        }
    }
    else
    {
	    $$('.tabsexh h4 a').removeClass('selected');
	    $$('.tabsexh li a')[0].addClass('selected');
    }
	
	$$('.tabsexh-info').each(function(el) {
				el.injectAfter('top-tabsexh');
				});

	$$('.tabsexh li h4').setStyle('top', '0'); 
	
	clear.injectAfter('top-tabsexh');

  };

});
