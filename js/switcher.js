function $() {
	var elems = [];
	for (var i=0; i<arguments.length; i++) {
		var elem = arguments[i];
		if (typeof elem == 'string') elem = document.getElementById(elem);
		if (arguments.length == 1) return elem;
		elems.push(elem);
	}
	return elems;
}
Object.prototype.$T = function (t,o) {
	o = o || document;
	return o.getElementsByTagName(t);
}
var requirementsSwap = {
	init : function() {
	},
	swap : function(sType) {
	var c = requirementsSwap;
	var oSwitcher = $('switcher');
	var oOsList = $('dealers');
	var oAppsList = $('write');
	var oAppsList2 = $('posters');
	if (sType == 'dealers') {
		oSwitcher.className = 'dealers';
		oOsList.className = '';
		oAppsList.className = 'hide';
		oAppsList2.className = 'hide';
	}
	else if (sType == 'write') {
		oSwitcher.className = 'write';
		oOsList.className = 'hide';
		oAppsList.className = '';
		oAppsList2.className = 'hide';
	}
	else if (sType == 'posters') {
		oSwitcher.className = 'posters';
		oOsList.className = 'hide';
		oAppsList.className = 'hide';
		oAppsList2.className = '';
	}
}
};
