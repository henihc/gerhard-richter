/*****************************************************************

	Script		: Jump Menu
	Version		: 3.0
	Authors		: Adobe Dreamweaver, modified by http://6stops.com
	Copyright	: Copyright (c) 2007 , MIT Style License.

*****************************************************************/

function jump_menu(target,object,restore){
  eval(target+".location='"+object.options[object.selectedIndex].value+"'");
  if (restore) object.selectedIndex=0;
}