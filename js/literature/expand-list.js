// | expand-list.js
// | 
// | Description: Tables with expandable rows for www.gerhard-richter.com
// | Requrements: jquery.js
// | Author: Eric Kuhnert
// | Date: 2012-10-19
// |

// .......................................................................................
var expand_list = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function()
	{
		var items = $('.table_item');
		for (var i = 0; i < items.length; i++)
		{
			var item = items[i];
			item.collapsed = false;
			item.thumb = $(item).find('img')[0];
			item.attribs = $(item).find('.table_item_attribs')[0];
			item.details = $(item).find('.table_item_details')[0];
			item.caption = $(item).find('p')[1];
			item.attribs.onclick = function (e) {
				if (this.parentNode.collapsed)
				{
					this.parentNode.details.style.height = '0px';
					this.parentNode.thumb.style.visibility = 'visible';
					this.parentNode.caption.style.fontWeight = 'normal';
					//this.parentNode.className = 'table_item';
                    if( search ) this.parentNode.className = 'table_item table_item_search';
                    else this.parentNode.className = 'table_item';
					this.parentNode.collapsed = false;
				} else {
					this.parentNode.details.style.height = 'auto';
					this.parentNode.thumb.style.visibility = 'hidden';
					this.parentNode.caption.style.fontWeight = 'bold';
					//this.parentNode.className = 'table_item_selected';
                    if( search ) this.parentNode.className = 'table_item_selected table_item_selected-search';
                    else this.parentNode.className = 'table_item_selected';
					this.parentNode.collapsed = true;
				}
			}
		}
	}
	
}

// .......................................................................................
$(document).ready(function() {
  expand_list.init();
});
