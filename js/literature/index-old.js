// | index.js
// | 
// | Description: Page base module (uses era framework)
// | Author: Eric Kuhnert
// | Date: 2011-08-09
// |

var index = {

	data : new Array(),

	// own functions etc. here ...


	// .............................................................................................
	myfunc : function()
	{
	},





	// .............................................................................................
	loop : function()
	{
		// usually gets triggered every 40 msecs <– era.data['loopInterval']
	},


	// module's meta information
	// _____________________________________________________________________________________________
	m_tit : 'index module', 
	m_ver : '0.1', 
	m_aut : 'Eric Kuhnert',
	m_dat : '2011-08-10',


	// .............................................................................................
	init : function()
	{
		// define variables
		this.data['config_file'] = 'src/xml/index.xml';

		// register js mod	
		//era.MOD.addMod(this.m_tit, this.m_ver, this.m_aut, this.m_dat);
		
		var items = document.getElementsByClassName('table_item');
		for (var i = 0; i < items.length; i++)
		{
			var item = items[i];
			item.collapsed = false;
			item.thumb = item.getElementsByTagName('img')[0];
			item.attribs = item.getElementsByClassName('table_item_attribs')[0];
			item.details = item.getElementsByClassName('table_item_details')[0];
			item.caption = item.getElementsByTagName('p')[1];
			//item.caption = item.getElementsByTagName('span')[0];
			//alert(item.title.className);
			
            if( item.className=="table_item table-this-selected" )
            {
				if ( !item.parentNode.collapsed )
                {
					// show details
					item.getElementsByClassName('table_item_details')[0].style.height = 'auto';
					item.getElementsByTagName('img')[0].style.visibility = 'hidden';
					item.getElementsByTagName('p')[1].style.fontWeight = 'bold';
                    if( search )
                    {
					    item.className = 'table_item_selected table_item_selected-search';
                    }
                    else
                    {
					    item.className = 'table_item_selected';
                    }
					item.collapsed = true;
				}
            }

			item.attribs.onclick = function (e) {
				if (this.parentNode.collapsed)
				{
					// hide details
					this.parentNode.details.style.height = '0px';
					this.parentNode.thumb.style.visibility = 'visible';
					this.parentNode.caption.style.fontWeight = 'normal';
                    if( search ) this.parentNode.className = 'table_item table_item_search';
                    else this.parentNode.className = 'table_item';
					this.parentNode.collapsed = false;
					
				} else {
					// show details
					this.parentNode.details.style.height = 'auto';
					this.parentNode.thumb.style.visibility = 'hidden';
					this.parentNode.caption.style.fontWeight = 'bold';
                    if( search ) this.parentNode.className = 'table_item_selected table_item_selected-search';
                    else this.parentNode.className = 'table_item_selected';
					this.parentNode.collapsed = true;
				}
			}
		}

	}
	
}

function init()
{
	//era.data["debug"] = "true";	// delete this line later
	index.init();
} 
