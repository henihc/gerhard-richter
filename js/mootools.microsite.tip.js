window.addEvent('domready', function(){

	//hook for microsites tooltip
	if($$('.edition-tip-en')){
		var micrositeTip = $$('.edition-tip-en');

		micrositeTip.setProperty('title', 'Search Help :: <em>Single edition =</em> type the edition number (eg; 23) <p><em>Multiple editions =</em> type each edition number, separated by commas (eg; 23, 32, 55)</p> <p><em>Groups =</em> type the start & end number of the group, separated by a colon (eg; 23:55)</p> ');

		$$('input.edition').setProperty('autocomplete', 'off');

		var tips = new Tips(micrositeTip);
	}
	
	if($$('.edition-tip-de')){
		var micrositeTip = $$('.edition-tip-de');

		micrositeTip.setProperty('title', 'Hilfe zur Suche :: <em>Einzelne Edition =</em> Nummer der Edition eingeben (z.B. 23) <p><em>Mehrere Editionen =</em> jede Editionsnummer durch Komma getrennt eingeben (z.B. 23, 32, 55)</p> <p><em>Gruppen =</em> Start- und Endnummer der Gruppe durch Doppelpunkt getrennt eingeben (z.B. 23:55)</p> ');

		$$('input.edition').setProperty('autocomplete', 'off');

		var tips = new Tips(micrositeTip);
	}

});

