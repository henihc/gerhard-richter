<?php
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
/*
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
*/

# COUNTRY LOCALE LANGUAGE
$browser_language = get_browsers_default_language($_SERVER['HTTP_ACCEPT_LANGUAGE']);

$options_lang_file=array();
$options_lang_file['get_lang']=$browser_language;
$language=UTILS::load_lang_file($db,$options_lang_file);
# END COUNTRY LOCALE LANGUAGE
?>
window.addEvent('domready', function(){

	new IntroFades({
        siteName:'<?=LANG_MICROSITE_SINBAD_JS_SITENAME?>',
		siteAddress:'/art/paintings/sinbad/',
		images:[
			"/sinbad/1.jpg",
			"/sinbad/2.jpg",
			"/sinbad/3.jpg",
			"/sinbad/4.jpg",
			"/sinbad/5.jpg",
			"/sinbad/6.jpg",
			"/sinbad/7.jpg",
		]
	});

});
