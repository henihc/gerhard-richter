window.addEvent('domready', function() {

	var tabs = $$('.tabs li.tab');
	var links = $$('.tabs li a.name');
	var info = $$('.tabs-info .holder');
	
	var clear = new Element('br').addClass('clear');
	
	if($('top-tabs'))
    {
	info.addClass('hide');
	info[0].removeClass('hide');
	$$('.tabs h4 a').removeClass('selected');
	$$('.tabs li a')[0].addClass('selected');
	
	$$('.tabs-info').each(function(el) {
				el.injectAfter('top-tabs');
				});

	tabs.setStyles('float: left; margin-right: 3px;');
	
	$$('.tabs li h4').setStyle('top', '0'); 
	
	clear.injectAfter('top-tabs');
	
	links.each(function(items) {
		items.addEvent('click', function(e) {
		
			e = new Event(e).stop();
			links.removeClass('selected');
			items.addClass('selected');
			
			var tabClass = items.getParent().getParent().getProperty('class');
			var tabClassSplit = tabClass.split(" ");
			info.each(function(el) {
				var infoClass = el.getProperty('class');
				if (!infoClass.test(tabClassSplit[0])) {
					el.addClass('hide');
				}
				else {
					el.removeClass('hide');
				}
			});
		});

	});

  };

});
