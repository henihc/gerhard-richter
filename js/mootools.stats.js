window.addEvent('load', function(){

	//styling for all the stats tables
	$$('.stats-table tr th, .stats-table tr td, .compare-table tr th, .compare-table tr td, #details tr th, #details tr td, #details-geo tr th, #details-geo tr td').setStyle('text-align', 'right');

	$$('.stats-table tr, .compare-table tr, #details-geo tr').each(function(element){
		element.getFirst().setStyle('text-align', 'left');
	});
	
	$$('.compare-table-options tr td').setStyle('text-align', 'center');

	//add the cursor style for the caption
	$$('.stats-table caption').setStyle('cursor', 'pointer');

	//show-hide effect for the stat table
	$$('.stats-table caption').addEvent('click', function(){
		this.getNext().toggleClass('hide');
		this.getNext().getNext().toggleClass('hide');
	});

	//initiate the calender
	new Calendar({ date1: 'Y-m-d' }, { classes: ['dashboard']});
	new Calendar({ date2: 'Y-m-d' }, { classes: ['dashboard']});

	//create a custom submit button for the custom search
	var customDateSubmit = new Element('a').setProperty('id','custom-date-filter-submit').setHTML('Search').injectInside($('filter-custom-dates'));
	
	customDateSubmit.addEvent('click', function(){
		$('filter-custom-dates').submit();
	});

});