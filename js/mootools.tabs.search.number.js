window.addEvent('domready', function() {

	var tabs = $$('.ul-search-number-tabs li.li-search-number-tab');
	var links = $$('.ul-search-number-tabs li a.name');
	var info = $$('.div-right-side-number-tabs-info .holder');
	
	var clear = new Element('br').addClass('clear');
	
	if($('ul-right-side-number-tabs'))
    {
	    info.addClass('hide');
	    info[0].removeClass('hide');
                
        //if( document.getElementById('div-right-side-number-selected') )
        if( $$('.this-select') )
        {   
            var divs = document.getElementsByTagName('div');
            for (var i = 0; i < divs.length; i++) 
            {   
                var div = divs[i];
                if( div.className.indexOf('holder')==0 ) div.addClass('hide');
                //document.getElementById('div-right-side-number-selected').removeClass('hide');
                $$('.this-select').removeClass('hide');
            }   
        }   
        else
        {   
	        $$('.ul-search-number-tabs h4 a').removeClass('selected');
	        $$('.ul-search-number-tabs li a')[0].addClass('selected');
        }
	    
	    $$('.div-right-side-number-tabs-info').each(function(el) 
        {
	        el.injectAfter('ul-right-side-number-tabs');
	    });

	    //tabs.setStyles('float: left; margin-right: 3px;');
	    
	    if(!window.devicePixelRatio) $$('.ul-search-number-tabs li h4').setStyle('top', '0'); 
	    
	    //clear.injectAfter('ul-right-side-number-tabs');
	    
	    links.each(function(items) 
        {
	    	items.addEvent('click', function(e) 
            {
                if( this.name=="list" )
                {
                    $('input-number-from').value="";
                    $('input-number-to').value="";
                }
                else if(this.name=="range")
                {
                    var inputs = document.getElementsByTagName('input');
                    for (var i = 0; i < inputs.length; i++)
                    {   
                        if( inputs[i].name=="number_list[]" ) inputs[i].value="";
                    }  
                }
	    		e = new Event(e).stop();
	    		links.removeClass('selected');
	    		items.addClass('selected');
	    		
	    		var tabClass = items.getParent().getParent().getProperty('class');
	    		var tabClassSplit = tabClass.split(" ");
	    		info.each(function(el) {
	    			var infoClass = el.getProperty('class');
	    			if (!infoClass.test(tabClassSplit[0])) {
	    				el.addClass('hide');
	    			}
	    			else {
	    				el.removeClass('hide');
	    			}
	    		});
	    	});

	    });
    };
});
