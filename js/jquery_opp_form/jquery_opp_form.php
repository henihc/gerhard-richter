<?php
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
/*
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
*/

# COUNTRY LOCALE LANGUAGE
$browser_language = get_browsers_default_language($_SERVER['HTTP_ACCEPT_LANGUAGE']);

$options_lang_file=array();
$options_lang_file['get_lang']=$browser_language;
$language=UTILS::load_lang_file($db,$options_lang_file);
# END COUNTRY LOCALE LANGUAGE
?>
(function($) {
    $(document).ready(
        function()
        {

            // more button slider bit
            $('#a_form_opp_read_more').on('click',function() {
               //console.log('pressed');
               if( $(this).html()=='[<?php print LANG_CATEGORY_DESC_MORE; ?>]' ) $(this).html('[<?php print LANG_CATEGORY_DESC_CLOSE; ?>]');
               else $(this).html('[<?php print LANG_CATEGORY_DESC_MORE; ?>]');
               $('#div-brief-desc').toggleClass('p-opened');
            });


            // opp validation form
            //$('#submit').attr('type','button');
            $('#submit').prop('type','button');


            $('#submit').on('click',function() {
                //console.log('pressed');
                var valid_color="#fff";
                var error_color="red";

                if( $('#email') )
                {   
                    var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var item = $('#email');
                    var item_text = $('#email_text');
                    var error1 = false;
                    if(!filter.test(item.val()))
                    {   
                        error1 = true;
                        item_text.css('color',error_color);
                    }   
                    else
                    {   
                        error1 = false;
                        item_text.css('color',valid_color);
                    }   
                }   

                if( $('#first_name') )
                {   
                var item = $('#first_name');
                var item_text = $('#first_name_text');
                var error2 = false;
                    if( item.val().length<=0 )
                    {   
                        error2 = true;
                        item_text.css('color',error_color);
                    }   
                    else
                    {   
                        error2 = false;
                        item_text.css('color',valid_color);
                    }
                }

                if( $('#last_name') )
                {
                var item = $('#last_name');
                var item_text = $('#last_name_text');
                var error3 = false;
                    if( item.val().length<=0 )
                    {
                        error3 = true;
                        item_text.css('color',error_color);
                    }
                    else
                    {
                        error3 = false;
                        item_text.css('color',valid_color);
                    }
                }

                if( $('#codenumb') )
                {
                var item = $('#codenumb');
                var item_text = $('#codenumb_error');
                var error4 = false;
                    if( item.val().length<=0 )
                    {
                        error4 = true;
                        item_text.css('color',error_color);
                    }
                    else
                    {
                        error4 = false;
                        item_text.css('color',valid_color);
                    }
                }

                if( $('#agree') )
                {
                    var item = $('#agree');
                    var error5 = false;
                    if( item.is(':checked')==false )
                    {
                        error5 = true;
                        alert('<?php print LANG_OPP_FORM_75; ?>');
                    }
                    else
                    {
                        error5 = false;
                    }
                }

                if( !error1 && !error2 && !error3 && !error4 && !error5 )
                {
                    $('#submit').attr('type','submit');
                }
                else if( !error5 ) alert('<?php print LANG_OPP_FORM_75_2; ?>');

            });


        }
    );
})(jQuery);
