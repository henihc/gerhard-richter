function validate_form()
{

    if( document.getElementById('email') )
    {
        var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = document.getElementById('email');
        var email_text = document.getElementById('email_text');
        var error1 = false;
        if(!filter.test(email.value))
        {
            error1 = true;
            email_text.style.color="red";
        }
        else
        {
            error1 = false;
            email_text.style.color="#000";
        }
    }

    if( document.getElementById('message') )
    {
        var message = document.getElementById('message');
        var message_text = document.getElementById('message_text');
        var error2 = false;
        if( message.value.length<10 )
        {
            error2 = true;
            message_text.style.color="red";
        }
        else
        {
            error2 = false;
            message_text.style.color="#000";
        }
    }

    if( document.getElementById('captcha') )
    {
        var captcha = document.getElementById('captcha');
        var captcha_text = document.getElementById('captcha_text');
        var error3 = false;
        if( captcha.value.length!=6 )
        {
            error3 = true;
            captcha_text.style.color="red";
        }
        else
        {
            error3 = false;
            captcha_text.style.color="#000";
        }
    }


    if( !error1 && !error2 && !error3 ) 
    {
        change_element_type('submit','submit');
    }

}

