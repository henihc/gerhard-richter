function close_books(){

	//hide all the book information rows
	$$('.bookInfo').addClass('hide');
	

	//mouseover add highlight class
	$$('.bookRow').addEvent('mouseover', function(){ 
		this.toggleClass('highlight');
	});

	//mouseout remove highlight class
	$$('.bookRow').addEvent('mouseout', function(){
		this.toggleClass('highlight');
	});

	$$('.bookRow').addEvent('click', function(){
		this.toggleClass('tr-selected');
	});

	//show hide toggle book information rows
	$$('.bookRow').addEvent('click', function(){
		this.getNext().toggleClass('hide');
	});

	$$('.close-lit').addEvent('click', function(){
		this.getParent().getParent().getParent().getParent().toggleClass('hide');
	});

	//styling the close button
	$$('.close-lit').addEvent('mouseover', function(){
		this.setStyle('cursor','pointer');
	});
	
	$$('.close-lit').setStyle('display','block');
}
