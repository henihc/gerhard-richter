/*!
	|	JS
	|	main.navigation.js
	|	Eric Kuhnert
	|	Javascript (Navigation)
*/

main.navigation = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	name: "navigation",
	version: "0.15.10.05",
	debug: false,
	modal: true,
	deviceType: 'desktop',
	
	// properties set at runtime
	scrollDelayedData: [null, 0],

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function () {
		main.log(this.name, this.version);
		// handle events		
		$( window ).resize( this.resize );
		$('#site-navigation div.content').scroll( this.scroll );
		$('#site-navigation-btn').click( this.clickMenuButton );
		$('#site-lang a.caption').click( this.clickLangButton );
		$('#site-lang ul a').click( this.clickLangItems );
		// listeners for expand sub-menu buttons
		$('#site-navigation a.expand').each(
			function( _index ) { $(this).off('click').click( main.navigation.clickSubMenuButton ); }
		);
		$('#site-navigation a.section').each(
			function( _index ) { $(this).off('click').click( main.openUrl ); }
		);
		// initialise
		this.changeDeviceType( main.deviceType, true );
		this.resize();
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	resize: function ( _e ) {
		// main.log('navigation', 'resize()' );
		// deviceType specific resize operations 
		if (main.deviceType == 'mobile') {
			var contentBox = $('#site-navigation div.content');
			contentBox.height( $(window).height() - parseInt($('#site-navigation-btn').outerHeight(), 0) - parseInt(contentBox.css('marginTop'), 0) );
			var siteLangUl = $('#site-lang ul');
			siteLangUl.width( $(window).width() - (2 * (parseInt(siteLangUl.css('marginLeft'), 0) + parseInt(siteLangUl.css('paddingLeft'),0) + parseInt(siteLangUl.css('border-left-width'),0) )) );
		}
		/*** added by Pauls ***/
		if (main.deviceType == 'desktop') {
			var siteLangUl = $('#site-lang ul');
			siteLangUl.width('auto');
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scroll: function ( _e ) {
		// main.log('navigation', 'scroll()' );
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	show: function ( ) {
		main.log('navigation', 'show()' );
		main.hideAllModal();
		if (!$('#site-navigation-btn').hasClass('selected')) {
			$('#site-navigation-btn').addClass('selected');
			$('#site-navigation').removeClass('hidden');
			// scroll menu to top
			$('#site-navigation div.content').scrollTop( 0 );
			// store page's scroll position
			main.modalScrollPos = [$(window).scrollLeft(), $(window).scrollTop()];
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	hide: function ( _force ) {
		main.log('navigation', 'hide()' );
		if ( $('#site-navigation-btn').hasClass('selected') || _force ) {
			$('#site-navigation-btn').removeClass('selected');
			$('#site-navigation').addClass('hidden');
			// restore page's scroll position, if possible
			$(window).scrollLeft( main.modalScrollPos[0] );
			$(window).scrollTop( main.modalScrollPos[1] );
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	changeDeviceType : function ( _type, _forceChange ) {
		if ((main.deviceType != _type) || _forceChange) {
			switch (_type) {
				case 'desktop':
					// switch to desktop mode...
					main.log('navigation', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$( "#site-navigation" ).removeClass("hidden");				// show navigation menu (override)
					$('#site-lang ul').removeClass('hidden');					// show language menu (override)
					$('#site-navigation div.content').css('height', 'auto');	// reset navigation's content div's height
					$('#site-navigation li').css('height', 'auto');				// reset navigation's list items' height
					$('#site-lang li').css('height', 'auto');					// reset language list items' height
					$('#site-navigation li.expanded').removeClass('expanded');	// reset navigation's list items
					$( "#site-lang" ).detach().appendTo("#site-lang-desktop");	// move language menu out of navigation menu
					break;
				case 'mobile':
					// switch to mobile mode...
					main.log('navigation', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$('#site-navigation-btn').removeClass("selected");			// reset navigation button (override)
					$( "#site-navigation" ).addClass("hidden");					// hide navigation menu (override)
					$('#site-navigation li').css('height', '50px');				// reset navigation's list items
					$('#site-lang ul').addClass('hidden');						// hide language menu (override)
					$( "#site-lang" ).detach().appendTo("#site-lang-mobile");	// move language menu into navigation menu
					break;
			}
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickMenuButton: function ( _e ) {
		if (main.deviceType == 'mobile') {
			// show / hide navigation
			($('#site-navigation-btn').hasClass('selected')) ? main.navigation.hide() : main.navigation.show();
			// reset some navigation content
			$('#site-lang ul').addClass('hidden');
		}
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickSubMenuButton: function ( _e ) {
		if (main.deviceType == 'mobile') {
			var li = $(_e.target).parents('li');
			if (li.hasClass('expanded')) { 
				li.removeClass('expanded') 
				li.height( 50 );
			} else {
				li.addClass('expanded');
				li.height( li.find('ul[data-nav-type="sub"]').outerHeight( true ) + 50 );
				// scroll to sub-items
				$(_e.target).parents('div.content').scrollTop( li.offset().top - 100);
				main.navigation.scrollDelayedData = [$(_e.target).parents('div.content'), li ];
				window.setTimeout(main.navigation.scrollDelayed, 151);
			}
			li.siblings('li').removeClass('expanded');
			li.siblings('li').height( 50 );
		}
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scrollDelayed: function ( ) {
		if (main.deviceType == 'mobile') {
			main.log('navigation', 'scrollDelayed()' );
			if (main.navigation.scrollDelayedData[0] !== null) {
				main.navigation.scrollDelayedData[0].animate( { 
					scrollTop: main.navigation.scrollDelayedData[1].offset().top - main.navigation.scrollDelayedData[0].offset().top + main.navigation.scrollDelayedData[0].scrollTop() - 1
				}, 150 );
			}
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickLangButton: function ( _e ) {
		if (main.deviceType == 'mobile') {
			var ul = $('#site-lang ul');
			(ul.hasClass('hidden')) ? ul.removeClass('hidden') : ul.addClass('hidden');
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickLangItems: function ( _e ) {
		var li = $(_e.target).parents('li');
		var evt_deleg = false;
		if (li.hasClass('selected')) { 
			li.removeClass('selected') 
		} else {
			li.addClass('selected');
			$('#site-lang a.caption').text( li.find('a').text() );
			main.navigation.clickLangButton( _e );
			evt_deleg = main.openUrl( _e );
		}
		li.siblings('li').removeClass('selected');
		return evt_deleg;
	},
	
};