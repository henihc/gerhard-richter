/*!
	|	JS
	|	main.collage.js
	|	Eric Kuhnert
	|	Javascript (Collage, Home)
*/

main.collage = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	name: "collage",
	version: "0.15.10.05",
	debug: false,
	modal: false,
	nodes: [],
	url_patterns: ['xlarge', 'medium'],

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function () {
		main.log(this.name, this.version);
		// find .collage nodes and process their data
		this.nodes = $(".collage");
		$.each( this.nodes, function (_index, _node) {
			$(_node).attr('data-selected', 0);
			var anchors = $(_node).find('a');
			$.each( anchors, function (_ianc, _nanc ) {
				main.log('collage', 'init(): collage node ' + (_index + 1) + ' anchor ' + (_ianc + 1) + '/' + anchors.length);
				// load low resolution images first (see main.collage.url_patterns)
				$(_nanc).attr("data-count", _ianc);
				var url =  $(this).attr('data-img').replace(main.collage.url_patterns[0], main.collage.url_patterns[1]);
				console.log(url);
				var node = $(_nanc).find('.img');
				node.img = new Image();
				node.img.node = node;
				$(node.img).load( function ( _e ) {
					$(this.node).css('backgroundImage', 'url("' +  this.src + '")' );
					$(this.node).addClass('loaded');
				});
				node.img.src = url;
			});
		});
		// handle events
		$( window ).resize( this.resize );
		$( "#main .collage .information .next" ).click( this.next );
		$( "#main .collage .information .prev" ).click( this.prev );
		$( "#main .collage" ).on( "swipe", this.swipe );
		// initialise
		this.changeDeviceType( main.deviceType, true );
		this.resize();
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	resize: function ( _e ) {
		main.log('collage', 'resize()' );
		// deviceType specific resize operations
		if (main.deviceType == 'mobile') {
			$('#main .collage .information').each( function ( _index, _node ) {
				$(_node).width( $(window).width() );
			} );
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scroll: function ( _e ) {
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	loadHighRes: function ( _anchor ) {
		main.log('collage', 'loadHighRes()' );
		// load high resolution images (see main.collage.url_patterns)
		var url =  $(_anchor).attr('data-img');
		var node = $(_anchor).find('.img');
		// load, if need be
		if ($(node).css('backgroundImage') != 'url("' +  url + '")') {
			node.img = new Image();
			node.img.node = node;
			$(node.img).load( function ( _e ) {
				$(this.node).css('backgroundImage', 'url("' +  this.src + '")' );
				$(this.node).addClass('loaded');
			});
			node.img.src = url;
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	next: function ( _e ) {
		main.log('collage', 'next()' );
		var collage = $(_e.target).parents('.collage');
		var current = $(collage).find('a.selected');
		current.removeClass('selected');
		var next = current.next();
		// use max set in attribute "data-slideshow-count" or end of anchor list
		if (((collage.attr("data-slideshow-count")) && (parseInt(next.attr("data-count")) >= parseInt(collage.attr("data-slideshow-count")))) || (next.length == 0)) {
			next = $($(collage).find('a')[0]);
		}
		next.addClass('selected');
		main.collage.loadHighRes(next);
		collage.find('.information .title').text( next.attr('title') );
		collage.find('.information .data').text( next.attr('data-info') );
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	prev: function ( _e ) {
		main.log('collage', 'prev()' );
		var collage = $(_e.target).parents('.collage');
		var current = $(collage).find('a.selected');
		current.removeClass('selected');
		var prev = current.prev();
		if (prev.length == 0) {
			if (collage.attr("data-slideshow-count")) {
				// use max set in attribute "data-slideshow-count"
				prev = $(collage).find("[data-count=" + (parseInt(collage.attr("data-slideshow-count")) - 1) + "]");
			} else {
				var anchors = $(collage).find('a');
				prev = $(anchors[anchors.length - 1]);
			}
		}
		prev.addClass('selected');
		main.collage.loadHighRes(prev);
		collage.find('.information .title').text( prev.attr('title') );
		collage.find('.information .data').text( prev.attr('data-info') );
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	swipe: function ( _e ) {
		main.log('collage', 'swipe()' );
		if (_e.swipestop)
			((_e.swipestop.coords[0] - _e.swipestart.coords[0]) > 0) ? main.collage.prev( _e ) : main.collage.next( _e );
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	changeDeviceType : function ( _type, _forceChange ) {
		if ((main.deviceType != _type) || _forceChange) {
			switch (_type) {
				case 'desktop':
					// switch to desktop mode...
					main.log('collage', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					break;
				case 'mobile':
					main.log('collage', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$.each( this.nodes, function (_index, _node ) {
						// select first anchor
						var anchors = $(_node).find('a');
						anchors.removeClass('selected');
						$(anchors[0]).addClass('selected');
						// load high-resolution images
						$.each( anchors, function (_index, _node) {
							//main.collage.loadHighRes(_node);

							if (_index <= parseInt( $($('.collage')[0]).attr("data-slideshow-count") )) main.collage.loadHighRes(_node);

						});
					});
					break;
			}
		}
	}

};
