/*
	|	JS
	|	main.js
	|	Eric Kuhnert
	|	Javascript (Core)
*/

var main = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	name: "main",
	version: "0.15.10.05",
	debug: false,
	path: '/js/mobile/menu/js/',
	deviceType: 'desktop',
	deviceTypeProfiles: {
		/*
		'desktop':	{ 'minWidth': 1001,	'maxWidth': 10000 },
		'mobile':	{ 'minWidth': 0,	'maxWidth': 1000 },
		*/
		'desktop':	{ 'minWidth': 960,	'maxWidth': 10000 },
		'mobile':	{ 'minWidth': 0,	'maxWidth': 959 },
	},

	// properties set at runtime
	modalScrollPos: [0, 0],

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	modules: {
		items: [],

		// .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
		load: function ( _name ) {
			$.getScript( main.path + main.name + '.' + _name + '.js', function( data, textStatus, jqxhr ) {
				if (typeof(main[_name].init) == 'function') {
					main.modules.items.push(main[_name]);	// register
					main[_name].init();
				} else {
					main.log( 'core.module', 'load(): cannot initialise module "' + _name + '".' );
				}
			}).fail(function( jqxhr, settings, exception ) {
			    main.log( 'core.module', 'load(): getScript failed.' );
			});
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	log: function (_context, _message) {
		if (this.debug && console && (typeof(console.log) == 'function'))
			if (typeof(_message) == 'string') {
				console.log( (Date.now()) + ' ' + this.name + '.' + _context + ": " + _message);
			} else {
				console.log( (Date.now()) + ' ' + this.name + '.' + _context + ": ");
				console.log(_message);
			}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function () {
		this.log('', this.version);
		// handle events
		$( window ).scroll( this.scroll );
		$( window ).resize( this.resize );
		// initialise
		this.changeDeviceType( this.deviceType, true );
		this.resize();
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	resize: function ( _e ) {
		// main.log('core', 'resize()' );
		var width = $(window).width();
		for (profile in main.deviceTypeProfiles) {
			if (( main.deviceTypeProfiles[profile].minWidth <= width) && (width <= main.deviceTypeProfiles[profile].maxWidth )) main.changeDeviceType(profile, false);
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scroll: function ( _e ) {
		//main.log('core', 'scroll()' );
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	changeDeviceType : function ( _type, _forceChange ) {
		if ((this.deviceType != _type) || _forceChange) {
			switch (_type) {
				case 'desktop':
					// switch to desktop mode...
					main.log('core', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$( "#site-lang" ).detach().appendTo("#site-lang-desktop");
					// call all modules' changeDeviceType functions...
					for (index in main.modules.items) {
						if (main.modules.items[index].modal) {
							if (typeof(main.modules.items[index].changeDeviceType) == 'function') main.modules.items[index].changeDeviceType( _type, _forceChange );
						}
					}
					this.deviceType = _type;
					break;
				case 'mobile':
					// switch to mobile mode...
					main.log('core', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$( "#site-lang" ).detach().appendTo("#site-lang-mobile");
					// call all modules' changeDeviceType functions...
					for (index in main.modules.items) {
						if (main.modules.items[index].modal) {
							if (typeof(main.modules.items[index].changeDeviceType) == 'function') main.modules.items[index].changeDeviceType( _type, _forceChange );
						}
					}
					this.deviceType = _type;
					break;
				default:
					main.log('core', 'changeDeviceType(): unknown deviceType "' + _type + '"' );
			}
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	hideAllModal: function ( ) {
		// call all modules' hide() function for modules with property 'modal' set to true
		main.log('core', 'hideAll()' );
		for (index in main.modules.items) {
			if (main.modules.items[index].modal) {
				if (typeof(main.modules.items[index].hide) == 'function') main.modules.items[index].hide();
			}
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	openUrl: function ( _e ) {
		if (main.deviceType == 'desktop') {
			// desktop profile: no handling through javascript
			//alert('DESKTOP: This would follow the original link "' + $(_e.target).attr('href') +'" ignoring jquery event handler.');	// REMOVE LINE FOR DEPLOYMENT
			return true; // proceed with event delegation

		} else {
			// mobile profile: some events handled through javascript
			//alert('MOBILE: This would open the "' + $(_e.target).html() +'" page via jquery event handler.');	// REMOVE LINE FOR DEPLOYMENT
			// navigation menu's section/sub-section link
			if ($(_e.target).hasClass('section') && ( $(_e.target).parents('#site-navigation').length > 0 )) {
				main.log('core', 'openUrl(): [site-navigation] –> "' + $(_e.target).attr('href') + '"');
				 return true;  // proceed with event delegation	// UNCOMMENT LINE FOR DEPLOYMENT
			}
			// language menu's link
			if ( $(_e.target).parents('#site-lang').length > 0 ) {
				main.log('core', 'openUrl(): [site-lang] –> "' + $(_e.target).attr('href') + '"');
				 return true;  // proceed with event delegation	// UNCOMMENT LINE FOR DEPLOYMENT
			}
			return false; // stop event delegation by default
		}

		// SUGGESTION
		// For a modern look and feel you might consider introducing an ajax/ajaj-based page loader, one which re-populates the DOM with the new page's data.
		// It would be triggered by this function.
	},

};


// .......................................................................................

jQuery(document).ready( function($) {
	// initialise main core
	main.init();
	// load additional modules
	main.modules.load( 'navigation' );
	main.modules.load( 'search' );
	main.modules.load( 'collage' );
} );
