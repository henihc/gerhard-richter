/*!
	|	JS
	|	main.search.js
	|	Eric Kuhnert
	|	Javascript (Search)
*/

main.search = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	name: "search",
	version: "0.15.10.05",
	debug: false,
	modal: true,

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	init : function () {
		main.log(this.name, this.version);
	
		// handle events		
		$( window ).resize( this.resize );
		$('#site-search div.content').scroll( this.scroll );
		$('#site-search-btn').click( this.clickMenuButton );
		
		$('#site-search a.section').click( this.clickSubMenuButton );
		$('#site-search a.expand').click( this.clickSubMenuButton );
		
		// initialise
		this.changeDeviceType( main.deviceType, true );
		this.resize();
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	resize: function ( _e ) {
		// main.log('search', 'resize()' );
		// deviceType specific resize operations 
		if (main.deviceType == 'mobile') {
			var contentBox = $('#site-search div.content');
			contentBox.height( $(window).height() - parseInt($('#site-search-btn').outerHeight(), 0) - parseInt(contentBox.css('marginTop'), 0) );
			
			// quick search input
			var searchInput = $('#input-basic-search-top-mobile');
			searchInput.width( $(window).width() - (2 * (parseInt(searchInput.css('marginLeft'), 0) + parseInt(searchInput.css('paddingLeft'),0) + parseInt(searchInput.css('border-left-width'),0) )) );
			
			// section from inputs
			var searchInputsection = $('#form-right-search-mobile input[type="text"]');
			var searchInputsection_width = $(window).width() - 41 - (2 * (parseInt(searchInputsection.css('marginLeft'), 0) + parseInt(searchInputsection.css('paddingLeft'),0) + parseInt(searchInputsection.css('border-left-width'),0) ));
			searchInputsection.width( searchInputsection_width );
			
			// section search reset and submit button
			var searchButtonssection_width = ( searchInputsection_width/2 );
			var searchButtonResetsection = $('#input-right-search-reset-mobile');
			var searchButtonSubmitsection = $('#input-right-search-submit-mobile');
			searchButtonResetsection.width( (searchButtonssection_width-22) );
			searchButtonSubmitsection.width( (searchButtonssection_width-22) );

			// section from inputs small - two in one line
			var searchInputsectionsmall = $('#form-right-search-mobile input[type="text"].input-text-small');
			searchInputsectionsmall.width( (searchButtonssection_width-34) );

			// section from select small - two in one line
			if( $('#form-right-search-mobile .select-field').length ) $('#form-right-search-mobile .select-field').selectBoxIt({ autoWidth: false });
			var searchSelectsectionsmall = $('#form-right-search-mobile .select-small, #year-from-mobileSelectBoxItOptions, #year-to-mobileSelectBoxItOptions');
			searchSelectsectionsmall.width( (searchButtonssection_width-21) );

			// section from select
			var searchSelectsection = $('#form-right-search-mobile .select-large, #artworkid_art-mobileSelectBoxItOptions, #colorid_art-mobileSelectBoxItOptions, #books_catid_book-mobileSelectBoxItOptions, #languageid_book-mobileSelectBoxItOptions, #articles_catid_articles-mobileSelectBoxItOptions, #languageid_articles-mobileSelectBoxItOptions');
			searchSelectsection.width( (searchInputsection_width) );

		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scroll: function ( _e ) {
		// main.log('search', 'scroll()' );
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	show: function ( ) {
		main.log('search', 'show()' );
		main.hideAllModal();
		if (!$('#site-search-btn').hasClass('selected')) {
			$('#site-search-btn').addClass('selected');
			$('#site-search').removeClass('hidden');
			// scroll menu to top
			$('#site-search div.content').scrollTop( 0 );
			// store page's scroll position
			main.modalScrollPos = [$(window).scrollLeft(), $(window).scrollTop()];
			//$('#site-search div.content input:first-child').focus();
			$('#input-basic-search-top-mobile').focus();
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	hide: function ( ) {
		main.log('search', 'hide()' );
		if ($('#site-search-btn').hasClass('selected')) {
			$('#site-search-btn').removeClass('selected');
			$('#site-search').addClass('hidden');
			// restore page's scroll position, if possible
			$(window).scrollLeft( main.modalScrollPos[0] );
			$(window).scrollTop( main.modalScrollPos[1] );
		}
		return false;
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	changeDeviceType : function ( _type, _forceChange ) {
		if ((main.deviceType != _type) || _forceChange) {
			switch (_type) {
				case 'desktop':
					// switch to desktop mode...
					main.log('search', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$('#site-search div.content').css('height', 'auto');	// reset search's content div's height
					$('#site-search li').css('height', 'auto');				// reset search's list items
					$('#site-search li.expanded').removeClass('expanded');	// reset search's list items' height
					$('#site-search').removeClass("hidden");				// show search menu (override)
					// reset elements possibly modified by clickSubMenuButton()
					$('#form-basic-search-top').removeClass('hidden');
					$('#input-basic-search-top').css('width', '');
					break;
				case 'mobile':
					main.log('search', 'changeDeviceType(): switching behavior to deviceType profile "' + _type + '"' );
					$('#site-search-btn').removeClass("selected");			// reset search button (override)
					$('#site-search').addClass("hidden");					// hide search menu (override)
					// reset elements possibly modified by clickSubMenuButton()
					$('#site-search li').css('height', 'auto');
					$('#site-search ul[data-nav-type="main"] > li').css('height', '50px');
					var ul = $('#site-search ul[data-nav-type="main"]');
					if (ul.attr('data-margin-top')) ul.css('marginTop', ul.attr('data-margin-top'));
					break;
			}
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickMenuButton: function ( _e ) {
		($('#site-search-btn').hasClass('selected')) ? main.search.hide() : main.search.show();
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	clickSubMenuButton: function ( _e ) {
		if (main.deviceType == 'mobile') {
			var li = $(_e.target).parents('li');
			if (li.hasClass('expanded')) { 
				li.removeClass('expanded') 
				li.height( 50 );
				$('#form-basic-search-top-mobile').removeClass('hidden');
				var ul = $('#site-search ul[data-nav-type="main"]');
				ul.css('marginTop', ul.attr('data-margin-top'));

			} else {
				li.addClass('expanded');
				li.height( li.find('ul[data-nav-type="sub"]').outerHeight( true ) + 50 );
				$('#form-basic-search-top-mobile').addClass('hidden');
				var ul = $('#site-search ul[data-nav-type="main"]');
				if (ul.css('marginTop') != '0px') ul.attr('data-margin-top', ul.css('marginTop'));
				ul.css('marginTop', 0);
				// scroll to sub-items
				$(_e.target).parents('div.content').scrollTop( li.offset().top - 100);
				main.search.scrollDelayedData = [$(_e.target).parents('div.content'), li ];
				window.setTimeout(main.search.scrollDelayed, 151);
			}
			li.siblings('li').removeClass('expanded');
			li.siblings('li').height( 50 );
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	scrollDelayed: function ( ) {
		if (main.deviceType == 'mobile') {
			main.log('navigation', 'scrollDelayed()' );
			if (main.search.scrollDelayedData[0] !== null) {
				main.search.scrollDelayedData[0].animate( { 
					scrollTop: main.search.scrollDelayedData[1].offset().top - main.search.scrollDelayedData[0].offset().top + main.search.scrollDelayedData[0].scrollTop() - 1
				}, 150 );
			}
		}
	},
	

// 


};