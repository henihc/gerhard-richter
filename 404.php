<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");
$html = new html_elements;
$html->title=LANG_TITLE_HOME;
$html->head('home');

?>
      <div id="intro" class="errorBg">
        <p class="intro1 error404">404 Error</p>
        <p class="intro1 errorText">Sorry but this page no longer exists. <br>
<a href="/" title="Go to www.Gerhard-Richter.com homepage">Click here to continue to www.Gerhard-Richter.com</a></p>
      </div>
      <div id="collage">
        <a href="/art" >
          <img src="/g/home/home_collage.jpg" alt="Continue to Gerhard Richter art" />
        </a>
      </div>
<?php /*?> <ul id='home-blocks'>
				<li>
					<a class='artwork-block' href='art/'><em><?=LANG_HEADER_MENU_ARTWORK?></em><br /><?=LANG_HOME_TEXT_ARTWORK?></a>
				</li>
				<li>
					<a class='biography-block' href='biography/'><em><?=LANG_HEADER_MENU_BIOGRAPHY?></em><br /><?=LANG_HOME_TEXT_BIOGRAPHY?></a>
				</li>
				<li>
					<a class='exhibitions-block' href='exhibitions/'><em><?=LANG_HEADER_MENU_EXHIBITIONS?></em><br /><?=LANG_HOME_TEXT_EXHIBITIONS?></a>
				</li>
				<li>
					<a class='literature-block' href='literature/'><em><?=LANG_HEADER_MENU_LITERATURE?></em><br /><?=LANG_HOME_TEXT_LITERATURE?></a>
				</li>
				<li>
					<a class='videos-block' href='videos/'><em><?=LANG_HEADER_MENU_VIDEOS?></em><br /><?=LANG_HOME_TEXT_VIDEOS?></a>
				</li>
			</ul><?php */
?>
<?php
$html -> foot();
?>
