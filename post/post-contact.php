<?php
session_start();
/*
session_start();
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
*/
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
require_once ($_SERVER["DOCUMENT_ROOT"].'/includes/php_mailer/PHPMailerAutoload.php');

$options_lang_file=array();
$options_lang_file['get_lang']=$_POST['lang'];
$language=UTILS::load_lang_file($db,$options_lang_file);

$_SESSION['form_contact']=$_POST;

$db=new dbCLASS;

$valid=true;
$error="";

if( !empty($_POST['email']) && UTILS::check_email_address($_POST['email']) )
{
    list($Username, $Domain) = explode("@",$_POST['email']);
    if(getmxrr($Domain, $MXHost))
    {
        $valid=true;
    }
    else
    {
        if(@fsockopen($Domain, 25, $errno, $errstr, 30))
        {
            $valid=true;
        }
        else
        {
            $valid=false;
        }
    }

}
else $valid=false;

if( !$valid ) $error.="?error1=1";
if( empty($_POST['message']) )
{
    if( empty($error) ) $error.="?error2=1";
    else $error.="&error2=1";
}
if( $_POST['captcha']!=$_SESSION['captcha'] || empty($_POST['captcha']) || empty($_SESSION['captcha']) )
{
    $valid=false;
    if( empty($error) ) $error.="?error3=1";
    else $error.="&error3=1";
}



if( !empty($_POST['message']) && $valid )
{
    $email=$db->db_replace_quotes($db,$_POST['email']);
    $subject=$db->db_replace_quotes($db,$_POST['subject']);
    $message=$db->db_replace_quotes($db,$_POST['message']);

    $db->query("INSERT INTO ".TABLE_CONTACT."(lang,email,type,message,date_created) VALUES('".$_POST['lang']."','".$email."','".$subject."','".$message."',NOW())");
    $submitid=$db->return_insert_id();

    #file upload
    $file_upload=false;
    if($_FILES['file_attachment']['error']==0)
    {
        $fileName = $_FILES['file_attachment']['name'];
        $tmpName  = $_FILES['file_attachment']['tmp_name'];
        $fileSize = $_FILES['file_attachment']['size'];
        $fileType = $_FILES['file_attachment']['type'];

            if ( is_uploaded_file($tmpName) )
            {
                //$mime = trim(UTILS::get_mime($tmpName));

                $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                $mime=finfo_file($finfo, $tmpName);
                finfo_close($finfo);

                //print "-".$mime."-".$original_extension;
                //exit;

                switch( $mime )
                {
                    case 'image/gif'  : $ext = ".gif"; break;
                    case 'image/png'  : $ext = ".png"; break;
                    case 'image/jpeg' : $ext = ".jpg"; break;
                    case 'image/bmp'  : $ext = ".bmp"; break;
                    case 'image/tiff'  : $ext = ".tiff"; break;
                    case 'image/tif'  : $ext = ".tif"; break;
                    case 'image/photoshop'  : $ext = ".psd"; break;
                    # pdf
                    case 'application/pdf'  : $ext = ".pdf"; break;
                    case 'application/x-pdf'  : $ext = ".pdf"; break;
                    # word docs
                    case 'application/msword'  : $ext = ".doc"; break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'  : $ext = ".docx"; break;
                    # Excel
                    case 'application/x-zip'  : $ext = ".xlsx"; break;
                    case 'application/vnd.ms-excel'  : $ext = ".xls"; break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  : $ext = ".xlsx"; break;
                    # Open Office word doc
                    case 'application/vnd.oasis.opendocument.text '  : $ext = ".odt"; break;
                    # Open Office excel doc
                    case 'application/vnd.oasis.opendocument.spreadsheet'  : $ext = ".ods"; break;
                    default : exit("Unsupported file format! Supported formats: JPEG, TIFF, PSD, GIF, PNG, BMP, PDF, DOC, DOCX, XLS, XLSX, ODT, ODS ");
                }

                $ext = (false === $pos = strrpos($fileName, '.')) ? '' : substr($fileName, $pos);

                if( $_FILES['file']['size'] >15360 ) exit("File too large max file size 15MB");

                $dir=DATA_PATH."/files_contact_form";
                $new_file_path=$dir."/".$submitid.$ext;

                $file_upload=true;
                #copy original
                if (!copy($tmpName, $new_file_path))
                {
                    exit("Error Uploading File.");
                    $file_upload=false;
                }
                else
                {
                    $filename_old=str_replace ($ext, "", $fileName);
                    $options_filename=array();
                    $options_filename['text']=$filename_old;
                    $options_filename['delimiter']="_";
                    $filename_old=UTILS::replace_accented_letters($db, $options_filename);
                    $filename_old=$filename_old['text_lower_converted'].$ext;
                    $db->query("UPDATE ".TABLE_CONTACT." SET src_old='".$filename_old."', src='".$submitid.$ext."' WHERE id='".$submitid."'");
                    $file_upload=true;
                }
            }
    }
    # END file upload


        $mail = new PHPMailer();
        $mail->IsSMTP();
        //$mail->SMTPDebug  = 2;
        $mail->Host       = EMAIL_INFO_HOST;
        $mail->Port       = EMAIL_INFO_PORT;
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = "tls";
        $mail->Username   = EMAIL_INFO;
        $mail->Password   = EMAIL_INFO_PASSWD;
        //$mail->Password   = "fgdfgfdgdfg";
        $mail->CharSet = DB_ENCODEING;
        //$mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_FULL);
        $mail->AddAddress( EMAIL_INFO_TO , EMAIL_INFO_TO_FULL);
        $mail->AddReplyTo($_POST['email'], $_POST['email']);
        $mail->SetFrom(EMAIL_INFO, EMAIL_INFO_FULL);
        $mail->Subject = $_POST['subject'];
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
        $mail->MsgHTML($_POST['message']);
        #AddAttachment(PATH_TO_FILE, FILENAME, ENCODING, HEADER_TYPE)
        //if( $file_upload ) $mail->AddAttachment($new_file_path, $submitid.$ext);
        if( $file_upload ) $mail->AddAttachment($new_file_path, $filename_old);
        if( !$mail->Send() )
        {
            # if email sending failed send email to admin iforming about this error
            $mail = new PHPMailer();
            $mail->IsSMTP();
            //$mail->SMTPDebug  = 2;
            $mail->Host       = EMAIL_ADMIN_HOST;
            $mail->Port       = EMAIL_ADMIN_PORT;
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = "tls";
            $mail->Username   = EMAIL_ADMIN;
            $mail->Password   = EMAIL_ADMIN_PASSWORDS;
            $mail->CharSet = DB_ENCODEING;
            $mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_FULL);
            $mail->AddReplyTo($_POST['email'], $_POST['email']);
            $mail->SetFrom(EMAIL_ADMIN, EMAIL_ADMIN_FULL);
            $mail->Subject = "FAIL - ".$_POST['subject'];
            $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
            $mail->MsgHTML("<p><h1>".DOMAIN." - contact form FAILED to send email</h1></p><br />".$_POST['message']);
            if( $file_upload ) $mail->AddAttachment($new_file_path, $filename_old);
            $mail->Send();
            # END
        }
        
        if ($mail->SMTPDebug == 2){
            $myfile = fopen("phpmailerdebug.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $mail->Debugoutput);
            fclose($myfile);
        }
        

    unset($_SESSION['form_contact']);

    UTILS::redirect("/".$_POST['lang']."/contact/thank-you");
}
else
{
    UTILS::redirect("/".$_POST['lang']."/contact/".$error);
}
?>
