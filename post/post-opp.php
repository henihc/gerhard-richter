<?php
session_start();
/*
session_start();
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");
*/
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
require_once ($_SERVER["DOCUMENT_ROOT"].'/includes/php_mailer/PHPMailerAutoload.php');
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/phpip2country.class.php");

$options_lang_file=array();
$options_lang_file['get_lang']=$_POST['lang'];
$language=UTILS::load_lang_file($db,$options_lang_file);

$_SESSION['form_opp']=$_POST;

$valid=true;
$error=array();

# error code 0 = email error
# error code 1 = email domain error
# error code 2 = captcha error

if ( !empty($_POST['email']) && UTILS::check_email_address($_POST['email']) )
{
    list($Username, $Domain) = explode("@",$_POST['email']);
    if(getmxrr($Domain, $MXHost))
    {
        $valid=true;
    }
    else
    {
        if(@fsockopen($Domain, 25, $errno, $errstr, 30))
        {
            $valid=true;
        }
        else
        {
            $valid=false;
            $error[1]=1;
        }
    }
}
else
{
    $valid=false;
    $error[0]=1;
}



//print_r($_SESSION);
//print "<br />";
//print_r($_POST);
//print_r($_FILES);
//exit;
//$_POST['captcha']="dfdgdhgfdhfgjhgf";


//$url_error="/art/overpainted-photographs/?";
$ip_address=UTILS::get_ip();
$url_error=$_SERVER['HTTP_REFERER']."?";
$link="";
if( !$valid )
{
    $url_error.="error_email=1";
    $link.="#email";
}
if( $_POST['captcha']!=$_SESSION['captcha'] || empty($_POST['captcha']) || empty($_SESSION['captcha']) )
{
    $valid=false;
    $url_error.="&error_code=1";
    if( empty($link) ) $link.="#codenumb";
    $error[2]=1;
}
if( $ip_address=="5.39.219.26" )
{
    $valid=false;
    $url_error.="error=1";
    $error[3]=1;
    $link.="";
}
$url_error.=$link;

if( !$error[2] )
{

$db=new dbCLASS;

/*
print "before-".$_POST['title']."<br />";
$title2=$db->db_replace_quotes2($db,$_POST['title']);
$title=$db->db_replace_quotes($db,$_POST['title']);
$title3=mysql_real_escape_string($_POST['title']);
$title4=str_replace("'", "&#39;", stripslashes($_POST['title']));
$title5=str_replace("/\'/g", "&#39;",$_POST['title']);

$title6=htmlentities($_POST['title'],ENT_QUOTES);                                // bob&#039;s apples
$title7=html_entity_decode(htmlentities($_POST['title'],ENT_QUOTES),ENT_QUOTES);
$title8=htmlspecialchars($_POST['title'], ENT_QUOTES);


print "after2-".$title2."<br />";
print "after-".$title."<br />";
print "after3-".$title3."<br />";
print "after4-".$title4."<br />";
print "after5-".$title5."<br />";
print "after6-".$title6."<br />";
print "after7-".$title7."<br />";
print "after8-".$title8."<br />";
*/

$_POST=$db->db_prepare_input($_POST,1);
//print_r($_POST);
//print "<br />";
$_POST=$db->db_replace_quotes($db,$_POST);
//print_r($_POST);



if( !$valid )
{
    # error
    $status=0;
}
else
{
    # valid
    $status=1;
}

    $i=0;
    $exhibition_history="";
    if( is_array($_POST['exhibition_history']) )
    {
        foreach($_POST['exhibition_history'] as $value)
        {
            $i++;
            if( $i>1 ) $exhibition_history.="<br />";

            $exhibitionid=UTILS::itemid($db,$value);
            $query_where_exhibition=" WHERE exID='".$exhibitionid."' ";
            $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition);
            $results_exhibition=$db->query($query_exhibition['query']);
            $count_exhibition=$db->numrows($results_exhibition);
            $row_exhibition=$db->mysql_array($results_exhibition,0);
            $title_exhibition=$row_exhibition['title_original'];
            $options_location=array();
            $options_location['row']=$row_exhibition;
            $location_exhibition=location($db,$options_location);
            $date_exhibition=exhibition_date($db,$row_exhibition);

            $exhibition_history.=$title_exhibition;
            if( !empty($location_exhibition) )
            {
                $exhibition_history.=", ".$location_exhibition.", ";
                $exhibition_history.=$date_exhibition;
            }

            //$exhibition_history.=$value;
        }
    }
    //$exhibition_history=stripslashes(utf8_decode($exhibition_history));
    $exhibition_history=stripslashes($exhibition_history);

    $i=0;
    $publication_history="";
    if( is_array($_POST['publication_history']) )
    {
        foreach($_POST['publication_history'] as $value)
        {
            $i++;
            if( $i>1 ) $publication_history.="<br />";

            $bookid=UTILS::itemid($db,$value);
            $query_where_book=" WHERE id='".$bookid."' ";
            $query_book=QUERIES::query_books($db,$query_where_book);
            $results_book=$db->query($query_book['query']);
            $count_book=$db->numrows($results_book);
            $row_book=$db->mysql_array($results_book,0);
            $title_book=$row_book['title'];
            $author_book=$row_book['author'];
            $publisher_book=$row_book['publisher'];
            $date_book=$row_book['date_display_year'];

            $publication_history.=$title_book;
            if( !empty($author_book) )
            {
                $publication_history.=" (".$author_book.").";
                if( !empty($publisher_book) )
                {
                    $publication_history.=" ".$publisher_book.",";
                }
            }
            $publication_history.=" ".$date_book;

            //$publication_history.=$value;
        }
    }

    $auth=UTILS::r_string(20);

    $db->query("INSERT INTO ".TABLE_FORM_OPP."(
        auth,
        codenumb,
        ip,
        lang,
        title,
        date,
        series,
        editions_number,
        medium,
        dimensions_photo_height,
        dimensions_photo_width,
        dimensions_photo_type,
        dimensions_mount_height,
        dimensions_mount_width,
        dimensions_mount_type,
        optionid_recto_signed_1,
        optionid_recto_signed_2,
        recto_signed_3,
        optionid_recto_dated_1,
        optionid_recto_dated_2,
        recto_dated_3,
        optionid_recto_numbered_1,
        optionid_recto_numbered_2,
        recto_numbered_3,
        optionid_recto_inscribed_1,
        optionid_recto_inscribed_2,
        recto_inscribed_3,
        optionid_verso_signed_1,
        optionid_verso_signed_2,
        verso_signed_3,
        optionid_verso_dated_1,
        optionid_verso_dated_2,
        verso_dated_3,
        optionid_verso_numbered_1,
        optionid_verso_numbered_2,
        verso_numbered_3,
        optionid_verso_inscribed_1,
        optionid_verso_inscribed_2,
        verso_inscribed_3,
        inscriptions,
        notations,
        copyright_info,
        provenance,
        acquired_from,
        date_of_acquisition,
        exhibition_history,
        exhibition_history_other,
        publication_history,
        publication_history_other,
        first_name,
        last_name,
        email,
        address,
        country,
        post_code,
        telephone,
        indicate_credit,
        additional_comments,
        send_email,
        status,
        error,
        date_created
    ) VALUES (
        '".$auth."',
        '".$_POST['captcha']."-".$_SESSION['captcha']."',
        '".$ip_address."',
        '".$_POST['lang']."',
        '".$_POST['title']."',
        '".$_POST['date']."',
        '".$_POST['series']."',
        '".$_POST['editions_number']."',
        '".$_POST['medium']."',
        '".$_POST['dimensions_photo_height']."',
        '".$_POST['dimensions_photo_width']."',
        '".$_POST['dimensions_photo_type']."',
        '".$_POST['dimensions_mount_height']."',
        '".$_POST['dimensions_mount_width']."',
        '".$_POST['dimensions_mount_type']."',
        '".$_POST['optionid_recto_signed_1']."',
        '".$_POST['optionid_recto_signed_2']."',
        '".$_POST['recto_signed_3']."',
        '".$_POST['optionid_recto_dated_1']."',
        '".$_POST['optionid_recto_dated_2']."',
        '".$_POST['recto_dated_3']."',
        '".$_POST['optionid_recto_numbered_1']."',
        '".$_POST['optionid_recto_numbered_2']."',
        '".$_POST['recto_numbered_3']."',
        '".$_POST['optionid_recto_inscribed_1']."',
        '".$_POST['optionid_recto_inscribed_2']."',
        '".$_POST['recto_inscribed_3']."',
        '".$_POST['optionid_verso_signed_1']."',
        '".$_POST['optionid_verso_signed_2']."',
        '".$_POST['verso_signed_3']."',
        '".$_POST['optionid_verso_dated_1']."',
        '".$_POST['optionid_verso_dated_2']."',
        '".$_POST['verso_dated_3']."',
        '".$_POST['optionid_verso_numbered_1']."',
        '".$_POST['optionid_verso_numbered_2']."',
        '".$_POST['verso_numbered_3']."',
        '".$_POST['optionid_verso_inscribed_1']."',
        '".$_POST['optionid_verso_inscribed_2']."',
        '".$_POST['verso_inscribed_3']."',
        '".$_POST['inscriptions']."',
        '".$_POST['notations']."',
        '".$_POST['copyright_info']."',
        '".$_POST['provenance']."',
        '".$_POST['acquired_from']."',
        '".$_POST['date_of_acquisition']."',
        '".$exhibition_history."',
        '".$_POST['exhibition_history_other']."',
        '".$publication_history."',
        '".$_POST['publication_history_other']."',
        '".$_POST['first_name']."',
        '".$_POST['last_name']."',
        '".$_POST['email']."',
        '".$_POST['address']."',
        '".$_POST['country']."',
        '".$_POST['post_code']."',
        '".$_POST['telephone']."',
        '".$_POST['indicate_credit']."',
        '".$_POST['additional_comments']."',
        '".$_POST['send_email']."',
        '".$status."',
        '".serialize($error)."',
        NOW()
    )");

$submitid=$db->return_insert_id();

#image upload
$file_upload=false;
$count_files=count($_FILES['image']['error']);
for($i=0;$i<$count_files;$i++ )
{
    if($_FILES['image']['error'][$i]==0)
    {
        $fileName = str_replace (" ", "_", $_FILES['image']['name'][$i]);
        $tmpName  = $_FILES['image']['tmp_name'][$i];
        $fileSize = $_FILES['image']['size'][$i];
        $fileType = $_FILES['image']['type'][$i];

            if ( is_uploaded_file($tmpName) )
            {
                $db->query("INSERT INTO ".TABLE_FORM_OPP_IMAGES."(submitid,date_created) VALUES('".$submitid."',NOW())");
                $imageid=$db->return_insert_id();

                $getimagesize = getimagesize($tmpName);

                switch( $getimagesize['mime'] )
                {
                    case 'image/gif'  : $ext = ".gif"; break;
                    case 'image/png'  : $ext = ".png"; break;
                    case 'image/jpeg' : $ext = ".jpg"; break;
                    case 'image/bmp'  : $ext = ".bmp"; break;
                    case 'image/tiff'  : $ext = ".tiff"; break;
                    case 'image/tif'  : $ext = ".tif"; break;
                    case 'image/photoshop'  : $ext = ".psd"; break;
                    default : exit("Unsupported image format! Supported formats: JPEG, TIFF, PSD, GIF, PNG, BMP");
                }

                $dir=DATA_PATH."/files_opp_form";
                $new_file_path=$dir."/".$imageid.$ext;

                $file_upload=true;
                #copy original
                if (!copy($tmpName, $new_file_path))
                {
                    exit("Error Uploading File.");
                    $file_upload=false;
                }
                else
                {
                    $db->query("UPDATE ".TABLE_FORM_OPP_IMAGES." SET src_old='".$fileName."', src='".$imageid.$ext."' WHERE imageid='".$imageid."'");
                    $file_upload=true;
                }
            }
    }
}
# END image upload

$subject="";
if( !$valid ) $subject.="ERROR - ";
$subject.="Gerhard-Richter.com - Overpainted Photographs form";


/*
    $dbConfigArray = array(
        'host' => DB_HOST, //example host name
        'port' => 3306, //3306 -default mysql port number
        'dbName' => "richter_ip2country", //example db name
        'dbUserName' => DB_USER, //example user name
        'dbUserPassword' => DB_PASSWORD, //example user password
        'tableName' => 'ip2c', //example table name
    );

    $phpIp2Country = new phpIp2Country(UTILS::get_ip(),$dbConfigArray);
    $country=$phpIp2Country->getInfo(IP_COUNTRY_NAME);
    */

$style_th="style='text-align:left;width:304px;vertical-align:top;'";

$message="<table cellspacing='0'>";

$message.="<tr><th ".$style_th.">".LANG_OPP_FORM_104."</th><td>".$submitid."</td></tr>";
if( $_POST['first_name']!="Pauls" && $_POST['last_name']!="Daugerts" ) $message.="<tr><th ".$style_th.">IP address</th><td>".UTILS::get_ip()."</td></tr>";
if( !empty($country) && $_POST['first_name']!="Pauls" && $_POST['last_name']!="Daugerts" ) $message.="<tr><th ".$style_th.">Country</th><td>".$country."</td></tr>";
if( $_POST['agree'] )
{
    $message.="<tr><th ".$style_th.">Terms and conditions agree</th><td>Yes</td></tr>";
}
if( !empty($_POST['title']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_20."</th><td>".$_POST['title']."</td></tr>";
if( !empty($_POST['date']) ) $message.="<tr><th ".$style_th.">".strip_tags(LANG_OPP_FORM_21)."</th><td>".$_POST['date']."</td></tr>";
if( !empty($_POST['series']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_33."</th><td>".$_POST['series']."</td></tr>";
if( !empty($_POST['editions_number']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_39."</th><td>".$_POST['editions_number']."</td></tr>";
if( !empty($_POST['medium']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_22."</th><td>".$_POST['medium']."</td></tr>";
if( !empty($_POST['dimensions_photo_height']) || !empty($_POST['dimensions_photo_width']) || !empty($_POST['dimensions_photo_type']) )
{
    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_27."</th><td>";
        $message.=$_POST['dimensions_photo_height']." ";
        if( !empty($_POST['dimensions_photo_height']) && !empty($_POST['dimensions_photo_width']) ) $message.="&#215; ";
        $message.=$_POST['dimensions_photo_width']." ";
        $message.=$_POST['dimensions_photo_type'];
    $message.="</td></tr>";
}
if( !empty($_POST['dimensions_mount_height']) || !empty($_POST['dimensions_mount_width']) || !empty($_POST['dimensions_mount_type']) )
{
    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_30."</th><td>";
        $message.=$_POST['dimensions_mount_height']." ";
        if( !empty($_POST['dimensions_mount_height']) && !empty($_POST['dimensions_mount_width']) ) $message.="&#215; ";
        $message.=$_POST['dimensions_mount_width']." ";
        $message.=$_POST['dimensions_mount_type'];
    $message.="</td></tr>";
}
if( !empty($_POST['indicate_credit']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_32."</th><td>".$_POST['indicate_credit']."</td></tr>";
if( !empty($_POST['optionid_recto_signed_1']) || !empty($_POST['optionid_recto_signed_2']) || !empty($_POST['recto_signed_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_signed_1=show_dropdown_option_title($db,$_POST['optionid_recto_signed_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_signed_2=show_dropdown_option_title($db,$_POST['optionid_recto_signed_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_1."</th><td>";
        $message.=$optionid_recto_signed_1." ";
        $message.=$optionid_recto_signed_2." ";
        $message.=$_POST['recto_signed_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_recto_dated_1']) || !empty($_POST['optionid_recto_dated_2']) || !empty($_POST['recto_dated_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_dated_1=show_dropdown_option_title($db,$_POST['optionid_recto_dated_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_dated_2=show_dropdown_option_title($db,$_POST['optionid_recto_dated_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_2."</th><td>";
        $message.=$optionid_recto_dated_1." ";
        $message.=$optionid_recto_dated_2." ";
        $message.=$_POST['recto_dated_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_recto_numbered_1']) || !empty($_POST['optionid_recto_numbered_2']) || !empty($_POST['recto_numbered_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_numbered_1=show_dropdown_option_title($db,$_POST['optionid_recto_numbered_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_numbered_2=show_dropdown_option_title($db,$_POST['optionid_recto_numbered_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_3."</th><td>";
        $message.=$optionid_recto_numbered_1." ";
        $message.=$optionid_recto_numbered_2." ";
        $message.=$_POST['recto_numbered_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_recto_inscribed_1']) || !empty($_POST['optionid_recto_inscribed_2']) || !empty($_POST['recto_inscribed_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_inscribed_1=show_dropdown_option_title($db,$_POST['optionid_recto_inscribed_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_recto_inscribed_2=show_dropdown_option_title($db,$_POST['optionid_recto_inscribed_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_4."</th><td>";
        $message.=$optionid_recto_inscribed_1." ";
        $message.=$optionid_recto_inscribed_2." ";
        $message.=$_POST['recto_inscribed_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_verso_signed_1']) || !empty($_POST['optionid_verso_signed_2']) || !empty($_POST['verso_signed_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_signed_1=show_dropdown_option_title($db,$_POST['optionid_verso_signed_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_signed_2=show_dropdown_option_title($db,$_POST['optionid_verso_signed_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_1."</th><td>";
        $message.=$optionid_verso_signed_1." ";
        $message.=$optionid_verso_signed_2." ";
        $message.=$_POST['verso_signed_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_verso_dated_1']) || !empty($_POST['optionid_verso_dated_2']) || !empty($_POST['verso_dated_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_dated_1=show_dropdown_option_title($db,$_POST['optionid_verso_dated_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_dated_2=show_dropdown_option_title($db,$_POST['optionid_verso_dated_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_2."</th><td>";
        $message.=$optionid_verso_dated_1." ";
        $message.=$optionid_verso_dated_2." ";
        $message.=$_POST['verso_dated_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_verso_numbered_1']) || !empty($_POST['optionid_verso_numbered_2']) || !empty($_POST['verso_numbered_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_numbered_1=show_dropdown_option_title($db,$_POST['optionid_verso_numbered_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_numbered_2=show_dropdown_option_title($db,$_POST['optionid_verso_numbered_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_3."</th><td>";
        $message.=$optionid_verso_numbered_1." ";
        $message.=$optionid_verso_numbered_2." ";
        $message.=$_POST['verso_numbered_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['optionid_verso_inscribed_1']) || !empty($_POST['optionid_verso_inscribed_2']) || !empty($_POST['verso_inscribed_3']) )
{
    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_inscribed_1=show_dropdown_option_title($db,$_POST['optionid_verso_inscribed_1'],$options_show_dropdown_title);

    $options_show_dropdown_title=array();
    $options_show_dropdown_title['html_return']=1;
    $optionid_verso_inscribed_2=show_dropdown_option_title($db,$_POST['optionid_verso_inscribed_2'],$options_show_dropdown_title);

    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_4."</th><td>";
        $message.=$optionid_verso_inscribed_1." ";
        $message.=$optionid_verso_inscribed_2." ";
        $message.=$_POST['verso_inscribed_3']." ";
    $message.="</td></tr>";
}
if( !empty($_POST['inscriptions']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_53."</th><td>".$_POST['inscriptions']."</td></tr>";
if( !empty($_POST['notations']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_54."</th><td>".$_POST['notations']."</td></tr>";
if( !empty($_POST['copyright_info']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_58."</th><td>".$_POST['copyright_info']."</td></tr>";
if( !empty($_POST['provenance']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_59."</th><td>".$_POST['provenance']."</td></tr>";
if( !empty($_POST['acquired_from']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_60."</th><td>".$_POST['acquired_from']."</td></tr>";
if( !empty($_POST['date_of_acquisition']) ) $message.="<tr><th ".$style_th.">".strip_tags(LANG_OPP_FORM_61)."</th><td>".$_POST['date_of_acquisition']."</td></tr>";
if( !empty($exhibition_history) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_62."</th><td>".$exhibition_history."</td></tr>";
if( !empty($_POST['exhibition_history_other']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_62_1."</th><td>".$_POST['exhibition_history_other']."</td></tr>";
if( !empty($publication_history) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_63."</th><td>".$publication_history."</td></tr>";
if( !empty($_POST['publication_history_other']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_63_1."</th><td>".$_POST['publication_history_other']."</td></tr>";
if( !empty($_POST['first_name']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_65."</th><td>".$_POST['first_name']."</td></tr>";
if( !empty($_POST['last_name']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_66."</th><td>".$_POST['last_name']."</td></tr>";
if( !empty($_POST['email']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_67."</th><td>".$_POST['email']."</td></tr>";
if( !empty($_POST['address']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68."</th><td>".$_POST['address']."</td></tr>";
if( !empty($_POST['post_code']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68_1."</th><td>".$_POST['post_code']."</td></tr>";
if( !empty($_POST['country']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68_2."</th><td>".$_POST['country']."</td></tr>";
if( !empty($_POST['telephone']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_69."</th><td>".$_POST['telephone']."</td></tr>";
if( !empty($_POST['additional_comments']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_70."</th><td>".$_POST['additional_comments']."</td></tr>";
//if( !empty($_POST['']) ) $message.="<tr><th ".$style_th."></th><td>".$_POST['']."</td></tr>";

$message.="</table>";

//print $message;
//exit;

        $mail = new PHPMailer();
        $mail->IsSMTP();
        //$mail->SMTPDebug  = 2;
        $mail->Host       = EMAIL_INFO_HOST;
        $mail->Port       = "25";
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = "tls";
        $mail->Username   = EMAIL_OPP;
        $mail->Password   = EMAIL_OPP_PASSWD;
        $mail->CharSet = DB_ENCODEING;
        //$mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_NAME);
        $mail->AddAddress( EMAIL_OPP , "Gerhard-Richter.com");
        if( $_POST['send_email'] )
        {
            $mail->AddBCC($_POST['email'], $_POST['email']);
        }
        $mail->AddReplyTo($_POST['email'], $_POST['email']);
        //$mail->SetFrom(EMAIL_OPP, $_POST['email']);
        $mail->SetFrom(EMAIL_OPP, EMAIL_OPP_NAME);
        $mail->Subject = $subject;
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
        $mail->MsgHTML($message);
        #AddAttachment(PATH_TO_FILE, FILENAME, ENCODING, HEADER_TYPE)
        if( $file_upload )
        {
            $query_images="SELECT * FROM ".TABLE_FORM_OPP_IMAGES." WHERE submitid='".$submitid."' ";
            $results_images=$db->query($query_images);
            while( $row_images=$db->mysql_array($results_images,0) )
            {
                $new_file_path=$dir."/".$row_images['src'];
                $mail->AddAttachment($new_file_path, $row_images['src']);
            }
        }
        if( !$mail->Send() )
        {
            # if email sending failed send email to admin iforming about this error
            $mail = new PHPMailer();
            $mail->IsSMTP();
            //$mail->SMTPDebug  = 2;
            $mail->Host       = EMAIL_ADMIN_HOST;
            $mail->Port       = EMAIL_ADMIN_PORT;
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = "tls";
            $mail->Username   = EMAIL_ADMIN;
            $mail->Password   = EMAIL_ADMIN_PASSWORDS;
            $mail->CharSet = DB_ENCODEING;
            $mail->AddAddress( EMAIL_ADMIN , EMAIL_ADMIN_FULL);
            $mail->SetFrom(EMAIL_ADMIN, EMAIL_ADMIN_FULL);
            $mail->AddReplyTo($_POST['email'], $_POST['email']);
            $mail->Subject = "FAIL - ".$_POST['subject'];
            $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
            $mail->MsgHTML("<p><h1>".DOMAIN." - OPP form FAILED to send email</h1></p><br />".$message);
            if( $file_upload )
            {
                $query_images="SELECT * FROM ".TABLE_FORM_OPP_IMAGES." WHERE submitid='".$submitid."' ";
                $results_images=$db->query($query_images);
                while( $row_images=$db->mysql_array($results_images,0) )
                {
                    $new_file_path=$dir."/".$row_images['src'];
                    $mail->AddAttachment($new_file_path, $row_images['src']);
                }
            }
            $mail->Send();
            # END
        }


}
//print $message;
        if( !$valid )
        {
            //print $url_error;
            UTILS::redirect($url_error);
        }
        else
        {
            unset($_SESSION['form_opp']);
            $url_redirect="/".$_POST['lang']."/art/overpainted-photographs/thank-you/?submitid=".$submitid."&auth=".$auth;
            //print $url_redirect;
            UTILS::redirect($url_redirect);
        }

?>
