<?php
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

switch ($_GET['section_0']) {
    case "":
        //require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/home/index.php");
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/home/html/home.html");
        break;
    case "art":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/art/index.php");
        break;
    case "biography":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/biography/index.php");
        break;
    case "chronology":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/chronology/index.php");
        break;
    case "quotes":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/quotes/index.php");
        break;
    case "exhibitions":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/exhibitions/index.php");
        break;
    case "literature":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/literature/index.php");
        break;
    case "videos":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/videos/index.php");
        break;
    case "audio":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/audio/index.php");
        break;
    case "news":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/news/index.php");
        break;
    case "search":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/search/index.php");
        break;
    case "auctions":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/auctions/index.php");
        break;
    case "credits":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/credits/index.php");
        break;
    case "disclaimer":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/disclaimer/index.php");
        break;
    case "contact":
        require ($_SERVER["DOCUMENT_ROOT"]."/includes/templates/contact/index.php");
        break;
    default:
        UTILS::not_found_404();
}?>
