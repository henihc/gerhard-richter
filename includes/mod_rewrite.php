<?php
$db=new dbCLASS;

$mod_rewrite=explode("/", $_GET['mod_rewrite']);
$mod_count=count($mod_rewrite);

# languages the website can deliver content for
$supported_languages = array("en", "de", "fr", "it", "zh");

# set website's default language
$_GET['lang'] = "en";

# get browser's default language
 $browser_language = get_browsers_default_language($_SERVER['HTTP_ACCEPT_LANGUAGE']);
//$browser_language = "en";

# if lang provided wthin url
if( in_array($mod_rewrite[0], $supported_languages) || in_array($_POST['lang'], $supported_languages) ) {
    $_GET['lang']=$mod_rewrite[0];
}
else
{
    $_GET['lang']=$browser_language;
}

# load correct lang file
$options_lang_file=array();
$options_lang_file['get_lang']=$_GET['lang'];
$language=UTILS::load_lang_file($db,$options_lang_file);
# END COUNTRY LOCALE LANGUAGE

# if no lang provided redirect to correct lang
if( $mod_rewrite[0]=="cn" )
{
    # old chinese lang value CN redirect to new value ZH
    $url_current=UTILS::replace_lang_for_link($db,$_SERVER['REQUEST_URI'],"zh");
    if( $_SERVER['REQUEST_URI']!="/" ) UTILS::redirect(HTTP_SERVER.$url_current,301);
}
elseif( !in_array($mod_rewrite[0], $supported_languages) )
{
    $url_current="/".$_GET['lang'].$_SERVER['REQUEST_URI'];
    if( $_SERVER['REQUEST_URI']!="/" ) UTILS::redirect(HTTP_SERVER.$url_current,301);
}
elseif( empty($mod_rewrite[0]) )
{
    UTILS::redirect(HTTP_SERVER."/".$_GET['lang'],301);
}

# set locale info
header('Content-Type: text/html; charset='.strtolower(DB_ENCODEING));
header('Content-language: '.$_GET['lang']);
# END set locale info

# DEFAULT rules
if( !empty($mod_rewrite[1]) ) $_GET['section_0']=$mod_rewrite[1];
if( !empty($mod_rewrite[2]) ) $_GET['section_1']=$mod_rewrite[2];
if( !empty($mod_rewrite[3]) ) $_GET['section_2']=$mod_rewrite[3];
if( !empty($mod_rewrite[4]) ) $_GET['section_3']=$mod_rewrite[4];
if( !empty($mod_rewrite[5]) ) $_GET['section_4']=$mod_rewrite[5];
if( !empty($mod_rewrite[6]) ) $_GET['section_5']=$mod_rewrite[6];

# if is mobile device
$detect = new Mobile_Detect;
if( $detect->isMobile() ) $_GET['mobile_device']=1;
//print $detect->getUserAgent();
//print "-".round($detect->version('BlackBerry'));
if( $detect->is('BlackBerry') && round($detect->version('BlackBerry'))<=6 )
{
    $_GET['mobile_device_old']=1;
}
//print count($mod_rewrite);
//print_r($mod_rewrite);
//print $dirname;
//print_r($_GET);

#*********** REDIRECT OLD SITE LINKS ************#

# /art/search/?artworkID1=paintings&title=&number=&number-from.............
if( isset($_GET['artworkID1']) )
{
    $pattern = '/(artworkID1)/i';
    $replacement = "artworkid";
    $url_redirect=preg_replace($pattern, $replacement, $_SERVER["REQUEST_URI"]);

    //print $url_redirect;
    //exit;
    UTILS::redirect($url_redirect,301);
}
elseif( $mod_rewrite[1]=="videos" && ( $mod_rewrite[0]=="en" || $mod_rewrite[0]=="de" || $mod_rewrite[0]=="fr" || $mod_rewrite[0]=="it" || $mod_rewrite[0]=="zh" ) )
{
    unset($mod_rewrite[0]);
    $mod_rewrite=array_values($mod_rewrite);
}

$lang=$browser_language;

$url_redirect="/".$lang;

# /home
if( $mod_rewrite[0]=="home" || $mod_rewrite[1]=="home" )
{
    $url_redirect="/".$lang;

    //print $url_redirect;
    //exit;
    UTILS::redirect($url_redirect,301);
}
elseif( $mod_rewrite[0]=="art" || $mod_rewrite[1]=="art" )
{
    $url_redirect.="/art";

    # /art/paintings.php
    if( $mod_rewrite[2]=="results_atlas.php" )
    {
        $url_redirect.="/atlas";

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    # /art/paintings.php
    elseif( $mod_rewrite[1]=="paintings.php" )
    {
        //$url_redirect="/".$lang;

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    # END /art/paintings.php
    /*
    elseif( !empty($_GET['paintid']) )
    {
        $options_painting_url=array();
        $options_painting_url['paintid']=$_GET['paintid'];
        $options_painting_url['lang']=$lang;
        $painting_url=UTILS::get_painting_url($db,$options_painting_url);
        $url_redirect=$painting_url['url'];

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    */
    # /art/search/detail.php?6851
    elseif( $mod_rewrite[1]=="detail.php" || $mod_rewrite[2]=="detail.php" || $mod_rewrite[3]=="detail.php" || $mod_rewrite[4]=="detail.php" || $mod_rewrite[2]=="atlas.php" )
    {
        if( isset($_GET['paintid']) ) $paintid=$_GET['paintid'];
        elseif( isset($_GET['paintID']) ) $paintid=$_GET['paintID'];
        else
        {
            $tmp=explode(".php&", $_SERVER["QUERY_STRING"]);
            $paintid=$tmp[1];
        }
        if( !empty($paintid) )
        {
            $options_painting_url=array();
            $options_painting_url['paintid']=$paintid;
            $options_painting_url['lang']=$lang;
            $painting_url=UTILS::get_painting_url($db,$options_painting_url);
            $url_redirect=$painting_url['url'];

            if( $url_redirect['found'] )
            {
                //print $url_redirect;
                //exit;
                UTILS::redirect($url_redirect,301);
            }
            else
            {
                UTILS::not_found_404();
            }
        }
        else
        {
           UTILS::not_found_404();
        }
    }
    //exit;
    # category redirect
    elseif( $mod_rewrite[1]=="paintings" || $mod_rewrite[2]=="paintings" )
    {
        $url_redirect_other=$url_redirect;
        $url_redirect.="/paintings";
        if( $mod_rewrite[3]=="category.php" || $mod_rewrite[4]=="category.php" )
        {
            if( $mod_rewrite[2]=="photo_paintings" ) $section=$mod_rewrite[2];
            if( $mod_rewrite[3]=="photo_paintings" ) $section=$mod_rewrite[3];
            if( $section=="photo_paintings" ) $section="photo-paintings";

            if( $mod_rewrite[2]=="abstracts" ) $section=$mod_rewrite[2];
            if( $mod_rewrite[3]=="abstracts" ) $section=$mod_rewrite[3];

            if( $mod_rewrite[2]=="other" ) $section=$mod_rewrite[2];
            if( $mod_rewrite[3]=="other" ) $section=$mod_rewrite[3];

            //print $url_redirect."<br />";
            $query_artwork="SELECT artworkID,titleurl FROM ".TABLE_ARTWORKS." WHERE titleurl='".$section."' LIMIT 1 ";
            //print $query_artwork;
            $results_artwork=$db->query($query_artwork);
            $count_artwork=$db->numrows($results_artwork);
            if( $count_artwork || $section=="other" )
            {
                if( $section=="other" )
                {
                    $url_redirect=$url_redirect_other."/other";
                    $artworkid=13;
                    //UTILS::redirect($url_redirect_other,301);
                }
                else
                {
                    $row_artwork=$db->mysql_array($results_artwork,0);
                    $url_redirect.="/".$row_artwork['titleurl'];
                    $artworkid=$row_artwork['artworkID'];
                }
                $categoryid=$_GET['catID'];

                //print $artworkid."-".$categoryid;
                //exit;
                if( !empty($categoryid) )
                {
                    $query_where_category=" WHERE catID='".$categoryid."' AND artworkID='".$artworkid."' AND enable=1 ";
                    $query_limit_category=" LIMIT 1 ";
                    $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                    $results_category=$db->query($query_category['query']);
                    $count_category=$db->numrows($results_category);
                    if( $count_category>0 )
                    {
                        $row_category=$db->mysql_array($results_category);

                        if( !empty($row_category['sub_catID']) )
                        {
                            $query_where_category=" WHERE catID='".$row_category['sub_catID']."' AND artworkID='".$artworkid."' AND enable=1 ";
                            $query_limit_category=" LIMIT 1 ";
                            $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
                            $results_sub_category=$db->query($query_category['query']);
                            $count_sub_category=$db->numrows($results_sub_category);
                            if( $count_sub_category>0 )
                            {
                                $row_sub_category=$db->mysql_array($results_sub_category);
                            }
                        }
                        if( !empty($row_sub_category['titleurl']) ) $url_redirect.="/".$row_sub_category['titleurl'];
                        if( !empty($row_category['titleurl']) ) $url_redirect.="/".$row_category['titleurl'];

                        //print $url_redirect;
                        //exit;
                        UTILS::redirect($url_redirect,301);
                    }
                }
                else
                {
                    UTILS::not_found_404();
                }
            }

        }
    }
    elseif( $mod_rewrite[2]=="paintings" )
    {
        if( $mod_rewrite[3]=="photo-paintings" && empty($mod_rewrite[4]) )
        {
            $url_redirect.="/paintings/#photo-paintings";
            //print $url_redirect;
            //exit;
            UTILS::redirect($url_redirect,301);
        }
        elseif( $mod_rewrite[3]=="abstracts" && empty($mod_rewrite[4]) )
        {
            $url_redirect.="/paintings/#abstracts";
            //print $url_redirect;
            //exit;
            UTILS::redirect($url_redirect,301);
        }
        elseif( $mod_rewrite[3]=="other" )
        {

            if( empty($mod_rewrite[4]) )
            {
                $url_redirect.="/other";
                //print $url_redirect;
                //exit;
                //UTILS::redirect($url_redirect,301);
            }
            elseif( empty($mod_rewrite[5]) )
            {
                $url_redirect.="/other/".$mod_rewrite[4];
                //print $url_redirect;
                //exit;
            }
            elseif( empty($mod_rewrite[6]) )
            {
                $url_redirect.="/other/".$mod_rewrite[4]."/".$mod_rewrite[5];
                $tmp=explode("/",$_SERVER['argv'][0]);
                $argv=$tmp[count($tmp)-1];
                //print $url_redirect;
                //exit;
            }
            UTILS::redirect($url_redirect,301);
        }
    }
}
elseif( $mod_rewrite[0]=="exhibitions" || $mod_rewrite[1]=="exhibitions" )
{
    if( $mod_rewrite[1]=="detail.php" || $mod_rewrite[2]=="detail.php" )
    {
        if( !empty($_GET['exID']) )
        {
            $exhibitionid=$_GET['exID'];
            $options_exh_url=array();
            $options_exh_url['exhibitionid']=$exhibitionid;
            $url=UTILS::get_exhibition_url($db,$options_exh_url);
            $url_redirect=$url;
        }
        if( !empty($_GET['paintID']) )
        {
            $paintid=$_GET['paintID'];
            $options_painting_url=array();
            $options_painting_url['paintid']=$paintid;
            $options_painting_url['lang']=$lang;
            $options_painting_url['painting_no_section']=1;
            $painting_url=UTILS::get_painting_url($db,$options_painting_url);
            $url_redirect.=$painting_url['url'];
        }

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    elseif( $mod_rewrite[1]=="exhibition.php" || $mod_rewrite[2]=="exhibition.php" )
    {
        if( !empty($_GET['exID']) )
        {
            $exhibitionid=$_GET['exID'];
            $options_exh_url=array();
            $options_exh_url['exhibitionid']=$exhibitionid;
            $url=UTILS::get_exhibition_url($db,$options_exh_url);
            $url_redirect=$url;
        }

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    //exit;
}
elseif( $mod_rewrite[0]=="literature" || $mod_rewrite[1]=="literature" )
{
    if( $mod_rewrite[1]=="detail.php" )
    {
        $tmp=explode(".php&", $_SERVER["QUERY_STRING"]);
        $bookid=$tmp[1];
        //print_r($tmp);
        $options_book_url=array();
        $options_book_url['bookid']=$bookid;
        $options_book_url['lang']=$lang;
        $url_literature=UTILS::get_literature_url($db,$options_book_url);
        $url_redirect=$url_literature['url'];

        //print $url_redirect;
        UTILS::redirect($url_redirect,301);
    }
    /*
    elseif( preg_match("/year/i", $_SERVER['REQUEST_URI']) &&  preg_match("/till/i", $_SERVER['REQUEST_URI']) )
    {
        $url_redirect="/en/literature";

        UTILS::redirect($url_redirect,301);
    }
    */
    /*
    elseif( isset($_GET['order_by']) || isset($_GET['order_how']) )
    {
        $url_redirect="/en/literature/search/?".$_SERVER["argv"][0];

        print $url_redirect;
        exit;
        //UTILS::redirect($url_redirect,301);
    }
    */
    //exit;
}
elseif( $mod_rewrite[0]=="videos" || $mod_rewrite[0]=="video" )
{
    $url_redirect.="/videos";

    if( $mod_rewrite[1]=="detail.php" )
    {
        $tmp=explode("&", $_SERVER["QUERY_STRING"]);
        foreach ($tmp as $value) {
            $tmp2=explode("=", $value);
            if($tmp2[0]=="vID")
            {
                $videoid=$tmp2[1];
            }
        }

        //print_r($tmp);
        $options_video_url=array();
        $options_video_url['videoid']=$videoid;
        $options_video_url['lang']=$lang;
        $url_video=UTILS::get_video_url($db,$options_video_url);
        $url_redirect=$url_video['url'];

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    elseif( $mod_rewrite[1]=="exhibitions-1" || $mod_rewrite[1]=="works-2" || $mod_rewrite[1]=="talks-3" )
    {
        if( !empty($mod_rewrite[1]) )
        {
            if( $mod_rewrite[1]=="exhibitions-1" ) $mod_rewrite[1]="exhibitions";
            if( $mod_rewrite[1]=="works-2" ) $mod_rewrite[1]="works";
            if( $mod_rewrite[1]=="talks-3" ) $mod_rewrite[1]="talks";

            $url_redirect.="/".$mod_rewrite[1];
        }

        if( !empty($mod_rewrite[2]) )
        {
            $url_redirect.="/".$mod_rewrite[2];
        }

        UTILS::redirect($url_redirect,301);
    }
    elseif( !empty($_GET['vID']) )
    {
        $videoid=$_GET['vID'];
        //print_r($tmp);
        $options_video_url=array();
        $options_video_url['videoid']=$videoid;
        $options_video_url['lang']=$lang;
        $url_video=UTILS::get_video_url($db,$options_video_url);
        $url_redirect=$url_video['url'];

        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    elseif( $mod_rewrite[0]=="video" )
    {
        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }
    elseif( $mod_rewrite[0]=="biography" )
    {
        //print $url_redirect;
        //exit;
        UTILS::redirect($url_redirect,301);
    }

    //print $url_redirect;
    //UTILS::redirect($url_redirect,301);
    //exit;
}
elseif( $mod_rewrite[0]=="links" || $mod_rewrite[1]=="links" )
{
    $url_redirect="/".$_GET['lang'];
    $url_redirect.="/literature/articles";

    //print $url_redirect;
    //exit;
    UTILS::redirect($url_redirect,301);
}
//exit;
//UTILS::redirect("/".$_GET['lang'],301);


?>
