<?php

function microsite_firenze($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";
                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";
            $html_print.="});\n";

            # search tooltips
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";

        $html_print.="});\n";
    $html_print.="</script>\n";

	$html_print.="<h1>Gerhard Richter - ".$options['title']."</h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title='' onclick=\"$('.accordion').slideToggle();\">".LANG_MICROSITE_ABOUT." ".$options['title']."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;' >\n";
	            $html_print.="<p style='padding-top:20px;'>";
	        	    $html_print.=$options['text'];
	        	    	$options_admin_edit=array();
						$options_admin_edit['html_return']=1;
	                //$html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$options['paintid'],$options_admin_edit);
	                $html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$options['micrositeid'],$options_admin_edit);
	            $html_print.="</p>";
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

		###information about edition
		$query=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ");
		$edition=$db->mysql_array($query);
		$count1=$db->numrows($query);
		#end

		$search_array=$options['search_array'];
		$searchstring=$options['searchstring'];

	    $pages=@ceil($count1/$_GET['sp']);

	    if(isset($searchstring)&&!empty($searchstring))
	    {
	        $query_text="SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ";
	        for( $i=0;$i<=sizeof($search_array);$i++ )
	        {
		        if(!empty($search_array[$i]))
		        {
		            if($i==0) $query_text.=" AND ";
	                else $query_text.=" OR ";
		            //$query_text.=" ( ednumber LIKE '".$search_array[$i]."%' OR ednumberroman LIKE '".$search_array[$i]."%' OR ednumberap LIKE '".$search_array[$i]."%' OR citylifepage LIKE '".$search_array[$i]."%' )";
		        	$query_text.=" titleEN LIKE 'Firenze (".$search_array[$i]."/99)%' ";
		        }
		    }
	    $query_text.=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
	    }
	    else
	    {
	        $query_text="SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
	    }

	    $query=$db->query($query_text);

	    $count=$db->numrows($query);

	    if( $_SESSION['debug_page'] )
	    {
	    	print $query_text."<br />";
	    	print $count;
	    }



		$html_print.="
			<div id='searchEditions'>
				<div id='searchPosition'>
				<form class='editionSearch' action='' method='get'>
					<label>
						<a class='edition-tip-".$_GET['lang']." help' title='' data-title='".LANG_MICROSITE_SEARCH_EDITION."' >".LANG_MICROSITE_SEARCH_EDITION."
						</a>
						<div class='search-help-tooltips'>
							<div class='tooltips-title'>
								".LANG_MICROSITE_SEARCH_HELP_TITLE."
							</div>

							<div class='tooltips-text'>
								".LANG_MICROSITE_SEARCH_HELP_TEXT."
							</div>
						</div>
					</label>
					<input class='edition' name='s' autocomplete='off' type='text' value='".$searchstring."' />
					<input name='sp' type='hidden' value='".$_GET['sp']."' />
					<input name='p' type='hidden' value='1' />
					<input name='button' type='submit' value='".LANG_MICROSITE_SEARCH."' />
				</form>
				</div>
				<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW."
		        <select name='sp' id='sp' onchange='this.form.submit();'>
			";

				for($i=1;$i<=$count1/20;$i++)
				{
					if($i!=$count1/20){
					if( $_GET['sp']==(20*$i) )$selected="selected='selected'"; else $selected="";
					$html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
					}
					//else {
					if( $_GET['sp']==118 )$selected2='selected';else $selected2='';
					//$html_print.="			<option $selected2 value='$count1'>".LANG_MICROSITE_SEE_ALL."</option>";
					//}
				}
			 $html_print.="            <option $selected2 value='$count1'>".LANG_MICROSITE_SEE_ALL."</option>";

		$html_print.="			</select><span class='pages'>";

		#page numbering
		if( isset($searchstring) && !empty($searchstring) )
		{
			$pages=@ceil(sizeof($search_array)/$_GET['sp']);
		}
		if( $pages>1 )
		{
			if( $_GET['p']!=1 )
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']-1)."&l=".($_GET['l']-$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/firenze-page-previous.gif' alt='' width='14' height='14' /></a>";
			}
			for($i=1;$i<=$pages;$i++)
			{
			 	if($i==1) $limit2=0;
			 	else $limit2=$limit2+$_GET['sp'];
			    if( $_GET['p']!=$i )
			    {
			        $html_print.=" <a href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			    else
			    {
			        $html_print.=" <a class='selected' href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			}
			if($_GET['p']!=$pages)
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']+1)."&l=".($_GET['l']+$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/firenze-page-next.gif' alt='' width='14' height='14' /></a>";
			}
		}
		#END

		$html_print.="</span>
				<input name='s' type='hidden' value='".$searchstring."' />
				<input name='p' type='hidden' value='1' />
				</form>
			</div>
			<ul class='paintings'>
		";


		if( isset($searchstring) )
		{
		    if( is_array($search_array) ) foreach( $search_array as $value ){$tmp1[]=str_replace("/99", "", $value);}
		    if( is_array($tmp1) ) foreach( $tmp1 as $value ){$tmp2[]=str_replace("/XII", "", $value);}
		    if( is_array($tmp2) ) foreach( $tmp2 as $value ){$tmp3[]=str_replace(" a.p.", "", $value);}
		    if( is_array($tmp3) ) foreach( $tmp3 as $value ){$tmp4[]=str_replace("/7", "", $value);}
		    $search_array=$tmp4;
		}

		$i=0;
		$ii=0;

		while( $row2=@$db->mysql_array($query) )
		{
		    //if( empty($_GET['s']) )
		    //{
		        if( $i<($_GET['l']+$_GET['sp']) && $i>=$_GET['l'] )
		        {
			    $html_print.="
					<li>
		            <div class='lightboxDesc fancybox-desc-".$row2['paintID']."'> ";


		            $html_print.="    <span class='year'>".$row2['year']."</span>
		                <span class='dimensions'>".$row2['size']."</span>";


		           $html_print.=" </div>";

						//if( $row2['paintID']=="15409" )
						//{
					        $query_images="SELECT
					            COALESCE(
					                r1.itemid1,
					                r2.itemid2
					            ) as itemid,
					            COALESCE(
					                r1.sort,
					                r2.sort
					            ) as sort_rel,
					            COALESCE(
					                r1.relationid,
					                r2.relationid
					            ) as relationid_rel
					            FROM
					                ".TABLE_PAINTING." p
					            LEFT JOIN
					                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
					            LEFT JOIN
					                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
					            WHERE p.paintID=".$row2['paintID']."
					            ORDER BY sort_rel ASC, relationid_rel DESC
					            LIMIT 1
					        ";
					        $results_related_image=$db->query($query_images);
					        $row_related_image=$db->mysql_array($results_related_image);
					        $imageid=$row_related_image['itemid'];

							$image="/images_new/xlarge/".$imageid.".jpg";
                			$href=DATA_PATH_DATADIR.$image;
                			$src=DATA_PATH_DATADIR."/images_new/cache/".$imageid.".png";



                		$title=UTILS::row_text_value2($db,$row2,"title");
						$html_print.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='microsite-photos' title='".$title."'>";
						$html_print.="<img src='".$src."'  alt='' width='134' data-paintid='".$row2['paintID']."' />";
	        	    	$options_admin_edit=array();
						$options_admin_edit['html_return']=1;
		                $html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$row2['paintID'],$options_admin_edit);
		                $html_print.="</a>
					</li>
			";
		        }
		    $i++;
		}


		if( $count==0 ) $html_print.=LANG_SEARCH_NORESULTS.".";
			$html_print.="</ul>";
		$html_print.="</div>";

	return $html_print;
}

function microsite_war_cut($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";
                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";
            $html_print.="});\n";

            # search tooltips
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";

        $html_print.="});\n";
    $html_print.="</script>\n";

	$html_print.="<h1>Gerhard Richter - ".$options['title']."</h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title='' onclick=\"$('.accordion').slideToggle();\">".LANG_MICROSITE_ABOUT." ".$options['title']."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;' >\n";
	            $html_print.="<p style='padding-top:20px;'>";
	        	    $html_print.=$options['text'];
	        	    	$options_admin_edit=array();
						$options_admin_edit['html_return']=1;
	                //$html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$options['paintid'],$options_admin_edit);
					$html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$options['micrositeid'],$options_admin_edit);
	            $html_print.="</p>";
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

		###information about edition
		$query=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ");
		$edition=$db->mysql_array($query);
		$count1=$db->numrows($query);
		#end

		$pages=@ceil($count1/$_GET['sp']);

		$search_array=$options['search_array'];
		$searchstring=$options['searchstring'];

		$query_text="SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ";

		if(isset($searchstring)&&!empty($searchstring))
		{
			for($i=0;$i<=sizeof($search_array);$i++)
			{
				if(!empty($search_array[$i]))
				{
				    if($i==0) $query_text.=" AND ";
				    else $query_text.=" OR ";

				    $query_text.=" ( titleEN LIKE 'War Cut II (".$search_array[$i]."/50)%' OR titleEN LIKE 'War Cut II (".$search_array[$i]."/XX)%' ) ";
				}
			}
		}

		$query_text.=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
		//echo $query_text;

		$query=$db->query($query_text);

		$count=$db->numrows($query);

	    if( $_SESSION['debug_page'] )
	    {
	    	print $query_text."<br />";
	    	print $count;
	    }

		$html_print.="
			<div id='searchEditions'>
				<div id='searchPosition'>
				<form class='editionSearch' action='' method='get'>
					<label>
						<a class='edition-tip-".$_GET['lang']." help' title='' data-title='".LANG_MICROSITE_SEARCH_EDITION."' >".LANG_MICROSITE_SEARCH_EDITION."
						</a>
						<div class='search-help-tooltips'>
							<div class='tooltips-title'>
								".LANG_MICROSITE_SEARCH_HELP_TITLE."
							</div>

							<div class='tooltips-text'>
								".LANG_MICROSITE_SEARCH_HELP_TEXT."
							</div>
						</div>
					</label>
					<input class='edition' name='s' autocomplete='off' type='text' value='".$searchstring."' />
					<input name='sp' type='hidden' value='".$_GET['sp']."' />
					<input name='p' type='hidden' value='1' />
					<input name='button' type='submit' value='".LANG_MICROSITE_SEARCH."' />
				</form>
				</div>
				<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW."
		        <select name='sp' id='sp' onchange='this.form.submit();'>
			";
				for($i=1;$i<=$count1/20;$i++)
				{
					if($i!=$count1/20){
					if( $_GET['sp']==(20*$i) )$selected="selected='selected'"; else $selected="";
					$html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
					}
					//else {
					if( $_GET['sp']==70 )$selected2='selected';else $selected2='';
					//$html_print.="			<option $selected2 value='$count1'>".LANG_MICROSITE_SEE_ALL."</option>";
					//}
				}
			 $html_print.="            <option $selected2 value='$count1'>".LANG_MICROSITE_SEE_ALL."</option>";

		$html_print.="			</select><span class='pages'>";


		#page numbering
		if( isset($searchstring) && !empty($searchstring) )
		{
			$pages=@ceil(sizeof($search_array)/$_GET['sp']);
		}
		if( $pages>1 )
		{
			if( $_GET['p']!=1 )
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']-1)."&l=".($_GET['l']-$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/war-cut-page-previous.gif' alt='' width='14' height='14' /></a>";
			}
			for($i=1;$i<=$pages;$i++)
			{
			 	if($i==1) $limit2=0;
			 	else $limit2=$limit2+$_GET['sp'];
			    if( $_GET['p']!=$i )
			    {
			        $html_print.=" <a href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			    else
			    {
			        $html_print.=" <a class='selected' href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			}
			if($_GET['p']!=$pages)
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']+1)."&l=".($_GET['l']+$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/war-cut-page-next.gif' alt='' width='14' height='14' /></a>";
			}
		}
		#END




		$html_print.="</span>
				<input name='s' type='hidden' value='".$searchstring."' />
				<input name='p' type='hidden' value='1' />
				</form>
			</div>
			<ul class='paintings'>
		";

		$i=0;
		$ii=0;
		while($row2=@$db->mysql_array($query))
		{
		    //if(empty($searchstring))
		    //{
		        if($i<($_GET['l']+$_GET['sp'])&&$i>=$_GET['l'])
		        {
		        $html_print.="
		          <li>
		                <div class='lightboxDesc fancybox-desc-".$row2['paintID']."'>
		                    <span class='year'>".$row2['year']."</span>
		                    <span class='dimensions'>".$row2['size']."</span>";

						$html_print.="<span class='cr'>";
							$html_print.=LANG_MICROSITE_EDITIONS_CR;
							$html_print.=": ";
							$html_print.=$row2['number'];
							/*
							if( !empty($row2['ednumber']) )
							{
								$html_print.=", ";
								$html_print.=$row2['ednumber'];
							}
							*/
						$html_print.="</span>";


		            $html_print.="</div>";

			        $query_images="SELECT
			            COALESCE(
			                r1.itemid1,
			                r2.itemid2
			            ) as itemid,
			            COALESCE(
			                r1.sort,
			                r2.sort
			            ) as sort_rel,
			            COALESCE(
			                r1.relationid,
			                r2.relationid
			            ) as relationid_rel
			            FROM
			                ".TABLE_PAINTING." p
			            LEFT JOIN
			                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
			            LEFT JOIN
			                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
			            WHERE p.paintID=".$row2['paintID']."
			            ORDER BY sort_rel ASC, relationid_rel DESC
			            LIMIT 1
			        ";
			        $results_related_image=$db->query($query_images);
			        $row_related_image=$db->mysql_array($results_related_image);
			        $imageid=$row_related_image['itemid'];

					$image="/images_new/xlarge/".$imageid.".jpg";
        			$href=DATA_PATH_DATADIR.$image;
        			$src=DATA_PATH_DATADIR."/images_new/cache/".$imageid.".png";

		            $title=UTILS::row_text_value2($db,$row2,"title");

		            $html_print.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='microsite-photos' title='".$title."'>";

		            	$html_print.="<img src='".$src."' width='129' alt='' data-paintid='".$row2['paintID']."' />";

		                $options_admin_edit=array();
		                $options_admin_edit['html_return']=1;
		        		$html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$row2['paintID'],$options_admin_edit);

		        	$html_print.="</a>";
		          	$html_print.="</li>";
		        }
		    $i++;
		}
		if( $count==0 ) $html_print.=LANG_SEARCH_NORESULTS.".";
			$html_print.="</ul>";
		$html_print.="</div>";






	return $html_print;
}

function microsite_snow_white($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";
                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";
            $html_print.="});\n";

            # search tooltips
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";

        $html_print.="});\n";
    $html_print.="</script>\n";

	$html_print.="<h1>Gerhard Richter - ".$options['title']."</h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title='' onclick=\"$('.accordion').slideToggle();\">".LANG_MICROSITE_ABOUT." ".$options['title']."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;' >\n";
	            $html_print.="<p style='padding-top:20px;'>";
	        	    $html_print.=$options['text'];
	        	    	$options_admin_edit=array();
						$options_admin_edit['html_return']=1;
	                //$html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$options['paintid'],$options_admin_edit);
					$html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$options['micrositeid'],$options_admin_edit);
	            $html_print.="</p>";
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

		###information about edition
		$query=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' ");
		$edition=$db->mysql_array($query);
		$count1=$db->numrows($query);
		#end

		$pages=@ceil($count1/$_GET['sp']);

		$search_array=$options['search_array'];
		$searchstring=$options['searchstring'];

		$query_text="SELECT * FROM ".TABLE_PAINTING." WHERE editionID='".$options['paintid']."' AND enable=1 ";

		if(isset($searchstring)&&!empty($searchstring))
		{
			for($i=0;$i<=sizeof($search_array);$i++)
			{
				if(!empty($search_array[$i]))
				{
				    if($i==0) $query_text.=" AND ";
				    else $query_text.=" OR ";

				    $query_text.=" ( titleEN LIKE 'Snow White (".$search_array[$i]."/100)%' ) ";
				}
			}

		}

		$query_text.=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";

		//echo $query_text;

		$query=$db->query($query_text);

		$count=$db->numrows($query);

	    if( $_SESSION['debug_page'] )
	    {
	    	print $query_text."<br />";
	    	print $count;
	    }

		$html_print.="
			<div id='searchEditions'>
				<div id='searchPosition'>
				<form class='editionSearch' action='' method='get'>
					<label>
						<a class='edition-tip-".$_GET['lang']." help' title='' data-title='".LANG_MICROSITE_SEARCH_EDITION."' >".LANG_MICROSITE_SEARCH_EDITION."
						</a>
						<div class='search-help-tooltips'>
							<div class='tooltips-title'>
								".LANG_MICROSITE_SEARCH_HELP_TITLE."
							</div>

							<div class='tooltips-text'>
								".LANG_MICROSITE_SEARCH_HELP_TEXT."
							</div>
						</div>
					</label>
					<input class='edition' name='s' autocomplete='off' type='text' value='".$searchstring."' />/$count1
					<input name='sp' type='hidden' value='".$_GET['sp']."' />
					<input name='p' type='hidden' value='1' />
					<input name='button' type='submit' value='".LANG_MICROSITE_SEARCH."' />
				</form>
				</div>
				<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW."
		        <select name='sp' id='sp' onchange='this.form.submit();'>
			";
				for($i=1;$i<=$count1/20;$i++)
				{
					if($i!=$count1/20){
					if( $_GET['sp']==(20*$i) )$selected="selected='selected'"; else $selected="";
					$html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
					}
					else {
					if( $_GET['sp']==100 )$selected2='selected';else $selected2='';
					$html_print.="			<option $selected2 value='$count1'>".LANG_MICROSITE_SEE_ALL."</option>";
					}
				}

		$html_print.="			</select><span class='pages'>";


		#page numbering
		if( isset($searchstring) && !empty($searchstring) )
		{
			$pages=@ceil(sizeof($search_array)/$_GET['sp']);
		}
		if( $pages>1 )
		{
			if( $_GET['p']!=1 )
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']-1)."&l=".($_GET['l']-$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/snow-white-page-previous.gif' alt='' width='14' height='14' /></a>";
			}
			for($i=1;$i<=$pages;$i++)
			{
			 	if($i==1) $limit2=0;
			 	else $limit2=$limit2+$_GET['sp'];
			    if( $_GET['p']!=$i )
			    {
			        $html_print.=" <a href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			    else
			    {
			        $html_print.=" <a class='selected' href='?sp=".$_GET['sp']."&p=".$i."&l=".$limit2;
			        if(!empty($searchstring))$html_print.="&s=".$searchstring;
			        $html_print.="' title=''>".$i."</a>";
			    }
			}
			if($_GET['p']!=$pages)
			{
				$html_print.=" <a href='?sp=".$_GET['sp']."&p=".($_GET['p']+1)."&l=".($_GET['l']+$_GET['sp']);
			 	if(!empty($searchstring))$html_print.="&s=".$searchstring;
				$html_print.="' title=''><img src='/g/microsite/snow-white-page-next.gif' alt='' width='14' height='14' /></a>";
			}
		}
		#END




		$html_print.="</span>
				<input name='s' type='hidden' value='".$searchstring."' />
				<input name='p' type='hidden' value='1' />
				</form>
			</div>
			<ul class='paintings'>
		";

		$i=0;
		$ii=0;
		while( $row2=@$db->mysql_array($query) )
		{
			//if( empty($searchstring) )
			//{
				if( $i<($_GET['l']+$_GET['sp']) && $i>=$_GET['l'] )
				{
					$html_print.="
							<li>
					        <div class='lightboxDesc fancybox-desc-".$row2['paintID']."'>
					            <span class='year'>".$row2['year']."</span>
					            <span class='dimensions'>".$row2['size']."</span>";
					$html_print.="<span class='cr'>";
						$html_print.=LANG_MICROSITE_EDITIONS_CR;
						$html_print.=": ";
						$html_print.=$row2['number'];
						/*
						if( !empty($row2['ednumber']) )
						{
							$html_print.=", ";
							$html_print.=$row2['ednumber'];
						}
						*/
					$html_print.="</span>";
					$html_print.="</div>\n";

					/*
					# image
					$results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row2['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row2['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
					$row_related_image=$db->mysql_array($results_related_image);
					$imageid=UTILS::get_relation_id($db,"17",$row_related_image);
					$link="/datadir/images_new/xlarge_snowwhite/".$imageid.".jpg";
					$src="/datadir/images_new/cache_microsites/".$imageid.".jpg";
					*/


			        $query_images="SELECT
			            COALESCE(
			                r1.itemid1,
			                r2.itemid2
			            ) as itemid,
			            COALESCE(
			                r1.sort,
			                r2.sort
			            ) as sort_rel,
			            COALESCE(
			                r1.relationid,
			                r2.relationid
			            ) as relationid_rel
			            FROM
			                ".TABLE_PAINTING." p
			            LEFT JOIN
			                ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
			            LEFT JOIN
			                ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
			            WHERE p.paintID=".$row2['paintID']."
			            ORDER BY sort_rel ASC, relationid_rel DESC
			            LIMIT 1
			        ";
			        $results_related_image=$db->query($query_images);
			        $row_related_image=$db->mysql_array($results_related_image);
			        $imageid=$row_related_image['itemid'];

					$image="/images_new/xlarge/".$imageid.".jpg";
        			$href=DATA_PATH_DATADIR.$image;
        			$src=DATA_PATH_DATADIR."/images_new/cache/".$imageid.".png";

        			$title=UTILS::row_text_value2($db,$row2,"title");

					$html_print.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='microsite-photos' title='".$title."'>\n";
					    $html_print.="<img src='".$src."' alt='' data-paintid='".$row2['paintID']."'  />\n";
					              $options_admin_edit=array();
					        $options_admin_edit['html_return']=1;
					    $html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$row2['paintID'],$options_admin_edit);
					$html_print.="</a>\n";
					# end image
					$html_print.="</li>
					";
				}
			$i++;
		}
		if( $count==0 ) $html_print.=LANG_SEARCH_NORESULTS.".";
			$html_print.="</ul>";
		$html_print.="</div>";


	return $html_print;
}

function microsite_4900($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
        	/*
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";

                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";

            $html_print.="});\n";
            */

            # search tooltips
            /*
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";
	        */

        $html_print.="});\n";
    $html_print.="</script>\n";

	if( empty($options['titleurl']) )
	{
	    $query_version="SELECT * FROM ".TABLE_MICROSITES_VERSIONS." ORDER BY sort ";
	}
	else
	{
	    $query_version="SELECT * FROM ".TABLE_MICROSITES_VERSIONS." WHERE number LIKE '".$options['titleurl']."' ";
	}
	$results_version=$db->query($query_version);
	$row_version=$db->mysql_array($results_version);
	$versionid=$row_version['versionid'];
	$version=$row_version['number'];
	$micrositeid=1;

	$html_print.="<h1><a href='/".$_GET['lang']."/art/microsites/4900-colours/' title='Gerhard Richter | ".$options['title']."'><span class='span_h1_fist_part'>Gerhard Richter</span><span class='span_h1_second_part'>".$options['title']."</span></a></h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title=''  onclick=\"$('.accordion').slideToggle();\">".LANG_MICROSITE_ABOUT." ".$options['title']."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;' >\n";
	            $html_print.="<p style='padding-top:20px;'>";
	        	    $html_print.=$options['text'];
	        	    	$options_admin_edit=array();
						$options_admin_edit['html_return']=1;
	                $html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$micrositeid,$options_admin_edit);
	            $html_print.="</p>";
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

	### images

	if(isset($searchstring)&&!empty($searchstring))
	{
	    $query_text="SELECT * FROM ".TABLE_MICROSITES_IMAGES." WHERE micrositeid='".$micrositeid."' AND versionid='".$versionid."' ORDER BY sort";
		for($i=0;$i<=sizeof($search_array);$i++)
		{
	      	if(!empty($search_array[$i]))
	      	{
	        	if($i==0) $query_text.=" AND paintID='".$search_array[$i]."'";
	        	else $query_text.=" OR paintID='".$search_array[$i]."'";
	      	}
	    }
		$query_text.=" ORDER BY sort ASC ";
	}
	else
	{
	    $query_text="SELECT * FROM ".TABLE_MICROSITES_IMAGES." WHERE micrositeid='".$micrositeid."' AND versionid='".$versionid."' ORDER BY sort";
	}

	$query=$db->query($query_text);


	$results_images=$db->query("SELECT * FROM ".TABLE_MICROSITES_IMAGES." WHERE micrositeid='".$micrositeid."' AND versionid='".$versionid."' ORDER BY sort");
	$count_images=$db->numrows($results_images);
	#end

	$pages=@ceil($count_images/$_GET['sp']);

	$html_print.="<div id='searchEditions'>\n";
	    $html_print.="<a href='/".$_GET['lang']."/art/microsites/4900-colours' title='Versions' class='a_versions'>Versions</a>\n";

	    $results_versions=$db->query("SELECT * FROM ".TABLE_MICROSITES_VERSIONS." WHERE micrositeid='".$micrositeid."' ORDER BY sort ");
	    $count_versions=$db->numrows($results_versions);

	    if( $count_versions>0 )
	    {
	        $html_print.="<ul class='ul_versions'>\n";
	            while( $row_versions=$db->mysql_array($results_versions) )
	            {
	                $title=UTILS::row_text_value($db,$row_versions,"title");
	                if( $row_versions['versionid']==$versionid ) $class="selected";
	                else $class="";
	                $html_print.="<li><a href='/".$_GET['lang']."/art/microsites/4900-colours/".$row_versions['number']."' title='".$title."' class='".$class."'>".$row_versions['number']."</a></li>\n";
	            }
	        $html_print.="</ul>\n";
	    }


			$html_print.="<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW;

			    $html_print.="&nbsp;&nbsp;<select name='sp' id='sp' onchange='this.form.submit();'>\n";
	                for($i=1;$i<=$count_images/20;$i++)
	                {
	                    if($i!=$count_images/20)
	                    {
			                if( $_GET['sp']==(20*$i) )$selected="selected='selected'";
	                        else $selected="";
			                $html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
			            }
			            if( $_GET['sp']==$count_images )$selected2='selected';
	                    else $selected2='';
	                }
	                $html_print.="<option $selected2 value='$count_images'>".LANG_MICROSITE_SEE_ALL."</option>\n";
	            $html_print.="</select>\n";

	            #page numbering
	            $html_print.="<span class='pages'>\n";
	            if($pages>1)
	            {
	                if( $_GET['p']!=1 )
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/4900-colours/".$version."/?sp=".$_GET['sp']."&amp;p=".($_GET['p']-1)."&amp;l=".($_GET['l']-$_GET['sp'])."' title=''>\n";
	                        $html_print.="<img src='/g/microsite/4900-colours/arrow_left.gif' alt='' />\n";
	                    $html_print.="</a>";
	                }
	                for($i=1;$i<=$pages;$i++)
	                {
	                    if( $i==1 ) $limit2=0; else $limit2=$limit2+$_GET['sp'];
	                    if($_GET['p']!=$i) $class="";
	                    else $class="selected";
	                    $html_print.=" <a class='".$class."' href='/".$_GET['lang']."/art/microsites/4900-colours/".$version."/?sp=".$_GET['sp']."&amp;p=".$i."&amp;l=".$limit2."' title=''>".$i."</a>";
	                }
	                if($_GET['p']!=$pages)
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/4900-colours/".$version."/?sp=".$_GET['sp']."&amp;p=".($_GET['p']+1)."&amp;l=".($_GET['l']+$_GET['sp'])."' title=''>";
	                        $html_print.="<img src='/g/microsite/4900-colours/arrow_right.gif' alt='' />\n";
	                    $html_print.="</a>\n";
	                }
	            }
	            $html_print.="</span>\n";
	            #END

	            $html_print.="<input name='p' type='hidden' value='1' />\n";
	        $html_print.="</form>\n";

	    $html_print.="</div>\n";

	    $html_print.="<div class='div_version_info'>\n";
	            $title=UTILS::row_text_value($db,$row_version,"title");
	            $text=UTILS::row_text_value($db,$row_version,"text");
	            $html_print.="<p class='p_version_title'>".$title."</p>";
	            $html_print.="<p class='p_version_text'>".$text."</p>";
	    $html_print.="</div>\n";

	    if( $count_images>0 )
	    {
	        $html_print.="<ul class='paintings'>\n";
	            $i=0;
	            $ii=0;
	            while($row_images=$db->mysql_array($results_images))
	            {
	                $title=UTILS::row_text_value($db,$row_images,"title");
	                if( $i<($_GET['l']+$_GET['sp']) && $i>=$_GET['l'] )
	                {
		                $html_print.="<li>\n";
			            	$image_href="/images/microsites/large/".$row_images['imageid'].".jpg";
							$href=DATA_PATH_DATADIR.$image_href;
					        $html_print.="<a href='".$href."' title='".$title."' class='fancybox-buttons' data-fancybox-group='microsite-photos'>\n";
				            	$image_src="/images/microsites/medium/".$row_images['imageid'].".jpg";
								$img_src=DATA_PATH_DATADIR.$image_src;
					            $html_print.="<img src='".$img_src."'  alt='".$title."' />\n";
	                        $html_print.="</a>\n";
		        	    	$options_admin_edit=array();
							$options_admin_edit['html_return']=1;
	                        $html_print.=UTILS::admin_edit("/admin/microsites/images/edit/?imageid=".$row_images['imageid']."&versionid=".$versionid."&micrositeid=1",$options_admin_edit);
	                    $html_print.="</li>\n";
	                }
	                $i++;
	            }
	        $html_print.="</ul>\n";
	    }
	    else
	    {
	    	//$html_print.="<p class='p_mocrosites_noresults'>".LANG_SEARCH_NORESULTS."</p>";
	    	UTILS::not_found_404();
	    }

	$html_print.="</div>\n";

	return $html_print;
}

function microsite_sinbad($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
        	/*
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";
                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";
            $html_print.="});\n";
            */

            # search tooltips
            /*
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";
	        */

        $html_print.="});\n";
    $html_print.="</script>\n";

	$micrositeid=3;

	$html_print.="<h1><a href='/".$_GET['lang']."/art/microsites/sinbad/' title='Gerhard Richter ".$options['title']."'><p class='p_h1_fist_part'>Gerhard Richter</p><p class='p_h1_second_part'>".$options['title']."</p></a></h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title='' onclick=\"$('.accordion').slideToggle();\" >".LANG_MICROSITE_ABOUT." ".$options['title']."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;'>\n";
	        	$html_print.=$options['text'];
    	    	$options_admin_edit=array();
				$options_admin_edit['html_return']=1;
	            $html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$micrositeid,$options_admin_edit);
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

		### images
		    $query_text="SELECT *
		        FROM ".TABLE_MICROSITES_IMAGES."
		        WHERE micrositeid='".$micrositeid."'
		        ORDER BY sort";
		$results_images=$db->query($query_text);
		$count=$db->numrows($results_images);
		$count_images=$db->numrows($results_images);

		if( isset($_GET['s']) && !empty($_GET['s']) )
		{

		    $query_text="SELECT *
		        FROM ".TABLE_MICROSITES_IMAGES."
		        WHERE micrositeid='".$micrositeid."' ";
		    for($i=0;$i<=sizeof($search_array);$i++)
		    {
		        if(!empty($search_array[$i]))
		        {
		            if($i==0) $query_text.=" AND number='".$search_array[$i]."'";
		            else $query_text.=" OR number='".$search_array[$i]."'";
		        }
		    }
		    $query_text.=" ORDER BY sort ASC ";
		    $results_images=$db->query($query_text);
		    $count_images=$db->numrows($results_images);
		}


		#end

		$pages=@ceil($count_images/$_GET['sp']);

		$html_print.="<div id='searchEditions'>\n";

			$html_print.="<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW;

			    $html_print.="&nbsp;&nbsp;<select name='sp' id='sp' onchange='this.form.submit();'>\n";
	                for($i=1;$i<=$count_images/20;$i++)
	                {
	                    if($i!=$count_images/20)
	                    {
			                if( $_GET['sp']==(20*$i) )$selected="selected='selected'";
	                        else $selected="";
			                $html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
			            }
			            if( $_GET['sp']==$count_images )$selected2='selected';
	                    else $selected2='';
	                }
	                $html_print.="<option $selected2 value='$count_images'>".LANG_MICROSITE_SEE_ALL."</option>\n";
	            $html_print.="</select>\n";

	            #page numbering
	            $html_print.="<span class='pages'>\n";
	            if($pages>1)
	            {
	                if( $_GET['p']!=1 )
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/sinbad/?sp=".$_GET['sp']."&amp;p=".($_GET['p']-1)."&amp;l=".($_GET['l']-$_GET['sp'])."' title=''>\n";
	                        $html_print.="<img src='/g/microsite/sinbad/arrow_left.gif' alt='' />\n";
	                    $html_print.="</a>";
	                }
	                for($i=1;$i<=$pages;$i++)
	                {
	                    if( $i==1 ) $limit2=0; else $limit2=$limit2+$_GET['sp'];
	                    if($_GET['p']!=$i) $class="";
	                    else $class="selected";
	                        $html_print.=" <a class='".$class."' href='/".$_GET['lang']."/art/microsites/sinbad/?sp=".$_GET['sp']."&amp;p=".$i."&amp;l=".$limit2."' title=''>".$i."</a>";
	                }
	                if($_GET['p']!=$pages)
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/sinbad/?sp=".$_GET['sp']."&amp;p=".($_GET['p']+1)."&amp;l=".($_GET['l']+$_GET['sp'])."' title=''>";
	                        $html_print.="<img src='/g/microsite/sinbad/arrow_right.gif' alt='' />\n";
	                    $html_print.="</a>\n";
	                }
	            }
	            $html_print.="</span>\n";
	            #END

	            $html_print.="<input name='p' type='hidden' value='1' />\n";
	            $html_print.="<div class='clearer'></div>";
	        $html_print.="</form>\n";

	        $html_print.="<div class='clearer'></div>";
	    $html_print.="</div>\n";

	    if( $count_images>0 )
	    {
	        $html_print.="<ul class='paintings'>\n";
	            $i=0;
	            while($row_images=$db->mysql_array($results_images))
	            {
	                $title=UTILS::row_text_value($db,$row_images,"title");
	                if( $i<($_GET['l']+$_GET['sp']) && $i>=$_GET['l'] )
	                {
	                	# view as pair
	                	//$href1="/includes/retrieve.image.php?microsites_imageid=".$row_images['imageid']."&amp;size=pair";
		            	$image1="/images/microsites/pair/".$row_images['imageid'].".jpg";
						$href1=DATA_PATH_DATADIR.$image1;
					    $img="\t<a href='".$href1."' title='".$title."' class='fancybox-buttons a_view_pairs' data-fancybox-group='microsite-photos-pair' >".LANG_MICROSITE_SINBAD_VIEW_AS_PAIR."</a>\n";

					    # thumbnails
		            	$image2="/images/microsites/large/".$row_images['imageid'].".jpg";
						$href2=DATA_PATH_DATADIR.$image2;
					    $list="\t<a href='".$href2."' title='".$row_images['number']."' class='fancybox-buttons' data-fancybox-group='microsite-photos'>\n";
			            	$image3="/images/microsites/medium/".$row_images['imageid'].".jpg";
							$img_src=DATA_PATH_DATADIR.$image3;
					        $list.="\t<img src='".$img_src."'  alt='".$title."' />\n";
	                    $list.="\t</a>\n";

	                    # admin edit link
		        	    	$options_admin_edit=array();
							$options_admin_edit['html_return']=1;
	                        $html_print.=UTILS::admin_edit("/admin/microsites/images/edit/?imageid=".$row_images['imageid']."&versionid=0&micrositeid=".$micrositeid,$options_admin_edit);

	                    $list_array[$i]['image_link']=$list;
	                    $list_array[$i]['image']=$img;
	                }
	                $i++;
	            }

	            $i=0;
	            $ii=0;
	            foreach( $list_array as $key => $value )
	            {
	                $i++;
	                $ii++;
	                if( $ii==1 )
	                {
	                    if( $class_li=="li_right" ) $class_li="li_left";
	                    else $class_li="li_right";
	                    $html_print.="<li class='".$class_li."' >\n";
	                }
	                    $html_print.=$value['image_link'];
	                if( $ii==2 || $i==count($list_array) )
	                {
	                        $html_print.=$value['image'];
	                    $html_print.="</li>\n";
	                    $ii=0;
	                }
	            }
	        $html_print.="</ul>\n";
	    }
	    else $html_print.="<p class='p_mocrosites_noresults'>".LANG_SEARCH_NORESULTS."</p>";

	$html_print.="</div>\n";

	return $html_print;
}

function microsite_elbe($db,$options=array())
{
	$html_print="";

    $html_print.="<script type='text/javascript'>\n";
        $html_print.="$(document).ready(function() {\n";

        	# fancy box
        	/*
            $html_print.="$('.fancybox-buttons').fancybox({";
                $html_print.="openEffect  : 'none',";
                $html_print.="closeEffect : 'none',";
                $html_print.="prevEffect : 'none',";
                $html_print.="nextEffect : 'none',";
                $html_print.="loop : 'false',";
                $html_print.="closeBtn  : false,";
                $html_print.="helpers : { ";
                    $html_print.="title : { ";
                        $html_print.="type : 'inside'";
                    $html_print.="},";
                    $html_print.="buttons : {}";
                $html_print.="},";
                $html_print.="afterLoad : function() {";
                    $html_print.="image_description='.fancybox-desc-' + $(this.element).find('img').attr('data-paintid');";
                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + $(image_description)[0].outerHTML;";
                $html_print.="}";
            $html_print.="});\n";
            */

            # search tooltips
            /*
	        $html_print.="$( document ).tooltip({";
	            $html_print.="items: '[data-title]',";
	            $html_print.="content: $('.search-help-tooltips').html(),";
	            $html_print.="tooltipClass: 'div-tooltips-microsites-search-help',";
	            //$html_print.="hide: {duration: 1000000 }"; //for debuging
	        $html_print.="});";
	        */

        $html_print.="});\n";
    $html_print.="</script>\n";

	$micrositeid=4;

	$html_print.="<h1><a href='/".$_GET['lang']."/art/microsites/elbe/' title='Gerhard Richter ".$options['title']."'><p class='p_h1_fist_part'>Gerhard Richter</p><p class='p_h1_second_part'>".strtoupper($options['title'])."</p></a></h1>\n";

	$html_print.="<div id='holder'>\n";
		$html_print.="<div id='about'>\n";
	    	$html_print.="<a class='about-hook' href='#' title='' onclick=\"$('.accordion').slideToggle();\">".LANG_MICROSITE_ABOUT." ".strtoupper($options['title'])."</a>\n";
	    	$html_print.="<div class='accordion' style='display:none;'>\n";
	            $text=UTILS::row_text_value($db,$row,"text");
	        	$html_print.=$options['text'];
    	    	$options_admin_edit=array();
				$options_admin_edit['html_return']=1;
	            $html_print.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$micrositeid,$options_admin_edit);
	        $html_print.="</div>\n";
	    $html_print.="</div>\n";

	    ### images
	    $results_images=$db->query("SELECT * FROM ".TABLE_MICROSITES_IMAGES." WHERE micrositeid='".$micrositeid."' ORDER BY sort");
	    $count_images=$db->numrows($results_images);
	    #end

	    $pages=@ceil($count_images/$_GET['sp']);

	    $html_print.="<div id='searchEditions'>\n";
			$html_print.="<form id='editionShow' action='' method='get'>".LANG_MICROSITE_SHOW;
			    $html_print.="&nbsp;&nbsp;<select name='sp' id='sp' onchange='this.form.submit();'>\n";
	                for($i=1;$i<=$count_images/20;$i++)
	                {
	                    if($i!=$count_images/20)
	                    {
			                if( $_GET['sp']==(20*$i) )$selected="selected='selected'";
	                        else $selected="";
			                $html_print.="<option $selected value='".(20*$i)."'>".(20*$i)." ".LANG_MICROSITE_PER_PAGE."</option>\n";
			            }
			            if( $_GET['sp']==$count_images )$selected2='selected';
	                    else $selected2='';
	                }
	                $html_print.="<option $selected2 value='$count_images'>".LANG_MICROSITE_SEE_ALL."</option>\n";
	            $html_print.="</select>\n";

	            #page numbering
	            $html_print.="<span class='pages'>\n";
	            if($pages>1)
	            {
	                if( $_GET['p']!=1 )
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/elbe/?sp=".$_GET['sp']."&amp;p=".($_GET['p']-1)."&amp;l=".($_GET['l']-$_GET['sp'])."' title=''>\n";
	                        $html_print.="<img src='/g/microsite/elbe/arrow_left.gif' alt='' />\n";
	                    $html_print.="</a>";
	                }
	                for($i=1;$i<=$pages;$i++)
	                {
	                    if( $i==1 ) $limit2=0; else $limit2=$limit2+$_GET['sp'];
	                    if($_GET['p']!=$i) $class="";
	                    else $class="selected";
	                        $html_print.=" <a class='".$class."' href='/".$_GET['lang']."/art/microsites/elbe/?sp=".$_GET['sp']."&amp;p=".$i."&amp;l=".$limit2."' title=''>".$i."</a>";
	                }
	                if($_GET['p']!=$pages)
	                {
	                    $html_print.=" <a href='/".$_GET['lang']."/art/microsites/elbe/?sp=".$_GET['sp']."&amp;p=".($_GET['p']+1)."&amp;l=".($_GET['l']+$_GET['sp'])."' title=''>";
	                        $html_print.="<img src='/g/microsite/elbe/arrow_right.gif' alt='' />\n";
	                    $html_print.="</a>\n";
	                }
	            }
	            $html_print.="</span>\n";
	            #END

	            $html_print.="<input name='p' type='hidden' value='1' />\n";
	        $html_print.="</form>\n";

	        $html_print.="<div class='clearer'></div>";
	    $html_print.="</div>\n";

	    if( $count_images>0 )
	    {
	        $html_print.="<ul class='paintings'>\n";
	            $i=0;
	            $ii=0;
	            while($row_images=$db->mysql_array($results_images))
	            {
	                $title=UTILS::row_text_value($db,$row_images,"title");
	                if( $i<($_GET['l']+$_GET['sp']) && $i>=$_GET['l'] )
	                {
		                $html_print.="<li>\n";
				        	$image1="/images/microsites/large/".$row_images['imageid'].".jpg";
							$href=DATA_PATH_DATADIR.$image1;
					        $html_print.="<a href='".$href."' title='".$title."' class='fancybox-buttons' data-fancybox-group='microsite-photos'>\n";
					        	$image2="/images/microsites/medium/".$row_images['imageid'].".jpg";
								$img_src=DATA_PATH_DATADIR.$image2;
					            $html_print.="<img src='".$img_src."'  alt='".$title."' />\n";
	                        $html_print.="</a>\n";
			    	    	$options_admin_edit=array();
							$options_admin_edit['html_return']=1;
	                        $html_print.=UTILS::admin_edit("/admin/microsites/images/edit/?imageid=".$row_images['imageid']."&versionid=0&micrositeid=4",$options_admin_edit);
	                    $html_print.="</li>\n";
	                }
	                $i++;
	            }
	        $html_print.="</ul>\n";
	    }
	    else $html_print.="<p class='p_mocrosites_noresults'>".LANG_SEARCH_NORESULTS."</p>";

	$html_print.="</div>\n";

	return $html_print;
}

function microsite_november($db,$options=array())
{
	$html_print="";

	$micrositeid=5;

	$html_print.='<!-- No JS Warning -->';
	$html_print.='<noscript>';
		$html_print.='<div class="banner">';
			$html_print.='<h1>This page requires JavaScript</h1>';
			$html_print.='<p>JavaScript currently is not available on your browser. This page will not work properly. Please enable <a href="http://en.wikipedia.org/wiki/JavaScript" target="blank">JavaScript</a> or upgrade your <a href="http://en.wikipedia.org/wiki/Comparison_of_web_browsers" target="blank">browser</a>.</p>';
		$html_print.='</div>';
	$html_print.='</noscript>';

	$html_print.='<div id="wrapper">';

		$html_print.='<!-- Header -->';
		$html_print.='<div id="header">';
			$html_print.='<div class="header_introduction">';
				$html_print.='<h1>'.$options['title'].'</h2>';
                $html_print.=$options['text'];
                /*
				$html_print.='<div class="header_descript_left">';
					$html_print.='<p><span>Gerhard Richter</span> produced <i>November</i>, a series of 54 works in ink, in 2008. These works were created using a variety of ways to manipulate the consistency of ink as well as its flow on highly absorbent paper. The ink permeated through the sheets creating two related images, one on the front, one on the back of each of the altogether 27 sheets. In some cases Richter also asplied lacquer or pencil.</p>';
				$html_print.='</div>';
				$html_print.='<div class="header_descript_right">';
					$html_print.='<p>The artist had a complete set of facsimiles made so both sides of the 27 sheets could be viewed at the same time. However, the sequence of mirrored pairs of images is disrupted, as some of the motifs have been rotated by 180 degrees.</p>';
					$html_print.='<p>Every page is dated, mainly in sequence of their production.<br />Website layout according to <a href="http://www.gerhard-richter.com/literature/detail.php?2004" target="blank" title="Show “November” in literature section"><i>November</i></a>, Heni Publishing, 2013.</p>';
				$html_print.='</div>';
                */
				$html_print.='<div class="clear"></div>';
			$html_print.='</div>';
			$html_print.='<div class="header_navigation">';
				$html_print.='<div id="header_per_page">';
					$html_print.='<ul>';
						$html_print.='<li>'.ucwords(LANG_MICROSITE_SHOW).':</li>';
						$html_print.='<li><a href="">'.ucwords(LANG_MICROSITE_SEE_ALL).'</a></li>';
						$html_print.='<li><a href="">24</a></li>';
						$html_print.='<li><a href="">12</a></li>';
						$html_print.='<li class="selected"><a href="">8</a></li>';
					$html_print.='</ul>';
				$html_print.='</div>';
				$html_print.='<div id="header_page_navigation"></div>';
				$html_print.='<div class="clear"></div>';
			$html_print.='</div>';

		$html_print.='</div>';

		$html_print.='<!-- Content -->';
		$html_print.='<div id="content"><ul></ul></div>';
		$html_print.='<div class="clear"></div>';

		$html_print.='<!-- Footer -->';
		$html_print.='<div id="footer">';
			$html_print.='<div class="footer_navigation">';
				$html_print.='<div id="footer_per_page">';
					$html_print.='<ul>';
						$html_print.='<li>'.ucwords(LANG_MICROSITE_SHOW).':</li>';
						$html_print.='<li><a href="">'.ucwords(LANG_MICROSITE_SEE_ALL).'</a></li>';
						$html_print.='<li><a href="">24</a></li>';
						$html_print.='<li><a href="">12</a></li>';
						$html_print.='<li class="selected"><a href="">8</a></li>';
					$html_print.='</ul>';
				$html_print.='</div>';
				$html_print.='<div id="footer_page_navigation"></div>';
				$html_print.='<div class="clear"></div>';
			$html_print.='</div>';
			$html_print.='<div class="footer_sitemap">';
                $html_print.='<p>© '.date('Y').' www.gerhard-richter.com – '.LANG_MICROSITE_NOVEMBER_FOOTER_1.' <a href="http://www.gerhard-richter.com/" target="blank">www.gerhard-richter.com</a> '.LANG_MICROSITE_NOVEMBER_FOOTER_2.'</p>';
			$html_print.='</div>';
		$html_print.='</div>';

    $html_print.='<ul id="languages">';
        $html_print.='<li><a href="#lang" data-lang="en">English</a></li>';
        $html_print.='<li class="selected" data-lang="de"><a href="#lang">Deutsch</a></li>';
    $html_print.='</ul>';

	$html_print.='</div>';

	$html_print.='<div id="overlay">';
		$html_print.='<div class="single">';
			$html_print.='<ul id="overlay_images">';
				$html_print.='<li><div></div><p><b>3 of 54</b> &mdash; sheet 2 of 27 (recto) &mdash; 3 Nov 2008</p></li>';
			$html_print.='</ul>';
			$html_print.='<ul id="overlay_modes">';
				$html_print.='<li></li>';
				$html_print.='<li></li>';
			$html_print.='</ul>';
			$html_print.='<a id="overlay_close" href="">Close</a>';
			$html_print.='<a id="overlay_prev" href=""></a>';
			$html_print.='<a id="overlay_next" href=""></a>';
		$html_print.='</div>';
	$html_print.='</div>';

	return $html_print;
}

?>
