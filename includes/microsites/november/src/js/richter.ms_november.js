/*
	| richter.ms_november.js
	| 
	| Description:
	| Gerhard Richter Microsite 'November' (www.gerhard-richter.com)
	| JavaScript file
	| Author: Eric Kuhnert
	| Date: 2013-02-20
	|

	| Changelog
	|
	| 2013-03-12 * ADD * overlay navigation via key events (cursor left: prev, cursor right: next, cursor up/down: toggle view modes)
	| 2013-03-12 * ADD * language texts from external json file
	| 2013-03-12 * ADD * language selection bar
*/

if (richter == null) var richter = {}

richter.ms_november = {

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	current_language : ($('html').attr('lang')) ? $('html').attr('lang').toLowerCase() : 'en',		// uses html lang attribute: <html lang="de">
	language_file : '/includes/microsites/november/src/json/richter.ms_november.languages.json',
	languages : null,

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	nav_nodes : [],
	max_items : 54,
	per_page : 0,
	curr_page : 0,
	max_page : 0,
	start_page : 1,
	overlay_image : 1,
	overlay_mode : 0,
	days : [
		'', '2', '2', '3', '3', '4', 
		'4', '5', '5', '7', '7', 
		'6', '6', '10', '10', '9', 
		'9', '8', '8', '11', '11', 
		'14', '14', '12', '12', '13', 
		'13', '15', '15', '16', '16', 
		'17', '17', '18', '18', '19', 
		'19', '20', '20', '21', '21', 
		'22', '22', '23', '23', '24', 
		'24', '25', '24', '26', '26', 
		'27', '27', '28', '28'
	],
	ids : [
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
		11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
		31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
		51, 52, 53, 54
	],
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// fixes layout issues IE has due to CSS selector bugs
	fixMSIE : function (_args)
	{
		if (!$.browser.msie) return true;
		var version = Number($.browser.version.split('.')[0]);
		if (version > 8) return true;
		$(document).find('#content > ul > li:nth-child(odd)').each(
			function (_event)
			{
				$(this).css({"margin-right":"1px"});
			}
		);
		$(document).find('#content > ul > li > ul > li:last-child > div').each(
			function (_event)
			{
				$(this).css({"margin-left":"16px"});
			}
		);
		$(document).find('#header_page_navigation > ul > li:last-child > a, #footer_page_navigation > ul > li:last-child > a').each(
			function (_event)
			{
				$(this).css({"background":'transparent url("/includes/microsites/november/src/pic/ric_ms_november_pagenav_ends.png") no-repeat scroll right top'});
			}
		);
		$(document).find('#content > ul > li > ul > li > div').each(
			function (_event)
			{
				$(this).css({"backgroundSize":'cover'});
			}
		);
		$(document).find('#overlay #overlay_modes > li:last-child').each(
			function (_event)
			{
				$(this).css({"backgroundPosition":'right top'});
			}
		);
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// switch between languages loaded from json file (init)
	switch_language : function (_args)
	{
		if ( (typeof(this.languages) == 'object') && (_args) && (_args.hasOwnProperty('language')) )
		{
			if (!this.languages.hasOwnProperty(_args.language)) _args.language = 'en';
			this.current_language = _args.language;
			$( $(document).find(".header_descript_left")[0] ).html(this.languages[this.current_language].description[0]);
			$( $(document).find(".header_descript_right")[0] ).html(this.languages[this.current_language].description[1]);
			$( $(document).find("#header_per_page > ul > li")[0] ).html(this.languages[this.current_language].navigation_show);
			$( $(document).find("#header_per_page > ul > li")[1] ).find("a").html(this.languages[this.current_language].navigation_all);
			$( $(document).find("#footer_per_page > ul > li")[0] ).html(this.languages[this.current_language].navigation_show);
			$( $(document).find("#footer_per_page > ul > li")[1] ).find("a").html(this.languages[this.current_language].navigation_all);
			$( $(document).find(".footer_sitemap")[0] ).html("<p>" + this.languages[this.current_language].footer + "</p>");
			var html = "";
			for (var i = 0; i < this.languages.languages.length; i++)
			{
				var selected = (this.languages.languages[i] == this.current_language) ? ' class="selected"' : '';
				html += "<li" + selected + "><a href='javascript:richter.ms_november.switch_language(" + ' { "language" : "' + this.languages.languages[i] + '" } );' + "' >" + this.languages[ this.languages.languages[i] ].lang + "</a></li>";
			}
			$(document).find("#languages").html(html);
		}
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// switch between 'show per page' options
	show_per_page : function (_args)
	{
		this.per_page = ($.isNumeric(_args.per_page)) ? _args.per_page : this.max_items;
		var nodes = [ this.nav_nodes['header_per_page'], this.nav_nodes['footer_per_page'] ];
		$( nodes ).each(
			function (_event)
			{
				$( this ).find('li').each( 
					function (_event)
					{
						((this.childNodes[0].tagName === 'A') && (this.childNodes[0].innerHTML == _args.per_page)) ? $(this).addClass('selected') : $(this).removeClass('selected');
					}
				);
			}
		);
		this.max_page = Math.ceil(this.max_items / this.per_page);
		this.start_page = 1;
		var args = {
			'page' : 1
		};
		this.show_page(args);
	},

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// displays a page with its items
	show_page : function (_args)
	{
		var delta = _args.page - this.curr_page;
		if (_args.page > this.max_page) _args.page = this.max_page;
		if (_args.page < 1) _args.page = 1;
		this.curr_page = _args.page;
		// page navigation...
		this.update_page_nav( { 'delta' : delta } );
		// content...
		var start = ((this.curr_page - 1) * this.per_page) + 1;
		var stop = start + (this.per_page - 1);
		if (stop > this.max_items) stop = this.max_items;
		var html = '<ul>';
		for (var i = start; i < stop; i += 2)
		{
			var img1 = (i < 10) ? '0' + i : '' + i;
			var img2 = (i < 9) ? '0' + (i + 1) : '' + (i + 1);
			var url = [ "/includes/microsites/november/src/pic/thumbs/ric_ms_november_work_" + img1 + ".jpg", "/includes/microsites/november/src/pic/thumbs/ric_ms_november_work_" + img2 + ".jpg" ];
			html += '<li><ul><li><div data-imgurl="' + url[0] + '"></div><p>' + i + '</p></li><li><div data-imgurl="' + url[1] + '"></div><p>' + (i + 1) + '</p></li></ul></li>';
		}
		html += '';
		html += '</ul>';
		$( document.getElementById('content') ).html(html);
		$( document.getElementById('content') ).find('li').each(
			function (_event)
			{
				$(this).click(
					function (_evt)
					{
						if ( $(this).find('ul').length == 0 ) richter.ms_november.open_overlay_image( { 'obj' : this } );
					}
				);
			}
		);
		$( document.getElementById('content') ).find('div').each(
			function (_evt)
			{
				if ($(this).attr('data-imgurl'))
				{
					this.image = new Image();
					this.image.owner = this;
					$(this.image).load(
						function ( _evt )
						{
							$(this.owner).append('<img src="' + this.src + '" width="' + $(this.owner).width() + 'px" height="' + ($(this.owner).height()) + 'px" />');
						}
					);
					this.image.src = $(this).attr('data-imgurl');
				}
			}
		);
		this.fixMSIE();
	},

	// . . . . . . . . . . . . . . . . . . .  . . . . . . . . . . . . . . . . . . . . . . .
	// updates page navigation
	update_page_nav : function (_args)
	{
		var args = {};
		args.pages = 3;
		args.delta = _args.delta;
		args.start = richter.ms_november.start_page;
		if (args.delta > 0) args.start = richter.ms_november.curr_page - args.pages + 1;
		if (args.start > richter.ms_november.curr_page) args.start = richter.ms_november.curr_page;
		if (args.start < 1) args.start = 1;
		if (args.start > richter.ms_november.max_page) args.start = richter.ms_november.max_page;
		richter.ms_november.start_page = args.start;
		var nodes = [ this.nav_nodes['header_page_nav'], this.nav_nodes['footer_page_nav'] ];
		$( nodes ).each(
			function (_event)
			{
				if (richter.ms_november.max_page == 1)
				{
					$(this).html('<ul></ul>');
					return;
				}
				var html = '<ul><li><a href="">1</a></li><li class="header_page_navigation_left"><a href=""></a></li>';
				for (var i = args.start; i < args.start + args.pages; i++)
					if (i <= richter.ms_november.max_page)
					{
						html += (i == richter.ms_november.curr_page) ? '<li class="selected"><a href="">' + i + '</a></li>' : '<li><a href="">' + i + '</a></li>';
					}
				html += '<li class="header_page_navigation_right"><a href=""></a></li><li><a href="">' + richter.ms_november.max_page + '</a></li></ul>';
				$(this).html(html);
				$(this).find('li').each(
					function (_event)
					{
						$(this).click(
							function (_event)
							{
								richter.ms_november.show_page_click(_event);
							}
						);
					}
				);
			}
		);		
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// event handler function which shows a page via show_page() 
	show_page_click : function (_event)
	{
		var num = ($.isNumeric(_event.target.innerHTML)) ? Number(_event.target.innerHTML) : ( (_event.target.parentNode.className == 'header_page_navigation_right') ? richter.ms_november.curr_page + 1 : richter.ms_november.curr_page - 1 );
		var args = { 'page' : num };
		_event.preventDefault();
		richter.ms_november.show_page( args );
	},	

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// toggle between overlay's single or pair (double) mode
	switch_overlay_mode : function (_args)
	{
		this.overlay_mode = _args.mode;
		function switch_overlay_mode_single () {
			var dialogue = $( document.getElementById('overlay') ).find('div')[0];
			dialogue.className = "single";
			richter.ms_november.show_overlay_image();
		}
		function switch_overlay_mode_double () {
			var dialogue = $( document.getElementById('overlay') ).find('div')[0];
			dialogue.className = "double";
			richter.ms_november.show_overlay_image();
		}
		(_args.mode == 0) ? switch_overlay_mode_single() : switch_overlay_mode_double();
	},	

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// return 'recto' or 'verso' for numeric index
	recto_verso : function (_value)
	{
		var side = (_value % 2 != 0) ? 'recto' : 'verso';
		return side;
	},

	get_image_descript_str : function (_args)
	{
		var url = (_args.work < 10) ? _args.url + '0' + _args.work : _args.url + _args.work;
		var side = this.recto_verso(richter.ms_november.ids[_args.work]);
		var str = '<li><div data-imgurl="' + url + '.jpg"></div><p>' + _args.description + '</li>';

		str = str.replace('%work', _args.work)
			.replace('%works', _args.works)
			.replace('%sheet', _args.sheet)
			.replace('%sheets', _args.sheets)
			.replace('%side', side)
			.replace('%day', _args.day);

		return str;

	},
		
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// show an overlay image, navigation via _args.image (index) or _args.delta (direction)
	show_overlay_image : function (_args)
	{
		if ((_args) && (_args.hasOwnProperty('image'))) this.overlay_image = _args.image;
		if ((_args) && (_args.hasOwnProperty('delta'))) this.overlay_image = (this.overlay_mode == 0) ? this.overlay_image + _args.delta : this.overlay_image + (2 * _args.delta);
		if (this.overlay_image < 1) this.overlay_image = 1;
		if (this.overlay_image > richter.ms_november.max_items) this.overlay_image = richter.ms_november.max_items;
		if ((this.overlay_mode == 1) && (this.overlay_image % 2 == 0)) this.overlay_image = this.overlay_image - 1;
		if (this.overlay_mode == 0)
		{
			var args = {
				description : ((this.languages != null)) ? this.languages[this.current_language].overlay : "<b>%work of %works</b> &mdash; sheet %sheet of %sheets (%side) &mdash; %day Nov 2008",
				work : this.overlay_image,
				works : richter.ms_november.max_items,
				sheet : Math.ceil(richter.ms_november.ids[this.overlay_image] / 2),
				sheets : richter.ms_november.max_items / 2,
				day : richter.ms_november.days[this.overlay_image],
				url : "/includes/microsites/november/src/pic/works/ric_ms_november_work_"
			};
			var html = this.get_image_descript_str(args);
		} else {
			var args = {
				description : ((this.languages != null)) ? this.languages[this.current_language].overlay : "<b>%work of %works</b> &mdash; sheet %sheet of %sheets (%side) &mdash; %day Nov 2008",
				work : this.overlay_image,
				works : richter.ms_november.max_items,
				sheet : Math.ceil(richter.ms_november.ids[this.overlay_image] / 2),
				sheets : richter.ms_november.max_items / 2,
				day : richter.ms_november.days[this.overlay_image],
				url : "/includes/microsites/november/src/pic/works/ric_ms_november_work_"
			};
			var html = this.get_image_descript_str(args);
			var args = {
				description : ((this.languages != null)) ? this.languages[this.current_language].overlay : "<b>%work of %works</b> &mdash; sheet %sheet of %sheets (%side) &mdash; %day Nov 2008",
				work : this.overlay_image + 1,
				works : richter.ms_november.max_items,
				sheet : Math.ceil(richter.ms_november.ids[this.overlay_image] / 2),
				sheets : richter.ms_november.max_items / 2,
				day : richter.ms_november.days[this.overlay_image],
				url : "/includes/microsites/november/src/pic/works/ric_ms_november_work_"
			};
			html += this.get_image_descript_str(args);
		}
		$( document.getElementById('overlay_images') ).html(html);
		$( document.getElementById('overlay_images') ).find('div').each(
			function (_evt)
			{
				if ($(this).attr('data-imgurl'))
				{
					this.image = new Image();
					this.image.owner = this;
					$(this.image).load(
						function ( _evt )
						{
							$(this.owner).append('<img src="' + this.src + '" width="' + $(this.owner).width() + 'px" height="' + ($(this.owner).height()) + 'px" />');
						}
					);
					this.image.src = $(this).attr('data-imgurl');
				}
			}
		);
	},	
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// open overlay image _args.image (index)
	open_overlay_image : function (_args)
	{
		if ((_args.hasOwnProperty('obj')) && (_args.obj != null))
		{
			var i = Number($(_args.obj).find('p')[0].innerHTML);
			$(document.getElementById('overlay')).show();
			this.show_overlay_image( { 'image' : i });
		}
	},
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// initialise microsite object
	init : function (_args)
	{
		this.nav_nodes['header_per_page'] = document.getElementById('header_per_page');
		this.nav_nodes['footer_per_page'] = document.getElementById('footer_per_page');
		this.nav_nodes['header_page_nav'] = document.getElementById('header_page_navigation');
		this.nav_nodes['footer_page_nav'] = document.getElementById('footer_page_navigation');
		var nodes = [ this.nav_nodes['header_per_page'], this.nav_nodes['footer_per_page'] ];
		$( nodes ).each(
			function (_event)
			{
				$( this ).find('li').each( 
					function (_event)
					{
						if (this.childNodes[0].tagName === 'A') 
						{
							$(this).click(
								function (_evt)
								{
									richter.ms_november.show_per_page( { 'per_page' : this.childNodes[0].innerHTML } );
									_evt.preventDefault();
								}
							);
						}
					}
				);
			}
		);
		this.show_per_page( { 'per_page' : ( ((_args) && (_args.hasOwnProperty('per_page')) && ($.isNumeric(_args.per_page))) ? _args.per_page : 8 ) } );
		$( document.getElementById('overlay') ).hide();
		$( document.getElementById('overlay_modes') ).find('li').each(
			function (_event)
			{
				this.data = _event;
				$(this).click(
					function (_e)
					{
						richter.ms_november.switch_overlay_mode( { 'mode' : this.data } );
					}
				);
			}
		);
		$(document.getElementById('overlay_next')).click( function (_event) { richter.ms_november.show_overlay_image( { 'delta' : 1 } ); _event.preventDefault(); } );
		$(document.getElementById('overlay_prev')).click( function (_event) { richter.ms_november.show_overlay_image( { 'delta' : -1 } ); _event.preventDefault(); } );
		$(document.getElementById('overlay_close')).click( function (_event) { $(document.getElementById('overlay')).hide(); _event.preventDefault(); } );
		$(document.getElementById('overlay')).click( function (_event) { if (_event.target.id == 'overlay') $(document.getElementById('overlay')).hide(); } );
		$('body').keydown( 
			function (_event) {
				if ($('#overlay:hidden').length == 0)
				 switch(_event.keyCode)
				{
					case 37:
  						// left
  						richter.ms_november.show_overlay_image( { 'delta' : -1 } );
  						_event.preventDefault();
  						break;
					case 39:
  						// right
  						richter.ms_november.show_overlay_image( { 'delta' : 1 } );
  						_event.preventDefault();
  						break;
					case 38:
  						// up
  						_event.preventDefault();
  						richter.ms_november.switch_overlay_mode( { 'mode' : 0 } );
  						break;
  					case 40:
  						// up
  						_event.preventDefault();
  						richter.ms_november.switch_overlay_mode( { 'mode' : 1 } );
  						break;
  					case 27:
  						// close
  						$(document.getElementById('overlay')).hide();
  						_event.preventDefault();
  						break;
				}
			}
		);

		// load language file...
		var jqxhr = $.getJSON(this.language_file, 
			function(data) {
				richter.ms_november.languages = data;
				(typeof(richter.ms_november.current_language) == 'string') ? richter.ms_november.switch_language( { "language" : richter.ms_november.current_language } ) : richter.ms_november.switch_language( { "language" : data.languages[0] } );
			}
		).error(
			function() {
				alert("Error: Couldn't load '" + this.url + "'");
			}
		);
	}

}

$(document).ready(function() {
      richter.ms_november.init( { 'per_page' : 12 } );
});