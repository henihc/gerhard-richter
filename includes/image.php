<?php
session_start();
/* -----DOC-----

  Generate the Captcha and set $_SESSION['captcha'] to compare it against.

  -----DOC-----*/
  
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");

    $fontarr = array("../fonts/Dustismo_Roman.ttf","../fonts/Dustismo_sans.ttf");
    $font_number = mt_rand(0,(sizeof($fontarr)-1));
    $font=$fontarr[$font_number];

    function generateCode($characters) {
        /* list all possible characters, similar looking characters and vowels have been removed */
        $possible = '23456789bcdfghjkmnpqrstvwxyz';
        $code = '';
        $i = 0;
        while ($i < $characters) { 
            $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
            $i++;
        }
        return $code;
    }

$width = isset($_GET['width']) ? $_GET['width'] : '170';
$height = isset($_GET['height']) ? $_GET['height'] : '50';
$characters = isset($_GET['characters']) && $_GET['characters'] > 1 ? $_GET['characters'] : '6';

        $code = generateCode($characters);
        /* font size will be 75% of the image height */
        $font_size = $height * 0.75;
        $image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
        /* set the colours */
        $background_color = imagecolorallocate($image, 181, 181, 181);
        $text_color = imagecolorallocate($image, 54, 54, 54);
        $noise_color = imagecolorallocate($image, 116, 119, 119);
        /* generate random dots in background */
        for( $i=0; $i<($width*$height)/3; $i++ ) {
            imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
        }
        /* generate random lines in background */
        for( $i=0; $i<($width*$height)/150; $i++ ) {
            imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
        }
        /* create textbox and add text */
        $textbox = imagettfbbox($font_size, 0, $font, $code) or die('Error in imagettfbbox function');
        $x = ($width - $textbox[4])/2;
        $y = ($height - $textbox[5])/2;
        imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code) or die('Error in imagettftext function');
        /* output captcha image to browser */
        header('Content-Type: image/png');
        imagepng($image);
        imagedestroy($image);
        $_SESSION['captcha'] = $code;

?>
