<?php
function search_art($db,$options)
{

    ### prepeare search variables
    $title=$db->db_prepare_input($options['get']['title']);
    $number=str_replace(" ", "", $db->db_prepare_input($options['get']['number']));
    $number_from=$db->db_prepare_input($options['get']['number-from']);
    $number_to=$db->db_prepare_input($options['get']['number-to']);
    $yearfrom=$db->db_prepare_input($options['get']['year-from']);
    $yearto=$db->db_prepare_input($options['get']['year-to']);
    $options['get']['location']=$db->db_prepare_input($options['get']['location']);
    $options['get']['keyword']=$db->db_prepare_input($options['get']['keyword']);
    $sizeheight_min=$db->db_prepare_input($options['get']['size-height-min']);
    $sizeheight_max=$db->db_prepare_input($options['get']['size-height-max']);
    $sizewidth_min=$db->db_prepare_input($options['get']['size-width-min']);
    $sizewidth_max=$db->db_prepare_input($options['get']['size-width-max']);
    //$options['get']=$db->db_prepare_input($options['get']);
    #end

    ### prepeare artwork search for query
    $artworkid=$db->db_prepare_input($options['get']['artworkid']);

    if($artworkid=="paintings")
    {
        $artworks=" AND (p.artworkID=1 or p.artworkID=2 or p.artworkID=13) ";
    }
    elseif( $artworkid=="other" )
    {
        $artworks=" AND p.artworkID=14 ";
    }
    elseif( !empty($artworkid) )
    {
        $artworks=" AND ( p.artworkID='".$artworkid."' OR p.artworkID2='".$artworkid."' ) ";
    }
    else
    {
        $artworks=" AND ( p.artworkID!=16 OR p.artworkID2!=16 ) ";
    }
    /*
    else
    {

        if( !$_SESSION['kiosk'] ) $artworks=" p.artworkID LIKE '%' ";
        else $artworks=" p.artworkID!=4 ";
    }
    */
    #end

    ### prepearing number for serach query
    if( !empty($number) || !empty($options['get']['number_list']) || !empty($options['get']['number-from']) || !empty($options['get']['number-to']) )
    {
        $numbers = preg_split("/[\s,]+/", $number);
        if( count($numbers)>1 )
        {
            $options['get']['number_list']=array();
            foreach ($numbers as $value)
            {
                $options['get']['number_list'][]=$value;
            }
            //print_r($options['get']['number_list']);
            //exit;
        }
        //print_r($numbers);

        # number_list
        if( !empty($options['get']['number_list']) )
        {
            $number="";
            foreach( $options['get']['number_list'] as $key => $number_list_value )
            {
                //$number_search.=$number_list_value."* ";
                $number.=$number_list_value.", ";
            }
        }
        # END number_list
        # number from to
        if( !empty($options['get']['number-from']) || !empty($options['get']['number-to']) )
        {
            if( !empty($options['get']['number-from']) && !empty($options['get']['number-to']) )
            {
                $number=$options['get']['number-from'].":".$options['get']['number-to'];
            }
            elseif( !empty($options['get']['number-from']) )
            {
                #could take quite long to search for this!!!!
                $number=$options['get']['number-from'].":1000";
            }
            elseif( !empty($options['get']['number-to']) )
            {
                $number="1:".$options['get']['number-to'];
            }
        }
        # END number from to

        $options_search_array_number=array();
        $options_search_array_number['number']=$number;
    	$number_search=UTILS::prepare_cr_number_search($db,$options_search_array_number);
    	//$search_array=UTILS::search_array($number);
        //print_r($search_array);
        //exit;

        //print $number_search;
    }
    #end number search preperae



	###search query
    $query_where_paintings=" WHERE p.enable=1 ";
    $query_where_paintings.=$artworks;

    # Location search
    if( strlen($options['get']['location'])>0 || strlen($options['get']['keyword'])>0 )
    {
        if( !empty($options['get']['keyword']) ) $location_search_term=$options['get']['keyword'];
        else $location_search_term=$options['get']['location'];
        //$location_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['location']));

        #LOCATION

        # prepare search keyword
        $options_search=array();
        $options_search['db']=$db;
        $prepear_search=UTILS::prepear_search($location_search_term,$options_search);
        # END prepare search keyword

        # prepare match
        $options_search_locations=array();
        $options_search_locations['search']=$prepear_search;
        $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$options_search_locations);
        $query_where_locations=" WHERE ".$query_match_locations;
        # END prepare match

        $query_locations=QUERIES::query_locations($db,$query_where_locations);
        $results_locations=$db->query($query_locations['query_without_limit']);
        $count_locations=$db->numrows($results_locations);
        //print "---".$count_locations;
        if( $_SESSION['debug_page'] )
        {
            print "<br /><br />".$query_where_locations."<br />";
            print "Locations found = ".$count_locations."<br />";
        }
        if( $count_locations>0 )
        {
            while( $row_locations=$db->mysql_array($results_locations) )
            {
                $results_loc=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE locationid='".$row_locations['locationid']."' OR loan_locationid='".$row_locations['locationid']."' ");
                $count_loc=$db->numrows($results_loc);
                if( $_SESSION['debug_page'] ) print "Found paintings with location ".$row_locations['location_en']." = ".$count_loc."<br />";
                if( $count_loc>0 )
                {
                    while( $row_loc=$db->mysql_array($results_loc) )
                    {
                        $locations_paintings[$row_loc['paintID']]=$row_loc['paintID'];
                    }
                }
            }
        }

        # LOCATION CITY

        # prepare match
        $options_search_locations_city=array();
        $options_search_locations_city['search']=$prepear_search;
        $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$options_search_locations_city);
        $query_where_locations_city=" WHERE ".$query_match_locations_city." AND ci.cityid!=668 ";
        # END prepare match

        $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
        $results_locations_city=$db->query($query_locations_city['query_without_limit']);
        $count_locations_city=$db->numrows($results_locations_city);
        //print "---".$count_locations_city;
        if( $_SESSION['debug_page'] )
        {
            print "<br /><br />".$query_where_locations_city."<br />";
            print "Cities found = ".$count_locations_city."<br />";
        }
        if( $count_locations_city>0 )
        {
            while( $row_locations_city=$db->mysql_array($results_locations_city) )
            {
                $results_loc_city=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE cityid='".$row_locations_city['cityid']."' OR loan_cityid='".$row_locations_city['cityid']."' ");
                $count_loc_city=$db->numrows($results_loc_city);
                if( $_SESSION['debug_page'] ) print "Found paintings with city ".$row_locations_city['city_en']." = ".$count_loc_country."<br />";
                if( $count_loc_city>0 )
                {
                    while( $row_loc_city=$db->mysql_array($results_loc_city) )
                    {
                        $locations_paintings[$row_loc_city['paintID']]=$row_loc_city['paintID'];
                    }
                }
            }
        }

        # LOCATION COUNTRY

        # prepare match
        $options_search_locations_country=array();
        $options_search_locations_country['search']=$prepear_search;
        $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$options_search_locations_country);
        $query_where_locations_country=" WHERE ".$query_match_locations_country." AND co.countryid!=83 ";
        # END prepare match

        $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
        $results_locations_country=$db->query($query_locations_country['query_without_limit']);
        $count_locations_country=$db->numrows($results_locations_country);
        //print "---".$count_locations_country;
        if( $_SESSION['debug_page'] )
        {
            print "<br /><br />".$query_where_locations_country."<br />";
            print "Countries found = ".$count_locations_country."<br />";
        }
        if( $count_locations_country>0 )
        {
            while( $row_locations_country=$db->mysql_array($results_locations_country) )
            {
                $results_loc_country=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE countryid='".$row_locations_country['countryid']."' OR loan_countryid='".$row_locations_country['countryid']."' ");
                $count_loc_country=$db->numrows($results_loc_country);
                if( $_SESSION['debug_page'] ) print "Found paintings with country ".$row_locations_country['country_en']." = ".$count_loc_country."<br />";
                if( $count_loc_country>0 )
                {
                    while( $row_loc_country=$db->mysql_array($results_loc_country) )
                    {
                        $locations_paintings[$row_loc_country['paintID']]=$row_loc_country['paintID'];
                    }
                }
            }
        }

        if( !empty($locations_paintings) )
        {
            $query_where_paintings.=" AND ";
            if( !empty($title) ) $query_where_paintings.=" ( "; # bracket for ( location OR match )
            $query_where_paintings.=" ( "; # bracket for ( location )
            $i_loc=0;
            foreach( $locations_paintings as $paintid )
            {
                if( $i_loc==0 ) $or_loc="";
                else $or_loc=" OR ";
                $query_where_paintings.=$or_loc." paintID='".$paintid."' ";
                $i_loc++;
            }
            $query_where_paintings.=" ) ";
        }
        elseif( ( $count_locations || $count_locations_city || $count_locations_country ) && empty($locations_paintings) )
        {
            $query_where_paintings.=" AND paintID=-1 ";
        }
        elseif ( !$count_locations && !$count_locations_city && !$count_locations_country && !empty($options['get']['location']) )
        {
            $query_where_paintings.=" AND paintID=-1 ";
        }
        /*
        elseif( !empty($options['get']['location']) || !empty($options['get']['keyword']) )
        {
            //if( empty($options['get']['keyword']) ) $query_where_paintings.=" AND ";
            //$query_where_paintings.=" AND paintID=-1 ";
        }
        */



    }
    # END Location search

    # prepare search keyword
    $options_search=array();
    //$options_search['painting_search']=1;
    $options_search['search_art']=1;
    $options_search['db']=$db;
    if( !empty($_GET['search_main']) ) $title=$_GET['search_main'];
    $title_search=UTILS::prepear_search($title,$options_search);
    # END prepare search keyword

    # prepare match
    $options_search_paintings=array();
    $options_search_paintings['search']=$title_search;
    if( !empty($_GET['search_main']) ) $options_search_paintings['global_search']=1;
    $query_match=QUERIES_SEARCH::query_search_paintings($db,$options_search_paintings);
    # END prepare match

    /*
    if( !empty($locations_paintings) ) $and=" AND ";
    else $and=" AND ";
    */

    if( !empty($title) )
    {
        $and=" AND ";
        $query_where_paintings.=$and." ".$query_match." ";
        if( !empty($locations_paintings) ) $query_where_paintings.=" ) "; # bracket for ( location OR match )
    }

    # NUMBER
    if( !empty($options['get']['number']) || !empty($options['get']['number-from']) || !empty($options['get']['number-to']) || !empty($options['get']['number_list']) )
    {
        $query_where_paintings.=" AND ".$number_search." ";
    }
    # END NUMBER

    # YEAR
    if( !empty($yearfrom) && !empty($yearto) )
    {
        $query_where_paintings.=" AND p.year_search BETWEEN '".$yearfrom."' AND '".$yearto."' ";
    }
    elseif( !empty($yearfrom) && empty($yearto) )
    {
        $query_where_paintings.=" AND p.year_search BETWEEN '".$yearfrom."' AND ".date('Y')." ";
    }
    elseif( empty($yearfrom) && !empty($yearto) )
    {
        $query_where_paintings.=" AND p.year_search BETWEEN 1962 AND '".$yearto."' ";
    }
    # END YEAR

    # DATE
    if( $options['get']['artworkid']==5 || $options['get']['artworkid']==6 || $options['get']['artworkid']==8 )
    {
        $date_day_from=intval($options['get']['date-day-from']);
        $date_month_from=intval($options['get']['date-month-from']);
        $date_day_to=intval($options['get']['date-day-to']);
        $date_month_to=intval($options['get']['date-month-to']);

        if( empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
        {
            $date_day_from="00";
        }
        elseif( empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
        {
            $date_day_from="00"; $date_month_from="00";
        }

        if( empty($options['get']['date-day-to']) && !empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
        {
            $date_day_to="31";
        }
        elseif( empty($options['get']['date-day-to']) && empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
        {
            $date_day_to="31"; $date_month_to="12";
        }

        # if seraching by 2 digits year
        if( strlen($options['get']['date-year-from'])==2 )
        {
            $dt = DateTime::createFromFormat('y', $options['get']['date-year-from']);
            $options['get']['date-year-from']=$dt->format('Y');
        }
        if( strlen($options['get']['date-year-to'])==2 )
        {
            $dt = DateTime::createFromFormat('y', $options['get']['date-year-to']);
            $options['get']['date-year-to']=$dt->format('Y');
        }
        # END if seraching by 2 digits year

        if( ( !empty($options['get']['date-day-from']) || !empty($options['get']['date-month-from']) || !empty($options['get']['date-year-from']) ) && ( empty($options['get']['date-day-to']) && empty($options['get']['date-month-to']) && empty($options['get']['date-year-to']) ) )
        {
            if( !empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%c-%e')='".$options['get']['date-year-from']."-".$date_month_from."-".$date_day_from."' ";
            }
            elseif( empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%c-%e')='".$options['get']['date-year-from']."-".$date_month_from."-1' ";
            }
            elseif( !empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%e')='".$options['get']['date-year-from']."-".$date_day_from."' ";
            }
            elseif( !empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c-%e')='".$date_month_from."-".$date_day_from."' ";
            }
            elseif( !empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%e')='".$date_day_from."' ";
            }
            elseif( empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c')='".$date_month_from."' ";
            }
            elseif( empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                //$query_where_paintings.=" AND ( DATE_FORMAT(p.date, '%Y')='".$options['get']['date-year-from']."' OR ";
                //$query_where_paintings.=" p.year='".$options['get']['date-year-from']."' ) ";
                $query_where_paintings.=" AND ( DATE_FORMAT(p.date, '%Y')>='".$options['get']['date-year-from']."' OR ";
                $query_where_paintings.=" p.year>='".$options['get']['date-year-from']."' ) ";
            }
        }
        if( ( !empty($options['get']['date-day-from']) || !empty($options['get']['date-month-from']) || !empty($options['get']['date-year-from']) ) && ( !empty($options['get']['date-day-to']) || !empty($options['get']['date-month-to']) || !empty($options['get']['date-year-to']) ) )
        {
            if( !empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND p.date BETWEEN '".$options['get']['date-year-from']."-".$date_month_from."-".$date_day_from." 00:00:00' AND '".$options['get']['date-year-to']."-".$date_month_to."-".$date_day_to." 23:59:59' ";
            }
            elseif( empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                //$query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%c-%e') BETWEEN '".$options['get']['date-year-from']."-".$date_month_from."-1' AND '".$options['get']['date-year-to']."-".$date_month_to."-31' ";
                $query_where_paintings.=" AND p.date BETWEEN '".$options['get']['date-year-from']."-".$date_month_from."-01 00:00:00' AND '".$options['get']['date-year-to']."-".$date_month_to."-31 23:59:59' ";
            }
            elseif( !empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%e') BETWEEN '".$options['get']['date-year-from']."-".$date_day_from."' AND '".$options['get']['date-year-to']."-".$date_day_to."' ";
            }
            elseif( !empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c-%e') BETWEEN '".$date_month_from."-".$date_day_from."' AND '".$date_month_to."-".$date_day_to."' ";
            }
            elseif( !empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%e') BETWEEN '".$date_day_from."' AND '".$date_day_to."' ";
            }
            elseif( empty($options['get']['date-day-from']) && !empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c') BETWEEN '".$date_month_from."' AND '".$date_month_to."' ";
            }
            elseif( empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && !empty($options['get']['date-year-from']) )
            {
                $query_where_paintings.=" AND ( p.date BETWEEN '".$options['get']['date-year-from']."-00-00 00:00:00' AND '".$options['get']['date-year-to']."-12-31 23:59:59' OR ";
                $query_where_paintings.=" p.year_search BETWEEN '".$options['get']['date-year-from']."' AND '".$options['get']['date-year-to']."' ) ";
            }
        }
        if( ( !empty($options['get']['date-day-to']) || !empty($options['get']['date-month-to']) || !empty($options['get']['date-year-to']) ) && ( empty($options['get']['date-day-from']) && empty($options['get']['date-month-from']) && empty($options['get']['date-year-from']) ) )
        {
            if( !empty($options['get']['date-day-to']) && !empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%c-%e')='".$options['get']['date-year-to']."-".$date_month_to."-".$date_day_to."' ";
            }
            elseif( empty($options['get']['date-day-to']) && !empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%c-%e')='".$options['get']['date-year-to']."-".$date_month_to."-31' ";
            }
            elseif( !empty($options['get']['date-day-to']) && empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%Y-%e')='".$options['get']['date-year-to']."-".$date_day_to."' ";
            }
            elseif( !empty($options['get']['date-day-to']) && !empty($options['get']['date-month-to']) && empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c-%e')='".$date_month_to."-".$date_day_to."' ";
            }
            elseif( !empty($options['get']['date-day-to']) && empty($options['get']['date-month-to']) && empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%e')='".$date_day_to."' ";
            }
            elseif( empty($options['get']['date-day-to']) && !empty($options['get']['date-month-to']) && empty($options['get']['date-year-to']) )
            {
                $query_where_paintings.=" AND DATE_FORMAT(p.date, '%c')='".$date_month_to."' ";
            }
            elseif( empty($options['get']['date-day-to']) && empty($options['get']['date-month-to']) && !empty($options['get']['date-year-to']) )
            {
                //$query_where_paintings.=" AND ( DATE_FORMAT(p.date, '%Y')='".$options['get']['date-year-to']."' OR ";
                //$query_where_paintings.=" p.year='".$options['get']['date-year-to']."' ) ";
                $query_where_paintings.=" AND ( DATE_FORMAT(p.date, '%Y')<='".$options['get']['date-year-to']."' OR ";
                $query_where_paintings.=" p.year<='".$options['get']['date-year-to']."' ) ";
            }
        }
    }
    # END DATE

    # color
    $results_related_color=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=1 AND typeid2=23 AND itemid2='".$options['get']['colorid']."' ) OR ( typeid2=1 AND typeid1=23 AND itemid1='".$options['get']['colorid']."' ) ORDER BY sort ASC, relationid DESC");
    $count_related_color=$db->numrows($results_related_color);
    if( $count_related_color>0 )
    {
        $query_where_paintings.=" AND ( ";
        $i=0;
        while( $row_related_color=$db->mysql_array($results_related_color) )
        {
            $i++;
            $paintid=UTILS::get_relation_id($db,"1",$row_related_color);
            $query_where_paintings.=" p.paintid='".$paintid."' ";
            if( $count_related_color>1 && $count_related_color!=$i ) $query_where_paintings.=" OR ";
        }
        $query_where_paintings.=" ) ";
    }
    # END color


    # size
    if( !empty($sizeheight_min) && !empty($sizeheight_max) ) $query_where_paintings.=" AND  p.height BETWEEN '".$sizeheight_min."' AND '".$sizeheight_max."' ";
    elseif( !empty($sizeheight_min) && empty($sizeheight_max) ) $query_where_paintings.=" AND p.height='".$sizeheight_min."' ";
    elseif( empty($sizeheight_min) && !empty($sizeheight_max) ) $query_where_paintings.=" AND p.height='".$sizeheight_max."' ";

    if( !empty($sizewidth_min) && !empty($sizewidth_max) ) $query_where_paintings.=" AND p.width BETWEEN '".$sizewidth_min."' AND '".$sizewidth_max."' ";
    elseif( !empty($sizewidth_min) && empty($sizewidth_max) ) $query_where_paintings.=" AND p.width='".$sizewidth_min."' ";
    elseif( empty($sizewidth_min) && !empty($sizewidth_max) ) $query_where_paintings.=" AND p.width='".$sizewidth_max."' ";
    #END size




    ###counting total paintings
    $options_query_paintings=array();
    $options_query_paintings['column']=$options['column'];
    if( $options['artworkid']==8 )
    {
        $options_query_paintings['sort_column']="IF(`sort3`>0 AND artworkID=5 AND artworkID2=8,`sort3`,`sort2`) AS sort2";
    }
    elseif( $options['artworkid']==5 )
    {
        $options_query_paintings['sort_column']="IF(`sort3`>0 AND artworkID=8 AND artworkID2=5,`sort3`,`sort2`) AS sort2";
    }
    if( ( $options['artworkid']==8 || $options['artworkid']==5 ) && !empty($options['column']) ) $options_query_paintings['column'].=",".$options_query_paintings['sort_column'];
    //$query_order=" ORDER BY sort2 ASC, p.titleEN ASC ";
    $query_order=" ORDER BY sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
    if( $options['sp']!='all' ) $query_limit=" LIMIT ".($options['sp']*($options['p']-1)).",".$options['sp'];
    $query_paintings=QUERIES::query_painting($db,$query_where_paintings,$query_order,$query_limit,$query_from_paintings,$options_query_paintings);
    if( $_SESSION['debug_page'] ) print "<br /><br />".$query_paintings['query_without_limit'];
    //if( $_SESSION['debug_page'] ) print "<br /><br />".$query_where_paintings;
    $results_total=$db->query($query_paintings['query_without_limit']);
	$count=$db->numrows($results_total);
	$pages=@ceil($count/$options['sp']);
    //print "<br />count=".$count;
	#end

	$results=$db->query($query_paintings['query']);

    $return=array();
    $return['count']=$count;
    $return['pages']=$pages;
    $return['results']=$results;
    $return['results_total']=$results_total;
    $return['query']=$query_paintings['query'];

    return $return;
}
?>
