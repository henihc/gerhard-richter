<?php
class QUERIES
{
    public static function query_artworks($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            artworkID,
            artworkEN,
            artworkDE,
            artworkFR,
            artworkIT,
            artworkZH,
            desc_en,
            desc_de,
            desc_fr,
            desc_it,
            desc_zh,
            artwork_dir_name,
            artwork_dir_name_admin,
            sort
        FROM ".TABLE_ARTWORKS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_exhibition($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="";
        if( !empty($values['columns_distinct']) ) $query_columns.=$values['columns_distinct'];
        $query_columns.="
            e.exID,
            e.relations,
            e.newsid,
            e.exID AS itemid2,
            e.title_original,
            e.titleEN,
            e.titleDE,
            e.titleFR,
            e.titleIT,
            e.titleZH,
            e.titleurl,
            e.image_enable,
            e.description_enable,
            e.descriptionEN,
            e.descriptionDE,
            e.descriptionFR,
            e.descriptionIT,
            e.descriptionZH,
            e.typeid,
            e.notes,
            e.locationid,
            e.cityid,
            e.countryid,
            e.src_guide,
            e.guide_desc,
            e.date_from,
            e.date_to,
            DATE_FORMAT(e.date_from, '%d %M %Y') as startDate3_en,
            DATE_FORMAT(e.date_from, '%d. %M %Y') as startDate3_de,
            DATE_FORMAT(e.date_from, '%d %M %Y') as startDate3_fr,
            DATE_FORMAT(e.date_from, '%d %M %Y') as startDate3_it,
            DATE_FORMAT(e.date_from, '%d %M %Y') as startDate3_zh,
            DATE_FORMAT(e.date_to, '%d %M %Y') as endDate3_en,
            DATE_FORMAT(e.date_to, '%d. %M %Y') as endDate3_de,
            DATE_FORMAT(e.date_to, '%d %M %Y') as endDate3_fr,
            DATE_FORMAT(e.date_to, '%d %M %Y') as endDate3_it,
            DATE_FORMAT(e.date_to, '%d %M %Y') as endDate3_zh,
            DATE_FORMAT(e.date_from, '%Y') as startDate4,
            DATE_FORMAT(e.date_from, '%Y') as date_year,
            DATE_FORMAT(e.date_to, '%Y') as end_year,
            DATE_FORMAT(e.date_from, '%M %Y') as start_month,
            DATE_FORMAT(e.date_to, '%M %Y') as end_month,
            DATE_FORMAT(e.date_from ,'%d') AS start_day_admin,
            DATE_FORMAT(e.date_from ,'%m') AS start_month_admin,
            DATE_FORMAT(e.date_from ,'%Y') AS start_year_admin,
            DATE_FORMAT(e.date_from ,'%Y-%m-%d') AS start_date_admin,
            DATE_FORMAT(e.date_to ,'%Y-%m-%d') AS end_date_admin,
            e.showmonth_start,
            e.showyear_start,
            e.showmonth_end,
            e.showyear_end,
            e.tour_button_url,
            e.enable,
            DATE_FORMAT(e.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(e.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_EXHIBITIONS." e ";

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_painting($db,$where="",$order="",$limit="",$tables="",$values=array())
    {
        $query_columns="p.paintID,
            p.paintID AS itemid2,
            p.imageid,
            p.artworkID,
            p.artworkID2,
            p.editionID,
            p.edition_individual,
            p.titleEN,
            p.titleDE,
            p.titleFR,
            p.titleIT,
            p.titleZH,
            p.titleurl,
            p.year,
            p.date,
            DATE_FORMAT(date ,'%Y-%m-%d') AS date_admin,
            p.number,
            p.number_old,
            p.number_search,
            p.ednumber,
            p.ednumberroman,
            p.ednumberap,
            p.citylifepage,
            p.nr_of_editions,
            p.catID,
            p.size,
            p.size_parts,
            p.size_typeid,
            p.height,
            p.width,
            p.src,
            p.mID,
            p.muID,
            p.locationid,
            p.cityid,
            p.countryid,
            p.optionid_loan_type,
            p.loan_locationid,
            p.loan_cityid,
            p.loan_countryid,
            p.optionid_photograph_courtesy,
            p.optionid_photograph_courtesy_values,
            p.collection_notes_en,
            p.collection_notes_de,
            p.collection_notes_fr,
            p.collection_notes_it,
            p.collection_notes_zh,
            p.sort,";

        if( !empty($values['sort_column']) )
        {
            $query_columns.=$values['sort_column'].",";
        }
        else $query_columns.="p.sort2,";

        $query_columns.="p.sort3,
            p.artwork_notesEN,
            p.artwork_notesDE,
            p.artwork_notesFR,
            p.artwork_notesIT,
            p.artwork_notesZH,
            p.notes,
            p.image_zoomer,
            p.created,
            p.modified,
            DATE_FORMAT(p.modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(p.created ,'%d %b %Y %H:%i:%s') AS date_created_admin,
            p.enable,
            p.keywords,
            p.destroyed ";

        if( !empty($values['column']) ) $query_columns=$values['column'];
        elseif( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_PAINTING." p ";
        if( !empty($tables) ) $query_full.=$tables;

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_category($db,$where="",$order="",$limit="",$tables="",$values=array())
    {
        $query_columns="c.catID,
            c.sub_catID,
            c.src,
            c.artworkID,
            c.categoryEN,
            c.categoryDE,
            c.categoryFR,
            c.categoryIT,
            c.categoryZH,
            c.titleurl,
            c.descriptionEN,
            c.descriptionDE,
            c.descriptionFR,
            c.descriptionIT,
            c.descriptionZH,
            c.type,
            c.enable";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_CATEGORY." c ";
        if( !empty($tables) ) $query_full.=$tables;

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }


    public static function query_locations($db,$where="",$order="",$limit="",$values=array())
    {
        $query="SELECT
            lo.locationid,
            lo.countryid,
            lo.cityid,
            lo.location_en,
            lo.location_de,
            lo.location_fr,
            lo.location_it,
            lo.location_zh,
            DATE_FORMAT(lo.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(lo.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin ";
        if( !empty($values['columns']) ) $query.=$values['columns'];
        $query.=" FROM ".TABLE_LOCATIONS." lo";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_locations_city($db,$where="",$order="",$limit="",$values=array())
    {
        $query="SELECT
            ci.cityid,
            ci.countryid,
            ci.city_en,
            ci.city_de,
            ci.city_fr,
            ci.city_it,
            ci.city_zh,
            DATE_FORMAT(ci.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(ci.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin ";
        if( !empty($values['columns']) ) $query.=$values['columns'];
        $query.=" FROM ".TABLE_LOCATIONS_CITY." ci";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_locations_country($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            co.countryid,
            co.country_en,
            co.country_de,
            co.country_fr,
            co.country_it,
            co.country_zh,
            DATE_FORMAT(co.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(co.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_LOCATIONS_COUNTRY." co";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }





    public static function query_news($db,$where="",$order="",$limit="")
    {
        $query="SELECT
                n.newsID,
                n.type,
                DATE_FORMAT(n.datefrom ,'%M %d %Y') as ndatefrom,
                DATE_FORMAT(n.dateto ,'%M %d %Y') as ndateto,
                DATE_FORMAT(n.datefrom, '%d %M %Y') as ndatefrom_en,
                DATE_FORMAT(n.datefrom, '%d. %M %Y') as ndatefrom_de,
                DATE_FORMAT(n.datefrom, '%d %M %Y') as ndatefrom_fr,
                DATE_FORMAT(n.datefrom, '%d %M %Y') as ndatefrom_it,
                DATE_FORMAT(n.dateto, '%d %M %Y') as ndateto_en,
                DATE_FORMAT(n.dateto, '%d. %M %Y') as ndateto_de,
                DATE_FORMAT(n.dateto, '%d %M %Y') as ndateto_fr,
                DATE_FORMAT(n.dateto, '%d %M %Y') as ndateto_it,
                DATE_FORMAT(n.datefrom ,'%Y-%m-%d') as datefrom_admin,
                DATE_FORMAT(n.dateto ,'%Y-%m-%d') as dateto_admin,
                DATE_FORMAT(n.datefrom ,'%d %b %Y') as datefrom_list_admin,
                DATE_FORMAT(n.dateto ,'%d %b %Y') as dateto_list_admin,
                n.showdate,
                n.time,
                n.title_original,
                n.titleEN,
                n.titleDE,
                n.titleFR,
                n.titleIT,
                n.titleZH,
                n.person,
                n.locationEN,
                n.locationDE,
                n.locationFR,
                n.locationIT,
                n.locationZH,
                n.locationid,
                n.cityid,
                n.countryid,
                n.textEN,
                n.textDE,
                n.textFR,
                n.textIT,
                n.textZH,
                n.src,
                n.leaflet
        FROM ".TABLE_NEWS." n ";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_relations($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            relationid,
            typeid1,
            itemid1,
            typeid2,
            itemid2,
            sort,
            sort1,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin,
            DATE_FORMAT(date_created ,'%d %b %Y <br /> %H:%i') AS date_created_admin_table
        FROM ".TABLE_RELATIONS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_relations_literature($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            relationid_lit,
            relationid,
            illustrated,
            illustrated_bw,
            illustrated_typeid,
            show_illustrated,
            show_bw,
            mentioned,
            show_mentioned,
            discussed,
            show_discussed,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_RELATIONS_LITERATURE;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_relations_painting_colors($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            relationid_paint_color,
            relationid,
            percentage,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_RELATIONS_PAINTING_COLORS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_sale_history($db,$where="",$order="",$limit="",$tables="",$values=array())
    {
        $query_columns="
            s.saleID,
            s.paintID,
            s.ahID,
            DATE_FORMAT(s.saleDate ,'%d %M %Y') AS saleDate2,
            DATE_FORMAT(s.saleDate ,'%d %M %Y') AS saleDate_en,
            DATE_FORMAT(s.saleDate ,'%d. %M %Y') AS saleDate_de,
            DATE_FORMAT(s.saleDate ,'%d %M %Y') AS saleDate_fr,
            DATE_FORMAT(s.saleDate ,'%d %M %Y') AS saleDate_it,
            DATE_FORMAT(s.saleDate ,'%d %M %Y') AS saleDate_zh,
            DATE_FORMAT(s.saleDate ,'%d %b %Y') AS sale_date_short_en,
            DATE_FORMAT(s.saleDate ,'%d. %b %Y') AS sale_date_short_de,
            DATE_FORMAT(s.saleDate ,'%d %b %Y') AS sale_date_short_fr,
            DATE_FORMAT(s.saleDate ,'%d %b %Y') AS sale_date_short_it,
            DATE_FORMAT(s.saleDate ,'%d %b %Y') AS sale_date_short_zh,
            DATE_FORMAT(s.saleDate ,'%Y-%m-%d') AS sale_date_admin,
            DATE_FORMAT(s.saleDate ,'%d %b %Y') AS sale_date,
            s.lotNo,
            s.upon_request,
            s.tbannounced,
            s.estLow,
            s.estHigh,
            s.estCurrID,
            s.estLowUSD,
            s.estHighUSD,
            s.soldFor,
            s.soldForCurrID,
            s.soldForUSD,
            s.boughtIn,
            s.withdrawn,
            s.notes_admin,
            s.notes_auction,
            s.saleName,
            s.sale_number,
            s.saleType,
            s.number,
            s.news_auction,
            s.notes_en,
            s.notes_de,
            s.notes_fr,
            s.notes_it,
            s.notes_zh,
            s.news_auction_text_en,
            s.news_auction_text_de,
            s.news_auction_text_fr,
            s.news_auction_text_it,
            s.news_auction_text_zh,
            DATE_FORMAT(s.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(s.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin,
            s.enable ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_SALEHISTORY." s ";
        if( !empty($tables) ) $query_full.=$tables;

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }


    public static function query_videos($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="
            v.videoID,
            v.categoryid,
            v.paintid,
            v.titleEN,
            v.titleDE,
            v.titleFR,
            v.titleIT,
            v.titleZH,
            v.titlelong_en,
            v.titlelong_de,
            v.titlelong_fr,
            v.titlelong_it,
            v.titlelong_zh,
            v.titleurl,
            v.name,
            v.locationid,
            v.cityid,
            v.countryid,
            v.location_en,
            v.location_de,
            v.location_fr,
            v.location_it,
            v.location_zh,
            v.year,
            v.timeEN,
            v.timeDE,
            v.timeFR,
            v.timeIT,
            v.timeZH,
            v.image,
            v.descriptionEN,
            v.descriptionDE,
            v.descriptionFR,
            v.descriptionIT,
            v.descriptionZH,
            v.info_en,
            v.info_de,
            v.info_fr,
            v.info_it,
            v.info_zh,
            v.file,
            v.file_qt,
            v.enable,
            v.height,
            v.width,
            v.watermark,
            v.sort,
            v.date ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_VIDEO." v ";

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_videos_categories($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            categoryid,
            sub_categoryid,
            title_en,
            titleurl,
            title_de,
            title_fr,
            title_it,
            title_zh,
            desc_en,
            desc_de,
            desc_fr,
            desc_it,
            desc_zh,
            src,
            sort_en,
            enable,
            notes_admin,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_VIDEO_CATEGORIES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_related_videos($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            related_videoid,
            text_en,
            text_de,
            text_fr,
            text_it,
            text_zh,
            paintid,
            videoid,
            sort,
            DATE_FORMAT(date_modify ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_RELATED_VIDEOS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_audios($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            audioid,
            tracktitleen,
            tracktitlede,
            tracktitlefr,
            tracktitleit,
            tracktitlezh,
            src_audio,
            src_img,
            titleen,
            titleen AS title_en,
            titlede,
            titlede AS title_de,
            titlefr,
            titlefr AS title_fr,
            titleit,
            titleit AS title_it,
            titlezh,
            titlezh AS title_zh,
            year,
            width,
            height,
            artworken,
            artworken AS artwork_en,
            artworkde,
            artworkde AS artwork_de,
            artworkfr,
            artworkfr AS artwork_fr,
            artworkit,
            artworkit AS artwork_it,
            artworkzh,
            artworkzh AS artwork_zh,
            number,
            descriptionen,
            descriptionen AS description_en,
            descriptionde,
            descriptionde AS description_de,
            descriptionfr,
            descriptionfr AS description_fr,
            descriptionit,
            descriptionit AS description_it,
            descriptionzh,
            descriptionzh AS description_zh,
            enable,
            date,
            sort,
            DATE_FORMAT(date ,'%d %b %Y') as date_created_list_admin
        FROM ".TABLE_AUDIO;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_microsites($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="
            m.micrositeid,
            m.title_en,
            m.title_de,
            m.title_fr,
            m.title_it,
            m.title_zh,
            m.titleurl,
            m.desc_small_en,
            m.desc_small_de,
            m.desc_small_fr,
            m.desc_small_it,
            m.desc_small_zh,
            m.text_en,
            m.text_de,
            m.text_fr,
            m.text_it,
            m.text_zh,
            m.sort,
            m.enable,
            DATE_FORMAT(m.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(m.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin
            ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_MICROSITES." m ";
        //if( !empty($tables) ) $query_full.=$tables;

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_microsites_versions($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            versionid,
            micrositeid,
            number,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            text_en,
            text_de,
            text_fr,
            text_it,
            text_zh,
            sort,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2
        FROM ".TABLE_MICROSITES_VERSIONS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_photos($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            photoID,
            artworkID,
            title,
            description,
            year,
            src,
            sort,
            notes,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin
        FROM ".TABLE_PHOTOS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_biography($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            bi.biographyid,
            bi.title_en,
            bi.title_left_en,
            bi.titleurl,
            bi.title_de,
            bi.title_left_de,
            bi.title_fr,
            bi.title_left_fr,
            bi.title_it,
            bi.title_left_it,
            bi.title_zh,
            bi.title_left_zh,
            bi.text_en,
            bi.text_de,
            bi.text_fr,
            bi.text_it,
            bi.text_zh,
            bi.notes_admin,
            bi.enable,
            bi.sort,
            DATE_FORMAT(bi.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(bi.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_BIOGRAPHY." bi ";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }


    public static function query_articles_categories($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            articles_catid,
            title_en,
            titleurl,
            title_de,
            title_fr,
            title_it,
            title_zh,
            sort,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_ARTICLES_CATEGORIES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_quotes($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            q.quoteid,
            q.quotes_categoryid,
            q.year,
            q.text_en,
            q.text_de,
            q.text_fr,
            q.text_it,
            q.text_zh,
            q.source_en,
            q.source_de,
            q.source_fr,
            q.source_it,
            q.source_zh,
            q.source_text_en,
            q.source_text_de,
            q.source_text_fr,
            q.source_text_it,
            q.source_text_zh,
            q.sort,
            DATE_FORMAT(q.date_modify, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(q.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_QUOTES." q ";

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_quotes_categories($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            quotes_categoryid,
            sub_categoryid,
            title_en,
            title_short_en,
            titleurl,
            title_de,
            title_short_de,
            title_fr,
            title_short_fr,
            title_it,
            title_short_it,
            title_zh,
            title_short_zh,
            desc_en,
            desc_de,
            desc_fr,
            desc_it,
            desc_zh,
            src,
            enable,
            notes,
            date_modified,
            date_created,
            DATE_FORMAT(date_modified ,'%d %b %Y %H:%i:%s') AS date_modified2,
            DATE_FORMAT(date_created ,'%d %b %Y %H:%i:%s') AS date_created2
        FROM ".TABLE_QUOTES_CATEGORIES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_painting_notes($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="
            pn.noteid,
            pn.src,
            pn.noteEN,
            pn.noteDE,
            pn.noteFR,
            pn.noteIT,
            pn.noteZH,
            pn.display,
            DATE_FORMAT(pn.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(pn.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_PAINTING_NOTE." pn ";

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_museums($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="
            m.muID,
            m.museum,
            m.url,
            m.locationid,
            m.cityid,
            m.countryid,
            m.optionid_loan_type,
            m.loan_locationid,
            m.loan_cityid,
            m.loan_countryid,
            m.collection_desc_en,
            DATE_FORMAT(m.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(m.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin,
            notes ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_MUSEUM." m ";

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_painting_colors($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            colorid,
            color_code,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin,
            sort
        FROM ".TABLE_PAINTING_COLORS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_painting_size_types($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            size_typeid,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin,
            sort
        FROM ".TABLE_PAINTING_SIZE_TYPES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_select_dropdowns($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            selectid,
            name,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_SELECT_DROPDOWNS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_select_dropdowns_options($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            optionid,
            selectid,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin,
            sort
        FROM ".TABLE_SELECT_DROPDOWNS_OPTIONS;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_painting_images($db,$where="",$order="",$limit="",$values=array())
    {
        $query_columns="
            pp.painting_photoid,
            pp.typeid,
            pp.title_en,
            pp.title_de,
            pp.title_fr,
            pp.title_it,
            pp.title_zh,
            pp.src_old,
            pp.src,
            pp.notes_admin,
            pp.enable,
            pp.sort,
            DATE_FORMAT(pp.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(pp.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin ";

        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_PAINTING_PHOTOS." pp ";

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }

    public static function query_exhibitions_installations($db,$where="",$order="",$limit="",$tables="",$values=array())
    {
        $query_columns="
            installationid,
            installationid AS itemid2,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            titleurl,
            notes_admin,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin";
        if( !empty($values['columns']) ) $query_columns.=$values['columns'];

        $query="SELECT ".$query_columns." FROM ".TABLE_EXHIBITIONS_INSTALLATIONS;
        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_exhibitions_installations_images($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            imageid,
            installationid,
            src,
            src_old,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            notes_en,
            notes_de,
            notes_fr,
            notes_it,
            notes_zh,
            sort,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_EXHIBITIONS_INSTALLATIONS_IMAGES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_images($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            imageid,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            notes_en,
            notes_de,
            notes_fr,
            notes_it,
            notes_zh,
            src_old,
            src,
            notes_admin,
            enable,
            sort,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_IMAGES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_books_languages($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            languageid,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            notes_admin,
            enable,
            sort_en,
            sort_de,
            sort_fr,
            sort_it,
            sort_zh,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_BOOKS_LANGUAGES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_books_categories($db,$where="",$order="",$limit="")
    {
        $query="SELECT
            books_catid,
            sub_books_catid,
            title_en,
            title_de,
            title_fr,
            title_it,
            title_zh,
            titleurl,
            title_search_1,
            title_search_2,
            title_search_3,
            title_search_4,
            sort,
            enable,
            DATE_FORMAT(date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(date_created, '%d %b %Y %H:%i:%s') AS date_created_admin
        FROM ".TABLE_BOOKS_CATEGORIES;

        $sql['query']=$query." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query." ".$where." ".$order;

        return $sql;
    }

    public static function query_books($db,$where="",$order="",$limit="",$tables="",$values=array())
    {
        $query_columns="";
        if( $values['distinct_id'] ) $query_columns.="DISTINCT(b.id) AS id,";
        else $query_columns.="b.id,";
        $query_columns.="
            b.id AS itemid2,
            b.newsid,
            b.books_catid,
            b.articles_catid,
            b.enable,
            b.not_show_recently,
            b.type_ephemera,
            b.published_ephemera,
            b.occasion_ephemera,
            b.url,
            b.title,
            b.title_display,
            b.title_periodical,
            b.titleurl,
            b.author,
            b.editor,
            b.author_essay,
            b.author_essay_title,
            IF(b.author IS NULL OR b.author='', 1, 0) AS isnull,
            b.publisher,
            b.year,
            b.link_date,
            DATE_FORMAT(b.link_date, '%d %M %Y') AS date_display,
            DATE_FORMAT(b.link_date, '%d %M %Y') AS date_display_en,
            DATE_FORMAT(b.link_date, '%d. %M %Y') AS date_display_de,
            DATE_FORMAT(b.link_date, '%M, %Y') AS date_display_month_en,
            DATE_FORMAT(b.link_date, '%M %Y') AS date_display_month_de,
            DATE_FORMAT(b.link_date, '%Y') AS date_display_year,
            DATE_FORMAT(b.link_date, '%Y') AS date_year,
            DATE_FORMAT(b.link_date, '%Y-%m-%d') AS link_date_admin,
            b.link_date_month,
            b.link_date_year,
            b.link_date_year_full,
            b.vol_no,
            b.issue_no,
            b.link,
            b.link_broken,
            b.link_validated,
            b.language,
            b.isbn,
            b.galleryID,
            b.exID,
            b.coverid,
            b.NOpages,
            b.article_pages_from,
            b.article_pages_till,
            b.selectedTitle,
            b.notesEN,
            b.notesDE,
            b.notesFR,
            b.notesIT,
            b.notesZH,
            b.infoEN,
            b.infoDE,
            b.infoFR,
            b.infoIT,
            b.infoZH,
            b.infosmall_en,
            b.infosmall_de,
            b.infosmall_fr,
            b.infosmall_it,
            b.infosmall_zh,
            b.imageID,
            b.keywords,
            b.notes_admin,
            b.sort,
            b.sort_publications,
            DATE_FORMAT(b.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(b.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin ";
        if( !empty($values['columns']) ) $query_columns.=$values['columns'];
        $query_full=" SELECT ".$query_columns." FROM ".TABLE_BOOKS." b ";
        if( !empty($tables) ) $query_full.=$tables;

        $sql['query']=$query_full." ".$where." ".$order." ".$limit;
        $sql['query_without_limit']=$query_full." ".$where." ".$order;
        $sql['query_columns']=$query_columns;

        return $sql;
    }


    public static function query_text($db,$values=array())
    {
        $query_full="SELECT ";
        $query_rows="
            t.textid,
            t.title_en,
            t.title_de,
            t.title_fr,
            t.title_it,
            t.title_zh,
            t.text_en,
            t.text_de,
            t.text_fr,
            t.text_it,
            t.text_zh,
            t.notes_admin,
            DATE_FORMAT(t.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(t.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin ";
        $query_rows.=$values['query_rows'];
        $query_full.=$query_rows;
        $query_full.="FROM ".TABLE_TEXT." t";

        $sql['query_rows']=$query_rows;
        $sql['query_without_limit']=$query_full." ".$values['where']." ".$values['order'];
        $sql['query']=$sql['query_without_limit']." ".$values['limit'];

        return $sql;
    }

    public static function query_search_test_keywords($db,$values=array())
    {
        $query_full="SELECT ";
        $query_rows="
            sk.keywordid,
            sk.categoryid,
            sk.artworkid,
            sk.keywords,
            sk.valid,
            sk.count_valid,
            sk.ids_valid,
            sk.valid_test,
            sk.count_test,
            sk.ids_test,
            sk.count_current,
            sk.ids_current,
            DATE_FORMAT(sk.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin_sk,
            DATE_FORMAT(sk.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin_sk ";
        $query_rows.=$values['query_rows'];
        $query_full.=$query_rows;
        $query_full.="FROM ".TABLE_SEARCH_TEST_KEYWORDS." sk";

        $sql['query_rows']=$query_rows;
        $sql['query_without_limit']=$query_full." ".$values['where']." ".$values['order'];
        $sql['query']=$sql['query_without_limit']." ".$values['limit'];

        return $sql;
    }

    public static function query_search_test_categories($db,$values=array())
    {
        $query_full="SELECT ";
        $query_rows="
            sc.categoryid,
            sc.title,
            sc.sort,
            DATE_FORMAT(sc.date_modified, '%d %b %Y %H:%i:%s') AS date_modified_admin_sc,
            DATE_FORMAT(sc.date_created, '%d %b %Y %H:%i:%s') AS date_created_admin_sc ";
        $query_rows.=$values['query_rows'];
        $query_full.=$query_rows;
        $query_full.="FROM ".TABLE_SEARCH_TEST_CATEGORIES." sc";

        $sql['query_rows']=$query_rows;
        $sql['query_without_limit']=$query_full." ".$values['where']." ".$values['order'];
        $sql['query']=$sql['query_without_limit']." ".$values['limit'];

        return $sql;
    }

    public static function query_auction_houses($db,$values=array())
    {
        $query_full="SELECT ";
        $query_rows="
            ah.ahID,
            ah.house,
            ah.cityid,
            ah.countryid,
            ah.cityid AS ah_cityid,
            ah.countryid AS ah_countryid ";
        $query_rows.=$values['query_rows'];
        $query_full.=$query_rows;
        $query_full.="FROM ".TABLE_AUCTIONHOUSE." ah";

        $sql['query_rows']=$query_rows;
        $sql['query_columns']=$query_rows;
        $sql['query_without_limit']=$query_full." ".$values['where']." ".$values['order'];
        $sql['query']=$sql['query_without_limit']." ".$values['limit'];

        return $sql;
    }

    public static function query_painting_opp($db,$values=array())
    {
        $query_full="SELECT ";
        $query_rows="
            po.oppinfoid,
            po.paintid,
            po.verified_verso,
            po.optionid_verso_signed_1_1,
            po.optionid_verso_signed_1_2,
            po.verso_signed_1_3,
            po.optionid_verso_signed_2_1,
            po.optionid_verso_signed_2_2,
            po.verso_signed_2_3,
            po.optionid_verso_dated_1_1,
            po.optionid_verso_dated_1_2,
            po.verso_dated_1_3,
            po.optionid_verso_dated_2_1,
            po.optionid_verso_dated_2_2,
            po.verso_dated_2_3,
            po.optionid_verso_numbered_1,
            po.optionid_verso_numbered_2,
            po.verso_numbered_3,
            po.optionid_verso_inscribed_1,
            po.optionid_verso_inscribed_2,
            po.verso_inscribed_3,
            po.verified_recto,
            po.optionid_recto_signed_1_1,
            po.optionid_recto_signed_1_2,
            po.recto_signed_1_3,
            po.optionid_recto_signed_2_1,
            po.optionid_recto_signed_2_2,
            po.recto_signed_2_3,
            po.optionid_recto_dated_1_1,
            po.optionid_recto_dated_1_2,
            po.recto_dated_1_3,
            po.optionid_recto_dated_2_1,
            po.optionid_recto_dated_2_2,
            po.recto_dated_2_3,
            po.optionid_recto_numbered_1,
            po.optionid_recto_numbered_2,
            po.recto_numbered_3,
            po.optionid_recto_inscribed_1,
            po.optionid_recto_inscribed_2,
            po.recto_inscribed_3,
            po.inscriptions,
            po.notations,
            po.city,
            po.country,
            po.collection,
            po.private_col,
            po.copyright,
            po.publish,
            po.source,
            po.ready_for_usage,
            po.licence,
            po.width,
            po.height,
            DATE_FORMAT(po.date_modified ,'%d %b %Y %H:%i:%s') AS date_modified_admin,
            DATE_FORMAT(po.date_created ,'%d %b %Y %H:%i:%s') AS date_created_admin ";
        $query_rows.=$values['query_rows'];
        $query_full.=$query_rows;
        $query_full.="FROM ".TABLE_PAINTING_OPP." po";

        $sql['query_rows']=$query_rows;
        $sql['query_without_limit']=$query_full." ".$values['where']." ".$values['order'];
        $sql['query']=$sql['query_without_limit']." ".$values['limit'];

        return $sql;
    }



}
