<?php
# meta info
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION_HOME,"Find out more about the life and career of Gerhard Richter, one of the most important artists of the 20th and 21st centuries.");
define(LANG_META_DESCRIPTION_ART,"Discover Richter's versatile oeuvre, ranging from oils on canvas, works on paper, overpainted photographs and Atlas, his collection of photographs, newspaper cuttings and sketches.");
define(LANG_META_DESCRIPTION_BIOGRAPHY,"The biography sheds light on Richter's life and career. Born in Dresden in 1932, Richter was brought up under Third Reich and received the first part of his artistic education in East Germany. In 1961 he relocated to West Germany, where he developed into one of the most distinguished artists of his time.");
define(LANG_META_DESCRIPTION_CHRONOLOGY,"A concentrated overview of Richter's life and career: important chapters of his private life are mirrored by significant artistic achievements.");
define(LANG_META_DESCRIPTION_QUOTES,"A selection of Richter's quotes offer insight into his way of working, his understanding of art and his view of the world.");
define(LANG_META_DESCRIPTION_EXHIBITIONS,"Research over 50 years of exhibition history of Gerhard Richter's works.");
define(LANG_META_DESCRIPTION_LITERATURE,"Research Richter's artist's books, publications on his oeuvre as well as newspaper articles and online publications.");
define(LANG_META_DESCRIPTION_VIDEOS,"Watch videos on Richter's recent exhibitions, on selected groups of his works and talks.");
define(LANG_META_DESCRIPTION_LINKS,"Find links to articles in newspapers and magazines as well as to galleries dealing with Richter's works.");
define(LANG_META_DESCRIPTION_NEWS,"Current news: ongoing exhibitions, upcoming auctions and recent publications can be found here.");
# END meta info

#PAGE TITLES
if( $_SESSION['kiosk'] ) define(LANG_TITLE_HOME,"Gerhard-Richter.com");
else define(LANG_TITLE_HOME,"Gerhard Richter");
#PAGE TITLES END

# MENU
define(LANG_HEADER_MENU_HOME,'Home');
define(LANG_HEADER_MENU_HOME_TT,' Home page Gerhard Richter');
define(LANG_HEADER_MENU_ARTWORK,'Opere');
define(LANG_HEADER_MENU_ARTWORK_TT,"Le opere di Gerhard Richter: i quadri, l'Atlante, le edizioni, le fotografie sovraverniciate, gli acquerelli e i disegni");
define(LANG_HEADER_MENU_BIOGRAPHY,'Biografia');
define(LANG_HEADER_MENU_BIOGRAPHY_TT,'La vita di Gerhard Richter');
define(LANG_HEADER_MENU_CHRONOLOGY,'Cronologia');
define(LANG_HEADER_MENU_CHRONOLOGY_TT,' La cronologia della vita di Gerhard Richter');
define(LANG_HEADER_MENU_QUOTES,'Citazioni');
define(LANG_HEADER_MENU_QUOTES_TT,'Le citazioni di Gerhard Richter');
define(LANG_HEADER_MENU_EXHIBITIONS,'Mostre');
define(LANG_HEADER_MENU_EXHIBITIONS_TT,'Le mostre nel mondo di Gerhard Richter');
define(LANG_HEADER_MENU_LITERATURE,'Pubblicazioni');
define(LANG_HEADER_MENU_LITERATURE_TT,'I libri su Gerhard Richter');
define(LANG_HEADER_MENU_VIDEOS,'Video');
define(LANG_HEADER_MENU_VIDEOS_RELATED,'Video correlati');
define(LANG_HEADER_MENU_VIDEOS_TT,'I video e le interviste di Gerhard Richter');
define(LANG_HEADER_MENU_AUDIO,'Audio');
define(LANG_HEADER_MENU_AUDIO_TT,'I pezzi sonori');
define(LANG_HEADER_MENU_MEDIA,'Media');
define(LANG_HEADER_MENU_MEDIA_TT,' I media correlati a Gerhard Richter');
define(LANG_HEADER_MENU_CONTACT,'Link');
define(LANG_HEADER_MENU_CONTACT_TT,'I link correlati a Gerhard Richter');
define(LANG_HEADER_MENU_NEWS,'Attualità');
define(LANG_HEADER_MENU_NEWS_TT,' Le attualità di Gerhard Richter');
define(LANG_HEADER_MENU_SEARCH,'Cerca');
define(LANG_HEADER_MENU_SEARCH_TT,'Cerca Gerhard Richter');
define(LANG_NEWS,'Attualità');
define(LANG_HEADER_MENU_ADMIN,'Admin');
# MENU END

#LEFT SIDE
define(LANG_LEFT_SIDE_PAINTINGS,'Pitture');
define(LANG_LEFT_SIDE_CATEGORIES,'Categorie');
define(LANG_LEFT_SIDE_ATLAS,'Atlante');
define(LANG_LEFT_SIDE_EDITIONS,'Edizioni');
define(LANG_LEFT_SIDE_YEARS,'Anni');
define(LANG_LEFT_SIDE_DRAWINGS,'Disegni');
define(LANG_LEFT_SIDE_OILSONPAPER,'Olio su carta');
define(LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS,'Fotografie sovraverniciate');
define(LANG_LEFT_SIDE_WATERCOLOURS,'Acquerelli');
define(LANG_LEFT_SIDE_OTHER,'Altro');
define(LANG_LEFT_SIDE_PRINTS,"Prints");
define(LANG_LEFT_SIDE_MICROSITES,'Micrositi');
#LEFT SIDE END

#RIGHT SIDE
define(LANG_RIGHT_SEARCH_H1,'Cerca');
define(LANG_RIGHT_SEARCH_EXH_H1,'Cerca');
define(LANG_RIGHT_SEARCH_QUOTES_H1,'Cerca');
define(LANG_RIGHT_SEARCH_VIDEOS_H1,'Cerca');
define(LANG_RIGHT_SEARCH_SEARCHNOW,'Cerca ora');
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE1,"per");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE2,"tecnica");
define(LANG_RIGHT_SEARCH_BYTITLE1,"per");
define(LANG_RIGHT_SEARCH_BYTITLE2,"titolo");
define(LANG_RIGHT_SEARCH_BY,"per");
define(LANG_RIGHT_SEARCH_BYPAINTINGS,"numero (Pitture) di CR");
define(LANG_RIGHT_SEARCH_BYATLAS,"Tavola");
define(LANG_RIGHT_SEARCH_BYEDITION,"numero (Edizioni) di CR");
define(LANG_RIGHT_SEARCH_BYDRAWINGS,"numero (Disegni) di CR");
define(LANG_RIGHT_SEARCH_BYNUMBER,"numero");
define(LANG_RIGHT_SEARCH_ADVANCED_SEARCH,"Ricerca avanzata");
define(LANG_RIGHT_SEARCH_QUICK_SEARCH,"Ricerca veloce");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST,"lista");
define(LANG_RIGHT_SEARCH_BYNUMBER_RANGE,"margine");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE,"Aggiungere");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE,"Eliminare");
define(LANG_RIGHT_SEARCH_YEARPAINTED1,"per");
define(LANG_RIGHT_SEARCH_YEARPAINTED2,"anno");
define(LANG_RIGHT_SEARCH_DATE,"data");
define(LANG_RIGHT_SEARCH_DATE_FROM,"Da");
define(LANG_RIGHT_SEARCH_DATE_TO,"A");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_D,"GG");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_M,"MM");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y,"AAAA");
define(LANG_RIGHT_SEARCH_COLOR1,"per");
define(LANG_RIGHT_SEARCH_COLOR2,"Colore");
define(LANG_RIGHT_SEARCH_BYSIZE,"dimensione");
define(LANG_RIGHT_SEARCH_BYSIZEH,"H");
define(LANG_RIGHT_SEARCH_BYSIZEH1,"Altezza in cm");
define(LANG_RIGHT_SEARCH_BYSIZEW,"L");
define(LANG_RIGHT_SEARCH_BYSIZEW1,"Larghezza in cm");
define(LANG_RIGHT_SEARCH_BYSIZEMIN,"Min");
define(LANG_RIGHT_SEARCH_BYSIZEMAX,"Max");
define(LANG_RIGHT_SEARCH_BY_COLLECTION,"per <strong>luogo</strong>");
define(LANG_RIGHT_SEARCH_SEARCHBUTTON,"Cerca");
define(LANG_RIGHT_SEARCH_SEARCHRESET,"Annulla");
define(LANG_RIGHT_SEARCH_EXH_LOCATION,"Luogo");
define(LANG_RIGHT_SEARCH_EXH_TITLE,"Titolo");
define(LANG_RIGHT_SEARCH_EXH_KEYWORD,"Parola chiave");
define(LANG_RIGHT_SEARCH_QUOTES_KEYWORD,"Parola chiave");
define(LANG_RIGHT_SEARCH_VIDEOS_KEYWORD,"Parola chiave");
define(LANG_RIGHT_SEARCH_QUOTES_SEARCHALL,"Consulta tutte le citazioni");
define(LANG_RIGHT_SEARCH_LIT_AUTHOR,"Autore");
define(LANG_RIGHT_SEARCH_LIT_TITLE,"Titolo");
define(LANG_RIGHT_SEARCH_LIT_KEYWORD,"Parola chiave");
define(LANG_RIGHT_SEARCH_LIT_CATEGORY,"Categoria");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT,"Selezionare...");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE,"Lingua");
define(LANG_RIGHT_FEATURED_TITLES,"Micrositi");
define(LANG_RIGHT_FEATURED_TITLES_SNOWWHITE,"Snow White");
define(LANG_RIGHT_FEATURED_TITLES_WARCUT,"War Cut II");
define(LANG_RIGHT_FEATURED_TITLES_FIRENZE,"Firenze");
define(LANG_RIGHT_FEATURED_TITLES_4900COLOURS,"4900 Colori");
define(LANG_RIGHT_FEATURED_TITLES_SINBAD,"Sinbad");
define(LANG_RIGHT_FEATURED_TITLES_ELBE,"Elbe");
define(LANG_RIGHT_FEATURED_TITLES_COLOGNE_CATHEDRAL,"Cattedrale di Colonia");
define(LANG_RIGHT_TIMELINE,"<span class='related'>Cronologia</span><br />Esplora la carriera dell'artista.");
define(LANG_RIGHT_TIMELINE_INTER,"Cronologia interattiva");
define(LANG_RIGHT_VIRTUALEXH,"<span class='related'>Virtuali</span><br />Mostre");
define(LANG_RIGHT_FETURED_TITLES,"Titoli in primo piano");
define(LANG_RIGHT_VIDEO,"Per vedere questi video hai bisogno di avere installato Flash sul tuo computer.");
define(LANG_RIGHT_VIDEO_DOWN,"Scarica");
define(LANG_RIGHT_AUDIO,"Per ascoltare questi file audio hai bisogno di avere installato Flash sul tuo computer.");
define(LANG_RELATED_BLOCK_BROWSE_CLIPS,"All clips");
define(LANG_RELATED_BLOCK_RECENTLY,"Recently Added");
#RIGHT SIDE END

# pagina dei crediti
define(LANG_CREDITS,'Crediti');
define(LANG_CREDITS_TEXT1_1,'Il sito ufficiale di Gerhard Richter');
define(LANG_CREDITS_TEXT1_2,'è stato creato ed è gestito da Joe Hage.');
define(LANG_CREDITS_TEXT2_1,'I nostri ringraziamenti vanno a Dietmar Elger (autore di');
define(LANG_CREDITS_TEXT2_2,'Gerhard Richter, Catalogo ragionato, Vol. 1, 2011');
define(LANG_CREDITS_TEXT2_3,') per il suo aiuto ed appoggio.');
define(LANG_CREDITS_TEXT3_1,"Un grazie a Hubertus Butin per il suo lavoro di ricerca approfondito e dettagliato sulle");
define(LANG_CREDITS_TEXT3_2,"Edizioni di Gerhard Richter.");
define(LANG_CREDITS_TEXT3_3,"");
define(LANG_CREDITS_TEXT4,'Grazie ad Amy Dickson per il suo lavoro sulla cronologia.');
# fine

# note a piè di pagina
define(LANG_FOOTER_COPYRIGHT,'Gerhard Richter - Tutti i diritti riservati');
define(LANG_FOOTER_CONTACT,'Contatto');
define(LANG_FOOTER_CREDITS,'Crediti');
define(LANG_FOOTER_DISCLIMER,'Menzioni legali');
# footer END

# home page
define(LANG_HOME_TEXT_INTRO1,"Gerhard Richter è uno dei grandi artisti del ventesimo e ventunesimo secolo; la sua carriera comprende un arco di tempo di più di cinquant'anni. Su questo sito potrete ammirare il suo lavoro e conoscere la sua vita. Clicca su una delle opere qui in basso per cominciare.");
# home END

# pagina delle opere
define(LANG_ARTWORK_PHOTO_PAINTINGS,'Pitture');
define(LANG_ARTWORK_ABSTRACTS,'Quadri astratti');
define(LANG_ARTWORK_ATLAS,'Atlante');
define(LANG_ARTWORK_ATLAS_SHEET,"Tavola dell'Atlante");
define(LANG_ARTWORK_EDITIONS,'Edizioni');
define(LANG_ARTWORK_WATERCOLOURS,'Acquerelli');
define(LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS,'Fotografie sovraverniciate');
define(LANG_ARTWORK_DRAWINGS,'Disegni');
define(LANG_ARTWORK_OILSONPAPER,'Olio su carta');
define(LANG_ARTWORK_OTHER,'Altro');
define(LANG_ARTWORK_MICROSITES,'Micrositi');
define(LANG_ARTWORK_MICROSITES2,'Micrositi');
define(LANG_ARTWORK_ARTISTS_BOOKS,"Libri d'artista");
define(LANG_ARTWORK_TEXT_H1,"Mezzo secolo nell'arte");
define(LANG_ARTWORK_TEXT,"Ufficialmente, Richter ha iniziato a dipingere nel 1962. In questo sito avete accesso alla diversità del suo lavoro che spazia dagli oli su tela alle fotografie sovraverniciate, fino ai riferimenti storici delle fonti che sono radunate nell'<i>Atlante</i>.");
define(LANG_ARTWORK_TEXT_PAINTINGS1,"Ufficialmente, Richter ha iniziato a dipingere nel 1962. Qui può accedere alle opere del artista che comprendono i quadri Foto-pitture e le astratti.");
define(LANG_ARTWORK_TEXT_PAINTINGS,"Nonostante l'artista eviti intenzionalmente di classificare il suo lavoro, abbiamo diviso le opere in categorie per facilitarne la lettura.");
define(LANG_ARTWORK_SECTION_PAINTINGS,"Pitture");
define(LANG_ARTWORK_SECTION_PHOTOPAINTINGS,"Foto-pitture");
define(LANG_ARTWORK_SECTION_ABSTRACTS,"Quadri astratti");
define(LANG_ARTWORK_SECTION_NOCR,"Senza CR");
define(LANG_ARTWORK_SECTION_OTHERMEDIA,"Altri");
define(LANG_ARTWORK_SECTION_OTHER,"Pitture distrutte");
define(LANG_ARTWORK_TEXT_ATLAS,"Guarda la totalità della collezione dell'<i>Atlante</i> - i ritagli di giornali, le foto e gli schizzi sono le fonti di gran parte del lavoro di Richter.");
define(LANG_ARTWORK_TITLE_OVERPAINTED_PHOTOS,"Catalogo ragionato delle Fotografie sovraverniciate di Gerhard Richter");
define(LANG_ARTWORK_TEXT_OVERPAINTED_PHOTOS,"Il catalogo ragionato delle Fotografie sovraverniciate di Gerhard Richter sarà pubblicato nel 2015. Stiamo dunque raccogliendo tutte le informazioni che potrebbero essere utili per la pubblicazione, comprese le immagini e i nomi dei proprietari di tali opere. Vi saremmo grati se poteste fornirci tali informazioni. Vi preghiamo di contattarci; ogni informazione sarà trattata in maniera assolutamente confidenziale.");
define(LANG_ARTWORK_CONTACT_OVERPAINTED_PHOTOS,"Invia le informazioni");
define(LANG_ARTWORK_TEXT_ATLAS_BACKTOATLAS,"Torna all'Atlante");
define(LANG_ARTWORK_TEXT_ATLAS_ASOCIATED,'Lavori correlati');
define(LANG_ARTWORK_BROWSE_WORKS,'Consulta tutte le pitture');
define(LANG_ARTWORK_BROWSE_OPP,'Consulta tutte le Fotografie sovraverniciate');
define(LANG_ARTWORK_BROWSE_ATLAS,"Consulta tutto l'Atlante");
define(LANG_ARTWORK_BROWSE_EDITIONS,'Consulta tutte le Edizioni');
define(LANG_ARTWORK_ATLAS_ASSOCIATED,'I riquadri qui sotto mostrano le immagini delle pitture provenienti dalle tavole dell’Atlante.');
define(LANG_ARTWORK_BOOKS_REL_TAB,'Pubblicazioni');
define(LANG_ARTWORK_BOOKS_REL_COLOUR,'colore');
define(LANG_ARTWORK_BOOKS_REL_BW,'nero/bianco');
define(LANG_ARTWORK_BOOKS_REL_ARTWORK,'Opere');
define(LANG_ARTWORK_BOOKS_REL_ILLUSTRATED,'Illustrato');
define(LANG_ARTWORK_BOOKS_REL_MENTIONED,'Menzionati');
define(LANG_ARTWORK_BOOKS_REL_DISCUSSED,'Discussi');
define(LANG_ARTWORK_BOOKS_REL_P,'p.');
define(LANG_ARTWORK_BOOKS_REL_PP,'pp.');
define(LANG_ARTWORK_BOOKS_REL_P2,'');
define(LANG_ARTWORK_BOOKS_REL_PP2,'');
define(LANG_ARTWORK_BOOKS_REL_CHINESE,'');
define(LANG_ARTWORK_PHOTOS_TAB,'Foto');
define(LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS,"Vedute dell'installazione:");
define(LANG_ARTWORK_PHOTOS_TAB_DEATILS,'Dettagli');
define(LANG_ARTWORK_THUMB_SALES_HIST_AVAIL,'Cronistoria delle vendite');
define(LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL,'Informazioni disponibili sulla collezione');
define(LANG_ARTWORK_BREADCRUMB_ALL,'Tutte le pitture');
define(LANG_ARTWORK_BREADCRUMB_ALL_OPP,'Tutte le fotografie');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN,"Questa opera d'arte è stata esposta nelle seguenti mostre:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED,"Questa opera d'arte è inclusa nelle seguenti pubblicazioni:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS,"Questa opera d'arte è inclusa nelle seguenti pubblicazioni:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS,"Fotografie dettagliate dell'opera d'arte:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS,"Questa opera d'arte si basa su una imagine inclusa in Atlas:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC,"Questa scheda di Atlas comprende le immagini di origine delle seguenti opera:");
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB,'Individual works');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC,'This edition consists of the following individual works:');
define(LANG_ARTWORK_DESTROYED,"DISTRUTTO");
# artwork END

# Tabs titles
define(LANG_TAB_TITLE_ARTWORKS,'Opere');
# Tabs titles END

# pagina dei contatti
define(LANG_CONTACT_SUBNAV_TEXT_HEADING,"Link dell'artista");
define(LANG_CONTACT_SUBNAV_TEXT_ITEM1,'Scriveteci');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM2,'Galleristi principali');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM4,'Galleristi');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM3,'Vendita di poster');
define(LANG_CONTACT_WRITE_TEXT_BRIEF,'Se si desidera contattare il sito web di Gerhard Richter siete pregati di utilizzare il modulo riportato qui sotto.');
define(LANG_CONTACT_WRITE_INPUTVALUE_EMAIL,'Inserisci la tua mail');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT1,'Domanda generale/Commento');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT2,'Cerca galleristi');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT3,'Compra le Edizioni dei poster');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT4,'Image permissions');
define(LANG_CONTACT_WRITE_INPUTVALUE_MESSAGE,'Scrivi qui il tuo messaggio');
define(LANG_CONTACT_WRITE_INPUTVALUE_SEND,'Invio');
define(LANG_CONTACT_POSTERS_LINK,' Le Edizioni dei poster di Gerhard Richter');
define(LANG_CONTACT_SHOP_LINK,' La boutique di Gerhard Richter ');
define(LANG_CONTACT_EMAIL,'Indirizzo email ');
define(LANG_CONTACT_SUBJECT_SELECT,'Seleziona un tema');
define(LANG_CONTACT_SUBJECT,'Tema');
define(LANG_CONTACT_FILE,'File');
define(LANG_CONTACT_MESSAGE,'Messaggio');
define(LANG_CONTACT_CAPCHA,'Si prega di inserire i seguenti caratteri nel campo sottostante (no lettere maiuscole o spazi).');
define(LANG_CONTACT_THANX1,'Grazie');
define(LANG_CONTACT_THANX2,'Grazie per aver contattato il sito web di Gerhard Richter.');
# contact END

# link
define(LANG_LINKS_ARTICLES,'Articoli');
define(LANG_LINKS_DEALERS,'Galleristi');
define(LANG_LINKS,'Link');
define(LANG_LINKS_ARTICLES_PUBLISHED_IN,'In');
# links END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_xsmall.png');
define(LANG_NO_IMAGE_SMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_small.png');
define(LANG_NO_IMAGE_MEDIUM,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_LARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_XLARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_ORIGINAL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_THUMBNAIL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_thumbnail.png');
# IF NO IMAGE To DISPLAY END

# DIFFERENT
define(LANG_ATLASSHEETS,'Tavole dell’Atlante');
define(LANG_RELEATED,'Sezioni relative');
define(LANG_RELEATED_ART,'Opere correlate');
define(LANG_COLLECTION,'Collezione');
define(LANG_NOTES,'Note');
define(LANG_SHOP,'Boutique');
define(LANG_SHOP_INFO,'Articoli disponibili online');
define(LANG_NEXTDECADE,'prossimo decennio');
define(LANG_PREVDECADE,'decennio precedente');
define(LANG_SELECT,'Selezionare..');
define(LANG_SELECT_FROM,'Da');
define(LANG_SELECT_TO,'A');
define(LANG_DIMENSIONS_FROM,'Da');
define(LANG_DIMENSIONS_TO,'A');
define(LANG_BACK_TO_ALL_KEYWORDS,'guarda tutto');
define(LANG_BACK_TO_ALL, 'guarda tutto');
define(LANG_SEE_ALL, 'Guarda tutto');
define(LANG_SEE,'Guarda');
define(LANG_CLICK_IMAGE_TO_ENLARGE,'Clicca sull’immagine per ingrandire');
define(LANG_CATALOGUE_RESONE,"Catalogo ragionato");
define(LANG_ATLAS_SHEET,"Tavola dell’Atlante");
define(LANG_GO_TO_ATLAS_SHEET,"Vai alla tavola dell’Atlante");
define(LANG_MUSEUM_COLLECTION,"Collezione pubblica");
define(LANG_SALES_HISTORY,"Cronistoria delle vendite");
define(LANG_SALES_HISTORY_UPON_REQUEST,"Contatto casa d'aste");
define(LANG_SALES_HISTORY_TB_ANNOUNCED,"da definire");
define(LANG_OF,"di");
define(LANG_IN,"in");
define(LANG_SEARCH_RESULTS_ALL,"Cercare nei risultati");
define(LANG_SEARCH_PAINTINGS,"Cerca");
define(LANG_SEARCH_RESULT, "Risultato");
define(LANG_SEARCH_RESULTS, "Risultati");
define(LANG_SEARCH_ART, "Opere");
define(LANG_SEARCH_BIOGRAPHY, "Biografia");
define(LANG_SEARCH_QUOTES, "Citazioni");
define(LANG_SEARCH_EXHIBITIONS, "Mostre");
define(LANG_SEARCH_LITERATURE, "Pubblicazioni");
define(LANG_SEARCH_VIDEOS, "Video");
define(LANG_SEARCH_LINKS, "Link");
define(LANG_SEARCH_NEWS_TALKS, "Conferenza");
define(LANG_SEARCH_NEWS_AUCTIONS, "Aste");
define(LANG_SEARCH_CREDITS, "Crediti");
define(LANG_SEARCH_DISCLAIMER, "Menzioni legali");
define(LANG_SEARCH_CHRONOLOGY, "Cronologia");
define(LANG_SEARCH_BASIC_TOP_VALUE_SEARCH, "Ricerca veloce");
define(LANG_SEARCH_SHOW_ALL, "Mostra tutto");
define(LANG_SEARCH_YOUSEARCHEDFOR,"Hai cercato");
define(LANG_SEARCH_ALLPAINTINGS,"Tutte le pitture");
define(LANG_SEARCH_NUMBER,"Numero");
define(LANG_SEARCH_ARTWORKTYPE,"Tipo di opera");
define(LANG_SEARCH_TITLE,"Titolo");
define(LANG_SEARCH_LOCATION,"Luogo");
define(LANG_SEARCH_SHEETNUMBER,"Numero di tavola");
define(LANG_SEARCH_CRNUMBER,"Numero di CR");
define(LANG_SEARCH_YEARFROM,"Anno da");
define(LANG_SEARCH_YEAR,"Anno");
define(LANG_SEARCH_YEARTO,"Anno a");
define(LANG_SEARCH_DATE,"Data");
define(LANG_SEARCH_COLOR,"Colore");
define(LANG_SEARCH_HEIGHT,"Altezza");
define(LANG_SEARCH_WIDTH,"Larghezza");
define(LANG_SEARCH_NORESULTS,"Siamo spiacenti, la ricerca non ha portato a nessun risultato");
define(LANG_SEARCH_RESULTS1,"La ricerca ha prodotto");
define(LANG_SEARCH_RESULTS2,"risultato");
define(LANG_SEARCH_RESULTS3,"risultati");
define(LANG_CATEGORY_DESC_MORE,"più");
define(LANG_CATEGORY_DESC_CLOSE,"chiudi");
define(LANG_SEARCH_RETURNTOTSEARCH,"Torna ai risultati dell’ultima ricerca");
define(LANG_USE_THIS,"Utilizza questo menù a discesa per passare a qualsiasi sezione");
define(LANG_BACKTOTOP,"torna all’inizio");
define(LANG_SHOW,"Mostra");
define(LANG_ALL,"Tutto");
define(LANG_PERPAGE,"per pagina");
define(LANG_IMAGES_PERPAGE,"Show");
define(LANG_BACKTOEXH,"Torna alla mostra");
define(LANG_GOBACK,"vai indietro");
define(LANG_SALEHIST_TAB_NOTES,"Note:");
define(LANG_ESTIMATE,"Stimato a");
define(LANG_BOUGHT_IN,"Invenduto");
define(LANG_WITHDRAWN,"Ritirata");
define(LANG_PREMIUM,"(inclusa commissione d'acquisto)");
define(LANG_HAMMER,"(prezzo di aggiudicazione)");
define(LANG_UNKNOWN,"Unknown");
define(LANG_LOT,"Lotto");
define(LANG_SOLDPR,"Venduto a");
define(LANG_SHOWING,"Mostra");
define(LANG_BOOKS_OF,"di");
define(LANG_BOOKS_FROM,"da");
define(LANG_BOOKS,"libri");
define(LANG_BOOKS_BY,"Autore");
define(LANG_BOOKS_DETAILS,"Dettagli");
define(LANG_BOOKS_CLOSE,"Chiudi");
define(LANG_BOOKS_MORE,"più");
define(LANG_BOOKS_MORE_DETAILS,"More Details");
define(LANG_BOOKS_LANGUAGES,"Lingue");
define(LANG_BOOKS_LANGUAGE,"Lingua");
define(LANG_BOOKS_CATEGORY,"Categoria");
define(LANG_CONTACT," 'Se volete contattarci siete pregati di usare questo modulo");
define(LANG_CONTACT_UNAVAILABLE,"Al momento non disponibile. Ci scusiamo per questo inconveniente.");
define(LANG_CONTACT_UNAVAILABLE2," 'Se volete contattarci, scrivete a info@gerhard-richter.com");
define(LANG_BOOKS_LANGUAGE_EXHCATALOG,"Mostre");
define(LANG_BOOKS_READ_ARTICLE,"Leggere l'articolo");
define(LANG_COPY_TO_CLIPBOARD,"Copia collegamento negli appunti");
define(LANG_PAINTING_SIZE_PARTS," parti");
define(LANG_SUBMITING_FORM,"submitting form.....");
define(LANG_FOUND,"found");
define(LANG_BOOKS_READ_ESSAY,"Leggere Essay");
define(LANG_CR,"CR");
define(LANG_CR_EDITIONS,LANG_LEFT_SIDE_EDITIONS." ".LANG_CR);
define(LANG_CR_DRAWINGS,LANG_LEFT_SIDE_DRAWINGS." ".LANG_CR);
# DIFFERENT END

# pagina della biografia
define(LANG_BIOGRAPHY,"Biografia");
define(LANG_WORK,"Opere");
define(LANG_PHOTOS,"Foto");
define(LANG_QUOTES,"Citazioni");
define(LANG_QUOTES_NO_QUOTES_FOUND,"Nessuna citazione trovata!");
define(LANG_QUOTES_SOURCE,"SOURCE");
# biograpphy page end

#cronologia
define(LANG_CHRONOLOGY_H1_1, "Cronologia");
define(LANG_CHRONOLOGY_H1_2, "di Amy Dickson");
define(LANG_AMY_DICKSON_THANKS, "Un grazie speciale a Amy Dickson per il suo lavoro sulla cronologia. Questa è una versione editata della cronologia compilata da Amy Dickson e pubblicata in <i>Gerhard Richter: Panorama</i> (&#169;Tate, 2011. Riprodotta con il permesso di Tate Trustees).");
define(LANG_CHRONOLOGY_LEFT_1930, "Anni 1930");
define(LANG_CHRONOLOGY_LEFT_1940, "Anni 1940");
define(LANG_CHRONOLOGY_LEFT_1950, "Anni 1950");
define(LANG_CHRONOLOGY_LEFT_1960, "Anni 1960");
define(LANG_CHRONOLOGY_LEFT_1970, "Anni 1970");
define(LANG_CHRONOLOGY_LEFT_1980, "Anni 1980");
define(LANG_CHRONOLOGY_LEFT_1990, "Anni 1990");
define(LANG_CHRONOLOGY_LEFT_2000, "Anni 2000");
define(LANG_CHRONOLOGY_LEFT_2010, "Anni 2010");
#chronology end

# exhibition
define(LANG_VIEW_EXHIBITION,"Guarda la mostra");
define(LANG_EXH_ONWARDS,"2020 in poi");
define(LANG_GROUP,"collettive");
define(LANG_GROUP_EXH,"Mostra di gruppo");
define(LANG_GROUP_EXHS,"Mostre di gruppo");
define(LANG_SOLO,"personali");
define(LANG_SOLO_EXH,"Mostra personale");
define(LANG_SOLO_EXHS,"Mostre personali");
define(LANG_EXH_DECADES_S,"s");
define(LANG_EXH_SORT_BY,"Ordina per");
define(LANG_EXH_SORT_BY_DATE,"Data");
define(LANG_EXH,"Mostre");
define(LANG_EXH_FOUND,"Mostre");
define(LANG_EXH_FOUND_S,"Mostre");
define(LANG_EXH_SHOW_ALL,"Mostra tutto");
define(LANG_EXH_ARTWORK,"Opere");
define(LANG_EXH_RELATED_EXHIBITIONS,"Mostre correlate");
define(LANG_EXH_INST_SHOTS,"Vedute dell'allestimento");
define(LANG_EXH_LITERATURE,"Pubblicazioni");
define(LANG_EXH_VIDEOS,"Video");
define(LANG_EXH_GUIDE,"Guida");
define(LANG_EXH_SHOW_ALL_WORKS,"Mostra tutte le opere");
define(LANG_EXH_ARTWORK_TAB_WORKS,"opere");
define(LANG_EXH_ARTWORK_TAB_WORK,"opera");
define(LANG_EXH_INST_TAB_PHOTOS,"foto");
define(LANG_EXH_INST_TAB_PHOTO,"foto");
define(LANG_EXH_ARTWORK_SORT_BY,"Ordina per");
define(LANG_EXH_ARTWORK_SORT_CR_NUMBER,"Numero di CR");
define(LANG_EXH_ARTWORK_SORT_YEAR,"Anno");
define(LANG_INSTALLSHOT,"Vedute dell’allestimento");
define(LANG_ALL_EXH,"Tutte le mostre");
define(LANG_LEFT_SIDE_INSTALLATION_SOTS,"Vedute dell’allestimento");
define(LANG_LEFT_SIDE_GUIDE,"Guida");
define(LANG_LEFT_SIDE_EXH_TOUR,"Tour della mostra");
# END exhibition

# pubblicazioni
define(LANG_LIT_LEFT_MONOGRAPHS,"Monografie");
define(LANG_LIT_LEFT_CATALOGUES,"Cataloghi");
define(LANG_LIT_LEFT_JOURNALS,"Riviste");
define(LANG_LIT_LEFT_NEWS_ART,"Articoli di giornale");
define(LANG_LIT_LEFT_ONL_PUBL,"Pubblicazioni on-line");
define(LANG_LIT_LEFT_OTHERS,"Altrui");
define(LANG_LIT_LEFT_FILMS,"Film");
define(LANG_LIT_LEFT_THESES,"Tesi");
define(LANG_LIT_LEFT_SOLO_EXH,"Mostre personali");
define(LANG_LIT_LEFT_GROUP_EXH,"Mostre di gruppo");
define(LANG_LIT_LEFT_COLLECTIONS,"Collezioni");
define(LANG_LIT_ONWARDS,"2010 in poi");
define(LANG_LIT_PUBLISHED,"Anno di pubblicazione");
define(LANG_LIT_SEARCH,"Cerca pubblicazioni");
define(LANG_LIT_BOOKS_PER,"libri per pagina");
define(LANG_LIT_SEARCH_HELP,"testata=[Cerca per libro aiuto] body=[&#60;p&#62;Se cerchi un libro specifico devi seguire alcuni criteri.&#60;/p&#62;&#60;p&#62; Puoi cercare per titolo, autore o entrambi, o per parola chiave.&#60;/p&#62;&#60;p&#62;Esempi:&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;J&uuml;rgen Schilling&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter, J&uuml;rgen Schilling &#60;/p&#62;&#60;p&#62;- &lsquo;Schilling&rsquo;&#60;/p&#62;]");
define(LANG_LIT_SEARCH_TITLE,"Titolo");
define(LANG_LIT_SEARCH_AUTHOR,"Autore");
define(LANG_LIT_SEARCH_AUTHOR2,"Autore");
define(LANG_LIT_SEARCH_DATE,"Data");
define(LANG_LIT_SEARCH_SORTBYTITLE,"Ordina per titolo");
define(LANG_LIT_SEARCH_SORTBYAUTHOR,"Ordina per autore");
define(LANG_LIT_SEARCH_SORTBYDATE,"Ordina per data");
define(LANG_LIT_SEARCH_CLICK,"Clicca per guardare i dettagli");
define(LANG_LIT_SEARCH_PUBBY,"Editore");
define(LANG_LIT_SEARCH_EDITOR,"Editore");
define(LANG_LIT_SEARCH_UNKNOWN,"Sconosciuto");
define(LANG_LIT_SEARCH_ISBNNOTAVA,"isbn non disponibile");
define(LANG_LIT_SEARCH_SEERELEXH,"Guarda le mostre");
define(LANG_LIT_SEARCH_SEERELEXH2,"Guarda la mostra");
define(LANG_LIT_SEARCH_GOTORELEXH,"vai alle mostre correlate");
define(LANG_LIT_SEARCH_GOTORELEXH2,"vai alla mostra correlata");
define(LANG_LIT_SEARCH_PAGES,"pagine");
define(LANG_LIT_SEARCH_NOFOUND,"Nessun libro trovato, prova ancora.");
define(LANG_LIT_SEARCH_BACKTOLIT,"Torna alle pubblicazioni");
define(LANG_LIT_SEARCH_INFORMATION,"Informazione");
define(LANG_LITERATURE_ENLARGE, "ingrandire");
define(LANG_LITERATURE_IN, "In");
define(LANG_LITERATURE_ISSUE, "Edizione");
define(LANG_LITERATURE_VOL, "Vol.");
define(LANG_LITERATURE_NO, "No.");
define(LANG_LITERATURE_PAGES, "p.");
define(LANG_LITERATURE_PAGESS, "pp.");
define(LANG_LITERATURE_COVER_HARDBACK, "Copertina rigida");
define(LANG_LITERATURE_COVER_SOFTCOVER, "Brossura");
define(LANG_LITERATURE_COVER_UNKNOWNBINDING, "Rilegatura sconosciuta");
define(LANG_LITERATURE_COVER_SPECIALBINDING, "Rilegatura speciale");
define(LANG_LITERATURE_ISBN, "ISBN");
define(LANG_LITERATURE_ISSN, "ISSN");
define(LANG_LITERATURE_DESC_TEXT, "Il lavoro e la vita di Gerhard Richter sono stati oggetto di numerose pubblicazioni. Da letteratura varia a cataloghi di mostre e cataloghi di raccolta di monografie, film e articoli. Le pubblicazioni di Richter in forma di libri d'artista sono una parte importante delle sue opere. Troverete una selezione di letteratura Richter correlata qui.");
define(LANG_LITERATURE_SHOW_MORE_OPTIONS, "piu' opzioni");
define(LANG_LITERATURE_SHOW_LESS_OPTIONS, "meno opzioni");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEO, "Video correlate");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS, "Video correlati");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION, "Questo catalogo è stato pubblicato in occasione delle seguenti mostre:");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS, "Questo catalogo è stato pubblicato in occasione delle seguenti mostre:");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE, "Related publication");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES, "Related publications");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK, "Le seguenti opere sono visualizzate, discuse o citate nel libro:");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS, "Le seguenti opere sono visualizzate, discuse o citate nel libro:");
define(LANG_LITERATURE_TITLE, "Titolo");
define(LANG_LITERATURE_TITLES, "Titoli");
define(LANG_LITERATURE_SHOW_ALL,"Mostra tutto");
# literature end

# video
define(LANG_VIDEO_ALLTITLES,"Tutti i titoli");
define(LANG_VIDEO_VIDEOS,"Video");
define(LANG_VIDEO_AUDIO,"Audio");
define(LANG_AUDIO_CR,"Catalogo ragionato:");
define(LANG_VIDEO_RUNTIME,"Tempo di esecuzione");
define(LANG_VIDEO_WATCH1,"Guarda questo video in formato Quicktime");
define(LANG_VIDEO_WATCH2," Guarda questo video in formato Flash");
define(LANG_VIDEO_WATCH3,"Quicktime");
define(LANG_VIDEO_WATCH4,"Flash");
define(LANG_VIDEO_BACKTOVID,"Torna ai video");
define(LANG_VIDEO_TAB_INFO, "Informazione");
define(LANG_VIDEO_TAB_IMAGES, "Immagini");
define(LANG_VIDEO_ARTWORK_MENTIONED, "Quest'opera d'arte è mostrato o menzionato nei sequenti video:");
define(LANG_VIDEO_EXHIBITION_MENTIONED, "Questa mostra è rappresentata anche nei seguenti video:");
define(LANG_VIDEO_IN_SECTION1, "Ci sono");
define(LANG_VIDEO_IN_SECTION2, "video in questa sezione:");
# video end

# attualità
define(LANG_NEWS_CURRENT,"In corso");
define(LANG_NEWS_UPCOMING,"Futuro");
define(LANG_NEWS_RESULTS,"Risultati");
define(LANG_NEWS_PUBLICATIONS,"Pubblicazioni");
define(LANG_NEWS_TALKS,"Conferenza");
define(LANG_NEWS_AUCTIONS,"Aste");
define(LANG_NEWS_EXHIBITIONS,"Mostre");
define(LANG_NEWS_ARCHIVE,"Archivio");
define(LANG_NEWS_NO_AUCTIONS,"Al momento non ci sono opere all'asta");
define(LANG_NEWS_NOCURRENT,"Nessuna attualità");
define(LANG_NEWS_NO_TALKS,"No information currently available");
define(LANG_NEWS_NOUPCOMING," Nessuna attualità ");
define(LANG_NEWS_NOARCHIVE,"Nessun archivio");
define(LANG_NEWS_NOPUBLICATIONS,"Nessuna pubblicazione recente");
define(LANG_NEWS_DOWNLOADLEAFLET,"Scarica il dépliant");
define(LANG_NEWS_ALSO_VISIT,"Contattaci anche su");
define(LANG_NEWS_SHOW_ALL,"Mostra tutto");
define(LANG_NEWS_DISCLIMER,"Menzioni legali");
define(LANG_NEWS_DISCLIMER_TEXT1,"I contenuti di questo sito sono a scopo puramente informativo. Le informazioni sono fornite da <a href='/' title='www.gerhard-richter.com'>www.gerhard-richter.com</a>; sebbene ci impegniamo ad aggiornare e verificare l’esattezza dei contenuti, non offriamo alcuna garanzia, implicita od esplicita, di esaustività, affidabilità, precisione per quanto riguarda il sito o le informazioni contenute in esso, qualunque sia lo scopo. L’affidamento che farete a queste informazioni è sotto la vostra responsabilità. In nessun caso saremo responsabili di eventuali perdite connesse o derivanti dall’uso di questo sito.");
define(LANG_NEWS_DISCLIMER_TEXT2,"Questo sito non è e non pretende di essere un catalogo ragionato; la presenza o meno di opere su questo sito non costituisce una garanzia che tale lavoro sia o non sia un’opera autentica di Gerhard Richter (e come tale non può essere fatta valere");
define(LANG_NEWS_UPCOMING_FOR_AUCTION,"Aste");
define(LANG_NEWS_UPCOMING_FOR_AUCTION_DESC,"Le seguenti opere saranno prossimamente all'asta:");
define(LANG_NEWS_REACEN_AUCTION_RES,"Results");
define(LANG_NEWS_REACEN_AUCTION_RES_DESC,"Le seguenti opere sono state vendute all'asta negli ultimi 6 mesi:");
define(LANG_NEWS_NEW_PUBLICATIONS,"Nuove pubblicazioni");
define(LANG_NEWS_PUBLICATIONS_COUNT1,"There currently are");
define(LANG_NEWS_PUBLICATIONS_COUNT2,"new publications");
define(LANG_NEWS_NEW_TALKS,"Conferenza");
define(LANG_NEWS_AUCTIONS_AUCTIONHOUSE,"Casa d'aste");
define(LANG_NEWS_AUCTIONS_DATE,"Date");
define(LANG_NEWS_RETURN_TO_NEWS_PAGE,"Return to news page");
# news end

#search box tooltip start
define(LANG_SEARCH_TIP_TITLE,"Cerca aiuto");
define(LANG_SEARCH_TIP_TEXT,"<p class=\"tip\"><span>Numero:</span> lista di numeri usando le virgole eg. \"5,137,211\" o un intervallo di numeri usando i due punti eg.\"5:20\".</p><p class=\"tip\"><span>Year:</span> un singolo anno, o un intervallo di anni.</p><p class=\"tip\"><span>Data:</span> una singola data, o un intervallo di date.</p><p class=\"tip\"><span>Misura:</span> una misura esatta, o un intervallo usando i campi </p> &#8216;da&#8217; e &#8216;a&#8217");
#search box tooltip end

#search box EXH tooltip start
define(LANG_SEARCH_EXH_TIP_TITLE,"Cerca aiuto");
define(LANG_SEARCH_EXH_TIP_TEXT,"<p class=\"tip\"><span>Luogo:</span> Luogo della mostra</p> <p class=\"tip\"><span>Parola chiave:</span> Cerca per parola chiave</p> <p class=\"tip\"><span>Anno:</span> Un singolo anno, o un intervallo di anni </p>");
#search box EXH tooltip end

#search box QUOTES tooltip start
define(LANG_SEARCH_QUOTES_TIP_TITLE,"Cerca aiuto");
define(LANG_SEARCH_QUOTES_TIP_TEXT,"<p class=\"tip\"><span>Parola chiave:</span> Cerca per parola chiave</p> <p class=\"tip\"><span>Anno:</span> Un singolo anno, o un intervallo di anni </p> ");
#search box QUOTES tooltip end

#search box LIT tooltip start
define(LANG_SEARCH_LIT_TIP_TITLE," Cerca aiuto ");
define(LANG_SEARCH_LIT_TIP_TEXT,"<p class=\"tip\"><span>Autore:</span> Cerca per autore</p> <p class=\"tip\"><span>Titolo:</span> Cerca per titolo</p><p class=\"tip\"><span>Paola chiave:</span> Cerca per parola chiave</p><p class=\"tip\"><span>Categoria:</span> Cerca per categoria</p><p class=\"tip\"><span>Lingua:</span> Cerca per lingua</p><p class=\"tip\"><span>Anno:</span> Cerca per un singolo anno, o un intervallo di anni </p>");
#search box LIT tooltip end

#search box ARTICLES tooltip start
define(LANG_SEARCH_ARTICLES_TIP_TITLE,"Cerca aiuto");
define(LANG_SEARCH_ARTICLES_TIP_TEXT,"<p class=\"tip\"><span>Autore:</span> Cerca per autore </p> <p class=\"tip\"><span>Titolo:</span> Cerca per titolo </p><p class=\"tip\"><span>Parola chiave:</span> Cerca per parola chiave </p><p class=\"tip\"><span>Anno:</span> Cerca per un singolo anno, o un intervallo di anni </p>");
#search box ARTICLES tooltip end

#search box VIDEOS tooltip start
define(LANG_SEARCH_VIDEOS_TIP_TITLE,"SCerca aiuto");
define(LANG_SEARCH_VIDEOS_TIP_TEXT,"<p class=\"tip\"><span>Parola chiave:</span> Cerca per parola chiave</p>");
#search box VIDEOS tooltip end

#zoomer start
define(LANG_ZOOMER_OPEN,"Guarda in dettaglio");
define(LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE,"See Microsite");
#zoomer end

#virtual gallery
define(LANG_VIRTUALGALLERY_OPEN," Guarda queste immagini nella nostra galleria in formato Flash");
define(LANG_VIRTUALGALLERY,"Galleria virtuale");
#end

# MICROSITE
define(LANG_MICROSITE_TITLE_SINBAD,"Sinbad");
define(LANG_MICROSITE_SHOW,"mostra");
define(LANG_MICROSITE_PER_PAGE,"per pagina");
define(LANG_MICROSITE_SEE_ALL,"tutto");
define(LANG_MICROSITE_SEARCH_EDITION,"cerca per edizione");
define(LANG_MICROSITE_ABOUT,"A proposito");
define(LANG_MICROSITE_SEARCH,"cerca");
define(LANG_MICROSITE_SINBAD_TITLE,"Edizioni Sinbad Gerhard Richter");
define(LANG_MICROSITE_SINBAD_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_JS_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_VIEW_AS_PAIR,"Guardali a due a due");
define(LANG_MICROSITE_SEARCH_HELP_TITLE,"Cerca Aiuto");
define(LANG_MICROSITE_SEARCH_HELP_TEXT,"<em>Singola edizione =</em> scrivi il numero di edizione (eg; 23) <p><em>Edizioni multiple =</em> scrivi ogni numero di edizione separato da una virgola (eg; 23, 32, 55)</p> <p><em>Gruppi =</em> scrivi il numero di inizio e di fine del gruppo, separato da due punti (eg; 23:55)</p>");
define(LANG_MICROSITE_EDITIONS_CR,"Edizioni CR");
define(LANG_MICROSITE_IMAGE,"Immagine");
define(LANG_MICROSITE_OF,"di");
define(LANG_MICROSITE_NOVEMBER_FOOTER_1,"Per ulterior information su Gerhard Richter e la sua opera,<br />si prega di visitare il sito");
define(LANG_MICROSITE_NOVEMBER_FOOTER_2,"e conoscere la sua vita e il lavoro.");
# MICROSITE END

# OVERPAINTED PHOTOGRAPHS FORM
define(LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS,"Catalogo fotografie sovraverniciate");
define(LANG_OPP_FORM_CATALOGUE_RAISONNE,"Un catalogo ragionato è un sistematico e completo testo di riferimento scientifico che ha lo scopo di stabilire un corpus preciso di opere autentiche. Ogni opera, riconosciuta per essere stata eseguita da quel particolare artista, è illustrata, documentata e descritta. Un catalogo ragionato fornisce delle informazioni circa il titolo, la data, la tecnica, le dimensioni, la firma, la provenienza, la cronistoria delle mostre e la bibliografia. Assegna inoltre ad ogni opera un numero di riferimento in modo permanente.");
//define(LANG_OPP_FORM_1,"A <span style='border-bottom:1px dotted #fff;' class='help' title='".LANG_OPP_FORM_CATALOGUE_RAISONNE."'> Il catalogo ragionato</span> delle Fotografie sovraverniciate di Gerhard Richter sarà pubblicato nel 2015.");
define(LANG_OPP_FORM_1,"Un catalogo completo delle fotografie sovraverniciate di Gerhard Richter sarà pubblicato nel 2017.");
define(LANG_OPP_FORM_1_1,"Per ulteriori informazioni o per contribuire al catalogo, clicca ");
define(LANG_OPP_FORM_1_2,"qui");
define(LANG_OPP_FORM_2,"Gerhard Richter ha creato un vasto numero di Fotografie sovraverniciate durante la sua carriera e continua a produrne di nuove. Il catalogo documenterà questo aspetto dell’opera di Gerhard Richter, ancora piuttosto sconosciuto dagli specialisti del settore artistico e dal grande pubblico. Mostrerà la varietà del talento di Richter in questa tecnica.");
define(LANG_OPP_FORM_3,"Il catalogo delle Fotografie sovraverniciate di Gerhard Richter mira ad essere una pubblicazione esaustiva che comprenderà ed enumererà tutte le opere note. Stabilirà un inventario, il più esatto possibile, delle Fotografie sovraverniciate ed autentificate da Gerhard Richter.");
define(LANG_OPP_FORM_4,"Per questo motivo stiamo procedendo a raccogliere i seguenti dati per ogni fotografia dipinta conosciuta:");
define(LANG_OPP_FORM_5,"titolo");
define(LANG_OPP_FORM_6,"anno");
define(LANG_OPP_FORM_7,"tecnica");
define(LANG_OPP_FORM_8,"dimensioni");
define(LANG_OPP_FORM_9,"luogo di provenienza");
define(LANG_OPP_FORM_10,"cronistoria delle mostre");
define(LANG_OPP_FORM_11,"riferimenti bibliografici");
define(LANG_OPP_FORM_12,"altre informazioni utili");
define(LANG_OPP_FORM_13,"Saremmo molto grati di ricevere queste informazioni da parte di quei collezionisti privati e pubblici che desiderano includere le loro Fotografie sovraverniciate nel catalogo. Completate gentilmente il modulo online o contattateci per mail <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>email.</a>");
define(LANG_OPP_FORM_14,"Sebbene speriamo che ci possiate fornire il maggior numero di informazioni possibili, ci rendiamo conto che certi dati possono rivelarsi personali, privati e delicati; non sentitevi in nessun caso obbligati a riempire le sezioni che ritenete inopportune.");
define(LANG_OPP_FORM_15,"Vi assicuriamo che tutte le informazioni saranno trattate nel modo più discreto e confidenziale possibile. Tutte le richieste di anonimato saranno completamente rispettate.");
define(LANG_OPP_FORM_16,"Vi ringraziamo per il tempo dedicato e per il vostro contributo al progetto.");
define(LANG_OPP_FORM_17,"Formulario di presentazione delle opere");
define(LANG_OPP_FORM_18,"Le informazioni seguenti saranno prese in considerazione durante il lavoro di preparazione del catalogo  delle Fotografie sovraverniciate. Questo modulo è stato pensato per dei fini di ricerca e di archiviazione e la sua compilazione non garantirà per forza un riferimento nel catalogo.");
define(LANG_OPP_FORM_19,"Se non siete in grado di rispondere a una domanda, vi preghiamo di lasciare il campo vuoto. Se avete bisogno di aiuto per completare il modulo, non esitate a contattarci <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>email</a>.");
define(LANG_OPP_FORM_20,"Titolo");
define(LANG_OPP_FORM_21_1,"gg/mm/aaaa");
define(LANG_OPP_FORM_21,"Data");
define(LANG_OPP_FORM_22,"Tecnica");
define(LANG_OPP_FORM_23,"Olio su fotografia a colori");
define(LANG_OPP_FORM_24,"Olio su fotografia in bianco e nero");
define(LANG_OPP_FORM_25,"Smalto su fotografia a colori ");
define(LANG_OPP_FORM_26,"Smalto su fotografia in bianco e nero ");
define(LANG_OPP_FORM_26_1,"Oil on gelatin silver print");
define(LANG_OPP_FORM_27,"Dimensioni della fotografia (h &#215; l)");
define(LANG_OPP_FORM_28,"cm");
define(LANG_OPP_FORM_29,"inches");
define(LANG_OPP_FORM_30,"Dimensioni della cornice (h &#215; l)");
define(LANG_OPP_FORM_31," Vi preghiamo di indicare come desiderate fare apparire la menzione nel catalogo. Se preferite rimanere anonimi, vi preghiamo di specificare “collezione privata”, aggiungendo il luogo, se desiderate che questo compaia.");
define(LANG_OPP_FORM_32,"Menzione / courtesy");
define(LANG_OPP_FORM_33,"Serie / edizione");
define(LANG_OPP_FORM_35,"Firenze (edition)");
define(LANG_OPP_FORM_36,"Florence (series)");
define(LANG_OPP_FORM_37,"Grauwald");
define(LANG_OPP_FORM_38,"Museum Visit");
define(LANG_OPP_FORM_38_1,"Sils");
define(LANG_OPP_FORM_38_2,"Wald");
define(LANG_OPP_FORM_38_3,"128 Fotos von einem Bild (Halifax 1978)");
define(LANG_OPP_FORM_38_4,"8.2.92");
define(LANG_OPP_FORM_39,"Serie / numero d’edizione <br />(se applicabile)");
define(LANG_OPP_FORM_40_1,"Fronte:");
define(LANG_OPP_FORM_40_1_1,"Firmato");
define(LANG_OPP_FORM_40_1_2,"Datato");
define(LANG_OPP_FORM_40_1_3,"Numerato");
define(LANG_OPP_FORM_40_1_4,"Iscritto");
define(LANG_OPP_FORM_40_2,"Retro:");
define(LANG_OPP_FORM_40_2_1,"Firmato");
define(LANG_OPP_FORM_40_2_2,"Datato");
define(LANG_OPP_FORM_40_2_3,"Numerato");
define(LANG_OPP_FORM_40_2_4,"Iscritto");
define(LANG_OPP_FORM_41,"sulla cornice");
define(LANG_OPP_FORM_41_1,"on backing board");
define(LANG_OPP_FORM_42,"sulla fotografia");
define(LANG_OPP_FORM_42_1,"retro");
define(LANG_OPP_FORM_43,"in alto a sinistra");
define(LANG_OPP_FORM_44,"in alto al centro");
define(LANG_OPP_FORM_45,"in alto a destra");
define(LANG_OPP_FORM_46,"centro sinistra");
define(LANG_OPP_FORM_47,"in mezzo al centro");
define(LANG_OPP_FORM_48,"centro destra");
define(LANG_OPP_FORM_49,"in basso a sinistra");
define(LANG_OPP_FORM_50,"in basso al centro");
define(LANG_OPP_FORM_51,"in basso a destra");
define(LANG_OPP_FORM_53,"Altre registrazioni");
define(LANG_OPP_FORM_54,"Note / etichette dietro");
define(LANG_OPP_FORM_55,"Carica l’immagine");
define(LANG_OPP_FORM_55_1,"è possibile caricare piu' immagini");
define(LANG_OPP_FORM_56,"Vi saremmo molto grati se poteste inviarci immagini in formato digitale con una risoluzione di 300dpi (e se fosse possibile includendo la parte dell’opera con la firma dell’artista).");
define(LANG_OPP_FORM_57,"Formati consigliati: JPEG, TIFF, PSD, PNG");
define(LANG_OPP_FORM_58,"Copyright / credito fotografico");
define(LANG_OPP_FORM_59,"Luogo di provenienza");
define(LANG_OPP_FORM_60,"Comprato da");
define(LANG_OPP_FORM_61,"Data di acquisto");
define(LANG_OPP_FORM_62,"Cronistoria delle mostre");
define(LANG_OPP_FORM_62_1,"Exhibition history other");
define(LANG_OPP_FORM_63,"Cronistoria delle pubblicazioni");
define(LANG_OPP_FORM_63_1,"Publication history other");
define(LANG_OPP_FORM_64,"Dettagli personali");
define(LANG_OPP_FORM_65,"Nome");
define(LANG_OPP_FORM_66,"Cognome");
define(LANG_OPP_FORM_67,"Email");
define(LANG_OPP_FORM_68,"Indirizzo");
define(LANG_OPP_FORM_68_1,"Codice postale");
define(LANG_OPP_FORM_68_2,"Nazione");
define(LANG_OPP_FORM_69,"Telefono");
define(LANG_OPP_FORM_70,"Altri commenti");
define(LANG_OPP_FORM_71,"campi obbligatori");
define(LANG_OPP_FORM_72,"Inviando questo modulo il proprietario certifica che tutte le informazioni fornite sono esatte e che nessun dato concernente l’opera, la sua storia o appartenenza è stato volontariamente omesso.");
define(LANG_OPP_FORM_73,"Il proprietario ha preso coscienza del fatto che l’invio del modulo non garantisce l’inclusione dell’opera nel catalogo.");
define(LANG_OPP_FORM_74,"Invia il modulo");
define(LANG_OPP_FORM_75,"Conferma l’invio!");
define(LANG_OPP_FORM_75_2,"Please fill in all required fields!");
define(LANG_OPP_FORM_75_3,"Verification characters you entered where not correct please try again!");
define(LANG_OPP_FORM_76,"Il modulo è stato inviato.");
define(LANG_OPP_FORM_77,"Grazie.");
define(LANG_OPP_FORM_77_2,"Stampa i dati");

# exhibition list
define(LANG_OPP_FORM_78_9_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_78_9_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_78_9_D,", maggio &#8211; giugno 2016");
define(LANG_OPP_FORM_78_8_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_78_8_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_78_8_D,", novembre &#8211; dicembre 2015");
define(LANG_OPP_FORM_78_7_T,"Gerhard Richter. Das kleine Format");
define(LANG_OPP_FORM_78_7_L,", Galerie Schwarzer, Dusseldorf");
define(LANG_OPP_FORM_78_7_D,", aprile &#8211; giugno 2015");
define(LANG_OPP_FORM_78_6_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_6_L,", The Lone T Art Space, Milan");
define(LANG_OPP_FORM_78_6_D,", aprile &#8211; maggio 2015");
define(LANG_OPP_FORM_78_5_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_78_5_L,", Hannah Hoffman Gallery, Los Angeles");
define(LANG_OPP_FORM_78_5_D,", marzo &#8211; aprile 2015");
define(LANG_OPP_FORM_78_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_4_L,", Repetto Gallery, London");
define(LANG_OPP_FORM_78_4_D,", Febbraio &#8211; Marzo 2015");
define(LANG_OPP_FORM_78_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_3_L,", Marian Goodman Gallery, London");
define(LANG_OPP_FORM_78_3_D,", ottobre &#8211; dicembre 2014");
define(LANG_OPP_FORM_78_1_T,"What is a Photograph");
define(LANG_OPP_FORM_78_1_L,", International Center of Photography, New York");
define(LANG_OPP_FORM_78_1_D,", gennaio &#8211; maggio 2014");
define(LANG_OPP_FORM_78_2_T,"Purer Zufall. Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_78_2_L,", Sprengel Museum Hannover, Hanover");
define(LANG_OPP_FORM_78_2_D,", maggio &#8211; settembre 2013");
define(LANG_OPP_FORM_78_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_L,", Beirut Art Center, Beirut");
define(LANG_OPP_FORM_78_D,", aprile &#8211; giugno 2012");
define(LANG_OPP_FORM_79_T,"Gerhard Richter &#8211; Arbeiten 1968-2008");
define(LANG_OPP_FORM_79_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_79_D,", novembre 2011 &#8211; gennaio 2012");
define(LANG_OPP_FORM_80_T,"Editionen und übermalte Fotografien");
define(LANG_OPP_FORM_80_L,", Wolfram Völcker Fine Art, Berlin");
define(LANG_OPP_FORM_80_D,", settembre &#8211; ottobre 2011");
define(LANG_OPP_FORM_81_T,"De-Natured: German Art From Joseph Beuys to Martin Kippenberger");
define(LANG_OPP_FORM_81_L,", Ackland Art, Museum, Chapel Hill");
define(LANG_OPP_FORM_81_D,", aprile &#8211; luglio 2011");
define(LANG_OPP_FORM_82_T,"Gerhard Richter “New Overpainted Photographs“");
define(LANG_OPP_FORM_82_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_82_D,", febbraio &#8211; marzo 2010");
define(LANG_OPP_FORM_82_1_T,"Best of Fifty Years");
define(LANG_OPP_FORM_82_1_L,", Kunstverein Wolfsburg");
define(LANG_OPP_FORM_82_1_D,", novembre 2009 &#8211; febbraio 2010");
define(LANG_OPP_FORM_83_T,"Photo España 2009: Gerhard Richter, Fotografías pintadas");
define(LANG_OPP_FORM_83_L,", Fundación Telefónica, Madrid");
define(LANG_OPP_FORM_83_D,", giugno 2009");
define(LANG_OPP_FORM_84_T,"The Photographic Object");
define(LANG_OPP_FORM_84_L,", Photographers’ Gallery, London");
define(LANG_OPP_FORM_84_D,", aprile &#8211; giugno 2009");
define(LANG_OPP_FORM_85_T,"Gerhard Richter &#8211; Übermalte Fotografien &#8211; Photographies Peintes");
define(LANG_OPP_FORM_85_L,", Centre de la photographie, Geneva");
define(LANG_OPP_FORM_85_D,", febbraio  &#8211;  maggio 2009");
define(LANG_OPP_FORM_86_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_86_L,", Museum Morsbroich, Leverkusen");
define(LANG_OPP_FORM_86_D,", ottobre 2008 &#8211; gennaio 2009");
define(LANG_OPP_FORM_86_1_T,"Gerhard Richter “New Works“");
define(LANG_OPP_FORM_86_1_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_86_1_D,", novembre &#8211; dicembre 2005");
define(LANG_OPP_FORM_87_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_87_L,", Anthony Meier Fine Arts, San Francisco");
define(LANG_OPP_FORM_87_D,", giugno &#8211; agosto 2005");
define(LANG_OPP_FORM_88_T,"Gerhard Richter &#8211; Editionen 1968 &#8211; 2004");
define(LANG_OPP_FORM_88_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_88_D,", giugno &#8211; july 2005");
define(LANG_OPP_FORM_89_T,"Attack: Attraction Painting/Photography");
define(LANG_OPP_FORM_89_L,", Marcel Sitcoske Gallery, San Francisco");
define(LANG_OPP_FORM_89_D,", dicembre 2002 &#8211; febbraio 2003");
define(LANG_OPP_FORM_90_T,"Gerhard Richter");
define(LANG_OPP_FORM_90_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_90_D,", dicembre 2002 &#8211; gennaio 2003");
define(LANG_OPP_FORM_91_T,"Gerhard Richter &#8211; Firenze");
define(LANG_OPP_FORM_91_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_91_D,", giugno &#8211; agosto 2002");
define(LANG_OPP_FORM_92_T,"Gerhard Richter &#8211; Editionen 1969 &#8211; 1998");
define(LANG_OPP_FORM_92_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_92_D,", marzo &#8211; maggio 2001");
define(LANG_OPP_FORM_93_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_93_L,", Galerie Fred Jahn, Munich");
define(LANG_OPP_FORM_93_D,", ottobre 2000");
define(LANG_OPP_FORM_93_1_T,"Gerhard Richter &#8211; Bilder 1972 &#8211; 1996");
define(LANG_OPP_FORM_93_1_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_1_D,", maggio &#8211; luglio 1998");
define(LANG_OPP_FORM_93_2_T,"Gerhard Richter");
define(LANG_OPP_FORM_93_2_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_2_D,", gennaio &#8211; marzo 1994");
define(LANG_OPP_FORM_93_3_T,"Gerhard Richter Part II: Painting, Mirror Painting, Watercolour, Photograph, Print");
define(LANG_OPP_FORM_93_3_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_3_D,", marzo 1996");
define(LANG_OPP_FORM_93_4_T,"Gerhard Richter &#8211; Editionen 1967 &#8211; 1993");
define(LANG_OPP_FORM_93_4_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_4_D,", marzo 1994");
define(LANG_OPP_FORM_93_5_T,"Gerhard Richter: Sils");
define(LANG_OPP_FORM_93_5_L,", Nietzsche-Haus, Sils-Maria");
define(LANG_OPP_FORM_93_5_D,", marzo 1993");
define(LANG_OPP_FORM_94_T,"altre mostre");
define(LANG_OPP_FORM_94_L,"");
define(LANG_OPP_FORM_94_D,"");

# publications list
define(LANG_OPP_FORM_95_6_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_95_6_L,". Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_95_6_D,", 2015");
define(LANG_OPP_FORM_95_5_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_95_5_L," (Buchloh, Benjamin H.D., Schwarz, Dieter). Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_95_5_D,", 2016");
define(LANG_OPP_FORM_95_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_95_4_L," (Barbero, Luca Massimo). Nuvole Rosse");
define(LANG_OPP_FORM_95_4_D,", 2015");
define(LANG_OPP_FORM_95_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_95_3_L," (Buchloh, Benjamin H.D., Schwarz, Dieter, Storr, Robert). London: Marian Goodman Gallery");
define(LANG_OPP_FORM_95_3_D,", 2014");
define(LANG_OPP_FORM_95_2_T,"What is a Photograph");
define(LANG_OPP_FORM_95_2_L," (Squiers, Carol, Baker, George, Batchen, Geoffrey, Steyerl, Hito). New York: Prestel");
define(LANG_OPP_FORM_95_2_D,", 2014");
define(LANG_OPP_FORM_95_T,"Purer Zufall &#8211; Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_95_L," (Krempel, Ulrich, Rist, Annerose, Schwarz, Isabelle). Hanover: Sprengel Museum");
define(LANG_OPP_FORM_95_D,", 2013");
define(LANG_OPP_FORM_95_1_T,"Beirut");
define(LANG_OPP_FORM_95_1_L," (Borchardt-Hume, Achim, Joreige, Lamia, Dagner, Sandra). Beirut: Beirut Art Center; London: Heni Publishing; Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_95_1_D,", 2012");
define(LANG_OPP_FORM_96_T,"Best of Fifty Years");
define(LANG_OPP_FORM_96_L," (Justin Hoffmann/Kunstverein Wolfsburg). Wolfsburg: Kunstverein Wolfsburg");
define(LANG_OPP_FORM_96_D,", 2010");
define(LANG_OPP_FORM_97_T,"Gerhard Richter. Obrist &#8211; O’Brist");
define(LANG_OPP_FORM_97_L," (Obrist, Hans Ulrich). Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_97_D,", 2009");
define(LANG_OPP_FORM_97_1_T,"Gerhard Richter. Overpainted Photographs");
define(LANG_OPP_FORM_97_1_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_97_1_D,", 2008");
define(LANG_OPP_FORM_98_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_98_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_98_D,", 2008");
define(LANG_OPP_FORM_99_T,"Gerhard Richter.");
define(LANG_OPP_FORM_99_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_99_D,", 2005");
define(LANG_OPP_FORM_100_T,"City Life");
define(LANG_OPP_FORM_100_L," (Cora, Bruno, Restagno, Enzo, Gori, Giuliano). Prato: Gli Ori");
define(LANG_OPP_FORM_100_D,", 2002");
define(LANG_OPP_FORM_100_1_T,"Gerhard Richter.");
define(LANG_OPP_FORM_100_1_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_1_D,", 2002");
define(LANG_OPP_FORM_100_2_T,"Sils");
define(LANG_OPP_FORM_100_2_L," (eds. Obrist, Hans Ulrich, Bloch, Peter Andre). Munich: Oktagon, 1992; New York: D. A. P., Distributed Art Publishers");
define(LANG_OPP_FORM_100_2_D,", 2002 ");
define(LANG_OPP_FORM_100_3_T,"Florence");
define(LANG_OPP_FORM_100_3_L," (Elger, Dietmar). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_100_3_D,", 2001");
define(LANG_OPP_FORM_100_4_T,"Gerhard Richter, Part I: New Painting; Part II, Part II: Painting, Mirror Painting, Watercolour, Photograph, Print.");
define(LANG_OPP_FORM_100_4_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_4_D,", 1996");
define(LANG_OPP_FORM_100_5_T,"altri pubblicazioni");
define(LANG_OPP_FORM_100_5_L,"");
define(LANG_OPP_FORM_100_5_D,"");

define(LANG_OPP_FORM_103,"Vorrei ricevere copia dei dati per email");
define(LANG_OPP_FORM_104,"Numero di riferimento");
# END OVERPAINTED PHOTOGRAPHS FORM

# TABLE
define(LANG_TABLE_DESC_PUBLICATION_RELATED,"correlata");
define(LANG_TABLE_DESC_PUBLICATION,"pubblicazione");
define(LANG_TABLE_DESC_PUBLICATIONS,"pubblicazioni");
define(LANG_TABLE_TITLE,"Titolo");
define(LANG_TABLE_FURTHER_ARTWORKS_IN_LOT,"Further artworks in this lot:");
# END TABLE

# Search
define(LANG_SEARCH_SHOW_MORE_OPTIONS,"Show more options");
# END Search

# share button
define(LANG_SHARE_THIS_PAGE,"Share this page");
define(LANG_SHARE_EMAIL_SUBJECT,"Visit this page on gerhard-richter.com");
define(LANG_SHARE_EMAIL_BODY_1,"Dear friend,");
define(LANG_SHARE_EMAIL_BODY_2,"I would like to share the following page with you:");
define(LANG_SHARE_EMAIL_BODY_3,"Kind regards,");
define(LANG_SHARE_EMAIL,"Email");
# END share button

# Mobile site specific
define(LANG_MOBILE_SEARCH_FIND,"Find");
# END Mobile site specific
?>
