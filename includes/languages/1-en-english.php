<?php
# meta info
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION_HOME,"Find out more about the life and career of Gerhard Richter, one of the most important artists of the 20th and 21st centuries.");
define(LANG_META_DESCRIPTION_ART,"Discover Richter's versatile oeuvre, ranging from oils on canvas, works on paper, overpainted photographs and Atlas, his collection of photographs, newspaper cuttings and sketches.");
define(LANG_META_DESCRIPTION_BIOGRAPHY,"The biography sheds light on Richter's life and career. Born in Dresden in 1932, Richter was brought up under Third Reich and received the first part of his artistic education in East Germany. In 1961 he relocated to West Germany, where he developed into one of the most distinguished artists of his time.");
define(LANG_META_DESCRIPTION_CHRONOLOGY,"A concentrated overview of Richter's life and career: important chapters of his private life are mirrored by significant artistic achievements.");
define(LANG_META_DESCRIPTION_QUOTES,"A selection of Richter's quotes offer insight into his way of working, his understanding of art and his view of the world.");
define(LANG_META_DESCRIPTION_EXHIBITIONS,"Research over 50 years of exhibition history of Gerhard Richter's works.");
define(LANG_META_DESCRIPTION_LITERATURE,"Research Richter's artist's books, publications on his oeuvre as well as newspaper articles and online publications.");
define(LANG_META_DESCRIPTION_VIDEOS,"Watch videos on Richter's recent exhibitions, on selected groups of his works and talks.");
define(LANG_META_DESCRIPTION_LINKS,"Find links to articles in newspapers and magazines as well as to galleries dealing with Richter's works.");
define(LANG_META_DESCRIPTION_NEWS,"Current news: ongoing exhibitions, upcoming auctions and recent publications can be found here.");
# END meta info

#PAGE TITLES
if( $_SESSION['kiosk'] ) define(LANG_TITLE_HOME,"Gerhard-Richter.com");
else define(LANG_TITLE_HOME,"Gerhard Richter");
#PAGE TITLES END

# MENU
define(LANG_HEADER_MENU_HOME,"Home");
define(LANG_HEADER_MENU_HOME_TT,"Gerhard Richter home page");
define(LANG_HEADER_MENU_ARTWORK,"Art");
define(LANG_HEADER_MENU_ARTWORK_TT,"Gerhard Richter art including works, atlas, editions, overpainted photographs, watercolours and drawings");
define(LANG_HEADER_MENU_BIOGRAPHY,"Biography");
define(LANG_HEADER_MENU_BIOGRAPHY_TT,"Gerhard Richter life history");
define(LANG_HEADER_MENU_CHRONOLOGY,"Chronology");
define(LANG_HEADER_MENU_CHRONOLOGY_TT,"Gerhard Richter chronology");
define(LANG_HEADER_MENU_QUOTES,"Quotes");
define(LANG_HEADER_MENU_QUOTES_TT,"Gerhard Richter quotes");
define(LANG_HEADER_MENU_EXHIBITIONS,"Exhibitions");
define(LANG_HEADER_MENU_EXHIBITIONS_TT,"Gerhard Richter worldwide exhibitions");
define(LANG_HEADER_MENU_LITERATURE,"Literature");
define(LANG_HEADER_MENU_LITERATURE_TT,"Gerhard Richter books");
define(LANG_HEADER_MENU_VIDEOS,"Videos");
define(LANG_HEADER_MENU_VIDEOS_RELATED,"Related videos");
define(LANG_HEADER_MENU_VIDEOS_TT,"Gerhard Richter videos and interviews");
define(LANG_HEADER_MENU_AUDIO,"Audio");
define(LANG_HEADER_MENU_AUDIO_TT,"Gerhard Richter audios");
define(LANG_HEADER_MENU_MEDIA,"Media");
define(LANG_HEADER_MENU_MEDIA_TT,"Gerhard Richter media");
define(LANG_HEADER_MENU_CONTACT,"Links");
define(LANG_HEADER_MENU_CONTACT_TT,"Gerhard Richter links");
define(LANG_HEADER_MENU_NEWS,"News");
define(LANG_HEADER_MENU_NEWS_TT,"Gerhard Richter news");
define(LANG_HEADER_MENU_SEARCH,"Search");
define(LANG_HEADER_MENU_SEARCH_TT,"Gerhard Richter search");
define(LANG_NEWS,"News");
define(LANG_HEADER_MENU_ADMIN,"Admin");
# MENU END

#LEFT SIDE
define(LANG_LEFT_SIDE_PAINTINGS,"Paintings");
define(LANG_LEFT_SIDE_CATEGORIES,"Categories");
define(LANG_LEFT_SIDE_ATLAS,"Atlas");
define(LANG_LEFT_SIDE_EDITIONS,"Editions");
define(LANG_LEFT_SIDE_YEARS,"Years");
define(LANG_LEFT_SIDE_DRAWINGS,"Drawings");
define(LANG_LEFT_SIDE_OILSONPAPER,"Oil on Paper");
define(LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS,"Overpainted Photographs");
define(LANG_LEFT_SIDE_WATERCOLOURS,"Watercolours");
define(LANG_LEFT_SIDE_OTHER,"Other");
define(LANG_LEFT_SIDE_PRINTS,"Prints");
define(LANG_LEFT_SIDE_MICROSITES,"Microsites");
#LEFT SIDE END

#RIGHT SIDE
define(LANG_RIGHT_SEARCH_H1,"Search");
define(LANG_RIGHT_SEARCH_EXH_H1,"Search");
define(LANG_RIGHT_SEARCH_QUOTES_H1,"Search");
define(LANG_RIGHT_SEARCH_VIDEOS_H1,"Search");
define(LANG_RIGHT_SEARCH_SEARCHNOW,"Search now");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE1,"by");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE2,"medium");
define(LANG_RIGHT_SEARCH_BYTITLE1,"by");
define(LANG_RIGHT_SEARCH_BYTITLE2,"title");
define(LANG_RIGHT_SEARCH_BY,"by");
define(LANG_RIGHT_SEARCH_BYPAINTINGS,"CR number (Paintings)");
define(LANG_RIGHT_SEARCH_BYATLAS,"Sheet");
define(LANG_RIGHT_SEARCH_BYEDITION,"CR number (Editions)");
define(LANG_RIGHT_SEARCH_BYDRAWINGS,"CR number (Drawings)");
define(LANG_RIGHT_SEARCH_BYNUMBER,"CR number");
define(LANG_RIGHT_SEARCH_ADVANCED_SEARCH,"Advanced search");
define(LANG_RIGHT_SEARCH_QUICK_SEARCH,"Quick search");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST,"list");
define(LANG_RIGHT_SEARCH_BYNUMBER_RANGE,"range");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE,"Add another");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE,"Remove");
define(LANG_RIGHT_SEARCH_YEARPAINTED1,"by");
define(LANG_RIGHT_SEARCH_YEARPAINTED2,"year");
define(LANG_RIGHT_SEARCH_DATE,"date");
define(LANG_RIGHT_SEARCH_DATE_FROM,"From");
define(LANG_RIGHT_SEARCH_DATE_TO,"To");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_D,"DD");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_M,"MM");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y,"YYYY");
define(LANG_RIGHT_SEARCH_COLOR1,"by");
define(LANG_RIGHT_SEARCH_COLOR2,"Colour");
define(LANG_RIGHT_SEARCH_BYSIZE,"size");
define(LANG_RIGHT_SEARCH_BYSIZEH,"H");
define(LANG_RIGHT_SEARCH_BYSIZEH1,"Height in cm");
define(LANG_RIGHT_SEARCH_BYSIZEW,"W");
define(LANG_RIGHT_SEARCH_BYSIZEW1,"Width in cm");
define(LANG_RIGHT_SEARCH_BYSIZEMIN,"Min");
define(LANG_RIGHT_SEARCH_BYSIZEMAX,"Max");
define(LANG_RIGHT_SEARCH_BY_COLLECTION,"by <strong>location</strong>");
define(LANG_RIGHT_SEARCH_SEARCHBUTTON,"Search");
define(LANG_RIGHT_SEARCH_SEARCHRESET,"Reset");
define(LANG_RIGHT_SEARCH_EXH_LOCATION,"Location");
define(LANG_RIGHT_SEARCH_EXH_TITLE,"Title");
define(LANG_RIGHT_SEARCH_EXH_KEYWORD,"Keyword");
define(LANG_RIGHT_SEARCH_QUOTES_KEYWORD,"Keyword");
define(LANG_RIGHT_SEARCH_VIDEOS_KEYWORD,"Keyword");
define(LANG_RIGHT_SEARCH_QUOTES_SEARCHALL,"Browse all quotes");
define(LANG_RIGHT_SEARCH_LIT_AUTHOR,"Author");
define(LANG_RIGHT_SEARCH_LIT_TITLE,"Title");
define(LANG_RIGHT_SEARCH_LIT_KEYWORD,"Keyword");
define(LANG_RIGHT_SEARCH_LIT_CATEGORY,"Category");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT,"Select...");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE,"Language");
define(LANG_RIGHT_FEATURED_TITLES,"Microsites");
define(LANG_RIGHT_FEATURED_TITLES_SNOWWHITE,"Snow White");
define(LANG_RIGHT_FEATURED_TITLES_WARCUT,"War Cut II");
define(LANG_RIGHT_FEATURED_TITLES_FIRENZE,"Firenze");
define(LANG_RIGHT_FEATURED_TITLES_4900COLOURS,"4900 Colours");
define(LANG_RIGHT_FEATURED_TITLES_SINBAD,"Sinbad");
define(LANG_RIGHT_FEATURED_TITLES_ELBE,"Elbe");
define(LANG_RIGHT_FEATURED_TITLES_COLOGNE_CATHEDRAL,"Cologne cathedral");
define(LANG_RIGHT_TIMELINE,"<span class='related'>Timeline</span><br />Explore the artist's career.");
define(LANG_RIGHT_TIMELINE_INTER,"Interactive timeline");
define(LANG_RIGHT_VIRTUALEXH,"<span class='related'>Virtual</span><br />Exhibitions");
define(LANG_RIGHT_FETURED_TITLES,"Featured Titles");
define(LANG_RIGHT_VIDEO,"To view these videos you will need to have Flash installed on your computer.");
define(LANG_RIGHT_VIDEO_DOWN,"Download");
define(LANG_RIGHT_AUDIO,"To listen to the audio you will need to have Flash installed on your computer.");
define(LANG_RELATED_BLOCK_BROWSE_CLIPS,"All clips");
define(LANG_RELATED_BLOCK_RECENTLY,"Recently Added");
#RIGHT SIDE END

# credits page
define(LANG_CREDITS,"Credits");
define(LANG_CREDITS_TEXT1_1,"The official website of Gerhard Richter");
define(LANG_CREDITS_TEXT1_2,"has been created, and is maintained, by Joe Hage.");
define(LANG_CREDITS_TEXT2_1,"Thanks to Dietmar Elger (author of");
define(LANG_CREDITS_TEXT2_2,"Gerhard Richter, Catalogue Raisonné, Vol. 1, 2011");
define(LANG_CREDITS_TEXT2_3,") for his help and support.");
define(LANG_CREDITS_TEXT3_1,"Thanks to Hubertus Butin for his extensive and detailed information on");
define(LANG_CREDITS_TEXT3_2,"Gerhard Richter's editions.");
define(LANG_CREDITS_TEXT3_3,"");
define(LANG_CREDITS_TEXT4,"Thanks to Amy Dickson for her work on the Chronology.");
# end

# footer
define(LANG_FOOTER_COPYRIGHT,"Gerhard Richter - All Rights Reserved");
define(LANG_FOOTER_CONTACT,"Contact");
define(LANG_FOOTER_CREDITS,"Credits");
define(LANG_FOOTER_DISCLIMER,"Disclaimer");
# footer END

# home page
define(LANG_HOME_TEXT_INTRO1,"Gerhard Richter is an important artist in the twentieth and twenty-first centuries; his work spans nearly five decades. Here, you can view his work and learn about his life. Click on a work below to begin.");
# home END

# artwork page
define(LANG_ARTWORK_PHOTO_PAINTINGS,"Paintings");
define(LANG_ARTWORK_ABSTRACTS,"Abstracts");
define(LANG_ARTWORK_ATLAS,"Atlas");
define(LANG_ARTWORK_ATLAS_SHEET,"Atlas Sheet");
define(LANG_ARTWORK_EDITIONS,"Editions");
define(LANG_ARTWORK_WATERCOLOURS,"Watercolours");
define(LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS,"Overpainted Photographs");
define(LANG_ARTWORK_DRAWINGS,"Drawings");
define(LANG_ARTWORK_OILSONPAPER,"Oil on Paper");
define(LANG_ARTWORK_OTHER,"Other");
define(LANG_ARTWORK_MICROSITES,"Microsites");
define(LANG_ARTWORK_MICROSITES2,"Microsites");
define(LANG_ARTWORK_ARTISTS_BOOKS,"Artist's Books");
define(LANG_ARTWORK_TEXT_H1,"A Half Century of Art");
define(LANG_ARTWORK_TEXT,"Richter officially began painting in 1962. Here we give you access to his various works, ranging from oils on canvas to overpainted photographs, and including the historical reference of photographs, <i>Atlas</i>.");
define(LANG_ARTWORK_TEXT_PAINTINGS1,"Richter officially began painting in 1962. Here we give you access to his various works, comprising photo paintings and abstracts.");
define(LANG_ARTWORK_TEXT_PAINTINGS,"Although the artist intentionally avoids classification, we have placed his paintings into subjective categories for easy viewing.");
define(LANG_ARTWORK_SECTION_PAINTINGS,"Works");
define(LANG_ARTWORK_SECTION_PHOTOPAINTINGS,"Photo Paintings");
define(LANG_ARTWORK_SECTION_ABSTRACTS,"Abstracts");
define(LANG_ARTWORK_SECTION_NOCR,"No CR");
define(LANG_ARTWORK_SECTION_OTHERMEDIA,"Other");
define(LANG_ARTWORK_SECTION_OTHER,"Destroyed paintings");
define(LANG_ARTWORK_TEXT_ATLAS,"View the comprehensive Atlas collection - the newspaper clippings, photos and sketches which are the source material for much of Richter's work.");
define(LANG_ARTWORK_TITLE_OVERPAINTED_PHOTOS,"Catalogue raisonné of Gerhard Richter's overpainted photographs");
define(LANG_ARTWORK_TEXT_OVERPAINTED_PHOTOS,"A catalogue raisonné of Gerhard Richter's overpainted photographs is to be published in 2013. We are therefore researching all information that might be relevant for that book, including information as to ownership and images. We would be very grateful if you could provide us with any such information. To do so, please contact us. Your information will be treated confidentially.");
define(LANG_ARTWORK_CONTACT_OVERPAINTED_PHOTOS,"Send information");
define(LANG_ARTWORK_TEXT_ATLAS_BACKTOATLAS,"back to Atlas");
define(LANG_ARTWORK_TEXT_ATLAS_ASOCIATED,"Associated Works");
define(LANG_ARTWORK_BROWSE_WORKS,"Browse All Paintings");
define(LANG_ARTWORK_BROWSE_OPP,"Browse All Overpainted Photographs");
define(LANG_ARTWORK_BROWSE_ATLAS,"Browse All Atlas");
define(LANG_ARTWORK_BROWSE_EDITIONS,"Browse All Editions");
define(LANG_ARTWORK_ATLAS_ASSOCIATED,"The thumbnails below show you images of the paintings sourced from the particular Atlas sheet.");
define(LANG_ARTWORK_BOOKS_REL_TAB,"Literature");
define(LANG_ARTWORK_BOOKS_REL_COLOUR,"colour");
define(LANG_ARTWORK_BOOKS_REL_BW,"b/w");
define(LANG_ARTWORK_BOOKS_REL_ARTWORK,"Artwork");
define(LANG_ARTWORK_BOOKS_REL_ILLUSTRATED,"Illustrated");
define(LANG_ARTWORK_BOOKS_REL_MENTIONED,"Mentioned");
define(LANG_ARTWORK_BOOKS_REL_DISCUSSED,"Discussed");
define(LANG_ARTWORK_BOOKS_REL_P,"p.");
define(LANG_ARTWORK_BOOKS_REL_PP,"pp.");
define(LANG_ARTWORK_BOOKS_REL_P2,"");
define(LANG_ARTWORK_BOOKS_REL_PP2,"");
define(LANG_ARTWORK_BOOKS_REL_CHINESE,"");
define(LANG_ARTWORK_PHOTOS_TAB,"Photos");
define(LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS,"Installation views:");
define(LANG_ARTWORK_PHOTOS_TAB_DEATILS,"Details");
define(LANG_ARTWORK_THUMB_SALES_HIST_AVAIL,"Sales history available");
define(LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL,"Collection information available");
define(LANG_ARTWORK_BREADCRUMB_ALL,"All Paintings");
define(LANG_ARTWORK_BREADCRUMB_ALL_OPP,"All Photographs");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN,"This artwork was shown in the following exhibitions:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED,"This artwork is included in the following publication:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS,"This artwork is included in the following publications:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS,"Detailed photographs of the artwork:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS,"This artwork is based on an image included in <i>Atlas</i>:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC,"This <i>Atlas</i> sheet includes the source images of the following works:");
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB,"Individual works");
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC,"This edition consists of the following individual works:");
define(LANG_ARTWORK_DESTROYED,"DESTROYED");
# artwork END

# Tabs titles
define(LANG_TAB_TITLE_ARTWORKS,"Artworks");
# Tabs titles END

# contact page
define(LANG_CONTACT_SUBNAV_TEXT_HEADING,"Artist links");
define(LANG_CONTACT_SUBNAV_TEXT_ITEM1,"Write to us");
define(LANG_CONTACT_SUBNAV_TEXT_ITEM2,"Primary Dealers");
define(LANG_CONTACT_SUBNAV_TEXT_ITEM4,"Dealers");
define(LANG_CONTACT_SUBNAV_TEXT_ITEM3,"Poster Sales");
define(LANG_CONTACT_WRITE_TEXT_BRIEF,"If you would like to contact the Gerhard Richter website, please email us using the form below.");
define(LANG_CONTACT_WRITE_INPUTVALUE_EMAIL,"Enter your email");
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT1,"General Enquiry/Comment");
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT2,"Finding Dealers");
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT3,"Buying Poster Editions");
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT4,"Image permissions");
define(LANG_CONTACT_WRITE_INPUTVALUE_MESSAGE,"Enter your message here");
define(LANG_CONTACT_WRITE_INPUTVALUE_SEND,"Send");
define(LANG_CONTACT_POSTERS_LINK," Gerhard Richter Poster Editions");
define(LANG_CONTACT_SHOP_LINK," Gerhard Richter Shop");
define(LANG_CONTACT_EMAIL,"Email address");
define(LANG_CONTACT_SUBJECT_SELECT,"Select a subject");
define(LANG_CONTACT_SUBJECT,"Subject");
define(LANG_CONTACT_FILE,"File");
define(LANG_CONTACT_MESSAGE,"Message");
define(LANG_CONTACT_CAPCHA,"Please enter the following characters into the field below (no uppercase letters or spaces).");
define(LANG_CONTACT_THANX1,"Thank you");
define(LANG_CONTACT_THANX2,"Thank you for contacting the Gerhard Richter website.");
# contact END

# links
define(LANG_LINKS_ARTICLES,"Articles");
define(LANG_LINKS_DEALERS,"Dealers");
define(LANG_LINKS,"Links");
define(LANG_LINKS_ARTICLES_PUBLISHED_IN,"In");
# links END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_xsmall.png");
define(LANG_NO_IMAGE_SMALL,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_small.png");
define(LANG_NO_IMAGE_MEDIUM,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_medium.png");
define(LANG_NO_IMAGE_LARGE,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_medium.png");
define(LANG_NO_IMAGE_XLARGE,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_large.png");
define(LANG_NO_IMAGE_ORIGINAL,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_large.png");
define(LANG_NO_IMAGE_THUMBNAIL,$_SERVER["DOCUMENT_ROOT"]."/g/no-img-available/no_image_thumbnail.png");
# IF NO IMAGE To DISPLAY END

# DIFFERENT
define(LANG_ATLASSHEETS,"Atlas Sheets");
define(LANG_RELEATED,"Related Sections");
define(LANG_RELEATED_ART,"Related Art");
define(LANG_COLLECTION,"Collection");
define(LANG_NOTES,"Notes");
define(LANG_SHOP,"Shop");
define(LANG_SHOP_INFO,"Items available online on");
define(LANG_NEXTDECADE,"next decade");
define(LANG_PREVDECADE,"previous decade");
define(LANG_SELECT,"Select..");
define(LANG_SELECT_FROM,"From");
define(LANG_SELECT_TO,"To");
define(LANG_DIMENSIONS_FROM,"From");
define(LANG_DIMENSIONS_TO,"To");
define(LANG_BACK_TO_ALL_KEYWORDS,"see all");
define(LANG_BACK_TO_ALL,"see all");
define(LANG_SEE_ALL,"See all");
define(LANG_SEE,"See");
define(LANG_CLICK_IMAGE_TO_ENLARGE,"click image to enlarge");
define(LANG_CATALOGUE_RESONE,"Catalogue Raisonn&eacute;");
define(LANG_ATLAS_SHEET,"Atlas Sheet");
define(LANG_GO_TO_ATLAS_SHEET,"Go to Atlas Sheet");
define(LANG_MUSEUM_COLLECTION,"Museum Collection");
define(LANG_SALES_HISTORY,"Sales");
define(LANG_SALES_HISTORY_UPON_REQUEST,"Contact auction house");
define(LANG_SALES_HISTORY_TB_ANNOUNCED,"To be announced");
define(LANG_OF,"of");
define(LANG_IN,"in");
define(LANG_SEARCH_RESULTS_ALL,"Search Results");
define(LANG_SEARCH_PAINTINGS,"Search");
define(LANG_SEARCH_RESULT, "Result");
define(LANG_SEARCH_RESULTS, "Results");
define(LANG_SEARCH_ART, "Art");
define(LANG_SEARCH_BIOGRAPHY, "Biography");
define(LANG_SEARCH_QUOTES, "Quotes");
define(LANG_SEARCH_EXHIBITIONS, "Exhibitions");
define(LANG_SEARCH_LITERATURE, "Literature");
define(LANG_SEARCH_VIDEOS, "Videos");
define(LANG_SEARCH_LINKS, "Links");
define(LANG_SEARCH_NEWS_TALKS, "Talks");
define(LANG_SEARCH_NEWS_AUCTIONS, "Auctions");
define(LANG_SEARCH_CREDITS, "Credits");
define(LANG_SEARCH_DISCLAIMER, "Disclaimer");
define(LANG_SEARCH_CHRONOLOGY, "Chronology");
define(LANG_SEARCH_BASIC_TOP_VALUE_SEARCH, "Quick Search");
define(LANG_SEARCH_SHOW_ALL, "Show all results");
define(LANG_SEARCH_YOUSEARCHEDFOR,"You searched for");
define(LANG_SEARCH_ALLPAINTINGS,"All Paintings");
define(LANG_SEARCH_NUMBER,"Number");
define(LANG_SEARCH_ARTWORKTYPE,"Artwork type");
define(LANG_SEARCH_TITLE,"Title");
define(LANG_SEARCH_LOCATION,"Location");
define(LANG_SEARCH_SHEETNUMBER,"Sheet number");
define(LANG_SEARCH_CRNUMBER,"CR Number");
define(LANG_SEARCH_YEARFROM,"Year from");
define(LANG_SEARCH_YEAR,"Year");
define(LANG_SEARCH_YEARTO,"Year to");
define(LANG_SEARCH_DATE,"Date");
define(LANG_SEARCH_COLOR,"Color");
define(LANG_SEARCH_HEIGHT,"Height");
define(LANG_SEARCH_WIDTH,"Width");
define(LANG_SEARCH_NORESULTS,"Sorry, your search returned no results");
define(LANG_SEARCH_RESULTS1,"Your search returned");
define(LANG_SEARCH_RESULTS2,"result");
define(LANG_SEARCH_RESULTS3,"results");
define(LANG_CATEGORY_DESC_MORE,"more");
define(LANG_CATEGORY_DESC_CLOSE,"close");
define(LANG_SEARCH_RETURNTOTSEARCH,"Return to last search results");
define(LANG_USE_THIS,"Use this drop down to jump to any section");
define(LANG_BACKTOTOP,"back to top");
define(LANG_SHOW,"Show");
define(LANG_ALL,"All");
define(LANG_PERPAGE,"per page");
define(LANG_IMAGES_PERPAGE,"Show");
define(LANG_BACKTOEXH,"Back to exhibition");
define(LANG_GOBACK,"go-back");
define(LANG_SALEHIST_TAB_NOTES,"Notes:");
define(LANG_ESTIMATE,"Estimate");
define(LANG_BOUGHT_IN,"Bought in");
define(LANG_WITHDRAWN,"Withdrawn");
define(LANG_PREMIUM,"(incl. buyer’s premium)");
define(LANG_HAMMER,"(Hammer price)");
define(LANG_UNKNOWN,"Unknown");
define(LANG_LOT,"Lot");
define(LANG_SOLDPR,"Sold For");
define(LANG_SHOWING,"Showing");
define(LANG_BOOKS_OF,"of");
define(LANG_BOOKS,"books");
define(LANG_BOOKS_BY,"Author");
define(LANG_BOOKS_DETAILS,"Details");
define(LANG_BOOKS_CLOSE,"Close");
define(LANG_BOOKS_MORE,"More");
define(LANG_BOOKS_MORE_DETAILS,"More Details");
define(LANG_BOOKS_LANGUAGES,"Languages");
define(LANG_BOOKS_LANGUAGE,"Language");
define(LANG_BOOKS_CATEGORY,"Category");
define(LANG_CONTACT,"If you would like to get in touch with us, just use this form");
define(LANG_CONTACT_UNAVAILABLE,"Currently unavailable. We apologise for any inconvenience.");
define(LANG_CONTACT_UNAVAILABLE2,"If you would like to get in touch with us, please email us at info@gerhard-richter.com");
define(LANG_BOOKS_LANGUAGE_EXHCATALOG,"Exhibitions");
define(LANG_BOOKS_READ_ARTICLE,"Read Article");
define(LANG_COPY_TO_CLIPBOARD,"Copy link to clipboard");
define(LANG_PAINTING_SIZE_PARTS," parts");
define(LANG_SUBMITING_FORM,"submitting form.....");
define(LANG_FOUND,"found");
define(LANG_BOOKS_READ_ESSAY,"Read Essay");
define(LANG_CR,"CR");
define(LANG_CR_EDITIONS,LANG_LEFT_SIDE_EDITIONS." ".LANG_CR);
define(LANG_CR_DRAWINGS,LANG_LEFT_SIDE_DRAWINGS." ".LANG_CR);
# DIFFERENT END

# biograpphy page
define(LANG_BIOGRAPHY,"Biography");
define(LANG_WORK,"Work");
define(LANG_PHOTOS,"Photos");
define(LANG_QUOTES,"Quotes");
define(LANG_QUOTES_NO_QUOTES_FOUND,"No quotes found!");
define(LANG_QUOTES_SOURCE,"SOURCE");
# biograpphy page end

#chronology
define(LANG_CHRONOLOGY_H1_1, "Chronology");
define(LANG_CHRONOLOGY_H1_2, "by Amy Dickson");
define(LANG_AMY_DICKSON_THANKS, "Special thanks to Amy Dickson for her work on the Chronology. This is an edited version of the Chronology by Amy Dickson as it is printed in <i>Gerhard Richter: Panorama</i> (&#169;Tate, 2011. Reproduced by permission of Tate Trustees).");
define(LANG_CHRONOLOGY_LEFT_1930, "1930s");
define(LANG_CHRONOLOGY_LEFT_1940, "1940s");
define(LANG_CHRONOLOGY_LEFT_1950, "1950s");
define(LANG_CHRONOLOGY_LEFT_1960, "1960s");
define(LANG_CHRONOLOGY_LEFT_1970, "1970s");
define(LANG_CHRONOLOGY_LEFT_1980, "1980s");
define(LANG_CHRONOLOGY_LEFT_1990, "1990s");
define(LANG_CHRONOLOGY_LEFT_2000, "2000s");
define(LANG_CHRONOLOGY_LEFT_2010, "2010s");
#chronology end

# exhibition
define(LANG_VIEW_EXHIBITION,"View exhibition");
define(LANG_EXH_ONWARDS,"2020 onwards");
define(LANG_GROUP,"Group");
define(LANG_GROUP_EXH,"Group Exhibition");
define(LANG_GROUP_EXHS,"Group Exhibitions");
define(LANG_SOLO,"Solo");
define(LANG_SOLO_EXH,"Solo Exhibition");
define(LANG_SOLO_EXHS,"Solo Exhibitions");
define(LANG_EXH_DECADES_S,"s");
define(LANG_EXH_SORT_BY,"Sort by");
define(LANG_EXH_SORT_BY_DATE,"Date");
define(LANG_EXH,"Exhibition");
define(LANG_EXH_FOUND,"exhibition");
define(LANG_EXH_FOUND_S,"exhibitions");
define(LANG_EXH_SHOW_ALL,"Show all");
define(LANG_EXH_ARTWORK,"Artworks");
define(LANG_EXH_RELATED_EXHIBITIONS,"Related Exhibitions");
define(LANG_EXH_INST_SHOTS,"Installation Views");
define(LANG_EXH_LITERATURE,"Literature");
define(LANG_EXH_VIDEOS,"Videos");
define(LANG_EXH_GUIDE,"Guide");
define(LANG_EXH_SHOW_ALL_WORKS,"Show all works");
define(LANG_EXH_ARTWORK_TAB_WORKS,"works");
define(LANG_EXH_ARTWORK_TAB_WORK,"work");
define(LANG_EXH_INST_TAB_PHOTOS,"photos");
define(LANG_EXH_INST_TAB_PHOTO,"photo");
define(LANG_EXH_ARTWORK_SORT_BY,"Sort by");
define(LANG_EXH_ARTWORK_SORT_CR_NUMBER,"CR Number");
define(LANG_EXH_ARTWORK_SORT_YEAR,"Year");
define(LANG_INSTALLSHOT,"Installation shots");
define(LANG_ALL_EXH,"All Exhibitions");
define(LANG_LEFT_SIDE_INSTALLATION_SOTS,"Installation Shots");
define(LANG_LEFT_SIDE_GUIDE,"Guide");
define(LANG_LEFT_SIDE_EXH_TOUR,"Exhibition Tour");
# END exhibition

# literature
define(LANG_LIT_LEFT_MONOGRAPHS,"Monographs");
define(LANG_LIT_LEFT_CATALOGUES,"Catalogues");
define(LANG_LIT_LEFT_JOURNALS,"Journals");
define(LANG_LIT_LEFT_NEWS_ART,"Newspaper Articles");
define(LANG_LIT_LEFT_ONL_PUBL,"Online Publications");
define(LANG_LIT_LEFT_OTHERS,"Others");
define(LANG_LIT_LEFT_FILMS,"Films");
define(LANG_LIT_LEFT_THESES,"Theses");
define(LANG_LIT_LEFT_SOLO_EXH,"Solo Exhibitions");
define(LANG_LIT_LEFT_GROUP_EXH,"Group Exhibitions");
define(LANG_LIT_LEFT_COLLECTIONS,"Collections");
define(LANG_LIT_ONWARDS,"2010 onwards");
define(LANG_LIT_PUBLISHED,"Published year");
define(LANG_LIT_SEARCH,"Literature search");
define(LANG_LIT_BOOKS_PER,"books per page");
define(LANG_LIT_SEARCH_HELP,"header=[Search for book help] body=[&#60;p&#62;If you want to search for a specific book you must follow certain criteria.&#60;/p&#62;&#60;p&#62;You may search by title, author, or both; or by keyword.&#60;/p&#62;&#60;p&#62;Examples:&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;J&uuml;rgen Schilling&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter, J&uuml;rgen Schilling &#60;/p&#62;&#60;p&#62;- &lsquo;Schilling&rsquo;&#60;/p&#62;]");
define(LANG_LIT_SEARCH_TITLE,"Title");
define(LANG_LIT_SEARCH_AUTHOR,"Author");
define(LANG_LIT_SEARCH_AUTHOR2,"Author");
define(LANG_LIT_SEARCH_DATE,"Date");
define(LANG_LIT_SEARCH_SORTBYTITLE,"Sort by title");
define(LANG_LIT_SEARCH_SORTBYAUTHOR,"Sort by author");
define(LANG_LIT_SEARCH_SORTBYDATE,"Sort by date");
define(LANG_LIT_SEARCH_CLICK,"Click to view details");
define(LANG_LIT_SEARCH_PUBBY,"Publisher");
define(LANG_LIT_SEARCH_EDITOR,"Editor");
define(LANG_LIT_SEARCH_UNKNOWN,"Unknown");
define(LANG_LIT_SEARCH_ISBNNOTAVA,"isbn not available");
define(LANG_LIT_SEARCH_SEERELEXH,"See Exhibitions");
define(LANG_LIT_SEARCH_SEERELEXH2,"See Exhibition");
define(LANG_LIT_SEARCH_GOTORELEXH,"go to related exhibitions");
define(LANG_LIT_SEARCH_GOTORELEXH2,"go to related exhibition");
define(LANG_LIT_SEARCH_PAGES,"pages");
define(LANG_LIT_SEARCH_NOFOUND,"No books found, please try again.");
define(LANG_LIT_SEARCH_BACKTOLIT,"Back to literature");
define(LANG_LIT_SEARCH_INFORMATION,"Information");
define(LANG_LITERATURE_ENLARGE, "enlarge");
define(LANG_LITERATURE_IN, "In");
define(LANG_LITERATURE_ISSUE, "Issue");
define(LANG_LITERATURE_VOL, "Vol.");
define(LANG_LITERATURE_NO, "No.");
define(LANG_LITERATURE_PAGES, "p.");
define(LANG_LITERATURE_PAGESS, "pp.");
define(LANG_LITERATURE_COVER_HARDBACK, "Hardback");
define(LANG_LITERATURE_COVER_SOFTCOVER, "Softcover");
define(LANG_LITERATURE_COVER_UNKNOWNBINDING, "Unknown binding");
define(LANG_LITERATURE_COVER_SPECIALBINDING, "Special binding");
define(LANG_LITERATURE_ISBN, "ISBN");
define(LANG_LITERATURE_ISSN, "ISSN");
define(LANG_LITERATURE_DESC_TEXT, "Gerhard Richter's work and life has been the subject of numerous publications. The literature ranges from exhibition catalogues and collection catalogues to monographs, films and articles. Richter's own publications in the form of artist's books are an important part of his œuvre. Please find a selection of Richter related literature here.");
define(LANG_LITERATURE_SHOW_MORE_OPTIONS, "Show more options");
define(LANG_LITERATURE_SHOW_LESS_OPTIONS, "Show less options");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEO, "Related video");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS, "Related videos");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION, "This catalogue was published on the occasion of the following exhibition:");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS, "This catalogue was published on the occasion of the following exhibitions:");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE, "Related publication");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES, "Related publications");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK, "The following artwork is displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS, "The following artworks are displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TITLE, "Title");
define(LANG_LITERATURE_TITLES, "Titles");
define(LANG_LITERATURE_SHOW_ALL,"Show All");
# literature end

# video
define(LANG_VIDEO_ALLTITLES,"All titles");
define(LANG_VIDEO_VIDEOS,"Videos");
define(LANG_VIDEO_AUDIO,"Audio");
define(LANG_AUDIO_CR,"Catalogue Raisonné:");
define(LANG_VIDEO_RUNTIME,"Running time");
define(LANG_VIDEO_WATCH1,"Watch this video in Quicktime format");
define(LANG_VIDEO_WATCH2,"Watch this video in Flash format");
define(LANG_VIDEO_WATCH3,"Quicktime");
define(LANG_VIDEO_WATCH4,"Flash");
define(LANG_VIDEO_BACKTOVID,"Back to videos");
define(LANG_VIDEO_TAB_INFO, "Information");
define(LANG_VIDEO_TAB_IMAGES, "Images");
define(LANG_VIDEO_ARTWORK_MENTIONED, "This artwork is shown or mentioned in the following videos:");
define(LANG_VIDEO_EXHIBITION_MENTIONED, "This exhibition is featured in the following videos:");
define(LANG_VIDEO_IN_SECTION1, "There are");
define(LANG_VIDEO_IN_SECTION2, "videos in this section:");
# video end

# NEWS
define(LANG_NEWS_CURRENT,"Current");
define(LANG_NEWS_UPCOMING,"Upcoming");
define(LANG_NEWS_RESULTS,"Results");
define(LANG_NEWS_PUBLICATIONS,"Publications");
define(LANG_NEWS_TALKS,"Talks");
define(LANG_NEWS_AUCTIONS,"Auctions");
define(LANG_NEWS_EXHIBITIONS,"Exhibitions");
define(LANG_NEWS_ARCHIVE,"Archive");
define(LANG_NEWS_NO,"No news");
define(LANG_NEWS_NO_TALKS,"No information currently available");
define(LANG_NEWS_NO_AUCTIONS,"There are currently no artworks coming up for auction.");
define(LANG_NEWS_NOUPCOMING,"No news");
define(LANG_NEWS_NOARCHIVE,"No archive");
define(LANG_NEWS_NOPUBLICATIONS,"No recent publications");
define(LANG_NEWS_DOWNLOADLEAFLET,"download leaflet");
define(LANG_NEWS_ALSO_VISIT,"Also follow us on");
define(LANG_NEWS_SHOW_ALL,"Show all");
define(LANG_NEWS_DISCLIMER,"Disclaimer");
define(LANG_NEWS_DISCLIMER_TEXT1,"The information contained in this website is for general information purposes only. The information is provided by <a href='/' title='www.gerhard-richter.com'>www.gerhard-richter.com</a> and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk. In no event will we be liable for any loss whatsoever arising out of, or in connection with, the use of this website.");
define(LANG_NEWS_DISCLIMER_TEXT2,"This website is not, and does not purport to be a catalogue raisonné, and the inclusion on, or exclusion from, this website of any work does not constitute, and may not be relied upon as constituting, a representation or warranty that such work is or is not an authentic work of Gerhard Richter.");
define(LANG_NEWS_UPCOMING_FOR_AUCTION,"Auctions");
define(LANG_NEWS_UPCOMING_FOR_AUCTION_DESC,"The following artworks are coming up for auction:");
define(LANG_NEWS_REACEN_AUCTION_RES,"Results");
define(LANG_NEWS_REACEN_AUCTION_RES_DESC,"The following artworks were sold at auction in the last 6 months:");
define(LANG_NEWS_NEW_PUBLICATIONS,"Recent Publications");
define(LANG_NEWS_PUBLICATIONS_COUNT1,"There currently are");
define(LANG_NEWS_PUBLICATIONS_COUNT2,"new publications");
define(LANG_NEWS_NEW_TALKS,"Talks");
define(LANG_NEWS_AUCTIONS_AUCTIONHOUSE,"Auction House");
define(LANG_NEWS_AUCTIONS_DATE,"Date");
define(LANG_NEWS_RETURN_TO_NEWS_PAGE,"Return to news page");
# news end

#search box tooltip start
define(LANG_SEARCH_TIP_TITLE,"Search help");
define(LANG_SEARCH_TIP_TEXT,"<p class=\"tip\"><span>Number:</span> a list of numbers using commas eg. \"5,137,211\" or a range of numbers using a colon eg.\"5:20\".</p><p class=\"tip\"><span>Year:</span> an individual year, or range of years.</p><p class=\"tip\"><span>Date:</span> an individual date, or range of dates.</p><p class=\"tip\"><span>Size:</span> an exact size, or a range using the &#8216;from&#8217; and &#8216;to&#8217; fields.</p>");
#search box tooltip end

#search box EXH tooltip start
define(LANG_SEARCH_EXH_TIP_TITLE,"Search help");
define(LANG_SEARCH_EXH_TIP_TEXT,"<p class=\"tip\"><span>Title:</span> Search by title</p><p class=\"tip\"><span>Location:</span> Location of exhibition</p> <p class=\"tip\"><span>Keyword:</span> Search by keyword</p> <p class=\"tip\"><span>Year:</span> An individual year, or range of years</p>");
#search box EXH tooltip end

#search box QUOTES tooltip start
define(LANG_SEARCH_QUOTES_TIP_TITLE,"Search help");
define(LANG_SEARCH_QUOTES_TIP_TEXT,"<p class=\"tip\"><span>Keyword:</span> Search by keyword</p> <p class=\"tip\"><span>Year:</span> An individual year, or range of years</p> ");
#search box QUOTES tooltip end

#search box LIT tooltip start
define(LANG_SEARCH_LIT_TIP_TITLE,"Search help");
define(LANG_SEARCH_LIT_TIP_TEXT,"<p class=\"tip\"><span>Author:</span> Search by author</p> <p class=\"tip\"><span>Title:</span> Search by title</p><p class=\"tip\"><span>Keyword:</span> Search by keyword</p><p class=\"tip\"><span>Category:</span> Search by category</p><p class=\"tip\"><span>Language:</span> Search by language</p><p class=\"tip\"><span>Year:</span> Search by individual year, or a range of years</p>");
#search box LIT tooltip end

#search box ARTICLES tooltip start
define(LANG_SEARCH_ARTICLES_TIP_TITLE,"Search help");
define(LANG_SEARCH_ARTICLES_TIP_TEXT,"<p class=\"tip\"><span>Author:</span> Search by author</p> <p class=\"tip\"><span>Title:</span> Search by title</p><p class=\"tip\"><span>Keyword:</span> Search by keyword</p><p class=\"tip\"><span>Year:</span> Search by individual year, or a range of years</p>");
#search box ARTICLES tooltip end

#search box VIDEOS tooltip start
define(LANG_SEARCH_VIDEOS_TIP_TITLE,"Search help");
define(LANG_SEARCH_VIDEOS_TIP_TEXT,"<p class=\"tip\"><span>Keyword:</span> Search by keyword</p>");
#search box VIDEOS tooltip end

#zoomer start
define(LANG_ZOOMER_OPEN,"View in detail");
define(LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE,"See Microsite");
#zoomer end

#virtual gallery
define(LANG_VIRTUALGALLERY_OPEN,"View these images in our flash gallery");
define(LANG_VIRTUALGALLERY,"Virtual Gallery");
#end

# MICROSITE
define(LANG_MICROSITE_TITLE_SINBAD,"Sinbad");
define(LANG_MICROSITE_SHOW,"show");
define(LANG_MICROSITE_PER_PAGE,"per page");
define(LANG_MICROSITE_SEE_ALL,"all");
define(LANG_MICROSITE_SEARCH_EDITION,"search for edition");
define(LANG_MICROSITE_ABOUT,"About");
define(LANG_MICROSITE_SEARCH,"search");
define(LANG_MICROSITE_SINBAD_TITLE,"Gerhard Richter Sinbad Edition");
define(LANG_MICROSITE_SINBAD_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_JS_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_VIEW_AS_PAIR,"view as pair");
define(LANG_MICROSITE_SEARCH_HELP_TITLE,"Search Help");
define(LANG_MICROSITE_SEARCH_HELP_TEXT,"<em>Single edition =</em> type the edition number (eg; 23) <p><em>Multiple editions =</em> type each edition number, separated by commas (eg; 23, 32, 55)</p> <p><em>Groups =</em> type the start & end number of the group, separated by a colon (eg; 23:55)</p>");
define(LANG_MICROSITE_EDITIONS_CR,"Editions CR");
define(LANG_MICROSITE_IMAGE,"Image");
define(LANG_MICROSITE_OF,"of");
define(LANG_MICROSITE_NOVEMBER_FOOTER_1,"For further information on Gerhard Richter and his work,<br />please visit");
define(LANG_MICROSITE_NOVEMBER_FOOTER_2,"and learn about his life and work.");
# MICROSITE END

# OVERPAINTED PHOTOGRAPHS FORM
define(LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS,"Overpainted Photographs");
define(LANG_OPP_FORM_1,"Since the mid 1980s Gerhard Richter has created more than 2,000 Overpainted Photographs – with new artworks continually being produced. This large body of work illustrates Richter’s wide range of creative achievement in this medium.");
define(LANG_OPP_FORM_1_1,"For more information or to submit details on artworks of this series, please click ");
define(LANG_OPP_FORM_1_2,"here");


define(LANG_OPP_FORM_CATALOGUE_RAISONNE,"Gerhard Richter has created a large number of overpainted photographs in his career and new works are continually being produced. The catalogue will document this aspect of Richter’s oeuvre, still largely unknown to both art specialists and the public. It will illustrate the range of Richter’s creative achievements in this medium.");


define(LANG_OPP_FORM_2,"Gerhard Richter has created a large number of Overpainted Photographs in his career and new works are still being produced. In order to establish as definitive an inventory as possible of his Overpainted Photographs, we are collecting any of the following data:");

define(LANG_OPP_FORM_3,"");

define(LANG_OPP_FORM_4,"");

define(LANG_OPP_FORM_5,"title");
define(LANG_OPP_FORM_6,"year");
define(LANG_OPP_FORM_7,"medium");
define(LANG_OPP_FORM_8,"dimensions");
define(LANG_OPP_FORM_9,"provenance");
define(LANG_OPP_FORM_10,"history of exhibitions");
define(LANG_OPP_FORM_11,"bibliographical references");
define(LANG_OPP_FORM_12,"any other relevant information");

define(LANG_OPP_FORM_13,"We would be most grateful to receive information from private and public collectors who wish to propose Overpainted Photographs for inclusion on the website. Please kindly complete the online form or contact us by <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>email.</a>");

define(LANG_OPP_FORM_14,"While we hope you will be able to supply as many details as possible, we appreciate that some information may be sensitive, personal or private, so please do not feel obliged to complete any sections of this form you prefer not to provide information for.");
define(LANG_OPP_FORM_15,"Please be assured that all information provided will be treated in the strictest confidentiality and all requests for anonymity will be fully respected.");
define(LANG_OPP_FORM_16,"With thanks for your time and support for the project.");
define(LANG_OPP_FORM_17,"Works submission form");
define(LANG_OPP_FORM_18,"The following information will be considered for publication on the official website of Gerhard Richter. This form is for research, authentication and archival purposes only.");
define(LANG_OPP_FORM_19,"If you are unable to answer a question, please leave the field blank. Should you require assistance in completing the form, please do not hesitate to contact us by <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>email</a>.");
define(LANG_OPP_FORM_20,"Title of work");
define(LANG_OPP_FORM_21_1,"dd/mm/yyyy");
define(LANG_OPP_FORM_21,"Date of work");
define(LANG_OPP_FORM_22,"Medium");
define(LANG_OPP_FORM_23,"Oil on colour photograph");
define(LANG_OPP_FORM_24,"Oil on b/w photograph");
define(LANG_OPP_FORM_25,"Lacquer on colour photograph");
define(LANG_OPP_FORM_26,"Lacquer on b/w photograph");
define(LANG_OPP_FORM_26_1,"Oil on gelatin silver print");
define(LANG_OPP_FORM_27,"Dimensions of photograph (h &#215; w)");
define(LANG_OPP_FORM_28,"cm");
define(LANG_OPP_FORM_29,"inches");
define(LANG_OPP_FORM_30,"Dimensions of mount (h &#215; w)");
define(LANG_OPP_FORM_31,"Please indicate if you wish a credit line or collection name to appear on the website.");
define(LANG_OPP_FORM_32,"Credit line");
define(LANG_OPP_FORM_33,"Series / edition");
define(LANG_OPP_FORM_35,"Firenze (edition)");
define(LANG_OPP_FORM_36,"Florence (series)");
define(LANG_OPP_FORM_37,"Grauwald");
define(LANG_OPP_FORM_38,"Museum Visit");
define(LANG_OPP_FORM_38_1,"Sils");
define(LANG_OPP_FORM_38_2,"Wald");
define(LANG_OPP_FORM_38_3,"128 Fotos von einem Bild (Halifax 1978)");
define(LANG_OPP_FORM_38_4,"8.2.92");
define(LANG_OPP_FORM_39,"Series / edition number <br />(if applicable)");
define(LANG_OPP_FORM_40_1,"Front:");
define(LANG_OPP_FORM_40_1_1,"Signed");
define(LANG_OPP_FORM_40_1_2,"Dated");
define(LANG_OPP_FORM_40_1_3,"Numbered");
define(LANG_OPP_FORM_40_1_4,"Inscribed");
define(LANG_OPP_FORM_40_2,"Back:");
define(LANG_OPP_FORM_40_2_1,"Signed");
define(LANG_OPP_FORM_40_2_2,"Dated");
define(LANG_OPP_FORM_40_2_3,"Numbered");
define(LANG_OPP_FORM_40_2_4,"Inscribed");
define(LANG_OPP_FORM_41,"on mount");
define(LANG_OPP_FORM_41_1,"on backing board");
define(LANG_OPP_FORM_42,"on photograph");
define(LANG_OPP_FORM_42_1,"verso");
define(LANG_OPP_FORM_43,"upper left");
define(LANG_OPP_FORM_44,"upper centre");
define(LANG_OPP_FORM_45,"upper right");
define(LANG_OPP_FORM_46,"centre left");
define(LANG_OPP_FORM_47,"centre middle");
define(LANG_OPP_FORM_48,"centre right");
define(LANG_OPP_FORM_49,"lower left");
define(LANG_OPP_FORM_50,"lower centre");
define(LANG_OPP_FORM_51,"lower right");
define(LANG_OPP_FORM_53,"Other inscriptions");
define(LANG_OPP_FORM_54,"Notations / labels on the back");
define(LANG_OPP_FORM_55,"Image upload");
define(LANG_OPP_FORM_55_1,"It is possible to upload more than one image");
define(LANG_OPP_FORM_56,"We would greatly appreciate high resolution digital images in at least 300dpi (whenever possible including the part of the mount featuring the artist’s signature).");
define(LANG_OPP_FORM_57,"Preferred formats are: JPEG, TIFF, PSD, PNG");
define(LANG_OPP_FORM_58,"Copyright / photographer credit");
define(LANG_OPP_FORM_59,"Provenance");
define(LANG_OPP_FORM_60,"Acquired from");
define(LANG_OPP_FORM_61,"Date of acquisition");
define(LANG_OPP_FORM_62,"Exhibition history");
define(LANG_OPP_FORM_62_1,"Exhibition history other");
define(LANG_OPP_FORM_63,"Publication history");
define(LANG_OPP_FORM_63_1,"Publication history other");
define(LANG_OPP_FORM_64,"Personal details");
define(LANG_OPP_FORM_65,"First name");
define(LANG_OPP_FORM_66,"Last name");
define(LANG_OPP_FORM_67,"Email address");
define(LANG_OPP_FORM_68,"Address");
define(LANG_OPP_FORM_68_1,"Post code");
define(LANG_OPP_FORM_68_2,"Country");
define(LANG_OPP_FORM_69,"Telephone number");
define(LANG_OPP_FORM_70,"Additional comments");
define(LANG_OPP_FORM_71,"Required field");
define(LANG_OPP_FORM_72,"By submitting this form the owner certifies that all information provided is accurate and that no information pertaining to the artwork, its history or ownership has been knowingly withheld.");
define(LANG_OPP_FORM_73,"The owner understands that submission of the form does not guarantee inclusion of the work on the website.");
define(LANG_OPP_FORM_74,"submit form");
define(LANG_OPP_FORM_75,"Please read the statement on the previous page, and tick the box to indicate you understand the conditions.");
define(LANG_OPP_FORM_75_2,"Please fill in all required fields!");
define(LANG_OPP_FORM_75_3,"Verification characters you entered where not correct please try again!");
define(LANG_OPP_FORM_76,"Your form has been submitted.");
define(LANG_OPP_FORM_77,"Thank you for your time.");
define(LANG_OPP_FORM_77_2,"Print data");



# exhibition list
define(LANG_OPP_FORM_78_9_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_78_9_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_78_9_D,", May &#8211; June 2016");
define(LANG_OPP_FORM_78_8_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_78_8_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_78_8_D,", November &#8211; December 2015");
define(LANG_OPP_FORM_78_7_T,"Gerhard Richter. Das kleine Format");
define(LANG_OPP_FORM_78_7_L,", Galerie Schwarzer, Dusseldorf");
define(LANG_OPP_FORM_78_7_D,", April &#8211; June 2015");
define(LANG_OPP_FORM_78_6_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_6_L,", The Lone T Art Space, Milan");
define(LANG_OPP_FORM_78_6_D,", April &#8211; May 2015");
define(LANG_OPP_FORM_78_5_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_78_5_L,", Hannah Hoffman Gallery, Los Angeles");
define(LANG_OPP_FORM_78_5_D,", March  &#8211; April 2015");
define(LANG_OPP_FORM_78_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_4_L,", Repetto Gallery, London");
define(LANG_OPP_FORM_78_4_D,", February &#8211; March 2015");
define(LANG_OPP_FORM_78_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_3_L,", Marian Goodman Gallery, London");
define(LANG_OPP_FORM_78_3_D,", October &#8211; December 2014");
define(LANG_OPP_FORM_78_1_T,"What is a Photograph");
define(LANG_OPP_FORM_78_1_L,", International Center of Photography, New York");
define(LANG_OPP_FORM_78_1_D,", January &#8211; May 2014");
define(LANG_OPP_FORM_78_2_T,"Purer Zufall. Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_78_2_L,", Sprengel Museum Hannover, Hanover");
define(LANG_OPP_FORM_78_2_D,", May &#8211; September 2013");
define(LANG_OPP_FORM_78_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_L,", Beirut Art Center, Beirut");
define(LANG_OPP_FORM_78_D,", April &#8211; June 2012");
define(LANG_OPP_FORM_79_T,"Gerhard Richter &#8211; Arbeiten 1968-2008");
define(LANG_OPP_FORM_79_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_79_D,", November 2011 &#8211; January 2012");
define(LANG_OPP_FORM_80_T,"Editionen und übermalte Fotografien");
define(LANG_OPP_FORM_80_L,", Wolfram Völcker Fine Art, Berlin");
define(LANG_OPP_FORM_80_D,", September &#8211; October 2011");
define(LANG_OPP_FORM_81_T,"De-Natured: German Art From Joseph Beuys to Martin Kippenberger");
define(LANG_OPP_FORM_81_L,", Ackland Art, Museum, Chapel Hill");
define(LANG_OPP_FORM_81_D,", April &#8211; July 2011");
define(LANG_OPP_FORM_82_T,"Gerhard Richter “New Overpainted Photographs“");
define(LANG_OPP_FORM_82_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_82_D,", February &#8211; March 2010");
define(LANG_OPP_FORM_82_1_T,"Best of Fifty Years");
define(LANG_OPP_FORM_82_1_L,", Kunstverein Wolfsburg");
define(LANG_OPP_FORM_82_1_D,", November 2009 &#8211; February 2010");
define(LANG_OPP_FORM_83_T,"Photo España 2009: Gerhard Richter, Fotografías pintadas");
define(LANG_OPP_FORM_83_L,", Fundación Telefónica, Madrid");
define(LANG_OPP_FORM_83_D,", June 2009");
define(LANG_OPP_FORM_84_T,"The Photographic Object");
define(LANG_OPP_FORM_84_L,", Photographers’ Gallery, London");
define(LANG_OPP_FORM_84_D,", April &#8211; June 2009");
define(LANG_OPP_FORM_85_T,"Gerhard Richter &#8211; Übermalte Fotografien &#8211; Photographies Peintes");
define(LANG_OPP_FORM_85_L,", Centre de la photographie, Geneva");
define(LANG_OPP_FORM_85_D,", February &#8211; May 2009");
define(LANG_OPP_FORM_86_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_86_L,", Museum Morsbroich, Leverkusen");
define(LANG_OPP_FORM_86_D,", October 2008 &#8211; January 2009");
define(LANG_OPP_FORM_86_1_T,"Gerhard Richter “New Works“");
define(LANG_OPP_FORM_86_1_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_86_1_D,", November &#8211; December 2005");
define(LANG_OPP_FORM_87_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_87_L,", Anthony Meier Fine Arts, San Francisco");
define(LANG_OPP_FORM_87_D,", June &#8211; August 2005");
define(LANG_OPP_FORM_88_T,"Gerhard Richter &#8211; Editionen 1968 &#8211; 2004");
define(LANG_OPP_FORM_88_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_88_D,", June &#8211; July 2005");
define(LANG_OPP_FORM_89_T,"Attack: Attraction Painting/Photography");
define(LANG_OPP_FORM_89_L,", Marcel Sitcoske Gallery, San Francisco");
define(LANG_OPP_FORM_89_D,", December 2002 &#8211; February 2003");
define(LANG_OPP_FORM_90_T,"Gerhard Richter");
define(LANG_OPP_FORM_90_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_90_D,", December 2002 &#8211; January 2003");
define(LANG_OPP_FORM_91_T,"Gerhard Richter &#8211; Firenze");
define(LANG_OPP_FORM_91_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_91_D,", June &#8211; August 2002");
define(LANG_OPP_FORM_92_T,"Gerhard Richter &#8211; Editionen 1969 &#8211; 1998");
define(LANG_OPP_FORM_92_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_92_D,", March &#8211; May 2001");
define(LANG_OPP_FORM_93_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_93_L,", Galerie Fred Jahn, Munich");
define(LANG_OPP_FORM_93_D,", October 2000");
define(LANG_OPP_FORM_93_1_T,"Gerhard Richter &#8211; Bilder 1972 &#8211; 1996");
define(LANG_OPP_FORM_93_1_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_1_D,", May &#8211; July 1998");
define(LANG_OPP_FORM_93_2_T,"Gerhard Richter");
define(LANG_OPP_FORM_93_2_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_2_D,", October &#8211; November 1997");
define(LANG_OPP_FORM_93_3_T,"Gerhard Richter Part II: Painting, Mirror Painting, Watercolour, Photograph, Print");
define(LANG_OPP_FORM_93_3_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_3_D,", May 1996");
define(LANG_OPP_FORM_93_4_T,"Gerhard Richter &#8211; Editionen 1967 &#8211; 1993");
define(LANG_OPP_FORM_93_4_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_4_D,", January &#8211; March 1994");
define(LANG_OPP_FORM_93_5_T,"Gerhard Richter: Sils");
define(LANG_OPP_FORM_93_5_L,", Nietzsche-Haus, Sils-Maria");
define(LANG_OPP_FORM_93_5_D,", July 1992 &#8211; March 1993");
define(LANG_OPP_FORM_94_T,"Other exhibitions");
define(LANG_OPP_FORM_94_L,"");
define(LANG_OPP_FORM_94_D,"");

# publications list
define(LANG_OPP_FORM_95_6_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_95_6_L,". Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_95_6_D,", 2015");
define(LANG_OPP_FORM_95_5_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_95_5_L," (Buchloh, Benjamin H.D., Schwarz, Dieter). Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_95_5_D,", 2016");
define(LANG_OPP_FORM_95_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_95_4_L," (Barbero, Luca Massimo). Nuvole Rosse");
define(LANG_OPP_FORM_95_4_D,", 2015");
define(LANG_OPP_FORM_95_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_95_3_L," (Buchloh, Benjamin H.D., Schwarz, Dieter, Storr, Robert). London: Marian Goodman Gallery");
define(LANG_OPP_FORM_95_3_D,", 2014");
define(LANG_OPP_FORM_95_2_T,"What is a Photograph");
define(LANG_OPP_FORM_95_2_L," (Squiers, Carol, Baker, George, Batchen, Geoffrey, Steyerl, Hito). New York: Prestel");
define(LANG_OPP_FORM_95_2_D,", 2014");
define(LANG_OPP_FORM_95_T,"Purer Zufall &#8211; Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_95_L," (Krempel, Ulrich, Rist, Annerose, Schwarz, Isabelle). Hanover: Sprengel Museum");
define(LANG_OPP_FORM_95_D,", 2013");
define(LANG_OPP_FORM_95_1_T,"Beirut");
define(LANG_OPP_FORM_95_1_L," (Borchardt-Hume, Achim, Joreige, Lamia, Dagner, Sandra). Beirut: Beirut Art Center; London: Heni Publishing; Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_95_1_D,", 2012");
define(LANG_OPP_FORM_96_T,"Best of Fifty Years");
define(LANG_OPP_FORM_96_L," (Justin Hoffmann/Kunstverein Wolfsburg). Wolfsburg: Kunstverein Wolfsburg");
define(LANG_OPP_FORM_96_D,", 2010");
define(LANG_OPP_FORM_97_T,"Gerhard Richter. Obrist &#8211; O’Brist");
define(LANG_OPP_FORM_97_L," (Obrist, Hans Ulrich). Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_97_D,", 2009");
define(LANG_OPP_FORM_97_1_T,"Gerhard Richter. Overpainted Photographs");
define(LANG_OPP_FORM_97_1_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_97_1_D,", 2008");
define(LANG_OPP_FORM_98_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_98_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_98_D,", 2008");
define(LANG_OPP_FORM_99_T,"Gerhard Richter.");
define(LANG_OPP_FORM_99_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_99_D,", 2005");
define(LANG_OPP_FORM_100_T,"City Life");
define(LANG_OPP_FORM_100_L," (Cora, Bruno, Restagno, Enzo, Gori, Giuliano). Prato: Gli Ori");
define(LANG_OPP_FORM_100_D,", 2002");
define(LANG_OPP_FORM_100_1_T,"Gerhard Richter.");
define(LANG_OPP_FORM_100_1_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_1_D,", 2002");
define(LANG_OPP_FORM_100_2_T,"Sils");
define(LANG_OPP_FORM_100_2_L," (eds. Obrist, Hans Ulrich, Bloch, Peter Andre). Munich: Oktagon, 1992; New York: D. A. P., Distributed Art Publishers");
define(LANG_OPP_FORM_100_2_D,", 2002 ");
define(LANG_OPP_FORM_100_3_T,"Florence");
define(LANG_OPP_FORM_100_3_L," (Elger, Dietmar). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_100_3_D,", 2001");
define(LANG_OPP_FORM_100_4_T,"Gerhard Richter, Part I: New Painting; Part II, Part II: Painting, Mirror Painting, Watercolour, Photograph, Print.");
define(LANG_OPP_FORM_100_4_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_4_D,", 1996");
define(LANG_OPP_FORM_100_5_T,"Other publications");
define(LANG_OPP_FORM_100_5_L,"");
define(LANG_OPP_FORM_100_5_D,"");


define(LANG_OPP_FORM_103,"I would like to receive a copy of the data by email");
define(LANG_OPP_FORM_104,"Reference number");
# END OVERPAINTED PHOTOGRAPHS FORM

# TABLE
define(LANG_TABLE_DESC_PUBLICATION_RELATED,"related");
define(LANG_TABLE_DESC_PUBLICATION,"publication");
define(LANG_TABLE_DESC_PUBLICATIONS,"publications");
define(LANG_TABLE_TITLE,"Title");
define(LANG_TABLE_FURTHER_ARTWORKS_IN_LOT,"Further artworks in this lot:");
# END TABLE

# Search
define(LANG_SEARCH_SHOW_MORE_OPTIONS,"Show more options");
# END Search

# share button
define(LANG_SHARE_THIS_PAGE,"Share this page");
define(LANG_SHARE_EMAIL_SUBJECT,"Visit this page on gerhard-richter.com");
define(LANG_SHARE_EMAIL_BODY_1,"Dear friend,");
define(LANG_SHARE_EMAIL_BODY_2,"I would like to share the following page with you:");
define(LANG_SHARE_EMAIL_BODY_3,"Kind regards,");
define(LANG_SHARE_EMAIL,"Email");
# END share button

# Mobile site specific
define(LANG_MOBILE_SEARCH_FIND,"Find");
# END Mobile site specific
?>
