<?php
# meta info
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION_HOME,"Find out more about the life and career of Gerhard Richter, one of the most important artists of the 20th and 21st centuries.");
define(LANG_META_DESCRIPTION_ART,"Discover Richter's versatile oeuvre, ranging from oils on canvas, works on paper, overpainted photographs and Atlas, his collection of photographs, newspaper cuttings and sketches.");
define(LANG_META_DESCRIPTION_BIOGRAPHY,"The biography sheds light on Richter's life and career. Born in Dresden in 1932, Richter was brought up under Third Reich and received the first part of his artistic education in East Germany. In 1961 he relocated to West Germany, where he developed into one of the most distinguished artists of his time.");
define(LANG_META_DESCRIPTION_CHRONOLOGY,"A concentrated overview of Richter's life and career: important chapters of his private life are mirrored by significant artistic achievements.");
define(LANG_META_DESCRIPTION_QUOTES,"A selection of Richter's quotes offer insight into his way of working, his understanding of art and his view of the world.");
define(LANG_META_DESCRIPTION_EXHIBITIONS,"Research over 50 years of exhibition history of Gerhard Richter's works.");
define(LANG_META_DESCRIPTION_LITERATURE,"Research Richter's artist's books, publications on his oeuvre as well as newspaper articles and online publications.");
define(LANG_META_DESCRIPTION_VIDEOS,"Watch videos on Richter's recent exhibitions, on selected groups of his works and talks.");
define(LANG_META_DESCRIPTION_LINKS,"Find links to articles in newspapers and magazines as well as to galleries dealing with Richter's works.");
define(LANG_META_DESCRIPTION_NEWS,"Current news: ongoing exhibitions, upcoming auctions and recent publications can be found here.");
# END meta info

#PAGE TITLES
if( $_SESSION['kiosk'] ) define(LANG_TITLE_HOME,"Gerhard-Richter.com");
else define(LANG_TITLE_HOME,"格哈德·里希特");
#PAGE TITLES END

# MENU
define(LANG_HEADER_MENU_HOME,'首页');
define(LANG_HEADER_MENU_HOME_TT,'格哈德·里希特 首页');
define(LANG_HEADER_MENU_ARTWORK,'艺术');
define(LANG_HEADER_MENU_ARTWORK_TT,'格哈德·里希特 艺术 包括作品、图集、复制品、油彩涂抹的相片、水彩及素描和速写');
define(LANG_HEADER_MENU_BIOGRAPHY,'艺术家简介');
define(LANG_HEADER_MENU_BIOGRAPHY_TT,'格哈德·里希特 生平');
define(LANG_HEADER_MENU_CHRONOLOGY,'年表');
define(LANG_HEADER_MENU_CHRONOLOGY_TT,'Gerhard Richter 年表');
define(LANG_HEADER_MENU_QUOTES,'引语');
define(LANG_HEADER_MENU_QUOTES_TT,'格哈德·里希特 引语');
define(LANG_HEADER_MENU_EXHIBITIONS,'展览');
define(LANG_HEADER_MENU_EXHIBITIONS_TT,'格哈德·里希特 世界各地展览');
define(LANG_HEADER_MENU_LITERATURE,'文献');
define(LANG_HEADER_MENU_LITERATURE_TT,'格哈德·里希特 书籍');
define(LANG_HEADER_MENU_VIDEOS,'视频');
define(LANG_HEADER_MENU_VIDEOS_RELATED,'相关视频');
define(LANG_HEADER_MENU_VIDEOS_TT,'格哈德·里希特 视频与访谈');
define(LANG_HEADER_MENU_AUDIO,'音声资料');
define(LANG_HEADER_MENU_AUDIO_TT,'格哈德·里希特 音声资料');
define(LANG_HEADER_MENU_MEDIA,'媒体');
define(LANG_HEADER_MENU_MEDIA_TT,'Gerhard Richter 媒体');
define(LANG_HEADER_MENU_CONTACT,'链接');
define(LANG_HEADER_MENU_CONTACT_TT,'格哈德·里希特 链接');
define(LANG_HEADER_MENU_NEWS,'最新动态');
define(LANG_HEADER_MENU_NEWS_TT,'格哈德·里希特 新闻');
define(LANG_HEADER_MENU_SEARCH,'搜寻');
define(LANG_HEADER_MENU_SEARCH_TT,'Gerhard Richter 搜寻');
define(LANG_NEWS,'最新动态');
define(LANG_HEADER_MENU_ADMIN,'网站管理');
# MENU END

#LEFT SIDE
define(LANG_LEFT_SIDE_PAINTINGS,'绘画');
define(LANG_LEFT_SIDE_CATEGORIES,'种类');
define(LANG_LEFT_SIDE_ATLAS,'图集');
define(LANG_LEFT_SIDE_EDITIONS,'限量版画');
define(LANG_LEFT_SIDE_YEARS,'年');
define(LANG_LEFT_SIDE_DRAWINGS,'素描与速写');
define(LANG_LEFT_SIDE_OILSONPAPER,'纸本油画');
define(LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS,'油彩涂抹的相片');
define(LANG_LEFT_SIDE_WATERCOLOURS,'水彩');
define(LANG_LEFT_SIDE_OTHER,'其他');
define(LANG_LEFT_SIDE_PRINTS,"Prints");
define(LANG_LEFT_SIDE_MICROSITES,'微型网站');
#LEFT SIDE END

#RIGHT SIDE
define(LANG_RIGHT_SEARCH_H1,'搜寻');
define(LANG_RIGHT_SEARCH_EXH_H1,'搜寻');
define(LANG_RIGHT_SEARCH_QUOTES_H1,'搜寻');
define(LANG_RIGHT_SEARCH_VIDEOS_H1,'搜寻');
define(LANG_RIGHT_SEARCH_SEARCHNOW,'开始搜寻');
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE1,"依");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE2,"媒介");
define(LANG_RIGHT_SEARCH_BYTITLE1,"依");
define(LANG_RIGHT_SEARCH_BYTITLE2,"标题");
define(LANG_RIGHT_SEARCH_BY,"依");
define(LANG_RIGHT_SEARCH_BYPAINTINGS,"CR 编号");
define(LANG_RIGHT_SEARCH_BYATLAS,"图集页");
define(LANG_RIGHT_SEARCH_BYEDITION,"限量版画 CR");
define(LANG_RIGHT_SEARCH_BYDRAWINGS,"素描 CR");
define(LANG_RIGHT_SEARCH_BYNUMBER,"编号");
define(LANG_RIGHT_SEARCH_ADVANCED_SEARCH,"进阶搜寻");
define(LANG_RIGHT_SEARCH_QUICK_SEARCH,"快速检索");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST,"列表");
define(LANG_RIGHT_SEARCH_BYNUMBER_RANGE,"范围");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE,"再加");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE,"移除");
define(LANG_RIGHT_SEARCH_YEARPAINTED1,"依");
define(LANG_RIGHT_SEARCH_YEARPAINTED2,"创作年");
define(LANG_RIGHT_SEARCH_DATE,"日期");
define(LANG_RIGHT_SEARCH_DATE_FROM,"自");
define(LANG_RIGHT_SEARCH_DATE_TO,"至");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_D,"日日");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_M,"月月");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y,"年年年年");
define(LANG_RIGHT_SEARCH_COLOR1,"依");
define(LANG_RIGHT_SEARCH_COLOR2,"颜色");
define(LANG_RIGHT_SEARCH_BYSIZE,"尺寸");
define(LANG_RIGHT_SEARCH_BYSIZEH,"H");
define(LANG_RIGHT_SEARCH_BYSIZEH1,"高度（公分）");
define(LANG_RIGHT_SEARCH_BYSIZEW,"W");
define(LANG_RIGHT_SEARCH_BYSIZEW1,"宽度（公分）");
define(LANG_RIGHT_SEARCH_BYSIZEMIN,"最小");
define(LANG_RIGHT_SEARCH_BYSIZEMAX,"最大");
define(LANG_RIGHT_SEARCH_BY_COLLECTION,"依 <strong>地点</strong>");
define(LANG_RIGHT_SEARCH_SEARCHBUTTON,"搜寻");
define(LANG_RIGHT_SEARCH_SEARCHRESET,"重设");
define(LANG_RIGHT_SEARCH_EXH_LOCATION,"地点");
define(LANG_RIGHT_SEARCH_EXH_TITLE,"标题");
define(LANG_RIGHT_SEARCH_EXH_KEYWORD,"关键字");
define(LANG_RIGHT_SEARCH_QUOTES_KEYWORD,"关键字");
define(LANG_RIGHT_SEARCH_VIDEOS_KEYWORD,"关键字");
define(LANG_RIGHT_SEARCH_QUOTES_SEARCHALL,"浏览所有引语");
define(LANG_RIGHT_SEARCH_LIT_AUTHOR,"作者");
define(LANG_RIGHT_SEARCH_LIT_TITLE,"标题");
define(LANG_RIGHT_SEARCH_LIT_KEYWORD,"关键字");
define(LANG_RIGHT_SEARCH_LIT_CATEGORY,"文献种类");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT,'选择...');
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE,'语言');
define(LANG_RIGHT_FEATURED_TITLES,"微型网站");
define(LANG_RIGHT_FEATURED_TITLES_SNOWWHITE,"Snow White");
define(LANG_RIGHT_FEATURED_TITLES_WARCUT,"War Cut II");
define(LANG_RIGHT_FEATURED_TITLES_FIRENZE,"Firenze");
define(LANG_RIGHT_FEATURED_TITLES_4900COLOURS,"4900 Colours");
define(LANG_RIGHT_FEATURED_TITLES_SINBAD,"Sinbad");
define(LANG_RIGHT_FEATURED_TITLES_ELBE,"Elbe");
define(LANG_RIGHT_FEATURED_TITLES_COLOGNE_CATHEDRAL,"克隆大教堂（Cologne cathedral）");
define(LANG_RIGHT_TIMELINE,"<span class='related'>时间表</span><br />了解艺术家的创作生涯.");
define(LANG_RIGHT_TIMELINE_INTER,"互动化时间表");
define(LANG_RIGHT_VIRTUALEXH,"<span class='related'>虚拟 </span><br />展览");
define(LANG_RIGHT_FETURED_TITLES,"焦点书籍");
define(LANG_RIGHT_VIDEO,"您的电脑必须先安装Flash软件才能观赏这些视频。");
define(LANG_RIGHT_VIDEO_DOWN,"下载");
define(LANG_RIGHT_AUDIO,"您的电脑必须先安装Flash软件才能播放这些音声资料。");
define(LANG_RELATED_BLOCK_BROWSE_CLIPS,"All clips");
define(LANG_RELATED_BLOCK_RECENTLY,"Recently Added");
#RIGHT SIDE END

# credits page
define(LANG_CREDITS,'鸣谢');
define(LANG_CREDITS_TEXT1_1,'');
define(LANG_CREDITS_TEXT1_2,'本网站为格哈德·里希特的官方网站，由 Joe Hage 制作、架设并维护。');
define(LANG_CREDITS_TEXT2_1,'感谢');
define(LANG_CREDITS_TEXT2_2,'《里希特限量版画作品全目录第一册》 (2011年出版) ');
define(LANG_CREDITS_TEXT2_3,'作者 Dietmar Elger 的协助与支援。');
define(LANG_CREDITS_TEXT3_1,"感谢 Hubertus Butin 为");
define(LANG_CREDITS_TEXT3_2,"里希特限量版画 (有限复制品) ");
define(LANG_CREDITS_TEXT3_3,"提供周详资料。");
define(LANG_CREDITS_TEXT4,'特别感谢 Amy Dickson 对此年表的贡献。');
# end

# footer
define(LANG_FOOTER_COPYRIGHT,'格哈德·里希特 - 版权所有');
define(LANG_FOOTER_CONTACT,'联系我们');
define(LANG_FOOTER_CREDITS,'鸣谢');
define(LANG_FOOTER_DISCLIMER,'免责声明');
# footer END

# home page
define(LANG_HOME_TEXT_INTRO1,'格哈德·里希特是二十及二十一世纪的重要艺术家，其创作横跨五十年的历程。在这个网站上，你可以浏览他的作品并了解他的生平。请选择并点击下面的小图，开展你对里希特的探索之旅。');
# home END

# artwork page
define(LANG_ARTWORK_PHOTO_PAINTINGS,'绘画');
define(LANG_ARTWORK_ABSTRACTS,'抽象画');
define(LANG_ARTWORK_ATLAS,'图集');
define(LANG_ARTWORK_ATLAS_SHEET,'Atlas Sheet');
define(LANG_ARTWORK_EDITIONS,'限量版画');
define(LANG_ARTWORK_WATERCOLOURS,'水彩');
define(LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS,'油彩涂抹的相片');
define(LANG_ARTWORK_DRAWINGS,'素描和速写');
define(LANG_ARTWORK_OILSONPAPER,'纸本油画');
define(LANG_ARTWORK_OTHER,'其他');
define(LANG_ARTWORK_MICROSITES,'微型网站');
define(LANG_ARTWORK_MICROSITES2,'微型网站');
define(LANG_ARTWORK_ARTISTS_BOOKS,"艺术家著作");
define(LANG_ARTWORK_TEXT_H1,"横跨半世纪的艺术历程");
define(LANG_ARTWORK_TEXT,"里希特在1962年正式开始绘画，你可以在此看到他各种形式的作品，包括布面油画和油彩涂抹的相片，以及与历史相关的摄影作品《图集（Atlas）》");
define(LANG_ARTWORK_TEXT_PAINTINGS1,"Richter officially began painting in 1962. Here we give you access to his various works, comprising photo paintings and abstracts.");
define(LANG_ARTWORK_TEXT_PAINTINGS,'虽然艺术家本人刻意避免对其作品进行归类，但为了便于浏览，本网站将其画作做了主观的分类。');
define(LANG_ARTWORK_SECTION_PAINTINGS,"作品");
define(LANG_ARTWORK_SECTION_PHOTOPAINTINGS,"相片画");
define(LANG_ARTWORK_SECTION_ABSTRACTS,"抽象画");
define(LANG_ARTWORK_SECTION_NOCR,"No CR");
define(LANG_ARTWORK_SECTION_OTHERMEDIA,"其他");
define(LANG_ARTWORK_SECTION_OTHER,"破坏画");
define(LANG_ARTWORK_TEXT_ATLAS,"浏览完整的《图集》系列作品 - 这些是里希特诸多作品原始材料的新闻剪辑、相片和速写。");
define(LANG_ARTWORK_TITLE_OVERPAINTED_PHOTOS,"里希特的油彩涂抹相片作品全目录");
define(LANG_ARTWORK_TEXT_OVERPAINTED_PHOTOS,"里希特的油彩涂抹相片作品全目录将在2013年底出版，我们目前正全力搜索与该目录相关的所有信息，包括该类作品的拥有人资讯和图像资料等等。如果您能提供任何相关的信息，请与我们联系，感激不尽。我们将确保您所提供所有信息的隐私权。");
define(LANG_ARTWORK_CONTACT_OVERPAINTED_PHOTOS,"发送信息");
define(LANG_ARTWORK_TEXT_ATLAS_BACKTOATLAS,'回到作品');
define(LANG_ARTWORK_TEXT_ATLAS_ASOCIATED,'相关作品');
define(LANG_ARTWORK_BROWSE_WORKS,'浏览所有画作');
define(LANG_ARTWORK_BROWSE_OPP,'浏览所有油彩涂抹的相片作品');
define(LANG_ARTWORK_BROWSE_ATLAS,'浏览所有图集作品（Atlas）');
define(LANG_ARTWORK_BROWSE_EDITIONS,'你可以在此浏览里希特所有的图像与摄影的有限复制品（限量版画）作品');
define(LANG_ARTWORK_ATLAS_ASSOCIATED,'下面的缩略图显示来自某图集页（Atlas Sheet）的画作图像');
define(LANG_ARTWORK_BOOKS_REL_TAB,'文献');
define(LANG_ARTWORK_BOOKS_REL_COLOUR,'彩色');
define(LANG_ARTWORK_BOOKS_REL_BW,'黑白');
define(LANG_ARTWORK_BOOKS_REL_ARTWORK,'艺术品');
define(LANG_ARTWORK_BOOKS_REL_ILLUSTRATED,'图示本作品');
define(LANG_ARTWORK_BOOKS_REL_MENTIONED,'提到本作品');
define(LANG_ARTWORK_BOOKS_REL_DISCUSSED,'讨论到本作品');
define(LANG_ARTWORK_BOOKS_REL_P,'第');
define(LANG_ARTWORK_BOOKS_REL_PP,'第');
define(LANG_ARTWORK_BOOKS_REL_P2,'页');
define(LANG_ARTWORK_BOOKS_REL_PP2,'页');
define(LANG_ARTWORK_BOOKS_REL_CHINESE,'页');
define(LANG_ARTWORK_PHOTOS_TAB,'照片');
define(LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS,'展场图片:');
define(LANG_ARTWORK_PHOTOS_TAB_DEATILS,'细部');
define(LANG_ARTWORK_THUMB_SALES_HIST_AVAIL,'Sales history available');
define(LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL,'收藏资讯供览');
define(LANG_ARTWORK_BREADCRUMB_ALL,'所有绘画');
define(LANG_ARTWORK_BREADCRUMB_ALL_OPP,'所有相片');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN,'此作品曾于下列展览展出:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED,'关于此作的文献:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS,"关于此作的文献:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS,'作品细部图:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS,'此作品的创作来源为《图集》所收录的一幅影像:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC,'此《图集》页涵括下列作品的图片来源:');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB,'Individual works');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC,'This edition consists of the following individual works:');
define(LANG_ARTWORK_DESTROYED,"已毁");
# artwork END

# Tabs titles
define(LANG_TAB_TITLE_ARTWORKS,'作品');
# Tabs titles END

# contact page
define(LANG_CONTACT_SUBNAV_TEXT_HEADING,'艺术家链接');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM1,'写信联系我们');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM2,'一级艺术商');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM4,'艺术商');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM3,'海报出售');
define(LANG_CONTACT_WRITE_TEXT_BRIEF,'欲联系里希特网站，请填写以下表格');
define(LANG_CONTACT_WRITE_INPUTVALUE_EMAIL,'填写你的电子邮件地址');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT1,'一般询问/留言');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT2,'寻找艺术商');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT3,'购买海报有限复制品作品');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT4,'Image permissions');
define(LANG_CONTACT_WRITE_INPUTVALUE_MESSAGE,'在此留言');
define(LANG_CONTACT_WRITE_INPUTVALUE_SEND,'送出');
define(LANG_CONTACT_POSTERS_LINK,' 里希特《海报有限复制品（Poster Edition）》');
define(LANG_CONTACT_SHOP_LINK,'里希特 商店');
define(LANG_CONTACT_EMAIL,'电子邮件地址');
define(LANG_CONTACT_SUBJECT_SELECT,'选择信息主题');
define(LANG_CONTACT_SUBJECT,'选择信息主题');
define(LANG_CONTACT_FILE,'File');
define(LANG_CONTACT_MESSAGE,'信息');
define(LANG_CONTACT_CAPCHA,'请将验证码填入下方栏位（不含空格，皆为英文小写字母）');
define(LANG_CONTACT_THANX1,'謝謝');
define(LANG_CONTACT_THANX2,'感谢您联系里希特网站');
# contact END

# links
define(LANG_LINKS_ARTICLES,'文章');
define(LANG_LINKS_DEALERS,'艺术商');
define(LANG_LINKS,'链接');
define(LANG_LINKS_ARTICLES_PUBLISHED_IN,'于');
# links END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_xsmall.png');
define(LANG_NO_IMAGE_SMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_small.png');
define(LANG_NO_IMAGE_MEDIUM,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_LARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_XLARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_ORIGINAL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_THUMBNAIL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_thumbnail.png');
# IF NO IMAGE To DISPLAY END

# DIFFERENT
define(LANG_ATLASSHEETS,'图集页');
define(LANG_RELEATED,'相关栏目');
define(LANG_RELEATED_ART,'相关艺术');
define(LANG_COLLECTION,'收藏');
define(LANG_NOTES,'作品说明');
define(LANG_SHOP,'商店');
define(LANG_SHOP_INFO,'可以在… 网上购买 ');
define(LANG_NEXTDECADE,'后来十年');
define(LANG_PREVDECADE,'先前十年');
define(LANG_SELECT,'选择..');
define(LANG_SELECT_FROM,'自');
define(LANG_SELECT_TO,'至');
define(LANG_DIMENSIONS_FROM,'自');
define(LANG_DIMENSIONS_TO,'至');
define(LANG_BACK_TO_ALL_KEYWORDS,'浏览全部');
define(LANG_BACK_TO_ALL,'浏览全部');
define(LANG_SEE_ALL,'浏览全部');
define(LANG_SEE,'请看');
define(LANG_CLICK_IMAGE_TO_ENLARGE,'点这里放大图像');
define(LANG_CATALOGUE_RESONE,"作品全目录");
define(LANG_ATLAS_SHEET,"图集页");
define(LANG_GO_TO_ATLAS_SHEET,"直达 图集页");
define(LANG_MUSEUM_COLLECTION,"博物馆收藏");
define(LANG_SALES_HISTORY,"销售史");
define(LANG_SALES_HISTORY_UPON_REQUEST,"Contact auction house");
define(LANG_SALES_HISTORY_TB_ANNOUNCED,"To be announced");
define(LANG_OF,"之");
define(LANG_IN,"in");
define(LANG_SEARCH_RESULTS_ALL,"检索结果");
define(LANG_SEARCH_PAINTINGS,"搜寻");
define(LANG_SEARCH_RESULT, "搜寻结果");
define(LANG_SEARCH_RESULTS, "结果");
define(LANG_SEARCH_ART, "艺术");
define(LANG_SEARCH_BIOGRAPHY, "艺术家简介");
define(LANG_SEARCH_QUOTES, "引语");
define(LANG_SEARCH_EXHIBITIONS, "展览");
define(LANG_SEARCH_LITERATURE, "文献");
define(LANG_SEARCH_VIDEOS, "视频");
define(LANG_SEARCH_LINKS, "链接");
define(LANG_SEARCH_NEWS_TALKS, "讲演与讨论");
define(LANG_SEARCH_NEWS_AUCTIONS, "拍卖");
define(LANG_SEARCH_CREDITS, "鸣谢");
define(LANG_SEARCH_DISCLAIMER, "免责声");
define(LANG_SEARCH_CHRONOLOGY, "年表");
define(LANG_SEARCH_BASIC_TOP_VALUE_SEARCH, "快速检索");
define(LANG_SEARCH_SHOW_ALL, "显示所有");
define(LANG_SEARCH_YOUSEARCHEDFOR,"你搜寻了");
define(LANG_SEARCH_ALLPAINTINGS,"所有画作");
define(LANG_SEARCH_NUMBER,"编号");
define(LANG_SEARCH_ARTWORKTYPE,"作品类别");
define(LANG_SEARCH_TITLE,"标题");
define(LANG_SEARCH_LOCATION,"地点");
define(LANG_SEARCH_SHEETNUMBER,"图集页编号");
define(LANG_SEARCH_CRNUMBER,"作品全目录CR编号");
define(LANG_SEARCH_YEARFROM,"起始年");
define(LANG_SEARCH_YEAR,"年");
define(LANG_SEARCH_YEARTO,"终结年");
define(LANG_SEARCH_DATE,"日期");
define(LANG_SEARCH_COLOR,"Color");
define(LANG_SEARCH_HEIGHT,"高度");
define(LANG_SEARCH_WIDTH,"宽度");
define(LANG_SEARCH_NORESULTS,"抱歉，你的搜寻没有结果");
define(LANG_SEARCH_RESULTS1,"你的搜寻没有结果");
define(LANG_SEARCH_RESULTS2,"搜寻结果");
define(LANG_SEARCH_RESULTS3,"搜寻结果");
define(LANG_CATEGORY_DESC_MORE,"更多");
define(LANG_CATEGORY_DESC_CLOSE,"关闭");
define(LANG_SEARCH_RETURNTOTSEARCH,"回到上批搜寻结果");
define(LANG_USE_THIS,"利用拉下栏目跳到其他内容");
define(LANG_BACKTOTOP,"回到上面");
define(LANG_SHOW,"显示");
define(LANG_ALL,"全部");
define(LANG_PERPAGE,"张／每页");
define(LANG_IMAGES_PERPAGE,"Show");
define(LANG_BACKTOEXH,"回到展览");
define(LANG_GOBACK,"上一页");
define(LANG_SALEHIST_TAB_NOTES,"注解:");
define(LANG_ESTIMATE,"估价");
define(LANG_BOUGHT_IN,"未成交");
define(LANG_WITHDRAWN,"停止销售");
define(LANG_PREMIUM,"(包括买家佣金)");
define(LANG_HAMMER,"(落槌价)");
define(LANG_UNKNOWN,"Unknown");
define(LANG_LOT,"拍品编号");
define(LANG_SOLDPR,"售价");
define(LANG_SHOWING,"展示");
define(LANG_BOOKS_OF,"共");
define(LANG_BOOKS,"本书籍之");
define(LANG_BOOKS_BY,"作者");
define(LANG_BOOKS_DETAILS,"图书资料");
define(LANG_BOOKS_CLOSE,"关闭");
define(LANG_BOOKS_MORE,"更多");
define(LANG_BOOKS_MORE_DETAILS,"More Details");
define(LANG_BOOKS_LANGUAGES,"语言");
define(LANG_BOOKS_LANGUAGE,"语言");
define(LANG_BOOKS_CATEGORY,"种类");
define(LANG_CONTACT,"请利用此表与我们联系");
define(LANG_CONTACT_UNAVAILABLE,"网页架设中，目前无法读取，敬请原谅。");
define(LANG_CONTACT_UNAVAILABLE2,"如需与我们联系，请发邮件到 info@gerhard-richter.com");
define(LANG_BOOKS_LANGUAGE_EXHCATALOG,"展览");
define(LANG_BOOKS_READ_ARTICLE,"阅读文章");
define(LANG_COPY_TO_CLIPBOARD,"Copy link to clipboard");
define(LANG_PAINTING_SIZE_PARTS," 件一组");
define(LANG_SUBMITING_FORM,"submitting form.....");
define(LANG_FOUND,"found");
define(LANG_BOOKS_READ_ESSAY,"朗讀文章");
define(LANG_CR,"CR");
define(LANG_CR_EDITIONS,LANG_LEFT_SIDE_EDITIONS." ".LANG_CR);
define(LANG_CR_DRAWINGS,LANG_LEFT_SIDE_DRAWINGS." ".LANG_CR);
# DIFFERENT END

# biograpphy page
define(LANG_BIOGRAPHY,"艺术家简介");
define(LANG_WORK,"作品");
define(LANG_PHOTOS,"相片");
define(LANG_QUOTES,"引语");
define(LANG_QUOTES_NO_QUOTES_FOUND,"未搜寻到任何引语!");
define(LANG_QUOTES_SOURCE,"SOURCE");
# biograpphy page end

#chronology
define(LANG_CHRONOLOGY_H1_1, "年表");
define(LANG_CHRONOLOGY_H1_2, "by Amy Dickson");
define(LANG_AMY_DICKSON_THANKS, "Amy Dickson 编写制作里希特的年表不遗余力，在此对她特别致谢。《<i>Gerhard Richter: Panorama</i>》展览中所见的里希特年表是经由 Amy Dickson 编辑改写过的版本 (版权所有 &#169;Tate, 2011，经Tate Trustees 泰特美术馆理事会授权复制。)");
define(LANG_CHRONOLOGY_LEFT_1930, "1930s");
define(LANG_CHRONOLOGY_LEFT_1940, "1940s");
define(LANG_CHRONOLOGY_LEFT_1950, "1950s");
define(LANG_CHRONOLOGY_LEFT_1960, "1960s");
define(LANG_CHRONOLOGY_LEFT_1970, "1970s");
define(LANG_CHRONOLOGY_LEFT_1980, "1980s");
define(LANG_CHRONOLOGY_LEFT_1990, "1990s");
define(LANG_CHRONOLOGY_LEFT_2000, "2000s");
define(LANG_CHRONOLOGY_LEFT_2010, "2010s");
#chronology end

# exhibition
define(LANG_VIEW_EXHIBITION,"看看展览");
define(LANG_EXH_ONWARDS,"2020 年之后");
define(LANG_GROUP,"群展");
define(LANG_GROUP_EXH,"群展 所有展览");
define(LANG_GROUP_EXHS,"群展 所有展览");
define(LANG_SOLO,"个展");
define(LANG_SOLO_EXH,"个展 所有展览");
define(LANG_SOLO_EXHS,"个展 所有展览");
define(LANG_EXH_DECADES_S,"s");
define(LANG_EXH_SORT_BY,"Sort by");
define(LANG_EXH_SORT_BY_DATE,"Date");
define(LANG_EXH,"展览");
define(LANG_EXH_FOUND,"展览");
define(LANG_EXH_FOUND_S,"展览");
define(LANG_EXH_SHOW_ALL,"显示所有");
define(LANG_EXH_ARTWORK,"作品");
define(LANG_EXH_RELATED_EXHIBITIONS,"相关展览");
define(LANG_EXH_INST_SHOTS,"展场照");
define(LANG_EXH_LITERATURE,"文献");
define(LANG_EXH_VIDEOS,"视频");
define(LANG_EXH_GUIDE,"展览指南");
define(LANG_EXH_SHOW_ALL_WORKS,"看全部作品");
define(LANG_EXH_ARTWORK_TAB_WORKS,"作品");
define(LANG_EXH_ARTWORK_TAB_WORK,"作品");
define(LANG_EXH_INST_TAB_PHOTOS,"photos");
define(LANG_EXH_INST_TAB_PHOTO,"photo");
define(LANG_EXH_ARTWORK_SORT_BY,"排列依据");
define(LANG_EXH_ARTWORK_SORT_CR_NUMBER,"编号");
define(LANG_EXH_ARTWORK_SORT_YEAR,"创作年");
define(LANG_INSTALLSHOT,"装置截影");
define(LANG_ALL_EXH,"所有展览");
define(LANG_LEFT_SIDE_INSTALLATION_SOTS,"装置截影");
define(LANG_LEFT_SIDE_GUIDE,"展览指南");
define(LANG_LEFT_SIDE_EXH_TOUR,"展览导导览");
# END exhibition

# literature
define(LANG_LIT_LEFT_MONOGRAPHS,"专题论文");
define(LANG_LIT_LEFT_CATALOGUES,"展览目录");
define(LANG_LIT_LEFT_JOURNALS,"期刊");
define(LANG_LIT_LEFT_NEWS_ART,"报纸文章");
define(LANG_LIT_LEFT_ONL_PUBL,"网上出版品");
define(LANG_LIT_LEFT_OTHERS,"其他");
define(LANG_LIT_LEFT_FILMS,"影片");
define(LANG_LIT_LEFT_THESES,"论文");
define(LANG_LIT_LEFT_SOLO_EXH,"个展");
define(LANG_LIT_LEFT_GROUP_EXH,"群展");
define(LANG_LIT_LEFT_COLLECTIONS,"收藏");
define(LANG_LIT_ONWARDS,"2010年以后");
define(LANG_LIT_PUBLISHED,"出版年");
define(LANG_LIT_SEARCH,"文献搜寻");
define(LANG_LIT_BOOKS_PER,"册／每页");
define(LANG_LIT_SEARCH_HELP,"header=[Search for book help] body=[&#60;p&#62;如须搜寻特定书籍，请遵循搜寻准则。&#60;/p&#62;&#60;p&#62;你可以依标题、作者、标题兼作者，或是关键字来进行搜寻。&#60;/p&#62;&#60;p&#62;Examples:&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;J&uuml;rgen Schilling&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter, J&uuml;rgen Schilling &#60;/p&#62;&#60;p&#62;- &lsquo;Schilling&rsquo;&#60;/p&#62;]");
define(LANG_LIT_SEARCH_TITLE,"标题");
define(LANG_LIT_SEARCH_AUTHOR,"作者");
define(LANG_LIT_SEARCH_AUTHOR2,"作者");
define(LANG_LIT_SEARCH_DATE,"日期");
define(LANG_LIT_SEARCH_SORTBYTITLE,"依标题编排");
define(LANG_LIT_SEARCH_SORTBYAUTHOR,"依作者编排");
define(LANG_LIT_SEARCH_SORTBYDATE,"依日期编排");
define(LANG_LIT_SEARCH_CLICK,"点这里查看详细信息");
define(LANG_LIT_SEARCH_PUBBY,"出版社");
define(LANG_LIT_SEARCH_EDITOR,"编辑");
define(LANG_LIT_SEARCH_UNKNOWN,"未知");
define(LANG_LIT_SEARCH_ISBNNOTAVA,"isbn 不存在");
define(LANG_LIT_SEARCH_SEERELEXH,"看看展览");
define(LANG_LIT_SEARCH_SEERELEXH2,"看看展览");
define(LANG_LIT_SEARCH_GOTORELEXH,"到 相关展览");
define(LANG_LIT_SEARCH_GOTORELEXH2,"到 相关展览");
define(LANG_LIT_SEARCH_PAGES,"页");
define(LANG_LIT_SEARCH_NOFOUND,"无法搜寻到任何书籍，请再试一次。");
define(LANG_LIT_SEARCH_BACKTOLIT,"回到文献");
define(LANG_LIT_SEARCH_INFORMATION,"资讯");
define(LANG_LITERATURE_ENLARGE, "放大图像");
//define(LANG_LITERATURE_IN, "发表于");
define(LANG_LITERATURE_IN, "于");
define(LANG_LITERATURE_ISSUE, "期刊号");
define(LANG_LITERATURE_VOL, "Vol.");
define(LANG_LITERATURE_NO, "No.");
define(LANG_LITERATURE_PAGES, "p.");
define(LANG_LITERATURE_PAGESS, "pp.");
define(LANG_LITERATURE_COVER_HARDBACK, "精装");
define(LANG_LITERATURE_COVER_SOFTCOVER, "平装");
define(LANG_LITERATURE_COVER_UNKNOWNBINDING, "装订方式未知");
define(LANG_LITERATURE_COVER_SPECIALBINDING, "平装");
define(LANG_LITERATURE_ISBN, "国际标准书号");
define(LANG_LITERATURE_ISSN, "ISSN");
define(LANG_LITERATURE_DESC_TEXT, "Gerhard Richter's work and life has been the subject of numerous publications. The literature ranges from exhibition catalogues and collection catalogues to monographs, films and articles. Richter's own publications in the form of artist's books are an important part of his œuvre. Please find a selection of Richter related literature here.");
define(LANG_LITERATURE_SHOW_MORE_OPTIONS, "显示更多选项");
define(LANG_LITERATURE_SHOW_LESS_OPTIONS, "显示较少选项");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEO, "Related video");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS, "Related videos");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION, "This catalogue was published on the occasion of following exhibition:");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS, "This catalogue was published on the occasion of following exhibitions:");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE, "Related publication");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES, "Related publications");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK, "The following artwork is displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS, "The following artworks are displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TITLE, "本书籍");
define(LANG_LITERATURE_TITLES, "本书籍");
define(LANG_LITERATURE_SHOW_ALL,"显示所有");
# literature end

# video
define(LANG_VIDEO_ALLTITLES,"所有标题");
define(LANG_VIDEO_VIDEOS,"视频");
define(LANG_VIDEO_AUDIO,"音频");
define(LANG_AUDIO_CR,"作品全目录 CR:");
define(LANG_VIDEO_RUNTIME,"时长");
define(LANG_VIDEO_WATCH1,"以 Quicktime 观赏此视频");
define(LANG_VIDEO_WATCH2,"以 Flash 观赏此视频");
define(LANG_VIDEO_WATCH3,"Quicktime");
define(LANG_VIDEO_WATCH4,"Flash");
define(LANG_VIDEO_BACKTOVID,"回到视频");
define(LANG_VIDEO_TAB_INFO, "Information");
define(LANG_VIDEO_TAB_IMAGES, "Images");
define(LANG_VIDEO_ARTWORK_MENTIONED, "关于此作品的视频：");
define(LANG_VIDEO_EXHIBITION_MENTIONED, "This exhibition is featured in the following videos:");
define(LANG_VIDEO_IN_SECTION1, "此项目包含");
define(LANG_VIDEO_IN_SECTION2, "幅视频:");
# video end

# NEWS
define(LANG_NEWS_CURRENT,"目前进行中");
define(LANG_NEWS_UPCOMING,"即将进行");
define(LANG_NEWS_RESULTS,"结果");
define(LANG_NEWS_PUBLICATIONS,"新近出版品");
define(LANG_NEWS_TALKS,"讲演与讨论");
define(LANG_NEWS_AUCTIONS,"拍卖");
define(LANG_NEWS_EXHIBITIONS,"展览");
define(LANG_NEWS_ARCHIVE,"资料库");
define(LANG_NEWS_NO_AUCTIONS,"There are currently no artworks coming up for auction.");
define(LANG_NEWS_NOCURRENT,"无新闻");
define(LANG_NEWS_NO_TALKS,"No information currently available");
define(LANG_NEWS_NOUPCOMING,"无新闻");
define(LANG_NEWS_NOARCHIVE,"无资料库");
define(LANG_NEWS_NOPUBLICATIONS,"无新近出版品");
define(LANG_NEWS_DOWNLOADLEAFLET,"下载传单");
define(LANG_NEWS_ALSO_VISIT,"欢迎访问本站的社交网页");
define(LANG_NEWS_SHOW_ALL,"显示所有");
define(LANG_NEWS_DISCLIMER,"免责声明");
define(LANG_NEWS_DISCLIMER_TEXT1,"此处提供的所有信息仅供参考之用，信息来源为里希特官方网站（<a href='/' title='www.gerhard-richter.com'>www.gerhard-richter.com</a>）。我们致力于提供正确的最新信息，但不对本网站所提供直接表达或间接暗示的信息内容做任何保证，包括其完整性、正确性、可靠性、合适性或可取得性。您对本网站所有信息应用的后果请自行承担风险，因采用本网站信息或相关资料所导致的任何相关损失或损害，恕网站不承担任何法律或赔偿责任。");
define(LANG_NEWS_DISCLIMER_TEXT2,"本网站并非里希特的作品全目录、也无此意图。任何本网站所收录或未收录的作品，无法引以为据、代表或印证任何作品是否为里希特的真迹。");
define(LANG_NEWS_UPCOMING_FOR_AUCTION,"拍卖");
define(LANG_NEWS_UPCOMING_FOR_AUCTION_DESC,"The following artworks are coming up for auction:");
define(LANG_NEWS_REACEN_AUCTION_RES,"Results");
define(LANG_NEWS_REACEN_AUCTION_RES_DESC,"The following artworks were sold at auction in the last 6 months:");
define(LANG_NEWS_NEW_PUBLICATIONS,"Recent Publications");
define(LANG_NEWS_PUBLICATIONS_COUNT1,"There currently are");
define(LANG_NEWS_PUBLICATIONS_COUNT2,"new publications");
define(LANG_NEWS_NEW_TALKS,"讲演与讨论");
define(LANG_NEWS_AUCTIONS_AUCTIONHOUSE,"Auction House");
define(LANG_NEWS_AUCTIONS_DATE,"Date");
define(LANG_NEWS_RETURN_TO_NEWS_PAGE,"Return to news page");
# news end

#search box tooltip start
define(LANG_SEARCH_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_TIP_TEXT,"<p class=\"tip\"><span>编号:</span> 一组编号（利用逗号表示，如 \"5,137,211\" ）或一个范围内的编号（利用冒号表示，如 \"5:20\"）.</p><p class=\"tip\"><span>创作年:</span> 单年或数年 </p><p class=\"tip\"><span>Date:</span> an individual date, or range of dates.</p><p class=\"tip\"><span>尺寸:</span> 确切尺寸，或一个范围内尺寸（利用&#8216;从&#8217;和 &#8216;到 &#8217;空格选择）</p>");
#search box tooltip end

#search box EXH tooltip start
define(LANG_SEARCH_EXH_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_EXH_TIP_TEXT,"<p class=\"tip\"><span>标题:</span> 依标题搜寻</p><p class=\"tip\"><span>地点:</span> 展览地点</p> <p class=\"tip\"><span>关键字:</span> 依关键字搜寻</p> <p class=\"tip\"><span>年:</span> 单年或数年</p>");
#search box EXH tooltip end

#search box QUOTES tooltip start
define(LANG_SEARCH_QUOTES_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_QUOTES_TIP_TEXT,"<p class=\"tip\"><span>关键字:</span>依关键字搜寻</p> <p class=\"tip\"><span>年:</span> 单年或数年 </p> ");
#search box QUOTES tooltip end

#search box LIT tooltip start
define(LANG_SEARCH_LIT_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_LIT_TIP_TEXT,"<p class=\"tip\"><span>作者:</span> 依作者搜寻</p> <p class=\"tip\"><span>标题:</span> 依标题搜寻</p><p class=\"tip\"><span>关键字:</span> 根据关键字搜寻</p><p class=\"tip\"><span>类别:</span>依类别搜寻</p><p class=\"tip\"><span>语言:</span> 根据语言搜寻</p><p class=\"tip\"><span>年:</span> 依单年或数年搜寻</p>");
#search box LIT tooltip end

#search box ARTICLES tooltip start
define(LANG_SEARCH_ARTICLES_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_ARTICLES_TIP_TEXT,"<p class=\"tip\"><span>作者:</span> 依作者搜寻</p> <p class=\"tip\"><span>标题:</span> 依标题搜寻</p><p class=\"tip\"><span>关键字:</span> 根据关键字搜寻</p><p class=\"tip\"><span>年:</span> 依单年或数年搜寻</p>");
#search box ARTICLES tooltip end

#search box VIDEOS tooltip start
define(LANG_SEARCH_VIDEOS_TIP_TITLE,"搜寻协助");
define(LANG_SEARCH_VIDEOS_TIP_TEXT,"<p class=\"tip\"><span>关键字:</span> 依关键字搜寻</p>");
#search box VIDEOS tooltip end

#zoomer start
define(LANG_ZOOMER_OPEN,"显示细节");
define(LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE,"See Microsite");
#zoomer end

#virtual gallery
define(LANG_VIRTUALGALLERY_OPEN,"到 flash 相册中看这些图像");
define(LANG_VIRTUALGALLERY,"虚拟画廊");
#end

# MICROSITE
define(LANG_MICROSITE_TITLE_SINBAD,"Sinbad");
define(LANG_MICROSITE_SHOW,"显示");
define(LANG_MICROSITE_PER_PAGE,"张/每页");
define(LANG_MICROSITE_SEE_ALL,"全部");
define(LANG_MICROSITE_SEARCH_EDITION,"你可以在此浏览里希特所有的图像与摄影的有限复制品（限量版画）作品");
define(LANG_MICROSITE_ABOUT,"关于");
define(LANG_MICROSITE_SEARCH,"搜寻");
define(LANG_MICROSITE_SINBAD_TITLE,"里希特 Sinbad 限量版画");
define(LANG_MICROSITE_SINBAD_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_JS_SITENAME,"《Sinbad 辛巴达》");
define(LANG_MICROSITE_SINBAD_VIEW_AS_PAIR,"成对欣赏");
define(LANG_MICROSITE_SEARCH_HELP_TITLE,"Search Help");
define(LANG_MICROSITE_SEARCH_HELP_TEXT,"<em>Single edition =</em> type the edition number (eg; 23) <p><em>Multiple editions =</em> type each edition number, separated by commas (eg; 23, 32, 55)</p> <p><em>Groups =</em> type the start & end number of the group, separated by a colon (eg; 23:55)</p>");
define(LANG_MICROSITE_EDITIONS_CR,"Editions CR");
define(LANG_MICROSITE_IMAGE,"Image");
define(LANG_MICROSITE_OF,"of");
define(LANG_MICROSITE_NOVEMBER_FOOTER_1,"For further information on Gerhard Richter and his work,<br />please visit");
define(LANG_MICROSITE_NOVEMBER_FOOTER_2,"and learn about his life and work.");
# MICROSITE END

# OVERPAINTED PHOTOGRAPHS FORM
define(LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS,"里希特油彩涂抹相片作品全目录");
define(LANG_OPP_FORM_CATALOGUE_RAISONNE,"作品全目录是完整、系统化、详尽且学术严谨的参考文本，目的在于建立一套明确的真品作品集。目录里包含的每件作品都经过考证后，认定为由某特定艺术家创作，并附有作品图像与详细说明和描述。作品全目录提供的作品信息包括标题、创作日期、媒介、尺寸、题款、出处、展览史，以及著录，并为每件作品做永久性的参考编号。");
//define(LANG_OPP_FORM_1,"里希特油彩涂抹的相片 <span style='border-bottom:1px dotted #fff;' class='help' title='".LANG_OPP_FORM_CATALOGUE_RAISONNE."'>作品全目录</span> 预计在2015年出版。");
define(LANG_OPP_FORM_1,"里希特油彩涂抹相片作品全目录，预计于2017年出版。");
define(LANG_OPP_FORM_1_1,"若需要更多资讯、或提供作品讯息，请点选");
define(LANG_OPP_FORM_1_2,"此处");
define(LANG_OPP_FORM_2,"里希特已经创作了大量的油彩涂抹的相片，并且目前仍然持续有新作品的完成，大部分的这类作品目前仍不为专家和一般大众所知，这份作品全目录将编录里希特的该类作品，展示他对这类媒介的创作成就。");
define(LANG_OPP_FORM_3,"希特油彩涂抹相片作品全目录的出版，旨在涵盖所有已知的该类作品，并提供每份作品的图像，届时该图录将成为具权威性的资料，尽可能提供所有经鉴定的里希特该类作品真品的完整清单。");
define(LANG_OPP_FORM_4,"为此，我们目前正全力收集关于每一份已知的该类作品的资料：");
define(LANG_OPP_FORM_5,"标题");
define(LANG_OPP_FORM_6,"创作年");
define(LANG_OPP_FORM_7,"媒介");
define(LANG_OPP_FORM_8,"尺寸");
define(LANG_OPP_FORM_9,"出处");
define(LANG_OPP_FORM_10,"展览史");
define(LANG_OPP_FORM_11,"著录");
define(LANG_OPP_FORM_12,"其他相关信息");
define(LANG_OPP_FORM_13,"如果有私人藏家或公共收藏愿意提供其收藏的彩涂抹的相片作品信息，以编录于该作品全目录之内，我们将非常感激。");
define(LANG_OPP_FORM_14,"虽然我们希望您能提供尽可能详尽的信息，但我们非常理解某些个人或私人的信息具有高度敏感性，因此下表内若有部分内容您不便提供，请别介意。");
define(LANG_OPP_FORM_15,"我们将确保您所提供的所有信息都受到严格的隐私保护，也将严正尊重所有匿名的要求。");
define(LANG_OPP_FORM_16,"我们至诚感谢您对本项目的支持与投入。");
define(LANG_OPP_FORM_17,"作品提交表");
define(LANG_OPP_FORM_18,"下列资料将列入该作品全目录筹备的考量中，但此表仅供研究与建档之用，您在此提供的信息未必都会列入作品全目录最终发表的资料中。");
define(LANG_OPP_FORM_19,"倘若您无法回答标中的某个问题，请保持该栏空白。您若需要协助填写此表，请以电子邮件随时与我们联系，");
define(LANG_OPP_FORM_20,"作品标题");
define(LANG_OPP_FORM_21_1,"日/月/年");
define(LANG_OPP_FORM_21,"日期");
define(LANG_OPP_FORM_22,"媒介");
define(LANG_OPP_FORM_23,"彩色相片上油彩");
define(LANG_OPP_FORM_24,"黑白相片上油彩");
define(LANG_OPP_FORM_25,"彩色相片上瓷釉");
define(LANG_OPP_FORM_26,"黑白相片上瓷釉");
define(LANG_OPP_FORM_27,"相片尺寸  (高 &#215; 宽)");
define(LANG_OPP_FORM_28,"公分");
define(LANG_OPP_FORM_29,"英吋");
define(LANG_OPP_FORM_30,"装裱尺寸  (高 &#215; 宽)");
define(LANG_OPP_FORM_31,"请注明您希望鸣谢语在作品全目录中显示的形式，倘若您希望匿名，请注明‘私人收藏 (private collection)，若您不介意，也可以附上你所在的地理位置(城市或国家等)。");
define(LANG_OPP_FORM_32,"鸣谢 / 资料或图片提供");
define(LANG_OPP_FORM_33,"系列 / 限量版画(限量版复制品)");
define(LANG_OPP_FORM_35,"Firenze (edition)");
define(LANG_OPP_FORM_36,"Florence (series)");
define(LANG_OPP_FORM_37,"Grauwald");
define(LANG_OPP_FORM_38,"Museum Visit");
define(LANG_OPP_FORM_38_1,"Sils");
define(LANG_OPP_FORM_38_2,"Wald");
define(LANG_OPP_FORM_38_3,"128 Fotos von einem Bild (Halifax 1978)");
define(LANG_OPP_FORM_38_4,"8.2.92");
define(LANG_OPP_FORM_39,"系列 / 版画编号 <br />（如适用）");
define(LANG_OPP_FORM_40_1,"正面:");
define(LANG_OPP_FORM_40_1_1,"签名");
define(LANG_OPP_FORM_40_1_2,"作品日期");
define(LANG_OPP_FORM_40_1_3,"版数");
define(LANG_OPP_FORM_40_1_4,"落款");
define(LANG_OPP_FORM_40_2,"背面:");
define(LANG_OPP_FORM_40_2_1,"签名");
define(LANG_OPP_FORM_40_2_2,"作品日期");
define(LANG_OPP_FORM_40_2_3,"版数");
define(LANG_OPP_FORM_40_2_4,"落款");
define(LANG_OPP_FORM_41,"签于装裱上");
define(LANG_OPP_FORM_41_1,"on backing board");
define(LANG_OPP_FORM_42,"签于照片上");
define(LANG_OPP_FORM_42_1,"反面");
define(LANG_OPP_FORM_43,"左上方");
define(LANG_OPP_FORM_44,"上方中间");
define(LANG_OPP_FORM_45,"右上方");
define(LANG_OPP_FORM_46,"中段左边");
define(LANG_OPP_FORM_47,"中段中间");
define(LANG_OPP_FORM_48,"中段右边");
define(LANG_OPP_FORM_49,"左下方");
define(LANG_OPP_FORM_50,"下方中间");
define(LANG_OPP_FORM_51,"右下方");
define(LANG_OPP_FORM_53,"其他题款");
define(LANG_OPP_FORM_54,"其他记号 / 背面的标签");
define(LANG_OPP_FORM_55,"上载图像");
define(LANG_OPP_FORM_55_1,"可上传多张图像");
define(LANG_OPP_FORM_56,"若能提供至少300dpi的高分辨率图像，我们将感激不尽（任何图像皆如此，包括有艺术家签名的装裱相片在内）。");
define(LANG_OPP_FORM_57,"您偏好的图像形式: JPEG, TIFF, PSD, PNG");
define(LANG_OPP_FORM_58,"版权 / 摄影师");
define(LANG_OPP_FORM_59,"出处");
define(LANG_OPP_FORM_60,"购买自");
define(LANG_OPP_FORM_61,"购买日期");
define(LANG_OPP_FORM_62,"展览史(该作品曾在下列展览中展出)");
define(LANG_OPP_FORM_62_1,"Exhibition history other");
define(LANG_OPP_FORM_63,"著录(该作品曾收录在下列书目中)");
define(LANG_OPP_FORM_63_1,"Publication history other");
define(LANG_OPP_FORM_64,"个人资料");
define(LANG_OPP_FORM_65,"名");
define(LANG_OPP_FORM_66,"姓");
define(LANG_OPP_FORM_67,"电子邮件");
define(LANG_OPP_FORM_68,"地址");
define(LANG_OPP_FORM_68_1,"邮区编码");
define(LANG_OPP_FORM_68_2,"国家");
define(LANG_OPP_FORM_69,"电话");
define(LANG_OPP_FORM_70,"其他说明");
define(LANG_OPP_FORM_71,"必填栏位");
define(LANG_OPP_FORM_72,"一旦发送了此表格，即意味您能确保表格内提供的信息都正确无误，且您对于该作品相关的所有信息及作品出处和收藏历史都无蓄意保留。");
define(LANG_OPP_FORM_73,"作品拥有人须理解，发送此表格并不保证此作品将收录于作品全目录中。");
define(LANG_OPP_FORM_74,"发送表格");
define(LANG_OPP_FORM_75,"请阅读上一页声明，并请勾选表示了解及同意本网站使用条款。");
define(LANG_OPP_FORM_75_2,"Please fill in all required fields!");
define(LANG_OPP_FORM_75_3,"Verification characters you entered where not correct please try again!");
define(LANG_OPP_FORM_76,"您填写的表格已经送出。");
define(LANG_OPP_FORM_77,"感谢您宝贵的时间与协助。");
define(LANG_OPP_FORM_77_2,"打印资料");

# exhibition list
define(LANG_OPP_FORM_78_9_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_78_9_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_78_9_D,", May &#8211; June 2016");
define(LANG_OPP_FORM_78_8_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_78_8_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_78_8_D,", November &#8211; December 2015");
define(LANG_OPP_FORM_78_7_T,"Gerhard Richter. Das kleine Format");
define(LANG_OPP_FORM_78_7_L,", Galerie Schwarzer, Dusseldorf");
define(LANG_OPP_FORM_78_7_D,", April &#8211; June 2015");
define(LANG_OPP_FORM_78_6_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_6_L,", The Lone T Art Space, Milan");
define(LANG_OPP_FORM_78_6_D,", April &#8211; May 2015");
define(LANG_OPP_FORM_78_5_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_78_5_L,", Hannah Hoffman Gallery, Los Angeles");
define(LANG_OPP_FORM_78_5_D,", March  &#8211; April 2015");
define(LANG_OPP_FORM_78_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_4_L,", Repetto Gallery, London");
define(LANG_OPP_FORM_78_4_D,", February &#8211; March 2015");
define(LANG_OPP_FORM_78_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_3_L,", Marian Goodman Gallery, London");
define(LANG_OPP_FORM_78_3_D,", October &#8211; December 2014");
define(LANG_OPP_FORM_78_1_T,"What is a Photograph");
define(LANG_OPP_FORM_78_1_L,", International Center of Photography, New York");
define(LANG_OPP_FORM_78_1_L,", New York");
define(LANG_OPP_FORM_78_1_D,", January &#8211; May 2014");
define(LANG_OPP_FORM_78_2_T,"Purer Zufall. Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_78_2_L,", Sprengel Museum Hannover, Hanover");
define(LANG_OPP_FORM_78_2_D,", May &#8211; September 2013");
define(LANG_OPP_FORM_78_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_L,", Beirut Art Center, Beirut");
define(LANG_OPP_FORM_78_D,", April &#8211; June 2012");
define(LANG_OPP_FORM_79_T,"Gerhard Richter &#8211; Arbeiten 1968-2008");
define(LANG_OPP_FORM_79_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_79_D,", November 2011 &#8211; January 2012");
define(LANG_OPP_FORM_80_T,"Editionen und übermalte Fotografien");
define(LANG_OPP_FORM_80_L,", Wolfram Völcker Fine Art, Berlin");
define(LANG_OPP_FORM_80_D,", September &#8211; October 2011");
define(LANG_OPP_FORM_81_T,"De-Natured: German Art From Joseph Beuys to Martin Kippenberger");
define(LANG_OPP_FORM_81_L,", Ackland Art, Museum, Chapel Hill");
define(LANG_OPP_FORM_81_D,", April &#8211; July 2011");
define(LANG_OPP_FORM_82_T,"Gerhard Richter “New Overpainted Photographs“");
define(LANG_OPP_FORM_82_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_82_D,", February &#8211; March 2010");
define(LANG_OPP_FORM_82_1_T,"Best of Fifty Years");
define(LANG_OPP_FORM_82_1_L,", Kunstverein Wolfsburg");
define(LANG_OPP_FORM_82_1_D,", November 2009 &#8211; February 2010");
define(LANG_OPP_FORM_83_T,"Photo España 2009: Gerhard Richter, Fotografías pintadas");
define(LANG_OPP_FORM_83_L,", Fundación Telefónica, Madrid");
define(LANG_OPP_FORM_83_D,", June 2009");
define(LANG_OPP_FORM_84_T,"The Photographic Object");
define(LANG_OPP_FORM_84_L,", Photographers’ Gallery, London");
define(LANG_OPP_FORM_84_D,", April &#8211; June 2009");
define(LANG_OPP_FORM_85_T,"Gerhard Richter &#8211; Übermalte Fotografien &#8211; Photographies Peintes");
define(LANG_OPP_FORM_85_L,", Centre de la photographie, Geneva");
define(LANG_OPP_FORM_85_D,", February &#8211; May 2009");
define(LANG_OPP_FORM_86_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_86_L,", Museum Morsbroich, Leverkusen");
define(LANG_OPP_FORM_86_D,", October 2008 &#8211; January 2009");
define(LANG_OPP_FORM_86_1_T,"Gerhard Richter “New Works“");
define(LANG_OPP_FORM_86_1_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_86_1_D,", November &#8211; December 2005");
define(LANG_OPP_FORM_87_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_87_L,", Anthony Meier Fine Arts, San Francisco");
define(LANG_OPP_FORM_87_D,", June &#8211; August 2005");
define(LANG_OPP_FORM_88_T,"Gerhard Richter &#8211; Editionen 1968 &#8211; 2004");
define(LANG_OPP_FORM_88_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_88_D,", June &#8211; July 2005");
define(LANG_OPP_FORM_89_T,"Attack: Attraction Painting/Photography");
define(LANG_OPP_FORM_89_L,", Marcel Sitcoske Gallery, San Francisco");
define(LANG_OPP_FORM_89_D,", December 2002 &#8211; February 2003");
define(LANG_OPP_FORM_90_T,"Gerhard Richter");
define(LANG_OPP_FORM_90_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_90_D,", December 2002 &#8211; January 2003");
define(LANG_OPP_FORM_91_T,"Gerhard Richter &#8211; Firenze");
define(LANG_OPP_FORM_91_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_91_D,", June &#8211; August 2002");
define(LANG_OPP_FORM_92_T,"Gerhard Richter &#8211; Editionen 1969 &#8211; 1998");
define(LANG_OPP_FORM_92_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_92_D,", March &#8211; May 2001");
define(LANG_OPP_FORM_93_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_93_L,", Galerie Fred Jahn, Munich");
define(LANG_OPP_FORM_93_D,", October 2000");
define(LANG_OPP_FORM_93_1_T,"Gerhard Richter &#8211; Bilder 1972 &#8211; 1996");
define(LANG_OPP_FORM_93_1_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_1_D,", May &#8211; July 1998");
define(LANG_OPP_FORM_93_2_T,"Gerhard Richter");
define(LANG_OPP_FORM_93_2_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_2_D,", October &#8211; November 1997");
define(LANG_OPP_FORM_93_3_T,"Gerhard Richter Part II: Painting, Mirror Painting, Watercolour, Photograph, Print");
define(LANG_OPP_FORM_93_3_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_3_D,", May 1996");
define(LANG_OPP_FORM_93_4_T,"Gerhard Richter &#8211; Editionen 1967 &#8211; 1993");
define(LANG_OPP_FORM_93_4_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_4_D,", January &#8211; March 1994");
define(LANG_OPP_FORM_93_5_T,"Gerhard Richter: Sils");
define(LANG_OPP_FORM_93_5_L,", Nietzsche-Haus, Sils-Maria");
define(LANG_OPP_FORM_93_5_D,", July 1992 &#8211; March 1993");
define(LANG_OPP_FORM_94_T,"其他展览");
define(LANG_OPP_FORM_94_L,"");
define(LANG_OPP_FORM_94_D,"");

# publications list
define(LANG_OPP_FORM_95_6_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_95_6_L,". Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_95_6_D,", 2015");
define(LANG_OPP_FORM_95_5_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_95_5_L," (Buchloh, Benjamin H.D., Schwarz, Dieter). Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_95_5_D,", 2016");
define(LANG_OPP_FORM_95_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_95_4_L," (Barbero, Luca Massimo). Nuvole Rosse");
define(LANG_OPP_FORM_95_4_D,", 2015");
define(LANG_OPP_FORM_95_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_95_3_L," (Buchloh, Benjamin H.D., Schwarz, Dieter, Storr, Robert). London: Marian Goodman Gallery");
define(LANG_OPP_FORM_95_3_D,", 2014");
define(LANG_OPP_FORM_95_2_T,"What is a Photograph");
define(LANG_OPP_FORM_95_2_L," (Squiers, Carol, Baker, George, Batchen, Geoffrey, Steyerl, Hito). New York: Prestel");
define(LANG_OPP_FORM_95_2_D,", 2014");
define(LANG_OPP_FORM_95_T,"Purer Zufall &#8211; Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_95_L," (Krempel, Ulrich, Rist, Annerose, Schwarz, Isabelle). Hanover: Sprengel Museum");
define(LANG_OPP_FORM_95_D,", 2013");
define(LANG_OPP_FORM_95_1_T,"Beirut");
define(LANG_OPP_FORM_95_1_L," (Borchardt-Hume, Achim, Joreige, Lamia, Dagner, Sandra). Beirut: Beirut Art Center; London: Heni Publishing; Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_95_1_D,", 2012");
define(LANG_OPP_FORM_96_T,"Best of Fifty Years");
define(LANG_OPP_FORM_96_L," (Justin Hoffmann/Kunstverein Wolfsburg). Wolfsburg: Kunstverein Wolfsburg");
define(LANG_OPP_FORM_96_D,", 2010");
define(LANG_OPP_FORM_97_T,"Gerhard Richter. Obrist &#8211; O’Brist");
define(LANG_OPP_FORM_97_L," (Obrist, Hans Ulrich). Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_97_D,", 2009");
define(LANG_OPP_FORM_97_1_T,"Gerhard Richter. Overpainted Photographs");
define(LANG_OPP_FORM_97_1_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_97_1_D,", 2008");
define(LANG_OPP_FORM_98_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_98_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_98_D,", 2008");
define(LANG_OPP_FORM_99_T,"Gerhard Richter.");
define(LANG_OPP_FORM_99_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_99_D,", 2005");
define(LANG_OPP_FORM_100_T,"City Life");
define(LANG_OPP_FORM_100_L," (Cora, Bruno, Restagno, Enzo, Gori, Giuliano). Prato: Gli Ori");
define(LANG_OPP_FORM_100_D,", 2002");
define(LANG_OPP_FORM_100_1_T,"Gerhard Richter.");
define(LANG_OPP_FORM_100_1_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_1_D,", 2002");
define(LANG_OPP_FORM_100_2_T,"Sils");
define(LANG_OPP_FORM_100_2_L," (eds. Obrist, Hans Ulrich, Bloch, Peter Andre). Munich: Oktagon, 1992; New York: D. A. P., Distributed Art Publishers");
define(LANG_OPP_FORM_100_2_D,", 2002 ");
define(LANG_OPP_FORM_100_3_T,"Florence");
define(LANG_OPP_FORM_100_3_L," (Elger, Dietmar). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_100_3_D,", 2001");
define(LANG_OPP_FORM_100_4_T,"Gerhard Richter, Part I: New Painting; Part II, Part II: Painting, Mirror Painting, Watercolour, Photograph, Print.");
define(LANG_OPP_FORM_100_4_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_4_D,", 1996");
define(LANG_OPP_FORM_100_5_T,"其他出版品");
define(LANG_OPP_FORM_100_5_L,"");
define(LANG_OPP_FORM_100_5_D,"");

define(LANG_OPP_FORM_103,"请以电邮寄送给我一份资料。");
define(LANG_OPP_FORM_104,"Reference number");
# END OVERPAINTED PHOTOGRAPHS FORM

# TABLE
define(LANG_TABLE_DESC_PUBLICATION_RELATED,"篇相关文献");
define(LANG_TABLE_DESC_PUBLICATION,"");
define(LANG_TABLE_DESC_PUBLICATIONS,"");
define(LANG_TABLE_TITLE,"书名");
define(LANG_TABLE_FURTHER_ARTWORKS_IN_LOT,"Further artworks in this lot:");
# END TABLE

# Search
define(LANG_SEARCH_SHOW_MORE_OPTIONS,"Show more options");
# END Search

# share button
define(LANG_SHARE_THIS_PAGE,"Share this page");
define(LANG_SHARE_EMAIL_SUBJECT,"Visit this page on gerhard-richter.com");
define(LANG_SHARE_EMAIL_BODY_1,"Dear friend,");
define(LANG_SHARE_EMAIL_BODY_2,"I would like to share the following page with you:");
define(LANG_SHARE_EMAIL_BODY_3,"Kind regards,");
define(LANG_SHARE_EMAIL,"Email");
# END share button

# Mobile site specific
define(LANG_MOBILE_SEARCH_FIND,"Find");
# END Mobile site specific
?>
