<?php
# meta info
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION_HOME,"Erfahren Sie mehr über das Leben und Werk von Gerhard Richter, einem der wichtigsten Künstler des 20. und 21. Jahrhunderts.");
define(LANG_META_DESCRIPTION_ART,"Entdecken Sie Richters vielfältiges Werk, welches Gemälde, Arbeiten auf Papier sowie übermalte Fotografien umfasst und auch den Atlas einschließt, eine Sammlung von Fotografien, Zeitungsausschnitten und Skizzen.");
define(LANG_META_DESCRIPTION_BIOGRAPHY,"Die Biografie gibt Aufschluss über Gerhard Richter Werdegang. Im Jahr 1932 in Dresden geboren, wuchs Richter im Dritten Reich auf und erhielt eine erste künstlerische Ausbildung in der DDR. 1961 siedelte er nach Westdeutschland über, wo er sich zu einem der bedeutendsten Künstlern seiner Zeit entwickelte.");
define(LANG_META_DESCRIPTION_CHRONOLOGY,"Ein konzentrierter Überblick über Richters Werdegang: Wichtige Stationen in Richters Privatleben werden bedeutenden künstlerischen Errungenschaften gegenübergestellt.");
define(LANG_META_DESCRIPTION_QUOTES,"Eine Auswahl an Zitaten Richters gibt Einblick in seine Arbeitsweise, sein Kunstverständnis und sein Weltbild.");
define(LANG_META_DESCRIPTION_EXHIBITIONS,"Entdecken Sie über 50 Jahre Ausstellungsgeschichte von Gerhard Richter's Arbeiten.");
define(LANG_META_DESCRIPTION_LITERATURE,"Recherchieren Sie Richters Künstlerbücher, Publikationen über sein Werk sowie Zeitungsartikel und Onlinepublikationen.");
define(LANG_META_DESCRIPTION_VIDEOS,"Schauen Sie sich Videos über die letzten Richter-Ausstellungen an, über ausgewählte Werkgruppen und Vorträge von Experten.");
define(LANG_META_DESCRIPTION_LINKS,"Hier finden Sie Links zu Zeitungs- und Zeitschriftenartikeln sowie zu Galerien, die mit Richters Arbeiten handeln.");
define(LANG_META_DESCRIPTION_NEWS,"Aktuelle Informationen: laufende Ausstellungen, anstehende Auktionen und neue Publikationen finden Sie hier.");
# END meta info

#PAGE TITLES
if( $_SESSION['kiosk'] ) define(LANG_TITLE_HOME,"Gerhard-Richter.com");
else define(LANG_TITLE_HOME,"Gerhard Richter");
#PAGE TITLES END

# MENU
define(LANG_HEADER_MENU_HOME,'Home');
define(LANG_HEADER_MENU_HOME_TT,'Gerhard Richter Home Page');
define(LANG_HEADER_MENU_ARTWORK,'Kunst');
define(LANG_HEADER_MENU_ARTWORK_TT,'Gerhard Richters Kunst umfasst Arbeiten, Editionen, &Uuml;bermalte Fotografien, Aquarelle und Zeichnungen');
define(LANG_HEADER_MENU_BIOGRAPHY,'Biographie');
define(LANG_HEADER_MENU_BIOGRAPHY_TT,'Gerhard Richter Lebensgeschichte');
define(LANG_HEADER_MENU_CHRONOLOGY,'Chronologie');
define(LANG_HEADER_MENU_CHRONOLOGY_TT,'Gerhard Richter chronologie');
define(LANG_HEADER_MENU_QUOTES,'Zitate');
define(LANG_HEADER_MENU_QUOTES_TT,'Gerhard Richter zitate');
define(LANG_HEADER_MENU_EXHIBITIONS,'Ausstellungen');
define(LANG_HEADER_MENU_EXHIBITIONS_TT,'Gerhard Richter Weltweite Ausstellungen');
define(LANG_HEADER_MENU_LITERATURE,'Literatur');
define(LANG_HEADER_MENU_LITERATURE_TT,'Gerhard Richter B&uuml;cher');
define(LANG_HEADER_MENU_VIDEOS,'Videos');
define(LANG_HEADER_MENU_VIDEOS_RELATED,'Weitere Videos');
define(LANG_HEADER_MENU_VIDEOS_TT,'Gerhard Richter Videos und Interviews');
define(LANG_HEADER_MENU_AUDIO,'Audio');
define(LANG_HEADER_MENU_AUDIO_TT,'Gerhard Richter audios');
define(LANG_HEADER_MENU_MEDIA,'Media');
define(LANG_HEADER_MENU_MEDIA_TT,'Gerhard Richter media');
define(LANG_HEADER_MENU_CONTACT,'Links');
define(LANG_HEADER_MENU_CONTACT_TT,'Gerhard Richter Links');
define(LANG_HEADER_MENU_NEWS,'News');
define(LANG_HEADER_MENU_NEWS_TT,'Gerhard Richter news');
define(LANG_HEADER_MENU_SEARCH,'Suche');
define(LANG_HEADER_MENU_SEARCH_TT,'Gerhard Richter suche');
define(LANG_NEWS,'News');
define(LANG_HEADER_MENU_ADMIN,'Verwaltung');
# MENU END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_xsmall.png');
define(LANG_NO_IMAGE_SMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_small.png');
define(LANG_NO_IMAGE_MEDIUM,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_LARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_XLARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_ORIGINAL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_THUMBNAIL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_thumbnail.png');
# IF NO IMAGE To DISPLAY END

#LEFT SIDE
define(LANG_LEFT_SIDE_PAINTINGS,'Bilder');
define(LANG_LEFT_SIDE_CATEGORIES,'Kategorien');
define(LANG_LEFT_SIDE_ATLAS,'Atlas');
define(LANG_LEFT_SIDE_EDITIONS,'Editionen');
define(LANG_LEFT_SIDE_YEARS,'Jahre');
define(LANG_LEFT_SIDE_DRAWINGS,'Zeichnungen');
define(LANG_LEFT_SIDE_OILSONPAPER,'Öl auf Papier');
define(LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS,'Übermalte Fotografien');
define(LANG_LEFT_SIDE_WATERCOLOURS,'Aquarelle');
define(LANG_LEFT_SIDE_OTHER,'Sonstiges');
define(LANG_LEFT_SIDE_PRINTS,"Drucke");
define(LANG_LEFT_SIDE_MICROSITES,'Ausgewählte Projekte');
#LEFT SIDE END

#RIGHT SIDE
define(LANG_RIGHT_SEARCH_H1,'Suche');
define(LANG_RIGHT_SEARCH_EXH_H1,'Suche');
define(LANG_RIGHT_SEARCH_QUOTES_H1,'Suche');
define(LANG_RIGHT_SEARCH_VIDEOS_H1,'Suche');
define(LANG_RIGHT_SEARCH_SEARCHNOW,'Suche starten');
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE1,"nach");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE2,"Medium");
define(LANG_RIGHT_SEARCH_BYTITLE1,"nach");
define(LANG_RIGHT_SEARCH_BYTITLE2,"Titel");
define(LANG_RIGHT_SEARCH_BY,"nach");
define(LANG_RIGHT_SEARCH_BYPAINTINGS,"WVZ-Nummer (Bilder)");
define(LANG_RIGHT_SEARCH_BYATLAS,"Blatt");
define(LANG_RIGHT_SEARCH_BYEDITION,"WVZ-Nummer (Editionen)");
define(LANG_RIGHT_SEARCH_BYDRAWINGS,"WVZ-Nummer (Zeichnungen)");
define(LANG_RIGHT_SEARCH_BYNUMBER,"WVZ-Nummer");
define(LANG_RIGHT_SEARCH_ADVANCED_SEARCH,"Erweiterte Suche");
define(LANG_RIGHT_SEARCH_QUICK_SEARCH,"Schnellsuche");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST,"Liste");
define(LANG_RIGHT_SEARCH_BYNUMBER_RANGE,"Reihe");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE,"Hinzufügen");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE,"Entfernen");
define(LANG_RIGHT_SEARCH_YEARPAINTED1,"nach");
define(LANG_RIGHT_SEARCH_YEARPAINTED2,"Jahr");
define(LANG_RIGHT_SEARCH_DATE,"Datum");
define(LANG_RIGHT_SEARCH_DATE_FROM,"von");
define(LANG_RIGHT_SEARCH_DATE_TO,"bis");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_D,"TT");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_M,"MM");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y,"JJJJ");
define(LANG_RIGHT_SEARCH_COLOR1,"nach");
define(LANG_RIGHT_SEARCH_COLOR2,"Farbe");
define(LANG_RIGHT_SEARCH_BYSIZE,"Gr&ouml;&szlig;e");
define(LANG_RIGHT_SEARCH_BYSIZEH,"H");
define(LANG_RIGHT_SEARCH_BYSIZEH1,"Höhe in cm");
define(LANG_RIGHT_SEARCH_BYSIZEW,"W");
define(LANG_RIGHT_SEARCH_BYSIZEW1,"Weite in cm");
define(LANG_RIGHT_SEARCH_BYSIZEMIN,"Min");
define(LANG_RIGHT_SEARCH_BYSIZEMAX,"Max");
define(LANG_RIGHT_SEARCH_BY_COLLECTION,"nach <strong>Ort</strong>");
define(LANG_RIGHT_SEARCH_SEARCHBUTTON,"Suche");
define(LANG_RIGHT_SEARCH_SEARCHRESET,"L&ouml;schen");
define(LANG_RIGHT_SEARCH_EXH_LOCATION,"Ort");
define(LANG_RIGHT_SEARCH_EXH_TITLE,"Titel");
define(LANG_RIGHT_SEARCH_EXH_KEYWORD,"Stichwort");
define(LANG_RIGHT_SEARCH_QUOTES_KEYWORD,"Stichwort");
define(LANG_RIGHT_SEARCH_VIDEOS_KEYWORD,"Stichwort");
define(LANG_RIGHT_SEARCH_QUOTES_SEARCHALL,"Alle Zitate anzeigen");
define(LANG_RIGHT_SEARCH_LIT_AUTHOR,"Autor");
define(LANG_RIGHT_SEARCH_LIT_TITLE,"Titel");
define(LANG_RIGHT_SEARCH_LIT_KEYWORD,"Stichwort");
define(LANG_RIGHT_SEARCH_LIT_CATEGORY,"Kategorien");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT,'Auswahl...');
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE,'Sprache');
define(LANG_RIGHT_FEATURED_TITLES,"Ausgewählte Projekte");
define(LANG_RIGHT_FEATURED_TITLES_SNOWWHITE,"Snow White");
define(LANG_RIGHT_FEATURED_TITLES_WARCUT,"War Cut II");
define(LANG_RIGHT_FEATURED_TITLES_FIRENZE,"Firenze");
define(LANG_RIGHT_FEATURED_TITLES_4900COLOURS,"4900 Colours");
define(LANG_RIGHT_FEATURED_TITLES_SINBAD,"Sindbad");
define(LANG_RIGHT_FEATURED_TITLES_ELBE,"Elbe");
define(LANG_RIGHT_FEATURED_TITLES_COLOGNE_CATHEDRAL,"Cologne cathedral");
define(LANG_RIGHT_TIMELINE,"<span class='related'>Zeitleiste</span><br />Erkunden Sie die Karriere des Künstlers.");
define(LANG_RIGHT_TIMELINE_INTER,"Interaktive Zeitleiste");
define(LANG_RIGHT_VIRTUALEXH,"<span class='related'>Virtuelle</span><br />Ausstellung");
define(LANG_RIGHT_FETURED_TITLES,"Ausgewählte Titel");
define(LANG_RIGHT_VIDEO,"Um diese Videos anschauen zu können, müssen Sie entweder Flash auf Ihrem Computer installiert haben.");
define(LANG_RIGHT_VIDEO_DOWN,"Herunterladen");
define(LANG_RIGHT_AUDIO,"Sie müssen Flash auf Ihrem Computer installiert haben, um dieses Audioformat anhören zu können.");
define(LANG_RELATED_BLOCK_BROWSE_CLIPS,"Alle Clips");
define(LANG_RELATED_BLOCK_RECENTLY,"Zuletzt Hinzugefügt");
#RIGHT SIDE END

# credits page
define(LANG_CREDITS,'Impressum');
define(LANG_CREDITS_TEXT1_1,'Die offizielle Website des Künstlers Gerhard Richter');
define(LANG_CREDITS_TEXT1_2,'wurde von Joe Hage erstellt und wird von ihm betrieben.');
define(LANG_CREDITS_TEXT2_1,'Wir bedanken uns bei Dietmar Elger (Autor von ');
define(LANG_CREDITS_TEXT2_2,'Gerhard Richter, Catalogue Raisonné, Vol. 1, 2011');
define(LANG_CREDITS_TEXT2_3,') für seine Hilfe und Unterstützung.');
define(LANG_CREDITS_TEXT3_1,"Ebenso bedanken wir uns bei Hubertus Butin für seine umfangreichen Informationen über");
define(LANG_CREDITS_TEXT3_2,"Gerhard Richters Editionen.");
define(LANG_CREDITS_TEXT3_3,"");
define(LANG_CREDITS_TEXT4,'Vielen Dank an Amy Dickson für ihre Arbeit an der Chronologie.');
# end

# footer
define(LANG_FOOTER_COPYRIGHT,'Gerhard Richter - Alle Rechte vorbehalten');
define(LANG_FOOTER_CONTACT,'Kontakt');
define(LANG_FOOTER_CREDITS,'Impressum');
define(LANG_FOOTER_DISCLIMER,'Disclaimer');
# footer END

# home page
define(LANG_HOME_TEXT_INTRO1,'Gerhard Richter ist einer der bedeutendsten Künstler des 20. und 21. Jahrhunderts. Sein Werk umfasst einen Zeitraum von nahezu fünf Jahrzehnten. Hier können Sie seine Arbeiten betrachten und mehr über sein Leben erfahren. Um zu beginnen, klicken Sie bitte auf eines der unteren Werke.');
# home END

# artwork page
define(LANG_ARTWORK_PHOTO_PAINTINGS,'Bilder');
define(LANG_ARTWORK_ABSTRACTS,'Abstrakt');
define(LANG_ARTWORK_ATLAS,'Atlas');
define(LANG_ARTWORK_ATLAS_SHEET,'Atlas Sheet');
define(LANG_ARTWORK_EDITIONS,'Editionen');
define(LANG_ARTWORK_WATERCOLOURS,'Aquarelle');
define(LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS,'Übermalte Fotografien');
define(LANG_ARTWORK_DRAWINGS,'Zeichnungen');
define(LANG_ARTWORK_OILSONPAPER,'Öl auf Papier');
define(LANG_ARTWORK_OTHER,'Sonstiges');
define(LANG_ARTWORK_MICROSITES,'Ausgewählte Projekte');
define(LANG_ARTWORK_MICROSITES2,'Ausgewählte Projekte');
define(LANG_ARTWORK_ARTISTS_BOOKS,"Künstlerbücher");
define(LANG_ARTWORK_TEXT_H1,"Ein halbes Jahrhundert Malerei");
define(LANG_ARTWORK_TEXT,"Richter begann seine offizielle Tätigkeit im Jahr 1962. Hier bieten wir Ihnen die Möglichkeit sein Werk einzusehen, welches Arbeiten in Öl auf Leinwand bis hin zu übermalten Fotografien beinhaltet und zudem die Motivsammlung <i>Atlas</i> enthält.");
define(LANG_ARTWORK_TEXT_PAINTINGS1,"Richter begann seine offizielle Tätigkeit im Jahr 1962. Auf dieser Seite können Sie auf die Werke des Künstlers zugreifen, darunter abstrakte Foto-Bilder und Bilder.");
define(LANG_ARTWORK_TEXT_PAINTINGS,'Obwohl sich der Künstler selbst einer Einordnung entzieht, haben wir seine Bilder in thematische Kategorien unterteilt, um die Orientierung zu erleichtern.');
define(LANG_ARTWORK_SECTION_PAINTINGS,"Werke");
define(LANG_ARTWORK_SECTION_PHOTOPAINTINGS,"Foto-Bilder");
define(LANG_ARTWORK_SECTION_ABSTRACTS,"Abstrakte Bilder");
define(LANG_ARTWORK_SECTION_NOCR,"Keine WVZ");
define(LANG_ARTWORK_SECTION_OTHERMEDIA,"Weitere Werke");
define(LANG_ARTWORK_SECTION_OTHER,"Zerstörte Werke");
define(LANG_ARTWORK_TEXT_ATLAS,'Erforschen Sie die umfangreiche Motivsammlung Atlas - die Zeitungsausschnitte, Fotos und Entwürfe, welche die Vorlage für zahlreiche von Richters Werken bilden.');
define(LANG_ARTWORK_TITLE_OVERPAINTED_PHOTOS,"Werkverzeichnis von Gerhard Richters übermalten Fotografien");
define(LANG_ARTWORK_TEXT_OVERPAINTED_PHOTOS,"Für 2013 ist die Veröffentlichung eines Werkverzeichnisses von Gerhard Richters übermalten Fotografien geplant. Aus diesem Grund recherchieren wir zweckdienliche Informationen, zum Beispiel Provenienz und Abbildungen. Wir wären Ihnen sehr dankbar, wenn Sie uns bei unseren Recherchen behilflich sein könnten. Sie können uns hier kontaktieren. Ihre Angaben werden vertraulich behandelt.");
define(LANG_ARTWORK_CONTACT_OVERPAINTED_PHOTOS,"Informationen senden");
define(LANG_ARTWORK_TEXT_ATLAS_BACKTOATLAS,'zurück zu Atlas');
define(LANG_ARTWORK_TEXT_ATLAS_ASOCIATED,'Zugehörige Arbeiten');
define(LANG_ARTWORK_BROWSE_WORKS,'Alle Bilder ansehen');
define(LANG_ARTWORK_BROWSE_OPP,'Alle übermalten Fotografien ansehen');
define(LANG_ARTWORK_BROWSE_ATLAS,'Im Atlas suchen');
define(LANG_ARTWORK_BROWSE_EDITIONS,'In allen Editionen suchen');
define(LANG_ARTWORK_ATLAS_ASSOCIATED,'Die unteren Vorschaubilder zeigen Ihnen Abbildungen von Arbeiten, welche einem bestimmten Atlas-Blatt zugeordnet werden können.');
define(LANG_ARTWORK_BOOKS_REL_TAB,'Literatur');
define(LANG_ARTWORK_BOOKS_REL_COLOUR,'farbig');
define(LANG_ARTWORK_BOOKS_REL_BW,'s/w');
define(LANG_ARTWORK_BOOKS_REL_ARTWORK,'Kunstwerk');
define(LANG_ARTWORK_BOOKS_REL_ILLUSTRATED,'Illustriert');
define(LANG_ARTWORK_BOOKS_REL_MENTIONED,'Erwähnt');
define(LANG_ARTWORK_BOOKS_REL_DISCUSSED,'Diskutiert');
define(LANG_ARTWORK_BOOKS_REL_P,'S.');
define(LANG_ARTWORK_BOOKS_REL_PP,'S.');
define(LANG_ARTWORK_BOOKS_REL_P2,'');
define(LANG_ARTWORK_BOOKS_REL_PP2,'');
define(LANG_ARTWORK_BOOKS_REL_CHINESE,'');
define(LANG_ARTWORK_PHOTOS_TAB,'Fotos');
define(LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS,'Ausstellungsansichten:');
define(LANG_ARTWORK_PHOTOS_TAB_DEATILS,'Detailaufnahmen');
define(LANG_ARTWORK_THUMB_SALES_HIST_AVAIL,'Verkaufsgeschichte verfügbar');
define(LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL,'Sammlungsinformation verfügbar');
define(LANG_ARTWORK_BREADCRUMB_ALL,'Alle Bilder');
define(LANG_ARTWORK_BREADCRUMB_ALL_OPP,'Alle Fotografien');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN,'Dieses Kunstwerk wurde in den folgenden Ausstellungen gezeigt:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED,'Die folgenden Publikationen beinhalten dieses Kunstwerk:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS,"Die folgenden Publikationen beinhalten dieses Kunstwerk:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS,'Bildausschnitte des Kunstwerks:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS,'Dieses Kunstwerk basiert auf einem Bild aus dem <i>Atlas</i>:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC,'Auf dieser <i>Atlas</i>-Tafel finden sich die Vorlagen zu folgenden Bildern:');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB,'Einzelne Arbeiten');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC,'Diese Edition besteht aus folgenden Einzelarbeiten:');
define(LANG_ARTWORK_DESTROYED,"ZERSTÖRT");
# artwork END

# Tabs titles
define(LANG_TAB_TITLE_ARTWORKS,'Kunstwerke');
# Tabs titles END

# contact page
define(LANG_CONTACT_SUBNAV_TEXT_HEADING,'Links zu anderen Künstlern');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM1,'Kontakt');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM2,'Primärhändler');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM4,'Händler');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM3,'Poster-Verkauf');
define(LANG_CONTACT_WRITE_TEXT_BRIEF,'Wenn Sie die Gerhard Richter-Webseite kontaktieren möchten, benutzen Sie bitte das untenstehende Formular.');
define(LANG_CONTACT_WRITE_INPUTVALUE_EMAIL,'E-mail Adresse eingeben');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT1,'allgemeine Anfragen/Kommentare');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT2,'Händler finden');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT3,'Poster Editionen kaufen');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT4,'Image permissions');
define(LANG_CONTACT_WRITE_INPUTVALUE_MESSAGE,'Geben Sie Ihre Nachricht bitte hier ein');
define(LANG_CONTACT_WRITE_INPUTVALUE_SEND,'senden');
define(LANG_CONTACT_POSTERS_LINK,' Gerhard Richter Poster Editionen');
define(LANG_CONTACT_SHOP_LINK,' Gerhard Richter Shop');
define(LANG_CONTACT_EMAIL,'E-Mail-Adresse');
define(LANG_CONTACT_SUBJECT_SELECT,'Wählen Sie einen Betreff');
define(LANG_CONTACT_SUBJECT,'Betreff');
define(LANG_CONTACT_FILE,'File');
define(LANG_CONTACT_MESSAGE,'Nachricht');
define(LANG_CONTACT_CAPCHA,'Bitte tragen Sie die folgenden Buchstaben in das freie Feld ein (keine Großbuchstaben oder Leerzeichen).');
define(LANG_CONTACT_THANX1,'Vielen Dank');
define(LANG_CONTACT_THANX2,'Vielen Dank, dass Sie die Gerhard Richter-Webseite kontaktiert haben.');
# contact END

# links
define(LANG_LINKS_ARTICLES,'Artikel');
define(LANG_LINKS_DEALERS,'Händler');
define(LANG_LINKS,'Links');
define(LANG_LINKS_ARTICLES_PUBLISHED_IN,'In');
# links END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,'/g/no_image_xsmall.gif');
define(LANG_NO_IMAGE_SMALL,'/g/no_image_small.gif');
define(LANG_NO_IMAGE_MEDIUM,'/g/no_image_medium.gif');
define(LANG_NO_IMAGE_LARGE,'/g/no_image_large.gif');
# IF NO IMAGE To DISPLAY END

# DIFFERENT
define(LANG_RELEATED_ART,'Verwandte Kunst');
define(LANG_COLLECTION,'Sammlung');
define(LANG_NOTES,'Notizen');
define(LANG_SHOP,'Notizen');
define(LANG_SHOP_INFO,'Artikel erhältlich online bei');
define(LANG_ATLASSHEETS,'Atlas-Blätter');
define(LANG_RELEATED,'Verwandte Bereiche');
define(LANG_SELECT,'Auswahl..');
define(LANG_SELECT_FROM,'von');
define(LANG_SELECT_TO,'bis');
define(LANG_DIMENSIONS_FROM,'von');
define(LANG_DIMENSIONS_TO,'bis');
define(LANG_SEE_ALL,'Alle');
define(LANG_SEE,'Siehe');
define(LANG_NEXTDECADE,'Nächstes Jahrzehnt');
define(LANG_PREVDECADE,'Vorheriges Jahrzehnt');
define(LANG_BACK_TO_ALL_CATEGORIES,'zurück zu alle Kategorien');
define(LANG_BACK_TO_ALL,'zurück zu alle');
define(LANG_CLICK_IMAGE_TO_ENLARGE,'Klicken Sie auf das Bild, um es zu vergrößern');
define(LANG_CATALOGUE_RESONE,"Werkverzeichnis");
define(LANG_ATLAS_SHEET,"Atlas Blatt");
define(LANG_GO_TO_ATLAS_SHEET,"zum Atlas Blatt");
define(LANG_MUSEUM_COLLECTION,"Museum Kollektion");
define(LANG_SALES_HISTORY,"Verkäufe");
define(LANG_SALES_HISTORY_UPON_REQUEST,"Auktionshaus kontaktieren");
define(LANG_SALES_HISTORY_TB_ANNOUNCED,"folgt");
define(LANG_OF,"von");
define(LANG_IN,"in:");
define(LANG_SEARCH_PAINTINGS,"Suche in Bilder");
define(LANG_SEARCH_RESULT, "Ergebnis");
define(LANG_SEARCH_RESULTS, "Ergebnisse");
define(LANG_SEARCH_ART, "Kunst");
define(LANG_SEARCH_BIOGRAPHY, "Biographie");
define(LANG_SEARCH_QUOTES, "Zitate");
define(LANG_SEARCH_EXHIBITIONS, "Ausstellungen");
define(LANG_SEARCH_LITERATURE, "Literatur");
define(LANG_SEARCH_VIDEOS, "Videos");
define(LANG_SEARCH_LINKS, "Links");
define(LANG_SEARCH_NEWS_TALKS, "Vortr&#228;ge");
define(LANG_SEARCH_NEWS_AUCTIONS, "Auktionen");
define(LANG_SEARCH_CREDITS, "Impressum");
define(LANG_SEARCH_DISCLAIMER, "Disclaimer");
define(LANG_SEARCH_CHRONOLOGY, "Chronologie");
define(LANG_SEARCH_BASIC_TOP_VALUE_SEARCH, "Schnellsuche");
define(LANG_SEARCH_SHOW_ALL, "Alle");
define(LANG_SEARCH_YOUSEARCHEDFOR,"Sie haben gesucht");
define(LANG_SEARCH_ALLPAINTINGS,"Alle Bilder");
define(LANG_SEARCH_NUMBER,"Nummer");
define(LANG_SEARCH_ARTWORKTYPE,"Kunstgattung");
define(LANG_SEARCH_TITLE,"Titel");
define(LANG_SEARCH_LOCATION,"Ort");
define(LANG_SEARCH_SHEETNUMBER,"Blattnumner");
define(LANG_SEARCH_CRNUMBER,"Werkverzeichnisnummer");
define(LANG_SEARCH_YEARFROM,"Jahr von");
define(LANG_SEARCH_YEAR,"Jahr");
define(LANG_SEARCH_YEARTO,"Jahr bis");
define(LANG_SEARCH_DATE,"Datum");
define(LANG_SEARCH_COLOR,"Color");
define(LANG_SEARCH_HEIGHT,"Höhe");
define(LANG_SEARCH_WIDTH,"Weite");
define(LANG_SEARCH_NORESULTS,"Leider ergab Ihre Suche keine Treffer");
define(LANG_SEARCH_RESULTS1,"Ihre Suche ergab");
define(LANG_SEARCH_RESULTS2,"Ergebnis");
define(LANG_SEARCH_RESULTS3,"Ergebnisse");
define(LANG_CATEGORY_DESC_MORE,"mehr");
define(LANG_CATEGORY_DESC_CLOSE,"schlie&#223;en");
define(LANG_SEARCH_RETURNTOTSEARCH,"Zurück zu letzten Suchergebnissen");
define(LANG_USE_THIS,"Nutzen Sie dieses Menü, um in jeden beliebigen Bereich zu wechseln.");
define(LANG_BACKTOTOP,"Zurück zum Seitenanfang");
define(LANG_SHOW,"Zeigen");
define(LANG_ALL,"Alle");
define(LANG_PERPAGE,"Pro Seite");
define(LANG_IMAGES_PERPAGE,"Show");
define(LANG_BACKTOEXH,"zurück zu Ausstellung");
define(LANG_GOBACK,"zurück");
define(LANG_SALEHIST_TAB_NOTES,"Notizen:");
define(LANG_ESTIMATE,"Schätzung");
define(LANG_BOUGHT_IN,"nicht verkauft");
define(LANG_WITHDRAWN,"Zurückgezogen");
define(LANG_PREMIUM,"(inkl. Aufgeld)");
define(LANG_HAMMER,"(Hammerpreis)");
define(LANG_UNKNOWN,"Unknown");
define(LANG_LOT,"Los");
define(LANG_SOLDPR,"Verkauft für");
define(LANG_SHOWING,"Zeigt");
define(LANG_BOOKS_OF,"von");
define(LANG_BOOKS,"Bücher");
define(LANG_BOOKS_BY,"Autor");
define(LANG_BOOKS_DETAILS,"Details");
define(LANG_BOOKS_CLOSE,"Schließen");
define(LANG_BOOKS_MORE,"mehr");
define(LANG_BOOKS_MORE_DETAILS,"More Details");
define(LANG_BOOKS_LANGUAGES,"Sprachen");
define(LANG_BOOKS_LANGUAGE,"Sprache");
define(LANG_BOOKS_LANGUAGE_EXHCATALOG,"Ausstellungen");
define(LANG_BOOKS_CATEGORY,"Kategorie");
define(LANG_CONTACT,"Sie können dieses Formular benutzen, um mit uns Kontakt aufzunehmen");
define(LANG_CONTACT_UNAVAILABLE,"Im Moment leider nicht erreichbar. Wir bitten um Verständnis.");
define(LANG_CONTACT_UNAVAILABLE2,"Bitte nutzen sie <a href='mailto:info@gerhard-richter.com' title='info@gerhard-richter.com'>info@gerhard-richter.com</a>, um mit uns Kontakt aufzunehmen.");
define(LANG_BOOKS_READ_ARTICLE,"Artikel lesen");
define(LANG_COPY_TO_CLIPBOARD,"Link in die Zwischenablage kopieren");
define(LANG_PAINTING_SIZE_PARTS,"-teilig");
define(LANG_SUBMITING_FORM,"submitting form.....");
define(LANG_FOUND,"found");
define(LANG_BOOKS_READ_ESSAY,"Essay lesen");
define(LANG_CR,"WVZ");
define(LANG_CR_EDITIONS,"Editions-".LANG_CR);
define(LANG_CR_DRAWINGS,LANG_LEFT_SIDE_DRAWINGS."-".LANG_CR);
# DIFFERENT END

# biograpphy page
define(LANG_BIOGRAPHY,"Biographie");
define(LANG_WORK,"Werk");
define(LANG_PHOTOS,"Fotos");
define(LANG_QUOTES,"Zitate");
define(LANG_QUOTES_NO_QUOTES_FOUND,"Keine Zitate gefunden");
define(LANG_QUOTES_SOURCE,"QUELLE");
# biograpphy page end

#chronology
define(LANG_CHRONOLOGY_H1_1, "Chronologie");
define(LANG_CHRONOLOGY_H1_2, "by Amy Dickson");
define(LANG_AMY_DICKSON_THANKS, "Vielen Dank an Amy Dickson für Ihre Arbeit an der Chronologie. Dieses ist eine überarbeitete Version der Chronologie von Amy Dickson, wie sie in <i>Gerhard Richter: Panorama</i> abgedruckt ist (&#169; Tate, 2011. Reproduced by permission of Tate Trustees).");
define(LANG_CHRONOLOGY_LEFT_1930, "1930er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1940, "1940er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1950, "1950er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1960, "1960er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1970, "1970er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1980, "1980er-Jahre");
define(LANG_CHRONOLOGY_LEFT_1990, "1990er-Jahre");
define(LANG_CHRONOLOGY_LEFT_2000, "2000er-Jahre");
define(LANG_CHRONOLOGY_LEFT_2010, "2010er-Jahre");
#chronology end

# exhibition
define(LANG_VIEW_EXHIBITION,"Ausstellung anschauen");
define(LANG_EXH_ONWARDS,"ab 2020");
define(LANG_GROUP,"Gruppenausstellungen");
define(LANG_GROUP_EXH,"Gruppenausstellungen");
define(LANG_GROUP_EXHS,"Gruppenausstellungen");
define(LANG_SOLO,"Einzelausstellungen");
define(LANG_SOLO_EXH,"Einzelausstellungen");
define(LANG_SOLO_EXHS,"Einzelausstellungen");
define(LANG_EXH_DECADES_S,"er");
define(LANG_EXH_SORT_BY,"Sort by");
define(LANG_EXH_SORT_BY_DATE,"Date");
define(LANG_EXH,"Ausstellung");
define(LANG_EXH_FOUND,"Ausstellung");
define(LANG_EXH_FOUND_S,"Ausstellungen");
define(LANG_EXH_SHOW_ALL,"Alle");
define(LANG_EXH_ARTWORK,"Kunstwerke");
define(LANG_EXH_RELATED_EXHIBITIONS,"Zugehörige Ausstellungen");
define(LANG_EXH_INST_SHOTS,"Ausstellungsansichten");
define(LANG_EXH_LITERATURE,"Literatur");
define(LANG_EXH_VIDEOS,"Videos");
define(LANG_EXH_GUIDE,"Guide");
define(LANG_EXH_SHOW_ALL_WORKS,"Alle Arbeiten anzeigen");
define(LANG_EXH_ARTWORK_TAB_WORKS,"Werke");
define(LANG_EXH_ARTWORK_TAB_WORK,"Werk");
define(LANG_EXH_INST_TAB_PHOTOS,"Fotos");
define(LANG_EXH_INST_TAB_PHOTO,"Foto");
define(LANG_EXH_ARTWORK_SORT_BY,"Sortieren nach");
define(LANG_EXH_ARTWORK_SORT_CR_NUMBER,"Nummer");
define(LANG_EXH_ARTWORK_SORT_YEAR,"Jahr");
define(LANG_INSTALLSHOT,"Ausstellungsansichten");
define(LANG_SEARCH_RESULTS_ALL,"Suchergebnisse");
define(LANG_ALL_EXH,"alle Ausstellungen");
define(LANG_LEFT_SIDE_INSTALLATION_SOTS,"Ausstellungsfotos");
define(LANG_LEFT_SIDE_GUIDE,"Guide");
define(LANG_LEFT_SIDE_EXH_TOUR,"Ausstellungstour");
# END exhibition

# literature
define(LANG_LIT_LEFT_MONOGRAPHS,"Monografien");
define(LANG_LIT_LEFT_CATALOGUES,"Kataloge");
define(LANG_LIT_LEFT_JOURNALS,"Magazine");
define(LANG_LIT_LEFT_NEWS_ART,"Zeitungsartikel");
define(LANG_LIT_LEFT_ONL_PUBL,"Online-Publikationen");
define(LANG_LIT_LEFT_OTHERS,"Sonstiges");
define(LANG_LIT_LEFT_FILMS,"Filme");
define(LANG_LIT_LEFT_THESES,"Wissenschaftliche Arbeiten");
define(LANG_LIT_LEFT_SOLO_EXH,"Einzelausstellungen");
define(LANG_LIT_LEFT_GROUP_EXH,"Gruppenausstellungen");
define(LANG_LIT_LEFT_COLLECTIONS,"Sammlungen");
define(LANG_LIT_ONWARDS,"ab 2010");
define(LANG_LIT_PUBLISHED,"Veröffentlichungsjahr");
define(LANG_LIT_SEARCH,"Literatursuche");
define(LANG_LIT_BOOKS_PER,"Bücher pro Seite");
define(LANG_LIT_SEARCH_HELP,"header=[Literatursuche Hilfe] body=[&#60;p&#62;Wenn Sie nach einem bestimmten Buch suchen wollen, können Sie verschiedene Kriterien auswählen.&#60;/p&#62;&#60;p&#62;Sie können nach Buchtitel und/oder Autor oder Schlagwörtern suchen.&#60;/p&#62;&#60;p&#62;Beispiele:&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;J&uuml;rgen Schilling&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter, J&uuml;rgen Schilling &#60;/p&#62;&#60;p&#62;- &lsquo;Schilling&rsquo;&#60;/p&#62;]");
define(LANG_LIT_SEARCH_TITLE,"Titel");
define(LANG_LIT_SEARCH_AUTHOR,"Autor");
define(LANG_LIT_SEARCH_AUTHOR2,"Autor");
define(LANG_LIT_SEARCH_DATE,"Datum");
define(LANG_LIT_SEARCH_SORTBYTITLE,"Nach Titel sortieren");
define(LANG_LIT_SEARCH_SORTBYAUTHOR,"Nach Autor sortieren");
define(LANG_LIT_SEARCH_SORTBYDATE,"Nach Datum sortieren");
define(LANG_LIT_SEARCH_CLICK,"Klicken, um zur Detailansicht zu gelangen");
define(LANG_LIT_SEARCH_PUBBY,"Verlag");
define(LANG_LIT_SEARCH_EDITOR,"Lektor/in");
define(LANG_LIT_SEARCH_UNKNOWN,"Unbekannt");
define(LANG_LIT_SEARCH_ISBNNOTAVA,"ISBN nicht verfügbar");
define(LANG_LIT_SEARCH_SEERELEXH,"Siehe  zugehörige Ausstellung");
define(LANG_LIT_SEARCH_SEERELEXH2,"Siehe  zugehörige Ausstellung");
define(LANG_LIT_SEARCH_GOTORELEXH,"Gehe zu zugehöriger Ausstellung");
define(LANG_LIT_SEARCH_GOTORELEXH2,"Gehe zu zugehöriger Ausstellung");
define(LANG_LIT_SEARCH_PAGES,"Seiten");
define(LANG_LIT_SEARCH_NOFOUND,"Keine Bücher gefunden. Bitte versuchen Sie es noch einmal.");
define(LANG_LIT_SEARCH_BACKTOLIT,"Zurück zu Literatur");
define(LANG_LIT_SEARCH_INFORMATION,"Informationen");
define(LANG_LITERATURE_ENLARGE, "vergrößern");
define(LANG_LITERATURE_IN, "In");
define(LANG_LITERATURE_ISSUE, "Ausgabe");
define(LANG_LITERATURE_VOL, "Jg.");
define(LANG_LITERATURE_NO, "Nr.");
define(LANG_LITERATURE_PAGES, "S.");
define(LANG_LITERATURE_PAGESS, "S.");
define(LANG_LITERATURE_COVER_HARDBACK, "Hardcover");
define(LANG_LITERATURE_COVER_SOFTCOVER, "Softcover");
define(LANG_LITERATURE_COVER_UNKNOWNBINDING, "Unbekannter Einband");
define(LANG_LITERATURE_COVER_SPECIALBINDING, "Spezialeinband");
define(LANG_LITERATURE_ISBN, "ISBN");
define(LANG_LITERATURE_ISSN, "ISSN");
define(LANG_LITERATURE_DESC_TEXT, "Gerhard Richters Werk und Leben wird in zahlreichen Publikationen dargestellt und diskutiert. Die Literatur reicht von Ausstellungs- und Sammlungskatalogen bis zu Monographien, Filme und Artikel. Richters eigene Publikationen in Form von Künstlerbüchern bilden einen wichtigen Teil der Literatur. Hier finden Sie eine Auswahl an Literatur, die sich auf Richter und sein Werk bezieht.");
define(LANG_LITERATURE_SHOW_MORE_OPTIONS, "Mehr Optionen");
define(LANG_LITERATURE_SHOW_LESS_OPTIONS, "Weniger Optionen");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEO, "Zugehöriges Video");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS, "Zugehörige Videos");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION, "Dieser Katalog erschien anlässlich folgender Ausstellung:");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS, "Dieser Katalog erschien anlässlich folgender Ausstellungen:");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE, "Zugehörige Publikation");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES, "Zugehörige Publikationen");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS, "Folgende Kunstwerke werden im Buch dargestellt, erwähnt und/oder diskutiert:");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK, "Folgendes Kunstwerk wird im Buch dargestellt, erwähnt und/oder diskutiert:");
define(LANG_LITERATURE_TITLE, "Titel");
define(LANG_LITERATURE_TITLES, "Titel");
define(LANG_LITERATURE_SHOW_ALL,"Alle");
# literature end

# video
define(LANG_VIDEO_ALLTITLES,"Alle Titel");
define(LANG_VIDEO_VIDEOS,"Videos");
define(LANG_VIDEO_AUDIO,"Audio");
define(LANG_AUDIO_CR,"Catalogue Raisonné:");
define(LANG_VIDEO_RUNTIME,"Laufzeit");
define(LANG_VIDEO_WATCH1,"Video im Quicktime Format anschauen");
define(LANG_VIDEO_WATCH2,"Video im Flash Format anschauen");
define(LANG_VIDEO_WATCH3,"Quicktime");
define(LANG_VIDEO_WATCH4,"Flash");
define(LANG_VIDEO_BACKTOVID,"Zurück zu Videos");
define(LANG_VIDEO_TAB_INFO, "Information");
define(LANG_VIDEO_TAB_IMAGES, "Images");
define(LANG_VIDEO_ARTWORK_MENTIONED, "Dieses Kunstwerk wird in den folgenden Videos gezeigt oder besprochen:");
define(LANG_VIDEO_EXHIBITION_MENTIONED, "Diese Ausstellung wird in den folgenden Videos gezeigt:");
define(LANG_VIDEO_IN_SECTION1, "In dieser Kategorie gibt es");
define(LANG_VIDEO_IN_SECTION2, "Videos:");
# video end

# news
define(LANG_NEWS_CURRENT,"Aktuell");
define(LANG_NEWS_UPCOMING,"Vorschau");
define(LANG_NEWS_RESULTS,"Ergebnisse");
define(LANG_NEWS_PUBLICATIONS,"Publikationen");
define(LANG_NEWS_TALKS,"Vortr&#228;ge");
define(LANG_NEWS_AUCTIONS,"Auktionen");
define(LANG_NEWS_EXHIBITIONS,"Ausstellungen");
define(LANG_NEWS_ARCHIVE,"Archiv");
define(LANG_NEWS_NO_AUCTIONS,"Momentan sind keine Auktionen von Kunstwerken bekannt.");
define(LANG_NEWS_NOCURRENT,"Zur Zeit keine News");
define(LANG_NEWS_NO_TALKS,"No information currently available");
define(LANG_NEWS_NOUPCOMING,"Zur Zeit keine News");
define(LANG_NEWS_NOPUBLICATIONS,"Keine Neuerscheinungen");
define(LANG_NEWS_NOARCHIVE,"Kein Archiv");
define(LANG_NEWS_DOWNLOADLEAFLET,"Faltblatt herunterladen");
define(LANG_NEWS_ALSO_VISIT,"Besuchen Sie uns auch auf");
define(LANG_NEWS_SHOW_ALL,"Alle");
define(LANG_NEWS_DISCLIMER,"Disclaimer");
define(LANG_NEWS_DISCLIMER_TEXT1,"Die Angaben auf dieser Webseite dienen ausschließlich allgemeinen Informationszwecken. Die Angaben werden von <a href='/' title='www.gerhard-richter.com'>www.gerhard-richter.com</a> bereitgestellt und obwohl wir uns bemühen, die Inhalte unserer Seite aktuell zu halten, schließen wir jegliche Haftung aus. Wir geben keinerlei Garantien, weder direkt noch indirekt, für die Vollständigkeit, Richtigkeit, Zuverlässigkeit, Angemessenheit und Verfügbarkeit bezüglich der Webseite sowie der Informationen auf der Webseite. Die Nutzung dieser Informationen erfolgt auf eigene Verantwortung. In keinem Fall übernehmen wir die Verantwortung für Verluste entstanden aus oder in Verbindung mit der Nutzung dieser Webseite.");
define(LANG_NEWS_DISCLIMER_TEXT2,"Diese Webseite ist kein vollständiges Werkverzeichnis und hat nicht die Absicht eines zu sein. Die Aufnahme einer Arbeit auf die Webseite stellt keine Gewährleistung dar, und sollte auch nicht als solche aufgefasst werden, dass die Arbeit eine authentische Arbeit von Gerhard Richter ist. Ebenso heißt der Ausschluss einer Arbeit von der Webseite nicht, dass es sich nicht doch um eine authentische Arbeit von Gerhard Richter handeln kann.");
define(LANG_NEWS_UPCOMING_FOR_AUCTION,"Auktionen");
define(LANG_NEWS_UPCOMING_FOR_AUCTION_DESC,"Folgende Werke werden demnächst versteigert:");
define(LANG_NEWS_REACEN_AUCTION_RES,"Ergebnisse");
define(LANG_NEWS_REACEN_AUCTION_RES_DESC,"Folgende Werke wurden im vergangenen halben Jahr versteigert:");
define(LANG_NEWS_NEW_PUBLICATIONS,"Neue Publikationen");
define(LANG_NEWS_PUBLICATIONS_COUNT1,"There currently are");
define(LANG_NEWS_PUBLICATIONS_COUNT2,"new publications");
define(LANG_NEWS_NEW_TALKS,"Vorträge");
define(LANG_NEWS_AUCTIONS_AUCTIONHOUSE,"Auktionshaus");
define(LANG_NEWS_AUCTIONS_DATE,"Datum");
define(LANG_NEWS_RETURN_TO_NEWS_PAGE,"Return to news page");
# news end

#search box tooltip start
define(LANG_SEARCH_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_TIP_TEXT,"<p class=\"tip\"><span>Nummer:</span> eine Liste von Nummern, durch Kommas getrennt, z.B. \"5,137,211\" oder eine Auswahl von Nummern, durch Doppelpunkt getrennt, z.B. \"5:20\".</p><p class=\"tip\"><span>Jahr:</span> ein einzelnes Jahr oder eine Auswahl von Jahren.</p><p class=\"tip\"><span>Date:</span> an individual date, or range of dates.</p><p class=\"tip\"><span>Größe:</span> exakte Maße oder eine Auswahl mit Hilfe der Felder &#8216;von&#8217; &#8216;bis&#8217;.</p>");
#search box tooltip end

#search box EXH tooltip start
define(LANG_SEARCH_EXH_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_EXH_TIP_TEXT,"<p class=\"tip\"><span>Titel:</span> Suche nach einem Titel</p><p class=\"tip\"><span>Ort:</span> Ort der Ausstellung</p> <p class=\"tip\"><span>Stichwort:</span> Suche nach Stichwort</p> <p class=\"tip\"><span>Jahr:</span> Ein einzelnes Jahr oder eine Auswahl von Jahren</p>");
#search box EXH tooltip end

#search box QUOTES tooltip start
define(LANG_SEARCH_QUOTES_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_QUOTES_TIP_TEXT,"<p class=\"tip\"><span>Stichwort:</span> Suche nach Stichwort</p> <p class=\"tip\"><span>Jahr:</span> Ein einzelnes Jahr oder eine Auswahl von Jahren</p>");
#search box QUOTES tooltip end

#search box LIT tooltip start
define(LANG_SEARCH_LIT_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_LIT_TIP_TEXT,"<p class=\"tip\"><span>Autor:</span> Suche nach einem Autor</p> <p class=\"tip\"><span>Titel:</span> Suche nach einem Titel</p><p class=\"tip\"><span>Stichwort:</span> Suche nach einem Stichwort</p><p class=\"tip\"><span>Kategorie:</span> Suche nach einer Kategorie</p><p class=\"tip\"><span>Sprache:</span> Suche nach einer Sprache</p><p class=\"tip\"><span>Jahr:</span> Suche nach einem einzelnen Jahr oder einer Auswahl von Jahren</p>");
#search box LIT tooltip end

#search box ARTICLES tooltip start
define(LANG_SEARCH_ARTICLES_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_ARTICLES_TIP_TEXT,"<p class=\"tip\"><span>Autor:</span> Suche nach einem Autor</p> <p class=\"tip\"><span>Titel:</span> Suche nach einem Titel</p><p class=\"tip\"><span>Stichwort:</span> Suche nach einem Stichwort</p><p class=\"tip\"><span>Jahr:</span> Suche nach einem einzelnen Jahr oder einer Auswahl von Jahren</p>");
#search box ARTICLES tooltip end

#search box VIDEOS tooltip start
define(LANG_SEARCH_VIDEOS_TIP_TITLE,"Hilfe");
define(LANG_SEARCH_VIDEOS_TIP_TEXT,"<p class=\"tip\"><span>Stichwort:</span> Suche nach Stichwort</p>");
#search box VIDEOS tooltip end

#zoomer start
define(LANG_ZOOMER_OPEN,"Detailansicht");
define(LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE,"See Microsite");
#zoomer end

#virtual gallery
define(LANG_VIRTUALGALLERY_OPEN,"View these images in our flash gallery");
define(LANG_VIRTUALGALLERY,"Virtual Gallery");
#end

# MICROSITE
define(LANG_MICROSITE_TITLE_SINBAD,"Sindbad");
define(LANG_MICROSITE_SHOW,"Anzeigen");
define(LANG_MICROSITE_PER_PAGE,"Pro Seite");
define(LANG_MICROSITE_SEE_ALL,"Alle");
define(LANG_MICROSITE_SEARCH_EDITION,"Suche nach Editionen");
define(LANG_MICROSITE_ABOUT,"Über");
define(LANG_MICROSITE_SEARCH,"Suche");
define(LANG_MICROSITE_SINBAD_TITLE,"Gerhard Richter Sindbad Edition");
define(LANG_MICROSITE_SINBAD_SITENAME,"Sindbad");
define(LANG_MICROSITE_SINBAD_JS_SITENAME,"Sindbad");
define(LANG_MICROSITE_SINBAD_VIEW_AS_PAIR,"Als Paar anschauen");
define(LANG_MICROSITE_SEARCH_HELP_TITLE,"Hilfe zur Suche");
define(LANG_MICROSITE_SEARCH_HELP_TEXT,"<em>Einzelne Edition =</em> Nummer der Edition eingeben (z.B. 23) <p><em>Mehrere Editionen =</em> jede Editionsnummer durch Komma getrennt eingeben (z.B. 23, 32, 55)</p> <p><em>Gruppen =</em> Start- und Endnummer der Gruppe durch Doppelpunkt getrennteingeben (z.B. 23:55)</p>");
define(LANG_MICROSITE_EDITIONS_CR,"Editions WVZ");
define(LANG_MICROSITE_IMAGE,"Bild");
define(LANG_MICROSITE_OF,"von");
define(LANG_MICROSITE_NOVEMBER_FOOTER_1,"For further information on Gerhard Richter and his work,<br />please visit");
define(LANG_MICROSITE_NOVEMBER_FOOTER_2,"and learn about his life and work.");
# MICROSITE END

# OVERPAINTED PHOTOGRAPHS FORM
define(LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS,"Katalog der übermalten Fotografien");
define(LANG_OPP_FORM_CATALOGUE_RAISONNE,"Ein Catalogue Raisonné oder Werkverzeichnis ist ein systematisches und umfassendes wissenschaftliches Referenzwerk. Es stellt ein maßgebliches Verzeichnis aller authentifizierten Arbeiten eines Künstlers dar. Jedes Kunstwerk ist dokumentiert, illustriert und mit einer bleibenden Referenznummer versehen. Ein Werkverzeichnis beinhaltet Angaben zu Titel, Jahr, Medium, Maße, Signatur, Provenienz, Ausstellungsgeschichte und Bibliografie.");
//define(LANG_OPP_FORM_1,"Ein <span style='border-bottom:1px dotted #fff;' class='help' title='".LANG_OPP_FORM_CATALOGUE_RAISONNE."'>Catalogue Raisonné</span> der übermalten Fotografien Gerhard Richters ist für 2015 geplant.");
define(LANG_OPP_FORM_1,"Ein umfassender Katalog der übermalten Fotografien Gerhard Richters wird voraussichtlich 2017 erscheinen.");
define(LANG_OPP_FORM_1_1,"Für weitere Informationen klicken Sie bitte ");
define(LANG_OPP_FORM_1_2,"hier");
define(LANG_OPP_FORM_2,"Gerhard Richter hat in seiner Karriere eine große Anzahl übermalter Fotografien geschaffen und produziert fortlaufend neue Werke. Der geplante Katalog soll einen Aspekt von Gerhard Richters Œuvre dokumentieren, der sowohl Kunstkennern, als auch der breiteren Öffentlichkeit größtenteils unbekannt ist. Das Verzeichnis hat zum Ziel Gerhard Richters kreative Bandbreite in dieser Werkgruppe zu veranschaulichen und alle derzeit bekannten übermalten Fotografien aufzulisten und zu reproduzieren.");
define(LANG_OPP_FORM_3,"");
define(LANG_OPP_FORM_4,"Zu diesem Zweck sammeln wir zu jeder übermalten Fotografie folgende Daten:");
define(LANG_OPP_FORM_5,"Titel");
define(LANG_OPP_FORM_6,"Jahr");
define(LANG_OPP_FORM_7,"Medium");
define(LANG_OPP_FORM_8,"Maße");
define(LANG_OPP_FORM_9,"Provenienz");
define(LANG_OPP_FORM_10,"Ausstellungsgeschichte");
define(LANG_OPP_FORM_11,"Bibliografie");
define(LANG_OPP_FORM_12,"Andere wichtige Details");
define(LANG_OPP_FORM_13,"Für die Aufnahme von übermalten Fotografien in den Katalog bitten wir private Sammler sowie öffentliche Institutionen um Kontaktaufnahme und ersuchen Sie freundlich das untenstehende Formular auszufüllen oder uns per <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>E-Mail</a> zu kontaktieren.");
define(LANG_OPP_FORM_14,"Wir erhoffen uns ausführliche, möglichst vollständige Informationen. Sollten Sie es vorziehen bei sensiblen oder privaten Daten keine Angaben zu machen, lassen Sie diese Felder bitte frei.");
define(LANG_OPP_FORM_15,"Alle Ihre Angaben werden selbstverständlich mit höchster Diskretion behandelt.");
define(LANG_OPP_FORM_16,"Herzlichen Dank für Ihre Zeit und Unterstützung des Projektes.");
define(LANG_OPP_FORM_17,"Formular");
define(LANG_OPP_FORM_18,"Die gemachten Angaben werden für die Vorbereitung des Kataloges der übermalten Fotografien in Betracht gezogen. Das Formular dient ausschließlich der wissen-schaftlichen Recherche sowie Archivzwecken.");
define(LANG_OPP_FORM_19,"Falls die Beantwortung spezifischer Fragen nicht möglich ist, lassen Sie das Feld bitte leer. Sollten Sie Hilfe beim Ausfüllen des Formulars benötigen, kontaktieren Sie uns bitte per <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>E-Mail</a>.");
define(LANG_OPP_FORM_20,"Titel");
define(LANG_OPP_FORM_21_1,"TT.MM.JJJJ");
define(LANG_OPP_FORM_21,"Entstehungsjahr");
define(LANG_OPP_FORM_22,"Medium");
define(LANG_OPP_FORM_23,"Öl auf Farbfotografie");
define(LANG_OPP_FORM_24,"Öl auf Schwarzweißfotografie");
define(LANG_OPP_FORM_25,"Lack auf Farbfotografie");
define(LANG_OPP_FORM_26,"Lack auf Schwarzweißfotografie");
define(LANG_OPP_FORM_26_1,"Oil on gelatin silver print");
define(LANG_OPP_FORM_27,"Maße der Fotografie (H &#215; B)");
define(LANG_OPP_FORM_28,"cm");
define(LANG_OPP_FORM_29,"inches");
define(LANG_OPP_FORM_30,"Maße des Passepartouts (H &#215; B)");
define(LANG_OPP_FORM_31,"Bitte geben Sie an, wie die Sammlungsangabe im Katalog erscheinen soll. Falls Sie einen anonymen Eintrag bevorzugen, geben Sie bitte 'Privatsammlung' an, mit zusätzlicher Ortsangabe, sollte dies gewünscht sein. ");
define(LANG_OPP_FORM_32,"Credit");
define(LANG_OPP_FORM_33,"Serie / Edition");
define(LANG_OPP_FORM_35,"Firenze (edition)");
define(LANG_OPP_FORM_36,"Florence (series)");
define(LANG_OPP_FORM_37,"Grauwald");
define(LANG_OPP_FORM_38,"Museum Visit");
define(LANG_OPP_FORM_38_1,"Sils");
define(LANG_OPP_FORM_38_2,"Wald");
define(LANG_OPP_FORM_38_3,"128 Fotos von einem Bild (Halifax 1978)");
define(LANG_OPP_FORM_38_4,"8.2.92");
define(LANG_OPP_FORM_39,"Gegebenenfalls Serien - oder Editionsnummer");
define(LANG_OPP_FORM_40_1,"Vorderseite:");
define(LANG_OPP_FORM_40_1_1,"signiert");
define(LANG_OPP_FORM_40_1_2,"datiert");
define(LANG_OPP_FORM_40_1_3,"nummeriert");
define(LANG_OPP_FORM_40_1_4,"bezeichnet");
define(LANG_OPP_FORM_40_2,"Rückseite:");
define(LANG_OPP_FORM_40_2_1,"signiert");
define(LANG_OPP_FORM_40_2_2,"datiert");
define(LANG_OPP_FORM_40_2_3,"nummeriert");
define(LANG_OPP_FORM_40_2_4,"bezeichnet");
define(LANG_OPP_FORM_41,"auf Passepartout");
define(LANG_OPP_FORM_41_1,"on backing board");
define(LANG_OPP_FORM_42,"auf Fotografie");
define(LANG_OPP_FORM_42_1,"verso");
define(LANG_OPP_FORM_43,"oben links");
define(LANG_OPP_FORM_44,"oben Mitte");
define(LANG_OPP_FORM_45,"oben rechts");
define(LANG_OPP_FORM_46,"linke Bildhälfte");
define(LANG_OPP_FORM_47,"in der Mitte");
define(LANG_OPP_FORM_48,"rechte Bildhälfte");
define(LANG_OPP_FORM_49,"unten links");
define(LANG_OPP_FORM_50,"untere Mitte");
define(LANG_OPP_FORM_51,"unten rechts");
define(LANG_OPP_FORM_53,"Andere Beschriftungen");
define(LANG_OPP_FORM_54,"Anmerkungen / Aufkleber verso");
define(LANG_OPP_FORM_55,"Bild hochladen");
define(LANG_OPP_FORM_55_1,"Es ist möglich mehrere Fotos hochzuladen");
define(LANG_OPP_FORM_56,"Wir begrüßen hochaufgelöste Bilder von mindestens 300dpi in Originalgröße ");
define(LANG_OPP_FORM_57,"Die bevorzugten Bildformate sind: JPEG, TIFF, PSD, PNG");
define(LANG_OPP_FORM_58,"Copyright / Fotograf");
define(LANG_OPP_FORM_59,"Provenienz");
define(LANG_OPP_FORM_60,"Erworben von");
define(LANG_OPP_FORM_61,"Erwerbsdatum");
define(LANG_OPP_FORM_62,"Ausstellungsgeschichte");
define(LANG_OPP_FORM_62_1,"Exhibition history other");
define(LANG_OPP_FORM_63,"Bibliografie");
define(LANG_OPP_FORM_63_1,"Publication history other");
define(LANG_OPP_FORM_64,"Persönliche Angaben");
define(LANG_OPP_FORM_65,"Vorname");
define(LANG_OPP_FORM_66,"Nachname");
define(LANG_OPP_FORM_67,"E-Mail");
define(LANG_OPP_FORM_68,"Adresse");
define(LANG_OPP_FORM_68_1,"Postleitzahl");
define(LANG_OPP_FORM_68_2,"Land");
define(LANG_OPP_FORM_69,"Telefonnummer");
define(LANG_OPP_FORM_70,"Zusätzliche Angaben");
define(LANG_OPP_FORM_71,"Pflichtfeld");
define(LANG_OPP_FORM_72,"Durch Einreichen dieses Formulars bestätigt der Eigentümer, dass alle Angaben korrekt sind und keine Informationen die das Kunstwerk, seine Geschichte oder Eigentümerschaft betreffen, zurückgehalten wurden.");
define(LANG_OPP_FORM_73,"Der Eigentümer erkennt an, dass das Absenden des Formulars keine Garantie für die Aufnahme des Werkes in das Verzeichnis darstellt.");
define(LANG_OPP_FORM_74,"Absenden");
define(LANG_OPP_FORM_75,"Bitte lesen Sie die Erklärung auf der vorherigen Seite und bestätigen Sie, dass Sie mit den Bestimmungen einverstanden sind.");
define(LANG_OPP_FORM_75_2,"Please fill in all required fields!");
define(LANG_OPP_FORM_75_3,"Verification characters you entered where not correct please try again!");
define(LANG_OPP_FORM_76,"Ihr Formular wurde abgeschickt.");
define(LANG_OPP_FORM_77,"Vielen Dank für Ihre Zeit.");
define(LANG_OPP_FORM_77_2,"Angaben ausdrucken");



# exhibition list
define(LANG_OPP_FORM_78_9_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_78_9_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_78_9_D,", Mai &#8211; Juni 2016");
define(LANG_OPP_FORM_78_8_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_78_8_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_78_8_D,", November &#8211; Dezember 2015");
define(LANG_OPP_FORM_78_7_T,"Gerhard Richter. Das kleine Format");
define(LANG_OPP_FORM_78_7_L,", Galerie Schwarzer, Dusseldorf");
define(LANG_OPP_FORM_78_7_D,", April &#8211; Juni 2015");
define(LANG_OPP_FORM_78_6_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_6_L,", The Lone T Art Space, Milan");
define(LANG_OPP_FORM_78_6_D,", April &#8211; Mai 2015");
define(LANG_OPP_FORM_78_5_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_78_5_L,", Hannah Hoffman Gallery, Los Angeles");
define(LANG_OPP_FORM_78_5_D,", März &#8211; April 2015");
define(LANG_OPP_FORM_78_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_4_L,", Repetto Gallery, London");
define(LANG_OPP_FORM_78_4_D,", Februar &#8211; März 2015");
define(LANG_OPP_FORM_78_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_3_L,", Marian Goodman Gallery, London");
define(LANG_OPP_FORM_78_3_D,", Oktober &#8211; Dezember 2014");
define(LANG_OPP_FORM_78_1_T,"What is a Photograph");
define(LANG_OPP_FORM_78_1_L,", International Center of Photography, New York");
define(LANG_OPP_FORM_78_1_D,", Januar &#8211; Mai 2014");
define(LANG_OPP_FORM_78_2_T,"Purer Zufall. Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_78_2_L,", Sprengel Museum Hannover, Hanover");
define(LANG_OPP_FORM_78_2_D,", Mai &#8211; September 2013");
define(LANG_OPP_FORM_78_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_L,", Beirut Art Center, Beirut");
define(LANG_OPP_FORM_78_D,", April &#8211; Juni 2012");
define(LANG_OPP_FORM_79_T,"Gerhard Richter &#8211; Arbeiten 1968-2008");
define(LANG_OPP_FORM_79_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_79_D,", November 2011 &#8211; Januar 2012");
define(LANG_OPP_FORM_80_T,"Editionen und übermalte Fotografien");
define(LANG_OPP_FORM_80_L,", Wolfram Völcker Fine Art, Berlin");
define(LANG_OPP_FORM_80_D,", September &#8211; Oktober 2011");
define(LANG_OPP_FORM_81_T,"De-Natured: German Art From Joseph Beuys to Martin Kippenberger");
define(LANG_OPP_FORM_81_L,", Ackland Art, Museum, Chapel Hill");
define(LANG_OPP_FORM_81_D,", April &#8211; Juli 2011");
define(LANG_OPP_FORM_82_T,"Gerhard Richter “New Overpainted Photographs“");
define(LANG_OPP_FORM_82_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_82_D,", Februar &#8211; März 2010");
define(LANG_OPP_FORM_82_1_T,"Best of Fifty Years");
define(LANG_OPP_FORM_82_1_L,", Kunstverein Wolfsburg");
define(LANG_OPP_FORM_82_1_D,", November 2009 &#8211; Februar 2010");
define(LANG_OPP_FORM_83_T,"Photo España 2009: Gerhard Richter, Fotografías pintadas");
define(LANG_OPP_FORM_83_L,", Fundación Telefónica, Madrid");
define(LANG_OPP_FORM_83_D,", Juni 2009");
define(LANG_OPP_FORM_84_T,"The Photographic Object");
define(LANG_OPP_FORM_84_L,", Photographers’ Gallery, London");
define(LANG_OPP_FORM_84_D,", April &#8211; Juni 2009");
define(LANG_OPP_FORM_85_T,"Gerhard Richter &#8211; Übermalte Fotografien &#8211; Photographies Peintes");
define(LANG_OPP_FORM_85_L,", Centre de la photographie, Geneva");
define(LANG_OPP_FORM_85_D,", Februar &#8211; Mai 2009");
define(LANG_OPP_FORM_86_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_86_L,", Museum Morsbroich, Leverkusen");
define(LANG_OPP_FORM_86_D,", Oktober 2008 &#8211; Januar 2009");
define(LANG_OPP_FORM_86_1_T,"Gerhard Richter “New Works“");
define(LANG_OPP_FORM_86_1_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_86_1_D,", November &#8211; Dezember 2005");
define(LANG_OPP_FORM_87_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_87_L,", Anthony Meier Fine Arts, San Francisco");
define(LANG_OPP_FORM_87_D,", Juni &#8211; August 2005");
define(LANG_OPP_FORM_88_T,"Gerhard Richter &#8211; Editionen 1968 &#8211; 2004");
define(LANG_OPP_FORM_88_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_88_D,", Juni &#8211; Juli 2005");
define(LANG_OPP_FORM_89_T,"Attack: Attraction Painting/Photography");
define(LANG_OPP_FORM_89_L,", Marcel Sitcoske Gallery, San Francisco");
define(LANG_OPP_FORM_89_D,", Dezember 2002 &#8211; Januar 2003");
define(LANG_OPP_FORM_90_T,"Gerhard Richter");
define(LANG_OPP_FORM_90_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_90_D,", December 2002 &#8211; January 2003");
define(LANG_OPP_FORM_91_T,"Gerhard Richter &#8211; Firenze");
define(LANG_OPP_FORM_91_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_91_D,", Juni &#8211; August 2002");
define(LANG_OPP_FORM_92_T,"Gerhard Richter &#8211; Editionen 1969 &#8211; 1998");
define(LANG_OPP_FORM_92_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_92_D,", März &#8211; Mai 2001");
define(LANG_OPP_FORM_93_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_93_L,", Galerie Fred Jahn, Munich");
define(LANG_OPP_FORM_93_D,", Oktober 2000");
define(LANG_OPP_FORM_93_1_T,"Gerhard Richter &#8211; Bilder 1972 &#8211; 1996");
define(LANG_OPP_FORM_93_1_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_1_D,", Mai &#8211; Juli 1998");
define(LANG_OPP_FORM_93_2_T,"Gerhard Richter");
define(LANG_OPP_FORM_93_2_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_2_D,", Oktober &#8211; November 1997");
define(LANG_OPP_FORM_93_3_T,"Gerhard Richter Part II: Painting, Mirror Painting, Watercolour, Photograph, Print");
define(LANG_OPP_FORM_93_3_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_3_D,", Mai 1996");
define(LANG_OPP_FORM_93_4_T,"Gerhard Richter &#8211; Editionen 1967 &#8211; 1993");
define(LANG_OPP_FORM_93_4_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_4_D,", Januar &#8211; März 1994");
define(LANG_OPP_FORM_93_5_T,"Gerhard Richter: Sils");
define(LANG_OPP_FORM_93_5_L,", Nietzsche-Haus, Sils-Maria");
define(LANG_OPP_FORM_93_5_D,", Juli 1992 &#8211; März 1993");
define(LANG_OPP_FORM_94_T,"weitere Ausstellungen");
define(LANG_OPP_FORM_94_L,"");
define(LANG_OPP_FORM_94_D,"");

# publications list
define(LANG_OPP_FORM_95_6_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_95_6_L,". Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_95_6_D,", 2015");
define(LANG_OPP_FORM_95_5_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_95_5_L," (Buchloh, Benjamin H.D., Schwarz, Dieter). Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_95_5_D,", 2016");
define(LANG_OPP_FORM_95_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_95_4_L," (Barbero, Luca Massimo). Nuvole Rosse");
define(LANG_OPP_FORM_95_4_D,", 2015");
define(LANG_OPP_FORM_95_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_95_3_L," (Buchloh, Benjamin H.D., Schwarz, Dieter, Storr, Robert). London: Marian Goodman Gallery");
define(LANG_OPP_FORM_95_3_D,", 2014");
define(LANG_OPP_FORM_95_2_T,"What is a Photograph");
define(LANG_OPP_FORM_95_2_L," (Squiers, Carol, Baker, George, Batchen, Geoffrey, Steyerl, Hito). New York: Prestel");
define(LANG_OPP_FORM_95_2_D,", 2014");
define(LANG_OPP_FORM_95_T,"Purer Zufall &#8211; Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_95_L," (Krempel, Ulrich, Rist, Annerose, Schwarz, Isabelle). Hanover: Sprengel Museum");
define(LANG_OPP_FORM_95_D,", 2013");
define(LANG_OPP_FORM_95_1_T,"Beirut");
define(LANG_OPP_FORM_95_1_L," (Borchardt-Hume, Achim, Joreige, Lamia, Dagner, Sandra). Beirut: Beirut Art Center; London: Heni Publishing; Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_95_1_D,", 2012");
define(LANG_OPP_FORM_96_T,"Best of Fifty Years");
define(LANG_OPP_FORM_96_L," (Justin Hoffmann/Kunstverein Wolfsburg). Wolfsburg: Kunstverein Wolfsburg");
define(LANG_OPP_FORM_96_D,", 2010");
define(LANG_OPP_FORM_97_T,"Gerhard Richter. Obrist &#8211; O’Brist");
define(LANG_OPP_FORM_97_L," (Obrist, Hans Ulrich). Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_97_D,", 2009");
define(LANG_OPP_FORM_97_1_T,"Gerhard Richter. Overpainted Photographs");
define(LANG_OPP_FORM_97_1_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_97_1_D,", 2008");
define(LANG_OPP_FORM_98_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_98_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_98_D,", 2008");
define(LANG_OPP_FORM_99_T,"Gerhard Richter.");
define(LANG_OPP_FORM_99_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_99_D,", 2005");
define(LANG_OPP_FORM_100_T,"City Life");
define(LANG_OPP_FORM_100_L," (Cora, Bruno, Restagno, Enzo, Gori, Giuliano). Prato: Gli Ori");
define(LANG_OPP_FORM_100_D,", 2002");
define(LANG_OPP_FORM_100_1_T,"Gerhard Richter.");
define(LANG_OPP_FORM_100_1_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_1_D,", 2002");
define(LANG_OPP_FORM_100_2_T,"Sils");
define(LANG_OPP_FORM_100_2_L," (eds. Obrist, Hans Ulrich, Bloch, Peter Andre). Munich: Oktagon, 1992; New York: D. A. P., Distributed Art Publishers");
define(LANG_OPP_FORM_100_2_D,", 2002 ");
define(LANG_OPP_FORM_100_3_T,"Florence");
define(LANG_OPP_FORM_100_3_L," (Elger, Dietmar). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_100_3_D,", 2001");
define(LANG_OPP_FORM_100_4_T,"Gerhard Richter, Part I: New Painting; Part II, Part II: Painting, Mirror Painting, Watercolour, Photograph, Print.");
define(LANG_OPP_FORM_100_4_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_4_D,", 1996");
define(LANG_OPP_FORM_100_5_T,"weitere Publikationen");
define(LANG_OPP_FORM_100_5_L,"");
define(LANG_OPP_FORM_100_5_D,"");


define(LANG_OPP_FORM_103,"Ich möchte eine Kopie der Daten als E-Mail");
define(LANG_OPP_FORM_104,"Referenznummer");
# END OVERPAINTED PHOTOGRAPHS FORM

# TABLE
define(LANG_TABLE_DESC_PUBLICATION_RELATED,"zugehörige");
define(LANG_TABLE_DESC_PUBLICATION,"Publikation");
define(LANG_TABLE_DESC_PUBLICATIONS,"Publikationen");
define(LANG_TABLE_TITLE,"Titel");
define(LANG_TABLE_FURTHER_ARTWORKS_IN_LOT,"Weitere Werke in diesem Los:");
# END TABLE

# Search
define(LANG_SEARCH_SHOW_MORE_OPTIONS,"Show more options");
# END Search

# share button
define(LANG_SHARE_THIS_PAGE,"Diese Seite teilen");
define(LANG_SHARE_EMAIL_SUBJECT,"Besuchen Sie die folgende Seite auf gerhard-richter.com");
define(LANG_SHARE_EMAIL_BODY_1,"Dear friend,");
define(LANG_SHARE_EMAIL_BODY_2,"I would like to share the following page with you:");
define(LANG_SHARE_EMAIL_BODY_3,"Kind regards,");
define(LANG_SHARE_EMAIL,"E-mail");
# END share button

# Mobile site specific
define(LANG_MOBILE_SEARCH_FIND,"Find");
# END Mobile site specific
?>
