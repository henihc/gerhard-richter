<?php
# meta info
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION,"Gerhard Richter");
define(LANG_META_DESCRIPTION_HOME,"Find out more about the life and career of Gerhard Richter, one of the most important artists of the 20th and 21st centuries.");
define(LANG_META_DESCRIPTION_ART,"Discover Richter's versatile oeuvre, ranging from oils on canvas, works on paper, overpainted photographs and Atlas, his collection of photographs, newspaper cuttings and sketches.");
define(LANG_META_DESCRIPTION_BIOGRAPHY,"The biography sheds light on Richter's life and career. Born in Dresden in 1932, Richter was brought up under Third Reich and received the first part of his artistic education in East Germany. In 1961 he relocated to West Germany, where he developed into one of the most distinguished artists of his time.");
define(LANG_META_DESCRIPTION_CHRONOLOGY,"A concentrated overview of Richter's life and career: important chapters of his private life are mirrored by significant artistic achievements.");
define(LANG_META_DESCRIPTION_QUOTES,"A selection of Richter's quotes offer insight into his way of working, his understanding of art and his view of the world.");
define(LANG_META_DESCRIPTION_EXHIBITIONS,"Research over 50 years of exhibition history of Gerhard Richter's works.");
define(LANG_META_DESCRIPTION_LITERATURE,"Research Richter's artist's books, publications on his oeuvre as well as newspaper articles and online publications.");
define(LANG_META_DESCRIPTION_VIDEOS,"Watch videos on Richter's recent exhibitions, on selected groups of his works and talks.");
define(LANG_META_DESCRIPTION_LINKS,"Find links to articles in newspapers and magazines as well as to galleries dealing with Richter's works.");
define(LANG_META_DESCRIPTION_NEWS,"Current news: ongoing exhibitions, upcoming auctions and recent publications can be found here.");
# END meta info

#PAGE TITLES
if( $_SESSION['kiosk'] ) define(LANG_TITLE_HOME,"Gerhard-Richter.com");
else define(LANG_TITLE_HOME,"Gerhard Richter");
#PAGE TITLES END

# MENU
define(LANG_HEADER_MENU_HOME,'Accueil');
define(LANG_HEADER_MENU_HOME_TT,'Page d’accueil Gerhard Richter');
define(LANG_HEADER_MENU_ARTWORK,'Œuvres');
define(LANG_HEADER_MENU_ARTWORK_TT,'Œuvres de Gerhard Richter incluant ses tableaux, les atlas, les tirages d\'Art, les photographies peintes, les aquarelles et les dessins');
define(LANG_HEADER_MENU_BIOGRAPHY,'Biographie');
define(LANG_HEADER_MENU_BIOGRAPHY_TT,'L’histoire de la vie de Gerhard Richter');
define(LANG_HEADER_MENU_CHRONOLOGY,'Chronologie');
define(LANG_HEADER_MENU_CHRONOLOGY_TT,' Exposé chronologique de la vie de Gerhard Richter ');
define(LANG_HEADER_MENU_QUOTES,'Citations');
define(LANG_HEADER_MENU_QUOTES_TT,' Citations de Gerhard Richter');
define(LANG_HEADER_MENU_EXHIBITIONS,'Expositions');
define(LANG_HEADER_MENU_EXHIBITIONS_TT,' Expositions de Gerhard Richter dans le monde entier ');
define(LANG_HEADER_MENU_LITERATURE,'Publications');
define(LANG_HEADER_MENU_LITERATURE_TT,'Ouvrages publiés sur l’œuvre de Gerhard Richter');
define(LANG_HEADER_MENU_VIDEOS,'Vidéos');
define(LANG_HEADER_MENU_VIDEOS_RELATED,'Vidéos associées');
define(LANG_HEADER_MENU_VIDEOS_TT,' Vidéos et entretiens de Gerhard Richter ');
define(LANG_HEADER_MENU_AUDIO,'Audio');
define(LANG_HEADER_MENU_AUDIO_TT,' Enregistrements audio de Gerhard Richter ');
define(LANG_HEADER_MENU_MEDIA,'Média');
define(LANG_HEADER_MENU_MEDIA_TT,' Média de Gerhard Richter ');
define(LANG_HEADER_MENU_CONTACT,'Liens');
define(LANG_HEADER_MENU_CONTACT_TT,' Liens en rapport avec Gerhard Richter ');
define(LANG_HEADER_MENU_NEWS,'Actualités');
define(LANG_HEADER_MENU_NEWS_TT,' Actualités de Gerhard Richter');
define(LANG_HEADER_MENU_SEARCH,'Rechercher');
define(LANG_HEADER_MENU_SEARCH_TT,'Recherche sur Gerhard Richter');
define(LANG_NEWS,'Actualités');
define(LANG_HEADER_MENU_ADMIN,'Admin');
# MENU END

#LEFT SIDE
define(LANG_LEFT_SIDE_PAINTINGS,'Peintures');
define(LANG_LEFT_SIDE_CATEGORIES,'Catégories');
define(LANG_LEFT_SIDE_ATLAS,'Atlas');
define(LANG_LEFT_SIDE_EDITIONS,"Tirages d'Art");
define(LANG_LEFT_SIDE_YEARS,'Années');
define(LANG_LEFT_SIDE_DRAWINGS,'Dessins');
define(LANG_LEFT_SIDE_OILSONPAPER,'Huile sur papier');
define(LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS,'Photographies Peintes');
define(LANG_LEFT_SIDE_WATERCOLOURS,'Aquarelles');
define(LANG_LEFT_SIDE_OTHER,'Autres');
define(LANG_LEFT_SIDE_PRINTS,"Prints");
define(LANG_LEFT_SIDE_MICROSITES,'Micro-sites');
#LEFT SIDE END

#RIGHT SIDE
define(LANG_RIGHT_SEARCH_H1,'Recherche');
define(LANG_RIGHT_SEARCH_EXH_H1,'Recherche');
define(LANG_RIGHT_SEARCH_QUOTES_H1,'Recherche');
define(LANG_RIGHT_SEARCH_VIDEOS_H1,'Recherche');
define(LANG_RIGHT_SEARCH_SEARCHNOW,'Chercher');
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE1,"par");
define(LANG_RIGHT_SEARCH_BYARTWORKTYPE2,"technique");
define(LANG_RIGHT_SEARCH_BYTITLE1,"par");
define(LANG_RIGHT_SEARCH_BYTITLE2,"titre");
define(LANG_RIGHT_SEARCH_BY,"par");
define(LANG_RIGHT_SEARCH_BYPAINTINGS," référence CR (Tableaux)");
define(LANG_RIGHT_SEARCH_BYATLAS,"planche");
define(LANG_RIGHT_SEARCH_BYEDITION,"référence CR (Tirages d'Art)");
define(LANG_RIGHT_SEARCH_BYDRAWINGS,"référence CR (Dessins)");
define(LANG_RIGHT_SEARCH_BYNUMBER,"référence");
define(LANG_RIGHT_SEARCH_ADVANCED_SEARCH,"Recherche avancée");
define(LANG_RIGHT_SEARCH_QUICK_SEARCH,"Recherche rapide");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST,"liste");
define(LANG_RIGHT_SEARCH_BYNUMBER_RANGE,"plage");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE,"Ajouter");
define(LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE,"Retirer");
define(LANG_RIGHT_SEARCH_YEARPAINTED1,"par");
define(LANG_RIGHT_SEARCH_YEARPAINTED2,"année");
define(LANG_RIGHT_SEARCH_DATE,"date");
define(LANG_RIGHT_SEARCH_DATE_FROM,"de");
define(LANG_RIGHT_SEARCH_DATE_TO,"à");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_D,"JJ");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_M,"MM");
define(LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y,"AAAA");
define(LANG_RIGHT_SEARCH_COLOR1,"par");
define(LANG_RIGHT_SEARCH_COLOR2,"Couleur");
define(LANG_RIGHT_SEARCH_BYSIZE,"taille");
define(LANG_RIGHT_SEARCH_BYSIZEH,"H");
define(LANG_RIGHT_SEARCH_BYSIZEH1,"Hauteur en cm");
define(LANG_RIGHT_SEARCH_BYSIZEW,"L");
define(LANG_RIGHT_SEARCH_BYSIZEW1,"Largeur en cm");
define(LANG_RIGHT_SEARCH_BYSIZEMIN,"Min");
define(LANG_RIGHT_SEARCH_BYSIZEMAX,"Max");
define(LANG_RIGHT_SEARCH_BY_COLLECTION,"par <strong>lieu</strong>");
define(LANG_RIGHT_SEARCH_SEARCHBUTTON,"Chercher");
define(LANG_RIGHT_SEARCH_SEARCHRESET,"Réinit.");
define(LANG_RIGHT_SEARCH_EXH_LOCATION,"Lieu");
define(LANG_RIGHT_SEARCH_EXH_TITLE,"Titre");
define(LANG_RIGHT_SEARCH_EXH_KEYWORD,"Mot-clefs");
define(LANG_RIGHT_SEARCH_QUOTES_KEYWORD,"Mot-clefs");
define(LANG_RIGHT_SEARCH_VIDEOS_KEYWORD,"Mot-clefs");
define(LANG_RIGHT_SEARCH_QUOTES_SEARCHALL,"Consulter toutes les citations");
define(LANG_RIGHT_SEARCH_LIT_AUTHOR,"Auteur");
define(LANG_RIGHT_SEARCH_LIT_TITLE,"Titre");
define(LANG_RIGHT_SEARCH_LIT_KEYWORD,"Mot-clefs");
define(LANG_RIGHT_SEARCH_LIT_CATEGORY,"Catégorie");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT,"sélectionnez...");
define(LANG_RIGHT_SEARCH_LIT_LANGUAGE,"Langue");
define(LANG_RIGHT_FEATURED_TITLES,"Micro-sites");
define(LANG_RIGHT_FEATURED_TITLES_SNOWWHITE,"Snow White");
define(LANG_RIGHT_FEATURED_TITLES_WARCUT,"War Cut II");
define(LANG_RIGHT_FEATURED_TITLES_FIRENZE,"Firenze");
define(LANG_RIGHT_FEATURED_TITLES_4900COLOURS,"4900 Couleurs");
define(LANG_RIGHT_FEATURED_TITLES_SINBAD,"Sinbad");
define(LANG_RIGHT_FEATURED_TITLES_ELBE,"Elbe");
define(LANG_RIGHT_FEATURED_TITLES_COLOGNE_CATHEDRAL,"Cathédrale de Cologne");
define(LANG_RIGHT_TIMELINE,"<span class='related'>Frise</span><br />Explorer la carrière de l’artiste.");
define(LANG_RIGHT_TIMELINE_INTER,"Frise chronologique interactive");
define(LANG_RIGHT_VIRTUALEXH," </span><br />Expositions <span class='related'>Virtuelle ");
define(LANG_RIGHT_FETURED_TITLES,"Titres phares");
define(LANG_RIGHT_VIDEO,"Pour réussir à regarder ces vidéos, il est nécessaire que le lecteur Flash soit installé sur votre ordinateur.");
define(LANG_RIGHT_VIDEO_DOWN,"Téléchargement");
define(LANG_RIGHT_AUDIO,"Pour réussir à écouter les enregistrements audio il est nécessaire que le lecteur Flash soit installé sur votre ordinateur.");
define(LANG_RELATED_BLOCK_BROWSE_CLIPS,"All clips");
define(LANG_RELATED_BLOCK_RECENTLY,"Recently Added");
#RIGHT SIDE END

# credits page
define(LANG_CREDITS,'Crédits');
define(LANG_CREDITS_TEXT1_1,'Le site officiel de Gerhard Richter');
define(LANG_CREDITS_TEXT1_2,'a été crée et est entretenu par Joe Hage.');
define(LANG_CREDITS_TEXT2_1,"Remerciements à Dietmar Elger (l'auteur de");
define(LANG_CREDITS_TEXT2_2,'Gerhard Richter, Catalogue Raisonné, Vol. 1, 2011');
define(LANG_CREDITS_TEXT2_3,') pour son aide et son soutien.');
define(LANG_CREDITS_TEXT3_1,"Remerciements à Hubertus Butin  pour son travail de recherche approfondi et détaillé sur");
define(LANG_CREDITS_TEXT3_2,"les tirages d'Art de Gerhard Richter.");
define(LANG_CREDITS_TEXT3_3,"");
define(LANG_CREDITS_TEXT4,'Remerciements à Amy Dickson pour son travail sur la Chronologie.');
# end

# footer
define(LANG_FOOTER_COPYRIGHT,'Gerhard Richter – Tous droits réservés');
define(LANG_FOOTER_CONTACT,'Contactez-nous');
define(LANG_FOOTER_CREDITS,'Crédits');
define(LANG_FOOTER_DISCLIMER,'Mentions légales');
# footer END

# home page
define(LANG_HOME_TEXT_INTRO1,'Gerhard Richter est un artiste majeur du vingtième et du vingt-et-unième siècle; son travail s\'étend sur près de cinq décennies. Sur ce site, vous pouvez voir son travail et en apprendre davantage sur sa vie. Cliquez sur une œuvre ci-dessous pour commencer.');
# home END

# artwork page
define(LANG_ARTWORK_PHOTO_PAINTINGS,'Photos-peintures');
define(LANG_ARTWORK_ABSTRACTS,'Tableaux Abstraits');
define(LANG_ARTWORK_ATLAS,'Atlas');
define(LANG_ARTWORK_ATLAS_SHEET,'Atlas Sheet');
define(LANG_ARTWORK_EDITIONS,"Tirages d'Art");
define(LANG_ARTWORK_WATERCOLOURS,'Aquarelles');
define(LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS,'Photographies Peintes');
define(LANG_ARTWORK_DRAWINGS,'Dessins');
define(LANG_ARTWORK_OILSONPAPER,'Huile sur papier');
define(LANG_ARTWORK_OTHER,'Autres');
define(LANG_ARTWORK_MICROSITES,'Micro-sites');
define(LANG_ARTWORK_MICROSITES2,'Micro-sites');
define(LANG_ARTWORK_ARTISTS_BOOKS,"Livres d'Artiste");
define(LANG_ARTWORK_TEXT_H1,"Un demi-siècle d’œuvres d’art");
define(LANG_ARTWORK_TEXT,"Richter a commencé à peindre officiellement en 1962. Sur ce site nous vous donnons accès à la diversité de ses œuvres, allant de l'huile sur toile aux photographies peintes, ainsi qu’aux références historique d'images-source dont la collection apparaît dans l'<i>Atlas</i>.");
define(LANG_ARTWORK_TEXT_PAINTINGS1,"Richter officially began painting in 1962. Ici vous pouvez accéder aux œuvres de l’artiste que contiennent les tableaux Photo-peintures et les abstraits.");
define(LANG_ARTWORK_TEXT_PAINTINGS,'Bien que l’artiste évite intentionnellement de classifier son travail, nous avons classé ses œuvres par catégories subjectives pour en faciliter leurs accès.');
define(LANG_ARTWORK_SECTION_PAINTINGS,"Travaux");
define(LANG_ARTWORK_SECTION_PHOTOPAINTINGS,"Photos-peintures");
define(LANG_ARTWORK_SECTION_ABSTRACTS," Tableaux Abstraits");
define(LANG_ARTWORK_SECTION_NOCR,"Sans CR");
define(LANG_ARTWORK_SECTION_OTHERMEDIA,"Autres");
define(LANG_ARTWORK_SECTION_OTHER,"Œuvres détruites");
define(LANG_ARTWORK_TEXT_ATLAS," Voir la collection complète Atlas - les coupures de journaux, les photos et les croquis qui sont les sources d'une grande partie du travail de Richter.");
define(LANG_ARTWORK_TITLE_OVERPAINTED_PHOTOS,"Catalogue raisonné des Photographies Peintes de Gerhard Richter");
define(LANG_ARTWORK_TEXT_OVERPAINTED_PHOTOS,"Un catalogue raisonné des photographies peintes de Gerhard Richter à paraître en 2013. Nous sommes, par conséquent, à la recherche de toutes informations susceptibles d'être utiles à cet ouvrage, y compris des renseignements relatifs aux images et à la propriété des oeuvres. Nous vous serions très reconnaissants si vous pouviez nous fournir de telles informations. Pour cela, vous êtes priés de bien vouloir nous contacter. Les renseignements livrés seront traités de manière confidentielle.");
define(LANG_ARTWORK_CONTACT_OVERPAINTED_PHOTOS,"Envoyer les renseignements");
define(LANG_ARTWORK_TEXT_ATLAS_BACKTOATLAS,'retour à l’Atlas');
define(LANG_ARTWORK_TEXT_ATLAS_ASOCIATED,'Œuvres associées');
define(LANG_ARTWORK_BROWSE_WORKS,'Parcourir toutes les peintures');
define(LANG_ARTWORK_BROWSE_OPP,'Parcourir toutes les Photographies Peintes');
define(LANG_ARTWORK_BROWSE_ATLAS,'Parcourir tous les Atlas');
define(LANG_ARTWORK_BROWSE_EDITIONS,"Parcourir toutes les Tirages d'Art");
define(LANG_ARTWORK_ATLAS_ASSOCIATED,'Les vignettes ci-dessous vous montrent les images d’après lesquelles les tableaux sont crées sur la planche d’Atlas en question.');
define(LANG_ARTWORK_BOOKS_REL_TAB,'Publications');
define(LANG_ARTWORK_BOOKS_REL_COLOUR,'en couleur');
define(LANG_ARTWORK_BOOKS_REL_BW,'n&b');
define(LANG_ARTWORK_BOOKS_REL_ARTWORK,'Œuvre');
define(LANG_ARTWORK_BOOKS_REL_ILLUSTRATED,'Reproduit(e)');
define(LANG_ARTWORK_BOOKS_REL_MENTIONED,'Cité(e)');
define(LANG_ARTWORK_BOOKS_REL_DISCUSSED,'Détaillé(e)');
define(LANG_ARTWORK_BOOKS_REL_P,'p.');
define(LANG_ARTWORK_BOOKS_REL_PP,'pp.');
define(LANG_ARTWORK_BOOKS_REL_P2,'');
define(LANG_ARTWORK_BOOKS_REL_PP2,'');
define(LANG_ARTWORK_BOOKS_REL_CHINESE,'');
define(LANG_ARTWORK_PHOTOS_TAB,'Photos');
define(LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS,"Vues d'installation:");
define(LANG_ARTWORK_PHOTOS_TAB_DEATILS,'Détails');
define(LANG_ARTWORK_THUMB_SALES_HIST_AVAIL,'Historique de vente disponible');
define(LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL,'Informations relative à la collection disponibles');
define(LANG_ARTWORK_BREADCRUMB_ALL,'Toutes les peintures');
define(LANG_ARTWORK_BREADCRUMB_ALL_OPP,'Toutes les photographies');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN,'Cette œuvre a été montrée dans les expositions suivantes:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED,'Cette œuvre est incluse dans les publications suivantes:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS,"Cette œuvre est incluse dans les publications suivantes:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS,"Photographies détaillées de l'œuvre:");
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS,'Cette œuvre est basée sur une image incluse dans <i>Atlas</i>:');
define(LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC,'Cette planche <i>d’Atlas</i> comprend les images sources des œuvres suivantes:');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB,'Œuvres individuelles');
define(LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC,'This edition consists of the following individual works:');
define(LANG_ARTWORK_DESTROYED,"DÉTRUIT");
# artwork END

# Tabs titles
define(LANG_TAB_TITLE_ARTWORKS,'Œuvres');
# Tabs titles END

# contact page
define(LANG_CONTACT_SUBNAV_TEXT_HEADING,'Liens de l’Artiste');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM1,'Nous écrire');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM2,'Principaux marchands');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM4,'Marchands');
define(LANG_CONTACT_SUBNAV_TEXT_ITEM3,'Vente d’affiches');
define(LANG_CONTACT_WRITE_TEXT_BRIEF,'Si vous voulez entrer en contact avec le site web de Gerhard Richter, utilisez notre formulaire de contact.');
define(LANG_CONTACT_WRITE_INPUTVALUE_EMAIL,'Entrer votre adresse email');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT1,'Demande d’information générale/Commentaire');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT2,'Trouver un marchand');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT3,'Achat d’affiche des éditions');
define(LANG_CONTACT_WRITE_INPUTVALUE_SUBJECT4,'Image permissions');
define(LANG_CONTACT_WRITE_INPUTVALUE_MESSAGE,'Entrer votre message ici');
define(LANG_CONTACT_WRITE_INPUTVALUE_SEND,'Envoyer');
define(LANG_CONTACT_POSTERS_LINK,"Tirage d’affiche d'Art de Gerhard Richter ");
define(LANG_CONTACT_SHOP_LINK,'La boutique Gerhard Richter');
define(LANG_CONTACT_EMAIL,'Addresse email');
define(LANG_CONTACT_SUBJECT_SELECT,'Selectionner un sujet');
define(LANG_CONTACT_SUBJECT,'Sujet');
define(LANG_CONTACT_FILE,'File');
define(LANG_CONTACT_MESSAGE,'Message');
define(LANG_CONTACT_CAPCHA,'Veuillez entrer les caractères affichés dans le champ ci-dessous, en ne laissant pas d’espaces et en utilisant uniquement des lettres minuscules.');
define(LANG_CONTACT_THANX1,'Merci');
define(LANG_CONTACT_THANX2,'Merci d’avoir contacté le site web de Gerhard Richter.');
# contact END

# links
define(LANG_LINKS_ARTICLES,'Articles');
define(LANG_LINKS_DEALERS,'Marchands');
define(LANG_LINKS,'Liens');
define(LANG_LINKS_ARTICLES_PUBLISHED_IN,'Dans');
# links END

# IF NO IMAGE To DISPLAY
define(LANG_NO_IMAGE_XSMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_xsmall.png');
define(LANG_NO_IMAGE_SMALL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_small.png');
define(LANG_NO_IMAGE_MEDIUM,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_LARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_medium.png');
define(LANG_NO_IMAGE_XLARGE,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_ORIGINAL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_large.png');
define(LANG_NO_IMAGE_THUMBNAIL,$_SERVER["DOCUMENT_ROOT"].'/g/no-img-available/no_image_thumbnail.png');
# IF NO IMAGE To DISPLAY END

# DIFFERENT
define(LANG_ATLASSHEETS,'Planches d’Atlas');
define(LANG_RELEATED,'Sections associées');
define(LANG_RELEATED_ART,'Œuvres associées');
define(LANG_COLLECTION,'Collection');
define(LANG_NOTES,'Notes');
define(LANG_SHOP,'Boutique');
define(LANG_SHOP_INFO,'Articles disponibles en ligne sur');
define(LANG_NEXTDECADE,'décennie suivante');
define(LANG_PREVDECADE,'décennie précédente');
define(LANG_SELECT,'sélectionnez..');
define(LANG_SELECT_FROM,'de');
define(LANG_SELECT_TO,'à');
define(LANG_DIMENSIONS_FROM,'de');
define(LANG_DIMENSIONS_TO,'à');
define(LANG_BACK_TO_ALL_KEYWORDS,'Tout voir');
define(LANG_BACK_TO_ALL,'Tout voir');
define(LANG_SEE_ALL,'Tout voir');
define(LANG_SEE,'Voir');
define(LANG_CLICK_IMAGE_TO_ENLARGE,'Cliquer sur l’image pour l’agrandir');
define(LANG_CATALOGUE_RESONE,"Catalogue Raisonn&eacute;");
define(LANG_ATLAS_SHEET,"Planche d’Atlas");
define(LANG_GO_TO_ATLAS_SHEET,"Aller à la planche d’Atlas");
define(LANG_MUSEUM_COLLECTION,"Collection de musée");
define(LANG_SALES_HISTORY,"Ventes");
define(LANG_SALES_HISTORY_UPON_REQUEST,"Contacter la maison d'enchères");
define(LANG_SALES_HISTORY_TB_ANNOUNCED,"sera annoncé");
define(LANG_OF,"sur");
define(LANG_IN,"de");
define(LANG_SEARCH_RESULTS_ALL,"Résultats de recherche");
define(LANG_SEARCH_PAINTINGS,"Recherche");
define(LANG_SEARCH_RESULT, "Résultat");
define(LANG_SEARCH_RESULTS, "Résultats");
define(LANG_SEARCH_ART, "Œuvres");
define(LANG_SEARCH_BIOGRAPHY, "Biographie");
define(LANG_SEARCH_QUOTES, "Citations");
define(LANG_SEARCH_EXHIBITIONS, "Expositions");
define(LANG_SEARCH_LITERATURE, "Publications");
define(LANG_SEARCH_VIDEOS, "Vidéos");
define(LANG_SEARCH_LINKS, "Liens");
define(LANG_SEARCH_NEWS_TALKS, "Conférence");
define(LANG_SEARCH_NEWS_AUCTIONS, "Enchères");
define(LANG_SEARCH_CREDITS, "Crédits");
define(LANG_SEARCH_DISCLAIMER, "Mentions légales");
define(LANG_SEARCH_CHRONOLOGY, "Chronologie");
define(LANG_SEARCH_BASIC_TOP_VALUE_SEARCH, "Recherche rapide");
define(LANG_SEARCH_SHOW_ALL, "Montrer tout");
define(LANG_SEARCH_YOUSEARCHEDFOR,"Vous avez cherché");
define(LANG_SEARCH_ALLPAINTINGS,"Toutes les œuvres");
define(LANG_SEARCH_NUMBER,"Références");
define(LANG_SEARCH_ARTWORKTYPE," Type d’œuvres ");
define(LANG_SEARCH_TITLE,"Titre");
define(LANG_SEARCH_LOCATION,"Lieu");
define(LANG_SEARCH_SHEETNUMBER,"Numéro de planche");
define(LANG_SEARCH_CRNUMBER,"Référence de CR");
define(LANG_SEARCH_YEARFROM,"de l’année");
define(LANG_SEARCH_YEAR,"Année");
define(LANG_SEARCH_YEARTO,"à l’année");
define(LANG_SEARCH_DATE,"Date");
define(LANG_SEARCH_COLOR,"Color");
define(LANG_SEARCH_HEIGHT,"Hauteur");
define(LANG_SEARCH_WIDTH,"Largeur");
define(LANG_SEARCH_NORESULTS,"Désolé, votre recherche est infructueuse");
define(LANG_SEARCH_RESULTS1,"Résultat de la recherche");
define(LANG_SEARCH_RESULTS2,"Résultats");
define(LANG_SEARCH_RESULTS3,"Résultats");
define(LANG_CATEGORY_DESC_MORE,"Plus");
define(LANG_CATEGORY_DESC_CLOSE,"Fermer");
define(LANG_SEARCH_RETURNTOTSEARCH,"Retourner au derniers résultats de la recherche ");
define(LANG_USE_THIS," Utiliser ce menu déroulant pour accéder à n'importe laquelle des sections ");
define(LANG_BACKTOTOP,"Retour vers le haut");
define(LANG_SHOW,"Montrer");
define(LANG_ALL,"Tout");
define(LANG_PERPAGE,"par page");
define(LANG_IMAGES_PERPAGE,"Show");
define(LANG_BACKTOEXH,"Retour à l’exposition");
define(LANG_GOBACK,"Retour en arrière");
define(LANG_SALEHIST_TAB_NOTES,"Notes:");
define(LANG_ESTIMATE,"Estimation");
define(LANG_BOUGHT_IN,"Invendu");
define(LANG_WITHDRAWN,"Retirée");
define(LANG_PREMIUM,"(incl. la commission acheteur)");
define(LANG_HAMMER,"(prix d’adjudication)");
define(LANG_UNKNOWN,"Unknown");
define(LANG_LOT,"Lot");
define(LANG_SOLDPR,"Vendu");
define(LANG_SHOWING,"Montrant");
define(LANG_BOOKS_OF,"sur");
define(LANG_BOOKS,"livres");
define(LANG_BOOKS_BY,"Auteur");
define(LANG_BOOKS_DETAILS,"Détails");
define(LANG_BOOKS_CLOSE,"Fermer");
define(LANG_BOOKS_MORE,"Plus");
define(LANG_BOOKS_MORE_DETAILS,"More Details");
define(LANG_BOOKS_LANGUAGES,"Langues");
define(LANG_BOOKS_LANGUAGE,"Langue");
define(LANG_BOOKS_CATEGORY,"Catégorie");
define(LANG_CONTACT,"Si vous voulez entrer en contact avec nous, il vous suffit d'utiliser ce formulaire");
define(LANG_CONTACT_UNAVAILABLE," Actuellement indisponible. Nous nous excusons pour tous désagréments.");
define(LANG_CONTACT_UNAVAILABLE2," Si vous voulez entrer en contact avec nous, veuillez envoyer un email à info@gerhard-richter.com");
define(LANG_BOOKS_LANGUAGE_EXHCATALOG,"Expositions");
define(LANG_BOOKS_READ_ARTICLE,"Lire l'article");
define(LANG_COPY_TO_CLIPBOARD,"Copy link to clipboard");
define(LANG_PAINTING_SIZE_PARTS," parties");
define(LANG_SUBMITING_FORM,"submitting form.....");
define(LANG_FOUND,"found");
define(LANG_BOOKS_READ_ESSAY,"Lisez l'essai");
define(LANG_CR,"CR");
define(LANG_CR_EDITIONS,LANG_LEFT_SIDE_EDITIONS." ".LANG_CR);
define(LANG_CR_DRAWINGS,LANG_LEFT_SIDE_DRAWINGS." ".LANG_CR);
# DIFFERENT END

# biograpphy page
define(LANG_BIOGRAPHY,"Biographie");
define(LANG_WORK,"Œuvre");
define(LANG_PHOTOS,"Photos");
define(LANG_QUOTES,"Citations");
define(LANG_QUOTES_NO_QUOTES_FOUND,"Pas de citations trouvées!");
define(LANG_QUOTES_SOURCE,"SOURCE");
# biograpphy page end

#chronology
define(LANG_CHRONOLOGY_H1_1, "Chronologie");
define(LANG_CHRONOLOGY_H1_2, "par Amy Dickson");
define(LANG_AMY_DICKSON_THANKS, "'Remerciements particuliers à Amy Dickson pour son travail sur la Chronologie. Il s’agit d’une version éditée de la Chronologie établie par Amy Dickson telle qu’elle est imprimée dans <i>Gerhard Richter: Panorama</i> (&#169;Tate, 2011. Reproduite avec la permission de Tate Trustees).");
define(LANG_CHRONOLOGY_LEFT_1930, "Les années 1930");
define(LANG_CHRONOLOGY_LEFT_1940, "Les années 1940");
define(LANG_CHRONOLOGY_LEFT_1950, "Les années 1950");
define(LANG_CHRONOLOGY_LEFT_1960, "Les années 1960");
define(LANG_CHRONOLOGY_LEFT_1970, "Les années 1970");
define(LANG_CHRONOLOGY_LEFT_1980, "Les années 1980");
define(LANG_CHRONOLOGY_LEFT_1990, "Les années 1990");
define(LANG_CHRONOLOGY_LEFT_2000, "Les années 2000");
define(LANG_CHRONOLOGY_LEFT_2010, "Les années 2010");
#chronology end

# exhibition
define(LANG_VIEW_EXHIBITION,"Voir l’exposition");
define(LANG_EXH_ONWARDS,"À partir de 2020");
define(LANG_GROUP,"Collective");
define(LANG_GROUP_EXH,"Expositions de groupe");
define(LANG_GROUP_EXHS,"Expositions de groupe");
define(LANG_SOLO,"Personnelle");
define(LANG_SOLO_EXH,"Expositions personnelles");
define(LANG_SOLO_EXHS,"Expositions personnelles");
define(LANG_EXH_DECADES_S,"s");
define(LANG_EXH_SORT_BY,"Trier par");
define(LANG_EXH_SORT_BY_DATE,"Date");
define(LANG_EXH,"Exposition");
define(LANG_EXH_FOUND,"Exposition");
define(LANG_EXH_FOUND_S,"Expositions");
define(LANG_EXH_SHOW_ALL,"Montrer tout");
define(LANG_EXH_ARTWORK,"Œuvres");
define(LANG_EXH_RELATED_EXHIBITIONS,"Expositions similaires");
define(LANG_EXH_INST_SHOTS,"Vues d'exposition");
define(LANG_EXH_LITERATURE,"Publications");
define(LANG_EXH_VIDEOS,"Vidéos");
define(LANG_EXH_GUIDE,"Guide");
define(LANG_EXH_SHOW_ALL_WORKS,"Voir toutes les oeuvres");
define(LANG_EXH_ARTWORK_TAB_WORKS,"œuvres");
define(LANG_EXH_ARTWORK_TAB_WORK,"œuvre");
define(LANG_EXH_INST_TAB_PHOTOS,"photos");
define(LANG_EXH_INST_TAB_PHOTO,"photo");
define(LANG_EXH_ARTWORK_SORT_BY,"Trier par");
define(LANG_EXH_ARTWORK_SORT_CR_NUMBER,"référence");
define(LANG_EXH_ARTWORK_SORT_YEAR,"année");
define(LANG_INSTALLSHOT,"Vues d’Installation");
define(LANG_ALL_EXH,"Toutes les Expositions");
define(LANG_LEFT_SIDE_INSTALLATION_SOTS,"Vues d’Installation");
define(LANG_LEFT_SIDE_GUIDE,"Guide");
define(LANG_LEFT_SIDE_EXH_TOUR,"Visite d’exposition");
# END exhibition

# literature
define(LANG_LIT_LEFT_MONOGRAPHS,"Monographies");
define(LANG_LIT_LEFT_CATALOGUES,"Catalogues");
define(LANG_LIT_LEFT_JOURNALS,"Articles de presse");
define(LANG_LIT_LEFT_NEWS_ART,"Publications en ligne");
define(LANG_LIT_LEFT_ONL_PUBL,"Essais");
define(LANG_LIT_LEFT_OTHERS,"Autres");
define(LANG_LIT_LEFT_FILMS,"Films");
define(LANG_LIT_LEFT_THESES,"Theses");
define(LANG_LIT_LEFT_SOLO_EXH,"Personnelle Expositions");
define(LANG_LIT_LEFT_GROUP_EXH,"Collective Expositions");
define(LANG_LIT_LEFT_COLLECTIONS,"Collections");
define(LANG_LIT_ONWARDS,"À partir de 2010");
define(LANG_LIT_PUBLISHED,"Année de publication");
define(LANG_LIT_SEARCH," Recherche de publications ");
define(LANG_LIT_BOOKS_PER,"livres par page");
define(LANG_LIT_SEARCH_HELP,"header=[Aide à la recherche d’ouvrages] body=[&#60;p&#62; Si vous souhaitez rechercher un ouvrage spécifique, vous devez suivre certains critères.&#60;/p&#62;&#60;p&#62;vous pouvez chercher par titre, auteur, ou avec les deux; ou par mot-clefs.&#60;/p&#62;&#60;p&#62;Exemples:&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;J&uuml;rgen Schilling&rsquo;&#60;/p&#62;&#60;p&#62;- &lsquo;Gerhard Richter, J&uuml;rgen Schilling &#60;/p&#62;&#60;p&#62;- &lsquo;Schilling&rsquo;&#60;/p&#62;]");
define(LANG_LIT_SEARCH_TITLE,"Titre");
define(LANG_LIT_SEARCH_AUTHOR,"Auteur");
define(LANG_LIT_SEARCH_AUTHOR2,"Auteur");
define(LANG_LIT_SEARCH_DATE,"Date");
define(LANG_LIT_SEARCH_SORTBYTITLE,"Classer par titre");
define(LANG_LIT_SEARCH_SORTBYAUTHOR,"Classer par auteur");
define(LANG_LIT_SEARCH_SORTBYDATE,"Classer par date");
define(LANG_LIT_SEARCH_CLICK,"Cliquer pour accéder aux détails");
define(LANG_LIT_SEARCH_PUBBY,"Éditeur");
define(LANG_LIT_SEARCH_EDITOR,"Éditeur");
define(LANG_LIT_SEARCH_UNKNOWN,"Inconnu");
define(LANG_LIT_SEARCH_ISBNNOTAVA,"ISBN non disponible");
define(LANG_LIT_SEARCH_SEERELEXH,"Voir Expositions");
define(LANG_LIT_SEARCH_SEERELEXH2,"Voir Exposition");
define(LANG_LIT_SEARCH_GOTORELEXH,"Accéder aux expositions associées");
define(LANG_LIT_SEARCH_GOTORELEXH2,"Accéder à l’exposition associée");
define(LANG_LIT_SEARCH_PAGES,"pages");
define(LANG_LIT_SEARCH_NOFOUND,"Livres non trouvés, veuillez réessayer.");
define(LANG_LIT_SEARCH_BACKTOLIT,"Retour aux publications");
define(LANG_LIT_SEARCH_INFORMATION,"Informations");
define(LANG_LITERATURE_ENLARGE, "augmenter");
define(LANG_LITERATURE_IN, "de");
define(LANG_LITERATURE_ISSUE, "Publié");
define(LANG_LITERATURE_VOL, "Vol.");
define(LANG_LITERATURE_NO, "No.");
define(LANG_LITERATURE_PAGES, "p.");
define(LANG_LITERATURE_PAGESS, "pp.");
define(LANG_LITERATURE_COVER_HARDBACK, "Relié");
define(LANG_LITERATURE_COVER_SOFTCOVER, "Broché");
define(LANG_LITERATURE_COVER_UNKNOWNBINDING, "Reliure inconnue");
define(LANG_LITERATURE_COVER_SPECIALBINDING, "Relieure spéciale");
define(LANG_LITERATURE_ISBN, "ISBN");
define(LANG_LITERATURE_ISSN, "ISSN");
define(LANG_LITERATURE_DESC_TEXT, "Gerhard Richter's work and life has been the subject of numerous publications. The literature ranges from exhibition catalogues and collection catalogues to monographs, films and articles. Richter's own publications in the form of artist's books are an important part of his œuvre. Please find a selection of Richter related literature here.");
define(LANG_LITERATURE_SHOW_MORE_OPTIONS, "Plus d'options");
define(LANG_LITERATURE_SHOW_LESS_OPTIONS, "Moins d'options");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEO, "Related video");
define(LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS, "Related videos");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION, "This catalogue was published on the occasion of following exhibition:");
define(LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS, "This catalogue was published on the occasion of following exhibitions:");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE, "Related publication");
define(LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES, "Related publications");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK, "The following artwork is displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS, "The following artworks are displayed, discussed or mentioned in the book:");
define(LANG_LITERATURE_TITLE, "Title");
define(LANG_LITERATURE_TITLES, "Titles");
define(LANG_LITERATURE_SHOW_ALL,"Montrer tout");
# literature end

# video
define(LANG_VIDEO_ALLTITLES,"Tous les titres");
define(LANG_VIDEO_VIDEOS,"Vidéos");
define(LANG_VIDEO_AUDIO,"Audio");
define(LANG_AUDIO_CR,"Catalogue Raisonné:");
define(LANG_VIDEO_RUNTIME,"Durée");
define(LANG_VIDEO_WATCH1,"Regarder la vidéo en Quicktime ");
define(LANG_VIDEO_WATCH2,"Regarder la vidéo en Flash ");
define(LANG_VIDEO_WATCH3,"Quicktime");
define(LANG_VIDEO_WATCH4,"Flash");
define(LANG_VIDEO_BACKTOVID,"Retour aux Vidéos");
define(LANG_VIDEO_TAB_INFO, "Information");
define(LANG_VIDEO_TAB_IMAGES, "Images");
define(LANG_VIDEO_ARTWORK_MENTIONED, "Cette œuvre est montrée ou évoquée dans les vidéos suivantes:");
define(LANG_VIDEO_EXHIBITION_MENTIONED, "This exhibition is featured in the following videos:");
define(LANG_VIDEO_IN_SECTION1, "Il y a");
define(LANG_VIDEO_IN_SECTION2, "vidéos dans cette section:");
# video end

# NEWS
define(LANG_NEWS_CURRENT,"En cours");
define(LANG_NEWS_UPCOMING,"À venir");
define(LANG_NEWS_RESULTS,"Résultats");
define(LANG_NEWS_PUBLICATIONS,"Publications");
define(LANG_NEWS_TALKS,"Conférence");
define(LANG_NEWS_AUCTIONS,"Enchères");
define(LANG_NEWS_EXHIBITIONS,"Expositions");
define(LANG_NEWS_ARCHIVE,"Archive");
define(LANG_NEWS_NO_AUCTIONS,"There are currently no artworks coming up for auction.");
define(LANG_NEWS_NOCURRENT,"Pas d’actualités");
define(LANG_NEWS_NO_TALKS,"No information currently available");
define(LANG_NEWS_NOUPCOMING,"Pas d’actualités");
define(LANG_NEWS_NOARCHIVE,"Pas d’archives");
define(LANG_NEWS_NOPUBLICATIONS,"Pas de publications récentes");
define(LANG_NEWS_DOWNLOADLEAFLET,"télécharger la brochure");
define(LANG_NEWS_ALSO_VISIT,"Rejoignez nous aussi sur");
define(LANG_NEWS_SHOW_ALL,"Montrer tout");
define(LANG_NEWS_DISCLIMER,"Mentions légales");
define(LANG_NEWS_DISCLIMER_TEXT1,"Les renseignements sur ce site figurent uniquement à des fins d'information générale. L'information est fournie par <a href='/' title='www.gerhard-richter.com'> www.gerhard-richter.com </a> et bien que nous nous efforçons de garder les informations à jour et exactes, nous ne prétendons ni n'offrons aucune garantie de quelque nature, explicite ou implicite, de l'exhaustivité, la précision, la fiabilité, la pertinence ou de la disponibilité des œuvres du site Web ou des informations présentes sur le site à n'importe quelle fin. La confiance que vous placez dans de telles informations est donc strictement de votre propre responsabilité. En aucun cas nous ne serons responsables d'aucune dommage qui découlerait de, ou serait en relation avec, l'usage de ce site.");
define(LANG_NEWS_DISCLIMER_TEXT2,"Ce site n'est pas, et ne prétend pas être un catalogue raisonné. L'intégration, ou l'exclusion sur ce site de toute œuvre ne peut être ni un constituant sur lequel s'appuyer et ni constituer une représentation ou la garantie que ce travail est ou n'est pas une œuvre authentique de Gerhard Richter.");
define(LANG_NEWS_UPCOMING_FOR_AUCTION,"Enchères");
define(LANG_NEWS_UPCOMING_FOR_AUCTION_DESC,"Les œuvres suivantes vont être mises aux enchères prochainement:");
define(LANG_NEWS_REACEN_AUCTION_RES,"Results");
define(LANG_NEWS_REACEN_AUCTION_RES_DESC,"Les œuvres suivantes ont été vendues aux enchères au cours des six derniers mois:");
define(LANG_NEWS_NEW_PUBLICATIONS,"Publications récentes");
define(LANG_NEWS_PUBLICATIONS_COUNT1,"There currently are");
define(LANG_NEWS_PUBLICATIONS_COUNT2,"new publications");
define(LANG_NEWS_NEW_TALKS,"Conférence");
define(LANG_NEWS_AUCTIONS_AUCTIONHOUSE,"Auction House");
define(LANG_NEWS_AUCTIONS_DATE,"Date");
define(LANG_NEWS_RETURN_TO_NEWS_PAGE,"Return to news page");
# news end

#search box tooltip start
define(LANG_SEARCH_TIP_TITLE,"Aide à la recherche");
define(LANG_SEARCH_TIP_TEXT,"<p class=\"tip\"><span>Référence:</span> entrer un numéro de référence, une liste de numéros utilisant des virgules ex. \"5,137,211\" ou une plage utilisant les deux points ex.\"5:20\".</p><p class=\"tip\"><span>Année:</span> une année en particulier, ou un éventail d’années.</p><p class=\"tip\"><span>Date:</span> par date, ou par période.</p><p class=\"tip\"><span>Taille:</span> Entrer une taille exacte, ou une échelle de taille utilisant les champs: &#8216;de&#8217; et &#8216;à&#8217</p>");
#search box tooltip end

#search box EXH tooltip start
define(LANG_SEARCH_EXH_TIP_TITLE,"Aide à la recherche");
define(LANG_SEARCH_EXH_TIP_TEXT,"<p class=\"tip\"><span>Titre:</span> Rechercher par titre</p><p class=\"tip\"><span>Lieu:</span> Lieu d’exposition</p> <p class=\"tip\"><span>Mot-clefs:</span> Rechercher par Mot-clefs</p> <p class=\"tip\"><span>Année:</span> Une année en particulier, ou un éventail d’années</p>");
#search box EXH tooltip end

#search box QUOTES tooltip start
define(LANG_SEARCH_QUOTES_TIP_TITLE,"Aide de la recherche");
define(LANG_SEARCH_QUOTES_TIP_TEXT,"<p class=\"tip\"><span>Mot-clefs:</span> Rechercher par Mot-clefs</p> <p class=\"tip\"><span>Année:</span> Une année en particulier ou un éventail d’années</p> ");
#search box QUOTES tooltip end

#search box LIT tooltip start
define(LANG_SEARCH_LIT_TIP_TITLE,"Aide le recherche");
define(LANG_SEARCH_LIT_TIP_TEXT,"<p class=\"tip\"><span>Auteur:</span> Chercher par Auteur</p> <p class=\"tip\"><span>Titre:</span> Rechercher par titre</p><p class=\"tip\"><span>Mot-clefs:</span> Rechercher par Mot-clefs</p><p class=\"tip\"><span>Catégorie:</span> Rechercher par catégorie</p><p class=\"tip\"><span>Langue:</span> Rechercher par langue</p><p class=\"tip\"><span>Année:</span> Rechercher une année en particulier, ou un éventail d’années</p>");
#search box LIT tooltip end

#search box ARTICLES tooltip start
define(LANG_SEARCH_ARTICLES_TIP_TITLE,"Aide de la recherche");
define(LANG_SEARCH_ARTICLES_TIP_TEXT,"<p class=\"tip\"><span>Auteur:</span> Rechercher par auteur</p> <p class=\"tip\"><span>Titre:</span> Rechercher par titre</p><p class=\"tip\"><span>Mot-clefs:</span> Rechercher par mot-clefs</p><p class=\"tip\"><span>Année:</span> Rechercher une année en particulier, ou un éventail d’années</p>");
#search box ARTICLES tooltip end

#search box VIDEOS tooltip start
define(LANG_SEARCH_VIDEOS_TIP_TITLE,"Aide à la recherche");
define(LANG_SEARCH_VIDEOS_TIP_TEXT,"<p class=\"tip\"><span>Mot-clefs:</span> Rechercher par Mot-clefs</p>");
#search box VIDEOS tooltip end

#zoomer start
define(LANG_ZOOMER_OPEN,"Voir en détail");
define(LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE,"See Microsite");
#zoomer end

#virtual gallery
define(LANG_VIRTUALGALLERY_OPEN,"Visionner les images dans le diaporama flash");
define(LANG_VIRTUALGALLERY,"Musée Virtuel");
#end

# MICROSITE
define(LANG_MICROSITE_TITLE_SINBAD,"Sinbad");
define(LANG_MICROSITE_SHOW,"montrer");
define(LANG_MICROSITE_PER_PAGE,"par page");
define(LANG_MICROSITE_SEE_ALL,"Tout");
define(LANG_MICROSITE_SEARCH_EDITION,"Recherche de tirages");
define(LANG_MICROSITE_ABOUT,"À propos de");
define(LANG_MICROSITE_SEARCH,"Rechercher");
define(LANG_MICROSITE_SINBAD_TITLE," Impression de Gerhard Richter, Sinbad ");
define(LANG_MICROSITE_SINBAD_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_JS_SITENAME,"Sinbad");
define(LANG_MICROSITE_SINBAD_VIEW_AS_PAIR,"Voir par deux");
define(LANG_MICROSITE_SEARCH_HELP_TITLE,"Aide à la recherche");
define(LANG_MICROSITE_SEARCH_HELP_TEXT,"Pour une recherche de tirage individuel: taper le numéro du tirage (ex: 23)<br />Pour une recherche de plus d'un tirage: séparer chaque numéro de tirage par des virgules (ex: 23, 32, 55)<br />Pour une recherche groupée: taper le numéro de la première à la dernière séparés par un deux points (ex: 23:55)");
define(LANG_MICROSITE_EDITIONS_CR,"Tirages d'Art CR");
define(LANG_MICROSITE_IMAGE,"Image");
define(LANG_MICROSITE_OF,"sur");
define(LANG_MICROSITE_NOVEMBER_FOOTER_1,"For further information on Gerhard Richter and his work,<br />please visit");
define(LANG_MICROSITE_NOVEMBER_FOOTER_2,"and learn about his life and work.");
# MICROSITE END

# OVERPAINTED PHOTOGRAPHS FORM
define(LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS,"Catalogue des photographies peintes");
define(LANG_OPP_FORM_CATALOGUE_RAISONNE," Un catalogue raisonné est un ensemble complet, systématique et exhaustif de textes de référence universitaire ayant pour but d'établir un recueil précis d'œuvres authentiques. Chaque œuvre, connue pour avoir été exécutée par un artiste en particulier, est illustrée, documentée et décrite. Un catalogue raisonné fournit des informations telles que le titre, la date, la technique, les dimensions, les inscriptions, la provenance, l'histoire de l'exposition et la bibliographie. Il attribue à chaque travail un numéro de référence permanent.");
//define(LANG_OPP_FORM_1,"Un <span style='border-bottom:1px dotted #fff;' class='help' titre='".LANG_OPP_FORM_CATALOGUE_RAISONNE."'>catalogue raisonné</span> des Photographies peintes de Gerhard Richter est à paraître en 2015.");
define(LANG_OPP_FORM_1,"Un catalogue complet des Photographies peintes de Gerhard Richter est prévu pour être publié en 2017.");
define(LANG_OPP_FORM_1_1,"Pour plus d'informations ou pour contribuer au catalogue, cliquez ");
define(LANG_OPP_FORM_1_2,"ici");
define(LANG_OPP_FORM_2,"Gerhard Richter a créé un grand nombre de photographies peintes dans sa carrière et des œuvres nouvelles sont produites en permanence. Le catalogue permettra de documenter cet aspect de l'œuvre de Richter, encore largement méconnu à la fois des experts de l'art et du grand public. Il montrera l'éventail du talent de Richter en la matière.");
define(LANG_OPP_FORM_3,"Le catalogue des photographies peintes de Richter vise à être une publication complète qui énumérera toutes les œuvres connues en la matière. Elle établira un inventaire, le plus exact possible, des photographies peintes de Richter authentifiées.");
define(LANG_OPP_FORM_4,"Pour cette raison, nous sommes en train de recueillir sur chaque photographie peinte connue les données suivantes:");
define(LANG_OPP_FORM_5,"titre");
define(LANG_OPP_FORM_6,"année");
define(LANG_OPP_FORM_7,"médium");
define(LANG_OPP_FORM_8,"dimensions");
define(LANG_OPP_FORM_9,"Provenance");
define(LANG_OPP_FORM_10,"historique d’expositions");
define(LANG_OPP_FORM_11,"références bibliographiques ");
define(LANG_OPP_FORM_12,"toutes autres informations utiles");
define(LANG_OPP_FORM_13,"Nous serions très reconnaissants d'obtenir des informations venant de collectionneurs privés et publics souhaitant proposer des photographies peintes pour les inclure dans le catalogue. Pour cela, vous êtes priés de remplir le formulaire en ligne ou de <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>nous contacter</a> par e-mail.");
define(LANG_OPP_FORM_14,"Tout en espérant que vous serez en mesure de fournir autant de détails que possible, nous sommes conscients que certaines informations peuvent être sensibles, personnelles ou privées, de sorte que vous ne devriez pas vous sentir obligé de donner les renseignements demandés sur les sections de ce formulaire que vous ne souhaiteriez pas fournir.");
define(LANG_OPP_FORM_15,"Veuillez être assuré que tous les renseignements fournis seront traités avec la plus stricte confidentialité. Le respect de l’anonymat y sera garanti.");
define(LANG_OPP_FORM_16,"En vous remerciant pour le temps et le soutien que vous apportez à ce projet.");
define(LANG_OPP_FORM_17,"Formulaire de souscription des œuvres");
define(LANG_OPP_FORM_18,"Les informations suivantes seront prises en compte dans la préparation du catalogue de photographies peintes. Ce formulaire est conçu à des fins de recherche et d'archivage uniquement.");
define(LANG_OPP_FORM_19,"Si vous n’êtes pas en mesure de répondre à une question, veuillez laisser le champ libre. En cas de difficultés pour remplir ce formulaire, n’hésitez surtout pas à <a href='mailto:".EMAIL_OPP."?subject=Gerhard%20Richter%20-%20Overpainted%20Photographs' title='".EMAIL_OPP."' class='a-opp-email'>nous contacter</a>, nous serions heureux de vous venir en aide.");
define(LANG_OPP_FORM_20,"Titre de l’œuvre");
define(LANG_OPP_FORM_21_1,"jj/mm/aaaa");
define(LANG_OPP_FORM_21,"Date de l'oeuvre");
define(LANG_OPP_FORM_22,"Technique");
define(LANG_OPP_FORM_23,"Huile sur photo en couleur");
define(LANG_OPP_FORM_24,"Huile sur photo en N&B");
define(LANG_OPP_FORM_25,"Émail sur photo en couleur");
define(LANG_OPP_FORM_26,"Émail sur photo en N&B ");
define(LANG_OPP_FORM_26_1,"Oil on gelatin silver print");
define(LANG_OPP_FORM_27,"Dimensions de la photo (h &#215; l)");
define(LANG_OPP_FORM_28,"cm");
define(LANG_OPP_FORM_29,"pouces");
define(LANG_OPP_FORM_30,"Dimensions du cadre (h &#215; l)");
define(LANG_OPP_FORM_31,"Veuillez indiquer la manière dont vous souhaitez que la mention figure dans le catalogue. Si vous préférez rester anonyme, veuillez préciser «collection privée», en ajoutant le lieu où la pièce est entreposée, si vous souhaitez que cela soit inclu.");
define(LANG_OPP_FORM_32,"Mention/ Remerciements");
define(LANG_OPP_FORM_33,"Séries / Tirage d'Art");
define(LANG_OPP_FORM_35,"Firenze (edition)");
define(LANG_OPP_FORM_36,"Florence (series)");
define(LANG_OPP_FORM_37,"Grauwald");
define(LANG_OPP_FORM_38,"Museum Visit");
define(LANG_OPP_FORM_38_1,"Sils");
define(LANG_OPP_FORM_38_2,"Wald");
define(LANG_OPP_FORM_38_3,"128 Fotos von einem Bild (Halifax 1978)");
define(LANG_OPP_FORM_38_4,"8.2.92");
define(LANG_OPP_FORM_39,"Séries / Numéro / Nombre du <br />tirage (si cela s’applique)");
define(LANG_OPP_FORM_40_1,"Recto:");
define(LANG_OPP_FORM_40_1_1,"Signé");
define(LANG_OPP_FORM_40_1_2,"Daté");
define(LANG_OPP_FORM_40_1_3,"Numeroté");
define(LANG_OPP_FORM_40_1_4,"Inscrit");
define(LANG_OPP_FORM_40_2,"Verso:");
define(LANG_OPP_FORM_40_2_1,"Signé");
define(LANG_OPP_FORM_40_2_2,"Daté");
define(LANG_OPP_FORM_40_2_3,"Numeroté");
define(LANG_OPP_FORM_40_2_4,"Inscrit");
define(LANG_OPP_FORM_41,"encadré");
define(LANG_OPP_FORM_41_1,"on backing board");
define(LANG_OPP_FORM_42,"sur photographie");
define(LANG_OPP_FORM_42_1,"verso");
define(LANG_OPP_FORM_43,"en haut à gauche");
define(LANG_OPP_FORM_44,"en haut au centre");
define(LANG_OPP_FORM_45,"en haut à droite");
define(LANG_OPP_FORM_46,"au centre à gauche");
define(LANG_OPP_FORM_47,"au centre");
define(LANG_OPP_FORM_48,"au centre à droite");
define(LANG_OPP_FORM_49,"en bas à gauche");
define(LANG_OPP_FORM_50,"en bas au centre");
define(LANG_OPP_FORM_51,"en bas à droite");
define(LANG_OPP_FORM_53,"Autres inscriptions");
define(LANG_OPP_FORM_54,"Notations / Étiquettes au dos");
define(LANG_OPP_FORM_55,"Télécharger une image");
define(LANG_OPP_FORM_55_1,"Il est possible de télécharger plus d'une image");
define(LANG_OPP_FORM_56,"Nous apprécierions que les images soient en haute résolution avec au moins 300dpi (dans la mesure du possible, incluant l’emplacement sur l’œuvre de la signature de l'artiste).");
define(LANG_OPP_FORM_57,"Formats recommandés: JPEG, TIFF, PSD, PNG");
define(LANG_OPP_FORM_58,"Copyright / Crédits photographiques");
define(LANG_OPP_FORM_59,"Provenance");
define(LANG_OPP_FORM_60,"Cédé(e) par");
define(LANG_OPP_FORM_61,"Date d’acquisition");
define(LANG_OPP_FORM_62,"Historique des expositions");
define(LANG_OPP_FORM_62_1,"Exhibition history other");
define(LANG_OPP_FORM_63,"Historique des publications");
define(LANG_OPP_FORM_63_1,"Publication history other");
define(LANG_OPP_FORM_64,"Détails personnels");
define(LANG_OPP_FORM_65,"Nom ");
define(LANG_OPP_FORM_66,"Prénom");
define(LANG_OPP_FORM_67,"Email");
define(LANG_OPP_FORM_68,"Adresse");
define(LANG_OPP_FORM_68_1,"Code postal");
define(LANG_OPP_FORM_68_2,"Pays");
define(LANG_OPP_FORM_69,"Téléphone");
define(LANG_OPP_FORM_70,"Commentaires supplémentaires");
define(LANG_OPP_FORM_71,"Champ obligatoire");
define(LANG_OPP_FORM_72,"En soumettant ce formulaire son dépositaire certifie que tous les renseignements fournis sont exacts et qu'aucune information relative à l’œuvre d'art, son histoire ou son appartenance n'a été sciemment omise.");
define(LANG_OPP_FORM_73,"Il est entendu que le dépôt de ce formulaire par son dépositaire ne garantiera pas la référenciation de l'œuvre dans le catalogue.");
define(LANG_OPP_FORM_74,"Soumettre le formulaire");
define(LANG_OPP_FORM_75,"Veuillez, s’il-vous-plaît, lire les deux derniers paragraphes de la page précédente et cocher la case pour indiquer que vous en acceptez les conditions.");
define(LANG_OPP_FORM_75_2,"Please fill in all required fields!");
define(LANG_OPP_FORM_75_3,"Verification characters you entered where not correct please try again!");
define(LANG_OPP_FORM_76,"Votre formulaire a été envoyé.");
define(LANG_OPP_FORM_77,"Avec nos remerciements pour le temps que vous avez accordé.");
define(LANG_OPP_FORM_77_2,"Imprimer le formulaire");

# exhibition list
define(LANG_OPP_FORM_78_9_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_78_9_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_78_9_D,", mai &#8211; juin 2016");
define(LANG_OPP_FORM_78_8_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_78_8_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_78_8_D,", novembre &#8211; décembre 2015");
define(LANG_OPP_FORM_78_7_T,"Gerhard Richter. Das kleine Format");
define(LANG_OPP_FORM_78_7_L,", Galerie Schwarzer, Dusseldorf");
define(LANG_OPP_FORM_78_7_D,", avril &#8211; juin 2015");
define(LANG_OPP_FORM_78_6_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_6_L,", The Lone T Art Space, Milan");
define(LANG_OPP_FORM_78_6_D,", avril &#8211; mai 2015");
define(LANG_OPP_FORM_78_5_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_78_5_L,", Hannah Hoffman Gallery, Los Angeles");
define(LANG_OPP_FORM_78_5_D,", mars &#8211; avril 2015");
define(LANG_OPP_FORM_78_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_78_4_L,", Repetto Gallery, Londres");
define(LANG_OPP_FORM_78_4_D,", Février &#8211; Mars 2015");
define(LANG_OPP_FORM_78_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_3_L,", Marian Goodman Gallery, Londres");
define(LANG_OPP_FORM_78_3_D,", octobre &#8211; décembre 2014");
define(LANG_OPP_FORM_78_1_T,"What is a Photograph");
define(LANG_OPP_FORM_78_1_L,", International Center of Photography, New York");
define(LANG_OPP_FORM_78_1_D,", janvier &#8211; mai 2014");
define(LANG_OPP_FORM_78_2_T,"Purer Zufall. Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_78_2_L,", Sprengel Museum Hannover, Hanover");
define(LANG_OPP_FORM_78_2_D,", mai &#8211; septembre 2013");
define(LANG_OPP_FORM_78_T,"Gerhard Richter");
define(LANG_OPP_FORM_78_L,", Beirut Art Center, Beirut");
define(LANG_OPP_FORM_78_D,", avril &#8211; juin 2012");
define(LANG_OPP_FORM_79_T,"Gerhard Richter &#8211; Arbeiten 1968-2008");
define(LANG_OPP_FORM_79_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_79_D,", novembre 2011 &#8211; janvier 2012");
define(LANG_OPP_FORM_80_T,"Editionen und übermalte Fotografien");
define(LANG_OPP_FORM_80_L,", Wolfram Völcker Fine Art, Berlin");
define(LANG_OPP_FORM_80_D,", septembre &#8211; octobre 2011");
define(LANG_OPP_FORM_81_T,"De-Natured: German Art From Joseph Beuys to Martin Kippenberger");
define(LANG_OPP_FORM_81_L,", Ackland Art, Museum, Chapel Hill");
define(LANG_OPP_FORM_81_D,", avril &#8211; juillet 2011");
define(LANG_OPP_FORM_82_T,"Gerhard Richter “New Overpainted Photographs“");
define(LANG_OPP_FORM_82_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_82_D,", février &#8211; mars 2010");
define(LANG_OPP_FORM_82_1_T,"Best of Fifty Years");
define(LANG_OPP_FORM_82_1_L,", Kunstverein Wolfsburg");
define(LANG_OPP_FORM_82_1_D,", novembre 2009 &#8211; février 2010");
define(LANG_OPP_FORM_83_T,"Photo España 2009: Gerhard Richter, Fotografías pintadas");
define(LANG_OPP_FORM_83_L,", Fundación Telefónica, Madrid");
define(LANG_OPP_FORM_83_D,", juin 2009");
define(LANG_OPP_FORM_84_T,"The Photographic Object");
define(LANG_OPP_FORM_84_L,", Photographers’ Gallery, London");
define(LANG_OPP_FORM_84_D,", avril &#8211; juin 2009");
define(LANG_OPP_FORM_85_T,"Gerhard Richter &#8211; Übermalte Fotografien &#8211; Photographies Peintes");
define(LANG_OPP_FORM_85_L,", Centre de la photographie, Geneva");
define(LANG_OPP_FORM_85_D,", février &#8211; mai 2009");
define(LANG_OPP_FORM_86_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_86_L,", Museum Morsbroich, Leverkusen");
define(LANG_OPP_FORM_86_D,", octobre 2008 &#8211; janvier 2009");
define(LANG_OPP_FORM_86_1_T,"Gerhard Richter “New Works“");
define(LANG_OPP_FORM_86_1_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_86_1_D,", novembre &#8211; décembre 2005");
define(LANG_OPP_FORM_87_T,"Gerhard Richter: Overpainted Photographs");
define(LANG_OPP_FORM_87_L,", Anthony Meier Fine Arts, San Francisco");
define(LANG_OPP_FORM_87_D,", juin &#8211; août 2005");
define(LANG_OPP_FORM_88_T,"Gerhard Richter &#8211; Editionen 1968 &#8211; 2004");
define(LANG_OPP_FORM_88_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_88_D,", juin &#8211; juillet 2005");
define(LANG_OPP_FORM_89_T,"Attack: Attraction Painting/Photography");
define(LANG_OPP_FORM_89_L,", Marcel Sitcoske Gallery, San Francisco");
define(LANG_OPP_FORM_89_D,", décembre 2002 &#8211; février 2003");
define(LANG_OPP_FORM_90_T,"Gerhard Richter");
define(LANG_OPP_FORM_90_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_90_D,", décembre 2002 &#8211; janvier 2003");
define(LANG_OPP_FORM_91_T,"Gerhard Richter &#8211; Firenze");
define(LANG_OPP_FORM_91_L,", Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_91_D,", juin &#8211; août 2002");
define(LANG_OPP_FORM_92_T,"Gerhard Richter &#8211; Editionen 1969 &#8211; 1998");
define(LANG_OPP_FORM_92_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_92_D,", mars &#8211; mai 2001");
define(LANG_OPP_FORM_93_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_93_L,", Galerie Fred Jahn, Munich");
define(LANG_OPP_FORM_93_D,", octobre 2000");
define(LANG_OPP_FORM_93_1_T,"Gerhard Richter &#8211; Bilder 1972 &#8211; 1996");
define(LANG_OPP_FORM_93_1_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_1_D,", mai &#8211; juillet 1998");
define(LANG_OPP_FORM_93_2_T,"Gerhard Richter");
define(LANG_OPP_FORM_93_2_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_2_D,", octobre &#8211; novembre 1997");
define(LANG_OPP_FORM_93_3_T,"Gerhard Richter Part II: Painting, Mirror Painting, Watercolour, Photograph, Print");
define(LANG_OPP_FORM_93_3_L,", Wako Works of Art, Tokyo");
define(LANG_OPP_FORM_93_3_D,", mai 1996");
define(LANG_OPP_FORM_93_4_T,"Gerhard Richter &#8211; Editionen 1967 &#8211; 1993");
define(LANG_OPP_FORM_93_4_L,", Galerie Bernd Lutze, Friedrichshafen");
define(LANG_OPP_FORM_93_4_D,", janvier &#8211; mars 1994");
define(LANG_OPP_FORM_93_5_T,"Gerhard Richter: Sils");
define(LANG_OPP_FORM_93_5_L,", Nietzsche-Haus, Sils-Maria");
define(LANG_OPP_FORM_93_5_D,", julliet 1992 &#8211; mars 1993");
define(LANG_OPP_FORM_94_T,"autres expositions");
define(LANG_OPP_FORM_94_L,"");
define(LANG_OPP_FORM_94_D,"");

# publications list
define(LANG_OPP_FORM_95_6_T,"Gerhard Richter &#8211; Painting");
define(LANG_OPP_FORM_95_6_L,". Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_95_6_D,", 2015");
define(LANG_OPP_FORM_95_5_T,"GERHARD RICHTER: Paintings and Drawings");
define(LANG_OPP_FORM_95_5_L," (Buchloh, Benjamin H.D., Schwarz, Dieter). Marian Goodman Gallery, New York");
define(LANG_OPP_FORM_95_5_D,", 2016");
define(LANG_OPP_FORM_95_4_T,"OVERPAINTING. Beard – Christo – Kiefer – Nabil – Neshat – Ontani – Rainer – Richter – Schifano");
define(LANG_OPP_FORM_95_4_L," (Barbero, Luca Massimo). Nuvole Rosse");
define(LANG_OPP_FORM_95_4_D,", 2015");
define(LANG_OPP_FORM_95_3_T,"Gerhard Richter");
define(LANG_OPP_FORM_95_3_L," (Buchloh, Benjamin H.D., Schwarz, Dieter, Storr, Robert). London: Marian Goodman Gallery");
define(LANG_OPP_FORM_95_3_D,", 2014");
define(LANG_OPP_FORM_95_2_T,"What is a Photograph");
define(LANG_OPP_FORM_95_2_L," (Squiers, Carol, Baker, George, Batchen, Geoffrey, Steyerl, Hito). New York: Prestel");
define(LANG_OPP_FORM_95_2_D,", 2014");
define(LANG_OPP_FORM_95_T,"Purer Zufall &#8211; Unvorhersehbares von Marcel Duchamp bis Gerhard Richter");
define(LANG_OPP_FORM_95_L," (Krempel, Ulrich, Rist, Annerose, Schwarz, Isabelle). Hanover: Sprengel Museum");
define(LANG_OPP_FORM_95_D,", 2013");
define(LANG_OPP_FORM_95_1_T,"Beirut");
define(LANG_OPP_FORM_95_1_L," (Borchardt-Hume, Achim, Joreige, Lamia, Dagner, Sandra). Beirut: Beirut Art Center; London: Heni Publishing; Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_95_1_D,", 2012");
define(LANG_OPP_FORM_96_T,"Best of Fifty Years");
define(LANG_OPP_FORM_96_L," (Justin Hoffmann/Kunstverein Wolfsburg). Wolfsburg: Kunstverein Wolfsburg");
define(LANG_OPP_FORM_96_D,", 2010");
define(LANG_OPP_FORM_97_T,"Gerhard Richter. Obrist &#8211; O’Brist");
define(LANG_OPP_FORM_97_L," (Obrist, Hans Ulrich). Cologne: Verlag der Buchhandlung Walther König");
define(LANG_OPP_FORM_97_D,", 2009");
define(LANG_OPP_FORM_97_1_T,"Gerhard Richter. Overpainted Photographs");
define(LANG_OPP_FORM_97_1_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_97_1_D,", 2008");
define(LANG_OPP_FORM_98_T,"Gerhard Richter. Übermalte Fotografien");
define(LANG_OPP_FORM_98_L," (Heinzelmann, Markus, Schneede, Uwe W., Strauss, Botho, Hustvedt, Siri). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_98_D,", 2008");
define(LANG_OPP_FORM_99_T,"Gerhard Richter.");
define(LANG_OPP_FORM_99_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_99_D,", 2005");
define(LANG_OPP_FORM_100_T,"City Life");
define(LANG_OPP_FORM_100_L," (Cora, Bruno, Restagno, Enzo, Gori, Giuliano). Prato: Gli Ori");
define(LANG_OPP_FORM_100_D,", 2002");
define(LANG_OPP_FORM_100_1_T,"Gerhard Richter.");
define(LANG_OPP_FORM_100_1_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_1_D,", 2002");
define(LANG_OPP_FORM_100_2_T,"Sils");
define(LANG_OPP_FORM_100_2_L," (eds. Obrist, Hans Ulrich, Bloch, Peter Andre). Munich: Oktagon, 1992; New York: D. A. P., Distributed Art Publishers");
define(LANG_OPP_FORM_100_2_D,", 2002 ");
define(LANG_OPP_FORM_100_3_T,"Florence");
define(LANG_OPP_FORM_100_3_L," (Elger, Dietmar). Ostfildern-Ruit: Hatje Cantz");
define(LANG_OPP_FORM_100_3_D,", 2001");
define(LANG_OPP_FORM_100_4_T,"Gerhard Richter, Part I: New Painting; Part II, Part II: Painting, Mirror Painting, Watercolour, Photograph, Print.");
define(LANG_OPP_FORM_100_4_L," Tokyo: Wako Works of Art");
define(LANG_OPP_FORM_100_4_D,", 1996");
define(LANG_OPP_FORM_100_5_T,"autres publications");
define(LANG_OPP_FORM_100_5_L,"");
define(LANG_OPP_FORM_100_5_D,"");

define(LANG_OPP_FORM_103,"J'aimerais recevoir une copie des données par e-mail");
define(LANG_OPP_FORM_104,"Numéro de référence");
# END OVERPAINTED PHOTOGRAPHS FORM

# TABLE
define(LANG_TABLE_DESC_PUBLICATION_RELATED,"related");
define(LANG_TABLE_DESC_PUBLICATION,"publication");
define(LANG_TABLE_DESC_PUBLICATIONS,"publications");
define(LANG_TABLE_TITLE,"Titre");
define(LANG_TABLE_FURTHER_ARTWORKS_IN_LOT,"Further artworks in this lot:");
# END TABLE

# Search
define(LANG_SEARCH_SHOW_MORE_OPTIONS,"Show more options");
# END Search

# share button
define(LANG_SHARE_THIS_PAGE,"Share this page");
define(LANG_SHARE_EMAIL_SUBJECT,"Visit this page on gerhard-richter.com");
define(LANG_SHARE_EMAIL_BODY_1,"Dear friend,");
define(LANG_SHARE_EMAIL_BODY_2,"I would like to share the following page with you:");
define(LANG_SHARE_EMAIL_BODY_3,"Kind regards,");
define(LANG_SHARE_EMAIL,"Email");
# END share button

# Mobile site specific
define(LANG_MOBILE_SEARCH_FIND,"Find");
# END Mobile site specific
?>
