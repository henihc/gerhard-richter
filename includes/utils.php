<?php
class UTILS {

        public static function redirect($url,$type="")
        {
            session_write_close();

            //$url=urlencode($url);

            switch( $type )
            {
                // 301 Moved Permanently
                case 301 : header("Location: ".$url,TRUE,$type);break;
                // 302 Found
                case 302 : header("Location: ".$url,TRUE,$type);
                           header("Location: ".$url);
                           break;
                // 303 See Other
                case 303 : header("Location: ".$url,TRUE,$type);break;
                // 307 Temporary Redirect
                case 307 : header("Location: ".$url,TRUE,$type);break;
                default :  header("Location: ".$url);
            }

            exit;
        }


		public static function not_found_404()
		{
		    header('HTTP/1.0 404 Not Found');
		    echo "<h1>404 Not Found</h1>";
		    echo "The page that you have requested could not be found.";
		    exit;
		}

		public static function load_lang_file($db,$options=array())
		{
			$return=array();

			//print_r($options);
			//var_dump(debug_backtrace());

			$languages=array();
			$languages[1]['title']="English";
			$languages[1]['title_display']="English";
			$languages[1]['lang']="en";
			$languages[1]['locale']="en_EN";
			$languages[1]['locale_facebook']="en_GB";
			$languages[1]['class']=strtolower($languages[1]['title']);
			$languages[2]['title']="German";
			$languages[2]['title_display']="Deutsch";
			$languages[2]['lang']="de";
			$languages[2]['locale']="de_DE";
			$languages[2]['locale_facebook']="de_DE";
			$languages[2]['class']=strtolower($languages[2]['title']);
			$languages[3]['title']="French";
			$languages[3]['title_display']="Fran&#231;ais";
			$languages[3]['lang']="fr";
			$languages[3]['locale']="fr_FR";
			$languages[3]['locale_facebook']="fr_FR";
			$languages[3]['class']=strtolower($languages[3]['title']);
			$languages[4]['title']="Italian";
			$languages[4]['title_display']="Italiano";
			$languages[4]['lang']="it";
			$languages[4]['locale']="it_IT";
			$languages[4]['locale_facebook']="it_IT";
			$languages[4]['class']=strtolower($languages[4]['title']);
			$languages[5]['title']="Chinese";
			$languages[5]['title_display']="中文";
			$languages[5]['lang']="zh";
			$languages[5]['locale']="zh_ZH";
			$languages[5]['locale_facebook']="zh_CN";
			$languages[5]['class']=strtolower($languages[5]['title']);
			$return['languages']=$languages;

			if( $options['get_lang']=="de" )
			{
			    $return['lang']='de';
			    $return['locale']='de_DE';
			    $return['locale_facebook']=$languages[2]['locale_facebook'];
			    $return['languageid']=2;
			    if( !$options['not_load_lang_file'] ) require_once($_SERVER["DOCUMENT_ROOT"]."/includes/languages/2-de-deutsch.php");
			}
			elseif( $options['get_lang']=="fr" )
			{
			    $return['lang']='fr';
			    $return['locale']='fr_FR';
			    $return['locale_facebook']=$languages[3]['locale_facebook'];
			    $return['languageid']=3;
			    if( !$options['not_load_lang_file'] ) require_once($_SERVER["DOCUMENT_ROOT"]."/includes/languages/3-fr-french.php");
			}
			elseif( $options['get_lang']=="it" )
			{
			    $return['lang']='it';
			    $return['locale']='it_IT';
			    $return['locale_facebook']=$languages[4]['locale_facebook'];
			    $return['languageid']=4;
			    if( !$options['not_load_lang_file'] ) require_once($_SERVER["DOCUMENT_ROOT"]."/includes/languages/4-it-italiano.php");
			}
			elseif( $options['get_lang']=="zh" )
			{
			    $return['lang']='zh';
			    $return['locale']='zh_ZH';
			    $return['locale_facebook']=$languages[5]['locale_facebook'];
			    $return['languageid']=5;
			    if( !$options['not_load_lang_file'] ) require_once($_SERVER["DOCUMENT_ROOT"]."/includes/languages/5-zh-chinese.php");
			}
			else
			{
			    $return['lang']='en';
			    $return['locale']='en_EN';
			    $return['locale_facebook']=$languages[1]['locale_facebook'];
			    $return['languageid']=1;
			    if( !$options['not_load_lang_file'] ) require_once($_SERVER["DOCUMENT_ROOT"]."/includes/languages/1-en-english.php");
			}

			if( !$options['not_load_lang_file'] )
			{
				$_GET['lang']=$return['lang'];
				$_GET['locale']=$return['locale'];
				$_GET['locale_facebook']=$return['locale_facebook'];
			}

			return $return;
		}

		public static function replace_lang_for_link($db,$link,$current_lang,$options=array())
		{
            $pattern = '/(\/en\/)|(\/en$)|(\/de\/)|(\/de$)|(\/fr\/)|(\/fr$)|(\/it\/)|(\/it$)|(\/zh\/)|(\/zh$)|(\/cn\/)|(\/cn$)/i';
            $replacement = "/".$current_lang."/";
            $link=preg_replace($pattern, $replacement, $link);

			return $link;
		}


		public static function beginsWith($str, $sub)
		{
    	return (strncmp($str, $sub, strlen($sub)) == 0);
		}

	public static function url_var($db,$url,$values=array())
	{
    //$_SERVER["QUERY_STRING"];
	$url = preg_replace("/[?]/","",$url);
	@$var_array=explode("[&]",$url);
	for($i=0;$i<sizeof($var_array);$i++)
	{
	    if( !$values['no_exclude'] ) $var_array[$i]=str_replace ("_", " ", $db->db_prepare_input($var_array[$i]));
	}
	if(empty($var_array[0]))$var_array=array();
	return $var_array;
	}


	#function to select categoryEN or categoryDE and the same with all other and titleEN...
	public static function select_lang($lang,$type)
	{
	$lang=strtoupper($lang);
	return $type.$lang;
	}


	public static function split_string($string,$mark)
	{
	@$var_array=explode("[".$mark."]",$string);
	return $var_array;
	}


	public static function r_string($len) {
              list($usec, $sec) = explode(' ', microtime());
              $seed =  (float) $sec + ((float) $usec * 100000);
              srand($seed);
              $newstring = md5(rand());
              if ($len) return substr($newstring,0,$len);
	      else return $newstring;
	}


	public static function get_artwork_image_dir($db,$artwork)
	{
		$query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='".$artwork."' ");
		$row=$db->mysql_array($query);
		$artwork_image_dir=$row['artwork_dir_name'];

		return $artwork_image_dir;
	}


	public static function get_artwork_dir_admin($db,$artwork)
	{
		$query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='$artwork' ");
		$row=$db->mysql_array($query);
		$artwork_image_dir=$row['artwork_dir_name_admin'];

		return $artwork_image_dir;
	}

	public static function get_artwork_name($db,$artworkID)
	{
	    $query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='".$artworkID."' ");
	    $row=$db->mysql_array($query);
        $row=UTILS::html_decode($row);
        $artwork=UTILS::row_text_value2($db,$row,"artwork");
	    return $artwork;
	}

	public static function get_artwork_data($db,$options)
	{
		$return=array();

		if( !empty($options['artworkid']) )
		{
	    	$query=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='".$options['artworkid']."' ");
	    	$row=$db->mysql_array($query);
        	$row=UTILS::html_decode($row);
        	$artwork=UTILS::row_text_value2($db,$row,"artwork");
        	$return['title']=$artwork;
        	$desc=UTILS::row_text_value($db,$row,"desc");
        	$return['desc']=$desc;
        	$return['row']=$row;
        }
		elseif( !empty($options['titleurl']) )
		{
			$query_artwork="SELECT * FROM ".TABLE_ARTWORKS." ";
			//if( $options['titleurl']=="other" && $_GET['section_1']!="paintings" ) $query_artwork.=" WHERE artworkID=15 ";
			//else $query_artwork.=" WHERE titleurl='".$options['titleurl']."'";
			$query_artwork.=" WHERE titleurl='".$options['titleurl']."'";
	    	$query=$db->query($query_artwork);
	    	$row=$db->mysql_array($query);
        	$row=UTILS::html_decode($row);
        	$artwork=UTILS::row_text_value2($db,$row,"artwork");
        	$return['title']=$artwork;
        	$desc=UTILS::row_text_value($db,$row,"desc");
        	$return['desc']=$desc;
        	$return['row']=$row;
		}

	    return $return;
	}

	public static function get_category_data($db,$options)
	{
		$return=array();

		if( !empty($options['categoryid']) || !empty($options['section']) )
		{
			$query="SELECT * FROM ".TABLE_CATEGORY." WHERE ";
			if( !empty($options['categoryid']) ) $query.=" catID='".$options['categoryid']."' ";
			else $query.=" titleurl='".$options['section']."' ";
	    	$result=$db->query($query);
	    	$row=$db->mysql_array($result);
        	$row=UTILS::html_decode($row);
        	$category=UTILS::row_text_value2($db,$row,"category");
        	$return['title']=$category;
        	$return['row']=$row;
        	$desc=UTILS::row_text_value2($db,$row,"description");
        	$return['desc']=$desc;
        	$count=$db->numrows($result);
        	$return['count']=$count;
        }

	    return $return;
	}

    public static function get_currency($db,$id,$value="symbol")
    {
	    $result=$db->query("select symbol,currency from ".TABLE_CURRENCY." where currID='$id'");
	    $row=$db->mysql_array($result);
        if( $value=="symbol" ) return $row['symbol'];
        else if( $value=="currency" ) return $row['currency'];
    }

    public static function display_numbers($db,$number)
    {
        if( $_GET['lang']=="de" )
        {
            $number=number_format($number,0,',','.');
        }
        elseif( $_GET['lang']=="fr" )
        {
            $number=number_format($number,0,',',' ');
        }
        elseif( $_GET['lang']=="it" )
        {
            $number=number_format($number,0,',','.');
        }
        else
        {
            $number=number_format($number,0);
        }

        return $number;
    }

/** FOLDER FILE FUNCTIONS **/

public static function get_mime($file) {
  if (function_exists("finfo_file")) {
    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
    $mime = finfo_file($finfo, $file);
    finfo_close($finfo);
    return $mime;
  } else if (function_exists("mime_content_type")) {
    return mime_content_type($file);
  } else if (!stristr(ini_get("disable_functions"), "shell_exec")) {
    // http://stackoverflow.com/a/134930/1593459
    $file = escapeshellarg($file);
    $mime = shell_exec("file -bi " . $file);
    return $mime;
  } else {
    return false;
  }
}


	public static function fileTree($dir)
	{
	  $d = dir($dir);
	  $arFil=array();
	  while (false !== ($entry = $d->read())) {
	  	if($entry != '.' && $entry != '..' && substr($entry,0,1)!='.' ){
	  	$arFil[] = $entry;
	  	}
	  }
	  $d->close();
	  return $arFil;
	}



	public static function strip_ext($name)
	{
	    $ext = preg_replace("/[.]/","",strrchr($name, '.'));
	    $name_new=explode($ext,$name);
	    $tmp[]=preg_replace("/[.]/","",$name_new[0]);
	    $tmp[]=$ext;
	    return $tmp;//returns array[0]=file name array[1]=file extension
	}

	public static function file_type($path)
	{
	$dir = opendir($path);//Directory where catalogues is stored
		while($file = readdir($dir))//Readin all filse in array
		{
			if ($file != "." && $file != "..") {
			$ext=UTILS::strip_ext($file);
			if($ext==".jpg"||$ext==".jpeg"||$ext==".jpe"||$ext==".gif"||$ext==".png"||$ext==".tiff"||$ext==".tif"||$ext==".bmp"||$ext==".bitmap"){$filename=$file;}
			}
		}
	return $filename;

	}



/** END FOLDER FILE FUNCTIONS **/








/************************************************ IMAGE FUNCTIONS **/

	public static function resize_image($tempFile,$clientFile,$thumb_height,$thumb_width,$destFile,$quality=100)
	{
		//$section - atlas,books,edition_paintings,exhibitions,paintings
        $getimagesize = getimagesize($tempFile);

        switch( $getimagesize['mime'] )
        {
            case 'image/gif' : $im = imagecreatefromgif($tempFile);break;
            case 'image/png' : $im = imagecreatefrompng($tempFile);break;
            case 'image/jpeg' : $im = imagecreatefromjpeg($tempFile);break;
            case 'image/bmp' : $im = UTILS::imagecreatefrombmp($tempFile);break;
            default : exit($tempFile." - Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");$im=false;
        }

	    $width = imagesx($im);//width
	    $height = imagesy($im);//height

//resize
		  $max_width=$thumb_width;
		  $max_height=$thumb_height;

          $x_ratio = $max_width / $width;
          $y_ratio = $max_height / $height;

          if( ($width <= $max_width) && ($height <= $max_height) )
          {
               $tn_width = $width;
               $tn_height = $height;
          }
          elseif (($x_ratio * $height) < $max_height)
          {
               $tn_height = ceil($x_ratio * $height);
               $tn_width = $max_width;
          }
          else
          {
               $tn_width = ceil($y_ratio * $width);
               $tn_height = $max_height;
          }
//end

	        $nm = imagecreatetruecolor($tn_width, $tn_height);
	        imagecopyresampled($nm, $im, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
	        imagejpeg($nm, $destFile, $quality);

	}

/** *****************  converting bmp to gd format *************************  **/

	public static function ConvertBMP2GD($src, $dest = false) {
	if(!($src_f = fopen($src, "rb"))) {
	return false;
	}
	if(!($dest_f = fopen($dest, "wb"))) {
	return false;
	}
	$header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f,
	14));
	$info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",
	fread($src_f, 40));

	extract($info);
	extract($header);

	if($type != 0x4D42) { // signature "BM"
	return false;
	}

	$palette_size = $offset - 54;
	$ncolor = $palette_size / 4;
	$gd_header = "";
	// true-color vs. palette
	$gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF";
	$gd_header .= pack("n2", $width, $height);
	$gd_header .= ($palette_size == 0) ? "\x01" : "\x00";
	if($palette_size) {
	$gd_header .= pack("n", $ncolor);
	}
	// no transparency
	$gd_header .= "\xFF\xFF\xFF\xFF";

	fwrite($dest_f, $gd_header);

	if($palette_size) {
	$palette = fread($src_f, $palette_size);
	$gd_palette = "";
	$j = 0;
	while($j < $palette_size) {
	$b = $palette{$j++};
	$g = $palette{$j++};
	$r = $palette{$j++};
	$a = $palette{$j++};
	$gd_palette .= "$r$g$b$a";
	}
	$gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);
	fwrite($dest_f, $gd_palette);
	}

	$scan_line_size = (($bits * $width) + 7) >> 3;
	$scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &
	0x03) : 0;

	for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
	// BMP stores scan lines starting from bottom
	fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *
	$l));
	$scan_line = fread($src_f, $scan_line_size);
	if($bits == 24) {
	$gd_scan_line = "";
	$j = 0;
	while($j < $scan_line_size) {
	$b = $scan_line{$j++};
	$g = $scan_line{$j++};
	$r = $scan_line{$j++};
	$gd_scan_line .= "\x00$r$g$b";
	}
	}
	else if($bits == 8) {
	$gd_scan_line = $scan_line;
	}
	else if($bits == 4) {
	$gd_scan_line = "";
	$j = 0;
	while($j < $scan_line_size) {
	$byte = ord($scan_line{$j++});
	$p1 = chr($byte >> 4);
	$p2 = chr($byte & 0x0F);
	$gd_scan_line .= "$p1$p2";
	} $gd_scan_line = substr($gd_scan_line, 0, $width);
	}
	else if($bits == 1) {
	$gd_scan_line = "";
	$j = 0;
	while($j < $scan_line_size) {
	$byte = ord($scan_line{$j++});
	$p1 = chr((int) (($byte & 0x80) != 0));
	$p2 = chr((int) (($byte & 0x40) != 0));
	$p3 = chr((int) (($byte & 0x20) != 0));
	$p4 = chr((int) (($byte & 0x10) != 0));
	$p5 = chr((int) (($byte & 0x08) != 0));
	$p6 = chr((int) (($byte & 0x04) != 0));
	$p7 = chr((int) (($byte & 0x02) != 0));
	$p8 = chr((int) (($byte & 0x01) != 0));
	$gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";
	} $gd_scan_line = substr($gd_scan_line, 0, $width);
	}

	fwrite($dest_f, $gd_scan_line);
	}
	fclose($src_f);
	fclose($dest_f);
	return true;
	}

	public static function imagecreatefrombmp($filename) {
	$tmp_name = tempnam("/tmp", "GD");
	if(UTILS::ConvertBMP2GD($filename, $tmp_name)) {
	$img = imagecreatefromgd($tmp_name);
	unlink($tmp_name);

	return $img;
	} return false;
	}

/** ***************** END - converting bmp to gd format *************************  **/




public static function color_hex2dec ($color)
{
return array (hexdec (substr ($color, 0, 2)), hexdec (substr ($color, 2, 2)), hexdec (substr ($color, 4, 2)));
}




public static function LoadJpeg($imgname,$bgcolor,$maxwidth,$maxheight,$border)
{

$getimagesize = getimagesize($imgname);

	switch( $getimagesize['mime'] )
	{
	case 'image/gif' : $imgSrc = imagecreatefromgif($imgname);break;
	case 'image/png' : $imgSrc = imagecreatefrompng($imgname);break;
	case 'image/jpeg' : $imgSrc = imagecreatefromjpeg($imgname);break;
    case 'image/bmp' : $imgSrc = UTILS::imagecreatefrombmp($imgname);break;
    default : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
	}

//Do it
$srcWidth=imagesx($imgSrc);
$srcHeight=imagesy($imgSrc);
//Make thumbnails



	if( !$border )
	{
	        /* resize image */
	        if($srcHeight > $srcWidth)
	        { // image height is longer
	            $dh = $maxheight;// thumbnail max height for resize proportionately if height is longer than width
	            $dw = floor($srcWidth * ($maxheight/$srcHeight));
	        }
	        else
	        { // image width is wider or height = width (square)
	            $dw = $maxwidth;//thumbnail max width for resize proportionately if width is wider than height
	            $dh = floor( $srcHeight * ($maxwidth/$srcWidth));
	        }//end if-else
	        $imgOut = imagecreatetruecolor($dw, $dh);
	        //imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $srcWidth, $srcHeight);
	}
	else
	{
	    $imgOut=imagecreatetruecolor($maxwidth,$maxheight);
        /*
        list ($r, $g, $b) = UTILS::color_hex2dec ($bgcolor);
	    imagefill($imgOut,0,0,imagecolorallocate($imgOut,$r,$g,$b));
         */
        $black = imagecolorallocate($imgOut, 0, 0, 0);
        imagecolortransparent($imgOut, $black);


	    //Copy them proportionatly
	    $dx=0; $dy=0; $dw=$maxwidth; $dh=$maxheight;
		if ($maxwidth*$srcHeight!=$maxheight*$srcWidth)
		{ //Non-proportional, cut-off
		    //Which dimensions is larger
		    if ($srcWidth>$srcHeight)
		    { //Empty space on top and bottom
		        $dw=$maxwidth;
		        $dh=($dw*$srcHeight)/$srcWidth;
		        $dy=($maxheight-$dh)/2;
		    }
		    else
		    {  //Empty space on left and right
		        $dh=$maxheight;
		        $dw=($dh*$srcWidth)/$srcHeight;
		        $dx=($maxwidth-$dw)/2;
		    }
		}
	}

if( $border ) {$dx=$dx;$dy=$dy;}
else {$dx=0;$dy=0;}

imagecopyresampled($imgOut,$imgSrc,$dx ,$dy  ,0,0,$dw,$dh,$srcWidth,$srcHeight);

//Create the thumbnail and destroy everything else
return $imgOut;
}



/** ***************** END - image functions *************************  **/

/** ***************** date time function ****************************  **/


	public static function unixtimestamps($datetime)
	{
	// format MySQL DateTime (YYYY-MM-DD hh:mm:ss) using date()
	$year = substr($datetime,0,4);
	$month = substr($datetime,5,2);
	$day = substr($datetime,8,2);
	$hour = substr($datetime,11,2);
	$min = substr($datetime,14,2);
	$sec = substr($datetime,17,2);

	return mktime($hour,$min,$sec,$month,$day,$year);
	}


  /**
   mod of
   http://www.php.net/manual/en/function.date.php#71397
   * Converts a date and time string from one format to another (e.g. d/m/Y => Y-m-d, d.m.Y => Y/d/m, ...)
   *
   * @param string $date_format1
   * @param string $date_format2
   * @param string $date_str
   * @return string
  */
  public static function dates_interconv($date_format1, $date_format2, $date_str)
  {

      $base_struc     = explode('[:/.\ \-]', $date_format1);
      $date_str_parts = explode('[:/.\ \-]', $date_str );

      // print_r( $base_struc ); echo "\n"; // for testing
      // print_r( $date_str_parts ); echo "\n"; // for testing

      $date_elements = array();

      $p_keys = array_keys( $base_struc );
      foreach ( $p_keys as $p_key )
      {
          if ( !empty( $date_str_parts[$p_key] ))
          {
              $date_elements[$base_struc[$p_key]] = $date_str_parts[$p_key];
          }
          else
              return false;
      }

      // print_r($date_elements); // for testing

      if (array_key_exists('M', $date_elements)) {
        $Mtom=array(
          "Jan"=>"01",
          "Feb"=>"02",
          "Mar"=>"03",
          "Apr"=>"04",
          "May"=>"05",
          "Jun"=>"06",
          "Jul"=>"07",
          "Aug"=>"08",
          "Sep"=>"09",
          "Oct"=>"10",
          "Nov"=>"11",
          "Dec"=>"12",
        );
        $date_elements['m']=$Mtom[$date_elements['M']];
      }

      // print_r($date_elements); // for testing

      $dummy_ts = mktime(
        $date_elements['H'],
        $date_elements['i'],
        $date_elements['s'],
        $date_elements['m'],
        $date_elements['d'],
        $date_elements['Y']
      );

      return date( $date_format2, $dummy_ts );
  }


public static function date_month($db,$date,$short=0)
{
    if( $_GET['lang']=="de" )
    {
        if( $short )
        {
            //$date=str_replace("Jan","Januar", $date);
            //$date=str_replace("Feb","Februar", $date);
            $date=str_replace("Mar","M&#228;r", $date);
            //$date=str_replace("Apr","April", $date);
            $date=str_replace("May","Mai", $date);
            //$date=str_replace("Jun","Juni", $date);
            //$date=str_replace("Jul","Juli", $date);
            //$date=str_replace("Aug","August", $date);
            //$date=str_replace("Sep","September", $date);
            $date=str_replace("Oct","Okt", $date);
            //$date=str_replace("Nov","Nov", $date);
            $date=str_replace("Dec","Dez", $date);
        }
        else
        {
            $date=str_replace("January","Januar", $date);
            $date=str_replace("February","Februar", $date);
            $date=str_replace("March","M&#228;rz", $date);
            //$date=str_replace("April","April", $date);
            $date=str_replace("May","Mai", $date);
            $date=str_replace("June","Juni", $date);
            $date=str_replace("July","Juli", $date);
            //$date=str_replace("August","August", $date);
            //$date=str_replace("September","September", $date);
            $date=str_replace("October","Oktober", $date);
            //$date=str_replace("November","November", $date);
            $date=str_replace("December","Dezember", $date);
        }
    }
    elseif( $_GET['lang']=="fr" )
    {
        if( $short )
        {
            //$date=str_replace("Jan","Janvier", $date);
            $date=str_replace("Feb","Fév", $date);
            //$date=str_replace("Mar","Mar", $date);
            $date=str_replace("Apr","Avr", $date);
            $date=str_replace("May","Mai", $date);
            $date=str_replace("Jun","Juin", $date);
            $date=str_replace("Jul","Juil", $date);
            $date=str_replace("Aug","Aoû", $date);
            //$date=str_replace("Sep","Sep", $date);
            //$date=str_replace("Oct","Oct", $date);
            //$date=str_replace("Nov","Nov", $date);
            $date=str_replace("Dec","Déc", $date);
        }
        else
        {
            $date=str_replace("January","Janvier", $date);
            $date=str_replace("February","Février", $date);
            $date=str_replace("March","Mars", $date);
            $date=str_replace("April","Avril", $date);
            $date=str_replace("May","Mai", $date);
            $date=str_replace("June","Juin", $date);
            $date=str_replace("July","Juillet", $date);
            $date=str_replace("August","Août", $date);
            $date=str_replace("September","Septembre", $date);
            $date=str_replace("October","Octobre", $date);
            $date=str_replace("November","Novembre", $date);
            $date=str_replace("December","Décembre", $date);
        }
    }
    elseif( $_GET['lang']=="it" )
    {
        if( $short )
        {
            $date=str_replace("Jan","Gen", $date);
            //$date=str_replace("Feb","Feb", $date);
            //$date=str_replace("Mar","Mar", $date);
            //$date=str_replace("Apr","Apr", $date);
            $date=str_replace("May","Mag", $date);
            $date=str_replace("Jun","Giu", $date);
            $date=str_replace("Jul","Lug", $date);
            $date=str_replace("Aug","Ago", $date);
            $date=str_replace("Sep","Set", $date);
            $date=str_replace("Oct","Ott", $date);
            //$date=str_replace("Nov","Nov", $date);
            $date=str_replace("Dec","Dic", $date);
        }
        else
        {
            $date=str_replace("January","Gennaio", $date);
            $date=str_replace("February","Febbraio", $date);
            $date=str_replace("March","Marzo", $date);
            $date=str_replace("April","Aprile", $date);
            $date=str_replace("May","Maggio", $date);
            $date=str_replace("June","Giugno", $date);
            $date=str_replace("July","Luglio", $date);
            $date=str_replace("August","Agosto", $date);
            $date=str_replace("September","Settembre", $date);
            $date=str_replace("October","Ottobre", $date);
            $date=str_replace("November","Novembre", $date);
            $date=str_replace("December","Dicembre", $date);
        }
    }
    /*
    elseif( $_GET['lang']=="ch" )
    {
        if( $short )
        {
            //
        }
        else
        {
            $date=str_replace("January","一月", $date);
            $date=str_replace("February","二月", $date);
            $date=str_replace("March","三月", $date);
            $date=str_replace("April","四月", $date);
            $date=str_replace("May","五月", $date);
            $date=str_replace("June","六月", $date);
            $date=str_replace("July","七月", $date);
            $date=str_replace("August","八月", $date);
            $date=str_replace("September","九月", $date);
            $date=str_replace("October","十月", $date);
            $date=str_replace("November","十一月", $date);
            $date=str_replace("December","十二月", $date);
        }
    }
    */

    return $date;
}



/** ***************** date time function END ****************************  **/





        public static function crypt_password($plain_pass)
        {
        /* create a semi random salt */
        mt_srand ((double) microtime() * 1000000);
        for($i=0;$i<10;$i++){
        $tstring    .= mt_rand();
        }

        $salt = substr(md5($tstring),0, 2);

        $passtring = $salt . $plain_pass;

        $encrypted = md5($passtring);

        return($encrypted . ":" . $salt);
        } // function crypt_password($plain_pass)



    public static function get_ip(){
        $ip = (getenv(HTTP_X_FORWARDED_FOR))
        ?  getenv(HTTP_X_FORWARDED_FOR)
        :  getenv(REMOTE_ADDR);
    return($ip);
    }




            public static function validate_password($plain_pass, $db_pass)
            {

            /*Quick test to let this work on unencrypted passwords and NULL
            Passwords*/
            if($plain_pass == $db_pass){
                //return(true);
            }

            /* split apart the hash / salt*/
            if(!($subbits = explode(":", $db_pass, 2))){
                return(false);
            }

            if( empty($plain_pass) ){
                return(false);
            }

            $dbpassword = $subbits[0];
            $salt = $subbits[1];

            $passtring = $salt . $plain_pass;

            $encrypted = md5($passtring);

                if(strcmp($dbpassword, $encrypted) == 0)
                {
                    return(true);
                }
                else
                {
                    return(false);
                }
            } // function validate_password($plain_pass, $db_pass)





    public static function size_hum_read($size,$round,$ceil)
    {
    $size=filesize($size);

    $i=0;
    $iec = array("b", "kb", "mb", "gb", "TB", "PB", "EB", "ZB", "YB");
    while (($size/1024)>1) {
    $size=$size/1024;
    $i++;
    }
    if($round==true)$size=round(substr($size,0,strpos($size,'.')+4));
    if($ceil==true)$size=ceil(substr($size,0,strpos($size,'.')+4));
    else $size=substr($size,0,strpos($size,'.')+4);
    return $size.$iec[$i];
    }


/* function to use to genereate search string and to generate htaccess urls */

public static function toAscii($str, $delimiter=' ') {
    setlocale(LC_ALL, 'en_US.UTF8');
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

   public static function my_str_split($string)
   {
      $slen=strlen($string);
      for($i=0; $i<$slen; $i++)
      {
         $sArray[$i]=$string{$i};
      }
      return $sArray;
   }

   public static function noDiacritics($string)
   {
      //cyrylic transcription
      $cyrylicFrom = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
      $cyrylicTo   = array('A', 'B', 'W', 'G', 'D', 'Ie', 'Io', 'Z', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'Ch', 'C', 'Tch', 'Sh', 'Shtch', '', 'Y', '', 'E', 'Iu', 'Ia', 'a', 'b', 'w', 'g', 'd', 'ie', 'io', 'z', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'ch', 'c', 'tch', 'sh', 'shtch', '', 'y', '', 'e', 'iu', 'ia');


      $from = array("Á", "À", "Â", "Ä", "Ă", "Ā", "Ã", "Å", "Ą", "Æ", "Ć", "Ċ", "Ĉ", "Č", "Ç", "Ď", "Đ", "Ð", "É", "È", "Ė", "Ê", "Ë", "Ě", "Ē", "Ę", "Ə", "Ġ", "Ĝ", "Ğ", "Ģ", "á", "à", "â", "ä", "ă", "ā", "ã", "å", "ą", "æ", "ć", "ċ", "ĉ", "č", "ç", "ď", "đ", "ð", "é", "è", "ė", "ê", "ë", "ě", "ē", "ę", "ə", "ġ", "ĝ", "ğ", "ģ", "Ĥ", "Ħ", "I", "Í", "Ì", "İ", "Î", "Ï", "Ī", "Į", "Ĳ", "Ĵ", "Ķ", "Ļ", "Ł", "Ń", "Ň", "Ñ", "Ņ", "Ó", "Ò", "Ô", "Ö", "Õ", "Ő", "Ø", "Ơ", "Œ", "ĥ", "ħ", "ı", "í", "ì", "i", "î", "ï", "ī", "į", "ĳ", "ĵ", "ķ", "ļ", "ł", "ń", "ň", "ñ", "ņ", "ó", "ò", "ô", "ö", "õ", "ő", "ø", "ơ", "œ", "Ŕ", "Ř", "Ś", "Ŝ", "Š", "Ş", "Ť", "Ţ", "Þ", "Ú", "Ù", "Û", "Ü", "Ŭ", "Ū", "Ů", "Ų", "Ű", "Ư", "Ŵ", "Ý", "Ŷ", "Ÿ", "Ź", "Ż", "Ž", "ŕ", "ř", "ś", "ŝ", "š", "ş", "ß", "ť", "ţ", "þ", "ú", "ù", "û", "ü", "ŭ", "ū", "ů", "ų", "ű", "ư", "ŵ", "ý", "ŷ", "ÿ", "ź", "ż", "ž");
      $to   = array("A", "A", "A", "A", "A", "A", "A", "A", "A", "AE", "C", "C", "C", "C", "C", "D", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E", "G", "G", "G", "G", "G", "a", "a", "a", "a", "a", "a", "a", "a", "a", "ae", "c", "c", "c", "c", "c", "d", "d", "d", "e", "e", "e", "e", "e", "e", "e", "e", "g", "g", "g", "g", "g", "H", "H", "I", "I", "I", "I", "I", "I", "I", "I", "IJ", "J", "K", "L", "L", "N", "N", "N", "N", "O", "O", "O", "O", "O", "O", "O", "O", "CE", "h", "h", "i", "i", "i", "i", "i", "i", "i", "i", "ij", "j", "k", "l", "l", "n", "n", "n", "n", "o", "o", "o", "o", "o", "o", "o", "o", "o", "R", "R", "S", "S", "S", "S", "T", "T", "T", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "W", "Y", "Y", "Y", "Z", "Z", "Z", "r", "r", "s", "s", "s", "s", "B", "t", "t", "b", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y", "y", "y", "z", "z", "z");

      $search_german=array("ä", "à", "ã", "â", "á", "ç", "é", "ö", "œ", "ü", "ß", "Ä", "À", "Ã", "Â", "Á", "Ç", "É", "Ö", "Ü");
      $replace_german=array("ae","a", "a", "a", "a", "c", "e", "oe", "oe", "ue", "ss", "ae","a", "a", "a", "a", "c", "e", "oe","ue");

      $from = array_merge($from, $cyrylicFrom);
      $to   = array_merge($to, $cyrylicTo);
      $newstring=str_replace($from, $to, $string);

      $from_german = array_merge($search_german);
      $to_german   = array_merge($replace_german);
      $newstring_german=str_replace($from_german, $to_german, $string);
      $newstring_german=str_replace($from, $to, $newstring_german);

      $newstrings=array();
      $newstrings[0]=$newstring;
      $newstrings[1]=$newstring_german;

      return $newstrings;
   }

    public static function replace_accented_letters($db, $values)
    {
        //$values['text']=html_entity_decode ( html_entity_decode ($values['text']));
        if( empty($values['delimiter']) ) $values['delimiter']=" ";
        $delimiter=$values['delimiter'];
        $string=$values['text'];
        if( empty($values['maxlen']) ) $values['maxlen']=0;
        $maxlen=$values['maxlen'];

        $replaced_strings=UTILS::noDiacritics($string);

        foreach( $replaced_strings as $key => $string )
        {
            $newStringTab=array();
            $string=strtolower($string);
            if(function_exists('str_split'))
            {
               $stringTab=str_split($string);
            }
            else
            {
               $stringTab=UTILS::my_str_split($string);
            }

            $numbers=array("0","1","2","3","4","5","6","7","8","9","-","/");
            //$numbers=array("0","1","2","3","4","5","6","7","8","9");

            foreach($stringTab as $letter)
            {
               if(in_array($letter, range("a", "z")) || in_array($letter, $numbers))
               {
                  $newStringTab[]=$letter;
                  //print($letter);
               }
               elseif($letter==" ")
               {
                  $newStringTab[]=$delimiter;
               }
            }

            if(count($newStringTab))
            {
               $newStrings[$key]=implode($newStringTab);
               if($maxlen>0)
               {
                  $newStrings[$key]=substr($newStrings[$key], 0, $maxlen);
               }

               $newStrings[$key] = UTILS::removeDuplicates('--', $delimiter, $newStrings[$key]);
            }
            else
            {
               $newStrings[$key]='';
            }
        }

        $return=array();
        $newStrings[0]=UTILS::toAscii($newStrings[0], $values['delimiter']);
        $newStrings[1]=UTILS::toAscii($newStrings[1], $values['delimiter']);
        $return['text_lower_converted']=$newStrings[0];
        $return['text_lower_german_converted']=$newStrings[1];

        //print_r($return);

        return $return;
   }


   public static function checkSlug($sSlug)
   {
      if(ereg ("^[a-zA-Z0-9]+[a-zA-Z0-9\_\-]*$", $sSlug))
      {
         return true;
      }

      return false;
   }

   public static function removeDuplicates($sSearch, $sReplace, $sSubject)
   {
      $i=0;
      do{

         $sSubject=str_replace($sSearch, $sReplace, $sSubject);
         $pos=strpos($sSubject, $sSearch);

         $i++;
         if($i>100)
         {
            die('removeDuplicates() loop error');
         }

      }while($pos!==false);

      return $sSubject;
   }



#prepeare german title for serch field

    public static function replace_accented_letters_search($db, $values)
    {
        $values['text']=UTILS::strip_slashes_recursive($values['text']);
        $values['text']=strip_tags($values['text']);
        $values['text']=html_entity_decode($values['text'],ENT_QUOTES);
        $values['text']=trim($values['text']);

        ### replace differenc characters that we do not need for search values
        //$search_characters=array( "!","?","[","]","^","/","(",")","~","`"," & ",' - ','"',"'s","'",",",".",":",";");
        //$replace_characters=array("" ,"" ,"" ,"" ,"" ," ","" ,"" ,"" ,"" , ""  , " " ,"" ,""  ,"" ," "," "," ","");
        //$values['text']=str_replace($search_characters, $replace_characters, $values['text']);
        # END

        ### upper case letters
        $search_upper=array("À", "Á", "Â", "Ã", "Ä", "Å", "Ā", "Ą", "Ă", "Æ", "Ç", "Ć", "Č", "Ĉ", "Ċ", "Ď", "Đ", "È", "É", "Ê", "Ë", "Ē", "Ę", "Ě", "Ĕ", "Ė", "Ĝ", "Ğ", "Ġ", "Ģ", "Ĥ", "Ħ", "Ì", "Í", "Î", "Ï", "Ī", "Ĩ", "Ĭ", "Į", "İ", "Ĳ", "Ĵ", "Ķ", "Ł", "Ľ", "Ĺ", "Ļ", "Ŀ", "Ñ", "Ń", "Ň", "Ņ", "Ŋ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ō", "Ő", "Ŏ", "Œ", "Ŕ", "Ř", "Ŗ", "Ś", "Š", "Ş", "Ŝ", "Ș", "Ť", "Ţ", "Ŧ", "Ț", "Ù", "Ú", "Û", "Ü", "Ū", "Ů", "Ű", "Ŭ", "Ũ", "Ų", "Ŵ", "Ý", "Ŷ", "Ÿ", "Ź", "Ž", "Ż");
        //$replace_upper=array("A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "C", "C", "C", "C", "C", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E", "E", "G", "G", "G", "G", "H", "H", "I", "I", "I", "I", "I", "I", "I", "I", "I", "I", "J", "K", "L", "L", "L", "L", "L", "N", "N", "N", "N", "N", "O", "O", "O", "O", "O", "O", "O", "O", "O", "OE", "R", "R", "R", "S", "S", "S", "S", "S", "T", "T", "T", "T", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "W", "Y", "Y", "Y", "Z", "Z", "Z");
        //$replace_upper_to_small=array("à", "á", "â", "ã", "ä", "å", "ā", "ą", "ă", "æ", "ç", "ć", "č", "ĉ", "ċ", "ď", "đ", "è", "é", "ê", "ë", "ē", "ę", "ě", "ĕ", "ė", "ĝ", "ğ", "ġ", "ģ", "ĥ", "ħ", "ì", "í", "î", "ï", "ī", "ĩ", "ĭ", "į", "ı", "ĳ", "ĵ", "ķ", "ł", "ľ", "ĺ", "ļ", "ŀ", "ñ", "ń", "ň", "ņ", "ŋ", "ò", "ó", "ô", "õ", "ö", "ø", "ō", "ő", "ŏ", "œ", "ŕ", "ř", "ŗ", "ś", "š", "ş", "ŝ", "ș", "ť", "ţ", "ŧ", "ț", "ù", "ú", "û", "ü", "ū", "ů", "ű", "ŭ", "ũ", "ų", "ŵ", "ý", "ŷ", "ÿ", "ź", "ž", "ż");
        $replace_upper_to_small=array("a", "a", "a", "a", "a", "a", "a", "a", "a", "ae", "c", "c", "c", "c", "c", "d", "d", "e", "e", "e", "e", "e", "e", "e", "e", "e", "g", "g", "g", "g", "a", "a", "i", "i", "i", "i", "i", "i", "i", "i", "i", "ij", "j", "k", "l", "l", "l", "l", "l", "n", "n", "n", "n", "n", "o", "o", "o", "o", "o", "o", "o", "o", "o", "oe", "r", "r", "r", "s", "s", "s", "s", "s", "t", "t", "t", "t", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y", "y", "y", "z", "z", "z");
        $text_upper_to_lower_converted=str_replace($search_upper, $replace_upper_to_small, $values['text']);
        $text_upper_to_lower_converted=$db->db_prepare_input($text_upper_to_lower_converted);
        # END upper case letters

        ### lower case letters
        $search_lower=array("à", "á", "â", "ã", "ä", "å", "ā", "ą", "ă", "æ", "ç", "ć", "č", "ĉ", "ċ", "ď", "đ", "è", "é", "ê", "ë", "ē", "ę", "ě", "ĕ", "ė", "ƒ", "ĝ", "ğ", "ġ", "ģ", "ĥ", "ħ", "ì", "í", "î", "ï", "ī", "ĩ", "ĭ", "į", "ı", "ĳ", "ĵ", "ķ", "ĸ", "ł", "ľ", "ĺ", "ļ", "ŀ", "ñ", "ń", "ň", "ņ", "ŉ", "ŋ", "ò", "ó", "ô", "õ", "ö", "ø", "ō", "ő", "ŏ", "œ", "ŕ", "ř", "ŗ", "ś", "š", "ş", "ŝ", "ș", "ť", "ţ", "ŧ", "ț", "ù", "ú", "û", "ü", "ū", "ů", "ű", "ŭ", "ũ", "ų", "ŵ", "ý", "ÿ", "ŷ", "ž", "ż", "ź", "Þ", "þ", "ß", "ſ", "Ð", "ð");
        $replace_lower=array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "c", "c", "c", "c", "c", "d", "d", "e", "e", "e", "e", "e", "e", "e", "e", "e", "f", "g", "g", "g", "g", "h", "h", "i", "i", "i", "i", "i", "i", "i", "i", "i", "i", "j", "k", "k", "l", "l", "l", "l", "l", "n", "n", "n", "n", "n", "n", "o", "o", "o", "o", "o", "o", "o", "o", "o", "oe", "r", "r", "r", "s", "s", "s", "s", "s", "t", "t", "t", "t", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y", "y", "y", "z", "z", "z", "", "", "ss", "", "", "");
        $text_lower_converted=str_replace($search_lower, $replace_lower, $text_upper_to_lower_converted);
        $text_lower_converted=$db->db_prepare_input($text_lower_converted);
        $text_lower_converted=strtolower($text_lower_converted);
        # END lower case letters


        # GERMAN upper case letters
        $search_upper_german=array("Ä", "À", "Ã", "Â", "Á", "Ç", "É", "Ö", "Ü");
        $replace_upper_german=array("ae","a", "a", "a", "a", "c", "e", "oe","ue");
        $text_upper_to_lower_german_converted=str_replace($search_upper_german, $replace_upper_german, $values['text']);
        $text_upper_to_lower_german_converted=$db->db_prepare_input($text_upper_to_lower_german_converted);
        # END GERMAN upper case letters

        # GERMAN lower case letters
        $search_lower_german=array("ä", "à", "ã", "â", "á", "ç", "é", "ö", "ü", "ß");
        $replace_lower_german=array("ae","a", "a", "a", "a", "c", "e", "oe","ue","ss");
        $text_lower_german_converted=str_replace($search_lower_german, $replace_lower_german, $text_upper_to_lower_german_converted);
        $text_lower_german_converted=$db->db_prepare_input($text_lower_german_converted);
        $text_lower_german_converted=strtolower($text_lower_german_converted);
        # END GERMAN lower case letters

        $text_replaced=array();
        $text_replaced['text_lower_converted']=$text_lower_converted;
        $text_replaced['text_lower_german_converted']=$text_lower_german_converted;

        return $text_replaced;
    }

#end


#prepear search array
	public static function search_array($searchstring)
	{
		if( !empty($searchstring) )
		{
		$tmp=explode(",",$searchstring);
		$search_array=array();
			foreach($tmp as $value)
			{
			$tmp2=explode(":",$value);
            //print_r($tmp2);

                # if searching for numbers with hyphen
                if( stripos($tmp2[0],"-") || stripos($tmp2[1],"-") )
                {
                    if( empty($tmp2[1]) ) $tmp2[1]=$tmp2[0];
                    if( !stripos($tmp2[1],"-") ) $tmp2[1].="-999";
			        $tmp_hyph1=explode("-",$tmp2[0]);
			        $tmp_hyph2=explode("-",$tmp2[1]);
                    $hyph_main_1=$tmp_hyph1[0];
                    $hyph_second_1=$tmp_hyph1[1];
                    $hyph_main_2=$tmp_hyph2[0];
                    $hyph_second_2=$tmp_hyph2[1];
                    //print $hyph_main_1."|".$hyph_second_1."--".$hyph_main_2."|".$hyph_second_2;
				    for($ii=$hyph_main_1;$ii<=$hyph_main_2;$ii++)
                    {
				        for($iii=1;$iii<=1000;$iii++)
                        {
                            if( $ii==$hyph_main_1 && $hyph_second_1>$iii )
                            {
                            }
                            elseif( $ii==$hyph_main_2 && $iii>$hyph_second_2 )
                            {
                                //$search_array[]=($ii+0.999);
                            }
                            else if( $ii==$hyph_main_1 && !stripos($tmp2[0],"-") ) $search_array[]=($ii+($iii*0.000));
                            else if( $ii==$hyph_main_2 && !stripos($tmp2[1],"-") ) $search_array[]=($ii+($iii*0.000));
                            else $search_array[]=($ii+($iii*0.001));
                        }
                    }
                    //print_r($search_array);
                    //exit;
                    if( !empty($search_array) ) $search_array['hyph']=1;
                }
                # end
                elseif(sizeof($tmp2)==2)
				{
				  $tmp3=explode("-",$tmp2[0]);
				  $tmp4=explode("-",$tmp2[1]);

				  for($ii=$tmp3[0];$ii<=$tmp4[0];$ii++)
				  {
				    if( !in_array($ii,$search_array) && !empty($ii) ) $search_array[]=$ii;
				  }
				}
				else
				{
				  if( !in_array($tmp2[0],$search_array) && !empty($tmp2[0]) && $tmp2[0]!=" " )
				  {
                    $tmp2[0]=str_replace(" ", "", $tmp2[0]);
				  $search_array[]=$tmp2[0];
				  }
				}
			}
		return $search_array;
		}
	}
#END


    # if returned value in title contains search tearm order it in front of results
    public static function order_results($db,$values)
    {
    	if( is_array($values['results']) )
    	{
    		$thumb_array=$values['results'];
    	}
    	else
    	{
	        $thumb_array=array();
	        $i=0;
	        while( $row=$db->mysql_array($values['results']) )
	        {

	            if( !empty($values['title']) )
	            {
	                $titleurl=UTILS::convert_fortitleurl($db,TABLE_PAINTING,$values['title'],"","","","en");
	                //$titleurl=$values['title'];
	                //if( strpos($row['titleEN'],$values['title']) !== false || strpos($row['titleDE'],$values['title']) !== false || strpos($row['titleurl'],$titleurl) !== false )

	                $first_word=explode("-", $row['titleurl']);
	                if( $first_word[0]==$titleurl )
	                //if( substr( $titleurl, 0, 2 ) == substr( $row['titleurl'], 0, 2 ) )

	                //if( substr( $titleurl, 0, 2 ) == substr( $row['titleEN'], 0, 2 ) )
	                {
	                	//print "<br />".$values['title']."-".$titleurl."=".substr( $titleurl, 2, 1 )."|".$row['titleurl']."=".substr( $row['titleurl'], 2, 1 )."|||<br />";

	                	//print "found<br />";
	                	//if( substr( $row['titleurl'], 2, 1 )=="-" )

	                	$thumb_array[$i]=$row;
	                }
	                else
	                {
	                	//print "not-found<br />";
	                	$thumb_array[2000+$i]=$row;
	                }
	            }
	            else
	            {
	                $thumb_array[$i]=$row;
	            }

	            //$thumb_array[$i]=$row;

	            $i++;
	        }
	        ksort($thumb_array);
	        $thumb_array=array_values($thumb_array);
	    }
        //print_r($thumb_array);
        return $thumb_array;
    }
    # END



#admin edit item
	public static function admin_edit($url,$options=array())
	{
		//print_r($options);
        $html_print="";
        if( $_SESSION['userID'] )
        {
            @$html_print.="<span style='".$options['style']."' class='edit ".$options['class']['a-admin-edit-span']." ".$options['class']['a-admin-edit-exhibiton-detail']." ".$options['class']['a-admin-edit-exhibiton-list']." ".$options['class']['a-admin-edit-paint-detail-photos-tab']."'>";
            	@$html_print.="<a href='".$url."' title='Edit in admin' class='".$options['class']['a-admin-edit-a']."' >edit</a>";
            $html_print.="</span>";
        }

        if( $options['html_print'] || $options['html_return'] ) return $html_print;
        else print $html_print;
    }
#END


		public static function html_decode($array)
		{
			if( is_array($array) )
			{
				foreach( $array as $key => $value )
				{
					if( is_array($array[$key]) )
					{
					    UTILS::html_decode($array[$key]);
					}
					else
					{
					    $array[$key]=html_entity_decode($array[$key],ENT_NOQUOTES,DB_ENCODEING);
					    //$array[$key]=html_entity_decode($array[$key],ENT_COMPAT,DB_ENCODEING);
					    //$array[$key]=html_entity_decode($array[$key],ENT_QUOTES,DB_ENCODEING);
					}
				}
			}
		    return $array;
		}

          /* add breaks for posted text */
          public static function add_br($text)
          {
          return preg_replace("/\r\n|\n|\r/", "<br />", $text);
          }

		  public static function add_html($text)
          {
          return str_replace("&lt;", "<", str_replace("&gt;", ">", $text));
          }


public static function strip_slashes_recursive( $variable )
{
    if ( is_string( $variable ) )
        $variable=stripslashes( $variable );
        $variable=stripslashes( $variable );
        $variable=stripslashes( $variable );
        $variable=stripslashes( $variable );
        $variable=stripslashes( $variable );
        return $variable;
    if ( is_array( $variable ) )
        foreach( $variable as $i => $value )
            $variable[ $i ] = strip_slashes_recursive( $value ) ;

    return $variable ;
}

public static function just_text_in_quotes($str)
{
    $pattern="/\"(.*?)\"|'(.*?)'/";
    preg_match_all($pattern, $str, $matches);
    $pattern="/\"(.*?)\"|'(.*?)'| /";
    $tmp=preg_split($pattern,$str,-1,PREG_SPLIT_NO_EMPTY);
    $text_return['in_quotes']=$matches[1];
    $text_return['no_quotes']=$tmp;
    //print_r($tmp[0]."1|");
    return $text_return;
}

public static function search_punctuation($db,$values)
{
    $pattarn="/\d+(\.|-|\/)\d+/";
    //$pattarn="/\d.(\.|-|\/)\d(\.|-|\/)\d./";
    $punctuation_check=preg_match($pattarn, $values['word'],$matches);

    //print $values['word'];
    //print_r($matches);
    if( !empty($matches[0]) && $matches[0]!="-" && $matches[0]!="." && $matches[0]!="/"  )
    {
        $ready_search_string="+(";
        if( $punctuation_check )
        {
            # make search string format 14.3.98
            /*
            $tmp=explode(".",$values['word']);
            if( count($tmp)==1 ) $tmp=explode("-",$values['word']);
            if( count($tmp)==1 ) $tmp=explode("/",$values['word']);
            //print_r($tmp);
            $i=0;
            $values['word']="";
            foreach($tmp as $date)
            {
                $i++;
                if( $i==1 )
                {
                    $values['word'].=$date;
                }
                if( $i==2 )
                {
                    $values['word'].=".".$date;
                }
                if( $i==3 )
                {
                    $date=substr($date,2,2);
                    $values['word'].=".".$date;
                }
            }
             */
            # end
            $search = array(".","-","/");
            $ready_search_string.='"'.str_replace($search,".",$values['word']).'" ';
            $ready_search_string.='"'.str_replace($search,"-",$values['word']).'" ';
            $ready_search_string.='"'.str_replace($search,"/",$values['word']).'" ';
            $ready_search_string.='"'.str_replace($search,"",$values['word']).' " ';
        }
        $ready_search_string.=") ";
    }

    $return=array();
    $return['punctuation_check']=$punctuation_check;
    $return['ready_search_string']=$ready_search_string;
    return $return;
}


public static function stripText($text, $separator = '-')
{
    $bad = array(
  'À','à','Á','á','Â','â','Ã','ã','Ä','ä','Å','å','Ă','ă','Ą','ą',
  'Ć','ć','Č','č','Ç','ç',
  'Ď','ď','Đ','đ',
  'È','è','É','é','Ê','ê','Ë','ë','Ě','ě','Ę','ę',
  'Ğ','ğ',
  'Ì','ì','Í','í','Î','î','Ï','ï',
  'Ĺ','ĺ','Ľ','ľ','Ł','ł',
  'Ñ','ñ','Ň','ň','Ń','ń',
  'Ò','ò','Ó','ó','Ô','ô','Õ','õ','Ö','ö','Ø','ø','ő',
  'Ř','ř','Ŕ','ŕ',
  'Š','š','Ş','ş','Ś','ś',
  'Ť','ť','Ť','ť','Ţ','ţ',
  'Ù','ù','Ú','ú','Û','û','Ü','ü','Ů','ů',
  'Ÿ','ÿ','ý','Ý',
  'Ž','ž','Ź','ź','Ż','ż',
  'Þ','þ','Ð','ð','ß','Œ','œ','Æ','æ','µ',
  '”','“','‘','’',"'","\n","\r",'_');

  $good = array(
  'A','a','A','a','A','a','A','a','Ae','ae','A','a','A','a','A','a',
  'C','c','C','c','C','c',
  'D','d','D','d',
  'E','e','E','e','E','e','E','e','E','e','E','e',
  'G','g',
  'I','i','I','i','I','i','I','i',
  'L','l','L','l','L','l',
  'N','n','N','n','N','n',
  'O','o','O','o','O','o','O','o','Oe','oe','O','o','o',
  'R','r','R','r',
  'S','s','S','s','S','s',
  'T','t','T','t','T','t',
  'U','u','U','u','U','u','Ue','ue','U','u',
  'Y','y','Y','y',
  'Z','z','Z','z','Z','z',
  'TH','th','DH','dh','ss','OE','oe','AE','ae','u',
  '','','','','','','','-');

  // convert special characters
  $text = str_replace($bad, $good, $text);

  // convert special characters
  $text = utf8_decode($text);
  $text = htmlentities($text);
  $text = preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde);/', '$1', $text);
  $text = html_entity_decode($text);

  $text = strtolower($text);

  // strip all non word chars
  //$text = preg_replace('/\W/', ' ', $text);

  // replace all white space sections with a separator
  $text = preg_replace('/\ +/', $separator, $text);

  // trim separators
  $text = trim($text, $separator);
  //$text = preg_replace('/\-$/', '', $text);
  //$text = preg_replace('/^\-/', '', $text);

  return $text;
}



public static function prepear_search($search_string,$values=array())
{
    $search_string_full=$search_string;
    $search_string=UTILS::strip_slashes_recursive($search_string);
    $ready_search_string="";

    # cut all stop words
    $search['all'] = array('/^and\s|\sand\s|\sand$s/i','/^&\s|\s&\s|\s&$s/i',"/'|\?|\!|\=/i","/\=/i");
    $replace['all'] = array(' ',' ',""," ");
	$search_string=preg_replace($search['all'], $replace['all'], $search_string);

    //$search['all'] = array("'s","'t","'",",","–");
    //$replace['all'] = array("","","","","","");

    # end

    # find search parts in quotes
    $text_split=UTILS::just_text_in_quotes($search_string);
    //print_r($text_split['no_quotes'][0]."2|");
    foreach( $text_split['in_quotes'] as $word_in_quotes )
    {
        $ready_search_string.='+("'.$word_in_quotes.'") ';
        $search_string=str_replace($word_in_quotes,"",$search_string);
    }
    # end

    # find numbers with -
    /*
    $pattern="/(\d+- ?)+(\d+)?/";
    $numbers_comma_search=preg_match_all($pattern, $search_string, $matches);
    //print $search_string."-";
    //print_r($matches);
    if( $numbers_comma_search && !empty($matches[0]) )
    {
        foreach( $matches[0] as $found_dash )
        {
            //print $numbers_comma_search;
            //$ready_search_string.='+("'.$search_string.'")';
            $ready_search_string.='+("'.$found_dash.'")';
            //$search_string=str_replace($search_string,"",$search_string);
            $search_string=str_replace($found_dash,"",$search_string);
        }
    }
     */
    # end
    //print $ready_search_string."<br />";

    # find numbers with :
    $pattern="/(\d+: ?)+(\d+)?/";
    $numbers_comma_search=preg_match_all($pattern, $search_string, $matches);
    //print $search_string."-";
    //print_r($matches);
    if( $numbers_comma_search )
    {
        $numbers=UTILS::search_array($matches[0][0]);
        $ready_search_string.='(+(';
        foreach( $numbers as $number )
        {
            $ready_search_string.=$number." ";
        }
        $ready_search_string.=')) ';
        //print_r($numbers);
        //exit;
        $search_string=str_replace($matches[0][0],"",$search_string);
    }
    # end
    //print $search_string."-";

    # find only numbers like 2.4.08
    $pattern="/\d+(?:\.\d+)+/";
    $numbers_comma_search=preg_match_all($pattern, $search_string, $matches);
    if( $numbers_comma_search )
    {
        $numbers=UTILS::search_array($matches[0][0]);
        if( is_array($numbers) )
        {
            foreach( $numbers as $number )
            {
                //$ready_search_string.="+(\"".$number."\") ";
                $values_search_punc=array();
                $values_search_punc['word']=$number;
                $punctuation_check=UTILS::search_punctuation($values['db'],$values_search_punc);
                if( $punctuation_check['punctuation_check'] )
                {
                    $ready_search_string.=$punctuation_check['ready_search_string'];
                }
            }
        }
        //print_r($numbers);
        //exit;
        $search_string=str_replace($matches[0][0],"",$search_string);
    }
    # end
    //print $search_string."-";

    # find only numbers
    $leters_search=preg_match_all("/[a-zA-Z]+/i", $search_string, $matches_letters);
    //print $leters_search."-";
    //if( !$leters_search )
    if( !$leters_search && $numbers_comma_search )
    {
        $pattern="/[0-9]+/";
        $numbers_comma_search=preg_match_all($pattern, $search_string, $matches);
        //print_r($matches);
        if( $numbers_comma_search )
        {
            //$numbers=UTILS::search_array($matches[0][0]);
            $numbers=$matches[0];
            if( is_array($numbers) )
            {
                foreach( $numbers as $number )
                {
                    $ready_search_string.="+(".$number.") ";
                    $search_string=str_replace($number,"",$search_string);
                }
            }
            //print_r($matches);
            //print_r($numbers);
            //exit;
        }
    }
    # end
    //print $search_string."-";

    # find comma seperated numbers
    $pattern="/(\d+, ?)+(\d+)?/";
    $numbers_comma_search=preg_match_all($pattern, $search_string, $matches);
    if( $numbers_comma_search )
    {
        $numbers=explode(",", $matches[0][0]);
        $ready_search_string.='(+(';
        foreach( $numbers as $number )
        {
            $ready_search_string.=$number." ";
        }
        $ready_search_string.=')) ';
        //print_r($numbers);
        //exit;
        //$search_string=str_replace($matches[0][0],"",$search_string);
    }
    # end
    //print $search_string."-";

    # find comma seperated text parts
    $comma_check=preg_match("/,/", $search_string);
    if( $comma_check )
    {
        $pattern="/[a-zA-Z]+(,a-zA-Z]+)*/";
        $words_comma_search=preg_match_all($pattern, $search_string, $matches);
        //print_r($matches);
        if( !empty($matches[0][0]) )
        {
            foreach( $matches[0] as $word )
            {
                $ready_search_string.='(+('.$word.'*)) ';
                $search_string=str_replace($word,"",$search_string);
            }
        }
    }
    # end

    # cut all stop words
    /*
    $search['all'] = array("'s","'t","'","&",",","!","–"," and","and ");
    $replace['all'] = array("","","", "","","","","","","");
    $text_split['no_quotes']=str_replace($search['all'],$replace['all'],$text_split['no_quotes']);
    */
    //print_r($text_split);
    # end

    # search punctuation
    foreach( $text_split['no_quotes'] as $key => $word )
    {
        $values_search_punc=array();
        $values_search_punc['word']=$word;
        $punctuation_check=UTILS::search_punctuation($values['db'],$values_search_punc);
        if( $punctuation_check['punctuation_check'] )
        {
            $ready_search_string.=$punctuation_check['ready_search_string'];
            unset($text_split['no_quotes'][$key]);
            //print $punctuation_check['ready_search_string']."<br />";
        }
        //print $word."<br />";
    }
    //print $ready_search_string;
    # end

    //print_r($text_split['no_quotes']);
    # search alternatives
    foreach( $text_split['no_quotes'] as $key => $word )
    {
        $dash_seperated=explode("-", $word);
        if( count($dash_seperated)>1 )
        {
            $text_split['no_quotes'][$key]=$dash_seperated[0];
            $text_split['no_quotes'][]=$dash_seperated[1];
        }
    }

    foreach( $text_split['no_quotes'] as $key => $word )
    {
        //print "1.".$word."|";
        $word=UTILS::stripText($word," ");
        //print "2.".$word."|";
        $values_search_alt=array();
        $values_search_alt=$values;
        $values_search_alt['word']=$word;
        //$values_search_alt['search_string_full']=$search_string_full;
        $word=UTILS::search_alternative($values['db'],$values_search_alt);
        if( $numbers_comma_search ) $ready_search_string.="(";
        if( !empty($word) ) $ready_search_string.='+('.$word.') ';
        if( $numbers_comma_search ) $ready_search_string.=") ";
        //print "3.".$word."|";
    }
    # end

    //exit;

    //print $ready_search_string;
    //print $search_string_full."-";
    # search alternatives full search string
    $values_search_alt=array();
    $values_search_alt['search_string_full']=$search_string_full;
    $word=UTILS::search_alternative($values['db'],$values_search_alt);
    if( !empty($word) ) $ready_search_string.='+('.$word.') ';
    //print $ready_search_string;
    //print $word;
    # end

    //print_r($text_split['no_quotes']);
    //print $ready_search_string;


    //$ready_search_string='+( zwei* +two ) + greys';
    //$ready_search_string='+(10 ten* zehn*) +(colour* color*) ';
    //$ready_search_string='+(2 two* zwei*) +(grey* gray*) ';
    //$ready_search_string='(+(3 5 )) (+(drei* three* iii 3)) (+(funf* five* 5)) ';
    //$ready_search_string='+"woman" +"child" ';
    //$ready_search_string='+"woman" +"child" ';
    //$ready_search_string='+("woman with child") ';
    //$ready_search_string='+(woman* child*) ';
    //$ready_search_string='(+(woman*)) (+(child*)) ';
    //$ready_search_string='+(57* 58*) ';
    //$ready_search_string='(+(2)) (+(kerze*) +(zwei* two* ii 2))';
    //$ready_search_string='+(kerze*) +(zwei* two* ii 2)';
    //$ready_search_string='+"woma"* ';
    //$ready_search_string=' +(" women " ) +("" ) +(" table " ) ';
    //$ready_search_string=' +(" women " ) +(" table " ) ';
    //$ready_search_string='+(tacita dean*)';

    //$prepear_search=str_replace ("\'", "", $ready_search_string);
    //$prepear_search=str_replace ("'", "", $ready_search_string);

    $values_accented=array();
    $values_accented['text']=$ready_search_string;
    $replaced_lang_umplauts=UTILS::replace_accented_letters_search($values['db'], $values_accented);
    $ready_search_string=$replaced_lang_umplauts['text_lower_converted'];

    return $ready_search_string;
}

    public static function search_alternative($db,$values)
    {
        $search_word=strtolower($values['word']);
        //print "|".$search_word."|";
        //exit;

        $i=0;

        # text
        $i++;
        //$replacements[$i]['search']=array("text"); $replacements[$i]['replace']="text* -text-decoration*";

        # coulour
        $i++;
        $replacements[$i]['search']=array("color","colour"); $replacements[$i]['replace']="colour* color*";

        # self-portrait
        $i++;
        //$replacements[$i]['search']=array("self","portrait"); $replacements[$i]['replace']="self* portrait*";

        # Townscape
        $i++;
        $replacements[$i]['search']=array("townscape","cityscape"); $replacements[$i]['replace']="cityscape* townscape*";

        # Deer
        $i++;
        $replacements[$i]['search']=array("deer","stag"); $replacements[$i]['replace']="stag* deer*";

        # Toilet Paper
        $i++;
        $replacements[$i]['search']=array("toilet","loo","klorolle"); $replacements[$i]['replace']="toilet* loo* klorolle* -fuji*";

        # Paper
        $i++;
        $replacements[$i]['search']=array("paper"); $replacements[$i]['replace']="paper*";

        # Park Piece
        $i++;
        $replacements[$i]['search']=array("park","piece","parkstück","parkscape"); $replacements[$i]['replace']="park* piece* parkstück* parkscape* -car* -night* -door*";

        # Forest
        $i++;
        $replacements[$i]['search']=array("forest","wood"); $replacements[$i]['replace']="wood* forest*";

        # Grey
        $i++;
        $replacements[$i]['search']=array("grey","gray"); $replacements[$i]['replace']="gray* grey*";

        # Porträt
        $i++;
        //$replacements[$i]['search']=array("porträt","portrait"); $replacements[$i]['replace']="portrait*";

        # Stillleben
        $i++;
        $replacements[$i]['search']=array("stillleben","stilleben"); $replacements[$i]['replace']="stilleben* stillleben*";

        # still
        $i++;
        if( $values['search_art'] ) $replacements[$i]['search']=array("still"); $replacements[$i]['replace']="5165";

        # Graphik
        $i++;
        $replacements[$i]['search']=array("graphik","graphik"); $replacements[$i]['replace']="graphik* grafik*";

        # Photographie
        $i++;
        $replacements[$i]['search']=array("photographie","fotografie"); $replacements[$i]['replace']="photographie* fotografie*";

        # Gizeh
        $i++;
        $replacements[$i]['search']=array("gizeh","giseh","giza"); $replacements[$i]['replace']="gizeh* giseh* giza*";

        # numbers
        $i++;
        $replacements[$i]['search']=array("zwei","two","ii","II","2"); $replacements[$i]['replace']="zwei* two* II 2";
        $i++;
        $replacements[$i]['search']=array("drei","three","iii","III","3"); $replacements[$i]['replace']="drei* three* III 3";
        $i++;
        $replacements[$i]['search']=array("vier","four","iv","IV","4"); $replacements[$i]['replace']="vier* four* IV 4";
        $i++;
        $replacements[$i]['search']=array("fünf","five","5"); $replacements[$i]['replace']="fünf* five* 5 05";
        $i++;
        $replacements[$i]['search']=array("sechs","six","6"); $replacements[$i]['replace']="sechs* six* 6";
        $i++;
        $replacements[$i]['search']=array("sieben","seven","7"); $replacements[$i]['replace']="sieben* seven* 7";
        $i++;
        $replacements[$i]['search']=array("acht","eight","8"); $replacements[$i]['replace']="acht* eight* 8";
        $i++;
        $replacements[$i]['search']=array("neun","nine","9"); $replacements[$i]['replace']="neun* nine* 9";
        $i++;
        $replacements[$i]['search']=array("zehn","ten","10"); $replacements[$i]['replace']="zehn* ten* 10";
        $i++;
        $replacements[$i]['search']=array("elf","eleven","11"); $replacements[$i]['replace']="elf* eleven* 11";
        $i++;
        $replacements[$i]['search']=array("zwölf","twelve","12"); $replacements[$i]['replace']="zwölf* twelve* 12";
        $i++;
        $replacements[$i]['search']=array("dreizehn","thirteen","13"); $replacements[$i]['replace']="dreizehn* thirteen* 13";
        $i++;
        $replacements[$i]['search']=array("vierzehn","fourteen","14"); $replacements[$i]['replace']="vierzehn* fourteen* 14";
        $i++;
        $replacements[$i]['search']=array("fünfzehn","fifteen","15"); $replacements[$i]['replace']="fünfzehn* fifteen* 15";
        $i++;
        $replacements[$i]['search']=array("achtzehn","eighteen","18"); $replacements[$i]['replace']="achtzehn* eighteen* 18";

        # us
        $i++;
        //$replacements[$i]['search']=array("us","usa"); $replacements[$i]['replace']="usa* "; //-Usbekistan*

        # an aside
        $i++;
        //$replacements[$i]['search']=array("an","aside"); $replacements[$i]['replace']="Tacita Dean*";

        # said
        $i++;
        $replacements[$i]['search']=array("said"); $replacements[$i]['replace']="saidling*";

        # page
        $i++;
        $replacements[$i]['search']=array("page"); $replacements[$i]['replace']="Figurative*";

        # table
        $i++;
        //$replacements[$i]['search']=array("tabl","table"); $replacements[$i]['replace']="+table* -(table>)";

        # self-portrait
        $i++;
        //$replacements[$i]['search']=array("self-portrait"); $replacements[$i]['replace']='"self-portrait"';

        # forty
        $i++;
        $replacements[$i]['search']=array("forty","40"); $replacements[$i]['replace']='"forty*" "40*"';

        $replacements[$i]['search']=array("kunsthel"); $replacements[$i]['replace']='"kunsthandel*"';
        $i++;
        $replacements[$i]['search']=array("in finitum"); $replacements[$i]['replace']='"in-finitum*"';

        # d.a.p
        $i++;
        $replacements[$i]['search']=array("d.a.p"); $replacements[$i]['replace']='"d.a.p*"';

        # search individual word
        $found=0;
        foreach ($replacements as $key => $val)
        {
            if( is_array($val['search']) ) $search=in_array($search_word,$val['search']);
            if( $search )
            {
                $return_word=$replacements[$key]['replace'];
                $found=1;
            }
        }

        if( !$found && !empty($values['word']) ) $return_word=$values['word']."*";
        # end

        # search_string_full
        if( $values['search_string_full']=="how you look at it" ) $return_word="+Fotografien* +Jahrhunderts*";
        # end

        //print $return_word."-";
        return $return_word;
    }


    public static function prepare_cr_number_search($db,$values)
    {
        $search_array=UTILS::search_array($values['number']);

        //print_r($search_array);

        if( $search_array['hyph'] )
        {
            unset($search_array['hyph']);
            $min_number=$search_array[0];
            $max_number=$search_array[(count($search_array)-1)];
            $numer_search=" ( p.number_search2 BETWEEN '".$min_number."' AND '".$max_number."' OR p.number_old='".$values['number']."' ) ";
        }
        else
        {
            $numer_search=" ( ";
            for($i=0;$i<sizeof($search_array);$i++)
            {
                list($search_array[$i], $tmp) = explode('-', $search_array[$i]);
                if($i>0) $numer_search.=" OR ";
                $numer_search.=" p.number='".$search_array[$i]."' ";
                $numer_search.=" OR p.number_old='".$search_array[$i]."' ";
                $numer_search.=" OR p.number_search='".$search_array[$i]."' ";
                $numer_search.=" OR p.number_search LIKE '".str_replace("-", "", $search_array[$i])."-%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]." -%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]." - %' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."/%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."a' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."a%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."b' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."b%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."c' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."c%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."d' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."d%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."e' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."e%' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."f' ";
                $numer_search.=" OR p.number_search LIKE '".$search_array[$i]."f%' ";
                //$numer_search.=" OR p.number_search LIKE 'zu ".$search_array[$i]."' ";
                if( strlen($values['number'])>=3 ) $numer_search.=" OR number LIKE '".$values['number']."%' ";
            }

            if( sizeof($search_array)==0 )
            {
                list($values['number'], $tmp) = explode('-', $values['number']);
                $numer_search.=" p.number LIKE '".str_replace("-", "", $values['number'])."-%' ";
                $numer_search.=" OR p.number_old LIKE '".str_replace("-", "", $values['number'])."-%' ";
                $numer_search.=" OR p.number_search LIKE '".str_replace("-", "", $values['number'])."-%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']." -%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']." - %' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."/%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."a' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."a%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."b' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."b%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."c' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."c%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."d' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."d%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."e' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."e%' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."f' ";
                $numer_search.=" OR p.number_search LIKE '".$values['number']."f%' ";
                //$numer_search.=" OR p.number_search LIKE 'zu ".$values['number']."' ";
                if( strlen($values['number'])>=3 ) $numer_search.=" OR p.number_search LIKE '".$values['number']."%' ";
            }
            $numer_search.=" ) ";
        }

        return $numer_search;
    }


public static function ifemptyGET($get,$sp=SHOW_PER_PAGE,$p=1,$pp=0)
{
    ### if no category defined
    //if( !is_numeric($get['catID']) ) $get['catID']=1;
    #end

    ### if no page divideing variable spassed
    if( empty($get['show_per_page']) )
    {
        $get['page_selected']=1;
        if( $pp==0 )
        {
            $get['show_per_page']=SHOW_PER_PAGE;
        }
        else
        {
            $get['show_per_page']=$pp;
        }
    }
    /*
    if( empty($get['sp']) )
    {
        $get['p']=1;
        if( $p==0 )
        {
            $get['sp']=SHOW_PER_PAGE;
        }
        else
        {
            $get['sp']=$p;
        }
    }
     */
    #end

    if( empty($get['sp']) ) $get['sp']=$sp;
    if( empty($_SESSION['show_per_page']) ) $_SESSION['show_per_page']=$sp;
    if( empty($_SESSION['sp']) ) $_SESSION['sp']=$sp;
    if( empty($get['p']) ) $get['p']=$p;
    if( empty($get['pp']) ) $get['pp']=20;
    if( empty($get['l']) ) $get['l']=0;

//print_r($get);
    return $get;

}

public static function if_empty_get($db,$values)
{
	if( empty($values['get']['p']) ) $values['get']['p']=1;
    else $values['get']['p']=preg_replace("/[^0-9]/", "", $values['get']['p']);
	if( $values['get']['sp']!='all' ) $values['get']['sp']=preg_replace("/[^0-9]/", "", $values['get']['sp']);

    ### if no page divideing variable spassed
    if( $values['get']['sp']=='all' ) $values['get']['sp']=1000000;
    if( empty($values['get']['sp']) )
    {
        $values['get']['p']=1;
        if( $values['sp']==0 )
        {
            $values['get']['sp']=SHOW_PER_PAGE;
            if( empty($_SESSION['sp']) ) $_SESSION['sp']=SHOW_PER_PAGE;
        }
        else
        {
            $values['get']['sp']=$values['sp'];
            if( empty($_SESSION['sp']) ) $_SESSION['sp']=$values['sp'];
        }
    }
    else
    {
        $_SESSION['sp']=$values['get']['sp'];
    }
    #end

    return $values['get'];
}


    public static function row_text_value_main($db,$values)
    {
        $found=0;
        # if searching returning latnguage value in which search tearm was found
        /*
        if( !empty($values['search']) )
        {
            if( stripos($values['row'][$values['lang1']],$values['search']) ) $value=$values['row'][$values['lang1']];
            elseif( stripos($values['row'][$values['lang2']],$values['search']) ) $value=$values['row'][$values['lang2']];
            elseif( stripos($values['row'][$values['lang3']],$values['search']) ) $value=$values['row'][$values['lang3']];
            elseif( stripos($values['row'][$values['lang4']],$values['search']) ) $value=$values['row'][$values['lang4']];
            else $found=1;
        }
         */
        # end
        //if( empty($values['search']) || $found )
        //{
            $value=$values['row'][$values['lang_current']];
            //if( !$values['strip_tags'] ) $test=strip_tags($value);
            $test=strip_tags($value);
            //else $test=$value;
            $test=trim($test);
            if( empty($test) ) $value="";

            if( empty($value) )
            {
                $value=$values['row'][$values['lang1']];
                //if( !$values['strip_tags'] ) $test=strip_tags($value);
                $test=strip_tags($value);
                //else $test=$value;
                $test=trim($test);
                if( empty($test) ) $value="";
            }
            if( empty($value) )
            {
                $value=$values['row'][$values['lang2']];
                //if( !$values['strip_tags'] ) $test=strip_tags($value);
                $test=strip_tags($value);
                //else $test=$value;
                $test=trim($test);
                if( empty($test) ) $value="";
            }
        //}

        //print_r($value);
        return $value;
    }


    public static function row_text_value2($db,$row,$title,$values=array())
    {
        $values_row=array();
        $values_row['search']=$values['search'];
        $values_row['row']=$row;
        $values_row['lang_current']=$title.strtoupper($_GET['lang']);
        $values_row['lang1']=$title."EN";
        $values_row['lang2']=$title."DE";
        $values_row['lang3']=$title."FR";
        $values_row['lang4']=$title."IT";
        $values_row['lang5']=$title."ZH";
        $value=UTILS::row_text_value_main($db,$values_row);

        return $value;
    }

    public static function row_text_value($db,$row,$title,$options=array())
    {
        $options_row=array();
        $options_row['search']=$options['search'];
        $options_row['row']=$row;
        $options_row['lang_current']=$title."_".$_GET['lang'];
        $options_row['lang1']=$title."_en";
        $options_row['lang2']=$title."_de";
        $options_row['lang3']=$title."_fr";
        $options_row['lang4']=$title."_it";
        $options_row['lang5']=$title."_zh";
        $value=UTILS::row_text_value_main($db,$options_row);

        return $value;
    }



    public static function row_text_value3($db,$row,$title,$values=array())
    {
        $values_row=array();
        $values_row['search']=$values['search'];
        $values_row['row']=$row;
        $values_row['lang_current']=$title.$_GET['lang'];
        $values_row['lang1']=$title."en";
        $values_row['lang2']=$title."de";
        $values_row['lang3']=$title."fr";
        $values_row['lang4']=$title."it";
        $values_row['lang5']=$title."zh";
        $value=UTILS::row_text_value_main($db,$values_row);

        return $value;
    }

        public static function max_table_value($db,$value,$table,$where)
        {
            $query="SELECT MAX(".$value.")+1 AS maximum_value FROM ".$table." ";
            if( !empty($where) ) $query.=$where;
            $results=$db->query($query);
            $row=$db->mysql_array($results);
            if( !$row['maximum_value'] ) $maxsort=1;
            else $maxsort=$row['maximum_value'];
            return $maxsort;
        }


        public static function get_artwork_info($db,$artworkid)
        {
            $results=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='".$artworkid."' ");
            $row=$db->mysql_array($results);
            if( $row['artworkID']== 3 ) $type=LANG_ATLAS_SHEET;
            else if( $row['artworkID']== 4 ) $type=LANG_CR_EDITIONS;
            else if( $row['artworkID']== 7 ) $type=LANG_CR_DRAWINGS;
            else if( $row['artworkID']== 1 || $row['artworkID']== 2 || $row['artworkID']==13 ) $type=LANG_CATALOGUE_RESONE;
            else $type=LANG_SEARCH_NUMBER;
            //if( $row['artworkID']==4 ) $nr="Editions CR";
            //elseif( $row['artworkID']==3 ) $nr="Atlas Sheet";
            //else $nr="CR";
            $row['nr']=$type;

            return $row;
        }

        public static function get_books_category($db,$books_catid)
        {
            $results=$db->query("SELECT * FROM ".TABLE_BOOKS_CATEGORIES." WHERE books_catid='".$books_catid."' ");
            $row=$db->mysql_array($results);
            $title=UTILS::row_text_value($db,$row,"title");
            return $title;
        }



public static function array_average($arr)
{
    $sum = array_sum($arr);
    $num = sizeof($arr);
    return round($sum/$num);
}

public static function Dot2LongIP ($IPaddr) {
    if ($IPaddr == "")
    {
        return 0;
    } else {
        $ips = explode ("\.", "$IPaddr");
        return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
    }
}



    public static function translitForUrl($db,$lang,$text)
    {
        $search['all'] = array(
            " - "," – ","–","ä", "à", "ã", "â", "á", "ç", "é", "ö", "ü", "ß","«","»",'/','„','”','  ',' ','#','@','~','!','?','.','(',')',':',';','"','\"',',',"'","\'",'`','&quot;','&','“',
            "ä", "Ä", "à", "À", "ã", "Ã", "â", "Â", "á", "Á", "ç", "Ç", "é", "É", "ö", "Ö", "ü", "Ü", "Ü", "ß",
            'à','â','ä','è','é','ê','ë','î','ï','ô','œ','ù','û','ü','ÿ','À','Â','Ä','È','É','Ê','Ë','Î','Ï','Ô','Œ','Ù','Û','Ü','Ÿ','ç','Ç'
        );

        $replace['all'] = array(
            "-","-","-","a", "a", "a", "a", "a", "c", "e", "o", "u","ss","","",'-','','','','-','-','','','','','','','','','','','','','','','','','','and','',
            "a", "A", "a", "A", "a", "A", "a", "A", "a", "A", "c", "C", "e", "E", "o", "O", "u", "U", "U","ss",
            'a','a','a','e','e','e','e','i','i','o','oe','u','u','u','y','A','A','A','E','E','E','E','I','I','O','Oe','U','U','U','Y','c','C'
        );
        $text = trim($text);

        $replaced_string=str_replace($search['all'],$replace['all'],$text);

        return stripslashes($replaced_string);

    }


          public static function convert_fortitleurl($db,$table,$titleurl,$id_name,$id,$type,$lang="en",$length=55,$delimiter="-")
          {
            $titleurl=strip_tags($titleurl);
            $titleurl=stripslashes($titleurl);

            #
            $values_accented=array();
            $values_accented['text']=$titleurl;
            $values_accented['delimiter']=$delimiter;
            $title_replaced=UTILS::replace_accented_letters($db, $values_accented);
            $titleurl=$title_replaced['text_lower_converted'];
            #

            $titleurl_new=UTILS::translitForUrl($db, $lang,$titleurl);
            $titleurl_new=$titleurl;
            $titleurl_new=strtolower($titleurl_new);
            //$titleurl_new=urlencode($titleurl_new);
            $titleurl_new=substr($titleurl_new, 0, $length);
            //$tmp=str_split($titleurl_new);
            //if( $tmp[(count($tmp)-1)]=="_" )
            //{
                //$titleurl_new=wordwrap($titleurl_new,(strlen($titleurl_new)-1));
            //}
            //exit($titleurl_new);
            if( !empty($id) )
            {
            	if( !empty($titleurl_new) ) $titleurl_new=$titleurl_new.$delimiter.$id;
            	else $titleurl_new=$id;
            }

            return $titleurl_new;
          }


        public static function removeTags($source)
        {
            $source = strip_tags(stripslashes($source), '<p><a><br><span><div><sub><sup><img><ul><ol><li><hr><table><tr><td><thead><th><font>');
            return trim($source);
        }


    public static function cache_uri($src,$src_cache_uri,$forcecache=0,$values=array())
    {
        if( $_GET['forcecache'] ) $forcecache=1;

        if( empty($values['cache_dir']) ) $values['cache_dir']="cache";

        if( !empty($values['url']) ) $url=DATA_PATH.$values['url'];
        else $url=DATA_PATH."/images_new";

        $src_cache_uri=$url."/".$values['cache_dir']."/".$src_cache_uri;
        if( $values['debug'] ) print $src."-".$src_cache_uri;
        $exists=false;

        if( $forcecache==1 )
        {
            if( $_SERVER['SERVER_NAME']=="localhost" )
            {
                $url_server="localhost";
            }
            else
            {
                $url_server=$_SERVER['SERVER_NAME'];
            }

            if( !file_exists($url."/original/".$src) )
            {
                $tmp=explode(".", $src);
                $src_gif=$url."/original/".$tmp[0].".gif";
                $src_png=$url."/original/".$tmp[0].".png";
                $src_bmp=$url."/original/".$tmp[0].".bmp";
                if( file_exists($src_gif) ) $src=$tmp[0].".gif";
                elseif( file_exists($src_png) ) $src=$tmp[0].".png";
                elseif( file_exists($src_bmp) ) $src=$tmp[0].".bmp";
            }

            $handle = fopen( ("http://".$url_server.$src), "rb", false);
            $bin = '';
            while( !feof( $handle ) )
            {
                $bin .= fread( $handle, 8192 );
            }
            fclose($handle);

            $handle=fopen($src_cache_uri,"wb");
            fwrite($handle,$bin);
            fclose($handle);
            $exists=true;
        }
        else
        {
            if( file_exists($src_cache_uri) )
            {
                # HIT - cached file exists already.
                //error_log("HIT $src_cache_uri");
                $exists=true;
            }
            else
            {
                # MISS
                //error_log("MISS $src $src_cache_uri");
            }
        }

        return $exists;
    }

    public static function get_relation_id($db,$typeid,$row_relation)
    {
        if( $row_relation['typeid1']==$typeid ) $id=$row_relation['itemid1'];
        elseif( $row_relation['typeid2']==$typeid ) $id=$row_relation['itemid2'];
        return $id;
    }

    public static function get_sort_column($db,$typeid)
    {
        if( $typeid==1 ) $sort_column="sort1";
        else $sort_column="sort";
        return $sort_column;
    }


    public static function painting_sort_number($db,$values)
    {
        if( !empty($values['number']) )
        {
            if ( $values['artworkid']==4 && preg_match("/^Strip \([a-z\s]*\)$/", $values['title_en']) ) # edition title "Strip (proof print)"
            {
                $sort_number=$values['number']+0.900;
                $sort_number2=$values['number'];
                //print_r($painting_array);
                //exit;
            }
            elseif ( $values['artworkid']==4 && preg_match("/^Cage f.ff$/", $values['title_en']) ) # edition title "Cage f.ff"
            {
            	if( $values['sort_number_current']=="5000165.001" )
            	{
	                $sort_number="5000165.001";
	                $sort_number2="5000165";
            	}
            	else
            	{
	                $sort_number=$values['number']+0.002;
	                $sort_number2=$values['number'];
            	}
            }
            //if ( $values['artworkid']==4 && preg_match("/^Strip \(/", $values['title_en']) ) # edition title "Strip (160)"
            elseif ( $values['artworkid']==4 && preg_match("/^Strip \(\d*\)$/", $values['title_en']) )
            {
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Strip (%' AND titleEN NOT LIKE 'Strip (p%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    $painting_array[$number_extracted[1]]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.300;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;
            }
            elseif ( $values['artworkid']==4 && preg_match("/^War Cut II \(/", $values['title_en']) ) # editiaon title "War Cut II (1/50)" "War Cut II (I/XX)"
            {
                # case War Cut II (1/50)
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'War Cut II (%' AND titleEN LIKE '%/50)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    //$number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    preg_match("/\([0-9]+\/[0-9]+\)/",$row['titleEN'],$match);
                        //print $row['titleEN']."<br />";
                        //print $number_extracted[1]."<br />";
                        //print_r($tmp)."<br />";
                        //print_r($number_extracted)."<br />";
                    if( !empty($match[0]) )
                    {
                        $match[0]=str_replace("(", "", $match[0]);
                        $match[0]=str_replace(")", "", $match[0]);
                        $tmp=explode("/", $match[0]);
                        $number_extracted=$tmp[0];
                        $painting_array[$number_extracted]=$row;
                    }
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.100;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;

                #case War Cut II (I/XX)
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'War Cut II (%' AND titleEN LIKE '%/XX)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    //$number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    //$tmp=explode("/", $number_extracted[1]);

                    //preg_match("/\([0-9]+\/XX\)/",$row['titleEN'],$match);
                    $match=preg_split("/[\s,()]+/",$row['titleEN']);
                        //print $row['titleEN']."<br />";
                        //print $match[3]."<br />";
                        //print_r($tmp)."<br />";
                        //print_r($match)."<br />";
                        //print_r($number_extracted)."<br />";
                    if( !empty($match[3]) )
                    {
                        $tmp=explode("/", $match[3]);
                        $number_extracted=$tmp[0];
                        $number_extracted=UTILS::rom2arab($number_extracted);
                        $painting_array[$number_extracted]=$row;
                    }
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.300;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;

            }
            elseif ( $values['artworkid']==4 && preg_match("/^Park \(/", $values['title_en']) ) # edition title "Park (1 May 1990)"
            {
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Park (%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    $painting_array[$number_extracted[1]]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.300;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;
            }
            elseif ( $values['artworkid']==4 && preg_match("/^Kassel \(/", $values['title_en']) ) # editiaon title "Kassel (1/50)" "Kassel (VI/XXV)" "Kassel (a.p.)"
            {
                # case Kassel (1/50)
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Kassel (%' AND titleEN LIKE '%/50)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    $tmp=explode("/", $number_extracted[1]);
                    $number_extracted=$tmp[0];
                    //print $number_extracted[1]."<br />";
                    $painting_array[$number_extracted]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.100;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;

                #case Kassel (VI/XXV)
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Kassel (%' AND titleEN LIKE '%/XXV)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    $tmp=explode("/", $number_extracted[1]);
                    $number_extracted=$tmp[0];
                    $number_extracted=UTILS::rom2arab($number_extracted);
                    //print_r($number_extracted);
                    $painting_array[$number_extracted]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.300;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;
                #case Kassel (a.p.)
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Kassel (%' AND titleEN LIKE '%a.p.)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    //print_r($number_extracted);
                    //$painting_array[$number_extracted[1]]=$row;
                    $painting_array[$row['paintID']]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                	//print $key."-<br />";
                    if( $row['paintID']==$values['paintid'] )
                    {
                    	//print $key."-<br />";
                        $sort_number=$values['number']+($key*0.001)+0.500;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                }
                //print_r($painting_array);
                //exit;

            }
            elseif ( $values['artworkid']==4 && preg_match("/^Snow White \(/", $values['title_en']) ) #edition title "Snow White (1/100)"
            {
                $painting_array=array();
                $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='4' AND titleEN LIKE 'Snow White (%' AND titleEN LIKE '%/100)%' ");
                while( $row=$db->mysql_array($results) )
                {
                    $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                    //print_r($number_extracted);
                    $tmp=explode("/", $number_extracted[2]);
                    $number_extracted=$tmp[0];
                    //print $number_extracted[1]."<br />";
                    $painting_array[$number_extracted]=$row;
                }
                ksort($painting_array);
                $painting_array=array_values($painting_array);
                foreach( $painting_array as $key => $row )
                {
                    if( $row['paintID']==$values['paintid'] )
                    {
                        $sort_number=$values['number']+($key*0.001)+0.100;
                        $sort_number2=$values['number']+($key*0.001);
                    }
                    //else print "not-".$row['paintID']."-".$values['paintid']."<br />";
                }
                //print_r($painting_array);
                //print "|".$sort_number."|";
                //exit('here');

            }
            elseif (preg_match("/^[0-9]*$/", $values['number'])) #123
            {
                $sort_number=$values['number'];
                //$sort_number2=$values['number'];
            }
            elseif (preg_match("/^[0-9]*-[0-9]*$/", $values['number'])) #123-4
            {
                $tmp=explode("-", $values['number']);
                $sort_number=$tmp[0]+($tmp[1]*0.001)+0.300;
                $sort_number2=$tmp[0]+($tmp[1]*0.001);
            }
            elseif (preg_match("/^[0-9]*\/[0-9]*$/", $values['number'])) #123/4
            {
                $tmp=explode("/", $values['number']);
                if( $values['artworkid']==7 && $tmp[1]>=16 )
                {
                    $sort_number=$tmp[0]+($tmp[1]+200)*0.001; # for drawings
                    $sort_number2=$tmp[0]+$tmp[1]*0.001; # for drawings
                }
                else
                {
                    $sort_number=$tmp[0]+($tmp[1]*0.001);
                    $sort_number2=$tmp[0]+($tmp[1]*0.001);
                }
                if( $values['artworkid']==7 && $tmp[0]=="05" )
                {
                    $sort_number=$sort_number+2000; # for drawings 05/1, 05/2 ...
                    $sort_number2=$sort_number+2000; # for drawings 05/1, 05/2 ...
                }
                if( $values['artworkid']==7 && empty($values['number']) ) $sort_number=$sort_number+$values['year_drawings']; # for drawings 1965 year no number
                //print_r($tmp);
                //print $sort_number;
                //exit;
            }
            elseif (preg_match("/^[0-9]*\/[0-9]*-[0-9]*$/", $values['number'])) #123/1-44
            {
                $tmp=explode("/", $values['number']);
                $tmp2=explode("-", $tmp[1]);
                if( $values['artworkid']==7 )
                {
                    //$sort_number=$tmp[0]+$tmp2[0]*0.001+(($tmp2[1]+1)*0.001)+$values['year_drawings']; # for drawings 78/15-35
                    $sort_number=$tmp[0]+$tmp2[0]*0.001+(($tmp2[1]+1)*0.001); # for drawings 78/15-35
                    $sort_number2=$tmp[0]+$tmp2[0]*0.001+(($tmp2[1]+1)*0.001);
                }
                else
                {
                    $sort_number=$tmp[0]+(($tmp2[1]+1)*0.001);
                    $sort_number2=$tmp[0]+(($tmp2[1]+1)*0.001);
                }
            }
            elseif (preg_match("/^[0-9]*-[a-z]*$/", $values['number'])) #123-b
            {
                $tmp=explode("-", $values['number']);
                $values_letter['string']=$tmp[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+($replaced_letter*0.001)+0.600;
                $sort_number2=$tmp[0]+($replaced_letter*0.001);
            }
            elseif (preg_match("/^[0-9]*-[A-Z]*$/", $values['number'])) #123-B
            {
                $tmp=explode("-", $values['number']);
                $values_letter['string']=$tmp[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+($replaced_letter*0.001)+0.600;
                $sort_number2=$tmp[0]+($replaced_letter*0.001);
            }
            elseif (preg_match("/^[0-9]*\/[a-z]*-[a-z]*$/", $values['number'])) #123/a-b !!!!!!!!!
            {
                $tmp=explode("/", $values['number']);
                $tmp2=explode("-", $tmp[1]);
                $values_letter['string']=$tmp2[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+(($replaced_letter+1)*0.001)+0.600;
                $sort_number2=$tmp[0]+(($replaced_letter+1)*0.001);
            }
            elseif (preg_match("/^[0-9]*\/[A-Z]*-[A-Z]*$/", $values['number'])) #123/A-B
            {
                $tmp=explode("/", $values['number']);
                $tmp2=explode("-", $tmp[1]);
                $values_letter['string']=$tmp2[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+(($replaced_letter+1)*0.001)+0.500;
                $sort_number2=$tmp[0]+(($replaced_letter+1)*0.001);
            }
            elseif (preg_match("/^[0-9]*\/[0-9]*[a-z]-[0-9]*[a-z]*$/", $values['number'])) #161/1a-2a NOT FROM ANDY
            {
                $tmp=explode("/", $values['number']);
                $tmp2=explode("-", $tmp[1]);
                $values_letter['string']=$tmp2[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+($replaced_letter*0.001);
                $sort_number2=$tmp[0]+($replaced_letter*0.001);
            }
            elseif (preg_match("/^[0-9]*\/[0-9]*[A-Z]-[0-9]*[A-Z]*$/", $values['number'])) #161/1A-2A !!!!!!!!
            {
                $tmp=explode("/", $values['number']);
                $tmp2=explode("-", $tmp[1]);
                $values_letter['string']=$tmp2[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+($replaced_letter*0.001);
                $sort_number2=$tmp[0]+($replaced_letter*0.001);
            }
            elseif (preg_match("/^[0-9]*-[0-9]*[a-z]*$/", $values['number'])) #224-11a NOT FROM ANDY
            {
                $tmp=explode("-", $values['number']);
                $values_letter['string']=$tmp[1];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[0]+("0.".$replaced_letter)+0.200+0.001;
                $sort_number2=$tmp[0]+("0.".$replaced_letter)+0.001;
            }
            elseif (preg_match("/^[0-9]*[a-z]*$/", $values['number'])) #324a NOT FROM ANDY
            {
                preg_match('~(\d+(?:))\s*([a-z]+)~i', $values['number'], $tmp);
                $values_letter['string']=$tmp[2];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[1]+($replaced_letter*0.001)+0.600;
                $sort_number2=$tmp[1]+($replaced_letter*0.001);
            }
            elseif (preg_match("/^[0-9]*\s[a-z]*$/", $values['number'])) #789 a, 789 b....j
            {
                preg_match('~(\d+(?:))\s*([a-z]+)~i', $values['number'], $tmp);
                $values_letter['string']=$tmp[2];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[1]+($replaced_letter*0.001)+0.600;
                $sort_number2=$tmp[1]+($replaced_letter*0.001);
                //print $sort_number;
                //exit('here');
            }
            elseif (preg_match("/^[0-9]*[a-z]*[0-9]*$/", $values['number'])) #20a1 NOT FROM ANDY
            {
                preg_match('~(\d+(?:))\s*([a-z]+)~i', $values['number'], $tmp);
                $values_letter['string']=$tmp[2];
                $tmp2=preg_split("/[a-z]+/",$values['number']);
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                if( !empty($tmp2[1]) ) $temporary=($replaced_letter*0.01)+($tmp2[1]*0.001);
                else $temporary=($replaced_letter*0.001);
                $sort_number=$tmp[1]+$temporary+0.600;
                $sort_number2=$tmp[1]+$temporary;
            }
            /*
            elseif (preg_match("/^zu [0-9]*$/", $values['number'])) #zu 123 - not sure if this is used any more as tem agreed to remove zu wording in cr numbers 10.11.2016
            {
                $tmp=explode(" ", $values['number']);
                $sort_number=$tmp[1]+0.900;
                $sort_number2=$tmp[1]+0.900;
            }
            */
            elseif (preg_match("/^P[0-9]*$/", $values['number'])) # P1 P2 ...P11
            {
                $sort_number=preg_replace('/\D+/', '', $values['number']);
                $sort_number2=preg_replace('/\D+/', '', $values['number']);
            }
            /*
            elseif (preg_match("/^zu [0-9]* [a-z]$/", $values['number'])) #zu 123 e, zu 123 h, .....cfj - not sure if this is used any more as tem agreed to remove zu wording in cr numbers 10.11.2016
            {
                $tmp=explode(" ", $values['number']);
                $values_letter['string']=$tmp[2];
                $replaced_letter=UTILS::replace_letter_to_number($db,$values_letter);
                $sort_number=$tmp[1]+0.900+($replaced_letter*0.001);
                $sort_number2=$tmp[1]+0.900+($replaced_letter*0.001);
            }
            */
            else
            {
                $sort_number=0;
                $sort_number2=0;
            }

            $sort_number=$sort_number+($values['artwork_sort']*1000000);

        }
        elseif ( $values['artworkid']==6 && preg_match("/^Firenze \(/", $values['title_en']) ) #opp title "Firenze (1/99)" and "Firenze (1/7 a.p.)"
        {
            #case Firenze (1/99)
            $painting_array=array();
            $query_firenze_99="SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Firenze (%' AND titleEN LIKE '%/99)%' ";
            //print $query_firenze_99."<br />";
            $results=$db->query($query_firenze_99);
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted_array=preg_split("/[\s,()]+/",$row['titleEN']);
                $number_extracted=explode("/", $number_extracted_array[1]);
                //print_r($number_extracted);
                $painting_array[$number_extracted[0]]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] )
                {
                	$sort_number=$values['year']+($key*0.001)+0.500;
                	//print "here-".$sort_number."<br />";
                }
                //print $values['paintid']."-".$row['paintID']."<br />";
            }
            //print_r($painting_array);
            //exit;

            #case Firenze (XII/XII)
            $painting_array=array();
            $query_firenze_roman="SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Firenze (%' AND titleEN LIKE '%/XII)%' ";
            //print $query_firenze_roman."<br />";
            $results=$db->query($query_firenze_roman);
            while( $row=$db->mysql_array($results) )
            {
            	//print $row['paintID']."-".$row['titleEN']."<br />";
                $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                $tmp=explode("/", $number_extracted[1]);
                $number_extracted=$tmp[0];
                $number_extracted=UTILS::rom2arab($number_extracted);
                //print_r($number_extracted);
                $painting_array[$number_extracted]=$row;
                //print "<br /><br />";
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.600;
            }
            //print_r($painting_array);
            //exit;

            #case Firenze (a.p.)
            $painting_array=array();
            $query_firenze_ap="SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Firenze (%' AND titleEN LIKE '%a.p.)%' ";
            $results=$db->query($query_firenze_ap);
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                //print_r($number_extracted);
                $painting_array[$number_extracted[1]]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.900;
            }

            $sort_number=$sort_number+($values['artwork_sort']*1000000);

            //print $sort_number;
            //exit;
        }
        elseif ( $values['artworkid']==6 && preg_match("/^Forest/", $values['title_en']) && empty($values['date']) ) #opp title "Forest (1/80)"
        {
            $painting_array=array();
            $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Forest (%' ");
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted_array=preg_split("/[\s,()]+/",$row['titleEN']);
                $number_extracted=explode("/", $number_extracted_array[1]);
                //print_r($number_extracted);
                $painting_array[$number_extracted[0]]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.600;
            }
            //print_r($painting_array);
            //exit;

            $painting_array=array();
            $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Forest (%' AND titleEN LIKE '%/VIII)%' ");
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                $tmp=explode("/", $number_extracted[1]);
                $number_extracted=$tmp[0];
                $number_extracted=UTILS::rom2arab($number_extracted);
                //print_r($number_extracted);
                $painting_array[$number_extracted]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.900;
            }
            $sort_number=$sort_number+($values['artwork_sort']*1000000);

        }
        elseif ( $values['artworkid']==6 && ( preg_match("/^MV. /", $values['title_en']) || preg_match("/\.\sMV\.$/", $values['title_en']) || preg_match("/\.\sMV$/", $values['title_en']) ) && empty($values['date']) ) #opp title "MV. " or title "123213 MV." or title "1212 MV"
        {
            $painting_array=array();
            $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND ( titleEN LIKE 'MV. %' OR titleEN LIKE '%. MV.' OR titleEN LIKE '%. MV' ) ");
            while( $row=$db->mysql_array($results) )
            {
            	if( preg_match("/\.\sMV\.$/", $row['titleEN'],$tmp) )
            	{
            		//print "here<br />";
            		//print_r($tmp);
            		$number_extracted=explode(". MV.", $row['titleEN']);
            		//print_r($number_extracted);
            		$painting_array[$number_extracted[0]]=$row;
            	}
            	elseif( preg_match("/\.\sMV$/", $row['titleEN'],$tmp) )
            	{
            		//print "here<br />";
            		//print_r($tmp);
            		$number_extracted=explode(". MV", $row['titleEN']);
            		//print_r($number_extracted);
            		$painting_array[$number_extracted[0]]=$row;
            	}
            	else
            	{
                	$number_extracted=explode("MV. ", $row['titleEN']);
                	$painting_array[$number_extracted[1]]=$row;
            	}
                //print_r($number_extracted);

            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.600;
            }
            $sort_number=$sort_number+($values['artwork_sort']*1000000);
            //print round($sort_number,3);
            //print_r($painting_array);
            //exit;
        }
        elseif ( $values['artworkid']==6 && preg_match("/\/20 hc\.$/", $values['title_en']) ) #opp title "1/20 hc."
        {
            #case 1/20 hc.
            $painting_array=array();
            $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE '%/20 hc.' ");
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted_array=preg_split("/[\s,()]+/",$row['titleEN']);
                $number_extracted=explode("/", $number_extracted_array[1]);
                //print_r($number_extracted);
                $painting_array[$number_extracted[0]]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.900;
            }
            //print_r($painting_array);
            //exit;

            $sort_number=$sort_number+($values['artwork_sort']*1000000);
        }
        elseif ( $values['artworkid']==6 && preg_match("/\/3 Probe\)$/", $values['title_en']) && empty($values['date']) ) #opp title "128 Fotos von einem Bild (Halifax 1978), IV (1/3 Probe)"
        {
            preg_match("/[0-9]*\/[0-9]*/", $values['title_en'], $matches);
            //print_r($matches);
            $tmp=explode("/", $matches[0]);
            if( $tmp[1]==3 ) $additional=0.990;
            else $additional=0.700;
            $sort_number=($tmp[0]*0.001)+$additional;
            $sort_number=$sort_number+($values['artwork_sort']*1000000)+$values['year'];
            //print $tmp[1]."<br />";
            //print $values['title_en']."<br />";
            //print $sort_number."<br />";
            //exit('here1');
        }
        elseif ( $values['artworkid']==6 && preg_match("/^128 Details /", $values['title_en']) && empty($values['date']) ) #opp title "128 Details from a Picture (Halifax 1978), IV (12/128)"
        {
            preg_match("/[0-9]*\/[0-9]*/", $values['title_en'], $matches);
            //print_r($matches);
            $tmp=explode("/", $matches[0]);
            if( $tmp[1]==12 ) $additional=0.900;
            else $additional=0.700;
            $sort_number=($tmp[0]*0.001)+$additional;
            $sort_number=$sort_number+($values['artwork_sort']*1000000)+$values['year'];
            //print $values['title_en']."<br />";
            //print $sort_number."<br />";
            //exit('here2');
        }
        elseif ( $values['artworkid']==6 && preg_match("/^Grauwald \((.+)\/VIII\)/", $values['title_en']) ) # opp title "Grauwald (VIII/VIII)"
        {
            #case Grauwald (VIII/VIII)
            $painting_array=array();
            $results=$db->query("SELECT paintID,titleEN FROM ".TABLE_PAINTING." WHERE artworkID='6' AND titleEN LIKE 'Grauwald (%' AND titleEN LIKE '%/VIII)%' ");
            while( $row=$db->mysql_array($results) )
            {
                $number_extracted=preg_split("/[\s,()]+/",$row['titleEN']);
                $tmp=explode("/", $number_extracted[1]);
                $number_extracted=$tmp[0];
                $number_extracted=UTILS::rom2arab($number_extracted);
                //print_r($number_extracted);
                $painting_array[$number_extracted]=$row;
            }
            ksort($painting_array);
            $painting_array=array_values($painting_array);
            foreach( $painting_array as $key => $row )
            {
                if( $row['paintID']==$values['paintid'] ) $sort_number=$values['year']+($key*0.001)+0.600;
            }

           	$sort_number=$sort_number+($values['artwork_sort']*1000000);

            //print_r($painting_array);
            //exit;
        }
        elseif ( $values['artworkid']==6 && $values['paintid']==18003 ) # Grauwald paintid=18003 "
        {
            $sort_number=$values['year']+0.306;

           	$sort_number=$sort_number+($values['artwork_sort']*1000000);

            //print_r($painting_array);
            //exit;
        }
        elseif( !empty($values['date']) && $values['date']!="0000-00-00" ) # if date field provided
        {
            $tmp=explode("-",$values['date']);
            if( $tmp[1]==2 ) $month=32;
            elseif( $tmp[1]==3 ) $month=32*2;
            elseif( $tmp[1]==4 ) $month=32*3;
            elseif( $tmp[1]==5 ) $month=32*4;
            elseif( $tmp[1]==6 ) $month=32*5;
            elseif( $tmp[1]==7 ) $month=32*6;
            elseif( $tmp[1]==8 ) $month=32*7;
            elseif( $tmp[1]==9 ) $month=32*8;
            elseif( $tmp[1]==10 ) $month=32*9;
            elseif( $tmp[1]==11 ) $month=32*10;
            elseif( $tmp[1]==12 ) $month=32*11;
            $sort_number=$tmp[0]+($values['artwork_sort']*1000000)+(($tmp[1]+$tmp[2]+$month)/1000);
            if( $values['artworkid']==7 && $values['year_drawings']=="1989" ) $sort_number=($values['artwork_sort']*1000000)+89+0.014; # for drawings 1989 year, date yes, no number
            elseif( $values['artworkid']==7 && $values['year_drawings']=="1993" ) $sort_number=($values['artwork_sort']*1000000)+93+0.400; # for drawings 1993 year, date yes, no number
        }
        elseif ( $values['artworkid']==6 && !empty($values['year']) ) # all opp which has year provided
        {
        	$sort_number=$values['year']+($values['artwork_sort']*1000000);

        	# case when year is c. 1992, c. 1999 ....
        	if( preg_match("/^c\. +/", $values['year']) )
        	{
        		$sort_number=$sort_number+0.950;
        		$sort_number=$sort_number+str_replace("c. ", "", $values['year']);
        		//print "here3<br />";
        	}
        	elseif( preg_match("/^Untitled/", $values['title_en']) )
        	{
        		$sort_number=$sort_number+0.900;
        	}
        	else $sort_number=$sort_number+0.800;
        	//print "here2";
        	print $sort_number;
        }
        elseif( !empty($values['year']) ) # if year field provided
        {
            if( $values['artworkid']==7 && $values['year_drawings']=="1965" )
            {
                $sort_number=$values['artwork_sort']*1000000;
                $sort_number=$sort_number+65+0.014; # for drawings 1965 year no number
            }
            elseif( $values['artworkid']==7 && $values['year_drawings']=="1969" )
            {
                $sort_number=$values['artwork_sort']*1000000;
                $sort_number=$sort_number+69+0.012; # for drawings 1965 year no number
            }
            else
            {
                $sort_number=$values['year']+($values['artwork_sort']*1000000);
            }
        }

        //exit;

        if( $values['return_array'] ) return array(0=>$sort_number,1=>$sort_number2);
        else return $sort_number;
    }


    public static function replace_letter_to_number($db,$values)
    {
        $search_lower=array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $search_upper=array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

        $replace_lower=array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26");
        $replace_upper=array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26");

        $text_lower=str_replace($search_lower, $replace_lower, $values['string']);
        $string=str_replace($search_upper, $replace_upper, $text_lower);

        return $string;
    }

    public static function get_news_type($db,$typeid)
    {
        if( $typeid==1 ) $type="Exhibitions";
        elseif( $typeid==2 ) $type="Publications";
        elseif( $typeid==3 ) $type="Talks";
        return $type;
    }

    public static function prepare_sort_from_number($db,$sort,$paintid,$artwork_sort)
    {
        if( $artwork_sort==1 || $artwork_sort==2 || $artwork_sort==3 ) $artwork_sort=1;
        if( empty($sort) )
        {
            $sort_number=($artwork_sort*10000000)+$paintid;
        }
        else
        {
            $sort_number=str_replace("a",".1",$sort);
            $sort_number=str_replace("b",".2",$sort_number);
            $sort_number=str_replace("c",".3",$sort_number);
            $sort_number=str_replace("d",".4",$sort_number);
            $sort_number=str_replace("e",".5",$sort_number);
            $sort_number=str_replace("f",".6",$sort_number);
            $sort_number=str_replace("g",".7",$sort_number);
            $sort_number=str_replace("h",".8",$sort_number);
            $sort_number=str_replace("-",".",$sort_number);
            $sort_number=str_replace("/",".",$sort_number);
            $sort_number=str_replace("..",".",$sort_number);
            $count=substr_count($sort_number,".");
            if( $count>=2 )
            {
                $pos=strpos($sort_number, ".");
                $sort_number=substr($sort_number, 0, $pos);
            }
            $sort_number=($artwork_sort*10000000)+$sort;
        }
        return $sort_number;
        //return number_format($sort_number, 3, '.', '');
    }


    public static function rangeDownload($file) {

        $fp = @fopen($file, 'rb');

        $size   = filesize($file); // File size
        $length = $size;           // Content length
        $start  = 0;               // Start byte
        $end    = $size - 1;       // End byte
        // Now that we've gotten so far without errors we send the accept range header
        /* At the moment we only support single ranges.
         * Multiple ranges requires some more work to ensure it works correctly
         * and comply with the spesifications: http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
         *
         * Multirange support annouces itself with:
         * header('Accept-Ranges: bytes');
         *
         * Multirange content must be sent with multipart/byteranges mediatype,
         * (mediatype = mimetype)
         * as well as a boundry header to indicate the various chunks of data.
         */
        header("Accept-Ranges: 0-$length");
        // header('Accept-Ranges: bytes');
        // multipart/byteranges
        // http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
        if (isset($_SERVER['HTTP_RANGE'])) {

            $c_start = $start;
            $c_end   = $end;
            // Extract the range string
            list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
            // Make sure the client hasn't sent us a multibyte range
            if (strpos($range, ',') !== false) {

                // (?) Shoud this be issued here, or should the first
                // range be used? Or should the header be ignored and
                // we output the whole content?
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes $start-$end/$size");
                // (?) Echo some info to the client?
                exit;
            }
            // If the range starts with an '-' we start from the beginning
            // If not, we forward the file pointer
            // And make sure to get the end byte if spesified
            if ($range0 == '-') {

                // The n-number of the last bytes is requested
                $c_start = $size - substr($range, 1);
            }
            else {

                $range  = explode('-', $range);
                $c_start = $range[0];
                $c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
            }
            /* Check the range and make sure it's treated according to the specs.
             * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
             */
            // End bytes can not be larger than $end.
            $c_end = ($c_end > $end) ? $end : $c_end;
            // Validate the requested range and return an error if it's not correct.
            if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {

                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes $start-$end/$size");
                // (?) Echo some info to the client?
                exit;
            }
            $start  = $c_start;
            $end    = $c_end;
            $length = $end - $start + 1; // Calculate new content length
            fseek($fp, $start);
            header('HTTP/1.1 206 Partial Content');
        }
        // Notify the client the byte range we'll be outputting
        header("Content-Range: bytes $start-$end/$size");
        header("Content-Length: $length");

        // Start buffered download
        $buffer = 1024 * 8;
        while(!feof($fp) && ($p = ftell($fp)) <= $end) {

            if ($p + $buffer > $end) {

                // In case we're only outputtin a chunk, make sure we don't
                // read past the length
                $buffer = $end - $p + 1;
            }
            set_time_limit(0); // Reset time limit for big files
            echo fread($fp, $buffer);
            flush(); // Free up memory. Otherwise large files will trigger PHP's memory limit.
        }

        fclose($fp);

    }

    public static function itemid($db,$titleurl)
    {
        if( is_numeric($titleurl) ) $itemid=$titleurl;
        else
        {
            $tmp=explode("-",$titleurl);
            //print_r($tmp);
            //print "|".count($tmp)."|";
            $itemid=$tmp[(count($tmp)-1)];
            if( !is_numeric($itemid) ) unset($itemid);
            //if( empty($itemid) ) $itemid=intval($titleurl);
        }

        $itemid=preg_replace("/[^0-9]/", "", $itemid);

        return $itemid;
    }

        public static function prepare_nicedit_output($db,$text)
        {
            $replaced_text=str_replace("&nbsp;","",$text);
            return $replaced_text;
        }

    public static function get_painting_url($db,$options)
    {
    	$query_painting="SELECT p.paintID,p.catID AS p_categoryid,p.artworkID AS p_artworkid,p.titleurl AS p_titleurl, a.titleurl AS a_titleurl, edition_individual FROM ".TABLE_PAINTING." p
    		LEFT JOIN ".TABLE_ARTWORKS." a ON p.artworkID=a.artworkID
    		WHERE p.paintID='".$options['paintid']."'
    		LIMIT 1
    	";
        $results_painting=$db->query($query_painting);
        $count_painting=$db->numrows($results_painting);
        if( $count_painting>0 )
        {
      		$row_painting=$db->mysql_array($results_painting,0);
			//print_r($row_painting);

      		if( $options['painting_no_section'] )
      		{
      			$url="/".$row_painting['p_titleurl'];
      		}
      		else
      		{
      			if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
      			elseif( empty($_GET['lang']) ) $_GET['lang']="en";

      			$url="/".$_GET['lang']."/art";

      			# for photo-paintings, abstracts, paintings/other
	            //if( $row_painting['p_artworkid']==1 || $row_painting['p_artworkid']==2 || $row_painting['p_artworkid']==13 )
	            if( $row_painting['p_artworkid']==1 || $row_painting['p_artworkid']==2 )
	            {
	                $url.="/paintings";
	            }
	            # END for photo-paintings, abstracts, paintings/other

	        	$url.="/".$row_painting['a_titleurl'];

	            # for edition_individual
	            if( $row_painting['edition_individual'] && $row_painting['p_artworkid']==4 )
	            {
	                $results_edition=$db->query("SELECT p.paintID FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=1 AND r.itemid2='".$row_painting['paintID']."' AND r.typeid2=25 ) OR ( r.typeid2=1 AND r.itemid1='".$row_painting['paintID']."' AND r.typeid1=25 ) ) AND ( ( r.itemid1=p.paintID AND r.typeid1=1 ) OR ( r.itemid2=p.paintID AND r.typeid2=1 ) ) LIMIT 1 ");
	                $row_edition=$db->mysql_array($results_edition,0);

                    $options_paint_url=array();
                    $options_paint_url['painting_no_section']=1;
                    $options_paint_url['paintid']=$row_edition['paintID'];
                    $edition_url=UTILS::get_painting_url($db,$options_paint_url);
                    $url.=$edition_url['url'];

	            	$url.="/individual-works";
	            }
	            # END for edition_individual

	        	if( $row_painting['p_artworkid']==1 || $row_painting['p_artworkid']==2 || $row_painting['p_artworkid']==13 || $row_painting['p_artworkid']==6 )
	        	{
	                $query_where_category=" WHERE catID='".$row_painting['p_categoryid']."' AND enable=1 ";
	                $query_limit_category=" LIMIT 1 ";
	                $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
	                $results_category=$db->query($query_category['query']);
	                $count_category=$db->numrows($results_category);
	                if( $count_category>0 )
	                {
	                    $row_category=$db->mysql_array($results_category);

	                    if( !empty($row_category['sub_catID']) )
	                    {
	                        $query_where_category=" WHERE catID='".$row_category['sub_catID']."' AND enable=1 ";
	                        $query_limit_category=" LIMIT 1 ";
	                        $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category,$query_limit_category);
	                        $results_sub_category=$db->query($query_category['query']);
	                        $count_sub_category=$db->numrows($results_sub_category);
	                        if( $count_sub_category>0 )
	                        {
	                            $row_sub_category=$db->mysql_array($results_sub_category);
	                        }
	                    }

	                }
	                if( !empty($row_sub_category['titleurl']) ) $url.="/".$row_sub_category['titleurl'];
	                if( !empty($row_category['titleurl']) ) $url.="/".$row_category['titleurl'];
	            }
	            $url.="/".$row_painting['p_titleurl'];
        	}

        	$painting_url['url']=$url;
        	$painting_url['found']==1;
    	}
    	else
    	{
    		$painting_url['found']==0;
    	}

        return $painting_url;
    }


    public static function get_literature_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_where_books=" WHERE id='".$values['bookid']."' ";
        $query_books=QUERIES::query_books($db,$query_where_books);
        $results=$db->query($query_books['query']);
        $count=$db->numrows($results);
        if( $count )
        {
        	$row=$db->mysql_array($results,0);

            $values_book_cat_url=array();
            $values_book_cat_url['books_catid']=$row['books_catid'];
            $url_literature_cat=UTILS::get_literature_cat_url($db,$values_book_cat_url);

            $values_return=array();
            if( ( $row['books_catid']==6 || $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 ) && !empty($row['link']) )
            {
                $values_return['url']=$row['link'];
                $values_return['target']="_blank";
                /*
                $values_return['url']="/links/articles";
                $query_where_articles_cat=" WHERE articles_catid='".$row['articles_catid']."' ";
                $query_articles_cat=QUERIES::query_articles_categories($db,$query_where_articles_cat);
                $results_articles_cat=$db->query($query_articles_cat['query']);
                $row_articles_cat=$db->mysql_array($results_articles_cat,0);
                $sectionurl=UTILS::row_text_value($db,$row_articles_cat,"titleurl");
                $decade=UTILS::round_down_to_integer($row['date_year']);
                $values_return['url'].="?decade=".$decade."&year=".$row['date_year']."#".$row['id'];
                 */
            }
            else
            {
                if( $values['url_main_page'] )
                {
                	$values_return['url']="/".$_GET['lang']."/literature/".$row['titleurl'];
                    //$values_return['url']="/literature/".$row_category['titleurl']."/".$row['titleurl'];
                }
                else
                {

                    $values_return['url']=$url_literature_cat['url']."/".$row['titleurl'];
                }
            }
        }

        return $values_return;
    }

    public static function get_literature_cat_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_where_books=" WHERE books_catid='".$values['books_catid']."' ";
        $query_books=QUERIES::query_books_categories($db,$query_where_books);
        $results=$db->query($query_books['query']);
        $count=$db->numrows($results);
        if( $count )
        {
        	$row=$db->mysql_array($results,0);
        	if( !empty($row['sub_books_catid']) )
        	{
        		$row_sub=$row;
        		$count_sub=1;
         		$query_where_books=" WHERE books_catid='".$row['sub_books_catid']."' ";
        		$query_books=QUERIES::query_books_categories($db,$query_where_books);
        		$results=$db->query($query_books['query']);
        		$row=$db->mysql_array($results,0);
        	}

            $url="/".$_GET['lang']."/literature/".$row['titleurl'];

            /*
            $query_category_sub_where=" WHERE enable=1 AND sub_books_catid='".$row['books_catid']."' ";
            $query_category_sub_order=" ORDER BY sort ";
            if( $row['books_catid']==16 ) $query_category_sub_order.=" DESC ";
            $query_category_sub=QUERIES::query_books_categories($db,$query_category_sub_where,$query_category_sub_order);
            $results_category_sub=$db->query($query_category_sub['query']);
            $count_sub=$db->numrows($results_category_sub);
			*/

            if( $count_sub>0 )
            {
            	//$row_sub=$db->mysql_array($results_category_sub);
            	$url.="/".$row_sub['titleurl'];
            }
            else
            {
            	/*
	            $query_categories_sub_where=" WHERE enable=1 AND sub_books_catid='".$row['books_catid']."' ";
	            $query_categories_sub_order=" ORDER BY sort ";
	            if( $row['books_catid']==16 ) $query_categories_sub_order.=" DESC ";
	            $query_categories_sub=QUERIES::query_books_categories($db,$query_categories_sub_where,$query_categories_sub_order," LIMIT 1 ");
	            $results_categories_sub=$db->query($query_categories_sub['query']);
	            $count_sub_categories=$db->numrows($results_categories_sub);
	            $row_sub_categories=$db->mysql_array($results_categories_sub);
	            if( $count_sub_categories ) $url.="/".$row_sub_categories['titleurl'];
	            */
        	}

        }

        $values_return['url']=$url;

        return $values_return;
    }

    public static function get_video_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $values_return['url']="/".$_GET['lang']."/videos";
        $query_where_video=" WHERE videoID='".$values['videoid']."' ";
        $query_video=QUERIES::query_videos($db,$query_where_video);
        $results_video=$db->query($query_video['query']);
        $count=$db->numrows($results_video);
        if( $count )
        {
            $row=$db->mysql_array($results_video);
            $query_where_cat=" WHERE categoryid='".$row['categoryid']."' ";
            $query_cat=QUERIES::query_videos_categories($db,$query_where_cat);
            $results_cat=$db->query($query_cat['query']);
            $count_cat=$db->numrows($results_cat);
            $row_cat=$db->mysql_array($results_cat);
            $values_return['url'].="/".$row_cat['titleurl']."/".$row['titleurl'];
        }

        return $values_return;
    }

    public static function get_video_cat_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_categories="SELECT * FROM ".TABLE_VIDEO_CATEGORIES." WHERE enable=1 AND ";
        $query_categories.=" categoryid='".$values['categoryid']."' ";
        $results=$db->query($query_categories);
        $count=$db->numrows($results);
        if( $count )
        {
        	$row=$db->mysql_array($results,0);
        	if( !empty($row['sub_categoryid']) )
        	{
        		$row_sub=$row;
        		$count_sub=1;
		        $query_categories="SELECT * FROM ".TABLE_VIDEO_CATEGORIES." WHERE enable=1 AND ";
		        $query_categories.=" categoryid='".$row['sub_categoryid']."' ";
		        $results=$db->query($query_categories);
        		$row=$db->mysql_array($results,0);
        	}

            $url="/".$_GET['lang']."/videos/".$row['titleurl'];

            if( $count_sub>0 )
            {
            	//$row_sub=$db->mysql_array($results_category_sub);
            	$url.="/".$row_sub['titleurl'];
            }
            /*
            else
            {
	            $query_categories_sub_where=" WHERE enable=1 AND sub_books_catid='".$row['books_catid']."' ";
	            $query_categories_sub_order=" ORDER BY sort ";
	            if( $row['books_catid']==16 ) $query_categories_sub_order.=" DESC ";
	            $query_categories_sub=QUERIES::query_books_categories($db,$query_categories_sub_where,$query_categories_sub_order," LIMIT 1 ");
	            $results_categories_sub=$db->query($query_categories_sub['query']);
	            $count_sub_categories=$db->numrows($results_categories_sub);
	            $row_sub_categories=$db->mysql_array($results_categories_sub);
	            if( $count_sub_categories ) $url.="/".$row_sub_categories['titleurl'];
        	}
        	*/

        }

        $values_return['url']=$url;

        return $values_return;
    }

    public static function get_quote_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_where_quote=" WHERE quoteid='".$values['quoteid']."' ";
        $query_quote=QUERIES::query_quotes($db,$query_where_quote,$query_order,$query_limit);
        $results=$db->query($query_quote['query']);
        $count=$db->numrows($results);

        if( $count>0 )
        {
            $row=$db->mysql_array($results,0);
            $query_where_category=" WHERE quotes_categoryid='".$row['quotes_categoryid']."' ";
            $query_limit_category=" LIMIT 1 ";
            $query_category=QUERIES::query_quotes_categories($db,$query_where_category,$query_order_category,$query_limit_category);
            $results_category=$db->query($query_category['query']);
            $count_category=$db->numrows($results_category);
            if( $count_category>0 )
            {
                $row_category=$db->mysql_array($results_category);
                $titleurl_category=UTILS::row_text_value($db,$row_category,"titleurl");
            }
            $url="/".$_GET['lang']."/quotes";
            if( !empty($titleurl_category) ) $url.="/".$titleurl_category;
            if( $values['location'] ) $url.="#".$row['quoteid'];
        }

        $quote_url['url']=$url;

        return $quote_url;
    }

    public static function get_quote_cat_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_categories="SELECT * FROM ".TABLE_QUOTES_CATEGORIES." WHERE enable=1 AND ";
        $query_categories.=" quotes_categoryid='".$values['quotes_categoryid']."' ";
        $results=$db->query($query_categories);
        $count=$db->numrows($results);
        if( $count )
        {
        	$row=$db->mysql_array($results,0);
        	if( !empty($row['sub_categoryid']) )
        	{
        		$row_sub=$row;
        		$count_sub=1;
		        $query_categories="SELECT * FROM ".TABLE_QUOTES_CATEGORIES." WHERE enable=1 AND ";
		        $query_categories.=" quotes_categoryid='".$row['sub_categoryid']."' ";
		        $results=$db->query($query_categories);
        		$row=$db->mysql_array($results,0);
        	}

            $url="/".$_GET['lang']."/quotes/".$row['titleurl'];

            if( $count_sub>0 )
            {
            	//$row_sub=$db->mysql_array($results_category_sub);
            	$url.="/".$row_sub['titleurl'];
            }
            /*
            else
            {
	            $query_categories_sub_where=" WHERE enable=1 AND sub_books_catid='".$row['books_catid']."' ";
	            $query_categories_sub_order=" ORDER BY sort ";
	            if( $row['books_catid']==16 ) $query_categories_sub_order.=" DESC ";
	            $query_categories_sub=QUERIES::query_books_categories($db,$query_categories_sub_where,$query_categories_sub_order," LIMIT 1 ");
	            $results_categories_sub=$db->query($query_categories_sub['query']);
	            $count_sub_categories=$db->numrows($results_categories_sub);
	            $row_sub_categories=$db->mysql_array($results_categories_sub);
	            if( $count_sub_categories ) $url.="/".$row_sub_categories['titleurl'];
        	}
        	*/

        }

        $values_return['url']=$url;

        return $values_return;
    }

    public static function get_news_url($db,$values)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

        $query_where_news=" WHERE newsID='".$values['newsid']."' ";
        $query_news=QUERIES::query_news($db,$query_where_news,$query_order,$query_limit);
        $results=$db->query($query_news['query']);
        $count=$db->numrows($results);

        if( $count>0 )
        {
            $row=$db->mysql_array($results,0);
            # Exhibitions
            if( $row['type']==1 )
            {
                $current_date=date("Y-m-d");
                if( $current_date<$row['datefrom_admin'] && $current_date<$row['dateto_admin'] ) $titleurl="upcoming";
                elseif( $current_date>=$row['datefrom_admin'] && $current_date<=$row['dateto_admin'] ) $titleurl="";
            }
            # Publications
            elseif( $row['type']==2 )
            {
                $titleurl="publications";
            }
            # Talks
            elseif( $row['type']==3 )
            {
                $titleurl="talks";
            }
            $url="/".$_GET['lang']."/news";
            if( !empty($titleurl) ) $url.="/".$titleurl;
            if( $values['location'] ) $url.="#".$row['newsID'];
        }

        $news_url['url']=$url;

        return $news_url;
    }


    public static function get_exhibition_url($db,$options)
    {
		if( !empty($options['lang']) ) $_GET['lang']=$options['lang'];
		elseif( empty($_GET['lang']) ) $_GET['lang']="en";

		if( !empty($options['installationid']) )
		{
            $results_exhibition_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=5 AND itemid1='".$options['installationid']."' AND typeid2=4 ) OR ( typeid2=5 AND itemid2='".$options['installationid']."' AND typeid1=4 ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
            $row_exhibition_relations=$db->mysql_array($results_exhibition_relations);
            $options['exhibitionid']=UTILS::get_relation_id($db,"4",$row_exhibition_relations);
		}

        $results=$db->query("SELECT titleurl FROM ".TABLE_EXHIBITIONS." WHERE exID='".$options['exhibitionid']."' ");
        $row=$db->mysql_array($results);
        $exh_url="/".$_GET['lang']."/exhibitions/".$row['titleurl'];
        if( !empty($options['get_params']) )
        {
            $exh_url.=$options['get_params'];
        }

        if( !empty($options['installationid']) )
        {
        	$exh_url.="/?tab=installation-views-tabs";
        }

        return $exh_url;
    }

    public static function get_painting_title($db,$options)
    {
        if( !empty($options['title']) ) $name_title=$options['title'];
        else $name_title="title";

        # English
        $name_title_new_en=$name_title."_en";
        $name_title_old_en=$name_title."EN";
        if( !empty($options['row'][$name_title_new_en]) ) $title_en=$options['row'][$name_title_new_en];
        else $title_en=$options['row'][$name_title_old_en];
        if( $options['add_html'] ) $title_en=UTILS::add_html($title_en);

        //print $options['row'][$name_title_new_en];
        //print $options['row'][$name_title_old_en];

        # German
        $name_title_new_de=$name_title."_de";
        $name_title_old_de=$name_title."DE";
        if( !empty($options['row'][$name_title_new_de]) ) $title_de=$options['row'][$name_title_new_de];
        else $title_de=$options['row'][$name_title_old_de];
        if( $options['add_html'] ) $title_de=UTILS::add_html($title_de);

        # French
        $name_title_new_fr=$name_title."_fr";
        $name_title_old_fr=$name_title."FR";
        if( !empty($options['row'][$name_title_new_fr]) ) $title_fr=$options['row'][$name_title_new_fr];
        else $title_fr=$options['row'][$name_title_old_fr];
        if( $options['add_html'] ) $title_fr=UTILS::add_html($title_fr);

        # Italian
        $name_title_new_it=$name_title."_it";
        $name_title_old_it=$name_title."IT";
        if( !empty($options['row'][$name_title_new_it]) ) $title_it=$options['row'][$name_title_new_it];
        else $title_it=$options['row'][$name_title_old_it];
        if( $options['add_html'] ) $title_it=UTILS::add_html($title_it);

        # Chinese
        $name_title_new_zh=$name_title."_zh";
        $name_title_old_zh=$name_title."ZH";
        if( !empty($options['row'][$name_title_new_zh]) ) $title_zh=$options['row'][$name_title_new_zh];
        else $title_zh=$options['row'][$name_title_old_zh];
        if( $options['add_html'] ) $title_zh=UTILS::add_html($title_zh);

        # All other languages
        $lang_title_old=$name_title.strtoupper($_GET['lang']);
        $lang_title_new=$name_title."_".$_GET['lang'];
        if( !empty($options['row'][$lang_title_new]) ) $title=$options['row'][$lang_title_new];
        else $title=$options['row'][$lang_title_old];
        if( $options['add_html'] ) $title=UTILS::add_html($title);


        # if searching returning language value in which search tearm was found
        if( !empty($options['search']) )
        {
        	/*
            if( stripos($title_en,$options['search']) ) $title_found=$title_en;
            elseif( stripos($title_de,$options['search']) ) $title_found=$title_de;
            elseif( stripos($title_fr,$options['search']) ) $title_found=$title_fr;
            elseif( stripos($title_it,$options['search']) ) $title_found=$title_it;
            elseif( stripos($title_zh,$options['search']) ) $title_found=$title_zh;
            */
            if( preg_match("/".$options['search']."/i", $title_en) ) $title_found=$title_en;
            elseif( preg_match("/".$options['search']."/i", $title_de) ) $title_found=$title_de;
            elseif( preg_match("/".$options['search']."/i", $title_fr) ) $title_found=$title_fr;
            elseif( preg_match("/".$options['search']."/i", $title_it) ) $title_found=$title_it;
            elseif( preg_match("/".$options['search']."/i", $title_zh) ) $title_found=$title_zh;
        }
        # end

        if( !$options['class']['span-painting-title'] )
        {
            $class_span_painting_title1="span-painting-title1";
            $class_span_painting_title2="span-painting-title2";
        }

        $title_return="";

        if( $_GET['lang']=="en" || $_GET['lang']=="de" )
        {
        	//print "|".$title_de."|".$title_en."|".$title_found."<br />";
            if( !empty($title_de) )
            {
                $title_return.="<span class='".$class_span_painting_title1."'>".$title_de."</span>";
            }
            if( !empty($title_found) && $title_found!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title_found."</span>";
            elseif( !empty($title_en) && $title_en!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title_en."</span>";
        }
        else if( $_GET['lang']=="fr" )
        {
            if( !empty($title_de) )
            {
                $title_return.="<span class='".$class_span_painting_title1."'>".$title_de."</span>";
            }
            if( !empty($title_found) && $title_found!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title_found."</span>";
            elseif( !empty($title) && $title!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title."</span>";
        }
        else if( $_GET['lang']=="it" )
        {
            if( !empty($title_de) )
            {
                $title_return.="<span class='".$class_span_painting_title1."'>".$title_de."</span>";
            }
            if( !empty($title_found) && $title_found!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title_found."</span>";
            elseif( !empty($title) && $title!=$title_de ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title."</span>";
        }
        else if( $_GET['lang']=="zh" )
        {
            if( !empty($title_en) )
            {
                $title_return.="<span class='".$class_span_painting_title1."'>".$title_en."</span>";
            }
            if( !empty($title_found) && $title_found!=$title_en ) $title_return.="<br /><span class='".$class_span_painting_title2."'>".$title_found."</span>";
            elseif( !empty($title) && $title!=$title_en )
            {
            	if( !empty($title_en) ) $title_return.="<br />";
            	$title_return.="<span class='".$class_span_painting_title2."'>".$title."</span>";
            }
            if( empty($title_en) && empty($title_zh) && !empty($title_de) ) $title_return.="<span class='".$class_span_painting_title1."'>".$title_de."</span>";
        }

        if( !empty($options['search']) )
        {
            $options_search_found=array();
            $options_search_found['search']=$options['search'];
            $options_search_found['text']=$title_return;
            $options_search_found['strip_tags']=1;
            $options_search_found['a_name']=1;
            $title_return=UTILS::show_search_found_keywords($db,$options_search_found);
        }

        //print_r($options);
        //print $title_return;exit;

        if( $options['return'] || $options['html_return'] ) return $title_return;
        else print $title_return;

    }

            public static function array_nsearch($needle, array $haystack) {
                   $it = new IteratorIterator(new ArrayIterator($haystack));
                      foreach($it as $key => $val) {
                                 //if(strcasecmp($val,$needle) === 0) {
                                 if(stripos($val, $needle) === false) {
                                                return false;
                                                       }
                                 else return $key;
                                    }
                      return false;
            }


    public static function show_search_found_keywords($db,$values)
    {
        //if( !empty($values['class']['span-keyword-found2']) ) $class=$values['class']['span-keyword-found2'];
        if( !empty($values['class']['span-keyword-found2']) ) $class="f1o1u1n1d12";
        else $class="f1o1u1n1d1";


		if( !empty($values['class']['span_keyword_found']) ) $class_span_keyword_found=$values['class']['span_keyword_found'];
		else $class_span_keyword_found="span-keyword-found";

		$highlight = '<span class="'.$class_span_keyword_found.'">\1</span>';

		if( !$values['a_name'] )
	    {
			$highlight = '<a name="replace_this_whith_search_keyword" class="a-found-search-highlight">'.$highlight.'</a>';
		}

        //$text=str_replace("<br>","\n",$text);
        //$text=str_replace("<br\>","\n",$text);
        if( !$values['strip_tags'] )
        {
        	$text=$values['text'];
            $text=strip_tags($text);
            $text=preg_replace('/\&nbsp\;/', '', $text);
            $text=preg_replace("/[[:blank:]]+/"," ",$text);
        }
        else $text=$values['text'];

        //$text = html_entity_decode($text);

        if( strlen($values['text'])>(strlen($values['search'])+180) && $values['type'] )
        {
            $values['search']=str_replace('"',"",$values['search']);
            $dots=1;
            $text=nl2br($text);
            $text=strip_tags($text, '<em><i><strong><b><u><sup><sub><hr>');

            //$search_words=explode(" ",$values['search']);
            $search_words = preg_split("/[\s,.]+/", $values['search']);

            # cut out text paragraph with found keywords
            $limit=60;
            //print $text;
            //print $search_what;
            $search= new Search($text);
            $found=$search->find($search_words,$limit,$limit);
            //$found=$search->find("Düsseldorf",$limit,$limit);
            //print_r($found);
            if( !empty($found['found']) ) $text=$found['found'];
            else $text=substr($text,0,$limit*15);

            $matches_em1=substr_count($text, "<em>");
            $matches_em2=substr_count($text, "</em>");
            if( $matches_em1!=$matches_em2 ) $text.="</em>";
            # END cut out text paragraph with found keywords


            //print $text;
            //print $pattern;
            //print_r($matches);
            //exit;
            # END cut out text paragraph with found keywords



$options_highlight=array();
$options_highlight=$values;
$options_highlight['STR_HIGHLIGHT_SIMPLE']=1;
//$options_highlight['STR_HIGHLIGHT_WHOLEWD']=1;
//$options_highlight['STR_HIGHLIGHT_CASESENS']=1;
//$options_highlight['STR_HIGHLIGHT_STRIPLINKS']=1;
$text=UTILS::str_highlight($text, $search_words, $options_highlight, $highlight);

            if( $values['show_dots'] )
            {
                if( $dots ) $text="[...] ".$text;
                if( $dots ) $text=$text." [...]";
            }
            //$text=str_ireplace($values['search'],"<span class='".$class."'>".$values['search']."</span>",$text);
        }
        else
        {
            if( !empty($values['search']) && !is_numeric($values['search']) )
            {
                //$allowed = "/[^a-z\\040\\.\\-\/]/i";
                //$values['search']=trim(preg_replace($allowed,"",$values['search']));
                //print trim($values['search'])."-";

                $search_words = preg_split("/[\s,.]+/", $values['search']);





//$text = html_entity_decode($text);
$options_highlight=array();
$options_highlight=$values;
$options_highlight['STR_HIGHLIGHT_SIMPLE']=1;
//$options_highlight['STR_HIGHLIGHT_WHOLEWD']=1;
//$options_highlight['STR_HIGHLIGHT_CASESENS']=1;
//$options_highlight['STR_HIGHLIGHT_STRIPLINKS']=1;
$text=UTILS::str_highlight($text, $search_words, $options_highlight, $highlight);



            }
            //print UTILS::strip_slashes_recursive($search_word)."-".$text."<br />";
        }

        //$text=nl2br($text);
        //$text=htmlentities($text);
        //$text=htmlentities($text, ENT_QUOTES, 'UTF-8');
        //$text=htmlspecialchars($text, ENT_QUOTES);
        //$text=html_entity_decode($text);
        //$text=str_replace("&ndash","&#8211;",$text);

        return $text;

    }




/**
 * Highlight a string in text without corrupting HTML tags
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     3.1.1
 * @link        http://aidanlister.com/2004/04/highlighting-a-search-string-in-html-text/
 * @param       string          $text           Haystack - The text to search
 * @param       array|string    $needle         Needle - The string to highlight
 * @param       bool            $options        Bitwise set of options
 * @param       array           $highlight      Replacement string
 * @return      Text with needle highlighted
 */
public static function str_highlight($text, $needle, $options = null, $highlight = null)
{
	/**
	 * Perform a simple text replace
	 * This should be used when the string does not contain HTML
	 * (off by default)
	 */
	//define('STR_HIGHLIGHT_SIMPLE', 1);

	/**
	 * Only match whole words in the string
	 * (off by default)
	 */
	//define('STR_HIGHLIGHT_WHOLEWD', 2);

	/**
	 * Case sensitive matching
	 * (off by default)
	 */
	//define('STR_HIGHLIGHT_CASESENS', 4);

	/**
	 * Overwrite links if matched
	 * This should be used when the replacement string is a link
	 * (off by default)
	 */
	//define('STR_HIGHLIGHT_STRIPLINKS', 8);

    // Default highlighting
    if ($highlight === null) {
        $highlight = '<strong>\1</strong>';
    }

    // Select pattern to use
    if (!$options['STR_HIGHLIGHT_SIMPLE']) {
        $pattern = '#(%s)#';
        $sl_pattern = '#(%s)#';
    } else {
        $pattern = '#(?!<.*?)(%s)(?![^<>]*?>)#';
        $sl_pattern = '#<a\s(?:.*?)>(%s)</a>#';
    }

    // Case sensitivity
    if (!($options['STR_HIGHLIGHT_CASESENS'])) {
        $pattern .= 'i';
        $sl_pattern .= 'i';
    }

    $needle = (array) $needle;
    foreach ($needle as $needle_s) {
    	if( strlen($needle_s)>1 )
    	{
	        if( !$options['a_name'] )
	        {
	        	$highlight=str_replace("replace_this_whith_search_keyword", $needle_s, $highlight);
	        	//$highlight="<a name='".$needle_s."' class='a-found-search-highlight'>".$highlight."</a>";
	    	}

	        $needle_s = preg_quote($needle_s);

	        // Escape needle with optional whole word check
	        if ($options['STR_HIGHLIGHT_WHOLEWD']) {
	            $needle_s = '\b' . $needle_s . '\b';
	        }

	        // Strip links
	        if ($options['STR_HIGHLIGHT_STRIPLINKS']) {
	            $sl_regex = sprintf($sl_pattern, $needle_s);
	            $text = preg_replace($sl_regex, '\1', $text);
	        }

	        $regex = sprintf($pattern, $needle_s);
	        $text = preg_replace($regex, $highlight, $text);
    	}
    }

    return $text;
}




    public static function str_highlight2($text, $needle, $options = null, $highlight = null)
    {
    $original_text = $text;
    $original_needle = $needle;

    // Normalize both text and needle to get rid of the accents e.g. “être humain” becomes “etre humain”
    $text = UTILS::strip_accents($text);
    $needle = UTILS::strip_accents($needle);

    // Default highlighting
    if ($highlight === null) {
    $highlight = '\1';
    }

    // Select pattern to use
    if ($options & $options['STR_HIGHLIGHT_SIMPLE']) {
    $pattern = '#(%s)#';
    $sl_pattern = '#(%s)#';
    } else {
    $pattern = '#(?!<.*?)(%s)(?![^]*?>)#';
    $sl_pattern = '#(%s)#';
    }

    // Case sensitivity
    if (!($options & $options['STR_HIGHLIGHT_CASESENS'])) {
    $pattern .= 'i';
    $sl_pattern .= 'i';
    }

    $needle = (array) $needle;
    foreach ($needle as $needle_s) {
    $needle_s = preg_quote($needle_s);

    // Escape needle with optional whole word check
    if ($options & $options['STR_HIGHLIGHT_WHOLEWD']) {
    $needle_s = '\b' . $needle_s . '\b';
    }

    // Strip links
    if ($options & $options['STR_HIGHLIGHT_STRIPLINKS']) {
    $sl_regex = sprintf($sl_pattern, $needle_s);
    $text = preg_replace($sl_regex, '\1', $text);
    }

    $regex = sprintf($pattern, $needle_s);
    $text = preg_replace($regex, $highlight, $text);
    }

    $opening_tag = substr($highlight,0,strpos($highlight,'\\1'));
    $closing_tag =substr($highlight,strpos($highlight,'\\1')+2);

    // Now the text contains the highlighted version of the text but without accents
    // We need to set the same highlighting but in the original text with accents

    $currPos = 0; $count = 0; $len_open = strlen($opening_tag); $len_close = strlen($closing_tag);
    $original_length = mb_strlen($original_text);
    while (true) {
    $start_pos = mb_strpos( $text, $opening_tag, $currPos);
    if ($start_pos === FALSE) break;
    $stop_pos = mb_strpos($text, $closing_tag, $start_pos +1);

    if ($stop_pos === FALSE) $stop_pos = mb_strlen($text);
    $original_text=mb_substr($original_text,0, $start_pos) . $opening_tag . mb_substr($original_text,$start_pos, $stop_pos - $start_pos - $len_open) . $closing_tag . mb_substr($original_text, $stop_pos - $len_open );
    $currPos = $stop_pos;

    $count++;
    }

    // This can be useful for debugging
    // echo “length should be ” . ($original_length + $count* ($len_open + $len_close)) . “==” . mb_strlen($text) . ” and is ” . mb_strlen($original_text) . “”;
    return $original_text;
    }



    public static function strip_accents($string) {

    // http://patisserie.keensoftware.com/en/pages/gerer-les-accents-dans-les-recherches-textes
    // https://github.com/cakephp/cakephp/blob/master/lib/Cake/Utility/Inflector.php
    /**
    * Default map of accented and special characters to ASCII characters
    *
    * @var array
    */
    $map = array(
    '/æ|ǽ/' => 'ae',
    '/ü/' => 'u',
    '/Ä/' => 'A',
    '/Ü/' => 'U',
    '/Ö/' => 'O',
    '/À|Á|Â|Ã|Å|Ǻ|Ā|Ă|Ą|Ǎ/' => 'A',
    '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ä|ª/' => 'a',
    '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
    '/ç|ć|ĉ|ċ|č/' => 'c',
    '/Ð|Ď|Đ/' => 'D',
    '/ð|ď|đ/' => 'd',
    '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/' => 'E',
    '/è|é|ê|ë|ē|ĕ|ė|ę|ě/' => 'e',
    '/Ĝ|Ğ|Ġ|Ģ/' => 'G',
    '/ĝ|ğ|ġ|ģ/' => 'g',
    '/Ĥ|Ħ/' => 'H',
    '/ĥ|ħ/' => 'h',
    '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ/' => 'I',
    '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı/' => 'i',
    '/Ĵ/' => 'J',
    '/ĵ/' => 'j',
    '/Ķ/' => 'K',
    '/ķ/' => 'k',
    '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
    '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
    '/Ñ|Ń|Ņ|Ň/' => 'N',
    '/ñ|ń|ņ|ň|ŉ/' => 'n',
    '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ/' => 'O',
    '/ò|ó|ô|õ|ö|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º/' => 'o',
    '/Ŕ|Ŗ|Ř/' => 'R',
    '/ŕ|ŗ|ř/' => 'r',
    '/Ś|Ŝ|Ş|Ș|Š/' => 'S',
    '/ś|ŝ|ş|ș|š|ſ/' => 's',
    '/Ţ|Ț|Ť|Ŧ/' => 'T',
    '/ţ|ț|ť|ŧ/' => 't',
    '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
    '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ|ü/' => 'u',
    '/Ý|Ÿ|Ŷ/' => 'Y',
    '/ý|ÿ|ŷ/' => 'y',
    '/Ŵ/' => 'W',
    '/ŵ/' => 'w',
    '/Ź|Ż|Ž/' => 'Z',
    '/ź|ż|ž/' => 'z',
    '/Æ|Ǽ/' => 'AE',
    '/ß/' => 'ss',
    '/Ĳ/' => 'IJ',
    '/ĳ/' => 'ij',
    '/Œ/' => 'OE',
    '/ƒ/' => 'f'
    );
    return preg_replace(array_keys($map), array_values($map), $string);

    }






	public static function painting_size_info($db,$size_typeid,$options=array())
	{
	    $results=$db->query("SELECT * FROM ".TABLE_PAINTING_SIZE_TYPES." WHERE size_typeid='".$size_typeid."' LIMIT 1 ");
	    $row=$db->mysql_array($results);
        $row=UTILS::html_decode($row);
        $title=UTILS::row_text_value($db,$row,"title");

	    return $title;
	}

    public static function get_cook_cover($db,$values)
    {
        if( $values['coverid']==1 ) $cover=LANG_LITERATURE_COVER_HARDBACK;
        elseif( $values['coverid']==2 ) $cover=LANG_LITERATURE_COVER_SOFTCOVER;
        elseif( $values['coverid']==3 ) $cover=LANG_LITERATURE_COVER_UNKNOWNBINDING;
        elseif( $values['coverid']==4 ) $cover=LANG_LITERATURE_COVER_SPECIALBINDING;
        return $cover;
    }

    public static function get_language_info($db,$values)
    {
        $query_where_language=" WHERE languageid='".$values['languageid']."' ";
        $query_language=QUERIES::query_books_languages($db,$query_where_language);
        $results_language=$db->query($query_language['query']);
        $row_language=$db->mysql_array($results_language);
        $row_language=UTILS::html_decode($row_language);
        $title_language=UTILS::row_text_value($db,$row_language,"title");

        return $title_language;
    }

    public static function search_vars_from_get($get,$values=array())
    {
        $search_vars="";

        $catid_for_url=urlencode(UTILS::strip_slashes_recursive($get['catID']));
        if( !empty($get['catID']) ) $search_vars.="&catID=".$catid_for_url;

        $categoryid_for_url=urlencode(UTILS::strip_slashes_recursive($get['categoryid']));
        if( !empty($get['categoryid']) ) $search_vars.="&categoryid=".$categoryid_for_url;

        $artworkid_for_url=urlencode(UTILS::strip_slashes_recursive($get['artworkid']));
        if( !empty($get['artworkid']) ) $search_vars.="&artworkid=".$artworkid_for_url;

        $referer_for_url=urlencode(UTILS::strip_slashes_recursive($get['referer']));
        if( !empty($get['referer']) ) $search_vars.="&referer=".$referer_for_url;

        $title_for_url=urlencode(UTILS::strip_slashes_recursive($get['title']));
        if( !empty($get['title']) ) $search_vars.="&title=".$title_for_url;

        $number_for_url=urlencode(UTILS::strip_slashes_recursive($get['number']));
        if( !empty($get['number']) ) $search_vars.="&number=".$number_for_url;

        $number_from_for_url=urlencode(UTILS::strip_slashes_recursive($get['number-from']));
        if( !empty($get['number-from']) ) $search_vars.="&number-from=".$number_from_for_url;

        $number_to_for_url=urlencode(UTILS::strip_slashes_recursive($get['number-to']));
        if( !empty($get['number-to']) ) $search_vars.="&number-to=".$number_to_for_url;

        if( !empty($get['number_list']) )
        {
            foreach( $get['number_list'] as $number_list_value )
            {
                $number_list_for_url=urlencode(UTILS::strip_slashes_recursive($number_list_value));
                if( !empty($get['number_list']) ) $search_vars.="&number_list[]=".$number_list_for_url;
            }
        }

        $museum_for_url=urlencode(UTILS::strip_slashes_recursive($get['museum']));
        if( !empty($get['museum']) ) $search_vars.="&museum=".$museum_for_url;

        $year_from_for_url=urlencode(UTILS::strip_slashes_recursive($get['year-from']));
        if( !empty($get['year-from']) ) $search_vars.="&year-from=".$year_from_for_url;

        $year_to_for_url=urlencode(UTILS::strip_slashes_recursive($get['year-to']));
        if( !empty($get['year-to']) ) $search_vars.="&year-to=".$year_to_for_url;

        $date_day_from_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-day-from']));
        if( !empty($get['date-day-from']) ) $search_vars.="&date-day-from=".$date_day_from_for_url;

        $date_month_from_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-month-from']));
        if( !empty($get['date-month-from']) ) $search_vars.="&date-month-from=".$date_month_from_for_url;

        $date_year_from_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-year-from']));
        if( !empty($get['date-year-from']) ) $search_vars.="&date-year-from=".$date_year_from_for_url;

        $date_day_to_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-day-to']));
        if( !empty($get['date-day-to']) ) $search_vars.="&date-day-to=".$date_day_to_for_url;

        $date_month_to_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-month-to']));
        if( !empty($get['date-month-to']) ) $search_vars.="&date-month-to=".$date_month_to_for_url;

        $date_year_to_for_url=urlencode(UTILS::strip_slashes_recursive($get['date-year-to']));
        if( !empty($get['date-year-to']) ) $search_vars.="&date-year-to=".$date_year_to_for_url;

        $size_height_min_for_url=urlencode(UTILS::strip_slashes_recursive($get['size-height-min']));
        if( !empty($get['size-height-min']) ) $search_vars.="&size-height-min=".$size_height_min_for_url;

        $size_height_max_for_url=urlencode(UTILS::strip_slashes_recursive($get['size-height-max']));
        if( !empty($get['size-height-max']) ) $search_vars.="&size-height-max=".$size_height_max_for_url;

        $size_width_min_for_url=urlencode(UTILS::strip_slashes_recursive($get['size-width-min']));
        if( !empty($get['size-width-min']) ) $search_vars.="&size-width-min=".$size_width_min_for_url;

        $size_width_max_for_url=urlencode(UTILS::strip_slashes_recursive($get['size-width-max']));
        if( !empty($get['size-width-max']) ) $search_vars.="&size-width-max=".$size_width_max_for_url;

        $colorid_for_url=urlencode(UTILS::strip_slashes_recursive($get['colorid']));
        if( !empty($get['colorid']) ) $search_vars.="&colorid=".$colorid_for_url;

        $info_for_url=urlencode(UTILS::strip_slashes_recursive($get['info']));
        if( !empty($get['info']) ) $search_vars.="&info=".$info_for_url;

        $search_main=urlencode(UTILS::strip_slashes_recursive($get['search_main']));
        if( !empty($get['search_main']) ) $search_vars.="&search_main=".$search_main;

        $keyword=urlencode(UTILS::strip_slashes_recursive($get['keyword']));
        if( !empty($get['keyword']) ) $search_vars.="&keyword=".$keyword;

        $location=urlencode(UTILS::strip_slashes_recursive($get['location']));
        if( !empty($get['location']) ) $search_vars.="&location=".$location;

        if( $get['search'] ) $search_vars.="&search=1";

        $return=array();
        $return['no_sp']=$search_vars;

        $p_for_url=urlencode(UTILS::strip_slashes_recursive($get['p']));
        if( !empty($get['p']) ) $search_vars.="&p=".$p_for_url;

        $sp_for_url=urlencode(UTILS::strip_slashes_recursive($get['sp']));
        if( !empty($get['sp']) ) $search_vars.="&sp=".$sp_for_url;

        $return['with_sp']=$search_vars;

        return $return;
    }

    static function check_relations($db,$options)
    {
        $return=array();
        $relations=0;
        if( !empty($options['exhibitionid']) ) $options['itemid']=$options['exhibitionid'];
        if( empty($options['typeid']) ) $options['typeid']=4;

        # Artwork
        $query_exh_painting_relations="SELECT r.relationid FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1='".$options['typeid']."' AND r.itemid1='".$options['itemid']."' AND r.typeid2=1 ) OR ( r.typeid2='".$options['typeid']."' AND r.itemid2='".$options['itemid']."' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) AND p.enable=1 ";
        $query_exh_painting_relations.=" AND p.artworkID!=16 "; # do not display artworkID 16(Prints) while Joe hasnt allowed this
        $query_exh_painting_relations.=" LIMIT 1 ";
        $results_exh_painting_relations=$db->query($query_exh_painting_relations);
        $count_relations=$db->numrows($results_exh_painting_relations);
        if( $count_relations )
        {
            $return['tab']['artwork']=1;
            $relations++;
        }
        # END

        # Exhibitions
        $results_related_exh_relations=$db->query("SELECT r.relationid FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1='".$options['typeid']."' AND r.itemid1='".$options['itemid']."' AND r.typeid2='".$options['typeid']."' ) OR ( r.typeid2='".$options['typeid']."' AND r.itemid2='".$options['itemid']."' AND r.typeid1='".$options['typeid']."' ) ) AND ( ( r.itemid2=e.exID AND r.typeid2='".$options['typeid']."' ) OR ( r.itemid1=e.exID AND r.typeid1='".$options['typeid']."' ) ) AND e.exID!='".$options['itemid']."' LIMIT 1");
        $count_related_exh_relations=$db->numrows($results_related_exh_relations);
        if( $count_related_exh_relations )
        {
            $return['tab']['exhibitions']=1;
            $relations++;
        }
        # END

        ### INSTALLATION SHOTS
        $results_installations_relations=$db->query("SELECT relationid FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$options['typeid']."' AND itemid1='".$options['itemid']."' AND typeid2=5 ) OR ( typeid2='".$options['typeid']."' AND itemid2='".$options['itemid']."' AND typeid1=5 ) LIMIT 1 ");
        $count_installations_relations=$db->numrows($results_installations_relations);
        if( $count_installations_relations )
        {
            $return['tab']['installation_shots']=1;
            $relations++;
        }
        # END

        ### LITERATURE
        $results_literature=$db->query("SELECT r.relationid FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1='".$options['typeid']."' AND r.itemid1='".$options['itemid']."' AND r.typeid2=12 ) OR ( r.typeid2='".$options['typeid']."' AND r.itemid2='".$options['itemid']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1  LIMIT 1 ");
        $count_literature=$db->numrows($results_literature);
        if( $count_literature )
        {
            $return['tab']['literature']=1;
            $relations++;
        }
        # END

        ### VIDEOS
        $results_videos_relations=$db->query("SELECT relationid FROM ".TABLE_RELATIONS." WHERE ( typeid1='".$options['typeid']."' AND itemid1='".$options['itemid']."' AND typeid2=10 ) OR ( typeid2='".$options['typeid']."' AND itemid2='".$options['itemid']."' AND typeid1=10 ) LIMIT 1 ");
        $count_videos_relations=$db->numrows($results_videos_relations);
        if( $count_videos_relations )
        {
            $return['tab']['videos']=1;
            $relations++;
        }
        # END

        ### GUIDE
        $guide=0;
        if( !empty($options['src_guide']) && $options['typeid']==4 )
        {
            $return['tab']['guide']=1;
            $guide=1;
            $relations++;
        }
        #end

        # exhibtion description
        if( !$relations && $options['typeid']==4 )
        {
            $query_where_exhibition=" WHERE exID='".$options['itemid']."' ";
            $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
            $results=$db->query($query_exhibition['query']);
            $row=$db->mysql_array($results,0);

            if( $row['description_enable'] )
            {
                $return['tab']['exhibition_description']=1;
                $relations++;
            }
        }

        $return['relations']=$relations;

        if( $options['return'] ) return $return;
        else return $relations;

    }


public static function roundUpToTen($roundee)
{
  $r = $roundee % 10;
  //if ($r == 0) return $roundee;
  return $roundee + 10 - $r;
}

function round_down_to_integer($input_integer, $nearest_value = 0){

///Missing an input
if(!$input_integer || $input_integer < 1){return false;}

//Convert decimals to integers
$input_integer = round($input_integer,0);
$nearest_value = round($nearest_value,0);

//Can't round down any more
if($input_integer < $nearest_value){return false;}

//USED IN CALCULATION
if($nearest_value <= 10 && $nearest_value >= 0){$calc_upper_limit = 10;}
elseif($nearest_value <= 100 && $nearest_value > 10){$calc_upper_limit = 100;}
elseif($nearest_value <= 1000 && $nearest_value > 100){$calc_upper_limit = 1000;}
elseif($nearest_value <= 10000 && $nearest_value > 1000){$calc_upper_limit = 10000;}
//could go further to 100K, 1M...etc if you need it.
else{return false;}

//Intermediate Calculations
$subtract_by = $calc_upper_limit - $nearest_value;
$significant_digits_input = substr($input_integer, strlen($input_integer)-strlen($nearest_value),strlen($nearest_value));

//Final Calculations
if($significant_digits_input >= $nearest_value){$output = $input_integer - ($significant_digits_input - $nearest_value);}
else{$output = $input_integer - $significant_digits_input - $subtract_by;}

if($output > 0){return $output;}
else{return false;}

} //end function

public static function check_email_address($email) {
    return filter_var( $email, FILTER_VALIDATE_EMAIL );
}

public static function rom2arab($rom,$letters=array()){
    if(empty($letters)){
        $letters=array('M'=>1000,
                       'D'=>500,
                       'C'=>100,
                       'L'=>50,
                       'X'=>10,
                       'V'=>5,
                       'I'=>1);
    }else{
        arsort($letters);
    }
    $arab=0;
    foreach($letters as $L=>$V){
        while(strpos($rom,$L)!==false){
            $l=$rom[0];
            $rom=substr($rom,1);
            $m=$l==$L?1:-1;
            $arab += $letters[$l]*$m;
        }
    }
    return $arab;
}




/**
 * searches a simple as well as multi dimension array
 * @param type $needle
 * @param type $haystack
 * @return boolean
 */
public static function in_array_multi($needle, $haystack){
    $needle = trim($needle);
    if(!is_array($haystack))
        return False;

    foreach($haystack as $key=>$value){
        if(is_array($value)){
            if(self::in_array_multi($needle, $value))
                return True;
            else
               self::in_array_multi($needle, $value);
        }
        else
        if(trim($value) === trim($needle)){//visibility fix//
            error_log("$value === $needle setting visibility to 1 hidden");
            return True;
        }
    }

    return False;
}



}
?>
