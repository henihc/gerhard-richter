<?php

/*
define("EMAIL_INFO", 'info@gerhard-richter.com');
define("EMAIL_INFO_FULL", ' <'.EMAIL_INFO.'>');
define("EMAIL_INFO_PASSWD", 'oSW2Ft5R-Lgv');
define("EMAIL_INFO_HOST", 'mail.gerhard-richter.com');
define("EMAIL_INFO_PORT", '25');

define("EMAIL_OPP", 'opp@gerhard-richter.com');
define("EMAIL_OPP_NAME", 'Overpainted Photographs');
define("EMAIL_OPP_PASSWD", '3sRaaAfdHvlB');
//define("EMAIL_OPP_PASSWD", '0PPem@il');

define("EMAIL_ADMIN", 'pauls.daugerts@henipublishing.com');
define("EMAIL_ADMIN_NAME", 'Pauls Daugerts');
define("EMAIL_ADMIN_FULL", EMAIL_ADMIN_NAME.' <'.EMAIL_ADMIN.'>');
define("EMAIL_ADMIN_PASSWORDS", 'saulite22');
define("EMAIL_ADMIN_HOST", 'site2.exch500.serverdata.net');
define("EMAIL_ADMIN_PORT", '25');
*/

define("EMAIL_INFO", 'postmaster@heni.com');
define("EMAIL_INFO_FULL", ' <'.EMAIL_INFO.'>');
define("EMAIL_INFO_TO", 'info@gerhard-richter.com');
define("EMAIL_INFO_TO_FULL", '<.'.EMAIL_INFO_TO.'>');
define("EMAIL_INFO_PASSWD", 'Pada2100');
define("EMAIL_INFO_HOST", 'smtp.office365.com');
define("EMAIL_INFO_PORT", '25');

define("EMAIL_OPP", 'opp@gerhard-richter.com');
define("EMAIL_OPP_NAME", 'Overpainted Photographs');
define("EMAIL_OPP_PASSWD", '3sRaaAfdHvlB');
//define("EMAIL_OPP_PASSWD", '0PPem@il');

define("EMAIL_ADMIN", 'postmaster@heni.com');
define("EMAIL_ADMIN_NAME", 'postmaster@heni.com');
define("EMAIL_ADMIN_FULL", EMAIL_ADMIN_NAME.' <'.EMAIL_ADMIN.'>');
define("EMAIL_ADMIN_PASSWORDS", 'Pada2100');
define("EMAIL_ADMIN_HOST", 'smtp.office365.com');
define("EMAIL_ADMIN_PORT", '25');

//RESIZED IMAGE SIZE
define("THUMB_XS_WIDTH", '24');
define("THUMB_XS_HEIGHT", '25');
define("THUMB_S_WIDTH", '55');
define("THUMB_S_HEIGHT", '39');
define("THUMB_M_WIDTH", '118');
define("THUMB_M_HEIGHT", '200');//91
define("THUMB_L_WIDTH", '160');
define("THUMB_L_HEIGHT", '300');
define("THUMB_LL_HEIGHT", '170');//atlas
define("THUMB_LL_WIDTH", '160');//atlas
define("THUMB_LLL_HEIGHT", '450');
define("THUMB_LLL_WIDTH", '450');
define("THUMB_XL_WIDTH", '532');
define("THUMB_XL_HEIGHT", '532');
define("THUMB_XXL_WIDTH", '673');
define("THUMB_XXL_HEIGHT", '673');

define("SIZE_SNOWWHITE_HEIGHT", '489');
define("SIZE_SNOWWHITE_WIDTH", '700');

define("SIZE_WARCUT_HEIGHT", '650');
define("SIZE_WARCUT_WIDTH", '565');

define("SIZE_FLORENCE_HEIGHT", '650');
define("SIZE_FLORENCE_WIDTH", '565');

define("SIZE_VIDEO_THUMB_HEIGHT", '100');
define("SIZE_VIDEO_THUMB_WIDTH", '150');
define("SIZE_VIDEO_XSMALL_HEIGHT", '50');
define("SIZE_VIDEO_XSMALL_WIDTH", '50');
define("SIZE_VIDEO_SMALL_HEIGHT", '100');
define("SIZE_VIDEO_SMALL_WIDTH", '150');
define("SIZE_VIDEO_MEDIUM_HEIGHT", '200');
define("SIZE_VIDEO_MEDIUM_WIDTH", '350');
define("SIZE_VIDEO_LARGE_HEIGHT", '304');
define("SIZE_VIDEO_LARGE_WIDTH", '544');



define("SIZE_XSMALL_4900_COLOURS_HEIGHT", '50');
define("SIZE_XSMALL_4900_COLOURS_WIDTH", '50');

define("SIZE_SMALL_4900_COLOURS_HEIGHT", '90');
define("SIZE_SMALL_4900_COLOURS_WIDTH", '93');

define("SIZE_MEDIUM_4900_COLOURS_HEIGHT", '150');
define("SIZE_MEDIUM_4900_COLOURS_WIDTH", '150');

define("SIZE_LARGE_4900_COLOURS_HEIGHT", '650');
define("SIZE_LARGE_4900_COLOURS_WIDTH", '565');

define("SIZE_LARGE_ELBE_COLOURS_HEIGHT", '550');
define("SIZE_LARGE_ELBE_COLOURS_WIDTH", '465');

define("SIZE_SMALL_QUOTES_CAT_HEIGHT", '50');
define("SIZE_SMALL_QUOTES_CAT_WIDTH", '50');

define("SIZE_MEDIUM_QUOTES_CAT_HEIGHT", '100');
define("SIZE_MEDIUM_QUOTES_CAT_WIDTH", '170');

define("SIZE_LARGE_QUOTES_CAT_HEIGHT", '300');
define("SIZE_LARGE_QUOTES_CAT_WIDTH", '300');

//** END - RESIZED IMAGE SIZE

//RESIZED IMAGE SIZE - BOOKS
define("THUMB_XS_WIDTH_BOOKS", '50');
define("THUMB_XS_HEIGHT_BOOKS", '50');
define("THUMB_S_WIDTH_BOOKS", '134');
define("THUMB_S_HEIGHT_BOOKS", '81');
define("THUMB_M_WIDTH_BOOKS", '131');
define("THUMB_M_HEIGHT_BOOKS", '130');
define("THUMB_L_WIDTH_BOOKS", '320');
define("THUMB_L_HEIGHT_BOOKS", '426');
define("THUMB_XL_WIDTH_BOOKS", '450');
define("THUMB_XL_HEIGHT_BOOKS", '700');
//** END - RESIZED IMAGE SIZE - BOOKS

# photos (detail photographs)
define("SIZE_PAINTING_PHOTOS_LARGE_HEIGHT", '590');
define("SIZE_PAINTING_PHOTOS_LARGE_WIDTH", '800');
# END photos (detail photographs)

# installation photos
define("SIZE_INSTAL_PHOTO_XLARGE_HEIGHT", '590');
define("SIZE_INSTAL_PHOTO_XLARGE_WIDTH", '800');
# END installation photos

# microsite preview image thumb
define("SIZE_MICROSITE_SMALL_WIDTH", '134');
define("SIZE_MICROSITE_SMALL_HEIGHT", '67');
# END microsite preview image thumb

define("THUMBNAIL_WIDTH", '148');
define("THUMBNAIL_HEIGHT", '148');
define("THUMBNAIL_QUALITY", '75');
define("THUMBNAIL_BGCOLOUR", 'B5B5B5');


define("SHOW_PER_PAGE", '32');


define("DB_ENCODEING", 'UTF-8');

define("DOMAIN", 'gerhard-richter.com');
define("WWW_DOMAIN", 'www.gerhard-richter.com');
define("NEW_DOMAIN", 'new.gerhard-richter.com');

define("HTTP_SERVER", 'https://'.$_SERVER['HTTP_HOST']);
define("SERVER", $_SERVER['HTTP_HOST']);
define("HTTPS_SERVER", 'https://'.$_SERVER['HTTP_HOST']);

define("GOOGLE_ANALYTICS_USER", 'UA-1725572-7');

#DB const
define("DB_HOST", 'localhost');
//define("DB_HOST", '127.0.0.1');
//define("DB_HOST", '77.72.201.34:3306');

if( $_SERVER['HTTP_HOST']=="new.gerhard-richter.com" )
{
    define("DB_USER", 'richter_staging');
    define("DB_PASSWORD", 'yhbvgt098');
    define("DB_DB", 'richter_staging');

    define(DB_USER_IP_COUNTRY, DB_USER);
    define(DB_PASSWORD_IP_COUNTRY, DB_PASSWORD);
    define(DB_DB_IP_COUNTRY, 'richter_ip2country');

    define("DATA_PATH", '/home/richter/datadir/staging');
    define("DATA_PATH_DISPLAY", '/home/richter/datadir/live');
    define("DATA_PATH_SHOP", '/home/grshop/datadir/staging');
    define("DATA_PATH_DATADIR", '/datadir_live');
    define("NICEDIT_UPLOAD_PATH", $_SERVER["DOCUMENT_ROOT"].'/../datadir/nicedit');
    define("NICEDIT_IMG_PATH", HTTP_SERVER.'/datadir/nicedit');
    define("IS_STAGING", 1);
    define("IS_DEV", 0);

    /*
    define("DB_SHOP_USER", 'grshop_staging');
    define("DB_SHOP_PASSWORD", 'yhbvgt098');
    define("DB_SHOP_DB", 'grshop_staging');
    */
}
else if( $_SERVER['HTTP_HOST']=="gerhard-richter.com" || $_SERVER['HTTP_HOST']=="www.gerhard-richter.com" )
{
    define("DB_USER", 'richter_live');
    define("DB_PASSWORD", 'yhbvgt098');
    define("DB_DB", 'richter_live_fixed_encodings');

    define(DB_USER_IP_COUNTRY, DB_USER);
    define(DB_PASSWORD_IP_COUNTRY, DB_PASSWORD);
    define(DB_DB_IP_COUNTRY, 'richter_ip2country');

    define("DB_USER_FILEMAKE", 'richter_filemake');
    define("DB_PASSWORD_FILEMAKE", 'G&ucaB?h');

    define("DATA_PATH", '/home/richter/datadir/live');
    define("DATA_PATH_DISPLAY", '/home/richter/datadir/live');
    define("DATA_PATH_SHOP", '/home/grshop/datadir/live');
    define("DATA_PATH_DATADIR", '/datadir');
    define("NICEDIT_UPLOAD_PATH", $_SERVER["DOCUMENT_ROOT"].'/../datadir/nicedit');
    define("NICEDIT_IMG_PATH", HTTP_SERVER.'/datadir/nicedit');
    define("IS_STAGING", 0);
    define("IS_DEV", 0);

    /*
    define("DB_SHOP_USER", 'grshop_live');
    define("DB_SHOP_PASSWORD", 'yhbvgt098');
    define("DB_SHOP_DB", 'grshop_live');
    */
}
else
{
    if( $_SERVER['HTTP_HOST']!="localhost" ) error_reporting(0);
    define("DB_USER", 'root');
    define("DB_PASSWORD", 'root');
    define("DB_DB", 'richter_live');

    define(DB_USER_IP_COUNTRY, DB_USER);
    define(DB_PASSWORD_IP_COUNTRY, DB_PASSWORD);
    define(DB_DB_IP_COUNTRY, 'ip2country');

    define("DATA_PATH", '/Users/paulsdaugerts/Sites/gerhard-richter.com/trunk/htdocs/datadir');
    define("DATA_PATH_DISPLAY", '/Users/paulsdaugerts/Sites/gerhard-richter.com/trunk/htdocs/datadir');
    define("DATA_PATH_SHOP", '/Users/paulsdaugerts/Sites/gerhardrichtershop.com/trunk/htdocs/datadir');
    define("DATA_PATH_DATADIR", '/datadir');
    define("NICEDIT_UPLOAD_PATH", $_SERVER["DOCUMENT_ROOT"].'/datadir/nicedit');
    define("NICEDIT_IMG_PATH", HTTP_SERVER.'/datadir/nicedit');
    define("IS_STAGING", 1);
    define("IS_DEV", 1);

    /*
    define("DB_SHOP_USER", 'root');
    define("DB_SHOP_PASSWORD", 'root');
    define("DB_SHOP_DB", 'grshop');
    */
}

# define DB table names
define("TABLE_PAINTING" , 'painting');
define("TABLE_PAINTING_SIZE_TYPES" , 'painting_size_types');
define("TABLE_PAINTING_COLORS" , 'painting_colors');
define("TABLE_PAINTING_PHOTOS" , 'painting_photos');
define("TABLE_PAINTING_OPP" , 'painting_opp');
define("TABLE_PAINTING_NOTE" , 'painting_note_images');
define("TABLE_ARTWORKS" , 'artworks');
define("TABLE_MEDIA" , 'media');
define("TABLE_MUSEUM" , 'museum');
define("TABLE_SALEHISTORY" , 'saleHistory');
define("TABLE_CURRENCY" , 'currency');
define("TABLE_AUCTIONHOUSE" , 'auctionHouse');
define("TABLE_CATEGORY" , 'category');
define("TABLE_EXHIBITIONS" , 'exhibitions');
define("TABLE_EXHIBITIONS_INSTALLATIONS" , 'exhibitions_installations');
define("TABLE_EXHIBITIONS_INSTALLATIONS_IMAGES" , 'exhibitions_installations_images');
define("TABLE_LOCATIONS" , 'locations');
define("TABLE_LOCATIONS_CITY" , 'locations_city');
define("TABLE_LOCATIONS_COUNTRY" , 'locations_country');
define("TABLE_SELECT_DROPDOWNS" , 'select_dropdowns');
define("TABLE_SELECT_DROPDOWNS_OPTIONS" , 'select_dropdowns_options');
define("TABLE_BOOKS" , 'books');
define("TABLE_BOOKS_ISBN" , 'books_isbn_numbers');
define("TABLE_BOOKS_CATEGORIES" , 'books_categories');
define("TABLE_BOOKS_LANGUAGES" , 'books_languages');
define("TABLE_VIDEO" , 'video');
define("TABLE_VIDEO_CATEGORIES" , 'video_categories');
define("TABLE_VIDEO_SUBTITLES" , 'video_subtitles');
define("TABLE_AUDIO" , 'audio');
define("TABLE_PHOTOS" , 'photos');
define("TABLE_ADMIN" , 'admin');
define("TABLE_ADMIN_LOGS" , 'admin_logs');
define("TABLE_NEWS" , 'news');
define("TABLE_BIOGRAPHY" , 'biography');
define("TABLE_ARTICLES_CATEGORIES" , 'articles_categories');
define("TABLE_QUOTES" , 'quotes');
define("TABLE_QUOTES_CATEGORIES" , 'quotes_categories');
define("TABLE_MICROSITES" , 'microsites');
define("TABLE_MICROSITES_IMAGES" , 'microsites_images');
define("TABLE_MICROSITES_VERSIONS" , 'microsites_versions');
define("TABLE_RELATIONS" , 'relations');
define("TABLE_RELATIONS_LITERATURE" , 'relations_literature');
define("TABLE_RELATIONS_PAINTING_COLORS" , 'relations_painting_colors');
define("TABLE_IMAGES" , 'images');
define("TABLE_FORM_OPP" , 'form_opp_data');
define("TABLE_FORM_OPP_IMAGES" , 'form_opp_data_images');
define("TABLE_TEXT" , 'text');
define("TABLE_SEARCH_TEST_KEYWORDS" , 'search_test_keywords');
define("TABLE_SEARCH_TEST_CATEGORIES" , 'search_test_categories');

#NOT USING
define("TABLE_CONTACT" , 'contact');
?>
