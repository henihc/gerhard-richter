<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");

$db=new dbCLASS;

$_GET=$db->db_prepare_input($_GET);


### what we are retrieving category image or painting image
if( isset( $_GET['videoID'] ) ) 
{
    $results=$db->query("SELECT * FROM ".TABLE_VIDEO." WHERE videoID='".$_GET['videoID']."' ");
}
elseif( isset( $_GET['subtitleid'] ) ) 
{
    $results=$db->query("SELECT * FROM ".TABLE_VIDEO_SUBTITLES." WHERE subtitleid='".$_GET['subtitleid']."' ");
}
#end

$row=$db->mysql_array($results);
$count=$db->numrows($results);
if( $count!=1 ) 
{
    exit;
    $error[]="Can't find video on db";
}

### if found image in db
if( sizeof($error)==0 )
{
    if( isset( $_GET['videoID'] ) ) $src=DATA_PATH."/videos/subtitles/".$row['videoID'].".srt";
    elseif( isset( $_GET['subtitleid'] ) ) $src=DATA_PATH."/videos/subtitles/".$row['subtitleid'].".srt";
    
	### if file exists on file system
    if( file_exists($src) )
    {
    	        
        ### if showing video straight from file system
            if( !$_GET['debug'] )
            {
                header ('Content-transfer-encoding: binary');
                header('Content-Type: text/plain');
                header('Content-Disposition: '.$disposition.'; filename='.basename($src)); 
                header ('Content-Length: ' . filesize($src));
                header ('Pragma: public');
                header ('Expires: 0');
                header ('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header ('Cache-Control: private');
                $f = fopen($src, 'rb');
        	    $contents = stream_get_contents($f);
        	    print $contents;
                fclose($f);
            } 
            else
            {
                print $src;
            }
		#end
	}
	#end
	else $error[]="Can't find file on file system ".$src;
	
}
#end
 

### if no video found showing no image available
if( sizeof($error)>0 )
{
    if( $_GET['debug'] )
    {
    print_r($error);
    exit;
    }
$src=LANG_NO_IMAGE_LARGE;
$getimagesize=getimagesize($no_image);//!!! if slow maybe need to take this getsize function out 
if( empty($getimagesize['mime']) ) $getimagesize['mime']="image/gif";
header('Content-Type: '.$getimagesize['mime']);
$f = fopen($src, 'rb');
$contents = stream_get_contents($f);
print $contents;
}
#end

?>
