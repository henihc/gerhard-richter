<?php

class html_elements
{
	var $css;
	var $js;
	var $bodyjs;
	var $js_footer;
	var $onload;
	var $onunload;
	var $title;
	var $right_side;
	var $left_side_menu_selected;
	var $lang_method;
    var $lang;

    function metainfo($db,$options=array())
    {
        $html_print="";
	    $html_print.="<!DOCTYPE html> \n";
		$html_print.="<html lang='".$options['lang']."' >\n";

        # HEAD
		$html_print.="<head>\n";
        $html_print.="  <meta charset='".strtolower(DB_ENCODEING)."' />\n";
        //$html_print.="  <meta http-equiv='Content-Language' content='".$options['locale']."' />\n";
        $html_print.="  <meta name='dcterms.rightsHolder' content='Joe Hage'>\n";
        if( (SERVER==DOMAIN || SERVER==WWW_DOMAIN) ) $robots_content="all";
        else $robots_content="nofollow, noindex";
		$html_print.="  <meta name='robots' content='".$robots_content."' />\n";

        # viewport
        if( !$_GET['mobile_device_old'] )
        {
            $html_print.='  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />'."\n";
            $html_print.='  <meta name="apple-mobile-web-app-capable" content="yes">'."\n";
            $html_print.='  <meta name="apple-mobile-web-app-status-bar-style" content="black" />'."\n";
        }
        # END viewport

        if( !empty($options['meta_description']) )
        {
            $meta_description=$options['meta_description'];
            $meta_description=strip_tags($meta_description);
            $meta_description=str_replace(array("\r", "\n", "\r\n"), "", $meta_description);
        }
        else $meta_description=LANG_META_DESCRIPTION;
        $html_print.="  <meta name='description' content=\"".$meta_description."\" />\n";
        $html_print.="  <meta name='SKYPE_TOOLBAR' content='SKYPE_TOOLBAR_PARSER_COMPATIBLE' />\n";
        $html_print.="  <meta name='google-site-verification' content='mTCgdOxKWlJ6AQzP2OtZc7E-MazklbLMSBkR9O1hn9o' />\n";
		$html_print.="  <meta name='msvalidate.01' content='58903FE373B59F37ECA7626029068DFC' />\n";
        $html_print.="  <meta property='fb:app_id' content='381754575506653'/>\n";

        $this->title=strip_tags($this->title);

        # facebook share og: variables
        $html_print.="  <meta property='og:url' content='".HTTP_SERVER.$_SERVER['REQUEST_URI']."' />\n";
        if( !empty($options['facbook_metainfo']['title']) ) $og_title=$options['facbook_metainfo']['title'];
        else $og_title=$this->title;
        $html_print.="  <meta property='og:title' content='".$og_title."' />\n";
        $html_print.="  <meta property='og:description' content=\"".$meta_description."\" />\n";
        if( !empty($options['facbook_metainfo']['image']) )
        {
            $html_print.="  <meta property='og:image' content='".$options['facbook_metainfo']['image']."' />\n";
        }
        $options_languages=array();
        $options_languages['not_load_lang_file']=1;
        $languages=UTILS::load_lang_file($db,$options_languages);
        foreach ($languages['languages'] as $key => $value)
        {
            if( $languages['languages'][$key]['lang']!=$options['lang'] )
            {
                $html_print.="  <meta property='og:locale:alternate' content='".$languages['languages'][$key]['locale_facebook']."' />\n";
            }
            else
            {
                $html_print.="  <meta property='og:locale' content='".$languages['languages'][$key]['locale_facebook']."' />\n";
            }
        }

        if( !empty($options['facbook_metainfo']['type']) ) $og_type=$options['facbook_metainfo']['type'];
        else $og_type="website";
        $html_print.="  <meta property='og:type' content='".$og_type."' />\n";
        if( !empty($options['facbook_metainfo']['updated_time']) )
        {
            $html_print.="  <meta property='og:updated_time' content='".$options['facbook_metainfo']['updated_time']."' />\n";
        }
        if( !empty($options['facbook_metainfo']['video']) )
        {
            $html_print.="  <meta property='og:video' content='".$options['facbook_metainfo']['video']."' />\n";
            if( !empty($options['facbook_metainfo']['video_secure_url']) )
            {
                $html_print.="  <meta property='og:video:secure_url' content='".$options['facbook_metainfo']['video_secure_url']."' />\n";
            }
            if( !empty($options['facbook_metainfo']['video_type']) )
            {
                $html_print.="  <meta property='og:video:type' content='".$options['facbook_metainfo']['video_type']."' />\n";
            }
            if( !empty($options['facbook_metainfo']['video_width']) )
            {
                $html_print.="  <meta property='og:video:width' content='".$options['facbook_metainfo']['video_width']."' />\n";
            }
            if( !empty($options['facbook_metainfo']['video_height']) )
            {
                $html_print.="  <meta property='og:video:height' content='".$options['facbook_metainfo']['video_height']."' />\n";
            }
        }
        if( !empty($options['facbook_metainfo']['book_author']) )
        {
            $html_print.="  <meta property='books:author' content='".$options['facbook_metainfo']['book_author']."' />\n";
        }
        if( !empty($options['facbook_metainfo']['book_isbn']) )
        {
            $html_print.="  <meta property='books:isbn' content='".$options['facbook_metainfo']['book_isbn']."' />\n";
        }

        # END facebook share og: variables

		$html_print.="  <title>".$this->title."</title>\n";
		$html_print.="  <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon' />\n";
        if( IS_STAGING ) $html_print.="  <link href='/css/staging.css' rel='stylesheet' type='text/css' media='screen' />\n";
		#adding CSS files
		for ($i=0;$i<sizeof($this->css);$i++)
        {
            if( !empty($this->css[$i]['ie']) && is_numeric($this->css[$i]['ie']) )
            {
                $html_print.="  <!--[if IE ".$this->css[$i]['ie']."]> <link rel='stylesheet' href='".$this->css[$i]['url']."' media='screen, projection' type='text/css' /> <![endif]-->\n";
            }
            else
            {
			    $html_print.="  <link href='".$this->css[$i]."' rel='stylesheet' type='text/css' media='screen' />\n";
            }
		}
		$html_print.="  <!--[if IE 8]><link href='/css/ie8.css' rel='stylesheet' type='text/css' media='screen' /><![endif]-->\n";
		$html_print.="  <!--[if IE 9]><link href='/css/ie9.css' rel='stylesheet' type='text/css' media='screen' /><![endif]-->\n";
		$html_print.="  <!--[if IE 10]><link href='/css/ie10.css' rel='stylesheet' type='text/css' media='screen' /><![endif]-->\n";
		#adding JS files
		$html_print.="  <script src='/js/css_browser_selector.js' type='text/javascript'></script>\n";
		for ($i=0;$i<sizeof($this->js);$i++)
		{
			$html_print.="  <script src='".$this->js[$i]."' type='text/javascript'></script>\n";
		}

        if( ( WWW_DOMAIN==$_SERVER['HTTP_HOST'] || DOMAIN==$_SERVER['HTTP_HOST'] )
            && $options['google_analytics']
            && !$_SESSION['userID']
        )
        {
            $html_print.="  <script type='text/javascript'>\n";

                $html_print.="  var _gaq = _gaq || [];\n";
                $html_print.="  _gaq.push(['_setAccount', '".GOOGLE_ANALYTICS_USER."']);\n";
                $html_print.="  _gaq.push(['_trackPageview']);\n";

                $html_print.="  (function() {\n";
                    $html_print.="  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n";
                    $html_print.="  ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';\n";
                    $html_print.="  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n";
                $html_print.="  })();\n";

            $html_print.="  </script>\n";
        }

        # lang alternatives
            # for home add x-default
            $url_other_lang_x_default=UTILS::replace_lang_for_link($db,$_SERVER['REQUEST_URI'],"en");
            if( $_SERVER['REQUEST_URI'] == "/" ) $href=HTTPS_SERVER."/en/";
            else $href=HTTP_SERVER.$url_other_lang_x_default;
            $html_print.="  <link rel='alternate' hreflang='x-default' href='".$href."' />\n";

            foreach ($languages['languages'] as $key => $value)
            {
                if( $_SERVER['REQUEST_URI']=="/" ) $url_other_lang="/".$options['lang'];
                else $url_other_lang=$_SERVER['REQUEST_URI'];
                $url_other_lang=UTILS::replace_lang_for_link($db,$url_other_lang,$languages['languages'][$key]['lang']);

                $hreflang=$languages['languages'][$key]['lang'];
                $html_print.="  <link rel='alternate' hreflang='".$hreflang."' href='".HTTP_SERVER.$url_other_lang."' />\n";
            }
        # END lang alternatives

		$html_print.="</head>\n";
        # end HEAD

		#adding BODY ONLOAD JS functions
		if (sizeof($this->onload)>0) $onload_tmp=" onload=\"";
		for ($i=0;$i<sizeof($this->onload);$i++)
		{
			$onload_tmp.=$this->onload[$i];
		}

		#adding BODY ONUNLOAD JS functions
		if(sizeof($this->onload)>0)$onload_tmp.="\"";
		if (sizeof($this->onunload)>0) $onunload_tmp=" onunload='";
		for ($i=0;$i<sizeof($this->onunload);$i++)
		{
			$onunload_tmp.=$this->onunload[$i];
		}
		if(sizeof($this->onunload)>0)$onunload_tmp.="'";

        # body tag start
		$html_print.="<body".$onload_tmp.$onunload_tmp." class='".$options['class']['body']." ".$options['class']['titleurl']."' >\n";

            //$html_print.="  <script defer>";
            $html_print.="  <script>";
                if( !IS_DEV )
                {
                    # facebook share button - loading facbook sdk
                    $html_print.="window.fbAsyncInit = function() {
                    FB.init({
                      appId      : '381754575506653',
                      xfbml      : true,
                      version    : 'v2.8'
                    });
                    FB.AppEvents.logPageView();
                  };

                  (function(d, s, id){
                     var js, fjs = d.getElementsByTagName(s)[0];
                     if (d.getElementById(id)) {return;}
                     js = d.createElement(s); js.id = id;
                     js.src = \"//connect.facebook.net/".$_GET['locale_facebook']."/sdk.js\";
                     fjs.parentNode.insertBefore(js, fjs);
                   }(document, 'script', 'facebook-jssdk'));";
                }
            $html_print.="</script>";

        $html_print.="<div id='div-page-loading' >";

            $html_print.="  <script type='text/javascript'>";
                # loading animation for mobile site - shows loasing animation while page is not 100% loaded
                $html_print.="jQuery(document).ready( function($) {
                    $('#div-page-loading').delay( 800 ).hide(0);
                    $( window ).resize(function() {
                        $('#div-page-loading').hide(0);
                    });
                } );";
            $html_print.="</script>";

        $html_print.="</div>";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function div_languages($db,$options=array())
    {
        $html_print="";

        $html_print="<div id='site-lang-desktop' class='placeholder'>";
            $html_print.="\t<div id='site-lang' class='".$options['class']['language']." ".$options['class']['div-languages-search']."'>\n";

                $html_print_ul="\t<ul data-role='site-meta'>\n";
                    $options_languages=array();
                    $options_languages['not_load_lang_file']=1;
                    $languages=UTILS::load_lang_file($db,$options_languages);
                    $i=0;
                    foreach ($languages['languages'] as $key => $value)
                    {
                        if( $_SERVER['REQUEST_URI']=="/" ) $url_other_lang="/".$_GET['lang'];
                        else $url_other_lang=$_SERVER['REQUEST_URI'];
                        $lang=$languages['languages'][$key]['lang'];
                        $title_display=$languages['languages'][$key]['title_display'];
                        $class=$languages['languages'][$key]['class'];
                        $url_other_lang=UTILS::replace_lang_for_link($db,$url_other_lang,$lang);
                        //print_r($_GET);

                        if( $lang==$_GET['lang'] )
                        {
                            $class_selected_li="selected";
                            $class_selected_span=$class_selected_li." lang-on";
                        }
                        else
                        {
                            $class_selected_li="";
                            $class_selected_span="";
                        }

                        $html_print_ul.="<li class='".$class_selected_li."'>";

                            if( $lang!=$_GET['lang'] )
                            {
                                $html_print_ul.="\t<a href='".$url_other_lang."' class='".$class." ".$class_selected_li."' data-lang-id='".$i."' data-lang='".$lang."' title='".$title_display."'>".$title_display."</a>\n";
                            }
                            else
                            {
                                $html_print_ul.="\t<span class='".$class." ".$class_selected_span."' data-lang-id='".$i."' data-lang='".$lang."' title='".$title_display."'>".$title_display."</span>\n";
                                $html_print_caption="\t<a href='javascript:;' class='caption'>".$title_display."</a>\n";
                            }
                        $html_print_ul.="</li>";
                        $i++;
                    }
                $html_print_ul.="\t</ul>\n";

                $html_print.=$html_print_caption;
                $html_print.=$html_print_ul;

            $html_print.="\t</div>\n";
        $html_print.="\t</div>\n";

        # END printing out available LANG links

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    static function form_search_top($db,$options=array())
    {
        $html_print="";

        if( $options['search_page'] )
        {
            $html_print.="\t<a href='/".$_GET['lang']."/search/' class='a-menu-search-page'></a>\n";
        }
        else
        {
            $id="form-basic-search-top";
            $id_input_basic="input-basic-search-top";
            $class_submit_basic="submit-basic-search-top";
            $id_submit_basic="submit-basic-search-top";
            $autocomplete="";

            if( $options['mobile'] )
            {
                $id.="-mobile";
                $id_input_basic.="-mobile";
                $class_submit_basic.="-mobile";
                $id_submit_basic.="-mobile";
                $autocomplete="off";
            }
            else
            {
                $autocomplete="off";
            }

            $html_print.="\t<form action='/".$_GET['lang']."/search/' method='get' id='".$id."' class='".$options['class']['form-hide']."'>\n";
                $html_print.="\t<input type='text' name='search' placeholder='".LANG_SEARCH_BASIC_TOP_VALUE_SEARCH."' id='".$id_input_basic."' onkeyup=\"enable_input(this,'".$id_submit_basic."','".LANG_SEARCH_BASIC_TOP_VALUE_SEARCH."');\" value='' autocomplete='".$autocomplete."' />\n";
                $html_print.="\t<input type='submit' value='' disabled='disabled' title='".LANG_RIGHT_SEARCH_SEARCHBUTTON."' class='".$class_submit_basic."' id='".$id_submit_basic."' />\n";
            $html_print.="\t</form>\n";

        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function head($db,$page,$options=array())
    {
        //print_r($_GET);
        //print_r($options);
        $html_print="";

        $options_meta_info=array();
        $options_meta_info=$options;
        //print $_GET['lang'];
        $options_meta_info['lang']=$_GET['lang'];
        //print $_GET['lang'];
        $options_meta_info['locale']=$_GET['locale'];
        if( !isset($options['google_analytics']) ) $options_meta_info['google_analytics']=1;
        //$options_meta_info['class']['body']="body";
        $html_print.=$this->metainfo($db,$options_meta_info);
        //print $_GET['lang'];

        if( !$options['only_meta_info'] )
        {
            $html_print.="  <div id='top'></div>\n";

            # show to admin if logged in
            if( $_SESSION['userID'] )
            {
                # admin toolbar
                $html_print.="<div id='div-admin'>";
                    $html_print.="<div class='div-admin-content'>";

                    $html_print.="<div class='div-admin-icons'>";

                        # admin main home
                        $html_print.="<a href='/admin' title='Dashboard' class='a-admin-dashboard' ></a>";

                        # show editing tool
                        if( !isset($_SESSION['admin']['show_edit']) ) $_SESSION['admin']['show_edit']=1;
                        if( isset($_SESSION['admin']['show_edit']) && $_SESSION['admin']['show_edit'] )
                        {
                            $show_edit=0;
                        }
                        else
                        {
                            $show_edit=1;
                        }
                        $html_print.="<a title='Show admin edit tools on page' class='a-admin-show-edit' data-value='".$show_edit."' ></a>";

                        //print_r($_SESSION);
                    $html_print.="</div>";

                    # logged in as
                    $query_admin_user=$db->query("SELECT * FROM ".TABLE_ADMIN." WHERE userID='".$_SESSION['userID']."'");
                    $row_admin_user=$db->mysql_array($query_admin_user);

                    $user="";
                    if( !empty($row_admin_user['fname']) ) $user.=" ".ucwords($row_admin_user['fname']);
                    if( !empty($row_admin_user['lname']) ) $user.=" ".ucwords($row_admin_user['lname']);

                    $html_print.="<a title='Logged in as' class='a-admin-logged-in-as' >Logged in as <span class='span-admin-loggedin'>".$user."</span></a>";

                    $html_print.="<ul class='ul-admin-more-options' style='display:none;'>";
                        $html_print.="<li>";
                            $html_print.="<a href='/admin/management/users/edit/?".$row_admin_user['userID']."' title=''>User Profile</a>";
                        $html_print.="</li>";
                        $html_print.="<li class='li-logout'>";
                            $html_print.="<a href='/admin/sign_out' title=''>Logout</a>";
                        $html_print.="</li>";
                    $html_print.="</ul>";

                    $html_print.="<script type='text/javascript'>";

                        # show edit tools
                        if( isset($_SESSION['admin']['show_edit']) && !$_SESSION['admin']['show_edit'] )
                        {
                            $html_print.="jQuery(document).ready( function($) {
                                $('.edit').hide();
                            } );";
                        }
                        $html_print.='
                        $(".a-admin-show-edit").click(function()
                        {
                            $(".edit").toggle();

                            $.ajax({url: "/ajax/session_set.php?ses_section=admin&ses_key=show_edit&ses_value="+$(".a-admin-show-edit").data("value"), success: function(result){
                                var show_edit=$(".a-admin-show-edit").data("value");
                                if( show_edit==1 ) $(".a-admin-show-edit").data("value","0");
                                else $(".a-admin-show-edit").data("value","1");
                            }});

                        })';

                        # show admin more options
                        $html_print.='
                        $(".a-admin-logged-in-as").click(function(e)
                        {
                            e.stopPropagation();
                            $(".ul-admin-more-options").toggle();
                            $(".a-admin-logged-in-as").toggleClass("a-admin-logged-in-as-hover");
                            return false;
                        });
                        $(document).click(function() {
                            $(".ul-admin-more-options").hide();
                            $(".a-admin-logged-in-as").removeClass("a-admin-logged-in-as-hover");
                        });
                        ';

                    $html_print.="</script>";

                    $html_print.="</div>";
                $html_print.="</div>";
                $html_print.="<div class='div-admin-clearer'></div>\n";

                # STAGING site admin inform to not edit data here
                if( stripos($_SERVER['HTTP_HOST'], 'new.gerhard-richter.com')!== false )
                {
                    $html_print.="<div style='color:red;font-weight:bold;margin:0 auto;padding: 0 0 10px;text-align: center;width: 942px;'>THIS IS TESTING SERVER - IF YOU WANT TO EDIT DATA PLEASE DO THAT ON LIVE SERVER - <a href='https://www.gerhard-richter.com' >www.gerhard-richter.com</a></div>";
                }
            }
            #end

            $html_print.="  <div id='div-header' class='wrapper'>\n";

                $html_print.="    <a href='/".$_GET['lang']."/' class='a-heade-site-title'>Gerhard Richter</a>\n";

                # printing out available LANG links
                $options_div_languages=array();
                $options_div_languages=$options;
                $html_print.=$this->div_languages($db,$options_div_languages);
                # END printing out available LANG links

                # search top form
                $options_form_search_top=array();
                $options_form_search_top=$options;
                $html_print.=$this->form_search_top($db,$options_form_search_top);
                # END search top form

            if( !$options['no_menu'] )
            {
                $options_menu=array();
                $options_menu=$options;
                $html_print.=$this->menu($db,$page,$options_menu);
            }

                $html_print.="  </div>\n";
                $html_print.="  <div id='main' class='wrapper ".$options['class']['class_main']." ".$options['class']['div-main-literature']." ".$options['class']['wrapper']."'>\n";
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

	public function menu($db,$page,$options=array())
	{
        $html_print="";

        $selected=" class='selected";

        # fix for the menu on ipad
        /*
        $browser = new Browser();
        if( $browser->getBrowser() == Browser::BROWSER_IPAD && $_GET['lang']=="zh"  )
        {
    	    $selected.=" main-nav-selected-ipad-zh";
            $class_ul="main-nav-ipad-zh";
        }
        else
        {
            $class_ul="";
        }
        */
        # END fix for the menu on ipad

	    $selected.="'";
	    if( !$options['no_nav'] )
        {
            $html_print.="    <nav role='navigation'>\n";
        }

        if( !$options['no_nav'] )
        {
        $html_print.='<div id="menu-header">';

                $html_print.='<header role="banner">';
                    $html_print.='<div id="header">';
                        $html_print.='<a href="/'.$_GET['lang'].'" class="header_logo u47">'.LANG_TITLE_HOME.'</a>';
                        $html_print.='<div id="site-search-btn"></div>';

                        $html_print.='<div id="site-search" role="search" class="hidden"><!-- shown by js later -->';
                            $html_print.='<div class="content">';

                                # search top form
                                $options_form_search_top=array();
                                $options_form_search_top=$options;
                                $options_form_search_top['mobile']=1;
                                $html_print.=html_elements::form_search_top($db,$options_form_search_top);
                                # END search top form

                                $html_print.='<ul id="main-nav" data-nav-type="main" class="">';

                                    # Art section search
                                    if( empty($_GET['section_0']) || $_GET['section_0']=="art" )
                                    {
                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_ARTWORK.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $options_search_block['advanced_search']=1;
                                                    $search_block=$this->search_form_art($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';
                                    }

                                    # Quotes section search
                                    if( $_GET['section_0']=="quotes" )
                                    {
                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_QUOTES.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $search_block=$this->search_form_quotes($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';
                                    }

                                    # Exhibition section search
                                    if( $_GET['section_0']=="exhibitions" )
                                    {
                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_EXHIBITIONS.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $search_block=$this->search_form_exhibitions($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';
                                    }

                                    # Literature section search
                                    if( $_GET['section_0']=="literature" )
                                    {
                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_LITERATURE.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $search_block=$this->search_form_literature($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';
                                    }

                                    # Videos section search
                                    if( $_GET['section_0']=="videos" )
                                    {

                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_VIDEOS.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $search_block=$this->search_form_videos($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';

                                    }

                                    # Links section search
                                    /*
                                    if( $_GET['section_0']=="links" )
                                    {
                                        $html_print.='<li class="main-nav-li">';
                                            $html_print.='<a href="#" class="section" title="">'.LANG_MOBILE_SEARCH_FIND.' '.LANG_HEADER_MENU_CONTACT.'</a>';
                                            $html_print.='<a class="expand" href="javascript:"></a>';
                                            $html_print.='<ul data-nav-type="sub">';
                                                $html_print.='<li class="main-nav-sub-li">';

                                                    $options_search_block=array();
                                                    $options_search_block=$options;
                                                    $options_search_block['options_head']['html_return']=$options['html_return'];
                                                    $options_search_block['class']['div-search-block']="div-content-header-block-right-mobile";
                                                    $options_search_block['id_additional']="-mobile";
                                                    $options_search_block['h1-title']="";
                                                    $options_search_block['more-options']="";
                                                    $options_search_block['html_return_reset']=1;
                                                    $search_block=$this->search_form_links($db,$options_search_block);

                                                    $html_print.=$search_block['html_print'];
                                                    //$html_print.=$search_block['html_print_script'];

                                                $html_print.='</li>';
                                            $html_print.='</ul>';
                                        $html_print.='</li>';
                                    }
                                    */

                                $html_print.='</ul>';

                                # search form reset script
                                $html_print.=$search_block['html_print_script'];

                            $html_print.='</div><!-- .content -->';
                        $html_print.='</div><!-- #site-search -->';

                        $html_print.='<div id="site-navigation-btn" class=""></div>';

                        $html_print.='<div id="site-navigation" role="navigation" class="hidden"><!-- shown by js later -->';
                            $html_print.='<div class="content">';
                                $html_print.='<nav>';
                                    $html_print.='<div id="site-lang-mobile" class="placeholder"></div>';
                                    $html_print.='<ul id="main-nav-mobile" data-nav-type="main" class="">';


                                    $menu_items=array();

                                    # HOME
                                    $i=0;
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_HOME;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']="/".$_GET['lang'];
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                    # ART
                                    $i++;
                                    $url="/".$_GET['lang']."/art";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_ARTWORK;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        # PAINTINGS
                                        $url_sub=$url."/paintings";

                                        $i_sub=0;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_PAINTINGS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                            # PHOTO PAINTINGS
                                            $url_sub_sub=$url_sub;

                                            $i_sub_sub=0;
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value']=LANG_ARTWORK_SECTION_PHOTOPAINTINGS;
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['url']=$url_sub_sub."/#photo-paintings";
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['class']="photo-paintings";

                                            $i_sub_sub++;
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value']=LANG_ARTWORK_SECTION_ABSTRACTS;
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['url']=$url_sub_sub."/#abstracts";
                                            $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value']);

                                        # ATLAS
                                        $url_sub=$url."/atlas";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_ATLAS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # EDITIONS
                                        $url_sub=$url."/editions";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_EDITIONS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # DRAWINGS
                                        $url_sub=$url."/drawings";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_DRAWINGS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # OVERPAINTED PHOTOGRAPHS
                                        $url_sub=$url."/overpainted-photographs";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']="overpainted-photographs";

                                        # OILS ON PAPER
                                        $url_sub=$url."/oils-on-paper";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_OILSONPAPER;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']="oils-on-paper";

                                        # WATERCOLOURS
                                        $url_sub=$url."/watercolours";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_WATERCOLOURS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # ARTISTS BOOKS
                                        $url_sub="/".$_GET['lang']."/literature/artists-books";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_ARTWORK_ARTISTS_BOOKS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']="artists-books";

                                        # PRINTS
                                        $url_sub=$url."/prints";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_PRINTS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # OTHER
                                        $url_sub=$url."/other";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_ARTWORK_SECTION_OTHERMEDIA;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # MICROSITES
                                        $url_sub=$url."/microsites";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LEFT_SIDE_MICROSITES;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                    # BIOGRAPHY
                                    $i++;
                                    $url="/".$_GET['lang']."/biography";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_BIOGRAPHY;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        $results_category=$db->query("SELECT * FROM ".TABLE_BIOGRAPHY." WHERE enable=1 ORDER BY sort ");

                                        $i_sub=0;
                                        while( $row=$db->mysql_array($results_category) )
                                        {
                                            $title=UTILS::row_text_value($db,$row,'title_left');
                                            $url_sub=$url."/".$row['titleurl'];

                                            $menu_items[$i]['sub-menu'][$i_sub]['value']=$title;
                                            $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                            $menu_items[$i]['sub-menu'][$i_sub]['class']=$row['titleurl'];

                                            $i_sub++;
                                        }

                                        # PHOTOS
                                        $url_sub=$url."/photos";

                                        //$i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_PHOTOS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        $i_sub_sub=0;
                                        $titleurl="1960-1969";
                                        $value=str_replace("-", "&#8211;", $titleurl);
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$value;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub."/#".$titleurl;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$titleurl;

                                        $i_sub_sub++;
                                        $titleurl="1970-1979";
                                        $value=str_replace("-", "&#8211;", $titleurl);
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$value;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub."/#".$titleurl;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$titleurl;

                                        $i_sub_sub++;
                                        $titleurl="1980-1989";
                                        $value=str_replace("-", "&#8211;", $titleurl);
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$value;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub."/#".$titleurl;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$titleurl;

                                        $i_sub_sub++;
                                        $titleurl="1990-1999";
                                        $value=str_replace("-", "&#8211;", $titleurl);
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$value;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub."/#".$titleurl;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$titleurl;

                                        $i_sub_sub++;
                                        $titleurl="2000-".date("Y");
                                        $value=str_replace("-", "&#8211;", $titleurl);
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$value;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub."/#".$titleurl;
                                        $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$titleurl;

                                    # CHRONOLOGY
                                    $i++;
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_CHRONOLOGY;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']="/".$_GET['lang']."/chronology";
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                    # QUOTES
                                    $i++;
                                    $url="/".$_GET['lang']."/quotes";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_QUOTES;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        $query_category_where=" WHERE enable=1 AND NULLIF(sub_categoryid, '') IS NULL ";
                                        $query_category_order=" ORDER BY sort ";
                                        $query_category=QUERIES::query_quotes_categories($db,$query_category_where,$query_category_order);
                                        $results_category=$db->query($query_category['query']);

                                        $i_sub=0;
                                        while( $row=$db->mysql_array($results_category) )
                                        {
                                            $title=UTILS::row_text_value($db,$row,'title');
                                            $url_sub=$url."/".$row['titleurl'];

                                            $menu_items[$i]['sub-menu'][$i_sub]['value']=$title;
                                            $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                            $menu_items[$i]['sub-menu'][$i_sub]['class']=$row['titleurl'];

                                            $i_sub_sub=0;

                                            $query_where_quote_cat_sub=" WHERE enable=1 AND sub_categoryid='".$row['quotes_categoryid']."' ";
                                            $query_order_sub=" ORDER BY sort ";
                                            $query_quote_categories_sub=QUERIES::query_quotes_categories($db,$query_where_quote_cat_sub,$query_order_sub);
                                            $results_quote_categories_sub=$db->query($query_quote_categories_sub['query']);

                                            while( $row_sub=$db->mysql_array($results_quote_categories_sub) )
                                            {
                                                $title=UTILS::row_text_value($db,$row_sub,'title');
                                                $title=strip_tags($title);
                                                $url_sub_sub=$url_sub."/".$row_sub['titleurl'];

                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$title;
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub_sub;
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$row_sub['titleurl'];
                                                $i_sub_sub++;
                                            }

                                            $i_sub++;
                                        }

                                    # EXHIBITIONS
                                    $i++;
                                    $url="/".$_GET['lang']."/exhibitions";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_EXHIBITIONS;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        # 2010-2019
                                        $i_sub=0;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_EXH_ONWARDS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/2010-2019/?year=".date('Y');
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # 2000-2009
                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']="2000 &#8211; 2009";
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/2000-2009/?year=2000";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # 1990-1999
                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']="1990 &#8211; 1999";
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/1990-1999/?year=1990";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # 1980-1989
                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']="1980 &#8211; 1989";
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/1980-1989/?year=1980";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # 1970-1979
                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']="1970 &#8211; 1979";
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/1970-1979/?year=1970";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # 1960-1969
                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']="1960 &#8211; 1969";
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url."/1960-1969/?year=1962";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);


                                    # LITERATURE
                                    $i++;
                                    $url="/".$_GET['lang']."/literature";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_LITERATURE;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        $query_categories_where=" WHERE enable=1 AND ( books_catid=7 OR ( sub_books_catid='' OR sub_books_catid IS NULL OR sub_books_catid=0 ) ) ";
                                        $query_categories_order=" ORDER BY sub_books_catid ASC, sort ASC ";
                                        $query_categories=QUERIES::query_books_categories($db,$query_categories_where,$query_categories_order);
                                        $results_categories=$db->query($query_categories['query']);
                                        $count_categories=$db->numrows($results_categories);

                                        $i_sub=0;
                                        while( $row=$db->mysql_array($results_categories) )
                                        {
                                            $title=UTILS::row_text_value($db,$row,'title');
                                            $url_sub=$url."/".$row['titleurl'];

                                            $menu_items[$i]['sub-menu'][$i_sub]['value']=$title;
                                            $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                            $menu_items[$i]['sub-menu'][$i_sub]['class']=$row['titleurl'];

                                            $i_sub++;
                                        }

                                    # VIDEOS
                                    $i++;
                                    $url="/".$_GET['lang']."/videos";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_VIDEOS;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        $query_where_video_cat=" WHERE enable=1 AND NULLIF(sub_categoryid, '') IS NULL ";
                                        $query_order=" ORDER BY sort_en ";
                                        $query_video_categories=QUERIES::query_videos_categories($db,$query_where_video_cat,$query_order);
                                        $results_video_categories=$db->query($query_video_categories['query']);

                                        $i_sub=0;
                                        while( $row=$db->mysql_array($results_video_categories) )
                                        {
                                            $title=UTILS::row_text_value($db,$row,'title');
                                            $url_sub=$url."/".$row['titleurl'];

                                            $menu_items[$i]['sub-menu'][$i_sub]['value']=$title;
                                            $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                            $menu_items[$i]['sub-menu'][$i_sub]['class']=$row['titleurl'];

                                            $i_sub_sub=0;

                                            $query_where_video_cat_sub=" WHERE enable=1 AND sub_categoryid='".$row['categoryid']."' ";
                                            $query_order_sub=" ORDER BY sort_en ";
                                            $query_video_categories_sub=QUERIES::query_videos_categories($db,$query_where_video_cat_sub,$query_order_sub);
                                            $results_video_categories_sub=$db->query($query_video_categories_sub['query']);

                                            while( $row_sub=$db->mysql_array($results_video_categories_sub) )
                                            {
                                                $title=UTILS::row_text_value($db,$row_sub,'title');
                                                $url_sub_sub=$url_sub."/".$row_sub['titleurl'];

                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value']=$title;
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['value'];
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['url']=$url_sub_sub;
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-sub-menu'][$i_sub_sub]['class']=$row_sub['titleurl'];
                                                $i_sub_sub++;
                                            }

                                            $i_sub++;
                                        }

                                        # AUDIO
                                        //$i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_VIDEO_AUDIO;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']="/".$_GET['lang']."/audio";
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower(LANG_VIDEO_AUDIO);


                                    # LINKS
                                    /*
                                    $i++;
                                    $url="/".$_GET['lang']."/links";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_CONTACT;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        # ARTICLES
                                        $url_sub=$url."/articles";

                                        $i_sub=0;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LINKS_ARTICLES;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                            # ARTICLES CATEGORIES
                                            $url_sub_sub=$url_sub;

                                            $query_order=" ORDER BY sort ";
                                            $query_articles_categories=QUERIES::query_articles_categories($db,"",$query_order);
                                            $results_categories=$db->query($query_articles_categories['query']);

                                            $i_sub_sub=0;
                                            while( $row=$db->mysql_array($results_categories) )
                                            {
                                                $title=UTILS::row_text_value($db,$row,'title');

                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value']=$title;
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['value'];
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['url']=$url_sub_sub."/".$row['titleurl'];
                                                $menu_items[$i]['sub-menu'][$i_sub]['sub-menu'][$i_sub_sub]['class']=$row['titleurl'];

                                                $i_sub_sub++;
                                            }
                                        */
                                        # DEALERS
                                        /*
                                        $url_sub=$url."/dealers";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_LINKS_DEALERS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);
                                        */


                                    # NEWS
                                    $i++;
                                    $url="/".$_GET['lang']."/news";
                                    $menu_items[$i]['value']=LANG_HEADER_MENU_NEWS;
                                    $menu_items[$i]['title']=$menu_items[$i]['value'];
                                    $menu_items[$i]['url']=$url;
                                    $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);

                                        # EXHIBITIONS
                                        $url_sub=$url."/";

                                        $i_sub=0;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_NEWS_EXHIBITIONS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # TALKS
                                        $query_where_talks=" WHERE type=3 AND NOW()<=dateto ";
                                        $query_order_talks=" ORDER BY dateto ASC, datefrom ASC ";
                                        $query_limit_talks=$query." LIMIT 1 ";
                                        $query_talks=QUERIES::query_news($db,$query_where_talks,$query_order_talks,$query_limit_talks);
                                        $results_talks_no_limit=$db->query($query_talks['query_without_limit']);
                                        $count_talks=$db->numrows($results_talks_no_limit);

                                        if( $count_talks>0 )
                                        {
                                            $i_sub++;
                                            $url_sub=$url."/talks";

                                            $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_NEWS_TALKS;
                                            $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                            $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                            $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);
                                        }

                                        # AUCTIONS
                                        $url_sub=$url."/auctions";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_NEWS_AUCTIONS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                        # PUBLICATIONS
                                        $url_sub=$url."/publications";

                                        $i_sub++;
                                        $menu_items[$i]['sub-menu'][$i_sub]['value']=LANG_NEWS_PUBLICATIONS;
                                        $menu_items[$i]['sub-menu'][$i_sub]['title']=$menu_items[$i]['sub-menu'][$i_sub]['value'];
                                        $menu_items[$i]['sub-menu'][$i_sub]['url']=$url_sub;
                                        $menu_items[$i]['sub-menu'][$i_sub]['class']=strtolower($menu_items[$i]['sub-menu'][$i_sub]['value']);

                                    # ADMIN
                                    if( isset($_SESSION['userID']) )
                                    {
                                        $i++;
                                        $menu_items[$i]['value']=LANG_HEADER_MENU_ADMIN;
                                        $menu_items[$i]['title']=$menu_items[$i]['value'];
                                        $menu_items[$i]['url']="/admin";
                                        $menu_items[$i]['class']=strtolower($menu_items[$i]['value']);
                                    }

                                    //print_r($menu_items);

                                    foreach ($menu_items as $i => $value)
                                    {
                                        //print_r($value);

                                        if( empty($value['sub-menu']) ) $class_hidden="hidden";
                                        else $class_hidden="";

                                        $html_print.='<li>';
                                            $html_print.='<a href="'.$value['url'].'" class="section" title="'.$value['title'].'">'.$value['value'].'</a>';

                                            #sub-menu
                                            if( !empty($value['sub-menu']) )
                                            {
                                                $html_print.='<a class="expand '.$class_hidden.'" href="javascript:"></a>';
                                                $html_print.='<ul data-nav-type="sub">';

                                                    foreach ($value['sub-menu'] as $i_sub => $value_sub)
                                                    {
                                                        //print_r($value_sub);

                                                        //$title=strip_tags($value_sub['title']);
                                                        $start_pos=strpos($value_sub['title'], ':');
                                                        if( $start_pos ) $html_value=substr($value_sub['title'], 0, $start_pos);
                                                        else $html_value=$value_sub['title'];
                                                        //print $html_value;
                                                        $html_print.='<li>';
                                                           $html_print.='<a href="'.$value_sub['url'].'" class="section" title="">'.$html_value.'</a>';
                                                        $html_print.='</li>';

                                                        # sub sub menu
                                                        if( !empty($value_sub['sub-sub-menu']) )
                                                        {
                                                            foreach ($value_sub['sub-sub-menu'] as $i_sub_sub => $value_sub_sub)
                                                            {
                                                                //print_r($value_sub_sub);
                                                                $start_pos=strpos($value_sub_sub['title'], ':');
                                                                if( $start_pos ) $html_value=substr($value_sub_sub['title'], 0, $start_pos);
                                                                else $html_value=$value_sub_sub['title'];
                                                                $html_print.='<li class="li-sub-sub">';
                                                                   $html_print.='<a href="'.$value_sub_sub['url'].'" class="section" title="">'.$html_value.'</a>';
                                                                $html_print.='</li>';
                                                            }
                                                        }

                                                    }

                                                $html_print.='</ul>';
                                            }
                                        $html_print.='</li>';
                                    }

                                    $html_print.='</ul>';
                                $html_print.='</nav>';
                            $html_print.='</div><!-- .content -->';
                        $html_print.='</div><!-- #site-navigation -->';
                    $html_print.='</div><!-- #header -->';
                $html_print.='</header>';

                $html_print.='<section class="header"></section>';

            $html_print.='</div>';
        }

        if( !empty($options['id_menu']) )
        {
            $id_menu=$options['id_menu'];
        }
        else
        {
            $id_menu="ul-main-nav";
        }
	    $html_print.="    <ul id='".$id_menu."' class='".$class_ul." ".$options['class']['main-nav']."'>\n";
		switch ($page)
		{
			case 'home':$home = $selected;break;
			case 'artwork':$artwork = $selected;break;
			case 'biography':$biography = $selected;break;
			case 'chronology':$chronology = $selected;break;
			case 'quotes':$quotes = $selected;break;
			case 'exhibitions':$exhibitions = $selected;break;
			case 'literature':$literature = $selected;break;
			case 'videos':$videos = $selected;break;
			case 'audio':$audio = $selected;break;
            //case 'links':$links = $selected;break;
            case 'news':$news = $selected;break;
			case 'search':$search = $selected;break;
			case 'admin':$admin = $selected;break;
			case 'contact':$contact = $selected;break;
			default: $home = " class='selected_none'";
		}
        if( !empty($admin) && !$_SESSION['userID'] ) $home = $selected;
    	$html_print.="\t<li><a href='/".$_GET['lang']."/'".$home." title='".LANG_HEADER_MENU_HOME_TT."'>".LANG_HEADER_MENU_HOME."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/art/'".$artwork." title=\"".LANG_HEADER_MENU_ARTWORK_TT."\">".LANG_HEADER_MENU_ARTWORK."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/biography'".$biography." title='".LANG_HEADER_MENU_BIOGRAPHY_TT."'>".LANG_HEADER_MENU_BIOGRAPHY."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/chronology/'".$chronology." title='".LANG_HEADER_MENU_CHRONOLOGY_TT."'>".LANG_HEADER_MENU_CHRONOLOGY."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/quotes/'".$quotes." title='".LANG_HEADER_MENU_QUOTES_TT."'>".LANG_HEADER_MENU_QUOTES."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/exhibitions/' ".$exhibitions." title='".LANG_HEADER_MENU_EXHIBITIONS_TT."'>".LANG_HEADER_MENU_EXHIBITIONS."</a></li>\n";
        $html_print.="\t<li><a href='/".$_GET['lang']."/literature/' ".$literature." title='".LANG_HEADER_MENU_LITERATURE_TT."'>".LANG_HEADER_MENU_LITERATURE."</a></li>\n";
    	$html_print.="\t<li><a href='/".$_GET['lang']."/videos/'".$videos." title='".LANG_HEADER_MENU_VIDEOS_TT."'>".LANG_HEADER_MENU_VIDEOS."</a></li>\n";
    	//if( !$_SESSION['kiosk'] ) $html_print.="\t<li><a href='/".$_GET['lang']."/links/'".$contact." title='".LANG_HEADER_MENU_CONTACT_TT."'>".LANG_HEADER_MENU_CONTACT."</a></li>\n";
        $html_print.="\t<li><a href='/".$_GET['lang']."/news/'".$news." title='".LANG_HEADER_MENU_NEWS_TT."'>".LANG_HEADER_MENU_NEWS."</a></li>\n";
        if( isset($_SESSION['userID']) ) $html_print.="\t<li><a href='/admin/'".$admin.">".LANG_HEADER_MENU_ADMIN."</a></li>\n";
    	$html_print.="    </ul>\n";
	    if( !$options['no_nav'] )
        {
            $html_print.="    </nav>\n";
        }
        $html_print.="<div class='clearer'></div>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
	}


    function foot($db="",$options=array())
    {
        $html_print="";

        if( !$options['only_meta_info'] )
        {
            $html_print.="\t</div>\n";
            $html_print.="\t<div id='footer' class='wrapper'>\n";

                # copyright
                $html_print.="\t<p class='copyright'>&copy; ".date("Y")." ".LANG_FOOTER_COPYRIGHT."</p>\n";
                # end copyright

                $html_print.="\t<div id='div-footer-right'>\n";

                    $html_print.="\t<div class='div-footer-more-info'>\n";

                        $html_print.="\t<p class='p-footer-more-info disable'>MORE INFORMATION</p>\n";

                        # credits
                        if( $options['selected']==2 ) $class_selected="selected";
                        else $class_selected="";
                        $html_print.="<a href='/".$_GET['lang']."/credits' class='".$class_selected."' title='".LANG_FOOTER_CREDITS."'>".LANG_FOOTER_CREDITS."</a>";
                        # end credits

                        # disclaimer
                        if( $options['selected']==3 ) $class_selected="selected";
                        else $class_selected="";
                        $html_print.="<a href='/".$_GET['lang']."/disclaimer' title='".LANG_FOOTER_DISCLIMER."' class='".$class_selected."'>".LANG_FOOTER_DISCLIMER."</a>";
                        # end disclaimer

                        # contact
                        if( $options['selected']==1 ) $class_selected="selected";
                        else $class_selected="";
                        if( !$_SESSION['kiosk'] ) $html_print.="<a href='/".$_GET['lang']."/contact/' class='".$class_selected."' title='".LANG_FOOTER_CONTACT."'>".LANG_FOOTER_CONTACT."</a>";
                        # end contact

                    $html_print.="\t</div>\n";

                    # follow us on
                    if( !$_SESSION['kiosk'] )
                    {
                        $html_print.="<div class='div-footer-social'>";
                            $options_social=array();
                            $options_social=$options;
                            $options_social['class']['twitter']="twitter";
                            $options_social['class']['facebook']="facebook";
                            $options_social['class']['span-follow-us']="span-footer-follow-us";
                            $html_print.=$this->social_buttons($options_social);
                        $html_print.="\t</div>\n";
                    }
                    # end

                    $html_print.="<div class='clearer'></div>\n";
                $html_print.="\t</div>\n";

                $html_print.="<div class='clearer'></div>\n";
            $html_print.="\t</div>\n";
        }

        if( !$options['html_return'] ) print $html_print;

        $options_footer=array();
        $options_footer=$options;
        $html_print.=$this->footer($db,$options_footer);

        if( $options['html_return'] ) return $html_print;
    }


    function footer($db="",$options=array())
    {
        $html_print="";

		# adding JS files
		for ($i=0;$i<sizeof($this->js_footer);$i++)
		{
			$html_print.="  <script src='".$this->js_footer[$i]."' type='text/javascript'></script>\n";
		}
        #end

		$html_print.="</body>\n";
		$html_print.="</html>";

        unset($db);

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function social_buttons($options=array())
    {
        $html_print="";
        $html_print.="<span class='".$options['class']['span-follow-us']."'>".LANG_NEWS_ALSO_VISIT."</span>";

        if( !empty($options['class']['facebook']) ) $class=$options['class']['facebook'];
        else $class="socal";
        $html_print.="<a href='https://www.facebook.com/gerhardrichterart' target='_blank' class='".$class." ".$options['class']['a-facebook']."'>";
            /*
            if( $options['sliding_title'] )
            {
                $html_print.="<div class='socal_item facebook-slide facebook'>";
                    $html_print.="<p class='socal_item'>facebook</p>";
                $html_print.="</div>";
            }
            */
            $html_print.="Facebook";
        $html_print.="</a>";

        if( $_GET['lang']=='de' ) $url_twitter="https://twitter.com/gerhard_richter";
        else $url_twitter="https://twitter.com/gerhardrichter";
        if( !empty($options['class']['twitter']) ) $class=$options['class']['twitter'];
        else $class="socal";
        $html_print.="<a href='".$url_twitter."' target='_blank' class='".$class." ".$options['class']['a-twitter']."'>";
            /*
            if( $options['sliding_title'] )
            {
                $html_print.="<div class='socal_item twitter-slide twitter'>";
                    $html_print.="<p class='socal_item'>twitter</p>";
                $html_print.="</div>";
            }
            */
            $html_print.="Twitter";
        $html_print.="</a>";


        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function left_side($selected,$options=array())
    {
        $html_print="";
        $html_print.="<div id='sub-nav'>\n";
            $html_print.="<ul id='ul-sub-nav-art'>";
                //if( $selected=="Paintings" || $selected=="allpaintings" || $selected==1 || $selected==2 || $selected==13 || $selected==12 )
                if( $selected=="Paintings" || $selected=="allpaintings" || $selected==1 || $selected==2 || $selected==12 )
                {
                    $worksSubmenu="<li class='li-sub-sub-nav'><ul class='ul-sub-sub-nav'>";

                        if( $this->left_side_menu_selected=="photo-paintings" || $this->left_side_menu_selected==1 ) $class=" class='selected' ";
                        else $class="";
                        $worksSubmenu.="<li $class ><a href='/".$_GET['lang']."/art/paintings/#photo-paintings'>".LANG_ARTWORK_SECTION_PHOTOPAINTINGS."</a></li>";

                        if( $this->left_side_menu_selected=="abstracts" || $this->left_side_menu_selected==2 ) $class=" class='selected' ";
                        else $class="";
                        $worksSubmenu.="<li $class ><a href='/".$_GET['lang']."/art/paintings/#abstracts'>".LANG_ARTWORK_SECTION_ABSTRACTS."</a></li>";

                    $worksSubmenu.="</ul></li>";
                }
                else $worksSubmenu="";

                //if( $selected=="Paintings" || $selected=="allpaintings" || strtolower($_GET['artworkid'])=="paintings" || $selected==1 || $selected==2 || $selected==13 || $selected==12 )
                if( $selected=="Paintings" || $selected=="allpaintings" || strtolower($_GET['artworkid'])=="paintings" || $selected==1 || $selected==2 || $selected==12 )
                {
                    $class="class='selected first has-sub-items'";
                }
                else $class="class='first has-sub-items'";
                $html_print.="<li ".$class." ><a href='/".$_GET['lang']."/art/paintings'>".LANG_LEFT_SIDE_PAINTINGS."</a>$worksSubmenu</li>\n";

                if( $selected=="Atlas" || $_GET['artworkid']==3 || $selected==3 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/atlas'>".LANG_LEFT_SIDE_ATLAS."</a></li>\n";

                if( $selected=="Editions" || $_GET['artworkid']==4 || $selected==4 )
                {
                    $class="class='selected'";
                }
                else $class="";
                if( !$_SESSION['kiosk'] ) $html_print.="<li $class ><a href='/".$_GET['lang']."/art/editions'>".LANG_LEFT_SIDE_EDITIONS."</a></li>";

                if( $selected=="Drawings" || $_GET['artworkid']==7 || $selected==7 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/drawings'>".LANG_LEFT_SIDE_DRAWINGS."</a></li>";

                if( $selected=="Overpainted-photographs" || $_GET['artworkid']==6 || $selected==6 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/overpainted-photographs'>".LANG_LEFT_SIDE_OVERPAINTEDPHOTOGRAPHS."</a></li>";

                if( $selected=="oils-on-paper" || $_GET['artworkid']==8 || $selected==8 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/oils-on-paper'>".LANG_LEFT_SIDE_OILSONPAPER."</a></li>";

                if( $selected=="Watercolours" || $_GET['artworkid']==5 || $selected==5 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/watercolours'>".LANG_LEFT_SIDE_WATERCOLOURS."</a></li>";

                /*
                if( $selected=="Other" || $_GET['artworkid']==15 || $selected==15 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/other'>".LANG_ARTWORK_SECTION_OTHERMEDIA."</a></li>";
                */

                $html_print.="<li><a href='/".$_GET['lang']."/literature/artists-books'>".LANG_ARTWORK_ARTISTS_BOOKS."</a></li>";

                if( $selected=="other" || $_GET['artworkid']==16 || $selected==16 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/prints'>".LANG_LEFT_SIDE_PRINTS."</a></li>";

                if( $selected=="other" || $_GET['artworkid']==13 || $selected==13 )
                {
                    $class="class='selected'";
                }
                else $class="";
                $html_print.="<li $class ><a href='/".$_GET['lang']."/art/other'>".LANG_ARTWORK_SECTION_OTHERMEDIA."</a></li>";

            $html_print.="</ul>\n";

            # showing related box
            if( $options['show_microsites'] && !$_SESSION['kiosk'] )
            {
                # microsites
                /*
                $html_print.="<div class='div-related-block'>\n";
                    $html_print.="<p class='p-related-block-title'>".LANG_RIGHT_FEATURED_TITLES."</p>\n";
                    $html_print.="<ul class='ul-related-block'>\n";
                        $results_microsites=$options['db']->query("SELECT * FROM ".TABLE_MICROSITES." WHERE enable=1 ORDER BY sort ");
                        while( $row_microsites=$options['db']->mysql_array($results_microsites) )
                        {
                            $title=UTILS::row_text_value($options['db'],$row_microsites,"title");
                            $html_print.="<li>";
                               $html_print.=" <a href='/".$_GET['lang']."/art/microsites/".$row_microsites['titleurl']."' target='_blank' title='Opens ".$title." in a new window'>".$title."</a>";
                            $html_print.="</li>\n";
                        }
                    $html_print.="</ul>\n";
                $html_print.="</div>\n";
                */

                $results_microsites=$options['db']->query("SELECT * FROM ".TABLE_MICROSITES." WHERE enable=1 ORDER BY sort ");
                $count_microsites=$options['db']->numrows($results_microsites);

                $options_related=array();
                $options_related=$options;
                $options_related['title']=LANG_RIGHT_FEATURED_TITLES;
                $options_related['results']=$results_microsites;
                $options_related['count']=$count_microsites;
                $options_related['url']="/".$_GET['lang']."/art/microsites";
                $options_related['target']=1;
                $html_print.=$this->div_related_block($options['db'],$options_related);

                # short clip videos
                /*
                $query_where_quote=" WHERE categoryid=4 ";
                $query_quote=QUERIES::query_videos_categories($db,$query_where_quote,""," LIMIT 1 ");
                $results=$options['db']->query($query_quote['query']);
                $count=$options['db']->numrows($results);
                $row=$options['db']->mysql_array($results);
                $title=UTILS::row_text_value($options['db'],$row,"title");
                $desc=UTILS::row_text_value($options['db'],$row,"desc");

                $html_print.="<div class='div-related-block'>\n";
                    $html_print.="<p class='p-related-block-title'>".$title."</p>\n";
                    if( !empty($desc) )
                    {
                        $options_div_text=array();
                        $options_div_text['class']['div_text_wysiwyg']="div-related-block-desc";
                        $html_print.=$this->div_text_wysiwyg($db,$desc,$options_div_text);
                    }
                    $html_print.="<ul class='ul-related-block'>\n";
                        $html_print.="<li>";
                           $html_print.=" <a href='/".$_GET['lang']."/videos/".$row['titleurl']."' target='_blank' title=''>".LANG_RELATED_BLOCK_BROWSE_CLIPS."</a>";
                        $html_print.="</li>\n";
                    $html_print.="</ul>\n";
                $html_print.="</div>\n";
                */


                $query_where_quote=" WHERE categoryid=4 ";
                $query_quote=QUERIES::query_videos_categories($db,$query_where_quote,""," LIMIT 1 ");
                $results=$options['db']->query($query_quote['query']);
                $count_videos=$options['db']->numrows($results);
                $row=$options['db']->mysql_array($results);
                $title=UTILS::row_text_value($options['db'],$row,"title");
                $desc=UTILS::row_text_value($options['db'],$row,"desc");

                $options_related=array();
                $options_related=$options;
                $options_related['title']=$title;
                $options_related['desc']=$desc;
                $options_related['items'][0]['title']=LANG_RELATED_BLOCK_BROWSE_CLIPS;
                $options_related['items'][0]['url']="/".$_GET['lang']."/videos/".$row['titleurl'];
                //$options_related['target']=1;
                $options_related['count']=$count_videos;
                $html_print.=$this->div_related_block($options['db'],$options_related);

            }

        $html_print.="</div>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }
	#end left side function

    function div_related_block($db,$options=array())
    {
        $html_print="";

        if( $options['count']>0 )
        {
            $html_print.="<div class='div-related-block'>\n";
                $html_print.="<p class='p-related-block-title'>".$options['title']."</p>\n";
                if( !empty($options['desc']) )
                {
                    $options_div_text=array();
                    $options_div_text['class']['div_text_wysiwyg']="div-related-block-desc";
                    $html_print.=$this->div_text_wysiwyg($db,$options['desc'],$options_div_text);
                }
                $html_print.="<ul class='ul-related-block'>\n";
                    if( count($options['items'])>0 )
                    {
                        for ($i=0; $i < count($options['items']); $i++)
                        {
                            $html_print.="<li>";
                                if( $options['target'] ) $target="target='_blank'";
                                else $target="";
                                $html_print.=" <a href='".$options['items'][$i]['url']."' ".$target." >".$options['items'][$i]['title']."</a>";
                            $html_print.="</li>\n";
                        }
                    }
                    elseif( $options['count']>0 )
                    {
                        while( $row=$db->mysql_array($options['results']) )
                        {
                            //print_r($row);
                            if( !empty($options['title_column']) )
                            {
                                $title=$row[$options['title_column']];
                            }
                            else
                            {
                                $title=UTILS::row_text_value($db,$row,"title");
                            }
                            if( !empty($row['date_display_year']) )
                            {
                                $title.=" / ".$row['date_display_year'];
                            }
                            $html_print.="<li>";
                                if( $options['target'] ) $target="target='_blank'";
                                else $target="";
                                if( $options['bookid'] )
                                {
                                    $options_book_url=array();
                                    $options_book_url['bookid']=$row['id'];
                                    $url_literature=UTILS::get_literature_url($db,$options_book_url);
                                    $url=$url_literature['url'];
                                }
                                else $url=$options['url']."/".$row['titleurl'];
                                $html_print.=" <a href='".$url."' ".$target." >".$title."</a>";
                            $html_print.="</li>\n";
                        }
                    }
                $html_print.="</ul>\n";
            $html_print.="</div>\n";

        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function ul_exhibitions($db,$options=array())
    {
        $html_print="";

        $html_print.="<div id='sub-nav'>\n";
            $html_print.="<ul id='ul-biography'>\n";
                $years_array[0]['section']="2010-2019";
                $years_array[0]['url']="2010-2019/?year=".date('Y');$years_array[0]['title']=LANG_EXH_ONWARDS;
                $years_array[1]['section']="2000-2009";
                $years_array[1]['url']="2000-2009/?year=2000";$years_array[1]['title']="2000 &#8211; 2009";
                $years_array[2]['section']="1990-1999";
                $years_array[2]['url']="1990-1999/?year=1990";$years_array[2]['title']="1990 &#8211; 1999";
                $years_array[3]['section']="1980-1989";
                $years_array[3]['url']="1980-1989/?year=1980";$years_array[3]['title']="1980 &#8211; 1989";
                $years_array[4]['section']="1970-1979";
                $years_array[4]['url']="1970-1979/?year=1970";$years_array[4]['title']="1970 &#8211; 1979";
                $years_array[5]['section']="1960-1969";
                $years_array[5]['url']="1960-1969/?year=1962";$years_array[5]['title']="1960 &#8211; 1969";

                foreach( $years_array as $key => $value )
                {
                    if( $options['section_1']==$value['section'] ) $selected="selected";
                    else $selected="";
                    if( $key==0 ) $class_first="first";
                    else $class_first="";
                    $html_print.="<li class='".$class_first." ".$selected."'><a href='/".$_GET['lang']."/exhibitions/".$value['url']."'>".$value['title']."</a></li>\n";
                }
            $html_print.="</ul>\n";
        $html_print.="</div>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function ul_links($db,$options=array())
    {
        $html_print.="<ul id='ul-biography'>\n";
            if( $options['selectedid']==1 ) $selected="selected";
            else $selected="";
            $html_print.="<li class='first ".$selected." has-sub-items'><a href='/".$_GET['lang']."/links/articles'>".LANG_LINKS_ARTICLES."</a></li>\n";
            if( $options['selectedid']==1 )
            {
                $html_print.="<li class='li-sub-sub-nav'><ul class='ul-sub-sub-nav'>\n";
                    $i=0;

                    $query_order=" ORDER BY sort ";
                    $query_articles_categories=QUERIES::query_articles_categories($db,"",$query_order);
                    $results=$db->query($query_articles_categories['query']);

                    while( $row=$db->mysql_array($results) )
                    {
                        $row=UTILS::html_decode($row);
                        $i++;
                        $title=UTILS::row_text_value($db,$row,'title');
                        if( $options['section_2']==$row['titleurl'] || $options['section_2']==$row['titleurl'] )
                        {
                            $selected="selected";
                            $return['titleurl']=$titleurl;
                            $return['title']=$title;
                        }
                        else $selected="";
                        if( $i==1 ) $first="first";
                        else $first="";
                        $html_print.="<li class='".$firs." ".$selected."'><a href='/".$_GET['lang']."/links/articles/".$row['titleurl']."' title=''>".$title."</a></li>\n";
                    }
                $html_print.="</ul></li>\n";
            }
            /*
            if( $options['selectedid']==2 ) $selected="selected";
            else $selected="";
            $html_print.="<li class='".$selected."'><a href='/".$_GET['lang']."/links/dealers'>".LANG_LINKS_DEALERS."</a></li>\n";
            */
        $html_print.="</ul>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

	function ul_videos($db,$selectedid,$options=array())
    {
        $html_print="";

        $html_print.="<ul id='ul-biography'>\n";
            if( $selectedid==1 ) $selected="selected";
            else $selected="";
            $html_print.="<li class='first ".$selected." has-sub-items'><a href='/".$_GET['lang']."/videos/'>".LANG_VIDEO_VIDEOS."</a></li>\n";
            if( $selectedid==1 )
            {
                $html_print.="<li class='li-sub-sub-nav'><ul class='ul-sub-sub-nav'>\n";

                /*
                    $i=0;
                    $query_where_video_cat=" WHERE enable=1 ";
                    $query_order=" ORDER BY sort_en ";
                    $query_video_categories=QUERIES::query_videos_categories($db,$query_where_video_cat,$query_order);
                    $results_video_categories=$db->query($query_video_categories['query']);
                    while( $row=$db->mysql_array($results_video_categories) )
                    {
                        $row=UTILS::html_decode($row);
                        $i++;
                        $title=UTILS::row_text_value($db,$row,'title');
                        if( $options['categoryid']==$row['categoryid'] ) $selected="selected";
                        else $selected="";
                        if( $i==1 ) $first="first";
                        else $first="";
                        $html_print.="<li class='".$firs." ".$selected."'><a href='/".$_GET['lang']."/videos/".$row['titleurl']."' title='".$title."'>".$title."</a></li>\n";
                    }
                */

                    $i=0;
                    $query_where_video_cat=" WHERE enable=1 AND NULLIF(sub_categoryid, '') IS NULL ";
                    $query_order=" ORDER BY sort_en ";
                    $query_video_categories=QUERIES::query_videos_categories($db,$query_where_video_cat,$query_order);
                    $results_categories=$db->query($query_video_categories['query']);
                    $count_categories=$db->numrows($results_categories);

                    $i=0;
                    $ii=0;
                    $categories=array();
                    while( $row=$db->mysql_array($results_categories) )
                    {
                        $categories[]=$row;
                    }

                    foreach( $categories as $row )
                    {
                        $query_category_sub_where=" WHERE enable=1 AND sub_categoryid='".$row['categoryid']."' ";
                        $query_category_sub_order=" ORDER BY sort_en ";
                        $query_category_sub=QUERIES::query_videos_categories($db,$query_category_sub_where,$query_category_sub_order);
                        $results_category_sub=$db->query($query_category_sub['query']);
                        $count_sub=$db->numrows($results_category_sub);

                        $sub_categories=array();
                        while( $row_sub=$db->mysql_array($results_category_sub) )
                        {
                            $sub_categories[]=$row_sub;

                            if( $options['categoryid']==$row_sub['categoryid'] )
                            {
                                $return['categories_sub']=$sub_categories;
                                $return['count_sub_categories']=$count_sub;
                            }
                        }

                        $title=UTILS::row_text_value($db,$row,'title');
                        $url="/".$_GET['lang']."/videos/".$row['titleurl'];
                        if( $row['categoryid']==$options['categoryid'] || $row['categoryid']==$options['sub_categoryid'] || $row['categoryid']==$_GET['categoryid'] ) $class=" selected ";
                        else $class="";
                        if( $i==0 ) $class_li=" first ";
                        else $class_li="";
                        if( $count_sub>0 ) $class_li_has_sub="has-sub-items";
                        else $class_li_has_sub="";

                        $html_print.="<li class='".$class." ".$class_li." ".$class_li_has_sub."'>";
                            $html_print.="<a href='".$url."' title='' class='".$class."'>".$title."</a>\n";
                            /*
                            $options_admin_edit=array();
                            $options_admin_edit=$options;
                            $html_print.=UTILS::admin_edit("/admin/videos/categories/edit/?categoryid=".$row['categoryid'],$options_admin_edit);
                            */
                        $html_print.="</li>\n";

                        # sub categories
                        if( $row['categoryid']==$options['categoryid'] || $row['categoryid']==$options['sub_categoryid'] || $row['categoryid']==$_GET['categoryid'] )
                        {
                            if( $count_sub>0 )
                            {
                                $html_print.="<li class='li-sub-sub-nav'>";
                                    $html_print.="<ul class='ul-sub-sub-sub-nav'>\n";
                                        foreach($sub_categories as $row_sub)
                                        {
                                            $title=UTILS::row_text_value($db,$row_sub,'title');
                                            $url_sub=$url."/".$row_sub['titleurl'];
                                            if( $row_sub['categoryid']==$options['categoryid'] ) $class=" selected ";
                                            else $class="";
                                            $html_print.="<li>";
                                                $html_print.="<a href='".$url_sub."' title='' class='".$class."'>".$title."</a>\n";
                                                /*
                                                $options_admin_edit=array();
                                                $options_admin_edit=$options;
                                                $html_print.=UTILS::admin_edit("/admin/videos/categories/edit/?categoryid=".$row_sub['categoryid'],$options_admin_edit);
                                                */
                                            $html_print.="</li>\n";
                                        }
                                    $html_print.="</ul>\n";
                                $html_print.="</li>\n";
                            }
                        }
                        $i++;
                    }

                $html_print.="</ul></li>\n";

            }
            if( $selectedid==2 ) $selected="selected";
            else $selected="";
            $html_print.="<li class='".$selected."'><a href='/".$_GET['lang']."/audio'>".LANG_VIDEO_AUDIO."</a></li>\n";
        $html_print.="</ul>\n";

        $return['return']=$html_print;
        $return['categories']=$categories;
        $return['count_categories']=$count_categories;
        $return['sub_categories']=$sub_categories;
        $return['count_sub_categories']=$count_sub;

        if( $options['html_return'] ) return $return;
        else print $html_print;
    }

	function ul_chronology($db,$selectedid="",$options=array())
    {
        $html_print="";

        $html_print.="<ul id='ul-biography'>\n";
            $selected="selected";
            $html_print.="<li class='first ".$selected." has-sub-items'><a href='/chronology'>".LANG_CHRONOLOGY_H1_1."</a></li>\n";
                $html_print.="<li class='li-sub-sub-nav'>\n";
                    $html_print.="<ul class='ul-sub-sub-nav'>\n";
                        $html_print.="<li class='first'>";
                            $url="#1930";
                            $title=LANG_CHRONOLOGY_LEFT_1930;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class=''>";
                            $url="#1940";
                            $title=LANG_CHRONOLOGY_LEFT_1940;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#1950";
                            $title=LANG_CHRONOLOGY_LEFT_1950;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#1960";
                            $title=LANG_CHRONOLOGY_LEFT_1960;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#1970";
                            $title=LANG_CHRONOLOGY_LEFT_1970;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#1980";
                            $title=LANG_CHRONOLOGY_LEFT_1980;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#1990";
                            $title=LANG_CHRONOLOGY_LEFT_1990;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#2000";
                            $title=LANG_CHRONOLOGY_LEFT_2000;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                        $html_print.="<li class='first'>";
                            $url="#2010";
                            $title=LANG_CHRONOLOGY_LEFT_2010;
                            $html_print.="<a href='".$url."' title='".$title."' >".$title."</a>\n";
                        $html_print.="</li>\n";
                    $html_print.="</ul>\n";
                $html_print.="</li>\n";
            $html_print.="<li class=''><a>&nbsp;</a></li>\n";
        $html_print.="</ul>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

	function ul_biography($db,$selectedid,$sectionid="",$options=array())
    {
        $html_print="";

        $url="/".$_GET['lang']."/biography";

        $html_print.="<ul id='ul-biography'>\n";
            if( $selectedid!="photos" ) $selected="selected";
            else $selected="";
            $html_print.="<li class='first ".$selected." has-sub-items'><a href='".$url."'>".LANG_BIOGRAPHY."</a></li>\n";
            if( $selectedid!="photos" )
            {
                $i=0;
                $results=$db->query("SELECT * FROM ".TABLE_BIOGRAPHY." WHERE enable=1 ORDER BY sort ");
                $count=$db->numrows($results);
                if( $count )
                {
                    $html_print.="<li class='li-sub-sub-nav'>\n";
                        $html_print.="<ul class='ul-sub-sub-nav'>\n";
                            while( $row=$db->mysql_array($results) )
                            {
                                $title=UTILS::row_text_value($db,$row,'title');
                                $title_left=UTILS::row_text_value($db,$row,'title_left');
                                $url_biography=$url."/".$row['titleurl'];
                                if( $row['biographyid']==$selectedid ) $class=" selected ";
                                else $class="";
                                if( $i==0 ) $class_li=" first ";
                                else $class_li="";
                                $html_print.="<li class='".$class_li." ".$class."'>";
                                    $html_print.="<a href='".$url_biography."' title='".$title."' class='".$class."'>".$title_left."</a>\n";
                                    $options_admin_edit=array();
                                    $options_admin_edit=$options;
                                    $html_print.=UTILS::admin_edit("/admin/biography/edit/?biographyid=".$row['biographyid'],$options_admin_edit);
                                $html_print.="</li>\n";
                                $i++;
                            }
                        $html_print.="</ul>\n";
                    $html_print.="</li>\n";
                }
            }

            # PHOTOS section
            $url.="/photos";
            if( $selectedid=="photos" ) $class="selected";
            else $class="";
            $html_print.="<li class='".$class." has-sub-items'><a href='".$url."'>".LANG_PHOTOS."</a></li>\n";
            if( $selectedid=="photos" )
            {
                $html_print.="<li class='li-sub-sub-nav'>\n";
                    $html_print.="<ul class='ul-sub-sub-nav'>\n";
                        if( $sectionid=="1960-1969" ) $class="selected";
                        else $class="";
                        $html_print.="<li class='".$class."'>";
                            $url_photo=$url."/#1960-1969";
                            $title="1960&#8211;1969";
                            $html_print.="<a href='".$url_photo."' title='".$title."' class=''>".$title."</a>\n";
                        $html_print.="</li>\n";
                        if( $sectionid=="1970-1979" ) $class="selected";
                        else $class="";
                        $html_print.="<li class='".$class."'>";
                            $url_photo=$url."/#1970-1979";
                            $title="1970&#8211;1979";
                            $html_print.="<a href='".$url_photo."' title='".$title."' class=''>".$title."</a>\n";
                        $html_print.="</li>\n";
                        if( $sectionid=="1980-1989" ) $class="selected";
                        else $class="";
                        $html_print.="<li class='".$class."'>";
                            $url_photo=$url."/#1980-1989";
                            $title="1980&#8211;1989";
                            $html_print.="<a href='".$url_photo."' title='".$title."' class=''>".$title."</a>\n";
                        $html_print.="</li>\n";
                        if( $sectionid=="1990-1999" ) $class="selected";
                        else $class="";
                        $html_print.="<li class='".$class."'>";
                            $url_photo=$url."/#1990-1999";
                            $title="1990&#8211;1999";
                            $html_print.="<a href='".$url_photo."' title='".$title."' class=''>".$title."</a>\n";
                        $html_print.="</li>\n";
                        if( $sectionid=="2000-".date("Y") ) $class="selected";
                        else $class="";
                        $html_print.="<li class='".$class."'>";
                            $url_photo=$url."/#2000-".date("Y");
                            $title="2000&#8211;".date("Y");
                            $html_print.="<a href='".$url_photo."' title='".$title."' class=''>".$title."</a>\n";
                        $html_print.="</li>\n";
                    $html_print.="</ul>\n";
                $html_print.="</li>\n";
            }
            # *** END *** PHOTOS section

        $html_print.="</ul>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }


	function ul_quotes($db,$selectedid,$options=array())
    {
        $return=array();
        $html_print="";

        if( $selectedid==3 ) $class="selected";
        else $class="";
        if( $selectedid==3 )
        {
            $html_print.="<ul id='ul-biography'>\n";
                $i=0;
                $query_category_where=" WHERE enable=1 AND sub_categoryid='' ";
                $query_category_order=" ORDER BY sort ";
                $query_category=QUERIES::query_quotes_categories($db,$query_category_where,$query_category_order);
                $results_categories=$db->query($query_category['query']);
                $count_categories=$db->numrows($results_categories);

                $i=0;
                $ii=0;
                $categories=array();
                while( $row=$db->mysql_array($results_categories) )
                {
                    $categories[]=$row;
                }

                foreach( $categories as $row )
                {

                //while( $row=$db->mysql_array($results_category) )
                //{
                    $query_category_sub_where=" WHERE enable=1 AND sub_categoryid='".$row['quotes_categoryid']."' ";
                    $query_category_sub_order=" ORDER BY sort ";
                    $query_category_sub=QUERIES::query_quotes_categories($db,$query_category_sub_where,$query_category_sub_order);
                    $results_category_sub=$db->query($query_category_sub['query']);
                    $count_sub=$db->numrows($results_category_sub);

                    $sub_categories=array();
                    while( $row_sub=$db->mysql_array($results_category_sub) )
                    {
                        $sub_categories[]=$row_sub;

                        if( $options['categoryid']==$row_sub['sub_categoryid'] )
                        {
                            $return['categories_sub']=$sub_categories;
                            $return['count_sub_categories']=$count_sub;
                        }
                    }


                    $title=UTILS::row_text_value($db,$row,'title');
                    $url="/".$_GET['lang']."/quotes/".$row['titleurl'];
                    if( $row['quotes_categoryid']==$options['categoryid'] ) $class=" selected ";
                    else $class="";
                    if( $i==0 ) $class_li=" first ";
                    else $class_li="";
                    if( $count_sub>0 ) $class_li_has_sub="has-sub-items";
                    else $class_li_has_sub="";

                    $html_print.="<li class='".$class." ".$class_li." ".$class_li_has_sub."'>";
                        $html_print.="<a href='".$url."' title='' class='".$class."'>".$title."</a>\n";
                        $options_admin_edit=array();
                        $options_admin_edit=$options;
                        $html_print.=UTILS::admin_edit("/admin/quotes/categories/edit/?quotes_categoryid=".$row['quotes_categoryid'],$options_admin_edit);
                    $html_print.="</li>\n";

                    if( $row['quotes_categoryid']==$options['categoryid'] )
                    {

                        if( $count_sub>0 )
                        {
                            $html_print.="<li class='li-sub-sub-nav'>";
                                $html_print.="<ul class='ul-sub-sub-nav'>\n";
                                    foreach($sub_categories as $row_sub)
                                    //while( $row_sub=$db->mysql_array($results_category_sub) )
                                    {
                                        $title=UTILS::row_text_value($db,$row_sub,'title');
                                        $url_sub=$url."/".$row_sub['titleurl'];
                                        if( $row_sub['quotes_categoryid']==$options['categoryid_sub'] ) $class=" selected ";
                                        else $class="";
                                        $html_print.="<li>";
                                            $html_print.="<a href='".$url_sub."' title='' class='".$class."'>".$title."</a>\n";
                                            $options_admin_edit=array();
                                            $options_admin_edit=$options;
                                            $html_print.=UTILS::admin_edit("/admin/quotes/categories/edit/?quotes_categoryid=".$row_sub['quotes_categoryid'],$options_admin_edit);
                                        $html_print.="</li>\n";
                                    }
                                $html_print.="</ul>\n";
                            $html_print.="</li>\n";
                        }
                    }
                    $i++;
                }
            $html_print.="</ul>\n";
        }

        $return['return']=$html_print;
        $return['categories']=$categories;
        $return['count_categories']=$count_categories;

        if( $options['html_return'] ) return $return;
        else print $html_print;
    }

    function ul_literature($db,$selectedid,$options=array())
    {
        $return=array();
        $html_print="";
        //print_r($options);

        $html_print.="<ul id='ul-biography'>\n";
            $query_categories_where=" WHERE enable=1 AND ( books_catid=7 OR ( sub_books_catid='' OR sub_books_catid IS NULL OR sub_books_catid=0 ) ) ";
            $query_categories_order=" ORDER BY sub_books_catid ASC, sort ASC ";
            $query_categories=QUERIES::query_books_categories($db,$query_categories_where,$query_categories_order);
            $results_categories=$db->query($query_categories['query']);
            $count_categories=$db->numrows($results_categories);

            $i=0;
            $ii=0;
            $categories=array();
            while( $row=$db->mysql_array($results_categories) )
            {
                $categories[]=$row;
            }

            foreach( $categories as $row )
            {
                if( $row['books_catid']!=7 ) # films category is excluded from left submenu
                {
                    $ii++;
                    $query_category_sub_where=" WHERE enable=1 AND sub_books_catid='".$row['books_catid']."' ";
                    $query_category_sub_order=" ORDER BY sort ";
                    if( $row['books_catid']==16 ) $query_category_sub_order.=" DESC ";
                    $query_category_sub=QUERIES::query_books_categories($db,$query_category_sub_where,$query_category_sub_order);
                    $results_category_sub=$db->query($query_category_sub['query']);
                    $count_sub=$db->numrows($results_category_sub);
                    $sub_categories=array();
                    while( $row_sub=$db->mysql_array($results_category_sub) )
                    {
                        $sub_categories[]=$row_sub;
                        if( $options['categoryid']==$row_sub['sub_books_catid'] )
                        {
                            $return['categories_sub']=$sub_categories;
                            $return['count_sub_categories']=$count_sub;
                        }
                    }

                    $title=UTILS::row_text_value($db,$row,'title');
                    //if( $count_sub ) $url="/".$_GET['lang']."/literature/".$row['titleurl']."/".$sub_categories[0]['titleurl'];
                    //else $url="/".$_GET['lang']."/literature/".$row['titleurl'];
                    $url="/".$_GET['lang']."/literature/".$row['titleurl'];
                    if( $row['titleurl']==$_GET['section_1'] ) $class=" selected ";
                    else $class="";
                    if( $i==0 ) $class_li=" first ";
                    else $class_li="";
                    if( $ii==$count_categories ) $class_last=" last ";
                    else $class_last="";
                    if( $count_sub>0 ) $class_li_has_sub="has-sub-items";
                    else $class_li_has_sub="";

                    $html_print.="<li class='".$class_li." ".$class_last." ".$class_li_has_sub." ".$class."'>";
                        $html_print.="<a href='".$url."' title='' class='".$class."'>".$title."</a>\n";
                    $html_print.="</li>\n";
                    $options_admin_edit=array();
                    $options_admin_edit=$options;
                    $html_print.=UTILS::admin_edit("/admin/books/categories/edit/?books_catid=".$row['books_catid'],$options_admin_edit)."\n";

                    if( $row['titleurl']==$_GET['section_1'] )
                    {
                        if( $count_sub>0 )
                        {
                            $html_print.="<li class='li-sub-sub-nav'>";
                                $html_print.="<ul class='ul-sub-sub-nav'>\n";
                                    foreach($sub_categories as $row_sub)
                                    {
                                        $title=UTILS::row_text_value($db,$row_sub,'title');
                                        $url_sub="/".$_GET['lang']."/literature/".$row['titleurl']."/".$row_sub['titleurl'];
                                        if( $row_sub['titleurl']==$_GET['section_2'] ) $class=" selected ";
                                        else $class="";
                                        $html_print.="<li>";
                                            $html_print.="<a href='".$url_sub."' title='' class='".$class."'>".$title."</a>\n";
                                            $options_admin_edit=array();
                                            $options_admin_edit=$options;
                                            $html_print.=UTILS::admin_edit("/admin/books/categories/edit/?books_catid=".$row_sub['books_catid'],$options_admin_edit)."\n";
                                        $html_print.="</li>\n";
                                    }
                                $html_print.="</ul>\n";
                            $html_print.="</li>\n";
                        }
                    }
                    $i++;
                }
            }

        $html_print.="</ul>\n";

        $return['return']=$html_print;
        $return['categories']=$categories;
        $return['count_categories']=$count_categories;

        if( $options['html_return'] ) return $return;
        else print $html_print;
    }

    function ul_news($db,$options=array())
    {
        $html_print.="<ul id='ul-biography'>\n";
            if( $options['selectedid']==1 ) $class_selected="selected";
            else $class_selected="";
            $html_print.="<li class='first ".$class_selected."'><a href='/".$_GET['lang']."/news' title='".LANG_NEWS_EXHIBITIONS."'>".LANG_NEWS_EXHIBITIONS."</a></li>\n";
            if( $options['selectedid']==3 ) $class_selected="selected";
            else $class_selected="";
            $html_print.="<li class='".$class_selected."'><a href='/".$_GET['lang']."/news/auctions' title='".LANG_NEWS_AUCTIONS."'>".LANG_NEWS_AUCTIONS."</a></li>\n";
            if( $options['selectedid']==4 ) $class_selected="selected";
            else $class_selected="";
            $html_print.="<li class='".$class_selected."'><a href='/".$_GET['lang']."/news/publications' title='".LANG_NEWS_PUBLICATIONS."'>".LANG_NEWS_PUBLICATIONS."</a></li>\n";
            # talks
            $query_where_talks=" WHERE type=3 AND NOW()<=dateto ";
            $query_order_talks=" ORDER BY dateto ASC, datefrom ASC ";
            //$query_order_talks=" ORDER BY datefrom DESC, dateto DESC ";
            $query_limit_talks=$query." LIMIT 1 ";
            $query_talks=QUERIES::query_news($db,$query_where_talks,$query_order_talks,$query_limit_talks);
            $results_talks_no_limit=$db->query($query_talks['query_without_limit']);
            $count_talks=$db->numrows($results_talks_no_limit);
            if( $options['selectedid']==2 ) $class_selected="selected";
            else $class_selected="";
            if( $count_talks>0 ) $html_print.="<li class='".$class_selected."'><a href='/".$_GET['lang']."/news/talks' title='".LANG_NEWS_TALKS."'>".LANG_NEWS_TALKS."</a></li>\n";
            # talks end
        $html_print.="</ul>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function ul_basic_search($db,$options)
    {
        $html_print="";

        /*
        print "\t<ul id='ul-biography'>\n";
            if( !$options['count_paintings'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='first ".$class."'>\n";
                if( $options['count_paintings'] ) $href="href='#art'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_ART."'>\n";
                    print LANG_SEARCH_ART."<span class='span-basic-search-left-count'>(".$options['count_paintings'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_microsites'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_microsites'] ) $href="href='#microsites'";
                else $href="";
                print "\t<a ".$href." title='".LANG_LEFT_SIDE_MICROSITES."'>\n";
                    print LANG_LEFT_SIDE_MICROSITES."<span class='span-basic-search-left-count'>(".$options['count_microsites'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_biography'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_biography'] ) $href="href='#biography'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_BIOGRAPHY."'>\n";
                    print LANG_SEARCH_BIOGRAPHY."<span class='span-basic-search-left-count'>(".$options['count_biography'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_text_chronology'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_text_chronology'] ) $href="href='#chronology'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_CHRONOLOGY."'>\n";
                    print LANG_SEARCH_CHRONOLOGY."<span class='span-basic-search-left-count'>(".$options['count_text_chronology'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_quotes'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_quotes'] ) $href="href='#quotes'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_QUOTES."'>\n";
                    print LANG_SEARCH_QUOTES."<span class='span-basic-search-left-count'>(".$options['count_quotes'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_exhibitions'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_exhibitions'] ) $href="href='#exhibitions'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_EXHIBITIONS."'>\n";
                    print LANG_SEARCH_EXHIBITIONS."<span class='span-basic-search-left-count'>(".$options['count_exhibitions'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_literature'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_literature'] ) $href="href='#literature'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_LITERATURE."'>\n";
                    print LANG_SEARCH_LITERATURE."<span class='span-basic-search-left-count'>(".$options['count_literature'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_videos'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_videos'] ) $href="href='#videos'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_VIDEOS."'>\n";
                    print LANG_SEARCH_VIDEOS."<span class='span-basic-search-left-count'>(".$options['count_videos'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_links'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_links'] ) $href="href='#links'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_LINKS."'>\n";
                    print LANG_SEARCH_LINKS."<span class='span-basic-search-left-count'>(".$options['count_links'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( $options['count_news_talks'] )
            {
                if( !$options['count_news_talks'] ) $class="li-basic-search-color-0-results";
                else $class="";
                print "\t<li class='".$class."'>\n";
                    if( $options['count_news_talks'] ) $href="href='#talks'";
                    else $href="";
                    print "\t<a ".$href." title='".LANG_SEARCH_NEWS_TALKS."'>\n";
                        print LANG_SEARCH_NEWS_TALKS."<span class='span-basic-search-left-count'>(".$options['count_news_talks'].")</span>\t\n";
                    print "\t</a>\n";
                print "\t</li>\n";
            }
            if( !$options['count_news_auctions'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_news_auctions'] ) $href="href='#auctions'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_NEWS_AUCTIONS."'>\n";
                    print LANG_SEARCH_NEWS_AUCTIONS."<span class='span-basic-search-left-count'>(".$options['count_news_auctions'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_text_credits'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_text_credits'] ) $href="href='#credits'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_CREDITS."'>\n";
                    print LANG_SEARCH_CREDITS."<span class='span-basic-search-left-count'>(".$options['count_text_credits'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
            if( !$options['count_text_disclaimer'] ) $class="li-basic-search-color-0-results";
            else $class="";
            print "\t<li class='".$class."'>\n";
                if( $options['count_text_disclaimer'] ) $href="href='#disclaimer'";
                else $href="";
                print "\t<a ".$href." title='".LANG_SEARCH_DISCLAIMER."'>\n";
                    print LANG_SEARCH_DISCLAIMER."<span class='span-basic-search-left-count'>(".$options['count_text_disclaimer'].")</span>\t\n";
                print "\t</a>\n";
            print "\t</li>\n";
        print "\t</ul>\n";
        */

        if( $options['count_total'] )
        {
            $html_print.="<script type='text/javascript'>\n";

                $html_print.="$('#ul-biography').remove();";

                $html_print.="$('<ul/>', {id: 'ul-biography'}).appendTo('#sub-nav');";

                $html_li="";
                if( !$options['count_paintings'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='first ".$class."'>";
                    if( $options['count_paintings'] ) $href="href='#art'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_ART."'>";
                        $html_li.=LANG_SEARCH_ART."<span class='span-basic-search-left-count'>(".$options['count_paintings'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_microsites'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_microsites'] ) $href="href='#microsites'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_LEFT_SIDE_MICROSITES."'>";
                        $html_li.=LANG_LEFT_SIDE_MICROSITES."<span class='span-basic-search-left-count'>(".$options['count_microsites'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_biography'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_biography'] ) $href="href='#biography'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_BIOGRAPHY."'>";
                        $html_li.=LANG_SEARCH_BIOGRAPHY."<span class='span-basic-search-left-count'>(".$options['count_biography'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_text_chronology'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_text_chronology'] ) $href="href='#chronology'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_CHRONOLOGY."'>";
                        $html_li.=LANG_SEARCH_CHRONOLOGY."<span class='span-basic-search-left-count'>(".$options['count_text_chronology'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_quotes'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_quotes'] ) $href="href='#quotes'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_QUOTES."'>";
                        $html_li.=LANG_SEARCH_QUOTES."<span class='span-basic-search-left-count'>(".$options['count_quotes'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_exhibitions'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_exhibitions'] ) $href="href='#exhibitions'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_EXHIBITIONS."'>";
                        $html_li.=LANG_SEARCH_EXHIBITIONS."<span class='span-basic-search-left-count'>(".$options['count_exhibitions'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_literature'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_literature'] ) $href="href='#literature'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_LITERATURE."'>";
                        $html_li.=LANG_SEARCH_LITERATURE."<span class='span-basic-search-left-count'>(".$options['count_literature'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_videos'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_videos'] ) $href="href='#videos'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_VIDEOS."'>";
                        $html_li.=LANG_SEARCH_VIDEOS."<span class='span-basic-search-left-count'>(".$options['count_videos'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_links'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_links'] ) $href="href='#links'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_LINKS."'>";
                        $html_li.=LANG_SEARCH_LINKS."<span class='span-basic-search-left-count'>(".$options['count_links'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( $options['count_news_talks'] )
                {
                    if( !$options['count_news_talks'] ) $class="li-basic-search-color-0-results";
                    else $class="";
                    $html_li.="<li class='".$class."'>";
                        if( $options['count_news_talks'] ) $href="href='#talks'";
                        else $href="";
                        $html_li.="<a ".$href." title='".LANG_SEARCH_NEWS_TALKS."'>";
                            $html_li.=LANG_SEARCH_NEWS_TALKS."<span class='span-basic-search-left-count'>(".$options['count_news_talks'].")</span>";
                        $html_li.="</a>";
                    $html_li.="</li>";
                    $html_print.="$('#ul-biography').append(\"".$html_li."\");";
                }

                $html_li="";
                if( !$options['count_news_auctions'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_news_auctions'] ) $href="href='#auctions'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_NEWS_AUCTIONS."'>";
                        $html_li.=LANG_SEARCH_NEWS_AUCTIONS."<span class='span-basic-search-left-count'>(".$options['count_news_auctions'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_text_credits'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_text_credits'] ) $href="href='#credits'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_CREDITS."'>";
                        $html_li.=LANG_SEARCH_CREDITS."<span class='span-basic-search-left-count'>(".$options['count_text_credits'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

                $html_li="";
                if( !$options['count_text_disclaimer'] ) $class="li-basic-search-color-0-results";
                else $class="";
                $html_li.="<li class='".$class."'>";
                    if( $options['count_text_disclaimer'] ) $href="href='#disclaimer'";
                    else $href="";
                    $html_li.="<a ".$href." title='".LANG_SEARCH_DISCLAIMER."'>";
                        $html_li.=LANG_SEARCH_DISCLAIMER."<span class='span-basic-search-left-count'>(".$options['count_text_disclaimer'].")</span>";
                    $html_li.="</a>";
                $html_li.="</li>";
                $html_print.="$('#ul-biography').append(\"".$html_li."\");";

            $html_print.="</script>\n";

            if( $options['html_return'] ) return $html_print;
            else print $html_print;
        }

    }

    function div_videos($db,$options)
    {
        $html_print="";

        ### if theer is no search result found
        if( is_array($options['results']) ) $count_total=count($options['results']);
        else $count_total=$db->numrows($options['results']);
        if( empty($count_total) ) $html_print.=LANG_SEARCH_NORESULTS.".";
        #end

        if( !is_array($options['results']) )
        {
            while( $row=$db->mysql_array($options['results']) )
            {
                $thumb_array[]=$row;
            }
        }
        else $thumb_array=$options['results'];

        $pages=@ceil($count_total/$_GET['sp']);
        $lines=@ceil($count_total/$options['thumbs_per_line']);

        if( $count_total )
        {
            $html_print.="<div class='div-videos ".$options['class']['div-videos-page']."'>\n";
                $iii=0;
                $line=1;
                for( $ii=$options['values_from'];$ii<$options['values_to'];$ii++ )
                {
                    $iii++;

                    # gethering info abour video
                    $row=$thumb_array[$ii];
                    $row=UTILS::html_decode($row);
                    $options_row_text=array();
                    $options_row_text['search']=$options['search'];
                    $title_video=UTILS::row_text_value2($db,$row,"title",$options_row_text);
                    if( !empty($options['search']) )
                    {
                        $options_search_found=array();
                        $options_search_found['search']=$options['search'];
                        $options_search_found['text']=$title_video;
                        $options_search_found['a_name']=1;
                        $title_video=UTILS::show_search_found_keywords($db,$options_search_found);
                    }
                    $options_location=array();
                    $options_location['row']=$row;
                    $location=location($db,$options_location);
                    if( !empty($options['search']) )
                    {
                        $options_search_found=array();
                        $options_search_found['search']=$options['search'];
                        $options_search_found['text']=$location;
                        $options_search_found['a_name']=1;
                        $location=UTILS::show_search_found_keywords($db,$options_search_found);
                    }
                    $description=UTILS::row_text_value2($db,$row,"description");
                    $time=UTILS::row_text_value2($db,$row,"time");
                    $options_url=array();
                    $options_url['videoid']=$row['videoID'];
                    $video_url=UTILS::get_video_url($db,$options_url);
                    # END gethering info abour video

                    # last item in line
                    if( $options['thumbs_per_line']==$iii || $count_total==($ii+1) ) $class_last="last";
                    else $class_last="";

                    # finding first line of videos
                    if( $line==1 ) $class_first_line="first-line";
                    else $class_first_line="";

                    $html_print.="<div class='div-video ".$class_last." ".$class_first_line."'>\n";
                        $html_print.="<a href='".$video_url['url']."' title='".strip_tags($title_video)."'>\n";
                            $html_print.="<img src='/g/videos/play_button.png' alt='' class='img-video-thumb-play' />";
                            $html_print.="<span class='span-img-video-thumb'>";
                                $src_image_medium=DATA_PATH_DATADIR."/images/video/medium/".$row['videoID'].".jpg";
                                $html_print.="<img alt='' class='img-video-thumb' src='".$src_image_medium."' />\n";
                            $html_print.="</span>";
                            $html_print.="<span class='span-video-thumb-desc'>";
                                $html_print.="<span class='span-video-thumb-category'>";
                                    # video category info
                                    $query_where_video_cat=" WHERE categoryid='".$row['categoryid']."' ";
                                    $query_video_cat=QUERIES::query_videos_categories($db,$query_where_video_cat);
                                    $results_video_cat=$db->query($query_video_cat['query']);
                                    $row_video_cat=$db->mysql_array($results_video_cat);
                                    $row_video_cat=UTILS::html_decode($row_video_cat);
                                    $title_cat=UTILS::row_text_value($db,$row_video_cat,"title");
                                    $html_print.=$title_cat;
                                $html_print.="</span>";
                                $html_print.="<span class='span-video-thumb-title-short'>";
                                    $html_print.=$title_video;
                                $html_print.="</span>";
                                if( !empty($row['name']) )
                                {
                                    $html_print.="<span class='span-video-thumb-name'>";
                                        if( !empty($options['search']) )
                                        {
                                            $options_search_found=array();
                                            $options_search_found['search']=$options['search'];
                                            $options_search_found['text']=$row['name'];
                                            $options_search_found['strip_tags']=1;
                                            $options_search_found['a_name']=1;
                                            $name=UTILS::show_search_found_keywords($db,$options_search_found);
                                        }
                                        else $name=$row['name'];
                                        $html_print.=$name;
                                    $html_print.="</span>";
                                }
                                if( !empty($location) || !empty($row['year']) )
                                {
                                    $html_print.="<span class='span-video-thumb-location'>";
                                        if( !empty($location) )
                                        {
                                            $html_print.=$location.",";
                                        }
                                        if( !empty($row['year']) )
                                        {
                                            $options_search_found=array();
                                            $options_search_found['search']=$options['search'];
                                            $options_search_found['text']=$row['year'];
                                            $options_search_found['a_name']=1;
                                            $year=UTILS::show_search_found_keywords($db,$options_search_found);
                                            $html_print.=" ".$year;
                                        }
                                    $html_print.="</span>";
                                }
                                $html_print.="&nbsp;";
                                $options_admin_edit=array();
                                $options_admin_edit=$options;
                                $html_print.=UTILS::admin_edit("/admin/videos/edit/?videoid=".$row['videoID'],$options_admin_edit);
                            $html_print.="</span>\n";
                        $html_print.="</a>\n";
                    $html_print.="</div>\n";#div_video end
                    if( $iii==$options['thumbs_per_line'] )
                    {
                        $html_print.="<div class='clearer'></div>\n";
                        $iii=0;
                        $line++;
                    }
                }
                $html_print.="<div class='clearer'></div>\n";
            $html_print.="</div>\n";#div_videos end
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function div_articles($db,$options=array())
    {
        $html_print="";

        if( is_array($options['results']) ) $count_total=count($options['results']);
        else $count_total=$db->numrows($options['results']);

        if( !is_array($options['results']) )
        {
            while( $row=$db->mysql_array($options['results']) )
            {
                $thumb_array[]=$row;
            }
        }
        else $thumb_array=array_values($options['results']);

        for( $i=0;$i<$count_total;$i++ )
        {
            $row=UTILS::html_decode($thumb_array[$i]);
            if( $i==$count_total ) $class="div-articles-list-last";
            elseif( $i==0 ) $class="div-articles-list-first";
            else $class="";
            $html_print.="<div class='div-articles-list ".$class."'>\n";
                $options_row_text=array();
                $options_row_text['search']=$options['search'];
                $notes=UTILS::row_text_value2($db,$row,"notes",$options_row_text);
                $link_display=str_replace("http://","",$row['link']);

                $options_search_found=array();
                $options_search_found['search']=$_GET['search'];
                $options_search_found['text']=$row['title'];
                $options_search_found['class']['span-keyword-found2']="span-keyword-found2";
                $options_search_found['a_name']=1;
                $title=UTILS::show_search_found_keywords($db,$options_search_found);

                if( !empty($row['link']) )
                {
                    $class="a-links-link";
                    $href="href='".$row['link']."'";
                }
                else
                {
                    $class="";
                    $href="";
                }
                $html_print.="<a ".$href." title='".$row['link']."' name='".$row['id']."' class='".$class."' target='_blank'>";
                    $html_print.="<span class='span-links-title'>".$title."</span>";
                    if( !empty($row['author']) )
                    {
                        $html_print.="<span class='span-links-author'>";
                            $options_search_found=array();
                            $options_search_found['search']=$_GET['search'];
                            $options_search_found['text']=$row['author'];
                            $options_search_found['class']['span-keyword-found2']="span-keyword-found2";
                            $options_search_found['a_name']=1;
                            $author=UTILS::show_search_found_keywords($db,$options_search_found);
                            $html_print.=$author;
                        $html_print.="</span>";
                    }
                    if( !empty($row['title_periodical']) )
                    {
                        $html_print.="<span class='span-links-in'>";
                            //if( !empty($row['author']) && !empty($row['title_periodical']) ) $html_print.="<br />";
                            $options_search_found=array();
                            $options_search_found['search']=$_GET['search'];
                            $options_search_found['text']=$row['title_periodical'];
                            $options_search_found['class']['span-keyword-found2']="span-keyword-found2";
                            $options_search_found['a_name']=1;
                            $title_periodical=UTILS::show_search_found_keywords($db,$options_search_found);
                            $html_print.=LANG_LINKS_ARTICLES_PUBLISHED_IN.": ".$title_periodical;
                        $html_print.="</span>";
                    }
                    $html_print.="<span class='span-links-date'>";

                        # Journals
                        if( $row['books_catid']==6 )
                        {
                            if( !empty($row['vol_no']) || !empty($row['link_date']) || !empty($row['issue_no']) || !empty($row['article_pages_from']) || !empty($row['article_pages_till']) )
                            {
                                # vol NO
                                if( !empty($row['vol_no']) ) $html_print.=LANG_LITERATURE_VOL." ".$row['vol_no'];

                                # link date
                                if( !empty($row['vol_no']) ) $comma=", ";
                                else $comma="";
                                if( !empty($row['link_date']) )
                                {
                                    $html_print.=$comma;
                                    /*
                                    if( $row['link_date_month'] )
                                    {
                                        if( $_GET['lang']=="de" ) $html_print.=UTILS::date_month($db,$row['date_display_month_de']);
                                        else $html_print.=UTILS::date_month($db,$row['date_display_month_en']);
                                    }*/
                                    //elseif( $row['link_date_year'] )
                                    if( !empty($row['date_display_year']) && $row['link_date_admin']==$row['date_display_year']."-00-00" )
                                    {
                                        if( !empty($row['link_date_year_full']) )
                                        {
                                            $html_print.=UTILS::date_month($db,$row['link_date_year_full'])." ";
                                        }
                                        $html_print.=$row['date_display_year'];
                                    }
                                    else
                                    {
                                        if( $_GET['lang']=="de" ) $html_print.=UTILS::date_month($db,$row['date_display_de']);
                                        else $html_print.=UTILS::date_month($db,$row['date_display_en']);
                                    }
                                    //$html_print.="|".$row['date_display_year']."|".$row['link_date_admin ']."|";
                                }
                                //$html_print.="-".$row['date_display_en']."-".$row['link_date_year']."-".$row['link_date_year_full']."-".$row['link_date'];

                                # issue NO
                                if( !empty($row['vol_no']) || !empty($row['link_date'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($row['issue_no']) ) $html_print.=$comma.LANG_LITERATURE_NO." ".$row['issue_no'];

                                # pages
                                if( !empty($row['vol_no']) || !empty($row['link_date']) || !empty($row['issue_no'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($row['article_pages_from']) || !empty($row['article_pages_till']) )
                                {
                                    $html_print.=$comma;
                                    if( !empty($row['article_pages_from']) && !empty($row['article_pages_till']) )
                                    {
                                        $html_print.=LANG_LITERATURE_PAGESS." ";
                                        $html_print.=$row['article_pages_from']."&#8211;".$row['article_pages_till'];
                                    }
                                    else
                                    {
                                        $html_print.=LANG_LITERATURE_PAGES." ";
                                        $html_print.=$row['article_pages_from'];
                                    }
                                }
                            }
                        }
                        # END Journals

                        # Newspaper Articles && Online Publications
                        if( $row['books_catid']==12 || $row['books_catid']==13 || $row['books_catid']==19 )
                        {
                            if( !empty($row['link_date']) || !empty($row['issue_no']) || !empty($row['article_pages_from']) || !empty($row['article_pages_till']) )
                            {
                                if( !empty($row['link_date']) )
                                {
                                    if( !empty($row['date_display_year']) && $row['link_date_admin']==$row['date_display_year']."-00-00" )
                                    {
                                        if( !empty($row['link_date_year_full']) )
                                        {
                                            $html_print.=UTILS::date_month($db,$row['link_date_year_full'])." ";
                                        }
                                        $html_print.=$row['date_display_year'];
                                    }
                                    else
                                    {
                                        if( $_GET['lang']=="de" ) $html_print.=UTILS::date_month($db,$row['date_display_de']);
                                        else $html_print.=UTILS::date_month($db,$row['date_display_en']);
                                    }
                                }
                                if( !empty($row['link_date'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($row['issue_no']) ) $html_print.=$comma.LANG_LITERATURE_NO." ".$row['issue_no'];
                                if( !empty($row['link_date']) || !empty($row['issue_no'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($row['article_pages_from']) || !empty($row['article_pages_till']) )
                                {
                                    $html_print.=$comma.LANG_LITERATURE_PAGES." ";
                                    if( !empty($row['article_pages_from']) && !empty($row['article_pages_till']) )
                                    {
                                        $html_print.=$row['article_pages_from']."&#8211;".$row['article_pages_till'];
                                    }
                                    else $html_print.=$row['article_pages_from'];
                                }
                            }
                        }
                        # END Newspaper Articles && Online Publications

                    $html_print.="</span>";
                if( !empty($description) ) $html_print.="<span class='span-links-description'>".$notes."</span>";
                $html_print.="</a>";
                $options_admin_edit=array();
                $options_admin_edit=$options;
                $html_print.=UTILS::admin_edit("/admin/books/edit/?bookid=".$row['id'],$options_admin_edit);
            $html_print.="</div>\n";
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }


    function section_categories_thumbs($db,$options=array())
    {
        $html_print="";

        //print_r($options);

        $html_print.="<div class='div-section-categories ".$options['class']['div-section-categories']."'>";
            $i=0;
            $ii=0;
            if( is_array($options['categories']) )
            {
                foreach( $options['categories'] as $row )
                {
                    $i++;
                    $ii++;
                    if( ( $ii==$options['count_categories'] && !$options['show_all'] ) || $i==4 || ( $ii==$options['count_categories']-1 && $options['categories'][count($options['categories'])-1]['class']['div-section-category']=="disable" ) )
                    {
                        $class_last="div-section-categories-last";
                        $i=0;
                    }
                    else $class_last="";
                    if( $ii<=4  )
                    {
                        $class_first_row="div-section-categories-first-row";
                    }
                    else $class_first_row="";
                    # quotes categories
                    //print_r($row);
                    if( !empty($row['quotes_categoryid']) )
                    {
                        $title=UTILS::row_text_value($db,$row,'title');
                        $categoryid=$row['quotes_categoryid'];
                        $options_quote_cat_url=array();
                        $options_quote_cat_url['quotes_categoryid']=$categoryid;
                        $url_cat=UTILS::get_quote_cat_url($db,$options_quote_cat_url);
                        $href=$url_cat['url'];
                        $typeid=19;
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='".$typeid."' AND itemid2='".$categoryid."' ) OR ( typeid2=17 AND typeid1='".$typeid."' AND itemid1='".$categoryid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        //$src="/images/size_m__imageid_".$imageid.".jpg";
                        $src=DATA_PATH_DATADIR."/images_new/original/".$imageid.".jpg";
                    }
                    # Literature categories
                    if( !empty($row['books_catid']) )
                    {
                        $title=UTILS::row_text_value($db,$row,'title');
                        $categoryid=$row['books_catid'];
                        $options_book_cat_url=array();
                        $options_book_cat_url['books_catid']=$categoryid;
                        $url_cat=UTILS::get_literature_cat_url($db,$options_book_cat_url);
                        $href=$url_cat['url'];
                        $typeid=26;
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='".$typeid."' AND itemid2='".$categoryid."' ) OR ( typeid2=17 AND typeid1='".$typeid."' AND itemid1='".$categoryid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        //$src="/images/size_m__imageid_".$imageid.".jpg";
                        $src=DATA_PATH_DATADIR."/images_new/original/".$imageid.".jpg";
                        //print $categoryid."| ";
                    }
                    # Links categories
                    if( !empty($row['links_categoryid']) )
                    {
                        $categoryid=$row['links_categoryid'];
                        $title=$row['title'];
                        $src=$row['src'];
                        $href=$row['href'];
                    }
                    # Art categories
                    if( !empty($row['art_categoryid']) )
                    {
                        $categoryid=$row['art_categoryid'];
                        $title=$row['title'];
                        $src=$row['src'];
                        $href=$row['href'];
                        if( !empty($row['target']) ) $target="target='_blank'";
                    }
                    # Art/... categories
                    if( !empty($row['catID']) )
                    {
                        $categoryid=$row['catID'];
                        $title=UTILS::row_text_value2($db,$row,"category");
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=18 AND itemid2='".$categoryid."' ) OR ( typeid2=17 AND typeid1=18 AND itemid1='".$categoryid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                        $src=DATA_PATH_DATADIR."/images_new/medium/".$imageid.".jpg";
                        # url
                        $href="/".$_GET['lang']."/art";
                        if( $row['artworkID']==1 || $row['artworkID']==2 )
                        {
                            $href.="/paintings";
                        }
                        $href.="/".$row['a_titleurl'];
                        if( !empty($row['titleurl']) ) $href.="/".$row['titleurl'];
                    }
                    # videos categories and all future category types
                    if( !empty($row['categoryid']) )
                    {
                        $title=UTILS::row_text_value($db,$row,'title');
                        $categoryid=$row['categoryid'];
                        $options_quote_cat_url=array();
                        $options_quote_cat_url['categoryid']=$categoryid;
                        $url_cat=UTILS::get_video_cat_url($db,$options_quote_cat_url);
                        $href=$url_cat['url'];
                        $src=DATA_PATH_DATADIR."/images/video/categories/original/".$row['src'];
                    }
                    $html_print.="<div class='div-section-category ".$class_last." ".$class_first_row." ".$row['class']['div-section-category']."'>";
                        $html_print.="<div class='div-section-category-mobile'>";
                            $html_print.="<a href='".$href."' title=\"".$title."\" class='a-lit-cat' ".$target.">";
                                $html_print.="<img src='".$src."' alt='' />";
                                $html_print.="<span class='span-lit-cat-title'>";
                                    $html_print.=$title;
                                $html_print.="</span>";
                            $html_print.="</a>";
                            if( !empty($options['url_admin']) )
                            {
                                $options_admin_edit=array();
                                $options_admin_edit=$options;
                                $html_print.=UTILS::admin_edit($options['url_admin'].$categoryid,$options_admin_edit);
                            }
                        $html_print.="</div>";
                    $html_print.="</div>";
                }
                # show all
                if( $options['show_all'] )
                {
                    $html_print.="<div class='div-section-category div-section-categories-last div-section-categories-show-all '>";
                        $html_print.="<a href='".$options['url_show_all']."' title=\"".LANG_LITERATURE_SHOW_ALL."\" class='a-lit-cat' >";
                            $html_print.="<span class='span-lit-cat-title'>";
                                $html_print.=LANG_LITERATURE_SHOW_ALL;
                            $html_print.="</span>";
                        $html_print.="</a>";
                    $html_print.="</div>";
                }
                # end show all
            }
            $html_print.="<div class='clearer'></div>";
        $html_print.="</div>";

        $return['count']=count($options['categories']);
        $return['html_print']=$html_print;

        if( $options['html_return'] ) return $return;
        else print $html_print;
    }


    function quotes($db,$options)
    {
        $html_print="";

        $results=$db->query($options['query']);
        $count=$db->numrows($results);
        if( $count>0 )
        {
            $html_print.="<div id='div-quotes'>\n";
                $i=0;
                while( $row=$db->mysql_array($results,0) )
                {
                    $i++;

                    # search keyword higlight
                    $options_row_text=array();
                    $options_row_text['search']=$options['search'];
                    //$options_row_text['strip_tags']=0;//1 = false
                    $text=UTILS::row_text_value($db,$row,"text",$options_row_text);
                    $text=str_replace($title,"",$text);
                    $options_search_found=array();
                    $options_search_found['search']=$options['search'];
                    $options_search_found['text']=$text;
                    $options_search_found['convert_utf8']=1;
                    $options_search_found['a_name']=1;
                    $options_search_found['strip_tags']=1;
                    $text=UTILS::show_search_found_keywords($db,$options_search_found);

                    # search keyword higlight
                    $options_row_text=array();
                    $options_row_text['search']=$options['search'];
                    $source=UTILS::row_text_value($db,$row,"source",$options_row_text);
                    $options_search_found=array();
                    $options_search_found['search']=$options['search'];
                    $options_search_found['text']=$source;
                    //$options_search_found['convert_utf8']=1;
                    $options_search_found['a_name']=1;
                    $options_search_found['strip_tags']=1;
                    $source=UTILS::show_search_found_keywords($db,$options_search_found);

                    # search keyword higlight
                    $options_row_text=array();
                    $options_row_text['search']=$options['search'];
                    $source_text=UTILS::row_text_value($db,$row,"source_text",$options_row_text);
                    $options_search_found=array();
                    $options_search_found['search']=$options['search'];
                    $options_search_found['text']=$source_text;
                    $options_search_found['convert_utf8']=1;
                    $options_search_found['a_name']=1;
                    $options_search_found['strip_tags']=1;
                    $source_text=UTILS::show_search_found_keywords($db,$options_search_found);

                    $row=UTILS::html_decode($row);

                    $html_print.="<div class='div-quote'>\n";
                        if( $options['search']==1 && !empty($row['year']) && $year!=$row['year']  ) $html_print.="<h2 class='h2_quote_year'>".$row['year']."</h2>\n";
                        $html_print.="<a name='".$row['quoteid']."'></a>\n";
                        if( !empty($options['global_search']) ) $class_search="span-basic-search-text";
                        else $class_search="";
                        $html_print.="<blockquote class='blockquote_quote div_text_wysiwyg ".$class_search."'>";
                                $html_print.=$text;
                        $html_print.="</blockquote>\n";
                        if( !empty($source_text) || !empty($source) )
                        {
                            if( $i==$count && $options['global_search'] ) $class="div-quotes-cite-last";
                            else $class="div-quotes-cite";
                            $html_print.="<div class='".$class."'>\n";
                                if( !empty($source) ) $html_print.="<cite class='cite_quote'>".$source."</cite>\n";
                                if( !empty($source_text) )
                                {
                                    $html_print.="<span class='help-quote span_source_text' data-source='".$row['quoteid']."' >\n";
                                        $html_print.=LANG_QUOTES_SOURCE;
                                    $html_print.="</span>\n";
                                    $html_print.="<div class='div-tooltips-info-content div-tooltips-info-content-quotes-".$row['quoteid']."'>".$source_text."</div>";
                                }
                                $html_print.="<br class='clearer' />";
                            $html_print.="</div>\n";
                        }
                        $options_admin_edit=array();
                        $options_admin_edit=$options;
                        $html_print.=UTILS::admin_edit("/admin/quotes/edit/?quoteid=".$row['quoteid'],$options_admin_edit);
                        $year=$row['year'];
                    $html_print.="</div>\n";
                }
            $html_print.="</div>\n";
            if( $count>2 && $options['back_to_top'] ) $html_print.="<p class='margin'><a class='top' href='#top'>".LANG_BACKTOTOP."</a></p>\n";
        }
        elseif( !$options['count_sub_categories'] && $options['search_info']==0 ) $html_print.=LANG_QUOTES_NO_QUOTES_FOUND;
        elseif( !$options['count_sub_categories'] && $options['search_info']==1 )
        {
            $html_print.="<div class='div-results'>";
                $html_print.="<div class='result-error'>";
                    $html_print.=LANG_SEARCH_NORESULTS;
                $html_print.="</div>";
            $html_print.="</div>";
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }


    function div_news($db,$values=array())
    {
        while( $row=$db->mysql_array($values['results']) )
        {
            $row=UTILS::html_decode($row);
            print "\t<!--[if IE]><br /><![endif]-->\n";
            print "\t<a name='".$row['newsID']."' ></a>\n";
            print "\t<div class='new ".$values['class']['new-search']."' >\n";
                print "\t<h4>";
                    # title
                    $title=UTILS::row_text_value2($db,$row,"title");
                    $values_search_found=array();
                    $values_search_found['search']=$values['search'];
                    $values_search_found['text']=$title;
                    $values_search_found['a_name']=1;
                    $title=UTILS::show_search_found_keywords($db,$values_search_found);
                    # location
                    $values_location=array();
                    $values_location['row']=$row;
                    $location=location($db,$values_location);
                    $values_search_found=array();
                    $values_search_found['search']=$values['search'];
                    $values_search_found['text']=$location;
                    $values_search_found['class']['span-keyword-found2']="span-keyword-found2";
                    $values_search_found['a_name']=1;
                    $location=UTILS::show_search_found_keywords($db,$values_search_found);
                    # text
                    $text=UTILS::row_text_value2($db,$row,"text");
                    $values_search_found=array();
                    $values_search_found['search']=$values['search'];
                    $values_search_found['text']=$text;
                    $values_search_found['class']['span-keyword-found2']="span-keyword-found2";
                    $values_search_found['strip_tags']=$values['strip_tags'];
                    $values_search_found['a_name']=1;
                    $text=UTILS::show_search_found_keywords($db,$values_search_found);
                    print $title;
                    if( $_SESSION['userID'] ) print "&nbsp;<span class='edit'><a title='Edit in admin' href='/admin/news/edit/?newsid=".$row['newsID']."'>edit</a></span>";
                print "\t</h4>\n";
                print "<table class='news-info'>\n";
                    print "<tr>\n";
                        print "\t<td class='news-location' colspan='2' >";
                            if( !empty($location) ) print $location;
                        print "\t</td>\n";
                    print "</tr>\n";
                    print "<tr>\n";
                        print "<td class='news-date' >";
                            $ndatefrom=UTILS::row_text_value($db,$row,"ndatefrom");
                            $ndateto=UTILS::row_text_value($db,$row,"ndateto");
                            $ndatefrom_display=UTILS::date_month($db,$ndatefrom);
                            $ndateto_display=UTILS::date_month($db,$ndateto);
                            if( $row['ndatefrom']==$row['ndateto'] )
                            {
                                $date=$ndatefrom_display;
                            }
                            else
                            {
                                $date=$ndatefrom_display;
                                $date.=" &#8211; ";
                                $date.=$ndateto_display;
                            }
                            if( (!empty($row['ndatefrom']) || !empty($row['ndateto'])) && $row['showdate'] )
                            {
                                print $date;
                            }
                        print "</td>\n";
                        print "\t<td>";
                            $query_video_relations="SELECT typeid1,itemid1,typeid2,itemid2 from ".TABLE_RELATIONS." WHERE ( typeid1=7 AND itemid1='".$row['newsID']."' AND typeid2=10 ) OR ( typeid2=7 AND itemid2='".$row['newsID']."' AND typeid1=10 ) ORDER BY sort ASC, relationid DESC LIMIT 1 ";
                            $results_video_relations=$db->query($query_video_relations);
                            $count_video_relations=$db->numrows($results_video_relations);
                            $row_video_relations=$db->mysql_array($results_video_relations);
                            $videoid=UTILS::get_relation_id($db,"10",$row_video_relations);
                            if( $count_video_relations )
                            {
                                $query_where_video=" WHERE videoID='".$videoid."' AND enable=1 ";
                                $query_video=QUERIES::query_videos($db,$query_where_video,$query_order_video);
                                $results_video=$db->query($query_video['query']);
                                $count_video=$db->numrows($results_video);
                                if( $count_video )
                                {
                                    # video info
                                    $row_video=$db->mysql_array($results_video);
                                    $title=UTILS::row_text_value2($db,$row_video,"title");
                                    $titleurl=UTILS::row_text_value($db,$row_video,"titleurl");
                                    # video category info
                                    $query_where_video_cat=" WHERE categoryid='".$row_video['categoryid']."' ";
                                    $query_video_cat=QUERIES::query_videos_categories($db,$query_where_video_cat);
                                    $results_video_cat=$db->query($query_video_cat['query']);
                                    $row_video_cat=$db->mysql_array($results_video_cat);
                                    $titleurl_cat=UTILS::row_text_value($db,$row_video_cat,"titleurl");
                                    $url="/videos/".$titleurl_cat."/".$titleurl;
                                    print "\t<a title='".$title."' href='".$url."' style='float:right;margin-top:-15px;'>\n";
                                        print "\t<img src='/g/exhibitions/video.gif' alt='' />\n";
                                    print "\t</a>\n";
                                }
                            }
                        print "\t</td>\n";
                    print "</tr>\n";
                    if( !empty($text) ) print "<tr><td class='news-text' colspan='2'>".$text."</td></tr>\n";
                print "</table>\n";
            print "</div>\n";
        }

    }


        #Page numbering
        function page_numbering($db,$options=array())
        {
            $html_print="";

            if( $options['pages']>1 || $options['show_all'] || $options['count']>8 || !empty($options['info-text']) ) $html_print.="<div class='div-page-numbering-container ".$options['class']['div-page-numbering-container-border']."'>";

                if( !empty($options['info-text']) )
                {
                    $html_print.="<div class='div-page-numbering-info-text ".$options['class']['div-page-numbering-info-text']."'>\n";
                        $html_print.="<p>".$options['info-text']."</p>";
                    $html_print.="</div>\n";
                }

                if( empty($options['img_src_left']) || empty($options['img_src_right']) )
                {
                    $options['img_src_left']="/g/exhibitions/exh-detail-page-numb-arrow-left.png";
                    $options['img_src_right']="/g/exhibitions/exh-detail-page-numb-arrow-right.png";
                }

                if( $options['pages']>1 || $options['show_all'] || $options['count']>8  )
                {
                    $html_print.="<div class='pagelist_navigation ".$options['class']['div-page-navigation']."'>\n";
                }

                    $pattern = '/\?/i';
                    preg_match($pattern, $options['url'], $matches, PREG_OFFSET_CAPTURE);
                    //if( stripos($options['url'],"?") ) $and_sign="&";
                    if( count($matches)>0 ) $and_sign="&";
                    else $and_sign="?";

                    if( $options['pages']>1 )
                    {
                        $html_print.="<ul>";

                            # first item
                            $url_first=$options['url'].$and_sign."p=1";
                            $url_first.=$options['url_after'];
                            $html_print.="<li class='first'><a href='".$url_first."'>1</a></li>";
                            # END first item

                            # ARRWOS PREVIOUS
                            if( $options['p']==1 ) $tmp=$options['pages'];
                            else $tmp=$options['p']-1;

                            $url_prev=$options['url'].$and_sign."p=".$tmp;
                            $url_prev.=$options['url_after'];

                            $html_print.="<li class='prev'>";
                                $html_print.="<a href='".$url_prev."' class='a-page-left'>";
                                    //$html_print.="<img src='".$options['img_src_left']."' alt='' />";
                                $html_print.="</a>";
                            $html_print.="</li>";
                            # end

                            #NUMBERS
                            //if( $options['pages']>10 && $options['p']>=7 ) $html_print.="<li><a class='a-page-numbers-dots a-page-numbers-dots-left'>...</a></li>\n";

                            for($i=1;$i<=$options['pages'];$i++)
                            {
                                if( $i==$options['p'] )
                                {
                                    $class=" selected ";
                                }
                                else $class="";
                                if( $i==$options['pages'] )
                                {
                                    //$class.=" last ";
                                    $class.="";
                                }
                                else $class.="";

                                $url_number=$options['url'].$and_sign."p=".$i;
                                $url_number.=$options['url_after'];
                                $link="<li class='".$class."'><a href='".$url_number."'>$i</a></li>";

                                if( $options['pages']>5 )
                                {
                                    $a=$options['p']-2;
                                    $b=$options['p']+3;
                                    if( $a<=0 )
                                    {
                                        $b=$b+abs($a);
                                        $a=1;
                                    }
                                    else if( ($options['p']-$a)==2 )
                                    {
                                        $b=$options['p']+2;
                                    }
                                    else
                                    {
                                        $b=$options['p']+(5-$options['p']);
                                    }

                                    if( $b>=$options['pages'] )
                                    {
                                        $a=$a+($options['pages']-$b);
                                    }

                                    if( $i>=$a && $i<=$b  )
                                    {
                                        $html_print.=$link;
                                        $tmp=$i;
                                    }

                                }
                                else $html_print.=$link;
                            }

                            if( $options['pages']>5 && ($options['p']+1)<=$options['pages'] && $tmp!=$options['pages'] )
                            {
                                //$html_print.="<li><a class='a-page-numbers-dots a-page-numbers-dots-right'>...</a></li>\n";
                            }
                            #END

                            # ARROWS NEXT
                            if( $options['p']==$options['pages'] ) $tmp=1;
                            else $tmp=$options['p']+1;

                            $url_next=$options['url'].$and_sign."p=".$tmp;
                            $url_next.=$options['url_after'];

                            $html_print.="<li class='next'>";
                                $html_print.="<a href='".$url_next."' class='a-page-right'>";
                                    //$html_print.="<img src='".$options['img_src_right']."' alt='' />";
                                $html_print.="</a>";
                            $html_print.="</li>";
                            # END

                            # last item
                            $url_last=$options['url'].$and_sign."p=".$options['pages'];
                            $url_last.=$options['url_after'];
                            $html_print.="<li class='last'><a href='".$url_last."'>".$options['pages']."</a></li>";
                            # END last item


                        $html_print.="</ul>";
                    }

                if( $options['pages']>1 || $options['show_all'] || $options['count']>8 )
                {
                    $html_print.="</div>\n";
                }

                if( ( $options['pages']>1 || $options['show_all'] || $options['count']>8 ) && ( $options['per_page_list']==1 || $options['per_page'] )  )
                {
                    $options_page_per_page=array();
                    $options_page_per_page=$options;
                    $options_page_per_page['class']['select-show']="select-field select-show";
                    $html_print.=$this->page_per_page($db,$options_page_per_page);
                }

                if($options['pages']>1 || $options['show_all'] || $options['count']>8 || !empty($options['info-text']) ) $html_print.="<div class='clearer'></div>\n";

            if( $options['pages']>1 || $options['show_all'] || $options['count']>8 || !empty($options['info-text']) ) $html_print.="</div>\n"; # div-page-numbering-container

            if( $options['html_return'] ) return $html_print;
            else print $html_print;
        }

        # end Page numbering

    function page_per_page($db,$options=array())
    {
        $html_print="";

        if( $options['pages']>1 || $options['sp']=="all" || $options['count']>8  )
        {
            #Show per page
            $html_print.="<div class='page_header_caption ".$options['class']['div-per-page']."'>\n";
                $html_print.="<span class='span-text-per-page'>".LANG_IMAGES_PERPAGE."</span>";
                $onchange="onchange=\"window.location='?sp='+this.value+'".$options['url_after_per_page']."';\"";
                $html_print.="<select name='sp' ".$onchange." class='".$options['class']['select-show']."' >\n";
                    if( $options['thumbs_per_line']==5 )
                    {
                        if( $options['sp']==10 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='10'>10</option>\n";
                        if( $options['sp']==15 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='15'>15</option>\n";
                        if( $options['sp']==20 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='20'>20</option>\n";
                        if( $options['sp']==35 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='35'>35</option>\n";
                        if( $options['sp']==70 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='70'>70</option>\n";
                    }
                    else
                    {
                        if( $options['sp']==8 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='8'>8</option>\n";
                        if( $options['sp']==12 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='12'>12</option>\n";
                        if( $options['sp']==16 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='16'>16</option>\n";
                        if( $options['sp']==32 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='32'>32</option>\n";
                        if( $options['sp']==64 ) $select="selected='selected'";
                        else $select="";
                        $html_print.="<option ".$select." value='64'>64</option>\n";
                    }
                    if( $options['sp']=="all" || $options['sp']==1000000 ) $select="selected='selected'";
                    else $select="";
                    if( !$options['show_all'] ) $html_print.="<option ".$select." value='all'>".LANG_ALL."</option>\n";
                $html_print.="</select>\n";
            $html_print.="</div>\n";
            #end
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function previous_next_links($db,$options)
    {
        $html_print="";

        //print $options['title']."---";
        //print $options['results']['count']."-";
        //print_r($options);
        //print $url."------".$options['url1']."<br />";

        $results_total=$options['results']['results_total'];
        $count=$options['results']['count'];

        $options_order_res=array();
        $options_order_res['results']=$results_total;
        $options_order_res['title']=$options['title'];
        $thumb_array=UTILS::order_results($db,$options_order_res);

        $i=0;
        foreach( $thumb_array as $row_paintings )
        {
            $url=$options['url1'];
            $url3="/?".$options['search_vars']['with_sp'];
            if( $i==0 ) $first=$url."/".$row_paintings['titleurl'].$url3.$options['url_after'];
            else $last=$url."/".$row_paintings['titleurl'].$url3.$options['url_after'];
            $i++;
            //$tmp[$i]['paintid']="/".$row_paintings['titleurl'];
            $tmp[$i]['paintid']=$row_paintings['paintID'];
            if( $row_paintings['paintID']==$options['row']['paintID'] ) $selected=$i;
        }

        //if( $selected!=1 ) $previous=$url.$tmp[$selected-1]['paintid'].$url3.$options['url_after'];
        //if( $selected!=sizeof($tmp) ) $next=$url.$tmp[$selected+1]['paintid'].$url3.$options['url_after'];

        if( $selected!=1 )
        {
            $options_painting_url=array();
            $options_painting_url['paintid']=$tmp[$selected-1]['paintid'];
            $options_painting_url['painting_no_section']=$options['painting_no_section'];
            $painting_url=UTILS::get_painting_url($db,$options_painting_url);
            $previous=$painting_url['url'].$url3.$options['url_after'];
            //if( $options_painting_url['painting_no_section'] ) $previous=$url."/".$previous;
            if( $options_painting_url['painting_no_section'] ) $previous=$url.$previous;
        }
        if( $selected!=sizeof($tmp) )
        {
            $options_painting_url=array();
            $options_painting_url['paintid']=$tmp[$selected+1]['paintid'];
            $options_painting_url['painting_no_section']=$options['painting_no_section'];
            $painting_url=UTILS::get_painting_url($db,$options_painting_url);
            $next=$painting_url['url'].$url3.$options['url_after'];
            //if( $options_painting_url['painting_no_section'] ) $next=$url."/".$next;
            if( $options_painting_url['painting_no_section'] ) $next=$url.$next;
            //print $tmp[$selected+1]['paintid'];
            //print $painting_url['url'];
        }

        //print $next;

        $options_next_buttons=array();
        $options_next_buttons=$options;
        $options_next_buttons['previous']=$previous;
        $options_next_buttons['next']=$next;
        $options_next_buttons['last']=$last;
        $options_next_buttons['first']=$first;
        $options_next_buttons['count']=$count;
        $options_next_buttons['selected']=$selected;
        $html_print.=$this->previous_next_links_buttons_new($db,$options_next_buttons);

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function previous_next_links_buttons_new($db,$options=array())
    {
        //print_r($options);
        $html_print="";

        # previous
        if( empty($options['previous']) ) $href="";
        else $href="href='".$options['previous']."'";
        $a_previous="<a ".$href." title='' class='a-previous-next-left'>&#60;</a>";
        # END previous

        # next
        if( empty($options['next']) ) $href="";
        else $href="href='".$options['next']."'";
        $a_next="<a ".$href." title='' class='a-previous-next-right'>&#62;</a>";
        # END next

        ### draw previous and next links
        $html_print.="<div class='div-previous-next ".$options['class']['div-previous-next-exh-artwork']."'>\n";
            $html_print.="<div class='div-previous-next-left'>";
                if( !empty($options['url_back']) )
                {
                    $html_print.="<a href='".$options['url_back'].$options['url_after']."' class='a-previous-next-back'>".$options['back_text']."</a>";
                }
            $html_print.="</div>";
            if( !empty($options['selected']) && !empty($options['count']) )
            {
                $html_print.="<div class='div-previous-next-right'>";
                    $text=$a_next;
                    $text.="<div class='div-previous-next-box-around-numbers'>";
                        $text.=$options['selected']."/".$options['count']." ";
                    $text.="</div>";
                    $text.=$a_previous;
                    $html_print.=$text;
                $html_print.="</div>";
            }
            $html_print.="<div class='clearer'></div>\n";
        $html_print.="</div>\n";
        #end

        # javascript for item next previous keyboard arrows
        $html_print.="<script type='text/javascript'>\n";
            $html_print.="$(document).keypress(function(event) {\n";
                $html_print.="var tabs = $('#".$options['id']['tabs']."').tabs();\n";
                $html_print.="var selected = tabs.tabs('option', 'active');\n";
                //$html_print.="console.log('pressed-'+selected);";
                //$html_print.="console.debug(selected);";
                $html_print.="if ( event.keyCode == 37 ) {\n";
                    if( !empty($options['exhibitionid']) ) $html_print.="if ( selected==0 ) {\n";
                        if( !empty($options['previous']) ) $html_print.="window.location='".$options['previous']."';\n";
                    if( !empty($options['exhibitionid']) ) $html_print.="}\n";
                $html_print.="}\n";
                $html_print.="if ( event.keyCode == 39 ) {\n";
                    if( !empty($options['exhibitionid']) ) $html_print.="if ( selected==0 ) {\n";
                        if( !empty($options['next']) ) $html_print.="window.location='".$options['next']."';\n";
                    if( !empty($options['exhibitionid']) ) $html_print.="}\n";
                $html_print.="}\n";
            $html_print.="})\n";
        $html_print.="</script>\n";
        # END javascript for item next previous keyboard arrows

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }


    # currently used only for photos section
    function previous_next_links_photos($db,$options)
    {
        $html_print="";

        if( $options['row']['year']>=1960 && $options['row']['year']<=1969 ) $year=" year BETWEEN '1960' AND '1969' ";
        elseif( $options['row']['year']>=1970 && $options['row']['year']<=1979 ) $year=" year BETWEEN '1970' AND '1979' ";
        elseif( $options['row']['year']>=1980 && $options['row']['year']<=1989 ) $year=" year BETWEEN '1980' AND '1989' ";
        elseif( $options['row']['year']>=1990 && $options['row']['year']<=1999 ) $year=" year BETWEEN '1990' AND '1999' ";
        else $year=" year BETWEEN '2000' AND '".date("Y")."' ";

        $query="SELECT photoID FROM ".TABLE_PHOTOS." WHERE ".$year." ORDER BY photoID ASC ";
        $results2=$db->query($query);
        $count=$db->numrows($results2);

        $i=0;
        while($row2=$db->mysql_array($results2))
        {
            if( $i==0 ) $first=$row2['photoID'];
            else $last=$row2['photoID'];
            $i++;
            $tmp[$i]=$row2;
            if( $row2['photoID']==$options['row']['photoID'] ) $selected=$i;
        }

        for($i=1;$i<=sizeof($tmp);$i++)
        {
            if( $i==$selected-1 ) $previous=$tmp[$i]['photoID'];
            if( $i==$selected+1 ) $next=$tmp[$i]['photoID'];
        }

        $options_button=array();
        $options_button=$options;
        $options_button['section']="photos";
        $options_button['previous']=$previous;
        $options_button['next']=$next;
        $options_button['selected']=$selected;
        $options_button['count']=$count;
        $options_button['last']=$last;
        $options_button['first']=$first;

        $html_print.=$this->previous_next_links_buttons($db,$options_button);

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }



    # currently used only for photos section
    //function previous_next_links_buttons($db,$section,$previous,$next,$selected,$count,$last,$first,$htaccess=0)
    function previous_next_links_buttons($db,$options)
    {
        $html_print="";

        # javascript for painting view next previous
        if( empty($options['next']) ) $js_next=$options['first'];
        else $js_next=$options['next'];
        if( empty($options['previous']) ) $js_previous=$options['last'];
        else $js_previous=$options['previous'];
        $html_print.="<script type='text/javascript'>\n";
            $html_print.="var previous_painting='".$js_previous."';\n";
            $html_print.="var next_painting='".$js_next."';\n";
            if( !empty($options['search_vars']) ) $html_print.="var search_vars='".$options['search_vars']."';\n";
            $html_print.="var url_painting='".$options['url']."';\n";
        $html_print.="</script>\n";
        # END javascript for painting view next previous
        if( empty($options['previous']) )
        {
            $url_previous="";
            if( !empty($options['search_vars']) ) $url_previous.=$options['search_vars'];
            $previous_link="<a href='".$url_previous."' class='first'><img src='/g/base/arrow_left.gif' alt='previous photo' width='14' height='14' /></a>";
        }
        else
        {
            $url_previous=$options['url'].$options['previous'];
            if( !empty($options['search_vars']) ) $url_previous.=$options['search_vars'];
            $previous_link="<a href='".$url_previous."' class='first'><img src='/g/base/arrow_left.gif' alt='previous photo' width='14' height='14' /></a>";
        }
        $previous_link.="<span class='page-numbers' >";


        $next_link="</span>";
        if( empty($options['next']) )
        {
            $url_next=$options['url'].$options['first'];
            if( !empty($options['search_vars']) ) $url_next.=$options['search_vars'];
            $next_link.="<a href='".$url_next."' class='last'><img src='/g/base/arrow_right.gif' alt='next photo' width='14' height='14' /></a>";
        }
        else
        {
            $url_next=$options['url'].$options['next'];
            if( !empty($options['search_vars']) ) $url_next.=$options['search_vars'];
            $next_link.="<a href='".$url_next."' class='last'><img src='/g/base/arrow_right.gif' alt='next photo' width='14' height='14' /></a>";
        }

        $text=$previous_link;
        if( $_GET['lang']=="zh" ) $text.=$options['count']." ".LANG_OF." ".$options['selected']." ";
        else $text.=$options['selected']." ".LANG_OF." ".$options['count']." ";
        $text.=$next_link;

        ### draw previous and next links
        $html_print.="<form class='pages-form top' method='get' >";
        $html_print.="  <span class='pages'>";
        $html_print.=$text;

        ### shows decades link for PHOTOS
        /*
        if( stristr($options['previous'],"photoID") || stristr($options['next'],"photoID") || stristr($options['last'],"photoID") || stristr($options['first'],"photoID") )
        {

            list($name,$photoID) = explode("=", $options['previous']);
            if( empty($photoID) ) list($name,$photoID) = explode("=", $options['last']);

            $results=$db->query("SELECT year FROM ".TABLE_PHOTOS." WHERE photoID='".$photoID."'");
            $row=$db->mysql_array($results);

            if( $row['year']>=1960 && $row['year']<=1969 )
            {
                $year2=" year BETWEEN '2000' AND '".date('Y')."' ";
                $year=" year BETWEEN '1970' AND '1979' ";
                $decade="1960-1969";
            }
            elseif( $row['year']>=1970 && $row['year']<=1979 )
            {
                $year2=" year BETWEEN '1960' AND '1969' ";
                $year=" year BETWEEN '1980' AND '1989' ";
                $decade="1970-1979";
            }
            elseif( $row['year']>=1980 && $row['year']<=1989 )
            {
                $year2=" year BETWEEN '1970' AND '1979' ";
                $year=" year BETWEEN '1990' AND '1999' ";
                $decade="1980-1989";
            }
            elseif( $row['year']>=1990 && $row['year']<=1999 )
            {
                $year2=" year BETWEEN '1980' AND '1989' ";
                $year=" year BETWEEN '2000' AND '".date('Y')."' ";
                $decade="1990-1999";
            }
            else
            {
                $year2=" year BETWEEN '1990' AND '1999' ";
                $year=" year BETWEEN '1960' AND '1969' ";
                $decade="2000-".date('Y');
            }

            $results=$db->query("SELECT photoID FROM ".TABLE_PHOTOS." WHERE ".$year2." ORDER BY photoID ASC");
            $row2=$db->mysql_array($results);

            $results=$db->query("SELECT photoID FROM ".TABLE_PHOTOS." WHERE ".$year." ORDER BY photoID ASC");
            $row3=$db->mysql_array($results);

            $html_print.="<span class='next-decade'>";
                $html_print.="<a href='".$options['url'].$row2['photoID']."'><img src='/g/base/arrow_left.gif' alt='".LANG_PREVDECADE."' width='14' height='14' /></a>";
                    $html_print.="<span class='page-numbers'>".$decade."</span>";
                $html_print.="<a href='".$options['url'].$row3['photoID']."'><img src='/g/base/arrow_right.gif' alt='".LANG_NEXTDECADE."' width='14' height='14' /></a>";
            $html_print.="</span>";
        }
        */
        #end

        $html_print.="  </span>";
        $html_print.="</form>";
        #end

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    function div_thumb_images($db,$options)
    {
        //print $options['values_from']."-".$options['values_to'];
        $html_print.="";

        if( empty($options['cache_dir']) ) $options['cache_dir']="cache";

        # style
        if( !empty($options['style']) )
        {
            $style="style='".$options['style']."'";
        }
        else
        {
            $style="";
        }
        #end

        # data-test
        if( !empty($options['data-test']) )
        {
            $data_test="data-test='".$options['data-test']."'";
        }
        else
        {
            $data_test="";
        }
        #end

        //$html_print.="<div class='div-section-categories ".$options['class']['div-thumbs-global-search']." ".$options['class']['div-thumbs']." ".$options['class']['div-video-paint-thumbs']." ".$options['class']['category']."' ".$style." ".$data_test." >\n";
        $html_print.="<div class='div-section-categories ".$options['class']['div-section-categories']."' ".$style." ".$data_test." >\n";

            ### if there is no search result found
            if( is_array($options['results']) ) $count_total=count($options['results']);
            else $count_total=$db->numrows($options['results']);
            if( empty($count_total) ) $html_print.=LANG_SEARCH_NORESULTS.".";
            //print $count_total;
            #end

            if( !is_array($options['results']) )
            {
                while( $row=$db->mysql_array($options['results']) )
                {
                    $thumb_array[]=$row;
                }
            }
            else $thumb_array=$options['results'];

            $display=0;

            if( $options['show_all'] )
            {
                $options['values_from']=0;
                $options['values_to']=$count_total;
            }
            $iii=0;

            for( $ii=$options['values_from'];$ii<$options['values_to'];$ii++ )
            {
                $iii++;
                $row=$thumb_array[$ii];
                $row=UTILS::html_decode($row);

                # PAINTID
                if( $row['typeid1']==14 || $row['typeid2']==14 ) $typeid=14;
                elseif( $row['typeid1']==13 || $row['typeid2']==13 ) $typeid=13;
                else $typeid=1;
                if( empty($row['paintID']) ) $paintid=UTILS::get_relation_id($db,$typeid,$row);
                else $paintid=$row['paintID'];
                if( !empty($paintid) )
                {
                    $query_painting="SELECT paintID,artworkID FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ";
                    $query_painting.=" AND enable=1 ";
                    $query_painting.=" LIMIT 1";
                    $results_painting=$db->query($query_painting);
                    $row_item=$row;
                    $count=$db->numrows($results_painting);
                    $itemid=$paintid;
                    $title_value="title";
                    $url_admin_edit="/admin/paintings/edit/?paintid=".$paintid;
                    $display=1;
                    if( empty($options['url']) )
                    {
                        $options_painting_url=array();
                        $options_painting_url['artworkid']=$row_item['artworkID'];
                        $options_painting_url['paintid']=$row_item['paintID'];
                        $painting_url=UTILS::get_painting_url($db,$options_painting_url);
                    }
                }
                #end

                # NOTEID
                if( !empty($row['noteid']) )
                {
                    $row_item=$row;
                    $count=1;
                    $typeid=15;
                    $itemid=$row['noteid'];
                    $noteid=$row['noteid'];
                    $title_value="note";
                    $url_admin_edit="/admin/paintings/notes/edit/?noteid=".$row['noteid'];
                    $display=1;
                }
                #end

                # INSTALLATION PHOTO
                $imageid=UTILS::get_relation_id($db,"17",$row);
                if( !empty($imageid) )
                {
                    $query_where_installation=" WHERE installationid='".$options['installationid']."' ";
                    $query_installation=QUERIES::query_exhibitions_installations($db,$query_where_installation);
                    $results=$db->query($query_installation['query']);
                    $row=$db->mysql_array($results);
                    $titleurl_installation=UTILS::row_text_value($db,$row,"titleurl");
                    $query_where_installation_image=" WHERE imageid='".$imageid."' LIMIT 1 ";
                    $query_installation_image=QUERIES::query_images($db,$query_where_installation_image,$query_order,$query_limit);
                    $results_inst_images=$db->query($query_installation_image['query']);
                    $row_item=$db->mysql_array($results_inst_images);
                    $count=$db->numrows($results_inst_images);
                    $title_value="notes";
                    $url_admin_edit="/admin/images/edit/?imageid=".$imageid;
                    $display=1;
                    $id="id='installation-photo-".$imageid."'";
                    $data_fancybox_group="data-fancybox-group='installation-photos'";
                }
                #end

                # BIOGRAPHY PHOTOS
                if( !empty($row['photoID']) )
                {
                    $query_where_photo=" WHERE photoID='".$row['photoID']."' ";
                    $query_photo=QUERIES::query_photos($db,$query_where_photo);
                    $results_photo=$db->query($query_photo['query']);
                    $count=$db->numrows($results_photo);
                    $row_photo=$db->mysql_array($results_photo);
                    $url_admin_edit="/admin/photos/edit/?photoid=".$row['photoID'];
                    $display=1;
                    $itemid=$row['photoID'];
                    $typeid=8;
                }
                #end

                # Paintings photoid
                if( !empty($row['painting_photoid']) )
                {
                    $itemid=$row['painting_photoid'];
                    $typeid=24;
                    $row_item=$row;
                    $title_value="title";
                    $url_admin_edit="/admin/paintings/photos/edit/?painting_photoid=".$itemid;
                    $display=1;
                    $count=1;
                    $item_desc=UTILS::row_text_value($db,$row,"text");
                    $src_image=DATA_PATH_DATADIR."/painting/photos/cache/";
                    $imageid=$itemid;
                    $options['search_vars']=".jpg";
                    $id="id='painting-photo-".$imageid."'";
                    $data_fancybox_group="data-fancybox-group='painting-photos'";
                }
                #end

                # Microsites at global seach
                if( !empty($row['micrositeid']) )
                {
                    $results_microsites=$db->query("SELECT titleurl FROM ".TABLE_MICROSITES." WHERE micrositeid='".$row['micrositeid']."' LIMIT 1 ");
                    $row_microsites=$db->mysql_array($results_microsites);
                    $itemid=$row['micrositeid'];
                    $typeid=3;
                    $row_item=$row;
                    $title_value="title";
                    $url_admin_edit="/admin/microsites/edit/?micrositeid=".$itemid;
                    $painting_url['url']="/".$_GET['lang']."/art/microsites/".$row_microsites['titleurl'];
                    $display=1;
                    $count=1;
                    $item_desc=UTILS::row_text_value($db,$row,"text");
                    $src_image="/images/size_s__imageid_";
                }
                #end

                if( $count && $display )
                {
                    # IMAGE get from db
                    if( empty($imageid) )
                    {
                        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='".$typeid."' AND itemid2='".$itemid."' ) OR ( typeid2=17 AND typeid1='".$typeid."' AND itemid1='".$itemid."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                        $row_related_image=$db->mysql_array($results_related_image);
                        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    }

                    if( !empty($src_image) )
                    {
                        if( !empty($row['painting_photoid']) )
                        {
                            $src=$src_image.$imageid.".png";
                        }
                        else
                        {
                            $src=$src_image.$imageid.".jpg";
                        }
                    }
                    else
                    {
                        # Check cache image
                        $src_cache_uri=$imageid.".png";

                        if( empty($imageid) )
                        {
                            $src="/g/no-img-available/no_image_thumbnail.png";
                        }
                        else
                        {
                            $src=DATA_PATH_DATADIR."/images_new/".$options['cache_dir']."/".$src_cache_uri;
                        }
                    }
                    #end

                    # this prints path to fancybox image
                    if( $options['fancybox']==1 && !empty($options['url_image']) )
                    {
                        $alt=$options['url_image'].$options['url_image_after'].$imageid."#tabs";
                    }
                    else $alt="";
                    #end

                    # draw image thumb
                    $img_html="\t<div class='div-thumb-img-border' >\n";
                        $img_html.="\t<img src='".$src."' alt='".$alt."' />\n";
                    $img_html.="\t</div>\n";
                    # END draw image thumb

                    # ************ HREF - LINK - URL ***************
                    if( !empty($noteid) || !empty($options['installationid']) )
                    {
                        $href=$options['url'].$imageid.".jpg";
                    }
                    elseif( !empty($paintid) )
                    {
                        $options_paint_url=array();
                        $options_paint_url['painting_no_section']=$options['painting_no_section'];
                        $options_paint_url['paintid']=$row['paintID'];
                        $painting_url=UTILS::get_painting_url($db,$options_paint_url);
                        if( $options_paint_url['painting_no_section'] )
                        {
                            $href=$options['url'].$painting_url['url'];
                        }
                        else
                        {
                            $href=$painting_url['url'];
                        }

                        if( !empty($options['url_after']) ) $href.=$options['url_after'];
                        if( !empty($options['search_vars']) ) $href.="/?".$options['search_vars'];
                    }
                    else
                    {
                        if( !empty($options['url']) ) $href=$options['url'].$itemid;
                        else $href=$painting_url['url'];
                        if( !empty($options['search_vars']) ) $href.=$options['search_vars'];
                    }
                    # END ************ HREF - LINK - URL ***************

                    # Display per line
                    $i++;
                    if( $iii<=$options['thumbs_per_line'] ) $class_first_row=" div-thumb-first-row ";
                    else $class_first_row="";
                    //if( $row_item['display']==1 ) $class_thumb="div-thumb-one";
                    //else
                    //{
                        $class_thumb="div-thumb";
                        if( $i==$options['thumbs_per_line'] || $iii==($options['values_to']-$options['values_from']) ) $class_last=" div-thumb-last ";
                        else $class_last="";
                        if( !empty($noteid) ) $class_tab="div-thumb-tab";
                        else $class_tab="";
                    //}
                    if( $options['show_title'] ) $class_with_title="div-thumb-with-title";
                    else $class_with_title="";
                    #end

                    $html_print.="<div class='".$class_thumb." ".$class_with_title." ".$class_first_row." ".$class_last." ".$class_tab."' >\n";

                        ### TITLE section
                        if( $options['show_title'] )
                        {
                            if( $row_item['display']!=1 ) $title_html="<br />";
                            $title_html="";
                            $title_html.="<span class='span-thumb-title'>";
                            $title_clean="";

                            # thumbnail info for prints artworks
                            if( $row['artworkID']==16 )
                            {
                                # number and title
                                $title_html.="<span class='span-painting-titleandnumber-prints'>";
                                    $title_html.="<span class='span-painting-number-prints'>";
                                        $title_html.=$row['number'];
                                    $title_html.="</span>&nbsp;";
                                    $title_html.="<span class='span-painting-title-prints'>";
                                        $title_painting=UTILS::row_text_value2($db,$row_item,"title");
                                        $title_html.="<q>".$title_painting."</q>";
                                    $title_html.="</span>";
                                $title_html.="</span>";
                                # year
                                if( !empty($row['year']) )
                                {
                                    $title_html.="<span class='span-painting-year-prints'>".$row['year']."</span>";
                                }
                                # size
                                if( !empty($row['size']) )
                                {
                                    $title_html.="<span class='span-painting-size-prints'>".$row['size']."</span>";
                                }
                                # medium
                                if( !empty($row['mID']) )
                                {
                                    $results_media=$db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mID='".$row['mID']."' LIMIT 1");
                                    $row_media=$db->mysql_array($results_media);
                                    $row_media=UTILS::html_decode($row_media);
                                    $media=UTILS::row_text_value2($db,$row_media,"medium");
                                    $title_html.="<span class='span-painting-medium-prints'>".$media."</span>";
                                }
                            }
                            # END thumbnail info for prints artworks

                            # thumbnail info for rest artworks
                            else
                            {
                                if( $options['show_one_title'] )
                                {
                                    $title_current_lang=UTILS::row_text_value2($db,$row_item,$title_value);
                                    $title_html.="<span class='span-painting-title1'>".$title_current_lang."</span>";
                                    $title_clean.=strip_tags($title_current_lang);
                                    $title_clean=str_replace("'", "", $title_clean);
                                }
                                else
                                {
                                    # title original and translated
                                    $options_title=array();
                                    $options_title['title']=$title_value;
                                    $options_title['return']=1;
                                    $options_title['row']=$row_item;
                                    $options_title['search']=$options['search'];
                                    $title_html.=UTILS::get_painting_title($db,$options_title);
                                    $title_no_br=str_replace("<br />","\n",$title_html);
                                    $title_clean.=strip_tags($title_no_br);
                                    $title_clean=str_replace("'", "", $title_clean);
                                }

                                # year
                                if( !empty($row['year']) && $row['year']!="n.a." )
                                {
                                    $title_html.="<span class='span-painting-year'>".$row['year']."</span>";
                                }
                                else
                                {
                                    $title_html.="<span class='span-painting-year'>&nbsp;</span>";
                                }

                                # number
                                if( !empty($row['number']) || $options['show_artworkid'] )
                                {
                                    $artwork_info=UTILS::get_artwork_info($db,$row['artworkID']);
                                }

                                # arwtrok info
                                if( $options['show_artworkid'] )
                                {
                                    $artwork=UTILS::row_text_value2($db,$artwork_info,"artwork");
                                    $title_html.="<span class='span-painting-artworkid'>".$artwork."</span>";
                                }

                                # number
                                if( !empty($row['number']) )
                                {
                                    $title_html_number=$artwork_info['nr'].": ";
                                    $title_html.="<span class='span-painting-number'>";
                                        $title_html.=$title_html_number;
                                        $title_html.=$row['number'];
                                    $title_html.="</span>";
                                }

                                # desc - currently only for microsite thumbs
                                if( !empty($item_desc) )
                                {
                                    $title_html.="<span class='span-painting-desc'>";
                                        $item_desc=strip_tags($item_desc);
                                        $item_desc=substr($item_desc,0,120);
                                        $title_html.=$item_desc."..";
                                    $title_html.="</span>";
                                }

                                # sort
                                if( $_SESSION['debug_page'] )
                                {
                                    $title_html.="<span class='span-painting-year'>".$row['sort2']."</span>";
                                }

                                # if its a book and tad artworks related show discussed mentioned illustrated
                                if( !empty($options['bookid']) )
                                {
                                    /*
                                    $query_relations_literature="SELECT
                                                  rl.*
                                                FROM
                                                  ".TABLE_RELATIONS_LITERATURE." rl
                                                LEFT JOIN
                                                  ".TABLE_RELATIONS." r1 ON r1.typeid1 = 1 AND r1.itemid1='".$row['paintID']."' AND r1.typeid2 = 12 AND r1.relationid = rl.relationid
                                                LEFT JOIN
                                                  ".TABLE_RELATIONS." r2 ON r2.typeid2 = 1 AND r2.itemid2='".$row['paintID']."' AND r2.typeid1 = 12 AND r2.relationid = rl.relationid
                                                WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL )
                                            ";
                                    */

                                    $query_relations_literature="SELECT rl.*
                                    FROM ".TABLE_RELATIONS." r, ".TABLE_RELATIONS_LITERATURE." rl
                                    WHERE ( ( r.typeid1=1 AND r.itemid1='".$row['paintID']."' AND r.typeid2=12 AND r.itemid2='".$options['bookid']."' )
                                            OR ( r.typeid2=1 AND r.itemid2='".$row['paintID']."' AND r.typeid1=12 AND r.itemid1='".$options['bookid']."' ) )
                                        AND r.relationid=rl.relationid
                                        ";

                                    $results_relations_literature=$db->query($query_relations_literature);
                                    $count_relations_literature=$db->numrows($results_relations_literature);
                                    $ment_disc_illu=array();
                                    if( $count_relations_literature )
                                    {
                                        while( $row_relations_literature=$db->mysql_array($results_relations_literature,0) )
                                        {
                                            if( !$ment_disc_illu['mentioned'] || !$ment_disc_illu['discussed'] || !$ment_disc_illu['illustrated'] )
                                            {
                                                # MENTIONED
                                                if( !empty($row_relations_literature['mentioned']) || !empty($row_relations_literature['show_mentioned']) )
                                                {
                                                    $ment_disc_illu['mentioned']=1;
                                                }

                                                # DISCUSSED
                                                if( !empty($row_relations_literature['discussed']) || !empty($row_relations_literature['show_discussed']) )
                                                {
                                                    $ment_disc_illu['discussed']=1;
                                                }

                                                # ILLUSTRATED
                                                if( !empty($row_relations_literature['illustrated_bw']) || !empty($row_relations_literature['illustrated']) || !empty($row_relations_literature['show_illustrated']) || !empty($row_relations_literature['show_bw']) )
                                                {
                                                    $ment_disc_illu['illustrated']=1;
                                                }
                                                # END ILLUSTRATED
                                            }
                                        }
                                        if( $ment_disc_illu['mentioned'] ) $title_html.= "<span class='span-ment-disc-illus'>".LANG_ARTWORK_BOOKS_REL_MENTIONED."</span>";
                                        if( $ment_disc_illu['discussed'] ) $title_html.= "<span class='span-ment-disc-illus'>".LANG_ARTWORK_BOOKS_REL_DISCUSSED."</span>";
                                        if( $ment_disc_illu['illustrated'] ) $title_html.= "<span class='span-ment-disc-illus'>".LANG_ARTWORK_BOOKS_REL_ILLUSTRATED."</span>";
                                    }
                                }
                                # END - if its a book and......

                            }
                            # END thumbnail info for rest artworks

                            $title_html.="</span>"; # END - span-thumb-title
                        }
                        elseif( $options['show_a_title'] )
                        {
                            $options_title=array();
                            $options_title['title']=$title_value;
                            $options_title['return']=1;
                            $options_title['row']=$row_item;
                            $title_clean=UTILS::get_painting_title($db,$options_title);
                            $title_clean=strip_tags($title_clean);
                            $title_clean=str_replace("'", "", $title_clean);
                        }
                        #end TITLE section

                        #fancybox
                        if( $options['fancybox']==1 )
                        {
                            $class_fancybox="fancybox-buttons";
                            //$data_fancybox_group="data-fancybox-group='installation-photos'";
                            //$id="id='installation-photo-".$imageid."'";
                            //$id=$a_id;
                        }
                        else
                        {
                            $class_fancybox="";
                            $data_fancybox_group="";
                            $id="";
                        }
                        #end

                        #target
                        if( !empty($options['target']) )
                        {
                            $target="target='".$options['target']."'";
                        }
                        else
                        {
                            $target="";
                        }
                        #end

                        if( $class_thumb=="div-thumb-one" )
                        {
                            $html_print.="<div class='div_tab_left'>\n";
                                $html_print.="<a href='".$href."' title='".$title_clean."' ".$id." ".$rel." ".$target." ".$data_fancybox_group." class='a-thumb-link ".$class_fancybox." ".$class."' >\n";
                                    $html_print.=$img_html;
                                $html_print.="</a>\n";
                            $html_print.="</div>\n";
                            $html_print.="<div class='div_tab_right'>\n";
                                $html_print.=$title_html;
                            $html_print.="</div>\n";
                            $html_print.="<br class='clearer' />\n";
                        }
                        else
                        {
                            $html_print.="<a href='".$href."' title='".$title_clean."' ".$id." ".$rel." ".$target." ".$data_fancybox_group." class='a-thumb-link ".$class_fancybox."' >\n";
                                $html_print.=$img_html;
                                $html_print.=$title_html;

                                ### Sales history
                                $results_sale_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 from ".TABLE_RELATIONS." WHERE ( typeid1=1 AND itemid1='".$paintid."' AND typeid2=2 ) OR ( typeid2=1 AND itemid2='".$paintid."' AND typeid1=2 ) LIMIT 1");
                                $count_sale=$db->numrows($results_sale_relations);

                                /*
                                $count_sale=0;
                                while( $row_sale_relations=$db->mysql_array($results_sale_relations) )
                                {
                                    $saleid=UTILS::get_relation_id($db,"2",$row_sale_relations);
                                    $results_sale=$db->query("SELECT saleID FROM ".TABLE_SALEHISTORY." WHERE saleID='".$saleid."' AND enable=1 LIMIT 1 ");
                                    if( $db->numrows($results_sale)>0 ) $count_sale++;
                                }
                                */

                                if( $count_sale>0 && !empty($row['locationid']) )
                                {
                                    $class_museum="info-museum2";
                                }
                                else $class_museum="";

                                if( $count_sale>0 ) $html_print.="<span class='info-sales' title='".LANG_ARTWORK_THUMB_SALES_HIST_AVAIL."'></span>\n";
                                # end Sales history

                                ### Museum collection
                                if( !empty($row['locationid']) ) $html_print.="<span class='info-museum ".$class_museum."' title='".LANG_ARTWORK_THUMB_MUSEUM_COl_INFO_AVAIL."'></span>\n";
                                # Museum collection

                            $html_print.="</a>\n";
                        }

                        $options_admin_edit=array();
                        $options_admin_edit=$options;
                        $options_admin_edit['class']['a-admin-edit-a']="a-admin-edit-thumb";
                        $html_print.=UTILS::admin_edit($url_admin_edit,$options_admin_edit);

                    $html_print.="</div>\n"; # END div-thumb

                    if( ( $i==$options['thumbs_per_line'] || $iii==$count_total || ($options['values_from']+$iii)==$count_total ) && $class_thumb!="div-thumb-one" )
                    {
                        $html_print.="<div class='clearer'></div>\n";
                        $i=0;
                    }
                    else
                    {
                        //$html_print.="<div style='display:none;' date-test='".$i."-".$options['thumbs_per_line']."-".$iii."-".$count_total."-".($options['values_from']+$iii)."'></div>\n";
                    }
                }
            }

        $html_print.="</div>\n"; # END div-section-categories

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

function sales_history_list($db,$options)
{
    $html_print="";

    $html_print.="<div class='sale'>\n";
        while( $row_sale_history=$db->mysql_array($options['results']) )
        {
            $row_sale_history=UTILS::html_decode($row_sale_history);

            $options_admin_edit=array();
            $options_admin_edit=$options;
            $html_print.=UTILS::admin_edit("/admin/auctions-sales/saleHistory/edit/?saleid=".$row_sale_history['saleID'],$options_admin_edit);

            $results_auction_house=$db->query("SELECT house,cityid,countryid FROM ".TABLE_AUCTIONHOUSE." WHERE ahID='".$row_sale_history['ahID']."' LIMIT 1 ");
            $row_auction_house=$db->mysql_array($results_auction_house);
            $row_auction_house=UTILS::html_decode($row_auction_house);
            $options_location=array();
            $options_location['row']=$row_auction_house;
            $location=location($db,$options_location);
            $sale_date=UTILS::row_text_value($db,$row_sale_history,"saleDate");
            $sale_date=UTILS::date_month($db,$sale_date);
            $html_print.="<h3>";
                $html_print.=$row_auction_house['house'];
                if( !empty($row_auction_house['cityid']) || !empty($row_auction_house['countryid']) ) $html_print.=", ".$location;
                $html_print.=": ".$sale_date;
            $html_print.="</h3>\n";

            if( !empty($row_sale_history['number']) )
            {
                $html_print.="<div class='info'>\n";
                    $html_print.="<span class='sales-sold-for sales-sold-for-".$_GET['lang']."'>";
                        if( $row['artworkID']==4 ) $nr="Edition number:";
                        elseif( $row['artworkID']==3 ) $nr="Atlas:";
                        else $nr="CR:";
                        $html_print.=$nr;
                    $html_print.="</span>\n";
                    $html_print.="<span class='sales-number'>\n";
                        $html_print.=$row_sale_history['number'];
                    $html_print.="</span>\n";
                    $html_print.="<br class='clear' />";
                $html_print.="</div>\n";
            }

            $notes=UTILS::row_text_value($db,$row_sale_history,"notes");
            if( !empty($notes) )
            {
                $html_print.="<div class='info'>\n";
                    $html_print.="<span class='sales-sold-for sales-sold-for-".$_GET['lang']."'>".LANG_SALEHIST_TAB_NOTES."</span>\n";
                    //$html_print.="<span class='sales-number'>".$notes."</span>\n";
                    $options_div_text=array();
                    $options_div_text['class']['div_text_wysiwyg']="div-tab-sales-number-notes";
                    $html_print.=$this->div_text_wysiwyg($db,$notes,$options_div_text);
                    $html_print.="<br class='clear' />";
                $html_print.="</div>\n";
            }

            ### ESTIMATE sales history
            if( !empty($row_sale_history['estLow']) || !empty($row_sale_history['estHigh']) || !empty($row_sale_history['estCurrID']) || !empty($row_sale_history['estLowUSD']) || !empty($row_sale_history['estHighUSD']) || !empty($row_sale_history['upon_request']) || !empty($row_sale_history['tbannounced']) )
            {
                $html_print.="<div class='info'>\n";
                    $html_print.="<span class='sales-estimate sales-estimate-".$_GET['lang']."'>".LANG_ESTIMATE.":</span>\n";
                    $html_print.="<span class='sales-estimate-details'>\n";
                        $class="";
                        if( $row_sale_history['tbannounced'] )
                        {
                            $html_print.="<span class='sales-estimate-details1'>".LANG_SALES_HISTORY_TB_ANNOUNCED."</span>";
                        }
                        elseif( $row_sale_history['upon_request'] )
                        {
                            $html_print.="<span class='sales-estimate-details1'>".LANG_SALES_HISTORY_UPON_REQUEST."</span>";
                        }
                        else
                        {
                            if( (!empty($row_sale_history['estLow']) || !empty($row_sale_history['estHigh'])) && !empty($row_sale_history['estCurrID']) )
                            {
                                $html_print.="<span class='sales-estimate-details1'>";
                                    $html_print.=UTILS::get_currency($db,$row_sale_history['estCurrID'],"currency");
                                    if( !empty($row_sale_history['estLow']) ) $html_print.=" ".UTILS::display_numbers($db,$row_sale_history['estLow']);
                                    if( !empty($row_sale_history['estLow']) && !empty($row_sale_history['estHigh']) ) $html_print.=" &#8211; ";
                                    if( !empty($row_sale_history['estHigh']) ) $html_print.=" ".UTILS::display_numbers($db,$row_sale_history['estHigh']);
                                $html_print.="</span>\n";
                                $class=" class='sales-estimate-details2' ";
                            }

                            if( !empty($row_sale_history['estLowUSD']) || !empty($row_sale_history['estHighUSD'])  )
                            {
                                $html_print.="<span ".$class.">";
                                    $html_print.=UTILS::get_currency($db,1,"currency");
                                    if( !empty($row_sale_history['estLowUSD']) ) $html_print.=" ".UTILS::display_numbers($db,$row_sale_history['estLowUSD']);
                                    if( !empty($row_sale_history['estLowUSD']) && !empty($row_sale_history['estHighUSD'])  ) $html_print.=" &#8211; ";
                                    if( !empty($row_sale_history['estHighUSD']) ) $html_print.=" ".UTILS::display_numbers($db,$row_sale_history['estHighUSD']);
                                $html_print.="</span>\n";
                            }
                        }
                    $html_print.="</span>\n";
                    $html_print.="<br class='clear' />";
                    if( empty($row_sale_history['soldFor']) && empty($row_sale_history['soldForCurrID']) && empty($row_sale_history['soldForUSD']) && !$row_sale_history['boughtIn'] && !$row_sale_history['withdrawn'] ) $html_print.="<br class='clear' />";
                $html_print.="</div>\n";
            }
            #end

            ### SALES HISTORY sold for
            if( (!empty($row_sale_history['soldFor']) && !empty($row_sale_history['soldForCurrID'])) || (!empty($row_sale_history['soldForUSD']) || $row_sale_history['boughtIn'] || $row_sale_history['withdrawn']) )
            {
                $html_print.="<div class='info'>\n";
                    $html_print.="<span class='sales-sold-for sales-sold-for-".$_GET['lang']."'>".LANG_SOLDPR.":</span>\n";
                    $html_print.="<table>\n";

                        if( !$row_sale_history['boughtIn'] && !$row_sale_history['withdrawn'] )
                        {
                            $saletype="";
                            if( (!empty($row_sale_history['soldFor']) && !empty($row_sale_history['soldForCurrID'])) || !empty($row_sale_history['soldForUSD']) )
                            {
                                if( !empty($row_sale_history['saleType']) && $row_sale_history['saleType']!=3 )
                                {
                                    $style=" padding-left:10px; ";
                                    if( empty($row_sale_history['soldForUSD']) || (!empty($row_sale_history['soldForUSD']) && (!empty($row_sale_history['soldFor']) && !empty($row_sale_history['soldForCurrID']))) )
                                    {
                                        $rowspan=" rowspan='2' ";
                                    }
                                    $saletype="\t<td $rowspan style='".$style."'>\n";
                                        if( $row_sale_history['saleType']==1 ) $saletype.=LANG_PREMIUM;
                                        elseif( $row_sale_history['saleType']==2 ) $saletype.=LANG_HAMMER;
                                    $saletype.="\t</td>\n";
                                }
                            }

                            if( !empty($row_sale_history['soldFor']) && !empty($row_sale_history['soldForCurrID']) )
                            {
                                $html_print.="<tr>\n";
                                    $html_print.="<td>\n";
                                        $html_print.=UTILS::get_currency($db,$row_sale_history['soldForCurrID'],"currency")." ".UTILS::display_numbers($db,$row_sale_history['soldFor']);
                                    $html_print.="</td>\n";
                                    if( empty($row_sale_history['soldForUSD']) || (!empty($row_sale_history['soldForUSD']) && (!empty($row_sale_history['soldFor']) && !empty($row_sale_history['soldForCurrID']))) )
                                    {
                                        $html_print.=$saletype;
                                    }
                                $html_print.="</tr>\n";
                            }
                            if( !empty($row_sale_history['soldForUSD']) )
                            {
                                $html_print.="<tr>\n";
                                    if( !empty($row_sale_history['soldFor']) ) $style="padding-top:3px;";
                                    else $style="";
                                    $html_print.="<td style='".$style."'>\n";
                                        $html_print.=UTILS::get_currency($db,1,"currency")." ".UTILS::display_numbers($db,$row_sale_history['soldForUSD']);
                                    $html_print.="</td>\n";
                                    if( empty($row_sale_history['soldFor']) && empty($row_sale_history['soldForCurrID']) )
                                    {
                                        $html_print.=$saletype;
                                    }
                                $html_print.="</tr>\n";
                            }
                        }
                        else
                        {
                            $html_print.="<tr>\n";
                                $html_print.="<td>\n";
                                    if( $row_sale_history['withdrawn'] ) $html_print.=LANG_WITHDRAWN;
                                    else $html_print.=LANG_BOUGHT_IN;
                                $html_print.="</td>\n";
                            $html_print.="</tr>\n";
                        }
                    $html_print.="</table>\n";
                    $html_print.="<br class='clear' />";
                $html_print.="</div>\n";
            }
            #end
        }
    $html_print.="</div>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function div_opp_brief($db,$options=array())
{
    $html_print="";

    if( empty($options['href']) ) $options['href']="mailto:".EMAIL_OPP;
    $html_print.="<div class='div-brief-trp'>\n";
        $html_print.="<div class='div-brief-left'>\n";
            $html_print.="<img src='/g/opp_form/left-image.jpg' alt='' class='img-brief' />\n";
            //$html_print.="<a href='#div-brief-popup-content' title='' id='a_form_opp_read_more'>\n";
                //$html_print.=LANG_CATEGORY_DESC_MORE;
            //$html_print.="</a>\n";
            $html_print.="<div class='clearer'></div>\n";
        $html_print.="</div>\n";
        $html_print.="<div class='div-brief-right ".$options['class']['div-brief-right-large']."'>\n";
            $html_print.="<span class='span-opp-title'>".LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS."</span>\n";
            $html_print.="<p>".LANG_OPP_FORM_1."</p>\n";
            //$html_print.="<br /><p>";
                $html_print.=LANG_OPP_FORM_1_1;
                $html_print.="<a href='#div-brief-popup-content' title='' id='a_form_opp_click_here'>".LANG_OPP_FORM_1_2."</a>.";
            $html_print.="</p>\n";
        $html_print.="</div>\n";
        $html_print.="<div id='div-brief-popup-content'>\n";
            $html_print.="<span class='span-opp-title'>".LANG_OPP_FORM_CATALOGUE_TITLE_OVERPAINTED_PHOTOS."</span>\n";
            $html_print.="<p>".LANG_OPP_FORM_1."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_2."</p>\n";
            $LANG_OPP_FORM_3=LANG_OPP_FORM_3;
            if( !empty($LANG_OPP_FORM_3) ) $html_print.="<p>".LANG_OPP_FORM_3."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_4."</p>\n";

            $html_print.="<ul class='ul-opp-collecting'>\n";
                $html_print.="<li>".LANG_OPP_FORM_5."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_6."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_7."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_8."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_9."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_10."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_11."</li>\n";
                $html_print.="<li>".LANG_OPP_FORM_12."</li>\n";
            $html_print.="</ul>\n";

            $html_print.="<p>".LANG_OPP_FORM_13."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_14."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_15."</p>\n";
            $html_print.="<p style='font-weight:700;'>".LANG_OPP_FORM_16."</p>\n";
            $html_print.="<br />\n";
            $html_print.="<p class='span-opp-title'>".LANG_OPP_FORM_17."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_18."</p>\n";
            $html_print.="<p>".LANG_OPP_FORM_19."</p>\n";

            $html_print.="<form action='/post/post-opp.php' method='post' class='form-opp-submit-info' enctype='multipart/form-data'>\n";
                $html_print.="<div class='div-opp-section-divider'></div>\n";

                $html_print.="<table class='table-opp-1'>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='title-opp' id='title_text'>".LANG_OPP_FORM_20."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<input type='text' name='title' id='title-opp' value='".$_SESSION['form_opp']['title']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='date' id='date_text'>".LANG_OPP_FORM_21."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<input type='text' name='date' id='date' value='".$_SESSION['form_opp']['date']."' class='input-opp-form-large' placeholder='".LANG_OPP_FORM_21_1."' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='medium' id='medium_text'>".LANG_OPP_FORM_22."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<select name='medium' id='medium' style='width: 304px;'>\n";
                                $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                                $value=LANG_OPP_FORM_23;
                                if( $_SESSION['form_opp']['medium']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_24;
                                if( $_SESSION['form_opp']['medium']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_25;
                                if( $_SESSION['form_opp']['medium']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_26;
                                if( $_SESSION['form_opp']['medium']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_26_1;
                                if( $_SESSION['form_opp']['medium']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                            $html_print.="</select>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='dimensions_photo_height' id='dimensions_photo_text'>".LANG_OPP_FORM_27."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<input type='text' name='dimensions_photo_height' id='dimensions_photo_height' value='".$_SESSION['form_opp']['dimensions_photo_height']."' class='input-opp-form-date' />\n";
                            $html_print.=" &#215; <input type='text' name='dimensions_photo_width' id='dimensions_photo_width' value='".$_SESSION['form_opp']['dimensions_photo_width']."' class='input-opp-form-date' />\n";
                            $html_print.="<select name='dimensions_photo_type' style='width: 104px;'>\n";
                                $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                                if( $_SESSION['form_opp']['dimensions_mount_type']==LANG_OPP_FORM_28 ) $selected="selected='selected'";
                                else $selected="";
                                $html_print.="<option value='".LANG_OPP_FORM_28."' ".$selected.">".LANG_OPP_FORM_28."</option>\n";
                                if( $_SESSION['form_opp']['dimensions_mount_type']==LANG_OPP_FORM_29 ) $selected="selected='selected'";
                                else $selected="";
                                $html_print.="<option value='".LANG_OPP_FORM_29."' ".$selected.">".LANG_OPP_FORM_29."</option>\n";
                            $html_print.="</select>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='dimensions_mount_height' id='dimensions_mount_text'>".LANG_OPP_FORM_30."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<input type='text' name='dimensions_mount_height' id='dimensions_mount_height' value='".$_SESSION['form_opp']['dimensions_mount_height']."' class='input-opp-form-date' />\n";
                            $html_print.=" &#215; <input type='text' name='dimensions_mount_width' id='dimensions_mount_width' value='".$_SESSION['form_opp']['dimensions_mount_width']."' class='input-opp-form-date' />\n";
                            $html_print.="<select name='dimensions_mount_type' style='width: 104px;'>\n";
                                $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                                if( $_SESSION['form_opp']['dimensions_mount_type']==LANG_OPP_FORM_28 ) $selected="selected='selected'";
                                else $selected="";
                                $html_print.="<option value='".LANG_OPP_FORM_28."' ".$selected.">".LANG_OPP_FORM_28."</option>\n";
                                if( $_SESSION['form_opp']['dimensions_mount_type']==LANG_OPP_FORM_29 ) $selected="selected='selected'";
                                else $selected="";
                                $html_print.="<option value='".LANG_OPP_FORM_29."' ".$selected.">".LANG_OPP_FORM_29."</option>\n";
                            $html_print.="</select>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='series' id='series_text'>".LANG_OPP_FORM_33."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<select name='series' id='series' style='width: 304px;'>\n";
                                $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                                $value=LANG_OPP_FORM_35;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_36;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_37;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_38;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_38_1;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_38_2;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_38_3;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                                $value=LANG_OPP_FORM_38_4;
                                if( $_SESSION['form_opp']['series']==$value ) $selected=" selected='selected' ";
                                else $selected="";
                                $html_print.="<option value='".$value."' ".$selected.">".$value."</option>\n";
                            $html_print.="</select>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='editions_number' id='editions_number_text'>".LANG_OPP_FORM_39."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";
                            $html_print.="<input type='text' name='editions_number' id='editions_number' value='".$_SESSION['form_opp']['editions_number']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    # Front signed
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_recto_signed_1' id='signature_front_text_1'>".LANG_OPP_FORM_40_1."</label>\n";
                            $html_print.=LANG_OPP_FORM_40_1_1;
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_signed_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_signed_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='recto_signed_3' name='recto_signed_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['recto_signed_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Front signed

                    # Front dated
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_recto_dated_1' id='signature_front_text_2'>".LANG_OPP_FORM_40_1_2."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_dated_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_dated_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='recto_dated_3' name='recto_dated_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['recto_dated_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Front dated

                    # Front numbered
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_recto_numbered_1' id='signature_front_text_3'>".LANG_OPP_FORM_40_1_3."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_numbered_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_numbered_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='recto_numbered_3' name='recto_numbered_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['recto_numbered_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Front numbered

                    # Front inscribed
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_recto_inscribed_1' id='signature_front_text_4'>".LANG_OPP_FORM_40_1_4."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_inscribed_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,2,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_recto_inscribed_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='recto_inscribed_3' name='recto_inscribed_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['recto_inscribed_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Front inscribed

                    # Back signed
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_verso_signed_1' >".LANG_OPP_FORM_40_2."</label>\n";
                            $html_print.=LANG_OPP_FORM_40_2_1;
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_signed_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_signed_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='verso_signed_3' name='verso_signed_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['verso_signed_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Back signed

                    # Back dated
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_verso_dated_1' >".LANG_OPP_FORM_40_2_2."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_dated_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_dated_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='verso_dated_3' name='verso_dated_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['verso_dated_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Back dated

                    # Back numbered
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_verso_numbered_1' >".LANG_OPP_FORM_40_2_3."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_numbered_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_numbered_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='verso_numbered_3' name='verso_numbered_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['verso_numbered_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Back numbered

                    # Back inscribed
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-opp-form-title'>\n";
                            $html_print.="<label for='optionid_verso_inscribed_1' >".LANG_OPP_FORM_40_2_4."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-opp-form-value'>\n";

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_inscribed_1";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,4,$row_opp[$options_select_loan_type['name']],$options_select_loan_type);

                            $options_select_loan_type=array();
                            $options_select_loan_type['html_return']=$options['html_return'];
                            $options_select_loan_type['name']="optionid_verso_inscribed_2";
                            $options_select_loan_type['id']=$options_select_loan_type['name'];
                            $options_select_loan_type['class']['select_dropdown']="select-opp-signature1";
                            $html_print.=select_dropdown($db,3,"",$options_select_loan_type);

                            $html_print.="<textarea id='verso_inscribed_3' name='verso_inscribed_3' class='opp-textarea-like-input' cols='47' rows='1'>".$_SESSION['form_opp']['verso_inscribed_3']."</textarea>";

                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    # END Back inscribed

                $html_print.="</table>\n";

                $html_print.="<div class='div-opp-section-divider'></div>\n";
                $html_print.="<table class='table-opp-1'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='image'>".LANG_OPP_FORM_55."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='file'  name='image[]' multiple='multiple' id='image' accept='image/*' />\n";
                            $html_print.="<p>".LANG_OPP_FORM_55_1."</p>";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td colspan='2'>\n";
                            $html_print.="<p class='p-no-padding-top'>".LANG_OPP_FORM_56."</p>\n";
                            $html_print.="<p>".LANG_OPP_FORM_57."</p>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='copyright_info' id='copyright_info_text'>".LANG_OPP_FORM_58."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<textarea name='copyright_info' id='copyright_info' cols='47' rows='1' class='opp-textarea-like-input'>".$_SESSION['form_opp']['copyright_info']."</textarea>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";
                $html_print.="<div class='div-opp-section-divider'></div>\n";
                $html_print.="<table class='table-opp-1'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='acquired_from' id='acquired_from_text'>".LANG_OPP_FORM_60."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='acquired_from' id='acquired_from' value='".$_SESSION['form_opp']['acquired_from']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='date_of_acquisition' id='date_of_acquisition_text'>".LANG_OPP_FORM_61."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='date_of_acquisition' id='date_of_acquisition' value='".$_SESSION['form_opp']['date_of_acquisition']."' class='input-opp-form-large' placeholder='".LANG_OPP_FORM_21_1."' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='provenance' id='provenance_text'>".LANG_OPP_FORM_59."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<textarea name='provenance' id='provenance' cols='47' rows='1' class='opp-textarea-like-input'>".$_SESSION['form_opp']['provenance']."</textarea>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";
                $html_print.="<div class='div-opp-section-divider'></div>\n";
                # exhibtion history
                $html_print.="<table class='table-opp-1'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td style='vertical-align:top;'>\n";
                            //$html_print.="<label for='exhibition_history' id='exhibition_history_text'>".LANG_OPP_FORM_62."</label>\n";
                            $html_print.="".LANG_OPP_FORM_62."\n";
                            $html_print.="<div class='div-exhibition-publ-list' data-lang='".$_GET['lang']."'>\n";

                            		$exhibitions=array();
									$exhibitions[]=3549;$exhibitions[]=3505;$exhibitions[]=3412;$exhibitions[]=3408;$exhibitions[]=3402;
									$exhibitions[]=3353;$exhibitions[]=3293;$exhibitions[]=3219;$exhibitions[]=2461;$exhibitions[]=2103;
									$exhibitions[]=1871;$exhibitions[]=2855;$exhibitions[]=1488;$exhibitions[]=937;$exhibitions[]=924;
									$exhibitions[]=598;$exhibitions[]=914;$exhibitions[]=580;$exhibitions[]=571;$exhibitions[]=620;
									$exhibitions[]=398;$exhibitions[]=1872;$exhibitions[]=2973;$exhibitions[]=615;$exhibitions[]=1383;
									$exhibitions[]=1873;$exhibitions[]=65;$exhibitions[]=302;$exhibitions[]=651;$exhibitions[]=613;
									$exhibitions[]=716;$exhibitions[]=996;

									$count_exhibitions=count($exhibitions);
									$value_exhibitions=array();
                            		for ($i=0; $i < count($exhibitions); $i++) {
							            $query_where_exhibition=" WHERE exID='".$exhibitions[$i]."' ";
							            $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition);
							            $results_exhibition=$db->query($query_exhibition['query']);
							            $count_exhibition=$db->numrows($results_exhibition);
							            $row_exhibition=$db->mysql_array($results_exhibition,0);
							            $title_exhibition=$row_exhibition['title_original'];
										$options_location=array();
							            $options_location['row']=$row_exhibition;
							            $location_exhibition=location($db,$options_location);
							            $date_exhibition=exhibition_date($db,$row_exhibition);
	                                    $value_exhibitions[$i]['title']=$title_exhibition;
	                                    $value_exhibitions[$i]['titleurl']=$row_exhibition['titleurl'];
	                                    $value_exhibitions[$i]['location']=$location_exhibition;
	                                    $value_exhibitions[$i]['date']=$date_exhibition;
	                                    $string_len=strlen($title_exhibition.", ".$location_exhibition.", ".$date_exhibition);
	                                    if( $string_len>110 ) $exhibition_height="height:36px;";
	                                    else $exhibition_height="height:22px;";
	                                    $value_exhibitions[$i]['style']=$exhibition_height;
                            		}

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_94_T;
                                    $value_exhibitions[$i]['titleurl']="other-exhibitions";
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_94_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_94_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                            		$books=array();
									$books[]=2622;$books[]=2589;$books[]=2515;$books[]=2419;$books[]=2285;$books[]=2115;$books[]=1484;
									$books[]=335;$books[]=265;$books[]=248;$books[]=247;$books[]=55;$books[]=146;$books[]=49;$books[]=85;
									$books[]=155;$books[]=59;

									$count_publications=count($books);
									$value_publications=array();
                            		for ($i=0; $i < count($books); $i++) {

							            $query_where_book=" WHERE id='".$books[$i]."' ";
							            $query_book=QUERIES::query_books($db,$query_where_book);
							            $results_book=$db->query($query_book['query']);
							            $count_book=$db->numrows($results_book);
							            $row_book=$db->mysql_array($results_book,0);
							            $title_book=$row_book['title'];
							            $author_book=$row_book['author'];
							            $publisher_book=$row_book['publisher'];
							            $date_book=$row_book['date_display_year'];
	                                    $value_publications[$i]['title']=$title_book;
	                                    $value_publications[$i]['titleurl']=$row_book['titleurl'];
	                                    $value_publications[$i]['author']=$author_book;
	                                    $value_publications[$i]['publisher']=$publisher_book;
	                                    $value_publications[$i]['date']=$date_book;
	                                    $string_len=strlen($title_book." (".$author_book."). ".$publisher_book.", ".$date_book);
	                                    if( $string_len>110 ) $book_height="height:36px;";
	                                    else $book_height="height:22px;";
	                                    $value_publications[$i]['style']=$book_height;
                            		}

		                            $i++;
		                            $value_publications[$i]['title']=LANG_OPP_FORM_100_5_T;
		                            $value_publications[$i]['titleurl']="other-publications";
		                            $value_publications[$i]['author']=LANG_OPP_FORM_100_5_L;
		                            $value_publications[$i]['publisher']=LANG_OPP_FORM_100_5_L;
		                            $value_publications[$i]['date']=LANG_OPP_FORM_100_5_D;
		                            $value_publications[$i]['style']="height:22px;";

/*
                                    $i=0;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_9_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_9_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_9_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_8_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_8_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_8_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_7_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_7_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_7_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_6_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_6_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_6_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_5_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_5_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_5_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_4_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_4_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_4_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_3_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_3_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_3_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_1_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_1_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_1_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_2_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_2_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_2_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_78_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_78_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_78_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_79_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_79_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_79_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_80_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_80_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_80_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_81_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_81_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_81_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_82_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_82_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_82_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_82_1_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_82_1_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_82_1_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_83_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_83_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_83_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_84_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_84_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_84_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_85_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_85_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_85_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_86_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_86_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_86_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_86_1_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_86_1_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_86_1_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_87_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_87_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_87_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_88_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_88_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_88_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_89_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_89_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_89_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_90_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_90_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_90_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_91_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_91_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_91_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_92_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_92_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_92_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_1_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_1_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_1_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_2_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_2_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_2_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_3_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_3_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_3_D;
                                    $value_exhibitions[$i]['style']="height:36px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_4_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_4_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_4_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_93_5_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_93_5_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_93_5_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $i++;
                                    $value_exhibitions[$i]['title']=LANG_OPP_FORM_94_T;
                                    $value_exhibitions[$i]['location']=LANG_OPP_FORM_94_L;
                                    $value_exhibitions[$i]['date']=LANG_OPP_FORM_94_D;
                                    $value_exhibitions[$i]['style']="height:22px;";

                                    $count_exhibitions=count($value_exhibitions);
*/

                                    $i=0;
                                    $ii=0;
                                    //print_r($_SESSION['form_opp']['exhibition_history']);
                                    //print_r($_SESSION['form_opp']);
                                    if( is_array($_SESSION['form_opp']['exhibition_history']) )
                                    {
                                        foreach ($_SESSION['form_opp']['exhibition_history'] as $key => $value) {
                                            $_SESSION['form_opp']['exhibition_history'][$key]=UTILS::convert_fortitleurl($db,$table,$value,$id_name,$id,$type,"en",2000);
                                        }
                                    }
                                    foreach( $value_exhibitions as $value )
                                    {
                                        $i++;
                                        //$title=$value['title'].$value['location'].$value['date'];
                                        //$title=UTILS::convert_fortitleurl($db,$table,$title,$id_name,$id,$type,"en",2000);
                                        $title=$value['titleurl'];
                                        if( ( $_SESSION['form_opp']['exhibition_history']==$title || @in_array($title, $_SESSION['form_opp']['exhibition_history'] ) && !empty($title) && $title!="" && strlen($title)>0  ) ) $checked=" checked='checked' ";
                                        else $checked="";
                                        if( empty($title) || $title=="" || strlen($title)>0 ) $checked="";
                                        if( $ii==$count_exhibitions ) $class_last="last";
                                        else $class_last="";
                                        //$data_test="data-test='".$_SESSION['form_opp']['exhibition_history'][$ii]."|||".$title."|".strlen($title)."|'";
                                        $data_test="data-test='".$ii."'";
                                        $html_print.="<div style='".$value['style']."' class='".$class_last."'>\n";
                                            $html_print.="<input type='checkbox' name='exhibition_history[]' value='".$title."' ".$checked." ".$data_test." />\n";
                                            $html_print.="<span class='span-opp-exh-publ-list'>";
                                                $html_print.="<span class='span-opp-exh-publ-title'>".$value['title']."</span>";
                                                //$html_print.="<span class='span-opp-exh-publ-location'>".$value['location']."</span>";
                                                if( !empty($value['location']) )
                                                {
                                                	$html_print.="<span class='span-opp-exh-publ-location'>, ".$value['location'].", </span>";
                                                	$html_print.="<span class='span-opp-exh-publ-date'>".$value['date']."</span>";
                                            	}
                                            $html_print.="</span>";
                                        $html_print.="</div>\n";
                                        $ii++;
                                    }
                                    $html_print.="<div class='last'>\n";
                                        $html_print.="<textarea name='exhibition_history_other' id='exhibition_history_other' class='textarea-opp-form-full'>".$_SESSION['form_opp']['exhibition_history_other']."</textarea>\n";
                                    $html_print.="</div>\n";
                            $html_print.="</div>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";
                # END exhibtion history
                $html_print.="<div class='div-opp-section-divider'></div>\n";
                # publication history
                $html_print.="<table class='table-opp-1'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td style='vertical-align:top;' >\n";
                            //$html_print.="<label for='publication_history' id='publications_history_text'>".LANG_OPP_FORM_63."</label>\n";
                            $html_print.="".LANG_OPP_FORM_63."\n";
                            $html_print.="<div class='div-exhibition-publ-list'>\n";

/*
                            $i=0;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_5_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_5_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_5_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_6_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_6_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_6_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_4_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_4_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_4_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_3_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_3_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_3_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_2_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_2_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_2_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_95_1_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_95_1_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_95_1_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_96_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_96_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_96_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_97_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_97_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_97_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_97_1_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_97_1_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_97_1_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_98_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_98_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_98_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_99_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_99_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_99_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_1_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_1_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_1_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_2_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_2_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_2_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_3_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_3_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_3_D;
                            $value_publications[$i]['style']="height:22px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_4_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_4_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_4_D;
                            $value_publications[$i]['style']="height:36px;";

                            $i++;
                            $value_publications[$i]['title']=LANG_OPP_FORM_100_5_T;
                            $value_publications[$i]['location']=LANG_OPP_FORM_100_5_L;
                            $value_publications[$i]['date']=LANG_OPP_FORM_100_5_D;
                            $value_publications[$i]['style']="height:22px;";

                                    $count_publications=count($value_publications);

                                    */
                                    $i=0;
                                    $ii=0;
                                    if( is_array($_SESSION['form_opp']['publication_history']) )
                                    {
                                        foreach ($_SESSION['form_opp']['publication_history'] as $key => $value) {
                                            $_SESSION['form_opp']['publication_history'][$key]=UTILS::convert_fortitleurl($db,$table,$value,$id_name,$id,$type,"en",2000);
                                        }
                                    }
                                    foreach( $value_publications as $value )
                                    {
                                        $i++;
                                        //$title=$value['title'].$value['location'].$value['date'];
                                        //$title=UTILS::convert_fortitleurl($db,$table,$title,$id_name,$id,$type,"en",2000);
                                        $title=$value['titleurl'];
                                        if( ( $_SESSION['form_opp']['publication_history']==$title || @in_array($title, $_SESSION['form_opp']['publication_history']) ) && !empty($title) ) $checked=" checked='checked' ";
                                        else $checked="";
                                        if( $ii==$count_publications ) $class_last="last";
                                        else $class_last="";
                                        //$data_test="data-test='".$_SESSION['form_opp']['publication_history'][$ii]."|||".$title."|'";
                                        $html_print.="<div style='".$value['style']."' class='".$class_last."'>\n";
                                            $html_print.="<input type='checkbox' name='publication_history[]' value='".$title."' ".$checked." ".$data_test." />\n";
                                            $html_print.="<span class='span-opp-exh-publ-list'>";
                                                $html_print.="<span class='span-opp-exh-publ-title'>".$value['title']."</span>";
                                                if( !empty($value['author']) )
                                                {
	                                                $html_print.="<span class='span-opp-exh-publ-location'>";
		                                                $html_print.=" (".$value['author'].").";
		                                                if( !empty($value['publisher']) )
		                                                {
		                                                	$html_print.=" ".$value['publisher'].",";
		                                                }
	                                                $html_print.="</span>";
                                            	}
                                            	$html_print.="<span class='span-opp-exh-publ-date'> ".$value['date']."</span>";
                                            $html_print.="</span>";
                                        $html_print.="</div>\n";
                                        $ii++;
                                    }
                                    $html_print.="<div class='last'>\n";
                                        $html_print.="<textarea name='publication_history_other' id='publication_history_other' class='textarea-opp-form-full'>".$_SESSION['form_opp']['publication_history_other']."</textarea>\n";
                                    $html_print.="</div>\n";
                            $html_print.="</div>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";
                # END publication history
                $html_print.="<div class='div-opp-section-divider'></div>\n";

                $html_print.="<table class='table-opp-2'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td colspan='2'>\n";
                            $html_print.="<p class='p-no-padding-top'>".LANG_OPP_FORM_64."</p>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='first_name' id='first_name_text'>".LANG_OPP_FORM_65."*</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='first_name' id='first_name' value='".$_SESSION['form_opp']['first_name']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='last_name' id='last_name_text'>".LANG_OPP_FORM_66."*</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='last_name' id='last_name' value='".$_SESSION['form_opp']['last_name']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<a name='email'></a>\n";
                            if( $_GET['error_email'] ) $style="style='color:red;'";
                            else $style="";
                            $html_print.="<label for='email' id='email_text' ".$style.">".LANG_OPP_FORM_67."*</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='email' id='email' value='".$_SESSION['form_opp']['email']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td style='vertical-align:top;'>\n";
                            $html_print.="<label for='address' id='address_text'>".LANG_OPP_FORM_68."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<textarea name='address' id='address' class='opp-textarea-like-input'>".$_SESSION['form_opp']['address']."</textarea>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='post_code' id='post_code_text' >".LANG_OPP_FORM_68_1."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='post_code' id='post_code' value='".$_SESSION['form_opp']['post_code']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='country' id='country_text' >".LANG_OPP_FORM_68_2."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='country' id='country' value='".$_SESSION['form_opp']['country']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td>\n";
                            $html_print.="<label for='telephone' id='telephone_text'>".LANG_OPP_FORM_69."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='telephone' id='telephone' value='".$_SESSION['form_opp']['telephone']."' class='input-opp-form-large' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td colspan='2'>\n";
                            $html_print.="<p style='font-weight:700;'>".LANG_OPP_FORM_31."</p>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                    $html_print.="<tr>\n";
                        $html_print.="<td style='vertical-align: top;'>\n";
                            $html_print.="<label for='indicate_credit' id='indicate_credit_text' style='font-weight:700;'>".LANG_OPP_FORM_32."*</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<textarea name='indicate_credit' id='indicate_credit' cols='47' rows='1' class='opp-textarea-like-input'>".$_SESSION['form_opp']['indicate_credit']."</textarea>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";

                $html_print.="</table>\n";
                $html_print.="<div class='div-opp-section-divider'></div>\n";
                $html_print.="<table class='table-opp-1'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td style='vertical-align:top;'>\n";
                            $html_print.="<label for='additional_comments' id='additional_comments_text'>".LANG_OPP_FORM_70."</label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<textarea name='additional_comments' id='additional_comments' class='textarea-opp-form'>".$_SESSION['form_opp']['additional_comments']."</textarea>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td colspan='2'>* ".LANG_OPP_FORM_71."</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-label'>&nbsp;</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<a name='codenumb'></a>\n";
                            if( $_GET['error_code'] ) $style="color:red;";
                            else $style="";
                            $html_print.="<span id='codenumb_error' style='".$style."'>".LANG_CONTACT_CAPCHA."</span>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-label'>&nbsp;</td>\n";
                        $html_print.="<td id='td-image-capcha'>\n";
                            $html_print.="<img src='/includes/image.php' alt='' id='img-capcha-opp' title='Click on image to reload' />\n";
                            $html_print.="<img src='/g/icon_reload.png' alt='' id='img-reload' title='Click to reload image code' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-label'>\n";
                            $html_print.="<label for='codenumb'></label>\n";
                        $html_print.="</td>\n";
                        $html_print.="<td>\n";
                            $html_print.="<input type='text' name='captcha' id='codenumb' class='input-opp-form-large' autocomplete='off' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";

                $html_print.="<table class='table-opp-2'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-table2-checkbox'>\n";
                            if( $_SESSION['form_opp']['send_email'] ) $checked="checked='checked'";
                            else $checked="";
                            $html_print.="<input type='checkbox' name='send_email' id='send_email' value='1' ".$checked." />\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-table2-checkbox-info-part'>\n";
                            $html_print.="<p>".LANG_OPP_FORM_103."</p>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";

                $html_print.="<table class='table-opp-2'>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td class='td-table2-checkbox td-agree-left' style='vertical-align:top;padding-top: 10px;'>\n";
                            $html_print.="<input type='checkbox' name='agree' id='agree' value='1' />\n";
                        $html_print.="</td>\n";
                        $html_print.="<td class='td-table2-checkbox-info-part td-agree-right'>\n";
                            $html_print.="<p>".LANG_OPP_FORM_72."</p>\n";
                            $html_print.="<p>".LANG_OPP_FORM_73."</p>\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                    $html_print.="<tr>\n";
                        $html_print.="<td colspan='2' style='text-align:right;padding:25px 0 25px 0;'>\n";
                            $html_print.="<input type='hidden' name='lang' value='".$_GET['lang']."' />\n";
                            $html_print.="<input type='submit' name='submit' id='submit' class='submit-opp' value='".LANG_OPP_FORM_74."' />\n";
                        $html_print.="</td>\n";
                    $html_print.="</tr>\n";
                $html_print.="</table>\n";
            $html_print.="</form>\n";
            unset($_SESSION['form_opp']);
        $html_print.="</div>\n";
        $html_print.="<div class='clearer'></div>\n";
    $html_print.="</div>\n";
    $html_print.="<script type='text/javascript' >\n";
        $html_print.="(function($) {";
            $html_print.="$(document).ready(";
                $html_print.="function()";
                $html_print.="{";
                    $html_print.="$('a#a_form_opp_read_more, a#a_form_opp_click_here').fancybox({";
                        $html_print.="type: 'inline',";
                        $html_print.="padding: 0,";
                        $html_print.="helpers : {";
                            $html_print.="overlay: {";
                                $html_print.="css : {";
                                    //$html_print.="'background' : 'rgba(58, 42, 45, 2)'";
                                $html_print.="}";
                            $html_print.="}";
                        $html_print.="}";
                    $html_print.="});";
                    if( $_GET['error_email'] || $_GET['error_code'] || $_GET['error'] )
                    {
                        $html_print.="$('#a_form_opp_read_more').trigger('click');";
                        if( $_GET['error_code'] ) $html_print.="alert('".LANG_OPP_FORM_75_3."');";
                    }
                $html_print.="}";
            $html_print.=");";
        $html_print.="})(jQuery);";
    $html_print.="</script>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function div_painting_info($db,$row,$options=array())
{
    //print_r($options);

    $html_print="";

    $html_print.="<div class='".$options['class']['div-tabs-headline']." ".$options['class']['div-tabs-headline-border-bottom']." ".$options['class']['div-tabs-headline-exh']."'>";
        # next previous
        $options_next_links=array();
        $options_next_links=$options;
        unset($options['title']);
        $html_print.=$this->previous_next_links($db,$options_next_links);
        # END next previous
    $html_print.="</div>";

    $html_print.="<div id='div-painting-info' class='".$options['class']['div-painting-info']."'>\n";

        $html_print.="<a name='photo'></a>\n";

        # **************** painting image
        if( empty($options['imageid']) )
        {
            $query_images="SELECT
                COALESCE(
                    r1.itemid1,
                    r2.itemid2
                ) as itemid,
                COALESCE(
                    r1.sort,
                    r2.sort
                ) as sort_rel,
                COALESCE(
                    r1.relationid,
                    r2.relationid
                ) as relationid_rel
                FROM
                    ".TABLE_PAINTING." p
                LEFT JOIN
                    ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
                LEFT JOIN
                    ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
                WHERE p.paintID=".$row['paintID']."
                ORDER BY sort_rel ASC, relationid_rel DESC
                LIMIT 1
            ";
            $results_related_image=$db->query($query_images);
            $row_related_image=$db->mysql_array($results_related_image);
            $imageid=$row_related_image['itemid'];
        }
        else
        {
            $imageid=$options['imageid'];
        }

        $html_print.="<div class='div-painting-info-img ".$options['class']['div-painting-info-img-exh']."'>\n";
            if( empty($imageid) )
            {
                $image="/images/size_xl__imageid_0.jpg";
                $src=$image;
            }
            else
            {
                $image="/images_new/xxlarge/".$imageid.".jpg";
                $src=DATA_PATH_DATADIR.$image;
                $src_size=DATA_PATH_DISPLAY.$image;
            }
            list($width_img, $height_img) = @getimagesize($src_size);
            if( !$options['content_small'] )
            {
                $scale_from=500;
                $scale_to=672;
                if( $width_img>$height_img && $width_img>=$scale_from && $width_img<=$scale_to ) $class_scale="img-scale-width";
                elseif( $height_img>$width_img && $height_img>=$scale_from && $height_img<=$scale_to ) $class_scale="img-scale-height";
                else $class_scale="";
            }

            $html_print.="<img id='img-painting-info' class='".$class_scale."' src='".$src."' alt='' data-width='".$width_img."' data-height='".$height_img."'  />\n";
        $html_print.="</div>\n";
        #end ****************** painting image

        ### info - title, CR nr, year etc.
        $html_print.="<div id='div-painting-info-box'>\n";

            ### image zoomer for cage
            if( $row['paintID']==13796 ||
                $row['paintID']==13806 ||
                $row['paintID']==13807 ||
                $row['paintID']==13808 ||
                $row['paintID']==13809 ||
                $row['paintID']==13810  )
            {
                if( $row['paintID']==13796 ) $dir="cage-1";
                if( $row['paintID']==13806 ) $dir="cage-2";
                if( $row['paintID']==13807 ) $dir="cage-3";
                if( $row['paintID']==13808 ) $dir="cage-4";
                if( $row['paintID']==13809 ) $dir="cage-5";
                if( $row['paintID']==13810 ) $dir="cage-6";
                $html_zoomer="<div class='div-painting-info-button-box'>";
                    $html_zoomer.="<a class='a-painting-info-button a-painting-info-zoomer a-painting-info-zoomer-".$_GET['lang']."' title='Cage zoomer' onclick=\"popup('/".$_GET['lang']."/art/zoomer/cage/".$dir."','800','550')\" >".LANG_ZOOMER_OPEN."</a>";
                $html_zoomer.="</div>\n";
                if( !empty($options['exhibitionid']) ) $class_p_smaller="p-painting-info-smaller-exh";
                else $class_p_smaller="p-painting-info-smaller";
            }
            #end

            ### image zoomer for blau
            if( $row['paintID']==5245 )
            {
                $html_zoomer="<div class='div-painting-info-button-box'>";
                    $html_zoomer.="<a class='a-painting-info-button a-painting-info-zoomer a-painting-info-zoomer-".$_GET['lang']."' title='Blau zoomer' onclick=\"popup('/".$_GET['lang']."/art/zoomer/blau/".$dir."','800','550')\" >".LANG_ZOOMER_OPEN."</a>";
                $html_zoomer.="</div>\n";
                if( !empty($options['exhibitionid']) ) $class_p_smaller="p-painting-info-smaller-exh";
                else $class_p_smaller="p-painting-info-smaller";
            }
            #end

            ### linked microsite icon

            $query_where_microsite=" ";
            $options_query_microsites=array();
            $query_microsite=QUERIES::query_microsites($db,$query_where_microsite,"","",$options_query_microsites);
            $results=$db->query($query_microsite['query']);

            $query_related_microsite="
                SELECT ".$query_microsite['query_columns']."
                    FROM ".TABLE_MICROSITES." m
                LEFT JOIN relations r1 ON r1.typeid1 = 1 AND r1.itemid1='".$row['paintID']."' AND r1.typeid2 = 3 AND r1.itemid2 = m.micrositeid
                LEFT JOIN relations r2 ON r2.typeid2 = 1 AND r2.itemid2='".$row['paintID']."' AND r2.typeid1 = 3 AND r2.itemid1 = m.micrositeid
                WHERE r1.itemid2 IS NOT NULL LIMIT 1 ";

            //print $query_related_microsite;
            $results_related_microsite=$db->query($query_related_microsite);
            $count_related_microsite=$db->numrows($results_related_microsite);
            //print $count_related_microsite;
            if( $count_related_microsite>0 )
            {
                $row_related_microsite=$db->mysql_array($results_related_microsite);
                $row_related_microsite=UTILS::html_decode($row_related_microsite);
                $desc_small=UTILS::row_text_value($db,$row_related_microsite,"desc_small");
                # microite image
                $query_microsite_image="SELECT
                    COALESCE(
                        r1.itemid1,
                        r2.itemid2
                    ) as imageid,
                    COALESCE(
                        r1.sort,
                        r2.sort
                    ) as sort_rel,
                    COALESCE(
                        r1.relationid,
                        r2.relationid
                    ) as relationid_rel
                    FROM
                        ".TABLE_MICROSITES." m
                    LEFT JOIN
                        ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 3 and r1.itemid2 = m.micrositeid
                    LEFT JOIN
                        ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 3 and r2.itemid1 = m.micrositeid
                    WHERE m.micrositeid=".$row_related_microsite['micrositeid']."
                    ORDER BY sort_rel ASC, relationid_rel DESC
                    LIMIT 1
                ";
                $results_microsite_image=$db->query($query_microsite_image);
                $count_microsite_image=$db->numrows($results_microsite_image);
                $row_microsite_image=$db->mysql_array($results_microsite_image);
                $src_microsite_image="/images/size_s__imageid_".$row_microsite_image['imageid'].".jpg";
                # END microite image

                $html_microsite="<div class='div-painting-info-button-box' id='div-painting-info-button-box'>";
                    $html_microsite.="<a href='/".$_GET['lang']."/art/microsites/".$row_related_microsite['titleurl']."' target='_blank' data-microsite-preview='data-microsite-preview' class='a-painting-info-button a-painting-info-see-microsite a-painting-info-see-microsite-".$_GET['lang']."' title='".LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE."' >".LANG_PAINTING_DETAIl_VIEW_SEE_MICROSITE."</a>";
                    # microsite admin edit link
                    $options_admin_edit=array();
                    $options_admin_edit=$options;
                    $options_admin_edit['style']="float:right;";
                    $html_microsite.=UTILS::admin_edit("/admin/microsites/edit/?micrositeid=".$row_related_microsite['micrositeid'],$options_admin_edit);
                    # END microsite admin edit link
                $html_microsite.="</div>\n";

                # microsite preview tool tip box
                if( $count_microsite_image || !empty($desc_small) )
                {
                    $html_microsite.="<div class='div-tooltips-info-microsite'>";
                        $html_microsite.="<div class='div-tooltips-info-content-microsite'>";
                            $html_microsite.="<img src='".$src_microsite_image."' alt='' />\n";
                            $html_microsite.="<p class='p-paint-detail-microsite-prev-title'>".$desc_small."</p>";
                        $html_microsite.="</div>\n";
                        $html_microsite.="<div class='div-tooltips-info-content-microsite-bottom'></div>";
                    $html_microsite.="</div>\n";
                }
                # END microsite preview tool tip box

                if( !empty($options['exhibitionid']) ) $class_p_smaller="p-painting-info-smaller-exh";
                else $class_p_smaller="p-painting-info-smaller";
            }
            # end

            ### Title
            $html_print.="<p class='p-painting-info-title ".$class_p_smaller."'>\n";
                $options_painting_title=array();
                $options_painting_title=$options;
                $options_painting_title['row']=$row;
                $title_painting=UTILS::get_painting_title($db,$options_painting_title);
                # for prints show number in front of title and add quotes around title
                if( $row['artworkID']==16 )
                {
                    $html_print.="<span class='span-painting-info-number-in-title'>".$row['number']."</span> <q>".$title_painting."</q>";
                }
                else
                {
                    $html_print.=$title_painting;
                }
            $html_print.="</p>\n";
            #end

            if( !empty($row['year']) || !empty($row['size']) || !empty($row['number']) )
            {
                $html_print.="<p class='p-painting-info-year-size-etc ".$class_p_smaller."'>";

                    ### Year
                    if( !empty($row['year']) && $row['year']!=="n.a." )
                    {
                        $html_print.="<span class='span-painting-info-year'>";
                            $html_print.=$row['year'];
                        $html_print.="</span>\n";
                    }
                    #end

                    ### Dimension
                    if( !empty($row['size']) )
                    {
                        $html_print.="<span class='span-painting-info-size'>\n";
                            # new stuff
                            if( !empty($row['size_parts']) )
                            {
                                if( $_GET['lang']=="zh" )
                                {
                                    if( $row['size_parts']==2 ) $row['size_parts']="两件";
                                    elseif( $row['size_parts']==3 ) $row['size_parts']="三件";
                                    elseif( $row['size_parts']==4 ) $row['size_parts']="四件";
                                    elseif( $row['size_parts']==5 ) $row['size_parts']="五件";
                                    elseif( $row['size_parts']==6 ) $row['size_parts']="六件";
                                    elseif( $row['size_parts']==7 ) $row['size_parts']="七件";
                                    elseif( $row['size_parts']==8 ) $row['size_parts']="八件";
                                    elseif( $row['size_parts']==9 ) $row['size_parts']="九 件";
                                    elseif( $row['size_parts']==10 ) $row['size_parts']="十件";
                                }
                                $html_print.=$row['size_parts'].LANG_PAINTING_SIZE_PARTS.", ";
                            }
                            if( !empty($row['size_typeid']) )
                            {
                                $size_type=UTILS::painting_size_info($db,$row['size_typeid']);
                                if( !empty($size_type) ) $html_print.=$size_type.": ";
                            }
                            $html_print.=$row['size'];
                        $html_print.="</span>\n";
                    }
                    #end

                    ### CR number
                    if( !empty($row['number']) )
                    {
                        $html_print.="<span class='span-painting-info-crnumber'>\n";
                            $artwork_type=UTILS::get_artwork_info($db,$row['artworkID']);
                            $html_print.=$artwork_type['nr'].": ".$row['number'];
                        $html_print.="</span>\n";
                    }
                    #end

                $html_print.="</p>\n";
            }

            ### Medium
            if( !empty($row['mID']) )
            {
                $html_print.="<p class='p-painting-info-medium ".$class_p_smaller."'>\n";
                    $results_media=$db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mID='".$row['mID']."'");
                    $row_media=$db->mysql_array($results_media);
                    $row_media=UTILS::html_decode($row_media);
                    $media=UTILS::row_text_value2($db,$row_media,"medium");
                    $html_print.=$media;
                $html_print.="</p>\n";
            }
            #end

            ### Admin edit
            $options_admin_edit=array();
            $options_admin_edit=$options;
            $html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$row['paintID'],$options_admin_edit);
            #end

            ### image zoomer
            $html_print.=$html_zoomer;
            #end

            ### see microsite
            $html_print.=$html_microsite;
            #end

        $html_print.="</div>\n";
        #end div-painting-info-box

        if( $row['destroyed'] )
        {
            $html_print.="<p class='p-painting-detail-badge-destroyed'>".LANG_ARTWORK_DESTROYED."</p>\n";
        }

    $html_print.="</div>\n";
    #end div-painting-info

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

/*
function categories($db,$options=array())
{
    $html_print="";

    $query_where_categories="
        LEFT JOIN ".TABLE_ARTWORKS." a ON c.artworkID=a.artworkID
        WHERE c.enable=1 AND c.artworkID='".$options['artworkID']."'
    ";

    # if specific categories in options provided then selcting only these categories
    if( !empty($options['categoryids']) ) $query_where_categories.=" AND ( ";
    $i=0;
    foreach( $options['categoryids'] as $categoryid )
    {
        $i++;
        if( $i>1 ) $or=" OR ";
        else $or="";
        $query_where_categories.=$or." catID='".$categoryid."' ";
    }
    if( !empty($options['categoryids']) ) $query_where_categories.=" ) ";
    # END

    $query_order_categories=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
    $options_query_category=array();
    $options_query_category['columns']=",a.titleurl AS a_titleurl";
    $query_categories=QUERIES::query_category($db,$query_where_categories,$query_order_categories,"","",$options_query_category);
    $results_categories=$db->query($query_categories['query']);
    $count_categories=$db->numrows($results_categories);

	if( !empty($options['title']) )
    {
        $html_print.="<h3 class='h3-section-title'>";
            $html_print.="<a name='".$options['name']."'>".$options['title']."</a>";
        $html_print.="</h3>\n";
    }
	$html_print.="\t<div class='div-section-categories'>\n";
		$i=0;
        $ii=0;
		while( $row_categories=$db->mysql_array($results_categories) )
		{
            $i++;
            $ii++;
            $row_categories=UTILS::html_decode($row_categories);
            $title_category=UTILS::row_text_value2($db,$row_categories,"category");

            if( $ii<=4 ) $class_first_row="div-section-categories-first-row";
            else $class_first_row="";

			if( $i==4 || $ii==$count_categories ) $class_last="div-section-categories-last";
            else $class_last="";

			$html_print.="\t<div class='div-section-category ".$class_first_row." ".$class_last."'>\n";

                # url
                $url="/".$_GET['lang']."/art";
                //if( $row_categories['artworkID']==1 || $row_categories['artworkID']==2 || $row_categories['artworkID']==13 )
                if( $row_categories['artworkID']==1 || $row_categories['artworkID']==2 )
                {
                    $url.="/paintings";
                }
                $url.="/".$row_categories['a_titleurl'];
                if( !empty($row_categories['titleurl']) ) $url.="/".$row_categories['titleurl'];

                # link
			    $html_print.="\t<a href='".$url."' class='a-lit-cat'>\n";
                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=18 AND itemid2='".$row_categories['catID']."' ) OR ( typeid2=17 AND typeid1=18 AND itemid1='".$row_categories['catID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                    if( empty($imageid) )
                    {
                        //$src="/images/size_m__imageid_".$imageid.".jpg";
                        $src=DATA_PATH_DATADIR."/images_new/medium/".$imageid.".jpg";
                    }
                    else
                    {
                        //$src=DATA_PATH_DATADIR."/images_new/medium/".$imageid.".jpg";
                        $src=DATA_PATH_DATADIR."/images_new/medium/".$imageid.".jpg";
                    }

			        $html_print.="\t<img src='".$src."' alt='' />\n";
			        $html_print.="\t<span class='span-lit-cat-title'>".$title_category."</span>\n";
			    $html_print.="\t</a>\n";

                # admin edit link
                $options_admin_edit=array();
                $options_admin_edit=$options;
			    $html_print.=UTILS::admin_edit("/admin/paintings/categories/edit/?catid=".$row_categories['catID'],$options_admin_edit);

			$html_print.="\t</div>";
            if($i==4)
            {
                $html_print.="<div class='clearer'></div>\n";
                $i=0;
            }
		}
	$html_print.="\t</div>\n";
    $html_print.="<div class='clearer'></div>\n";
	//$html_print.="\t<br class='clear' />\n";
	//$html_print.="\t<p><a class='top' href='#top'>".LANG_BACKTOTOP."</a></p>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}
*/


public static function news_title($db,$values)
{
    $values_row_text=array();
    $values_row_text['search']=$values['search'];
    $title=UTILS::row_text_value2($db,$values['row'],"title",$values_row_text);
    $values_search_found=array();
    $values_search_found['search']=$values['search'];
    $values_search_found['text']=$title;
    $values_search_found['a_name']=1;
    $title_search=UTILS::show_search_found_keywords($db,$values_search_found);
    $values_search_found=array();
    $values_search_found['search']=$values['search'];
    $values_search_found['text']=$values['row']['title_original'];
    $values_search_found['a_name']=1;
    $title_original=UTILS::show_search_found_keywords($db,$values_search_found);
    print "\t<span class='".$values['class']['orig']."'>\n";
        if( !empty($values['row']['title_original']) ) print $title_original;
    print "\t</span>\n";
    print "\t<span class='".$values['class']['trans']."'>\n";
        if( !empty($values['row']['title_original']) && $title!=$values['row']['title_original'] ) print $title_search;
    print "\t</span>\n";
}

public static function exhibition_title($db,$options)
{
    $html_print="";

    $options_row_text=array();
    $options_row_text['search']=$options['search'];
    $title=UTILS::row_text_value2($db,$options['row'],"title",$options_row_text);
    $options_search_found=array();
    $options_search_found['search']=$options['search'];
    $options_search_found['text']=$title;
    $options_search_found['a_name']=1;
    $options_search_found['class']['span_keyword_found']="span-keyword-found2";
    $title_search=UTILS::show_search_found_keywords($db,$options_search_found);
    $options_search_found=array();
    $options_search_found['search']=$options['search'];
    $options_search_found['text']=$options['row']['title_original'];
    $options_search_found['a_name']=1;
    $options_search_found['class']['span_keyword_found']="span-keyword-found2";
    $title_original=UTILS::show_search_found_keywords($db,$options_search_found);
    $html_print.="<span class='".$options['class']['orig']."'>\n";
        if( !empty($options['row']['title_original']) ) $html_print.=$title_original;
    $html_print.="</span>\n";
    $html_print.="<span class='".$options['class']['trans']."'>\n";
        if( !empty($options['row']['title_original']) && $title!=$options['row']['title_original'] ) $html_print.=$title_search;
    $html_print.="</span>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}


function ul_breadcrumbles($db,$options)
{
    $html_print="";

    if( !empty($options['items']) )
    {
        if( !empty($options['admin_edit']) ) $html_print.=$options['admin_edit'];
        $html_print.="<div class='breadcrumb ".$options['class']['div']."'>";
            $html_print.="<ul class='".$options['class']['ul']."'>";
                //krsort($options['items']);
                $options['items']=array_values($options['items']);
                $options['items_new']=array();
                $options['items_new'][0]['href']="/".$_GET['lang'];
                $options['items_new'][0]['title']="Gerhard Richter";
                $options['items_new'][0]['inner_html']="Gerhard Richter";
                foreach( $options['items'] as $key => $item )
                {
                    $options['items_new'][$key+1]=$item;
                }
                //print_r($options['items_new']);
                foreach( $options['items_new'] as $item )
                {
                    if( empty($item['href']) ) $class_no_hover="no-link-breadcrumbles";
                    else $class_no_hover="";
                    $html_print.="<li class='".$class_no_hover."'>";
                        if( !empty($item['length']) ) $length=$item['length'];
                        else $length=60;
                        if( strlen($item['inner_html'])>$length )
                        {
                            //print "Before - ".$item['inner_html']."\n";
                            //$item['inner_html']=htmlentities($item['inner_html']);
                            //$item['inner_html']=htmlentities($item['inner_html'], ENT_QUOTES);
                            $item['inner_html'] = html_entity_decode($item['inner_html'],ENT_QUOTES);
                            //print "After - ".$item['inner_html']."\n";
                            $item['inner_html']=str_replace(" – ", " - ", $item['inner_html']);
                            $title_splited=preg_split("/gerhard\srichter.\s/i", $item['inner_html'],null,PREG_SPLIT_NO_EMPTY);
                            $title_inner_html=substr($title_splited[0],0,$length-1)."...";
                            //print "__".substr($title_splited[0],$length-1,$length)."__";
                        }
                        else $title_inner_html=$item['inner_html'];
                        if( !empty($item['href']) )
                        {
                            if( !empty($item['id']) ) $id="id='".$item['id']."'";
                            else $id="";
                            $html_print.="<a href='".$item['href']."' title='".$item['title']."' ".$id.">".$title_inner_html."</a>";
                        }
                        //else $html_print.="<span>".$item['inner_html']."</span>";
                        //else $html_print.=$item['inner_html'];
                        else
                        {
                            if( !empty($item['id']) ) $id="id='".$item['id']."'";
                            else $id="";
                            $html_print.="<a title='".$item['title']."' class='no-link-breadcrumbles' ".$id.">".$title_inner_html."</a>";
                        }
                    $html_print.="</li>";
                }
            $html_print.="</ul>";
        $html_print.="</div>";

        # mobile back button
        if( !empty($options['mobile_button_text']) && !empty($options['mobile_button_url']) )
        {
            $html_print.="<div class='disable div-mobile-back'>";
                $html_print.="<a href='".$options['mobile_button_url']."' class='a-mobile-back'>".$options['mobile_button_text']."</a>";
            $html_print.="</div>";
        }
        # END mobile back button

        # SHARE button
        $url_share=HTTPS_SERVER.$_SERVER['REQUEST_URI'];
        $title_share=$this->title;
        $html_print.="<div class='div-share'>";
            $html_print.="<a id='a-share-button' class='a-share-button close' ></a>";
            $html_print.="<ul id='ul-share-options'>";
                $html_print.="<li class='li-share-this-page'>";
                    $html_print.="<a class='a-share-button a-share-option a-share-this-page'>".LANG_SHARE_THIS_PAGE."</a>";
                $html_print.="</li>";
                $html_print.="<li>";
                    //$href="mailto:?subject=".LANG_SHARE_EMAIL_SUBJECT."&body=".LANG_SHARE_EMAIL_BODY_1."%20%0D%0A%0D%0A".LANG_SHARE_EMAIL_BODY_2."%20%0D%0A%0D%0A".$title_share."%0D%0A%0D%0A".urlencode($url_share)."%0D%0A%0D%0A".LANG_SHARE_EMAIL_BODY_3."%0D%0A%0D%0A";
                    //$href="mailto:?subject=".rawurlencode(LANG_SHARE_EMAIL_SUBJECT)."&amp;body=".rawurlencode(html_entity_decode($title_share))."%0D%0A%0D%0A".rawurlencode($url_share);
                    $href="mailto:?subject=".rawurlencode(LANG_SHARE_EMAIL_SUBJECT)."&amp;body=".rawurlencode($url_share);
                    $html_print.="<a href='".$href."' class='a-share-option a-share-email'>".LANG_SHARE_EMAIL."</a>";
                $html_print.="</li>";
                $html_print.="<li>";
                    $html_print.="<a class='a-share-option a-share-facebook' >Facebook</a>";
                $html_print.="</li>";
                $html_print.="<li>";
                    $html_print.="<a href='//twitter.com/share' class='a-share-option a-share-twitter'>Twitter</a>";
                $html_print.="</li>";
                $html_print.="<li>";
                    $html_print.="<a href=\"javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;//assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());\" class='a-share-option a-share-pinterest'>Pinterest</a>";
                    //$html_print.="<a data-pin-do='buttonBookmark' href='//www.pinterest.com/pin/create/button/' class='a-share-option a-share-pinterest'>Pinterest</a>";
                    //$html_print.="<script async defer src='//assets.pinterest.com/js/pinit.js'></script>";
                $html_print.="</li>";
            $html_print.="</ul>";
        $html_print.="</div>";

        $html_print.="<script type='text/javascript'>";

            # show share dialog
            $html_print.='
            $(".a-share-button, .a-share-option").click(function()
            {
                $("#ul-share-options").toggle();
                $("#a-share-button").toggleClass("close");
            })';

            # facebook share dialog
            $html_print.="
            $('.a-share-facebook').click(function()
            {
                FB.ui({
                  method: 'share',
                  href: '".$url_share."',
                }, function(response){});
            })";

            # twitter share dialog
            $html_print.="
            $('.a-share-twitter').click(function()
            {
                var width  = 575,
                    height = 400,
                    left   = ($(window).width()  - width)  / 2,
                    top    = ($(window).height() - height) / 2,
                    url    = this.href,
                    opts   = 'status=1' +
                             ',width='  + width  +
                             ',height=' + height +
                             ',top='    + top    +
                             ',left='   + left;

                window.open(url, 'twitter', opts);

                return false;
            })";

        $html_print.="</script>";

        # END SHARE button

        $html_print.="<div class='clearer'></div>\n";

        if( $options['return'] || $options['html_return'] ) return $html_print;
        else print $html_print;
    }
}

function div_exhibition($db,$options)
{
    $html_print="";

    $html_print.="<div class='div-exhibition ".$options['class']['div-exhibition-search']." ".$options['class']['div-exhibition-relations']." ".$options['class']['div-exhibition-relations-first']."'>\n";

        $row=UTILS::html_decode($options['row']);
        $results_exhibitions_relations=$db->query("SELECT typeid1 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row['exID']."'  AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row['exID']."' AND typeid1=1 ) LIMIT 1 ");
        $count_exhibitions_relations=$db->numrows($results_exhibitions_relations);

        $results_installations_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row['exID']."'  AND typeid2=5 ) OR ( typeid2=4 AND itemid2='".$row['exID']."' AND typeid1=5 ) LIMIT 1 ");
        $row_installations_relations=$db->mysql_array($results_installations_relations);
        $count_installations_relations=$db->numrows($results_installations_relations);
        $installationid=UTILS::get_relation_id($db,"5",$row_installations_relations);

        if( !empty($row['tour_button_url']) )
        {
            $exh_url=$row['tour_button_url'];
            $exh_title=$row['tour_button_url'];
            $exh_target="target='_blank'";
        }
        else
        {
            if( $row['relations'] )
            {
                $options_exh_url=array();
                $options_exh_url['exhibitionid']=$row['exID'];
                $exh_url=UTILS::get_exhibition_url($db,$options_exh_url);
                $exh_title="";
                $class_no_arrow="";
            }
            else
            {
                $class_no_arrow="a-exhibition-list-no-arrow";
            }
        }

        if( !empty($exh_url) ) $a_href="href='".$exh_url."'";

        $html_print.="<div class='div-exhibition-left-side'>\n";
            # exhibition image
            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
            $row_related_image=$db->mysql_array($results_related_image);
            $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
            if( !empty($imageid) )
            {
                $src=DATA_PATH_DATADIR."/images_new/cache/".$imageid.".png";
                $class="img-exh-cover";
            }
            else
            {
                $src="/g/exhibitions/exh-list-no-exh-cover-image.png";
                $class="";
            }
            $html_print.="<a ".$a_href." title='".$exh_title."' ".$exh_target." class='a-exhibition-list-img' >\n";
                $html_print.="<img src='".$src."' alt='' class='".$class."' />";
            $html_print.="</a>\n";
            # END exhibition image
            $options_admin_edit=array();
            $options_admin_edit=$options;
            $options_admin_edit['class']['a-admin-edit-exhibiton-list']="a-admin-edit-exhibiton-list";
            $html_print.=UTILS::admin_edit("/admin/exhibitions/edit/?exhibitionid=".$row['exID'],$options_admin_edit);
        $html_print.="</div>\n";

        $html_print.="<div class='div-exhibition-right-side ".$options['div-exhibition-right-side-search']."'>\n";
            $html_print.="<a ".$a_href." title='".$exh_title."' ".$exh_target." name='".$row['exID']."' class='a-exhibition-list ".$options['class']['a-exhibition-list-search']." ".$options['class']['a-exhibition-list-paint-detail']." ".$class_no_arrow."' >\n";

                # exhibition title
                $options_exh_title=array();
                $options_exh_title=$options;
                $options_exh_title['class']['orig']="span-exh-list-title-original ".$options['class']['span-title-orig-length'];
                $options_exh_title['class']['trans']="span-exh-list-title-translation ".$options['class']['span-title-trans-length'];
                $options_exh_title['row']=$row;
                $options_exh_title['search']=$options['search'];
                $html_print.=$this->exhibition_title($db,$options_exh_title);
                # END exhibition title

                # exhibition location
                $options_location=array();
                $options_location['row']=$row;
                $options_location['search']=$options['search'];
                $location=location($db,$options_location);
                $html_print.="<span class='span-exh-list-location ".$options['class']['span-location-length']."'>\n";
                    $html_print.=$location;
                $html_print.="</span>\n";
                # END exhibition location

                # END exhibition date
                $date=exhibition_date($db,$row);
                $html_print.="<span class='span-exh-list-date ".$options['class']['span-date-length']."'>\n";
                    $html_print.=$date;
                $html_print.="</span>\n";
                # END exhibition date

            $html_print.="</a>\n";
        $html_print.="</div>\n";

        $html_print.="<div class='clearer'></div>\n";
    $html_print.="</div>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

public function number_list($db,$class,$count_number,$number_list_value="")
{
    $html_print="";
    $html_print.="\t<div id='div-number-list".$count_number."'>\n";
        $html_print.="\t<input type='text' name='number_list[]' value='".$number_list_value."' class='".$class."' />\n";
        if( !empty($count_number) ) $onclick="onclick=\"remove_item_clone('div-number-list".$count_number."','div-number-lists');\"";
        else $onclick="";
        $html_print.="\t<a ".$onclick." title='".LANG_RIGHT_SEARCH_BYNUMBER_LIST_REMOVE_TITLE."' class='a-right-side-number-add' id='a-remove".$count_number."'>&#8211;</a>\n";
        $html_print.="\t<a onclick=\"add_item_clone('div-number-list','div-number-lists');\" title='".LANG_RIGHT_SEARCH_BYNUMBER_LIST_ADD_TITLE."' class='a-right-side-number-remove'>+</a>\n";
    $html_print.="\t</div>\n";

    return $html_print;
}

public function div_search_block($db,$options=array())
{
    $html_print.="";

    $html_print.="<div class='".$options['class']['div-search-block']."'>\n";
        if( !empty($options['h1-title']) )
        {
            $html_print.="<div class='div-content-header-block-right-search-title'>\n";
                $html_print.="<p class='p-content-header-block-right-search-title'>".$options['h1-title']."</p>\n";
            $html_print.="</div>";
        }

        $html_print.="<form method='get' action='".$options['url_form_action']."' id='form-right-search".$options['id_additional']."'>\n";

            $field_ids=array();
            $ii=0;
            $iii=0;
            for ($i=0; $i < count($options['fields']) ; $i++)
            {
                $ii++;
                if( $options['fields'][$i]['type']=="select-year-from-to" )
                {
                    $field_ids[$iii]['name']="year-from";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="year-to";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                }
                elseif( $options['fields'][$i]['id']=="text-date-art" )
                {
                    $field_ids[$iii]['name']="date-day-from";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="date-month-from";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="date-year-from";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="date-day-to";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="date-month-to";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="date-year-to";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                }
                elseif ( $options['fields'][$i]['type']=="text-size-art" )
                {
                    $field_ids[$iii]['name']="size-height-min";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="size-height-max";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="size-width-min";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                    $field_ids[$iii]['name']="size-width-max";
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$field_ids[$iii]['name'].$options['id_additional'];
                    $iii++;
                }
                else
                {
                    $field_ids[$iii]['name']=$options['fields'][$i]['name'];
                    $field_ids[$iii]['type']=$options['fields'][$i]['type'];
                    $field_ids[$iii]['id']=$options['fields'][$i]['id'];
                    $iii++;
                }

//print_r($field_ids);

                # printing hidden area
                if( $ii==3 )
                {
                    $html_print.="<div class='div-content-header-block-right-more-options ".$options['more-options']."'>\n";
                }

                    # text fields
                    if( $options['fields'][$i]['type']=="text" )
                    {
                        # CR number search
                        if( $options['fields'][$i]['id']=="number_art".$options['id_additional'] )
                        {
                            # if comma seperated numbers searched redirect to list search
                            $numbers = preg_split("/[\s,]+/", $_GET['number']);
                            if( count($numbers)>1 )
                            {
                                $_GET['number_list']=array();
                                foreach ($numbers as $value)
                                {
                                    $_GET['number_list'][]=$value;
                                }
                                unset($_GET['number']);
                            }
                            if( !empty($_GET['number_list']) )
                            {
                                foreach( $_GET['number_list'] as $key => $number_list_value )
                                {
                                    if( empty($number_list_value) ) unset($_GET['number_list'][$key]);
                                }
                            }
                            if( empty($_GET['number']) && ( !empty($_GET['number-from']) || !empty($_GET['number-to']) || !empty($_GET['number_list']) ) )
                            {
                                $style_basic="display:none;";
                                $style_advanced="display:block;";
                            }
                            else
                            {
                                $style_basic="display:block;";
                                $style_advanced="display:none";
                            }

                            $html_print.="<div id='div-block-crnumber".$options['id_additional']."' style='".$options['fields'][$i]['style']."'>";

                                # BASIC number search
                                $html_print.="<div id='div-block-crnumber-basic".$options['id_additional']."' style='".$style_basic."'>";
                                    $value_title=$_GET[$options['fields'][$i]['name']];
                                    $title_search=str_replace('"', "&#34;", $value_title);
                                    $title_search=str_replace("'", "&#39;", $title_search);
                                    $title_search=UTILS::strip_slashes_recursive($title_search);
                                    $html_print.="<!--[if lt IE 10]><span class='span-placeholder'>".$options['fields'][$i]['placeholder']."</span>:<![endif]-->";
                                    $html_print.="<input type='text' name='".$options['fields'][$i]['name']."' id='".$options['fields'][$i]['id']."' class='".$options['fields'][$i]['class']."' value='".$title_search."' placeholder='".$options['fields'][$i]['placeholder']."' />\n";
                                $html_print.="</div>";

                                # ADVANCED number search
                                if( !$options['advanced_search'] )
                                {
                                    $html_print.="<div id='div-block-crnumber-advanced' style='".$style_advanced."'>";

                                        # tabs
                                        $html_print.="\t<ul id='ul-right-side-number-tabs' class='ul-search-number-tabs ul-tabs'>\n";
                                            $html_print.="\t<li class='list li-search-number-tab' id='list'>\n";
                                                $html_print.="\t<a href='#tab-list' title='".LANG_RIGHT_SEARCH_BYNUMBER_LIST."' class='list name ".$selected."' >\n";
                                                    $html_print.=LANG_RIGHT_SEARCH_BYNUMBER_LIST;
                                                $html_print.="\t</a>\n";
                                            $html_print.="\t</li>\n";
                                            $html_print.="\t<li class='range li-search-number-tab' id='range'>\n";
                                                if( !empty($_GET['number-from']) || !empty($_GET['number-to']) )
                                                {
                                                    $selected="selected";
                                                }
                                                else $selected="";
                                                $html_print.="\t<a href='#tab-range' title='".LANG_RIGHT_SEARCH_BYNUMBER_RANGE."' class='range name ".$selected." range-".$_GET['lang']."' >\n";
                                                    $html_print.=LANG_RIGHT_SEARCH_BYNUMBER_RANGE;
                                                $html_print.="\t</a>\n";
                                            $html_print.="\t</li>\n";
                                        $html_print.="\t</ul>\n";

                                        $html_print.="<script type='text/javascript'>\n";
                                            $html_print.="$(function() {\n";
                                                $html_print.="$( '#div-block-crnumber-advanced' ).tabs({\n";
                                                    $html_print.="cache: false";
                                                $html_print.="});\n";
                                            $html_print.="});\n";
                                        $html_print.="</script>\n";

                                        #number_list
                                        $html_print.="\t<div id='tab-list' class='div-right-side-number-tabs-info'>\n";
                                            $html_print.="\t<div id='div-number-lists' class='".$this_select." holder list-info'>\n";
                                                if( !empty($_GET['number_list']) )
                                                {
                                                    $count_number="";
                                                    foreach( $_GET['number_list'] as $number_list_value )
                                                    {
                                                        $number_list_search=str_replace('"', "&#34;", $number_list_value);
                                                        $number_list_search=str_replace("'", "&#39;", $number_list_search);
                                                        $number_list_search=UTILS::strip_slashes_recursive($number_list_search);
                                                        $html_print.=$this->number_list($db,$class=$options['fields'][$i]['class'],$count_number,$number_list_search);
                                                        $count_number++;
                                                    }
                                                    $html_print.="\t<script type='text/javascript'>\n";
                                                        $html_print.="clone_number=".$count_number.";";
                                                    $html_print.="\t</script>\n";
                                                }
                                                else $html_print.=$this->number_list($db,$class=$options['fields'][$i]['class']." input-right-side-number-more",$count_number);

                                            $html_print.="\t</div>\n";
                                        $html_print.="\t</div>\n";

                                        #number-from number-to
                                        $html_print.="\t<div id='tab-range' class='div-right-side-number-tabs-info'>\n";
                                            if( !empty($_GET['number-from']) || !empty($_GET['number-to']) )
                                            {
                                                $this_select="this-select";
                                            }
                                            else $this_select="";
                                            $html_print.="\t<div class='".$this_select." holder range-info'>\n";
                                                # number-from
                                                if( !empty($_GET['number-from']) ) $number_from=$_GET['number-from'];
                                                $number_from_search=str_replace('"', "&#34;", $number_from);
                                                $number_from_search=str_replace("'", "&#39;", $number_from_search);
                                                $number_from_search=UTILS::strip_slashes_recursive($number_from_search);
                                                $html_print.="<!--[if lt IE 10]><span class='span-placeholder'>".LANG_DIMENSIONS_FROM."</span>:<![endif]-->";
                                                $html_print.="\t<input id='input-number-from' type='text' class='".$options['fields'][$i]['class']."' name='number-from' placeholder='".LANG_DIMENSIONS_FROM."' value='".$number_from_search."' />\n";
                                                # END number-from
                                                # number-to
                                                $html_print.="\t &#8211; ";
                                                if( !empty($_GET['number-to']) ) $number_to=$_GET['number-to'];
                                                $number_to_search=str_replace('"', "&#34;", $number_to);
                                                $number_to_search=str_replace("'", "&#39;", $number_to_search);
                                                $number_to_search=UTILS::strip_slashes_recursive($number_to_search);
                                                $html_print.="<!--[if lt IE 10]><span class='span-placeholder'>".LANG_DIMENSIONS_TO."</span>:<![endif]-->";
                                                $html_print.="\t<input id='input-number-to' type='text' class='".$options['fields'][$i]['class']."' name='number-to' placeholder='".LANG_DIMENSIONS_TO."' value='".$number_to_search."' />\n";
                                                # END number-to
                                            $html_print.="\t</div>\n";
                                        $html_print.="\t</div>\n";

                                    $html_print.="</div>";

                                    $html_print.="\t<a id='a-right-side-number-search' class='a-right-side-advanced-search' data-type='quick'>".LANG_RIGHT_SEARCH_ADVANCED_SEARCH."</a>\n";

                                    $html_print.="<script type='text/javascript'>";
                                        $html_print.='
                                        $("#a-right-side-number-search").click(function()
                                        {
                                            var $self = $(this);
                                            $self.html( $self.html() == "'.LANG_RIGHT_SEARCH_QUICK_SEARCH.'" ? "'.LANG_RIGHT_SEARCH_ADVANCED_SEARCH.'" : "'.LANG_RIGHT_SEARCH_QUICK_SEARCH.'");
                                            if( $self.attr("data-type")=="quick" )
                                            {
                                                $("#div-block-crnumber-basic").hide();
                                                $("#div-block-crnumber-advanced").show();
                                            }
                                            else
                                            {
                                                $("#div-block-crnumber-basic").show();
                                                $("#div-block-crnumber-advanced").hide();
                                            }
                                            $self.attr("data-type", $self.attr("data-type") == "quick" ? "advanced" : "quick");
                                        })';
                                    $html_print.="</script>";
                                }
                            $html_print.="</div>";

                        }
                        elseif( $options['fields'][$i]['id']=="text-date-art" )
                        {
                            $html_print.="\t<div id='div-block-search-date".$options['id_additional']."' style='".$options['fields'][$i]['style']."'>\n";
                                $html_print.="<p class='p-right-search-field-info'>".LANG_RIGHT_SEARCH_YEARPAINTED1." <strong>".LANG_RIGHT_SEARCH_DATE."</strong></p>\n";
                                $html_print.="\t<span class='span-search-from span-search-from-".$_GET['lang']."'>".LANG_RIGHT_SEARCH_DATE_FROM."\t</span>\n";
                                if( !empty($_GET['date-day-from'])
                                    && $_GET['date-day-from']!="DD"
                                    && $_GET['date-day-from']!="TT"
                                    && $_GET['date-day-from']!="JJ"
                                    && $_GET['date-day-from']!="日日" ) $value=$_GET['date-day-from'];
                                $html_print.="\t<input type='text' name='date-day-from' id='date-day-from".$options['id_additional']."' class='input-day-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-day-from']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_D."' maxlength='2' />\n";
                                if( !empty($_GET['date-month-from'])
                                    && $_GET['date-month-from']!="MM"
                                    && $_GET['date-month-from']!="月月" ) $value=$_GET['date-month-from'];
                                $html_print.="\t<input type='text' name='date-month-from' id='date-month-from".$options['id_additional']."' class='input-month-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-month-from']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_M."' maxlength='2' />\n";
                                if( !empty($_GET['date-year-from'])
                                    && $_GET['date-year-from']!="YYYY"
                                    && $_GET['date-year-from']!="JJJJ"
                                    && $_GET['date-year-from']!="AAAA"
                                    && $_GET['date-year-from']!="年年年年" ) $value=$_GET['date-year-from'];
                                $html_print.="\t<input type='text' name='date-year-from' id='date-year-from".$options['id_additional']."' class='input-year-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-year-from']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y."' maxlength='4' />\n";
                                $html_print.="\t<span class='span-search-to span-search-to-".$_GET['lang']."'>".LANG_RIGHT_SEARCH_DATE_TO."\t</span>\n";
                                if( !empty($_GET['date-day-to'])
                                    && $_GET['date-day-to']!="DD"
                                    && $_GET['date-day-to']!="TT"
                                    && $_GET['date-day-to']!="JJ"
                                    && $_GET['date-day-to']!="日日" ) $value=$_GET['date-day-to'];
                                $html_print.="\t<input type='text' name='date-day-to' id='date-day-to".$options['id_additional']."' class='input-day-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-day-to']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_D."' maxlength='2' />\n";
                                if( !empty($_GET['date-month-to'])
                                    && $_GET['date-month-to']!="MM"
                                    && $_GET['date-month-to']!="月月" ) $value=$_GET['date-month-to'];
                                $html_print.="\t<input type='text' name='date-month-to' id='date-month-to".$options['id_additional']."' class='input-month-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-month-to']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_M."' maxlength='2' />\n";
                                if( !empty($_GET['date-year-to'])
                                    && $_GET['date-year-to']!="YYYY"
                                    && $_GET['date-year-to']!="JJJJ"
                                    && $_GET['date-year-to']!="AAAA"
                                    && $_GET['date-year-to']!="年年年年" ) $value=$_GET['date-year-to'];
                                $html_print.="\t<input type='text' name='date-year-to' id='date-year-to".$options['id_additional']."' class='input-year-".$_GET['lang']." ".$options['fields'][$i]['class']."' value='".$_GET['date-year-to']."' placeholder='".LANG_RIGHT_SEARCH_DATE_EXAMPLE_Y."' maxlength='4' />\n";
                            $html_print.="\t</div>\n";
                        }
                        else
                        {
                            $value_title=$_GET[$options['fields'][$i]['name']];
                            $title_search=str_replace('"', "&#34;", $value_title);
                            $title_search=str_replace("'", "&#39;", $title_search);
                            $title_search=UTILS::strip_slashes_recursive($title_search);
                            $html_print.="<!--[if lt IE 10]><span class='span-placeholder'>".$options['fields'][$i]['placeholder']."</span>:<![endif]-->";
                            $html_print.="<input type='text' name='".$options['fields'][$i]['name']."' id='".$options['fields'][$i]['id']."' class='".$options['fields'][$i]['class']."' value=\"".$title_search."\" placeholder='".$options['fields'][$i]['placeholder']."' />\n";
                        }
                    }
                    # END text fields

                    # select fields
                    elseif ( $options['fields'][$i]['type']=="select" )
                    {
                        # art artworkid
                        if( $options['fields'][$i]['id']=="artworkid_art".$options['id_additional'] )
                        {
                            $results=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE enable=1 AND artworkID!=1 AND artworkID!=2 ORDER BY sort ASC");

                            # on change

                                $onchange_number="change_number($('#".$options['fields'][$i]['id']."'));";
                                $onchange_opp_date_search="if( $('#".$options['fields'][$i]['id']."').val()==5 || $('#".$options['fields'][$i]['id']."').val()==6 || $('#".$options['fields'][$i]['id']."').val()==8 )
                                {
                                    $('#div-block-search-year".$options['id_additional']."').hide();
                                    $('#div-block-search-date".$options['id_additional']."').show();
                                    $('#div-block-crnumber".$options['id_additional']."').hide();
                                }
                                else
                                {
                                    $('#div-block-search-year".$options['id_additional']."').show();
                                    $('#div-block-search-date".$options['id_additional']."').hide();
                                    $('#div-block-crnumber".$options['id_additional']."').show();
                                };";
                                $onchange_colorid="if( $('#".$options['fields'][$i]['id']."').val()==6 )
                                {
                                    $('#colorid_art".$options['id_additional']."SelectBoxItContainer').hide();
                                }
                                else
                                {
                                    $('#colorid_art".$options['id_additional']."SelectBoxItContainer').show();
                                };";

                                #function used to change CR number text at search form
                                $html_print.="<script type='text/javascript'>\n";
                                    $html_print.="function change_number(e)\n";
                                    $html_print.="{\n";
                                        $html_print.="var text='".LANG_RIGHT_SEARCH_BYNUMBER."';\n";
                                        $html_print.="if( e.val()=='paintings' ) text=\"".LANG_RIGHT_SEARCH_BYPAINTINGS."\";\n";
                                        $html_print.="else if( e.val()==3 ) text=\"".LANG_RIGHT_SEARCH_BYATLAS."\";\n";
                                        $html_print.="else if( e.val()==4 ) text=\"".LANG_RIGHT_SEARCH_BYEDITION."\";\n";
                                        $html_print.="else if( e.val()==7 ) text=\"".LANG_RIGHT_SEARCH_BYDRAWINGS."\";\n";
                                        $html_print.="$('#number_art".$options['id_additional']."').attr('placeholder', text);\n";
                                    $html_print.="}\n";
                                    $html_print.="$( document ).ready(function() {";
                                        $html_print.=$onchange_number;
                                        $html_print.=$onchange_opp_date_search;
                                        $html_print.=$onchange_colorid;
                                    $html_print.="});";
                                $html_print.="</script>\n";
                                #END function

                                $onchange_artworkid=$onchange_number.$onchange_opp_date_search.$onchange_colorid;

                            # END on change

                            $html_print.="<select name='".$options['fields'][$i]['name']."' id='".$options['fields'][$i]['id']."' onchange=\"".$onchange_artworkid."\" class='select-field select-large' style='width:300px;' >\n";
                                $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                                if( $_GET['artworkid']=="paintings" || strtolower($options['artworkid_search'])=="paintings" ) $selected="selected='selected'";
                                else $selected="";
                                $html_print.="<option ".$selected." value='paintings'>".LANG_LEFT_SIDE_PAINTINGS."</option>\n";
                                while( $row=$db->mysql_array($results,0) )
                                {
                                    $row=UTILS::html_decode($row);
                                    $artwork=UTILS::row_text_value2($db,$row,"artwork");
                                    if( $_GET['artworkid']==$row['artworkID'] || $options['artworkid_search']==$row['artworkID'] ) $selected="selected='selected'";
                                    else $selected="";
                                    $option="<option ".$selected." value='".$row['artworkID']."'>".$artwork."</option>";
                                    //if( $row['artworkID']!=4 )
                                    //{
                                        $html_print.=$option;
                                    //}
                                }
                                if($_GET['artworkid']=="other") $selected="selected='selected'";
                                else $selected="";
                            $html_print.="</select>\n";
                        }

                        # art colorid
                        if( $options['fields'][$i]['id']=="colorid_art".$options['id_additional'] )
                        {
                            $options_colors=array();
                            $options_colors=$options;
                            $options_colors['name']=$options['fields'][$i]['name'];
                            $options_colors['id']=$options['fields'][$i]['id'];
                            $options_colors['selectedid']=$_GET['colorid'];
                            $options_colors['class']['select-field']="select-field";
                            $options_colors['class']['select-large']="select-large";
                            $html_print.=$this->select_colors($db,$options_colors);
                        }

                        # book category
                        if( $options['fields'][$i]['id']=="books_catid_book".$options['id_additional'] )
                        {
                            $options_select_book_cat=array();
                            $options_select_book_cat=$options;
                            $options_select_book_cat['select_books_catid']="select_books_catid";
                            $options_select_book_cat['class']['select-field']="select-field";
                            $options_select_book_cat['class']['select-large']="select-large";
                            $options_select_book_cat['where']=" AND enable=1 ";
                            $options_select_book_cat['name']=$options['fields'][$i]['name'];
                            $options_select_book_cat['id']=$options['fields'][$i]['id'];
                            $options_select_book_cat['sub']=1;
                            $html_print.=select_books_categories($db,$_GET['books_catid'],$options_select_book_cat);
                        }

                        # book language
                        if( $options['fields'][$i]['id']=="languageid_book".$options['id_additional'] || $options['fields'][$i]['id']=="languageid_articles".$options['id_additional'] )
                        {
                            $options_select_book_lang=array();
                            $options_select_book_lang=$options;
                            $options_select_book_lang['class']['select_books_languageid']="select_books_languageid";
                            $options_select_book_lang['class']['select-field']="select-field";
                            $options_select_book_lang['class']['select-large']="select-large";
                            $options_select_book_lang['select_title']=LANG_RIGHT_SEARCH_LIT_LANGUAGE."...";
                            $options_select_book_lang['where']=" WHERE languageid!=11  ";
                            $options_select_book_lang['name']=$options['fields'][$i]['name'];
                            $options_select_book_lang['id']=$options['fields'][$i]['id'];
                            $html_print.=select_books_languages($db,$_GET['languageid'],$options_select_book_lang);
                        }

                        # article category
                        if( $options['fields'][$i]['id']=="articles_catid_articles".$options['id_additional'] )
                        {
                            $options_select_book_cat=array();
                            $options_select_book_cat=$options;
                            $options_select_book_cat['select_books_catid']="select_books_catid";
                            $options_select_book_cat['class']['select-field']="select-field";
                            $options_select_book_cat['class']['select-large']="select-large";
                            $options_select_book_cat['where']=" AND enable=1 ";
                            $options_select_book_cat['name']=$options['fields'][$i]['name'];
                            $options_select_book_cat['id']=$options['fields'][$i]['id'];
                            $options_select_book_cat['selectedid']=$_GET['articles_catid'];
                            $html_print.=$this->select_articles_categories($db,$options_select_book_cat);
                        }

                    }
                    # END select feilds

                    elseif ( $options['fields'][$i]['type']=="select-year-from-to" )
                    {
                        $html_print.="<div id='div-block-search-year".$options['id_additional']."'>";
                            #year from
                            $html_print.="<p class='p-right-search-field-info'>";
                                //$html_print.="<label for='year-from'>";
                                    $html_print.=LANG_RIGHT_SEARCH_YEARPAINTED1;
                                    $html_print.=" <strong>".LANG_RIGHT_SEARCH_YEARPAINTED2."</strong>";
                                //$html_print.="</label>";
                            $html_print.="</p>\n";
                            $html_print.="<select name='year-from' id='year-from".$options['id_additional']."' class='select-field select-small select-year-from'>\n";
                                $html_print.="<option value=''>".LANG_SELECT_FROM."</option>\n";
                                if($_GET['year-from']=='1949')
                                {
                                    $selected=" selected='selected' ";
                                }
                                else $selected="";
                                for($i_year=1962;$i_year<=(date("Y")+1);$i_year++)
                                {
                                    if($_GET['year-from']==$i_year) $selected="selected='selected'"; else $selected="";
                                    $html_print.="<option $selected value='".$i_year."'>".$i_year."</option>\n";
                                }
                            $html_print.="</select>";
                            #year to
                            $html_print.="<select name='year-to' id='year-to".$options['id_additional']."' class='select-field select-small'>";
                                $html_print.="<option value=''>".LANG_SELECT_TO."</option>";
                                if($_GET['year-to']=='1949')
                                {
                                    $selected=" selected='selected' ";
                                }
                                else $selected="";
                                for($i_year=1962;$i_year<=(date("Y")+1);$i_year++)
                                {
                                    if($_GET['year-to']==$i_year) $selected="selected='selected'"; else $selected="";
                                    $html_print.="<option $selected value='".$i_year."'>".$i_year."</option>\n";
                                }
                            $html_print.="</select>\n";
                        $html_print.="</div>";
                    }

                    elseif ( $options['fields'][$i]['type']=="text-size-art" )
                    {
                        $html_print.="<p class='p-right-search-field-info'>";
                            //$html_print.="<label for='year-from'>";
                                $html_print.=LANG_RIGHT_SEARCH_YEARPAINTED1." <strong>".LANG_RIGHT_SEARCH_BYSIZE."</strong> (cm)";
                            //$html_print.="</label>";
                        $html_print.="</p>\n";

                        # size height min max
                        $html_print.="<p>\n";
                            $html_print.="<strong>";
                                $html_print.="<abbr class='abbr-size' title='".LANG_RIGHT_SEARCH_BYSIZEH1."'>".LANG_RIGHT_SEARCH_BYSIZEH."</abbr>";
                            $html_print.="</strong>";
                            $html_print.="<input id='size-height-min".$options['id_additional']."' type='text' class='input-text-field input-text-small' name='size-height-min' placeholder='".LANG_DIMENSIONS_FROM."' value='".$_GET['size-height-min']."' />\n";
                            $html_print.=" <span class='span-dash'>&#8211;</span> ";
                            $html_print.="\t<input id='size-height-max".$options['id_additional']."' type='text' class='input-text-field input-text-small' name='size-height-max' placeholder='".LANG_DIMENSIONS_TO."' value='".$_GET['size-height-max']."' />\n";
                        $html_print.="</p>\n";

                        # size width min max
                        $html_print.="<p>\n";
                            $html_print.="<strong>";
                                $html_print.="<abbr class='abbr-size' title='".LANG_RIGHT_SEARCH_BYSIZEW1."'>".LANG_RIGHT_SEARCH_BYSIZEW."</abbr>";
                            $html_print.="</strong>";
                            $html_print.="<input id='size-width-min".$options['id_additional']."' type='text' class='input-text-field input-text-small' name='size-width-min' placeholder='".LANG_DIMENSIONS_FROM."' value='".$_GET['size-width-min']."' />\n";
                            $html_print.=" <span class='span-dash'>&#8211;</span> ";
                            $html_print.="\t<input id='size-width-max".$options['id_additional']."' type='text' class='input-text-field input-text-small' name='size-width-max' placeholder='".LANG_DIMENSIONS_TO."' value='".$_GET['size-width-max']."' />\n";
                        $html_print.="</p>\n";
                    }

                    # select fields
                    elseif ( $options['fields'][$i]['type']=="hidden" )
                    {
                        //print_r($options['fields'][$i]);
                        //print_r($field_ids[($iii-1)]);
                        $html_print.="<input id='".$options['fields'][$i]['id'].$options['id_additional']."' name='".$options['fields'][$i]['name']."' type='hidden' value='".$options['fields'][$i]['value']."' />\n";
                        //unset($field_ids[$i]);
                        unset($field_ids[($iii-1)]);
                    }



                /*
                # printing hidden area END
                if(  $ii>2 && $ii==count($options['fields']) && !empty($options['more-options']) )
                {
                    //$html_print.="</div>\n";
                    //print $ii."-".count($options['fields']);
                }
                */
            }

            # printing hidden area END
            if( count($options['fields'])>3 ) $html_print.="</div>\n";

            # submit & reset
            $reset_function_name="reset_search_form".str_replace("-", "_", $options['id_additional']);

            $html_print.="<input id='input-right-search-reset".$options['id_additional']."' type='button' value='".LANG_RIGHT_SEARCH_SEARCHRESET."' onclick=\"".$reset_function_name."();\" />\n";
            $html_print.="<input id='input-right-search-submit".$options['id_additional']."' type='submit' value='".LANG_RIGHT_SEARCH_SEARCHBUTTON."' />\n";
            $html_print_script="<script type='text/javascript'>";

/*
                $html_print_script.="$('#form-right-search".$options['id_additional']."').submit(function() {";

                        $html_print_script.="console.debug($('#year-from-mobile').val().length+'-1here');";
                        $html_print_script.="console.debug($('#year-to-mobile').val().length+'-2here');";
                        $html_print_script.="return false;";

                $html_print_script.="});";
*/
//print_r($field_ids);
                $html_print_script.="$('#form-right-search".$options['id_additional']."').submit(function() {";
                    $script_if="";
                    for ($i=0; $i < count($field_ids); $i++) {
                        if( $i!=0 ) $script_if.=" && ";
                        $script_if.=" !$('#".$field_ids[$i]['id']."').val().length ";
                        //print $field_ids[$i]['id']."<br />";
                        //print $field_ids[$i]['name']."<br />";
                        //print_r($field_ids[$i]);print "<br />";
                    }
                    $html_print_script.="if( ".$script_if." )";
                    $html_print_script.="{";
                        $html_print_script.="return false;";
                        //$html_print_script.="console.debug('do not submit-'+\"".$script_if."\");return false;";
                    $html_print_script.="}";
                    //$html_print_script.="else{console.debug('submit-'+\"".$script_if."\");return false;}";
                $html_print_script.="});";

                //print_r($options['fields']);

                $html_print_script.="
                    function ".$reset_function_name."()\n
                    {\n
                    ";
                        $select_field=0;
                        for ($i=0; $i < count($field_ids); $i++) {
                            if( preg_match("/select/", $field_ids[$i]['type']) )
                            {
                                //print_r($field_ids[$i]);
                                $select_field=1;
                                $html_print_script.="$('#".$field_ids[$i]['id']."').prop('selectedIndex',0);\n";
                                $html_print_script.="$(function() {\n";
                                    $html_print_script.="var selectBox = $('select#".$field_ids[$i]['id']."').data(\"selectBox-selectBoxIt\");\n";
                                    $html_print_script.="if( selectBox ) { selectBox.refresh(); }\n";
                                $html_print_script.="});\n";
                            }
                            else
                            {
                                //print_r($field_ids[$i]);
                                //print "-".$field_ids[$i]['id']."-<br />\n";
                                $html_print_script.=" $(\"#".$field_ids[$i]['id']."\").val('');\n";
                                if( $field_ids[$i]['name']=="number_art" && !$options['advanced_search'] )
                                {
                                    $html_print_script.=" $(\"#input-number-from\").val('');\n";
                                    $html_print_script.=" $(\"#input-number-to\").val('');\n";
                                    $html_print_script.=" $(\".input-right-side-number-more\").val('');\n";
                                    $html_print_script.=" $(\".div-number-list-added\").remove();\n";
                                }
                            }
                        }

                        if( $select_field && !empty($options['id_additional']) )
                        {
                            $html_print_script.="
                                var searchInputsection = $('#form-right-search-mobile input[type=\"text\"]');
                                var searchInputsection_width = $(window).width() - 41 - (2 * (parseInt(searchInputsection.css('marginLeft'), 0) + parseInt(searchInputsection.css('paddingLeft'),0) + parseInt(searchInputsection.css('border-left-width'),0) ));
                                var searchButtonssection_width = ( searchInputsection_width/2 );

                                // section from select small - two in one line
                                $('#form-right-search-mobile .select-field').selectBoxIt({ autoWidth: false });
                                var searchSelectsectionsmall = $('#form-right-search-mobile .select-small, #year-from-mobileSelectBoxItOptions, #year-to-mobileSelectBoxItOptions');
                                searchSelectsectionsmall.width( (searchButtonssection_width-21) );

                                // section from select
                                var searchSelectsection = $('#form-right-search-mobile .select-large, #artworkid_art-mobileSelectBoxItOptions, #colorid_art-mobileSelectBoxItOptions, #books_catid_book-mobileSelectBoxItOptions, #languageid_book-mobileSelectBoxItOptions, #articles_catid_articles-mobileSelectBoxItOptions, #languageid_articles-mobileSelectBoxItOptions');
                                searchSelectsection.width( (searchInputsection_width) );";
                        }

                    $html_print_script.="}\n";


            $html_print_script.="</script>";

            # show more options
            if( count($options['fields'])>2 && !empty($options['more-options']) ) $html_print.="<a title='' class='a-right-search-show-more-options'>".LANG_LITERATURE_SHOW_MORE_OPTIONS."</a>";

            # print hidden form inputs
            if( !empty($options['hidden']) )
            {
                foreach ($options['hidden'] as $value) {
                    $html_print.="<input type='hidden' name='".$value['name']."' value='".$value['value']."' />";
                }
            }

        $html_print.="</form>\n";

    $html_print.="</div>\n";

    //if( $options['html_return'] ) return $html_print;
    //else print $html_print;

    if( $options['html_return_reset'] )
    {
        $return=array();

        $return['html_print']=$html_print;
        $return['html_print_script']=$html_print_script;

        return $return;
    }
    elseif( $options['html_return'] )
    {
        $html_print.=$html_print_script;

        return $html_print;
    }
    else print $html_print;

}

    # SECTION SEARCHES

    function search_form_art($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        $options_search_block['advanced_search']=$options['advanced_search'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/art/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $options_search_block['fields'][0]['name']="referer";
        $options_search_block['fields'][0]['value']="search-art";
        $options_search_block['fields'][0]['type']="select";
        $options_search_block['fields'][0]['name']="artworkid";
        //$options_search_block['fields'][0]['id']="artworkid_art";
        $options_search_block['fields'][0]['id']="artworkid_art".$options['id_additional'];
        $options_search_block['fields'][0]['class']="input-text-field";
        if( !empty($options['artwork_1']['row']['artworkID']) ) $options_search_block['artworkid_search']=$options['artwork_1']['row']['artworkID'];

        $options_search_block['fields'][1]['type']="text";
        $options_search_block['fields'][1]['name']="title";
        $options_search_block['fields'][1]['id']="title_art".$options['id_additional'];
        $options_search_block['fields'][1]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
        $options_search_block['fields'][1]['class']="input-text-field";

        if( $_GET['artworkid']=='paintings' ) $title_number=LANG_RIGHT_SEARCH_BYPAINTINGS;
        else if( $_GET['artworkid']==3 ) $title_number=LANG_RIGHT_SEARCH_BYATLAS;
        else if( $_GET['artworkid']==4 ) $title_number=LANG_RIGHT_SEARCH_BYEDITION;
        else if( $_GET['artworkid']==7 ) $title_number=LANG_RIGHT_SEARCH_BYDRAWINGS;
        else $title_number=LANG_RIGHT_SEARCH_BYNUMBER;
        $options_search_block['fields'][2]['type']="text";
        $options_search_block['fields'][2]['name']="number";
        $options_search_block['fields'][2]['id']="number_art".$options['id_additional'];
        $options_search_block['fields'][2]['placeholder']=$title_number;
        $options_search_block['fields'][2]['class']="input-text-field";
        if( $_GET['artworkid']==6 ) $options_search_block['fields'][2]['style']="display: none;";
        else $options_search_block['fields'][2]['style']="display: block;";

        $options_search_block['fields'][3]['type']="text";
        $options_search_block['fields'][3]['name']="location";
        $options_search_block['fields'][3]['id']="location_art".$options['id_additional'];
        $options_search_block['fields'][3]['placeholder']=LANG_RIGHT_SEARCH_EXH_LOCATION;
        $options_search_block['fields'][3]['class']="input-text-field";

        $options_search_block['fields'][4]['type']="select-year-from-to";

        $options_search_block['fields'][5]['type']="text";
        $options_search_block['fields'][5]['id']="text-date-art";
        $options_search_block['fields'][5]['class']="input-text-field";
        if( $_GET['artworkid']==6 ) $options_search_block['fields'][5]['style']="display: block;";
        else $options_search_block['fields'][5]['style']="display: none;";

        $options_search_block['fields'][6]['type']="text-size-art";

        $options_search_block['fields'][7]['type']="select";
        $options_search_block['fields'][7]['name']="colorid";
        $options_search_block['fields'][7]['id']="colorid_art".$options['id_additional'];
        $options_search_block['fields'][7]['class']="input-text-field";

        $options_search_block['fields'][8]['type']="hidden";
        $options_search_block['fields'][8]['name']="referer";
        $options_search_block['fields'][8]['id']="referer".$options['id_additional'];
        $options_search_block['fields'][8]['class']="";
        $options_search_block['fields'][8]['value']="search-art";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    function search_form_quotes($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/quotes/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $options_search_block['fields'][0]['type']="text";
        $options_search_block['fields'][0]['name']="keyword";
        $options_search_block['fields'][0]['id']="keyword_video".$options['id_additional'];
        $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
        $options_search_block['fields'][0]['class']="input-text-field";

        $options_search_block['fields'][1]['type']="select-year-from-to";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    function search_form_exhibitions($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        $options_search_block['advanced_search']=$options['advanced_search'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/exhibitions/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $options_search_block['fields'][0]['type']="text";
        $options_search_block['fields'][0]['name']="title";
        $options_search_block['fields'][0]['id']="title_exhibition".$options['id_additional'];
        $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
        $options_search_block['fields'][0]['class']="input-text-field";

        $options_search_block['fields'][1]['type']="text";
        $options_search_block['fields'][1]['name']="location";
        $options_search_block['fields'][1]['id']="location_exhibition".$options['id_additional'];
        $options_search_block['fields'][1]['placeholder']=LANG_RIGHT_SEARCH_EXH_LOCATION;
        $options_search_block['fields'][1]['class']="input-text-field";

        $options_search_block['fields'][2]['type']="text";
        $options_search_block['fields'][2]['name']="keyword";
        $options_search_block['fields'][2]['id']="keyword_exhibition".$options['id_additional'];
        $options_search_block['fields'][2]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
        $options_search_block['fields'][2]['class']="input-text-field";

        $options_search_block['fields'][3]['type']="select-year-from-to";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    function search_form_literature($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        $options_search_block['advanced_search']=$options['advanced_search'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/literature/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $i=0;
        $options_search_block['fields'][$i]['type']="text";
        $options_search_block['fields'][$i]['name']="author";
        $options_search_block['fields'][$i]['id']="author_book".$options['id_additional'];
        $options_search_block['fields'][$i]['placeholder']=LANG_RIGHT_SEARCH_LIT_AUTHOR;
        $options_search_block['fields'][$i]['class']="input-text-field";

        $i++;
        $options_search_block['fields'][$i]['type']="text";
        $options_search_block['fields'][$i]['name']="title";
        $options_search_block['fields'][$i]['id']="title_book".$options['id_additional'];
        $options_search_block['fields'][$i]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
        $options_search_block['fields'][$i]['class']="input-text-field";

        $i++;
        $options_search_block['fields'][$i]['type']="text";
        $options_search_block['fields'][$i]['name']="keyword";
        $options_search_block['fields'][$i]['id']="keyword_book".$options['id_additional'];
        $options_search_block['fields'][$i]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
        $options_search_block['fields'][$i]['class']="input-text-field";

        $i++;
        $options_search_block['fields'][$i]['type']="text";
        $options_search_block['fields'][$i]['name']="isbn";
        $options_search_block['fields'][$i]['id']="isbn_book".$options['id_additional'];
        $options_search_block['fields'][$i]['placeholder']=LANG_LITERATURE_ISBN;
        $options_search_block['fields'][$i]['class']="input-text-field";

        $i++;
        $options_search_block['fields'][$i]['type']="select";
        $options_search_block['fields'][$i]['name']="books_catid";
        $options_search_block['fields'][$i]['id']="books_catid_book".$options['id_additional'];
        $options_search_block['fields'][$i]['class']="input-text-field";

        $i++;
        $options_search_block['fields'][$i]['type']="select";
        $options_search_block['fields'][$i]['name']="languageid";
        $options_search_block['fields'][$i]['id']="languageid_book".$options['id_additional'];
        $options_search_block['fields'][$i]['class']="input-text-field";

        $options_search_block['fields'][$i]['type']="select-year-from-to";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    function search_form_links($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        $options_search_block['advanced_search']=$options['advanced_search'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/links/articles/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $options_search_block['fields'][0]['type']="text";
        $options_search_block['fields'][0]['name']="author";
        $options_search_block['fields'][0]['id']="author_articles".$options['id_additional'];
        $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_AUTHOR;
        $options_search_block['fields'][0]['class']="input-text-field";

        $options_search_block['fields'][1]['type']="text";
        $options_search_block['fields'][1]['name']="title";
        $options_search_block['fields'][1]['id']="title_articles".$options['id_additional'];
        $options_search_block['fields'][1]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
        $options_search_block['fields'][1]['class']="input-text-field";

        $options_search_block['fields'][2]['type']="text";
        $options_search_block['fields'][2]['name']="keyword";
        $options_search_block['fields'][2]['id']="keyword_articles".$options['id_additional'];
        $options_search_block['fields'][2]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
        $options_search_block['fields'][2]['class']="input-text-field";

        $options_search_block['fields'][3]['type']="select";
        $options_search_block['fields'][3]['name']="articles_catid";
        $options_search_block['fields'][3]['id']="articles_catid_articles".$options['id_additional'];
        $options_search_block['fields'][3]['class']="input-text-field";

        $options_search_block['fields'][4]['type']="select";
        $options_search_block['fields'][4]['name']="languageid";
        $options_search_block['fields'][4]['id']="languageid_articles".$options['id_additional'];
        $options_search_block['fields'][4]['class']="input-text-field";

        $options_search_block['fields'][5]['type']="select-year-from-to";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    function search_form_videos($db,$options=array())
    {
        $html_print="";

        $options_search_block=array();
        $options_search_block=$options['options_head'];
        $options_search_block['html']=$options['html'];
        $options_search_block['id_additional']=$options['id_additional'];
        $options_search_block['html_return_reset']=$options['html_return_reset'];
        if( !empty($options['class']['div-search-block']) )
        {
            $options_search_block['class']['div-search-block']=$options['class']['div-search-block'];
        }
        else
        {
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
        }
        if( isset($options['h1-title']) ) $options_search_block['h1-title']=$options['h1-title'];
        else $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
        if( isset($options['more-options']) ) $options_search_block['more-options']=$options['more-options'];
        else $options_search_block['more-options']="hidden";
        $options_search_block['url_form_action']="/".$_GET['lang']."/videos/search/";
        if( isset($options['id']['form-right-search']) ) $options_search_block['id']['form-right-search']=$options['id']['form-right-search'];

        $options_search_block['fields'][0]['type']="text";
        $options_search_block['fields'][0]['name']="keyword";
        $options_search_block['fields'][0]['id']="keyword_video".$options['id_additional'];
        $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
        $options_search_block['fields'][0]['class']="input-text-field";

        $html_print=$this->div_search_block($db,$options_search_block);

        //if( $options_search_block['html_return'] ) return $html_print;
        //else print $html_print;

        if( $options['html_return_reset'] )
        {
            $return=array();

            $return['html_print']=$html_print['html_print'];
            $return['html_print_script']=$html_print['html_print_script'];

            return $return;
        }
        elseif( $options_search_block['html_return'] )
        {
            return $html_print;
        }
        else print $html_print;

    }

    # END SECTION SEARCHES

function div_detail_view_item_start($db,$options)
{
    $html_print="";

    if( !empty($options['row']['id']) ) $id=$options['row']['id']; # bookid
    if( !empty($options['row']['exID']) ) $id=$options['row']['exID']; # exhibitionid

    $html_print.="<div class='div-detail-view-item'>";

        if( $options['count_images']>0 )
        {
            $html_print.="<div class='div-detail-view-item-left ".$options['class']['div-detail-view-item-left']."'>";
                $html_print.="<div style='height:".$options['height']."px;width:".$options['width']."px;' class='div-detail-view-item-images'>";
                    $html_print.="<a href='".$options['href_image']."' class='fancybox-buttons' data-fancybox-group='itemid-".$id."'>\n";
                        $html_print.="<img class='img-enlarge-icon' alt='' src='/g/zoom-icon.png'>";
                        $html_print.="<img src='".$options['src_image']."' alt='' class='img-detail-view'>\n";
                    $html_print.="</a>\n";
                    for($i=1;$i<count($options['images_array']);$i++)
                    {
                        $imageid=UTILS::get_relation_id($db,"17",$options['images_array'][$i]);
                        //$href="/images/size_xl__imageid_".$imageid.".jpg";
                        $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                        $html_print.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='itemid-".$id."'>\n";
                            $html_print.="<img style='display:none;' src='' alt=''>\n";
                        $html_print.="</a>\n";
                    }
                $html_print.="</div>";
            $html_print.="</div>";
        }
        else $class_large="div-detail-view-item-right-large";

            # right side item info text
            $html_print.="<div class='div-detail-view-item-right ".$class_large." ".$options['class']['div-detail-view-item-right']."'>";
                if( !empty($options['title']) )
                {
                    $html_print.="<h1 class='h1-item ".$options['class']['h1-item']."'>".$options['title']."</h1>";
                }
                if( !empty($options['title_translation']) && $options['title_translation']!=$options['title'] )
                {
                    $html_print.="<p class='p-item-translation ".$options['class']['p-item-translation']."'>";
                        $html_print.=$options['title_translation'];
                    $html_print.="</p>\n";
                }

    if( $options['html_return'] ) return $html_print;
    else print $html_print;

}

function div_detail_view_item_end($db,$options=array())
{
    $html_print="";

        # item admin edit
        $options_admin_edit=array();
        $options_admin_edit=$options;
        $html_print.=UTILS::admin_edit($options['url_admin_edit'],$options_admin_edit)."\n";
        # END item admin edit

        $html_print.="</div>";
        # END right side item info text

        $html_print.="<div class='clearer'></div>";
    $html_print.="</div>";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function exhibitionlist($db,$values)
{
    if( $values['i']==1 )
    {
        print "\t<p class='exhibition-type solo margin'>".$values['row']['date_year']." (".$values['title'].")</p>\n";

        if( $values['type']==1 ) $class="first-solo";
        else $class="first-group";
        print "\t<ul class='exhibition-desc $class'>\n";
    }

        $row=UTILS::html_decode($values['row']);
        $results_exhibitions_relations=$db->query("SELECT typeid1 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row['exID']."'  AND typeid2=1 ) OR ( typeid2=4 AND itemid2='".$row['exID']."' AND typeid1=1 ) LIMIT 1 ");
        $count_exhibitions_relations=$db->numrows($results_exhibitions_relations);

        $results_installations_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row['exID']."'  AND typeid2=5 ) OR ( typeid2=4 AND itemid2='".$row['exID']."' AND typeid1=5 ) LIMIT 1 ");
        $row_installations_relations=$db->mysql_array($results_installations_relations);
        $count_installations_relations=$db->numrows($results_installations_relations);
        $installationid=UTILS::get_relation_id($db,"5",$row_installations_relations);

        print "\t<li>\n";

            print "\t<table class='exhibitondetail-table' >\n";

                if( !empty($row['tour_button_url']) ) $tour="<a href='".$row['tour_button_url']."' title='".$row['tour_button_url']."' target='blank'><img src='/g/exhibitions/tour.gif' alt='tour' /></a>";
                elseif( $count_exhibitions_relations>0 ) $tour="<a href='/exhibitions/exhibition.php?exID=".$row['exID']."' title=''><img src='/g/exhibitions/tour.gif' alt='tour' /></a>";
                else $tour="";
                print "\t<tr>\n";
                    print "\t<td style='vertical-align:top;width: 37px;padding-top: 5px;'>".$tour."</td>\n";
                    print "\t<td class='exhibition-title' colspan='2'>";
                        print "\t<a name='".$row['exID']."'></a>\n";
                        $title=UTILS::row_text_value2($db,$row,"title");
                        if( empty($row['title_original']) )
                        {
                            print $title;
                        }
                        else print $row['title_original'];
                        if( !empty($row['title_original']) && $title!=$row['title_original'] )
                        {
                            print "\t<br />\n";
                            print "\t<span class='span-exhibition-title-translation'>\n";
                                print $title;
                            print "\t</span>\n";
                        }
                        print "&nbsp;".UTILS::admin_edit("/admin/exhibitions/edit/?exhibitionid=".$row['exID']);
                    print "\t</td>\n";
                print "\t</tr>\n";

                if( $count_installations_relations>0 ) $installation="<a href='/exhibitions/installation_shots/?installationid=".$installationid."' title=''><img src='/g/exhibitions/photos_".$_GET['lang'].".gif' alt='photos' /></a>";
                else $installation="";
                print "\t<tr>\n";
                    print "\t<td style='vertical-align:bottom;width: 37px;'>".$installation."</td>\n";
                    print "\t<td colspan='2'>\n";
                        $values_location=array();
                        $values_location['row']=$row;
                        $location=location($db,$values_location);
                        print $location;
                    print"\t</td>";
                print "\t</tr>\n";

                $results_video_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row['exID']."'  AND typeid2=10 ) OR ( typeid2=4 AND itemid2='".$row['exID']."' AND typeid1=10 ) LIMIT 1 ");
                $count_video_relations=$db->numrows($results_video_relations);
                if( $count_video_relations )
                {
                    $row_video_relations=$db->mysql_array($results_video_relations);
                    $videoid=UTILS::get_relation_id($db,"10",$row_video_relations);
                    $query_where_video=" WHERE videoID='".$videoid."' AND enable=1 ";
                    $query_video=QUERIES::query_videos($db,$query_where_video);
                    $results_video=$db->query($query_video['query']);
                    $count_video=$db->numrows($results_video);
                    $row_video=$db->mysql_array($results_video);
                    $titleurl_video=UTILS::row_text_value($db,$row_video,"titleurl");

                    $query_where_cat=" WHERE categoryid='".$row_video['categoryid']."' AND enable=1 ";
                    $query_cat=QUERIES::query_videos_categories($db,$query_where_cat);
                    $results_cat=$db->query($query_cat['query']);
                    $count_cat=$db->numrows($results_cat);
                    $row_cat=$db->mysql_array($results_cat);
                    $sectionurl=UTILS::row_text_value($db,$row_cat,"titleurl");

                    $url_video="/videos/".$sectionurl."/".$titleurl_video;

                    if( $count_video && $count_cat ) $video="<a href='".$url_video."' class='exhibition-video' title=''><img src='/g/exhibitions/video.gif' alt='video' /></a>";
                }
                else $video="";

                $date=exhibition_date($db,$row);

                print "\t<tr>\n";
                    print "\t<td style='vertical-align:bottom;width: 37px;'>".$video."</td>\n";
                    print "\t<td class='exhibition-date' colspan='2'>".$date."</td>\n";
                print "\t</tr>\n";

            print "\t</table>\n";

        print "\t</li>\n";

    if( $values['i']==$values['count'] )
    {
        print "\t</ul>\n";
    }
}

function get_url_back($db,$options)
{
    //print_r($options);
    $return=array();
    if( empty($options['referer']) )
    {
        $return['url_back']=$options['url_back'];
        $return['back_text']=$options['url_back_text'];
    }
    else
    {
        if( $options['referer']=="search" || $options['referer']=="search-main" )
        {
            $return['url_back']="/".$_GET['lang']."/search/?search=".urlencode(UTILS::strip_slashes_recursive($options['title']));
            $return['back_text']=LANG_SEARCH_RETURNTOTSEARCH;
        }
        elseif( $options['referer']=="search-art" )
        {
            $return['url_back']="/".$_GET['lang']."/art/search/?".$options['search_vars'];
            $return['back_text']=LANG_SEARCH_RETURNTOTSEARCH;
        }
        elseif( $options['referer']=="news-upcoming" )
        {
            $return['url_back']="/".$_GET['lang']."/news/auctions/";
            $return['back_text']=LANG_NEWS_RETURN_TO_NEWS_PAGE;
        }
        elseif( $options['referer']=="news-results" )
        {
            $return['url_back']="/".$_GET['lang']."/news/auctions/results/";
            $return['back_text']=LANG_NEWS_RETURN_TO_NEWS_PAGE;
        }
    }

    return $return;
}


    function flash($db,$var)
    {

        $code="\t<div id='".$var['id']."'>\n";
            $code.="\t<a href='https://www.adobe.com/go/getflashplayer'>";
                $code.="<img src='/g/get_flash_player.gif' alt='Get Adobe Flash player' />";
            $code.="</a>\n";
        $code.="\t</div>\n";

        $code.="\t<script type='text/javascript'>";

            #flashvars
            $code.="var flashvars = {};";
            $code.="flashvars.urlsp='".HTTP_SERVER."';";
            $code.="flashvars.vwidth='".$var['width']."';";
            $code.="flashvars.vheight='".$var['height']."';";
            $code.="flashvars.lang='".$var['lang']."';";
            #end flashvars

            #params
            $code.="var params = {};";
            $code.="params.quality='best';";
            $code.="params.scale='noscale';";
            $code.="params.allownetworking='all';";
            $code.="params.menu='false';";
            $code.="params.bgcolor='".$var['bgcolor']."';";

            if( !empty($var['watermark']) ) $code.="flashvars.watermark='".$var['watermark']."';";
            if( !empty($var['introtxt']) ) $code.="flashvars.introtxt='".$var['introtxt']."';";
            if( !empty($var['videoID']) ) $code.="flashvars.videoID='".$var['videoID']."';";
            if( !empty($var['urlPath']) ) $code.="flashvars.urlPath='/includes/retrieve.video.php/".$var['videoID']."/f';";
            if( !empty($var['video_source']) ) $code.="flashvars.video_source='".$var['video_source']."';";
            if( !empty($var['paintID']) ) $code.="flashvars.paintID='".$var['paintID']."';";
            if( !empty($var['dir']) ) $code.="flashvars.dir='".$var['dir']."';";
            #end params

            #attributes
            $code.="var attributes = {};";
            $code.="attributes.id='".$var['id']."';";
            if( !empty($var['class']) ) $code.="attributes.class='".$var['class']."';";
            if( !empty($var['name']) ) $code.="attributes.name='".$var['name']."';";
            if( !empty($var['title']) ) $code.="attributes.title='".$var['title']."';";
            #end attributes

            $code.="swfobject.embedSWF('".$var['url']."', '".$var['id']."', '".$var['width']."', '".$var['height']."', '".$var['version']."',false, flashvars, params, attributes);";

        $code.="</script>\n";

        return $code;

    }

    public static function div_text_wysiwyg($db,$text,$options=array())
    {
        # replace in text everything what needs to be replaced

            # replace url links to be correct lang urls if link is local /en/art to /de/art ..
            $text=UTILS::replace_lang_for_link($db,$text,$_GET['lang']);
            # end

        # end

        $html_print="";

        if( !empty($options['id']) ) $id=" id='".$options['id']."' ";

        $html_print.="\t<div ".$id." class='div_text_wysiwyg ".$options['class']['div_text_wysiwyg']." ".$options['class']['div-news-disclimer']." ".$options['class']['div-category-desc']." ".$options['class']['div_text_wysiwyg_biography']."' >\n";
            $html_print.=$text;
        $html_print.="\t</div>\n";

        return $html_print;
    }

    # SELECT DROP DOWN LISTS

    function select_articles_categories($db,$options=array())
    {
        $html_print="";

        $query_order=" ORDER BY sort ASC ";
        $query_article=QUERIES::query_articles_categories($db,$query_where_article,$query_order);
        $results=$db->query($query_article['query_without_limit']);
        $count=$db->numrows($results);

        if( empty($options['id']) ) $options['id']=$options['name'];
        $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".$options['class']['select-field']." ".$options['class']['select-large']."' >\n";
            if( empty($options['multiple']) ) $html_print.="<option value=''>".LANG_RIGHT_SEARCH_LIT_CATEGORY."...</option>\n";
            while( $row=$db->mysql_array($results) )
            {
                if( $row['articles_catid']==$options['selectedid'] || @in_array($row['articles_catid'], $options['selectedid']) ) $selected="selected='selected'";
                else $selected="";
                $row=UTILS::html_decode($row);
                $title=UTILS::row_text_value($db,$row,"title");
                $html_print.="<option value='".$row['articles_catid']."' ".$selected." >".$title."</option>\n";
            }
        $html_print.="</select>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }


    function select_colors($db,$options=array())
    {
        $html_print="";

        $query_order=" ORDER BY sort ASC ";
        $query_colors=QUERIES::query_painting_colors($db,$query_where_colors,$query_order,$query_limit);
        $results=$db->query($query_colors['query_without_limit']);

        $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".$options['class']['select-field']." ".$options['class']['select-large']."' >\n";
            if( empty($options['multiple']) ) $html_print.="<option value=''>".LANG_RIGHT_SEARCH_COLOR2."</option>\n";
            while( $row=$db->mysql_array($results) )
            {
                if( $row['colorid']==$options['selectedid'] || @in_array($row['colorid'], $options['selectedid']) ) $selected="selected='selected'";
                else $selected="";
                //$row=UTILS::html_decode($row);
                $title=UTILS::row_text_value($db,$row,"title");
                $html_print.="<option value='".$row['colorid']."' ".$selected." >".$title."</option>\n";
            }
        $html_print.="</select>\n";

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    //function select_books_per_page($db,$name,$id,$selectedid,$onchange="",$class=array(),$disable="",$where="",$multiple="",$select_title=LANG_RIGHT_SEARCH_LIT_LANGUAGE_SELECT)
    function select_books_per_page($db,$options)
    {
        $html_print="";

        if( $options['count']>$options['sp'] )
        {
            $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".$options['class']['select_pp']."' >\n";
                $pages_show=20;
                if( $options['sp']==$pages_show ) $selected=" selected='selected' ";
                else $selected="";
                $html_print.="<option value='".$pages_show."' ".$selected." >".$pages_show."</option>\n";

                $pages_show=40;
                if( $options['sp']==$pages_show ) $selected=" selected='selected' ";
                else $selected="";
                $html_print.="<option value='".$pages_show."' ".$selected." >".$pages_show."</option>\n";

                $pages_show=60;
                if( $options['sp']==$pages_show ) $selected=" selected='selected' ";
                else $selected="";
                $html_print.="<option value='".$pages_show."' ".$selected." >".$pages_show."</option>\n";

                $pages_show=100;
                if( $options['sp']==$pages_show ) $selected=" selected='selected' ";
                else $selected="";
                $html_print.="<option value='".$pages_show."' ".$selected." >".$pages_show."</option>\n";

                if( $options['sp']=='1000000' ) $selected=" selected='selected' ";
                else $selected="";
                $html_print.="<option value='all' ".$selected.">".LANG_ALL."</option>\n";
            $html_print.="</select>\n";
        }

        if( $options['html_return'] ) return $html_print;
        else print $html_print;
    }

    # END SELECT DROP DOWN LISTS


}

?>
