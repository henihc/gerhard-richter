<?
//session_start();

# this is to cut out external links on site
/*
if( $_GET['kiosk']==1 )
{
    $_SESSION['kiosk']=$_GET['kiosk'];
}
else if( $_GET['kiosk']==2 ) unset($_SESSION['kiosk']);
*/
# END

# to display prints section - temporary
/*
if( isset($_GET['show_prints']) )
{
    $_SESSION['show_prints']=1;
}
*/
# END

require ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/db_search.php");
require ($_SERVER["DOCUMENT_ROOT"]."/back-soon.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");

# allowed ips
$ip=UTILS::get_ip();
//$ip="122.170.18.186";
if( !IS_DEV &&
    #Jin
    stripos($ip,"122.171.") === false && stripos($ip,"122.172.") === false && stripos($ip,"116.68") === false &&
    # babite
    $ip!="87.110.13.86" &&
    # all ips
    $ip!="77.72.201.34" && $ip!="78.84.245.247" && $ip!="78.84.231.123" && $ip!="195.99.219.98" && $ip!="77.93.128.83" && $ip!="81.130.105.222" && $ip!="85.240.32.220" && $ip!="87.194.121.242" && $ip!="87.81.225.122" && $ip!="77.12.229.123" && $ip!="212.140.254.194" )
    {
        //print "not allowed";
        //exit;
    }
    else
    {
    	session_start();
    }
# END allowed ips

require ($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/class.search.inc.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/table.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/browser_detect.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/relation_tabs.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/class.html.inc.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/class.mobile.detect.php");

?>
