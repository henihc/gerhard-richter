<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/const.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/db_queries.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/utils.php");

$db=new dbCLASS;
$_GET=$db->db_prepare_input($_GET);

### what we are retrieving category image or painting image
if( isset( $_GET['catID'] ) )
{
    //$results=$db->query("SELECT artworkID,src FROM ".TABLE_CATEGORY." WHERE catID='".$_GET['catID']."' ");
    $typeid_for_image_use=18;
    $itemid_for_image_use=$_GET['catID'];
}
elseif( isset( $_GET['photoID'] ) )
{
    //$results=$db->query("SELECT artworkID,src FROM ".TABLE_PHOTOS." WHERE photoID='".$_GET['photoID']."' ");
    $typeid_for_image_use=8;
    $itemid_for_image_use=$_GET['photoID'];
}
elseif( isset( $_GET['exID'] ) )
{
    //$results=$db->query("SELECT src FROM ".TABLE_EXHIBITIONS." WHERE exID='".$_GET['exID']."' ");
    $typeid_for_image_use=4;
    $itemid_for_image_use=$_GET['exID'];
}
elseif( isset( $_GET['bookID'] ) )
{
    $typeid_for_image_use=12;
    $itemid_for_image_use=$_GET['bookID'];
}
elseif( isset( $_GET['noteid'] ) )
{
    //$results=$db->query("SELECT src FROM ".TABLE_PAINTING_NOTE." WHERE noteid='".$_GET['noteid']."' ");
    $typeid_for_image_use=15;
    $itemid_for_image_use=$_GET['noteid'];
}
elseif( isset( $_GET['newsID'] ) )
{
    //$results=$db->query("SELECT src FROM ".TABLE_NEWS." WHERE newsID='".$_GET['newsID']."' ");
    $typeid_for_image_use=7;
    $itemid_for_image_use=$_GET['newsID'];
}
elseif( isset( $_GET['audioid'] ) )
{
    //$results=$db->query("SELECT src_img AS src FROM ".TABLE_AUDIO." WHERE audioid='".$_GET['audioid']."' ");
    $typeid_for_image_use=11;
    $itemid_for_image_use=$_GET['audioid'];
}
elseif( isset( $_GET['quotes_categoryid'] ) )
{
    //$results=$db->query("SELECT src FROM ".TABLE_QUOTES_CATEGORIES." WHERE quotes_categoryid='".$_GET['quotes_categoryid']."' ");
    $typeid_for_image_use=19;
    $itemid_for_image_use=$_GET['quotes_categoryid'];
}
else
{
    //$results=$db->query("SELECT paintID,artworkID,src FROM ".TABLE_PAINTING." WHERE paintID='".$_GET['paintID']."' ");
    $typeid_for_image_use=1;
    $itemid_for_image_use=$_GET['paintID'];
}
#end


if( isset( $_GET['microsites_imageid'] ) )
{
    $results=$db->query("SELECT src FROM ".TABLE_MICROSITES_IMAGES." WHERE imageid='".$_GET['microsites_imageid']."' ");
}
elseif( isset( $_GET['videoID'] ) )
{
    $results=$db->query("SELECT image AS src FROM ".TABLE_VIDEO." WHERE videoID='".$_GET['videoID']."' ");
}
elseif( isset( $_GET['installation_imageid'] ) )
{
    $results=$db->query("SELECT src FROM ".TABLE_EXHIBITIONS_INSTALLATIONS_IMAGES." WHERE imageid='".$_GET['installation_imageid']."' ");
}
else
{
    $query_related_image="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2='".$typeid_for_image_use."' AND itemid2='".$itemid_for_image_use."' ) OR ( typeid2=17 AND typeid1='".$typeid_for_image_use."' AND itemid1='".$itemid_for_image_use."' ) ";
    $query_related_image.=" ORDER BY sort ASC, relationid DESC";
    $results_related_image=$db->query($query_related_image);
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
    $results=$db->query("SELECT * FROM ".TABLE_IMAGES." WHERE imageid='".$imageid."'");
}




if( !empty($results2) ) 
{
    $count=$db2->numrows($results2);
    $row=$db2->mysql_array($results2);
}
else 
{
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);
}

if( $count!=1  ) $error[]="Can't find image on db";

		### get size folder name
		switch($_GET['size'])
		{
			case 'pair' : $size="pair"; $no_image=LANG_NO_IMAGE_XSMALL; break;
			case 'xs' : $size="xsmall"; $no_image=LANG_NO_IMAGE_XSMALL; break;
			case 's' : $size="small"; $no_image=LANG_NO_IMAGE_SMALL; break;
			case 'm' : $size="medium"; $no_image=LANG_NO_IMAGE_MEDIUM; break;
            case 'l' : $size="large"; $no_image=LANG_NO_IMAGE_LARGE; break;
            case 'll' : $size="llarge"; $no_image=LANG_NO_IMAGE_LARGE; break;
            case 'lll' : $size="lllarge"; $no_image=LANG_NO_IMAGE_XLARGE; break;
			case 'xl' : $size="xlarge"; $no_image=LANG_NO_IMAGE_LARGE; break;
            case 'xlsnowwhite' : $size="xlarge_snowwhite"; $no_image=LANG_NO_IMAGE_LARGE; break;
            case 'xlwarcut' : $size="xlarge_warcut"; $no_image=LANG_NO_IMAGE_LARGE; break;
            case 'xlflorence' : $size="xlarge_florence"; $no_image=LANG_NO_IMAGE_LARGE; break;
			case 'o' : $size="original"; $no_image=LANG_NO_IMAGE_ORIGINAL; break;
			default: $size="original"; $no_image=LANG_NO_IMAGE_THUMBNAIL; 
		}
		#end

	###for books medium size only
	if( isset($_GET['bookimageID']) && $_GET['size']=="m" ) $no_image=LANG_NO_IMAGE_SMALL;
	#end

### if found image in db but src is null
if( sizeof($error)==0 && empty($row['src']) ) $error[]="Image found in db but src is null";
#end

### if found image in db
if( sizeof($error)==0 )
{
	
$tmp=explode(".",$row['src']);
if( $size!="original" ) $row['src']=$tmp[0].".jpg"; 

if( isset( $_GET['quotes_categoryid'] ) )
{
    if( $_GET['size']=="t" ) $size="large";
}


if( isset( $_GET['microsites_imageid'] ) )
{
    $src=DATA_PATH_DISPLAY."/images/microsites/".$size."/".$row['src'];
}
elseif( isset( $_GET['videoID'] ) )
{
    $src=DATA_PATH_DISPLAY."/images/video/".$size."/".$row['src'];
}
elseif( isset( $_GET['installation_imageid'] ) )
{
    $src=DATA_PATH_DISPLAY."/images/exhibitions_installations/".$size."/".$row['src'];
}
/*
elseif( !empty($_GET['productid']) )
{
    $src=DATA_PATH_SHOP."/images/products/".$size."/".$row['src'];
}
*/
else
{
    $src=DATA_PATH_DISPLAY."/images_new/".$size."/".$row['src'];
}	

	### if file exists on file system
    if( file_exists($src) )
    {
    	### if showing THUMBNAIL
    	if( $_GET['thumb'] )
        {	
            if( !$_GET['debug'] ) 
            {
                $getimagesize = getimagesize($src);
                switch( $getimagesize['mime'] )
                {
                case 'image/gif'  : header("Content-Type: image/gif"); break;
                case 'image/png'  : header("Content-Type: image/png"); break;
                case 'image/jpeg' : header("Content-Type: image/jpeg"); break;
                case 'image/bmp'  : header("Content-Type: image/bmp"); break;
                default           : exit("Unsupported image format! Supported formats: JPEG, GIF, PNG, BMP");
                }
                header('Content-Disposition: inline; filename='.basename($src));

            	if( !empty($_GET['maxwidth']) && !empty($_GET['maxheight']) ) 
            	{
            	$maxwidth=$_GET['maxwidth'];
            	$maxheight=$_GET['maxheight'];
            	}
            	else
            	{
            	$maxwidth=THUMBNAIL_WIDTH;
            	$maxheight=THUMBNAIL_HEIGHT;
            	}
            	//exit($maxwidth."-".$maxheight);
                if( !isset($_GET['bgcolor']) ) $bgcolor=THUMBNAIL_BGCOLOUR;
                else $bgcolor=$_GET['bgcolor'];

        		$img = UTILS::LoadJpeg($src,$bgcolor,$maxwidth,$maxheight,$_GET['border']==0);
                if( !empty($_GET['quality']) ) $quality=$_GET['quality'];
                else $quality=THUMBNAIL_QUALITY;
                imagejpeg($img,null,$quality);
            }
            else
            {
                print $src;
            }
        }
        #end
        
        ### if showing image straight from file system
		else
        {
            if( !$_GET['debug'] )
            {
		       $getimagesize=getimagesize($src);//!!! if slow maybe need to take this getsize function out 
        	    if( empty($getimagesize['mime']) ) $getimagesize['mime']="image/jpeg";
                header('Content-Type: '.$getimagesize['mime']);
                header('Content-Disposition: inline; filename='.basename($src));
        	    $f = fopen($src, 'rb');
        	    $contents = stream_get_contents($f);
        	    print $contents;
                fclose($f);
            } 
            else
            {
                print $src;
            }
		}
		#end
	}
	#end
	else $error[]="Can't find image on file system ".$src;
	
}
#end

### if no image found showing no image available
if( sizeof($error)>0 )
{
    if( $_GET['debug'] )
    {
    print_r($error);
    exit;
    }
$src=$no_image;
$getimagesize=getimagesize($no_image);//!!! if slow maybe need to take this getsize function out 
if( empty($getimagesize['mime']) ) $getimagesize['mime']="image/gif";
header('Content-Type: '.$getimagesize['mime']);
$f = fopen($src, 'rb');
$contents = stream_get_contents($f);
print $contents;
}
#end

?>
