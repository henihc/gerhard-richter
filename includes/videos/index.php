<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";
        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="});";
        # END tooltips load

        # select box pretty
        $html_print_body.="$('.select-field').selectBoxIt({";
            //$html_print_body.="showFirstOption: false";
        $html_print_body.="});";
        //$html_print_body.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
        //$html_print_body.="selectBox.selectOption(0);";

    $html_print_body.="});";
$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

# short clips video detail view
if( $_GET['section_1']=="short-clips" && !empty($_GET['section_3'])  )
{
	$video_detail_view=$_GET['section_3'];
}
# short clips sub category view
elseif( $_GET['section_1']=="short-clips" && !empty($_GET['section_2']) && empty($_GET['section_3'])  )
{
	$video_category_view=$_GET['section_2'];
}
# all other categories video detail view
elseif( !empty($_GET['section_2']) )
{
	$video_detail_view=$_GET['section_2'];
}
# all other categories view
elseif( empty($_GET['section_2']) )
{
	if( !empty($_GET['section_1']) ) $video_category_view=$_GET['section_1'];
	else $video_category_view=1;
	if( $_GET['section_1']=="short-clips" ) $video_sub_category_view=1;
}

if( !empty($_GET['section_3']) && $_GET['section_1']!="short-clips" ) UTILS::not_found_404();
elseif( !empty($_GET['section_4']) ) UTILS::not_found_404();

# VIDEOS detail page /videos/panorama-1234
if( $video_detail_view )
{
	# video
	$videoid=UTILS::itemid($db,$video_detail_view);
    $query_where_video=" WHERE videoID='".$videoid."' AND enable=1 ";
    $query_video=QUERIES::query_videos($db,$query_where_video);
    $results_video=$db->query($query_video['query']);
    $count=$db->numrows($results_video);
    $row=$db->mysql_array($results_video);
    $row=UTILS::html_decode($row);
    $title_video=UTILS::row_text_value2($db,$row,"title");
    $info=UTILS::row_text_value($db,$row,"info");
    $title_meta.=$title_video." &raquo; ";

    $swf_file_path="/includes/videos/flashvideo.swf";
    $m4v_file_path="/datadir/videos/m4v/".$row['videoID'].".m4v";
    $ogv_file_path="/datadir/videos/ogv/".$row['videoID'].".ogv";

    $browser = new Browser();
    if( $browser->getBrowser() == Browser::BROWSER_IE && $browser->getVersion() <= 8 ) {}
    else
    {
        //$image_url="/includes/retrieve.image.php?videoID=".$row['videoID']."&size=l";
        $image_url=DATA_PATH_DATADIR."/images/video/large/".$row['videoID'].".jpg";
    }

    //$width_video="672";
    $width_video="672";
    //$height_video="376";
    $height_video="378";

    if( !$count) UTILS::not_found_404();

	# videos category
    $query_where_cat=" WHERE titleurl='".$_GET['section_1']."' ";
    $query_cat=QUERIES::query_videos_categories($db,$query_where_cat);
    $results_cat=$db->query($query_cat['query']);
    $count_cat=$db->numrows($results_cat);
    $row_cat=$db->mysql_array($results_cat);
    $row_cat=UTILS::html_decode($row_cat);
    $categoryid=$row_cat['categoryid'];
    $sectionurl=UTILS::row_text_value($db,$row_cat,"titleurl");
    $title_category=UTILS::row_text_value($db,$row_cat,"title");
    $desc_cat=UTILS::row_text_value($db,$row_cat,"desc");
    //$title_meta.=$title_category." &raquo; ";
    if( !$count_cat) UTILS::not_found_404();

	# title meta
	$html->title=$title_meta.LANG_HEADER_MENU_VIDEOS." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/videos.css";
	$html->css[]="/css/tabs.css";
	$html->css[]="/css/exhibitions.css";
	$html->css[]="/css/books.css";
	$html->css[]="/css/page-numbering.css";
	$html->css[]="/css/painting_detail.css";
	$html->css[]="/css/painting-detail-view.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# tabs and tooltips
	//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.js";

    #expand table
    $html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";
    $html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.js";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

    # fallback if video tag is not supported
    $html->js[]="/js/swfobject.js";

    # jquery subtitles
    $html->js[]="/js/jquery_video/jquery.srt.js";
    //$html->js[]="/js/jquery_video/jquery.srt_old.js";
    //$html->js[]="/js/video_subtitles/bubbles.js";

	# history support
	$html->js[]="/js/history/jquery.history.js";

	# jquery scroller support
	$html->js[]="/js/jquery-scroller-master/jquery.fs.scroller.js";
	$html->css[]="/js/jquery-scroller-master/jquery.fs.scroller.min.css";

	# meta-tags & head
	$options_head=array();
	$options_head['meta_description']=$info;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$options_head['facbook_metainfo']['type']="video.other";
	$options_head['facbook_metainfo']['image']=HTTP_SERVER.$image_url;
	$options_head['facbook_metainfo']['title']=$title_video;
	//$options_head['facbook_metainfo']['updated_time']=;
	$options_head['facbook_metainfo']['video']=HTTP_SERVER.$m4v_file_path;
	$options_head['facbook_metainfo']['video_secure_url']=HTTP_SERVER.$m4v_file_path;
	$options_head['facbook_metainfo']['video_type']="video/mp4";
	$options_head['facbook_metainfo']['video_width']=$width_video;
	$options_head['facbook_metainfo']['video_height']=$height_video;
	$html_print_head.=$html->head($db,'videos',$options_head);

	$html_print_body.="<script type='text/javascript'>\n";
	    $html_print_body.="$(function() {";
			# scrollbar for painting relation tab
			$html_print_body.="$('.scrollbar').scroller({";
				$html_print_body.="horizontal: true";
			$html_print_body.="});";
			# END scrollbar for painting relation tab
	    $html_print_body.="});";
	$html_print_body.="</script>\n";

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$options_left_side['categoryid']=$categoryid;
		$html_print_head.=$html->ul_videos($db,1,$options_left_side);
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	   	$options_breadcrumb['admin_edit']=UTILS::admin_edit("/admin/videos/edit/?videoid=".$row['videoID'],$options_head);
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
		$options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_VIDEOS;
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/videos";
	    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_VIDEOS;
	    $title_category=str_replace("<br/>"," ",$title_category);
	    $options_breadcrumb['items'][1]['inner_html']=$title_category;
	    $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/videos/".$row_cat['titleurl'];
	    $options_breadcrumb['items'][1]['title']=$title_category;
	    $options_breadcrumb['items'][2]['inner_html']=$title_video;
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

	    $html_print_body.="<div id='div-video-detail-content' role='main'>\n";

	        $html_print_body.="<p class='p-video-detile-title'>".$title_video."</p>";

	        $flashvars="?video_source=".$m4v_file_path."&skin=/includes/videos/SkinOverPlayStopSeekMuteVol.swf&poster=".$image_url."&width=".$width_video."&height=".$height_video;

	        $html_print_body.="<video id='video' width='".$width_video."' height='".$height_video."' autoplay='' controls='controls' poster='".$image_url."'>\n";
	            $html_print_body.="<source src='".$m4v_file_path."' type='video/mp4' />\n";
	            $html_print_body.="<source src='".$ogv_file_path."' type='video/ogg' />\n";

	            $html_print_body.="<div id='flashContent'>\n";

	                $html_print_body.="<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' width='".$width_video."' height='".$height_video."' id='flashvideo' align='middle'>\n";
	                    $html_print_body.="<param name='movie' value='".$swf_file_path.$flashvars."' />\n";
	                    $html_print_body.="<param name='quality' value='high' />\n";
	                    $html_print_body.="<param name='bgcolor' value='#ffffff' />\n";
	                    $html_print_body.="<param name='play' value='true' />\n";
	                    $html_print_body.="<param name='loop' value='true' />\n";
	                    $html_print_body.="<param name='wmode' value='transparent' />\n";
	                    $html_print_body.="<param name='scale' value='showall' />\n";
	                    $html_print_body.="<param name='menu' value='true' />\n";
	                    $html_print_body.="<param name='devicefont' value='false' />\n";
	                    $html_print_body.="<param name='salign' value='lt' />\n";
	                    $html_print_body.="<param name='width' value='".$width_video."' />\n";
	                    $html_print_body.="<param name='height' value='".$height_video."' />\n";
	                    $html_print_body.="<param name='allowScriptAccess' value='sameDomain' />\n";
	                    $html_print_body.="<!--[if !IE]>-->\n";
	                        $html_print_body.="<object type='application/x-shockwave-flash' data='".$swf_file_path.$flashvars."' width='".$width_video."' height='".$height_video."'>\n";
	                            $html_print_body.="<param name='movie' value='".$swf_file_path.$flashvars."' />\n";
	                            $html_print_body.="<param name='quality' value='high' />\n";
	                            $html_print_body.="<param name='bgcolor' value='#ffffff' />\n";
	                            $html_print_body.="<param name='play' value='true' />\n";
	                            $html_print_body.="<param name='loop' value='true' />\n";
	                            $html_print_body.="<param name='wmode' value='transparent' />\n";
	                            $html_print_body.="<param name='scale' value='showall' />\n";
	                            $html_print_body.="<param name='menu' value='true' />\n";
	                            $html_print_body.="<param name='devicefont' value='false' />\n";
	                            $html_print_body.="<param name='salign' value='lt' />\n";
	                            $html_print_body.="<param name='allowScriptAccess' value='sameDomain' />\n";
	                            $html_print_body.="<param name='allowfullscreen' value='true' />\n";
	                            $html_print_body.="<param name='width' value='".$width_video."' />\n";
	                            $html_print_body.="<param name='height' value='".$height_video."' />\n";
	                            $html_print_body.="<!--<![endif]-->\n";
	                                $html_print_body.="<a href='https://www.adobe.com/go/getflash'>\n";
	                                    $html_print_body.="<img src='https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' />\n";
	                                $html_print_body.="</a>\n";
	                            $html_print_body.="<!--[if !IE]>-->\n";
	                        $html_print_body.="</object>\n";
	                    $html_print_body.="<!--<![endif]-->\n";
	                $html_print_body.="</object>\n";
	            $html_print_body.="</div>\n";

	            //$html_print_body.="<track src='/includes/retrieve.video_subtitles.php?subtitleid=5' kind='subtitle' srclang='en-US' label='English' />\n";

	        $html_print_body.="</video>\n";

	        # subtitles
	        /*
	        $options_languages=array();
	        $options_languages['lang']=$_SESSION['lang'];
	        $language=$html->languages($db,$options_languages);
	        */

	        //$results_subtitles=$db->query("SELECT * FROM ".TABLE_VIDEO_SUBTITLES." WHERE videoid='".$row['videoID']."' AND languageid='".$language['languageid']."' ORDER BY sort LIMIT 1 ");
	        if( $_GET['lang']=="en" ) $_GET['languageid']=1;
	        if( $_GET['lang']=="de" ) $_GET['languageid']=2;
	        if( $_GET['lang']=="fr" ) $_GET['languageid']=3;
	        if( $_GET['lang']=="it" ) $_GET['languageid']=4;
	        if( $_GET['lang']=="zh" ) $_GET['languageid']=5;
	        $query_video_subtitles="SELECT * FROM ".TABLE_VIDEO_SUBTITLES." WHERE videoid='".$row['videoID']."' AND languageid='".$_GET['languageid']."' ORDER BY sort LIMIT 1 ";
	       	$results_subtitles=$db->query($query_video_subtitles);
	        $count_subtitles=$db->numrows($results_subtitles);
	        //print $count_subtitles;
	        //print $query_video_subtitles;
	        if( $count_subtitles )
	        {
	            $row_subtitles=$db->mysql_array($results_subtitles);
	            $data_srt="/includes/retrieve.video_subtitles.php?subtitleid=".$row_subtitles['subtitleid']."";
	            $html_print_body.="<div id='video-srt_subs' class='div-video-detail-subtitles-srt srt' data-video='video' data-srt='".$data_srt."'> </div>\n";
	        }
	        # END subtitles

	        $html_print_body.="<script type='text/javascript'>\n";

	            $html_print_body.="var video = document.getElementById('video');\n";
	            $html_print_body.="try { video.currentTime = 0; } catch (err) {}";

	            $html_print_body.="function video_callback (_pos) {\n";
	                $html_print_body.="video.currentTime = Number(_pos);\n";
	            $html_print_body.="}\n";

	        $html_print_body.="</script>\n";

	        # div-video-detail-info 
	        $time=UTILS::row_text_value2($db,$row,"time");
	        $html_print_body.="<div class='div-video-detail-info'>\n";
	            $html_print_body.="<p class='p-video-detail-title-year-etc'>";
	                if( !empty($row['year']) ) $html_print_body.="<span class='span-video-info-year'>".$row['year']."</span>";
	                if( !empty($row['name']) ) $html_print_body.="<span class='span-video-info-name'>".$row['name']."</span>";
	                $html_print_body.="<span class='span-video-info-time'>".$time."</span>";
	            $html_print_body.="</p>";
	        $html_print_body.="</div>\n";
	        # END div-video-detail-info 

	    $html_print_body.="</div>\n"; 
	    # div-video-detail-content end

        # relation tabs
        $options_relations=array();
        $options_relations=$options_head;
        $options_relations['row']=$row;
        $options_relations['videoid']=$row['videoID'];
        $options_relations['info']=$info;
        $options_relations['typeid']=10;
        $options_relations['itemid']=$row['videoID'];
        $options_relations['id']="tabs-video-detail";
        $options_relations['html_class']=$html;
        $options_relations['tab']="tab";
        $options_relations['target']="_blank";
        $options_relations['thumbs_per_line']=400;
        //$options_relations['content_large']=1;
        $options_relations['class']['div-relation-tabs']="div-painting-relation-tabs-exh";
        $options_relations['class']['div-relation-tabs-info']="div-painting-relation-tab-info";
        $options_relations['class']['div-tab-vid-paint']="div-tab-vid-paint";

        $html_print_body.=div_relation_tabs($db,$options_relations);
        # end

}
# END VIDEOS detail page /videos/panorama-1234

# MAIN VIDEO SECTION /videos
elseif( $video_category_view )
{
	$title_meta="";
	if( $_GET['section_1']=="search" )
	{
		$title_meta.=LANG_HEADER_MENU_SEARCH." &raquo; ";
		$search=1;
	}
	else
	{
		if( empty($_GET['section_1']) ) $categoryid=1;

		# videos category
	    if( !empty($_GET['section_1']) ) $query_where_cat=" WHERE titleurl='".$video_category_view."' ";
	    else $query_where_cat=" WHERE categoryid='".$categoryid."' ";

	    $query_cat=QUERIES::query_videos_categories($db,$query_where_cat);
	    $results_cat=$db->query($query_cat['query']);
	    $count_cat=$db->numrows($results_cat);
	    $row_cat=$db->mysql_array($results_cat);
	    $row_cat=UTILS::html_decode($row_cat);
	    $categoryid=$row_cat['categoryid'];
	    $sectionurl=UTILS::row_text_value($db,$row_cat,"titleurl");
	    $title_category=UTILS::row_text_value($db,$row_cat,"title");
	    $desc_cat=UTILS::row_text_value($db,$row_cat,"desc");
	    $title_meta.=$title_category." &raquo; ";
	    if( !$count_cat) UTILS::not_found_404();
	}

	$title_meta.=LANG_HEADER_MENU_VIDEOS;

	# title meta
	$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/videos.css";
	$html->css[]="/css/tabs.css";
	$html->css[]="/css/painting-detail-view.css";
	$html->css[]="/css/page-numbering.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# tabs and tooltips
	//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# meta-tags & head
	$options_head=array();
	$options_head['meta_description']=LANG_META_DESCRIPTION_VIDEOS;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'videos',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$options_left_side['categoryid']=$categoryid;
		if( !empty($row_cat['sub_categoryid']) ) $options_left_side['sub_categoryid']=$row_cat['sub_categoryid'];
		$ul_videos=$html->ul_videos($db,1,$options_left_side);
		$html_print_head.=$ul_videos['return'];
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
		$options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_VIDEOS;
	    if( $search || !empty($title_category) )
	    {
		    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/videos";
		    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_VIDEOS;
		}
	    if( $search )
	    {
	    	$options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
	    }
	    elseif( !empty($title_category) )
	    {
		    $title_category=str_replace("<br/>"," ",$title_category);
		    $options_breadcrumb['items'][1]['inner_html']=$title_category;
		}
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_HEADER_MENU_VIDEOS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/videos/1.jpg' alt='' width='523px' height='208px' />";
            $html_print_body.="</div>\n";

		    # search block
		    $options_search_block=array();
		    $options_search_block['options_head']=$options_head;
		    $html_print_body.=$html->search_form_videos($db,$options_search_block);
		    # END search block

            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block        

	    # prepare search vars
	    $search_vars=UTILS::search_vars_from_get($_GET);
	    # END

	    if( $video_sub_category_view ) $class_section_content="div-category-landing-page-padding-topbottom"; 
	    else $class_section_content="div-category-landing-page-content-quotes";

	    $html_print_body.="<div class='div-section-content ".$class_section_content."'>";

		    if( $search )
		    {
		    	$query_where.=" WHERE v.enable=1 ";

		    	if( !empty($_GET['keyword']) )
		    	{
				    $options_search=array();
				    $options_search['db']=$db;
				    $prepear_search=UTILS::prepear_search($_GET['keyword'],$options_search);
				    # prepare match
				    $options_search_videos=array();
				    $options_search_videos['search']=$prepear_search;
				    $query_match_videos=queries_search::query_search_videos($db,$options_search_videos);
				    $query_where.=" AND ".$query_match_videos;
				    # end prepare match
				}
		    	if( !empty($_GET['categoryid']) )
		    	{
				    $query_where.=" AND v.categoryid='".$_GET['categoryid']."' ";
				}
			    $query_order=" ORDER BY v.categoryid ASC, v.sort ASC ";
			    if( $_GET['sp']!='all' ) $query_limit_video=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp']." ";
			    $query_videos=QUERIES::query_videos($db,$query_where,$query_order,$query_limit_video);
			    if( $_SESSION['debug_page'] ) print $query_videos['query']."<br /><br />";
			    $results_videos_total=$db->query($query_videos['query_without_limit']);
			    $count_videos_total=$db->numrows($results_videos_total);
			    $results_videos=$db->query($query_videos['query']);
			    $count_videos=$db->numrows($results_videos);

			    $pages=@ceil($count_videos_total/$_GET['sp']);

			    if( $count_videos_total )
			    {
			        # page numbering
				    $options_page_numbering=array();
				    $options_page_numbering=$options_head;
				    //$options_page_numbering['search_vars_no_sp']=$search_vars['no_sp'];
				    $options_page_numbering['pages']=$pages;
				    $options_page_numbering['p']=$_GET['p'];
				    $options_page_numbering['sp']=$_GET['sp'];
				    $options_page_numbering['url']=$url;
				    $options_page_numbering['url_after'].="&sp=".$_GET['sp'].$search_vars['no_sp'];
				    $options_page_numbering['url_after_per_page'].=$search_vars['no_sp'];
				    $options_page_numbering['count']=$count_videos_total;
				    $options_page_numbering['class']['div-page-navigation']="";
				    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-bottom";
				    $options_page_numbering['per_page_list']=1;
			        if( $count==1 ) $s="video";
			        else $s="videos";
			        $options_page_numbering['info-text']=$count_videos_total." ".$s." found";
				   	$html_print_body.=$html->page_numbering($db,$options_page_numbering);

			    	# video thumbs
			        $options_videos=array();
			        $options_videos=$options_head;
			        $options_videos['results']=$results_videos;
			        $options_videos['values_from']=0;
			        $options_videos['values_to']=$count_videos;
			        $options_videos['thumbs_per_line']=4;
			        $options_videos['search']=$_GET['keyword'];
			        $options_videos['class']['div-videos-page']="div-videos-page-with-header";
			        $html_print_body.=$html->div_videos($db,$options_videos);

			        # page numbering
			        $options_page_numbering['info-text']="";
				    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-top";
				    $html_print_body.=$html->page_numbering($db,$options_page_numbering);
			    }
			    else
			    {
			        $html_print_body.=LANG_SEARCH_NORESULTS;
			    }
		    }
		    elseif( $video_sub_category_view )
		    {
		    	if( !empty($desc_cat) )
		    	{
		    		$options_wysiwyg=array();
		    		$options_wysiwyg['class']['div_text_wysiwyg']="p-section-description";
					$desc_cat=$html->div_text_wysiwyg($db,$desc_cat,$options_wysiwyg);

		    		$html_print_body.=$desc_cat;
		    	}

	            # print category thumbs
	            $options_cat_thumbs=array();
	            $options_cat_thumbs=$options_head;
	            $options_cat_thumbs['categories']=$ul_videos['sub_categories'];
	            $options_cat_thumbs['count_categories']=$ul_videos['count_sub_categories'];
	            $options_cat_thumbs['class']['div-section-categories']="div-section-categories-quotes";
	            $options_cat_thumbs['url_admin']="/admin/videos/categories/edit/?categoryid=";
	            $options_cat_thumbs['show_all']=1;
	            $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/videos/search/?categoryid=".$categoryid;
	            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
	            $html_print_body.=$return_cat_thumbs['html_print'];
	            # END print category thumbs
		    }
		    else
		    {
	        	if( !empty($desc_cat) ) $html_print_body.="<h2 class='h2-section-title'>".$desc_cat."</h2>";

		        $query_where_video=" WHERE enable=1 AND categoryid='".$categoryid."' ";
		        $query_order_video=" ORDER BY sort ";
		        if( $_GET['sp']!='all' ) $query_limit_video=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp']." ";
		        $query_video=QUERIES::query_videos($db,$query_where_video,$query_order_video,$query_limit_video);
		        $results_video=$db->query($query_video['query']);
		        $count_videos=$db->numrows($results_video);
		        $results_video_total=$db->query($query_video['query_without_limit']);
		        $count_videos_total=$db->numrows($results_video_total);

				$pages=@ceil($count_videos_total/$_GET['sp']);

		        //if( $count_videos>0 ) $html_print_body.="<p class='p-section-title-videos'>".LANG_VIDEO_IN_SECTION1." ".$count_videos." ".LANG_VIDEO_IN_SECTION2."</p>";

		        # page numbering
			    $options_page_numbering=array();
			    $options_page_numbering=$options_head;
			    //$options_page_numbering['search_vars_no_sp']=$search_vars['no_sp'];
			    $options_page_numbering['pages']=$pages;
			    $options_page_numbering['p']=$_GET['p'];
			    $options_page_numbering['sp']=$_GET['sp'];
			    $options_page_numbering['url']=$url;
			    $options_page_numbering['url_after'].="&sp=".$_GET['sp'].$search_vars['no_sp'];
			    $options_page_numbering['url_after_per_page'].=$search_vars['no_sp'];
			    $options_page_numbering['count']=$count_videos_total;
			    $options_page_numbering['class']['div-page-navigation']="";
			    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-bottom";
			    $options_page_numbering['per_page_list']=1;
		        $options_page_numbering['info-text']=LANG_VIDEO_IN_SECTION1." ".$count_videos." ".LANG_VIDEO_IN_SECTION2;
			   	if( $row_cat['sub_categoryid']==4 ) $html_print_body.=$html->page_numbering($db,$options_page_numbering);

			   	# video thumbs
		        $options_videos=array();
		        $options_videos=$options_head;
		        $options_videos['results']=$results_video;
		        $options_videos['values_from']=0;
		        $options_videos['values_to']=$count_videos;
		        $options_videos['thumbs_per_line']=4;
		        $options_videos['desc_text']=LANG_VIDEO_IN_SECTION1." ".$count_videos." ".LANG_VIDEO_IN_SECTION2;
		        if( $row_cat['sub_categoryid']==4 ) $options_videos['class']['div-videos-page']="div-videos-page-with-header";
		        $html_print_body.=$html->div_videos($db,$options_videos);

		        # page numbering
		        $options_page_numbering['info-text']="";
			    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-top";
			    if( $row_cat['sub_categoryid']==4 ) $html_print_body.=$html->page_numbering($db,$options_page_numbering);
			}

		$html_print_body.="</div>";

	#END div main content

}
# *** END ***  MAIN VIDEO SECTION /videos

else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
