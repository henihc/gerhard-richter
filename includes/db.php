<?php
class dbCLASS
{
	protected $db_handle;

    public function __construct($options=array(),$host=DB_HOST, $username=DB_USER, $password=DB_PASSWORD, $db_name=DB_DB)
    {
        //print "here------------";
        //var_dump(debug_backtrace());
        $this->db_handle=mysqli_connect($host,$username,$password,$db_name) or die("Error " . mysqli_error($this->db_handle));

        $utf8=strtolower(str_replace("-", "",DB_ENCODEING)); 
        $this->db_handle->set_charset($utf8);
    }

    public function __destruct() {
        $this->close();
        unset($this->db_handle);
    }

    public function close()
    {
        return mysqli_close($this->db_handle);
    }

	public function query($qry)
	{
        $text_error1="<span style='color:red;font-size:17px;'>Error in the consult..</span><br /><br />";
        $text_error2=$text_error1;
        $text_error2.=$qry;
        $text_error2.="<br /><br />";
        //$query=$this->db_handle->query($qry)  or die($text_error . mysqli_error($this->db_handle));
        $query=$this->db_handle->query($qry);
        if( !$query && IS_STAGING ) 
        {
            print $text_error2 . mysqli_error($this->db_handle);
        }
        elseif( !$query && $_SESSION['debug_page'] )
        {
            print $text_error2 . mysqli_error($this->db_handle);
        }
		return $query;
	}
	
	public function return_insert_id()
	{
		return $this->db_handle->insert_id;
	}

	public function mysql_array($qry,$htmlentities=true)
	{
		
		$array=mysqli_fetch_array($qry);
		
		if( is_array($array) && $htmlentities  )
		{
			foreach( $array as $key => $value )
			{
			    $array[$key]=htmlentities($array[$key],ENT_NOQUOTES,DB_ENCODEING);
			    $array[$key]=UTILS::add_html($array[$key]);
			}
		}
		return $array;
		
	}

	public function numrows($result)
	{
		return $result->num_rows;
	}

    public static function db_replace_quotes2($db,$text)
    {
        $text_replaced=str_replace('"', "&#34;", $text);
        $text_replaced=str_replace("'", "&#39;", $text_replaced);

        return $text_replaced;
    }

    public static function db_replace_quotes($db,$text)
    {
        if ( is_string($text) ) 
        {
            $text_replaced=str_replace('"', "&#34;", $text);
            $text_replaced=str_replace("'", "&#39;", $text_replaced);
            
            return $text_replaced;
        }
        elseif (is_array($text)) 
        {
	        reset($text);
	        while (list($key, $value) = each($text)) 
            {
		        $text[$key] = $db->db_replace_quotes($db,$value);
	        }
	        return $text;
        }
        else
        {
            //return $text;
        }
    }

    public function db_prepare_input($string,$html=0) 
    {
        if( $html==1 ) 
        {
            $string = str_replace('&amp;','&',$string);
        }
        else $string=UTILS::html_decode($string);
        if ( is_string($string) ) 
        {
	        return $this->db_handle->real_escape_string($string);
	    } 
        elseif (is_array($string)) 
        {
	        reset($string);
	        while (list($key, $value) = each($string)) 
            {
		        $string[$key] = $this->db_prepare_input($value);
	        }
	        return $string;
	    } 
        else 
        {
	        return $string;
	    }
    }

    public static function convert_to_html($string)
    {
        $string=str_replace('&', "&amp;", $string);
        $string=str_replace('"', "&#34;", $string);
        $string=str_replace("'", "&#39;", $string);
        
        return $string;
    }

    public function filter_parameters($array,$is_html=0) 
    {
        # Check if the parameter is an array
        if( is_array($array) ) 
        {
            # Loop through the initial dimension
            foreach($array as $key => $value) 
            {
                # Check if any nodes are arrays themselves
                if(is_array($array[$key]))
                {
                    # If they are, let the function call itself over that particular node
                    $array[$key] = $this->filter_parameters($array[$key]);
                }
                # Check if the nodes are strings
                if(is_string($array[$key]))
                {
                    $array[$key] = stripslashes($array[$key]);
                    if( !$is_html ) $array[$key] = $this->convert_to_html($array[$key]);
                    # If they are, perform the real escape function over the selected node
                    $array[$key] = $this->db_prepare_input($array[$key]);
                }
            }           
        }
        
        # Check if the parameter is a string
        if(is_string($array))
        {
            $array = stripslashes($array);
            if( !$is_html ) $array = $this->convert_to_html($array);
            # If it is, perform a  mysql_real_escape_string on the parameter
            $array = $this->db_prepare_input($array);
        }
       
        # Return the filtered result
        return $array;
    }

	public static function db_prepare_search($string) 
    {
    	$string = str_replace("*", "", $string);
    	$string = str_replace("%", "", $string);
    	$string = str_replace("_", "", $string);
    	$string = str_replace("^", "", $string);
    	$string = str_replace("|", "", $string);
    	$string = str_replace("?", "", $string);
    	return $string;
	}

    public static function cut_mysql_stopwords($searchstring)
    {
        $stopwords=array(" a's ", " able ", " about ", " above ", " according ", " accordingly ", " across ", " actually ", " after ", " afterwards ", " again ", " against ", " ain't ", " all ", " allow ", " allows ", " almost ", " alone ", " along ", " already ", " also ", " although ", " always ", " am ", " among ", " amongst ", " an ", " and ", " another ", " any ", " anybody ", " anyhow ", " anyone ", " anything ", " anyway ", " anyways ", " anywhere ", " apart ", " appear ", " appreciate ", " appropriate ", " are ", " aren't ", " around ", " as ", " aside ", " ask ", " asking ", " associated ", " at ", " available ", " away ", " awfully ", " be ", " became ", " because ", " become ", " becomes ", " becoming ", " been ", " before ", " beforehand ", " behind ", " being ", " believe ", " below ", " beside ", " besides ", " best ", " better ", " between ", " beyond ", " both ", " brief ", " but ", " by ", " c'mon ", " c's ", " came ", " can ", " can't ", " cannot ", " cant ", " cause ", " causes ", " certain ", " certainly ", " changes ", " clearly ", " co ", " com ", " come ", " comes ", " concerning ", " consequently ", " consider ", " considering ", " contain ", " containing ", " contains ", " corresponding ", " could ", " couldn't ", " course ", " currently ", " definitely ", " described ", " despite ", " did ", " didn't ", " different ", " do ", " does ", " doesn't ", " doing ", " don't ", " done ", " down ", " downwards ", " during ", " each ", " edu ", " eg ", " either ", " else ", " elsewhere ", " enough ", " entirely ", " especially ", " et ", " etc ", " even ", " ever ", " every ", " everybody ", " everyone ", " everything ", " everywhere ", " ex ", " exactly ", " example ", " except ", " far ", " few ", " fifth ", " first ", " followed ", " following ", " follows ", " for ", " former ", " formerly ", " forth ", " from ", " further ", " furthermore ", " get ", " gets ", " getting ", " given ", " gives ", " go ", " goes ", " going ", " gone ", " got ", " gotten ", " greetings ", " had ", " hadn't ", " happens ", " hardly ", " has ", " hasn't ", " have ", " haven't ", " having ", " he ", " he's ", " hello ", " help ", " hence ", " her ", " here ", " here's ", " hereafter ", " hereby ", " herein ", " hereupon ", " hers ", " herself ", " hi ", " him ", " himself ", " his ", " hither ", " hopefully ", " how ", " howbeit ", " however ", " i'd ", " i'll ", " i'm ", " i've ", " ie ", " if ", " ignored ", " immediate ", " in ", " inasmuch ", " inc ", " indeed ", " indicate ", " indicated ", " indicates ", " inner ", " insofar ", " instead ", " into ", " inward ", " is ", " isn't ", " it ", " it'd ", " it'll ", " it's ", " its ", " itself ", " just ", " keep ", " keeps ", " kept ", " know ", " known ", " knows ", " last ", " lately ", " later ", " latter", " latterly ", " least ", " less ", " lest ", " let ", " let's ", " like ", " liked ", " likely ", " little ", " look ", " looking ", " looks ", " ltd ", " mainly ", " many ", " may ", " maybe ", " me ", " mean ", " meanwhile ", " merely ", " might ", " more ", " moreover ", " most ", " mostly ", " much ", " must ", " my ", " myself ", " name ", " namely ", " nd ", " near ", " nearly ", " necessary ", " need ", " needs ", " neither ", " never ", " nevertheless ", " new ", " next ", " no ", " nobody ", " non ", " none ", " noone ", " nor ", " normally ", " not ", " nothing ", " novel ", " now ", " nowhere ", " obviously ", " of ", " off ", " often ", " oh ", " ok ", " okay ", " old ", " on ", " once ", " one ", " ones ", " only ", " onto ", " or ", " other ", " others ", " otherwise ", " ought ", " our ", " ours ", " ourselves ", " out ", " outside ", " over ", " overall ", " own ", " particular ", " particularly ", " per ", " perhaps ", " placed ", " please ", " plus ", " possible ", " presumably ", " probably ", " provides ", " que ", " quite ", " qv ", " rather ", " rd ", " re ", " really ", " reasonably ", " regarding ", " regardless ", " regards ", " relatively ", " respectively ", " right ", " said ", " same ", " saw ", " say ", " saying ", " says ", " second ", " secondly ", " see ", " seeing ", " seem ", " seemed ", " seeming ", " seems ", " seen ", " self ", " selves ", " sensible ", " sent ", " serious ", " seriously ", " several ", " shall ", " she ", " should ", " shouldn't ", " since ", " so ", " some ", " somebody ", " somehow ", " someone ", " something ", " sometime ", " sometimes ", " somewhat ", " somewhere ", " soon ", " sorry ", " specified ", " specify ", " specifying ", " still ", " sub ", " such ", " sup ", " sure ", " t's ", " take ", " taken ", " tell ", " tends ", " th ", " than ", " thank ", " thanks ", " thanx ", " that ", " that's ", " thats ", " the ", " their ", " theirs ", " them ", " themselves ", " then ", " thence ", " there ", " there's ", " thereafter ", " thereby ", " therefore ", " therein ", " theres ", " thereupon ", " these ", " they ", " they'd ", " they'll ", " they're ", " they've ", " think ", " third ", " this ", " thorough","thoroughly ", " those ", " though ", " through ", " throughout ", " thru ", " thus ", " to ", " together ", " too ", " took ", " toward ", " towards ", " tried ", " tries ", " truly ", " try ", " trying ", " twice ", " un ", " under ", " unfortunately ", " unless ", " unlikely ", " until ", " unto ", " up ", " upon ", " us ", " use ", " used ", " useful ", " uses ", " using ", " usually ", " value ", " various ", " very ", " via ", " viz ", " vs ", " want ", " wants ", " was ", " wasn't ", " way ", " we ", " we'd ", " we'll ", " we're ", " we've ", " welcome ", " well ", " went ", " were ", " weren't ", " what ", " what's ", " whatever ", " when ", " whence ", " whenever ", " where ", " where's ", " whereafter ", " whereas ", " whereby ", " wherein ", " whereupon ", " wherever ", " whether ", " which ", " while ", " whither ", " who ", " who's ", " whoever ", " whole ", " whom ", " whose ", " why ", " will ", " willing ", " wish ", " with ", " within ", " without ", " won't ", " wonder ", " would ", " wouldn't ", " yes ", " yet ", " you ", " you'd ", " you'll ", " you're ", " you've ", " your ", " yours ", " yourself ", " yourselves ", " zero ");

        $tmp=explode(" ",$searchstring);
        $return_string="";
        foreach( $tmp as $word )
        {
            $sql_cutted_out=str_ireplace($stopwords,""," ".$word." ");
            if( !empty($sql_cutted_out) ) $return_string.=$sql_cutted_out." ";
        }

        return $return_string;
    }
}
?>