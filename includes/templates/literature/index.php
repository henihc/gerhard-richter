<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( !empty($_GET['section_2']) ) $options_get['sp']=8;
else $options_get['sp']=20;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body.="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";

        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="});";
        # END tooltips load

        # select box pretty
        $html_print_body.="$('.select-field, .select_pp').selectBoxIt({";
            //$html_print_body.="showFirstOption: false";
        $html_print_body.="});";
        //$html_print_body.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
        //$html_print_body.="selectBox.selectOption(0);";

        # show more less search options
        $html_print_body.="$.fn.toggleText = function(t1, t2){
          if (this.text() == t1) this.text(t2);
          else                   this.text(t1);
          return this;
        };";
        $html_print_body.="$( '.a-right-search-show-more-options' ).click(function() {";
            $html_print_body.="$( '.div-content-header-block-right' ).toggleClass( 'div-content-header-block-right-large' );";
            $html_print_body.="$( this ).toggleClass( 'a-right-search-show-more-options-less' );";
            $html_print_body.="$( this ).toggleText(\"".LANG_LITERATURE_SHOW_LESS_OPTIONS."\", \"".LANG_LITERATURE_SHOW_MORE_OPTIONS."\");";
            $html_print_body.="$( '.div-content-header-block-right-more-options' ).toggleClass( 'show' );";
        $html_print_body.="});";

    $html_print_body.="});";
$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

# Literature detail view /literature/Emphera/Panorama-1234 && /literature/catalogues/solo-exhibitions/panorama-1234
$bookid=UTILS::itemid($db,$_GET['section_2']);
if( !empty($_GET['section_3']) || !empty($bookid) )
{
    if( empty($bookid) )
    {
        $bookid=UTILS::itemid($db,$_GET['section_3']);
        $section=$_GET['section_3'];
    }
    else $section=$_GET['section_2'];
    //$query_where_book=" WHERE id='".$bookid."' AND enable=1 ";
    $query_where_book=" WHERE titleurl='".$section."' AND enable=1 ";
    $query_book=QUERIES::query_books($db,$query_where_book);
    $results_book=$db->query($query_book['query']);
    $count_book=$db->numrows($results_book);
    $row_book=$db->mysql_array($results_book);
    $row_book=UTILS::html_decode($row_book);
    $title_meta.=$row_book['title']." &raquo; ";
    $notes=UTILS::row_text_value2($db,$row_book,"notes");
    $info=UTILS::row_text_value2($db,$row_book,"info");
    if( !$count_book ) UTILS::not_found_404();


        # category sub
        if( !empty($_GET['section_2']) && !empty($_GET['section_3']) )
        {
            $query_category_sub_where=" WHERE enable=1 AND titleurl='".$_GET['section_2']."' ";
            $query_category_sub=QUERIES::query_books_categories($db,$query_category_sub_where);
            $results_category_sub=$db->query($query_category_sub['query']);
            $row_category_sub=$db->mysql_array($results_category_sub);
            $count_category_sub=$db->numrows($results_category_sub);
            $title_category_sub=UTILS::row_text_value($db,$row_category_sub,"title");
            $categoryid_sub=$row_category_sub['books_catid'];
            if( !$count_category_sub ) UTILS::not_found_404();
        }

        # category
        $query_category_where=" WHERE enable=1 AND titleurl='".$_GET['section_1']."' ";
        $query_category=QUERIES::query_books_categories($db,$query_category_where);
        $results_category=$db->query($query_category['query']);
        $count_category=$db->numrows($results_category);
        $row_category=$db->mysql_array($results_category);
        $categoryid=$row_category['books_catid'];
        $title_category=UTILS::row_text_value($db,$row_category,"title");
        if( !$count_category ) UTILS::not_found_404();

    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row_book['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row_book['id']."' ) ORDER BY sort ASC, relationid DESC");
    $count_images=$db->numrows($results_related_image);
    if( $count_images>0 )
    {
        $images_array=array();
        while( $row_related_image=$db->mysql_array($results_related_image) )
        {
            $images_array[]=$row_related_image;
        }
        //$html_print_body.=count($images_array);
        $imageid=UTILS::get_relation_id($db,"17", $images_array[0]);
        //$href="/images/size_xl__imageid_".$imageid.".jpg";
        $href_image=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
        //$src="/images/size_l__imageid_".$imageid.".jpg";
        $src_image=DATA_PATH_DATADIR."/images_new/large/".$imageid.".jpg";
        list($width, $height) = @getimagesize(DATA_PATH."/images_new/large/".$imageid.".jpg");
    }

    # title meta
    $html->title=$title_meta.LANG_HEADER_MENU_LITERATURE." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.css";
    $html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
    $html->css[]="/css/tabs.css";
    $html->css[]="/css/exhibitions.css";
    $html->css[]="/css/books.css";
    $html->css[]="/css/page-numbering.css";
    $html->css[]="/css/painting_detail.css";
    $html->css[]="/css/videos.css";
    $html->css[]="/css/painting-detail-view.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

    # tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.js";

    #expand table
    $html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";
    $html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.js";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

    # history support
    $html->js[]="/js/history/jquery.history.js";

    # meta-tags & head
    $options_head=array();
    if( $main_section ) $options_head['meta_description']=LANG_META_DESCRIPTION_LITERATURE;
    else $options_head['meta_description']=$info;
    $options_head['html_return']=1;
    $options_head['class']['body']="body";

    $options_head['facbook_metainfo']['type']="books.author";
    $options_head['facbook_metainfo']['image']=HTTP_SERVER.$href_image;
    $options_head['facbook_metainfo']['title']=$row_book['title'];
    $options_head['facbook_metainfo']['updated_time']=$row_book['date_modified_admin'];
    $options_head['facbook_metainfo']['book_author']=$row_book['author'];
    $results_isbn=$db->query("SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$row_book['id']."' ORDER BY id ASC LIMIT 1");
    $row_isbn=$db->mysql_array($results_isbn);
    if( $row_isbn['isbn']!="" )
    {
        $og_book_isbn=$row_isbn['isbn'];
    }
    $options_head['facbook_metainfo']['book_isbn']=$og_book_isbn;

    $html_print_head.=$html->head($db,'literature',$options_head);

    # left side blocks
    $html_print_head.="<div id='sub-nav'>";

        # ul literature
        $options_left_side=array();
        $options_left_side=$options_head;
        $options_left_side['categoryid']=$categoryid;
        $options_left_side['categoryid_sub']=$categoryid_sub;
        $ul_literature=$html->ul_literature($db,$_GET['section_1'],$options_left_side);
        $html_print_head.=$ul_literature['return'];

        //print "-".$categoryid;

        # recently added books
        $query_where_books=" WHERE enable=1 AND not_show_recently=0 ";
        if( !empty($categoryid_sub) ) $query_where_books.=" AND books_catid='".$categoryid_sub."' ";
        elseif( !empty($categoryid) ) $query_where_books.=" AND books_catid='".$categoryid."' ";
        $query_order_books=" ORDER BY date_created DESC ";
        $query_books=QUERIES::query_books($db,$query_where_books,$query_order_books," LIMIT 5 ");
        $results_books_recently=$db->query($query_books['query']);
        $count_books_recently=$db->numrows($results_books_recently);

        if( !$count_books_recently )
        {
            $query_where_books=" WHERE enable=1 AND not_show_recently=0 ";
            $i=0;
            while( $row_category_sub=$db->mysql_array($results_category_sub) )
            {
                //print_r($row_category_sub);
                $i++;
                if( $i==1 )
                {
                    $query_where_books.=" AND (";
                }
                else
                {
                    $query_where_books.=" OR ";
                }
                $query_where_books.=" books_catid='".$row_category_sub['books_catid']."' ";
                if( $i==$count_categories_sub )
                {
                    $query_where_books.=" ) ";
                }
            }
            $query_order_books=" ORDER BY date_created DESC ";
            $query_books=QUERIES::query_books($db,$query_where_books,$query_order_books," LIMIT 5 ");
            //print $query_books['query'];
            $results_books_recently=$db->query($query_books['query']);
            $count_books_recently=$db->numrows($results_books_recently);
        }

        $options_related=array();
        $options_related=$options_head;
        $options_related['bookid']=1;
        $options_related['title_column']="title";
        $options_related['title']=LANG_RELATED_BLOCK_RECENTLY;
        $options_related['results']=$results_books_recently;
        $options_related['count']=$count_books_recently;
        $options_related['target']=1;
        $html_print_head.=$html->div_related_block($db,$options_related);

    $html_print_head.="</div>";

    # div main content


        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/literature";
        $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_LITERATURE;
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_LITERATURE;
        if( $count_book )
        {
            $href="/".$_GET['lang']."/literature";
            if( $count_category_sub )
            {
                $href.="/".$row_category['titleurl']."/".$row_category_sub['titleurl'];
                $title_category=$title_category_sub;
            }
            else
            {
                $href.="/".$row_category['titleurl'];
            }
            $options_breadcrumb['items'][1]['href']=$href;
            $options_breadcrumb['items'][1]['title']=$title_category;
            $options_breadcrumb['items'][1]['inner_html']=$title_category;

            $options_breadcrumb['items'][2]['length']=70;
            $options_breadcrumb['items'][2]['inner_html']=$row_book['title'];
        }
        $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
        $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

        # exhibition div_detail_view_item
        $options_item=array();
        $options_item=$options_head;
        $options_item['row']=$row_book;
        $options_item['count_images']=$count_images;
        $options_item['height']=$height;
        $options_item['width']=$width;
        $options_item['href_image']=$href_image;
        $options_item['src_image']=$src_image;
        $options_item['images_array']=$images_array;
        $options_item['title']=$row_book['title'];
        $options_item['url_admin_edit']="/admin/books/edit/?bookid=".$row_book['id'];
        $options_item['class']['h1-item']="h1-item-book";
        $options_item['class']['div-detail-view-item-right']="div-detail-view-item-left-literature";
        $options_item['class']['div-detail-view-item-right']="div-detail-view-item-right-literature";
        $html_print_body.=$html->div_detail_view_item_start($db,$options_item);

            $html_print_body.="<table>";
                # Author
                if( !empty($row_book['author']) )
                {
                    $html_print_body.="<tr>";
                        $html_print_body.="<td class='td-title'>".LANG_BOOKS_BY."</td>";
                        $html_print_body.="<td>".$row_book['author']."</td>";
                    $html_print_body.="</tr>";
                }
                # Date
                if( !empty($row_book['date_display_year']) )
                {
                    $html_print_body.="<tr>";
                        $html_print_body.="<td class='td-title'>".LANG_LIT_SEARCH_DATE."</td>";
                        $html_print_body.="<td>".$row_book['date_display_year']."</td>";
                    $html_print_body.="</tr>";
                }
                # Publisher
                # for essays where there is no publisher do not show unknown
                if( $row_book['books_catid']==19 && empty($row_book['publisher']) )
                {

                }
                elseif( !empty($row_book['publisher']) )
                {
                    $html_print_body.="<tr>";
                        $html_print_body.="<td class='td-title td-title-".$_GET['lang']."'>".LANG_LIT_SEARCH_PUBBY."</td>";
                        $html_print_body.="<td>".$row_book['publisher']."</td>";
                    $html_print_body.="</tr>";
                }
                # Editor
                if( !empty($row_book['editor']) )
                {
                    $html_print_body.="<tr>";
                        $html_print_body.="<td class='td-title td-title-".$_GET['lang']."'>".LANG_LIT_SEARCH_EDITOR."</td>";
                        $html_print_body.="<td>".$row_book['editor']."</td>";
                    $html_print_body.="</tr>";
                }
                # Details
                if( !empty($row_book['coverid']) || $row_book['NOpages']!=0 )
                {
                    $html_print_body.="<tr>";
                        $html_print_body.="<td class='td-title'>".LANG_BOOKS_DETAILS."</td>";
                        $html_print_body.="<td>";
                            if( !empty($row_book['coverid']) )
                            {
                                $i=0;
                                $coverids=explode(",", $row_book['coverid']);
                                $cover="";
                                foreach( $coverids AS $coverid )
                                {
                                    $options_cover=array();
                                    $options_cover['coverid']=$coverid;
                                    if( $i>0 ) $cover.=", ";
                                    $cover.=UTILS::get_cook_cover($db,$options_cover);
                                    $i++;
                                }
                                $html_print_body.=$cover;
                            }
                            if( $row_book['NOpages']!=0 )
                            {
                                if( !empty($row_book['coverid']) ) $html_print_body.=", ";
                                $html_print_body.=$row_book['NOpages']." ".LANG_LIT_SEARCH_PAGES;
                            }
                        $html_print_body.="</td>";
                    $html_print_body.="</tr>";
                }
                # ISBN
                $display_lang=0;
                $languages=array();
                $results_isbn=$db->query("SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$row_book['id']."' ORDER BY id ASC");
                while( $row_isbn=$db->mysql_array($results_isbn) )
                {
                    //if( $row_isbn['isbn']=="" ) $row_isbn['isbn']=LANG_LIT_SEARCH_ISBNNOTAVA;
                    if( $row_isbn['isbn']!="" )
                    {
                        $html_print_body.="<tr>\n";
                            $html_print_body.="<td class='td-title'>\n";
                                if( $row_book['books_catid']==6 || $row_book['books_catid']==20 || $row_book['books_catid']==12 ) $html_print_body.=LANG_LITERATURE_ISSN;
                                else $html_print_body.=LANG_LITERATURE_ISBN;
                            $html_print_body.="</td>\n";
                            $html_print_body.="<td>\n";
                                $html_print_body.=$row_isbn['isbn'];
                            $html_print_body.="</td>\n";
                        $html_print_body.="</tr>\n";
                    }
                    if( !empty($row_isbn['language']) || !empty($row_isbn['language2']) || !empty($row_isbn['language3']) )
                    {
                        $display_lang=1;
                        $query_where_language=" WHERE languageid='".$row_isbn['language']."' ";
                        $query_language=QUERIES::query_books_languages($db,$query_where_language);
                        $results_language=$db->query($query_language['query']);
                        $row_language=$db->mysql_array($results_language);
                        $row_language=UTILS::html_decode($row_language);
                        $title_language=UTILS::row_text_value($db,$row_language,"title");
                        $query_where_language=" WHERE languageid='".$row_isbn['language2']."' ";
                        $query_language=QUERIES::query_books_languages($db,$query_where_language);
                        $results_language=$db->query($query_language['query']);
                        $row_language=$db->mysql_array($results_language);
                        $row_language=UTILS::html_decode($row_language);
                        $title_language2=UTILS::row_text_value($db,$row_language,"title");
                        $query_where_language=" WHERE languageid='".$row_isbn['language3']."' ";
                        $query_language=QUERIES::query_books_languages($db,$query_where_language);
                        $results_language=$db->query($query_language['query']);
                        $row_language=$db->mysql_array($results_language);
                        $row_language=UTILS::html_decode($row_language);
                        $title_language3=UTILS::row_text_value($db,$row_language,"title");

                        $count3=0;
                        if( !empty($row_isbn['language']) ) $count3++;
                        if( !empty($row_isbn['language2']) ) $count3++;
                        if( !empty($row_isbn['language3']) ) $count3++;

                        if( !empty($row_isbn['language']) )
                        {
                            $languages[$title_language]=$title_language;
                        }
                        if( !empty($row_isbn['language2']) )
                        {
                            $languages[$title_language2]=$title_language2;
                        }
                        if( !empty($row_isbn['language3']) )
                        {
                            $languages[$title_language3]=$title_language3;
                        }
                    }
                }
                # Languages
                if( $display_lang && !empty($languages) )
                {
                    $html_print_body.="<tr>\n";
                        $html_print_body.="<td class='td-title'>\n";
                            if( count($languages)==1 ) $language=LANG_BOOKS_LANGUAGE;
                            else $language=LANG_BOOKS_LANGUAGES;
                            $html_print_body.=$language;
                        $html_print_body.="</td>\n";
                        $html_print_body.="<td>\n";
                            $i=0;
                            foreach ($languages as $language)
                            {
                                if( $i!=0 ) $html_print_body.=" / ";
                                $html_print_body.=$language;
                                $i++;
                            }
                        $html_print_body.="</td>\n";
                    $html_print_body.="</tr>\n";
                }
                # Notes
                if( !empty($notes) )
                {
                    $html_print_body.="<tr>\n";
                        $html_print_body.="<td class='td-title'>\n";
                            $html_print_body.=LANG_NOTES;
                        $html_print_body.="</td>\n";
                        $html_print_body.="<td>\n";
                            $html_print_body.="<div class='div_text_wysiwyg'>";
                                $html_print_body.=$notes;
                            $html_print_body.="</div>\n";
                        $html_print_body.="</td>\n";
                    $html_print_body.="</tr>\n";
                }
            $html_print_body.="</table>";

        $html_print_body.=$html->div_detail_view_item_end($db,$options_item);
        # END exhibition div_detail_view_item

        # relation tabs
        $options_relations=array();
        $options_relations=$options_head;
        $options_relations['row']=$row_book;
        $options_relations['bookid']=$row_book['id'];
        $options_relations['info']=$info;
        $options_relations['typeid']=12;
        $options_relations['itemid']=$row_book['id'];
        $options_relations['id']="div-painting-relation-tabs";
        $options_relations['html_class']=$html;
        $options_relations['content_large']=1;
        $options_relations['thumb_per_line_videos']=4;
        $options_relations['thumbs_per_line']=4; # for artwork tab
        $options_relations['tab']="tab";
        $options_relations['class']['div-relation-tabs']="div-painting-relation-tabs-paint-detail";
        $options_relations['class']['div-relation-tabs-info']="div-book-detail-tab-info";
        $options_relations['class']['p-section-desc-text']="p-literature-section-desc-text";
        $html_print_body.=div_relation_tabs($db,$options_relations);
        # end

}
# END Literature detail view /literature/Emphera/Panorama-1234

# MAIN LITERATURE SECTION /literature && cat view && search
else
{
    if( !empty($_GET['sort']) )
    {
        $tmp=explode(",", $_GET['sort']);
        $_GET['sort']=$tmp[0];
        if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
        else $_GET['sort_how']=1;
    }
    else
    {
        //$_GET['sort']=3;
        $_GET['sort']=4;
        $sort="link_date";
        $_GET['sort_how']=-1;
        $sort_how="DESC";
    }

    # sort
    if( $_GET['order_by']=="title" ) $_GET['sort']=1;
    elseif( $_GET['order_by']=="author" ) $_GET['sort']=2;
    elseif( $_GET['order_by']=="link_date" || $_GET['order_by']=="year" ) $_GET['sort']=3;
    /*
    elseif( $_GET['order_by']=="books_catid" ) $_GET['sort']=3;
    elseif( $_GET['order_by']=="link_date" || $_GET['order_by']=="year" ) $_GET['sort']=4;
    */

    if( $_GET['sort']==1 ) $sort="title";
    elseif( $_GET['sort']==2 ) $sort="author";
    elseif( $_GET['sort']==3 ) $sort="link_date";
    /*
    elseif( $_GET['sort']==3 ) $sort="books_catid";
    elseif( $_GET['sort']==4 ) $sort="link_date";
    */
    # sort how
    if( $_GET['order_how']=="DESC" ) $_GET['sort_how']=-1;
    elseif( $_GET['order_how']=="ASC" ) $_GET['sort_how']=1;
    if( $_GET['sort_how']==1 ) $sort_how="ASC";
    elseif( $_GET['sort_how']==-1 ) $sort_how="DESC";

    $url="&amp;sort=".$_GET['sort'].",".$_GET['sort_how'];
    # END sort

    # literature search
    if( $_GET['section_1']=="search" )
    {
        $title_meta.=LANG_HEADER_MENU_SEARCH." &raquo; ";
        $search=1;
    }

    # literature category view
    elseif( !empty($_GET['section_1']) )
    {
        # category sub
        if( !empty($_GET['section_2']) )
        {
            $query_category_sub_where=" WHERE enable=1 AND titleurl='".$_GET['section_2']."' ";
            $query_category_sub=QUERIES::query_books_categories($db,$query_category_sub_where);
            $results_category_sub=$db->query($query_category_sub['query']);
            $row_category_sub=$db->mysql_array($results_category_sub);
            $count_category_sub=$db->numrows($results_category_sub);
            $title_category_sub=UTILS::row_text_value($db,$row_category_sub,"title");
            $categoryid_sub=$row_category_sub['books_catid'];
            $title_meta.=$title_category_sub." &raquo; ";
            if( !$count_category_sub ) UTILS::not_found_404();
            $books_catid=$categoryid_sub;
        }

        # category
        $query_category_where=" WHERE enable=1 AND titleurl='".$_GET['section_1']."' ";
        $query_category=QUERIES::query_books_categories($db,$query_category_where);
        $results_category=$db->query($query_category['query']);
        $row_category=$db->mysql_array($results_category);
        $count_category=$db->numrows($results_category);
        $title_category=UTILS::row_text_value($db,$row_category,"title");
        if( !$count_category_sub ) $title_meta.=$title_category." &raquo; ";
        $categoryid=$row_category['books_catid'];
        //print $categoryid;
        if( empty($books_catid) ) $books_catid=$categoryid;
        if( !$count_category ) UTILS::not_found_404();

        if( !empty($_GET['section_3']) ) UTILS::not_found_404();


        $query_category_sub_where=" WHERE enable=1 AND sub_books_catid='".$books_catid."' ";
        $query_category_sub_order=" ORDER BY sort ";
        $query_category_sub=QUERIES::query_books_categories($db,$query_category_sub_where,$query_category_sub_order);
        $results_category_sub=$db->query($query_category_sub['query']);
        $count_categories_sub=$db->numrows($results_category_sub);
        //print $count_categories_sub;
    }
    # main literature section
    else
    {
        $main_section=1;
    }

    $title_meta.=LANG_HEADER_MENU_LITERATURE;

    # title meta
    $html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.css";
    $html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
    $html->css[]="/css/tabs.css";
    $html->css[]="/css/exhibitions.css";
    $html->css[]="/css/books.css";
    $html->css[]="/css/page-numbering.css";
    $html->css[]="/css/painting_detail.css";
    $html->css[]="/css/videos.css";
    $html->css[]="/css/painting-detail-view.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

    # tabs and tooltips
    $html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.js";

    #expand table
    $html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";
    $html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.js";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

    # history support
    $html->js[]="/js/history/jquery.history.js";

    # meta-tags & head
    $options_head=array();
    if( $main_section ) $options_head['meta_description']=LANG_META_DESCRIPTION_LITERATURE;
    $options_head['html_return']=1;
    $options_head['class']['body']="body";
    $options_head['class']['div-main-literature']="div-main-literature";
    $html_print_head.=$html->head($db,'literature',$options_head);

    # left side blocks
    $html_print_head.="<div id='sub-nav'>";

        # ul literature
        $options_left_side=array();
        $options_left_side=$options_head;
        $options_left_side['categoryid']=$categoryid;
        $options_left_side['categoryid_sub']=$categoryid_sub;
        $ul_literature=$html->ul_literature($db,$_GET['section_1'],$options_left_side);
        $html_print_head.=$ul_literature['return'];

        //print "-".$categoryid;

        # recently added books
        $query_where_books=" WHERE enable=1 AND not_show_recently=0 ";
        if( !empty($categoryid_sub) ) $query_where_books.=" AND books_catid='".$categoryid_sub."' ";
        elseif( !empty($categoryid) ) $query_where_books.=" AND books_catid='".$categoryid."' ";
        $query_order_books=" ORDER BY date_created DESC ";
        $query_books=QUERIES::query_books($db,$query_where_books,$query_order_books," LIMIT 5 ");
        $results_books_recently=$db->query($query_books['query']);
        $count_books_recently=$db->numrows($results_books_recently);
        if( !$count_books_recently )
        {
            $query_where_books=" WHERE enable=1 AND not_show_recently=0 ";
            $i=0;
            while( $row_category_sub=$db->mysql_array($results_category_sub) )
            {
                //print_r($row_category_sub);
                $i++;
                if( $i==1 )
                {
                    $query_where_books.=" AND (";
                }
                else
                {
                    $query_where_books.=" OR ";
                }
                $query_where_books.=" books_catid='".$row_category_sub['books_catid']."' ";
                if( $i==$count_categories_sub )
                {
                    $query_where_books.=" ) ";
                }
            }
            $query_order_books=" ORDER BY date_created DESC ";
            $query_books=QUERIES::query_books($db,$query_where_books,$query_order_books," LIMIT 5 ");
            //print $query_books['query'];
            $results_books_recently=$db->query($query_books['query']);
            $count_books_recently=$db->numrows($results_books_recently);
        }

        $options_related=array();
        $options_related=$options_head;
        $options_related['bookid']=1;
        $options_related['title_column']="title";
        $options_related['title']=LANG_RELATED_BLOCK_RECENTLY;
        $options_related['results']=$results_books_recently;
        $options_related['count']=$count_books_recently;
        $options_related['target']=1;
        $html_print_head.=$html->div_related_block($db,$options_related);

    $html_print_head.="</div>";

    # div main content

        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/literature";
        $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_LITERATURE;
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_LITERATURE;
        if( $search )
        {
            $options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
        }
        else
        {
            if( $count_category_sub )
            {
                $options_breadcrumb['items'][2]['inner_html']=$title_category_sub;
            }
            elseif( !empty($title_category) )
            {
                $options_breadcrumb['items'][1]['inner_html']=$title_category;
            }
        }
        if( !empty($_GET['section_1']) )
        {
            $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
            $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
        }
        else
        {
            $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
            $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
        }
        //print_r($_GET);
        //print_r($options_breadcrumb);
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                if( !empty($title_category_sub) ) $h1_title=$title_category_sub;
                elseif( !empty($title_category) ) $h1_title=$title_category;
                else $h1_title=LANG_HEADER_MENU_LITERATURE;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/literature_new/".rand(1,2).".jpg' alt='' width='523' height='208' />";
            $html_print_body.="</div>\n";

            # search block
            $options_search_block=array();
            $options_search_block['options_head']=$options_head;
            $html_print_body.=$html->search_form_literature($db,$options_search_block);
            # END search block

            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

        # lit main section /literature
        if( $main_section )
        {
            $class_landing_page="div-category-landing-page-padding-topbottom";
        }
        if( $search )
        {
            $class_section_content="";
        }
        else
        {
            $class_section_content="div-section-content";
        }

        $html_print_body.="<div class='".$class_section_content." ".$class_landing_page."'>";
            if( $main_section )
            {
                $html_print_body.="<p class='p-section-description'>".LANG_LITERATURE_DESC_TEXT."</p>";

                # print category thumbs
                $options_cat_thumbs=array();
                $options_cat_thumbs=$options_head;
                $options_cat_thumbs['categories']=$ul_literature['categories'];
                $options_cat_thumbs['count_categories']=$ul_literature['count_categories'];
                $options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
                $options_cat_thumbs['url_admin']="/admin/books/categories/edit/?books_catid=";
                $options_cat_thumbs['show_all']=1;
                $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/literature/search/?info=1";
                $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
                $html_print_body.=$return_cat_thumbs['html_print'];
                # END print category thumbs

                $html_print_body.="<div class='div-literature-featured-titles'>";
                    $html_print_body.="<h3 class='h3-section-title'>".LANG_RIGHT_FETURED_TITLES."</h3>\n";
                    $query_featured_where=" WHERE selectedTitle=true AND enable=1 ";
                    $query_featured_order=" ORDER BY sort ";
                    $query_featured=QUERIES::query_books($db,$query_featured_where,$query_featured_order);
                    $results_featured=$db->query($query_featured['query']);
                    $count_featured=$db->numrows($results_featured);

                    $featured_titles=array();
                    while( $row_featured=$db->mysql_array($results_featured) )
                    {
                        $featured_titles[]=$row_featured;
                    }

                    $i=0;
                    $ii=0;
                    foreach( $featured_titles as $row_featured )
                    {
                        $rand=mt_rand(0,($count_featured-1));
                        $row_featured=$featured_titles[$rand];
                        unset($featured_titles[$rand]);
                        $count_featured=$count_featured-1;
                        $featured_titles=array_values($featured_titles);
                        $i++;
                        $ii++;
                        if( $ii<=4 )
                        {
                            if( $ii==$count_featured || $i==4 )
                            {
                                $class_last="div-literature-featured-titles-last";
                                $i=0;
                            }
                            else $class_last="";
                            if( $ii<=4  )
                            {
                                $class_first_row="div-literature-featured-titles-first-row";
                            }
                            else $class_first_row="";
                            $options_featured_book_url=array();
                            $options_featured_book_url['bookid']=$row_featured['id'];
                            $options_featured_book_url['new']=1;
                            $url_literature=UTILS::get_literature_url($db,$options_featured_book_url);
                            $html_print_body.="<div class='div-literature-featured-title ".$class_last." ".$class_first_row."'>";
                                $html_print_body.="<a href='".$url_literature['url']."' title='".$row_featured['title']."' class='a-featured-list'>";
                                    $html_print_body.="<span class='span-featured-list'>";
                                        $html_print_body.=$row_featured['title'];
                                    $html_print_body.="</span>";
                                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row_featured['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row_featured['id']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                                    $row_related_image=$db->mysql_array($results_related_image);
                                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                                    //$src="/images/size_s__imageid_".$imageid.".jpg";
                                    $src=DATA_PATH_DATADIR."/images_new/small/".$imageid.".jpg";
                                    $html_print_body.="<div class='div-literature-featured-title-img'>";
                                        $html_print_body.="<img src='".$src."' alt=''/>";
                                    $html_print_body.="</div>";
                                $html_print_body.="</a>";
                                $options_admin_edit=array();
                                $options_admin_edit=$options_head;
                                $html_print_body.=UTILS::admin_edit("/admin/books/edit/?bookid=".$row_featured['id'],$options_admin_edit)."\n";
                            $html_print_body.="</div>";
                        }
                    }
                    $html_print_body.="<div class='clearer'></div>";
                $html_print_body.="</div>";
            }
            else
            {
                if( $count_categories_sub )
                {
                    $count_category=1;
                }

                $options_query_literature=array();
                $query_literature=QUERIES::query_books($db,"","","","",$options_query_literature);

                $query="SELECT ".$query_literature['query_columns']." FROM ".TABLE_BOOKS." b ";
                if( !empty($_GET['languageid']) ) $query.=", ".TABLE_BOOKS_ISBN." bi ";

                # SEARCH
                if( $search || $count_category )
                {
                    $query.=" WHERE b.enable=1 ";
                    $url_search="";

                    # author
                    if( !empty($_GET['author']) )
                    {
                        # prepare search author
                        $options_search=array();
                        $options_search['db']=$db;
                        $prepear_search=UTILS::prepear_search($_GET['author'],$options_search);
                        # END prepare search author

                        # prepare match
                        $options_search_literature_author=array();
                        $options_search_literature_author['search']=$prepear_search;
                        $query_match_literature_author=QUERIES_SEARCH::query_search_literature_author($db,$options_search_literature_author);
                        $query.=" AND ".$query_match_literature_author;
                        # END prepare match

                        $url_search.="&amp;author=".$_GET['author'];
                    }
                    # end author

                    # title
                    $and=" AND ";
                    if( !empty($_GET['title']) )
                    {
                        # prepare search title
                        $options_search=array();
                        $options_search['db']=$db;
                        $prepear_search=UTILS::prepear_search($_GET['title'],$options_search);
                        # END prepare search title

                        # prepare match
                        $options_search_literature_title=array();
                        $options_search_literature_title['search']=$prepear_search;
                        $query_match_literature_title=QUERIES_SEARCH::query_search_literature_title($db,$options_search_literature_title);
                        $query.=$and.$query_match_literature_title;
                        # END prepare match

                        $url_search.="&amp;title=".$_GET['title'];
                    }
                    # end title

                    # keyword
                    $and=" AND ";
                    if( !empty($_GET['keyword']) )
                    {
                        # prepare search keyword
                        $options_search=array();
                        $options_search['db']=$db;
                        $prepear_search=UTILS::prepear_search($_GET['keyword'],$options_search);
                        # END prepare search keyword

                        # prepare match
                        $options_search_literature_keyword=array();
                        $options_search_literature_keyword['search']=$prepear_search;
                        $query_match_literature_keyword=QUERIES_SEARCH::query_search_literature_keyword($db,$options_search_literature_keyword);
                        $query.=$and." ( ".$query_match_literature_keyword;
                        # END prepare match

                        # search isbn numbers
                        $results_isbn=$db->query("SELECT bookID FROM ".TABLE_BOOKS_ISBN." WHERE isbn='".$_GET['keyword']."' ");
                        $count_isbn=$db->numrows($results_isbn);
                        if( $count_isbn )
                        {
                            $i=0;
                            $query.=" OR ( ";
                            while( $row_isbn=$db->mysql_array($results_isbn) )
                            {
                                if( $i>0 ) $or=" OR ";
                                else $or="";
                                $query.=$or." b.id='".$row_isbn['bookID']."'";
                                $i++;
                            }
                            $query.=" ) ";
                        }
                        # END

                        preg_match("/^(19|20)\d{2}$/", $_GET['keyword'], $matches_years);
                        $search_year=$matches_years[0];
                        if( !empty($search_year) ) $query.=" OR DATE_FORMAT(link_date, '%Y') BETWEEN '".$search_year."' AND '".$search_year."' ";

                        # searching for linked exhibitions
                        $options_search_exh=array();
                        $options_search_exh['search']=$prepear_search;
                        $query_match_exh=QUERIES_SEARCH::query_search_exhibitions($db,$options_search_exh);
                        $results_exh=$db->query("SELECT e.exID FROM ".TABLE_EXHIBITIONS." e WHERE ".$query_match_exh);
                        $count_exh=$db->numrows($results_exh);
                        if( $count_exh>0 )
                        {
                            while( $row_exh=$db->mysql_array($results_exh) )
                            {
                                $locations_exhibitions[$row_exh['exID']]=$row_exh['exID'];
                            }
                        }
                        //print_r($locations_exhibitions);
                        //print $count_exh;
                        # searching for linked exhibitions

                        # SEARCH
                        if( !empty($_GET['title']) || !empty($_GET['location']) || !empty($_GET['keyword']) )
                        {
                            $url_search="";

                            $query_where_exhibitions.=" WHERE enable=1 AND ( ";

                            # add years from TITLE search
                            if( !empty($_GET['title']) || !empty($_GET['keyword']) )
                            {
                                $keyword_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['keyword']));
                                $title_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['title']));
                                if( !empty($_GET['keyword']) )
                                {
                                    $title_search_term=$_GET['keyword'];
                                }
                                else
                                {
                                    $title_search_term=$_GET['title'];
                                }

                                if( !empty($_GET['title']) ) $url_search.="&amp;title=".$title_for_url;
                                if( !empty($_GET['keyword']) ) $url_search.="&amp;keyword=".$keyword_for_url;

                                # prepare search keyword
                                $options_search=array();
                                $options_search['db']=$db;
                                $prepear_search=UTILS::prepear_search($title_search_term,$options_search);
                                # END prepare search keyword

                                # prepare match
                                $options_search_exh=array();
                                $options_search_exh['search']=$prepear_search;
                                $query_match_exh=QUERIES_SEARCH::query_search_exhibitions($db,$options_search_exh);
                                $query_search_title=" ".$query_match_exh;
                                # END prepare match

                                $query_where_exhibitions.=$query_search_title;
                            }
                            #end

                            # add years from location search
                            if( !empty($_GET['location']) || !empty($_GET['keyword']) )
                            {

                                if( !empty($_GET['keyword']) )
                                {
                                    $location_search_term=$_GET['keyword'];
                                    $and=" OR ";
                                }
                                else
                                {
                                    $location_search_term=$_GET['location'];
                                    $location_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['location']));
                                    $url_search.="&amp;location=".$location_for_url;
                                    if( !empty($_GET['title']) || !empty($_GET['keyword']) ) $and=" AND ";
                                }

                                #LOCATION

                                # prepare search keyword
                                $options_search=array();
                                $options_search['db']=$db;
                                $prepear_search=UTILS::prepear_search($location_search_term,$options_search);
                                # END prepare search keyword

                                # prepare match
                                $options_search_locations=array();
                                $options_search_locations['search']=$prepear_search;
                                $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$options_search_locations);
                                $query_where_locations=" WHERE ".$query_match_locations;
                                # END prepare match

                                $query_locations=QUERIES::query_locations($db,$query_where_locations);
                                $results_locations=$db->query($query_locations['query_without_limit']);
                                $count_locations=$db->numrows($results_locations);
                                //print $count_locations;
                                if( $count_locations>0 )
                                {
                                    while( $row_locations=$db->mysql_array($results_locations) )
                                    {
                                        $results_loc_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE locationid='".$row_locations['locationid']."'");
                                        $count_loc_exh=$db->numrows($results_loc_exh);
                                        if( $count_loc_exh>0 )
                                        {
                                            while( $row_loc_exh=$db->mysql_array($results_loc_exh) )
                                            {
                                                $locations_exhibitions[$row_loc_exh['exID']]=$row_loc_exh['exID'];
                                            }
                                        }
                                    }
                                }


                                # LOCATION CITY

                                # prepare match
                                $options_search_locations_city=array();
                                $options_search_locations_city['search']=$prepear_search;
                                $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$options_search_locations_city);
                                $query_where_locations_city=" WHERE ".$query_match_locations_city;
                                # END prepare match

                                $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
                                $results_locations_city=$db->query($query_locations_city['query_without_limit']);
                                $count_locations_city=$db->numrows($results_locations_city);
                                //print $count_locations_city;
                                if( $count_locations_city>0 )
                                {
                                    while( $row_locations_city=$db->mysql_array($results_locations_city) )
                                    {
                                        $results_loc_city_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE cityid='".$row_locations_city['cityid']."'");
                                        $count_loc_city_exh=$db->numrows($results_loc_city_exh);
                                        if( $count_loc_city_exh>0 )
                                        {
                                            while( $row_loc_city_exh=$db->mysql_array($results_loc_city_exh) )
                                            {
                                                $locations_exhibitions[$row_loc_city_exh['exID']]=$row_loc_city_exh['exID'];
                                            }
                                        }
                                    }
                                }


                                # LOCATION COUNTRY

                                # prepare match
                                $options_search_locations_country=array();
                                $options_search_locations_country['search']=$prepear_search;
                                $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$options_search_locations_country);
                                $query_where_locations_country=" WHERE ".$query_match_locations_country;
                                # END prepare match

                                $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
                                $results_locations_country=$db->query($query_locations_country['query_without_limit']);
                                $count_locations_country=$db->numrows($results_locations_country);
                                //print $count_locations_country;
                                if( $count_locations_country>0 )
                                {
                                    while( $row_locations_country=$db->mysql_array($results_locations_country) )
                                    {
                                        $results_loc_country_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE countryid='".$row_locations_country['countryid']."'");
                                        $count_loc_country_exh=$db->numrows($results_loc_country_exh);
                                        if( $count_loc_country_exh>0 )
                                        {
                                            while( $row_loc_country_exh=$db->mysql_array($results_loc_country_exh) )
                                            {
                                                $locations_exhibitions[$row_loc_country_exh['exID']]=$row_loc_country_exh['exID'];
                                            }
                                        }
                                    }
                                }

                                if( !empty($locations_exhibitions) )
                                {
                                    $query_where_exhibitions.=$and." ( ";
                                    $i_loc_exh=0;
                                    foreach( $locations_exhibitions as $exhibitionid )
                                    {
                                        if( $i_loc_exh==0 ) $or_loc="";
                                        else $or_loc=" OR ";
                                        $query_where_exhibitions.=$or_loc." exID='".$exhibitionid."' ";
                                        $i_loc_exh++;
                                    }
                                    $query_where_exhibitions.=" ) ";
                                }
                                elseif( !empty($_GET['location']) )
                                {
                                    $query_where_exhibitions.=$and." exID=-1 ";
                                }

                            }
                            # END add years from location search

                            //print_r($locations_exhibitions);

                            $query_where_exhibitions.=" ) ";


                            $query_exhibitions=QUERIES::query_exhibition($db,$query_where_exhibitions);
                            $results_exhibitions=$db->query($query_exhibitions['query_without_limit']);
                            $count_exhibitions=$db->numrows($results_exhibitions);
                            if( $count_exhibitions )
                            {
                                $i=0;
                                $ii=0;
                                $query_books="";
                                while($row_exhibitions=$db->mysql_array($results_exhibitions))
                                {
                                    $results_literature=$db->query("SELECT b.id FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibitions['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibitions['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ");
                                    $count_literature=$db->numrows($results_literature);
                                    if( $count_literature )
                                    {
                                        while( $row_literature=$db->mysql_array($results_literature) )
                                        {
                                            if( empty($query_books) ) $query_books.=" OR ( ";
                                            if( $ii==0 ) $or="";
                                            else $or="OR";
                                            $query_books.=$or." b.id='".$row_literature['id']."' ";
                                            $ii++;
                                        }
                                    }
                                    $i++;
                                }
                                if( !empty($query_books) )
                                {
                                    $query_books.=" ) ";
                                    $query.=$query_books;
                                }
                            }

                            $query.=" ) ";
                        }
                        # END searching for linked exhibitions

                    }
                    # end keyword

                    # ISBN
                    $and=" AND ";
                    if( !empty($_GET['isbn']) )
                    {
                        $results_isbn=$db->query("SELECT bookID FROM ".TABLE_BOOKS_ISBN." WHERE isbn='".$_GET['isbn']."' ");
                        $count_isbn=$db->numrows($results_isbn);
                        if( $count_isbn )
                        {
                            $i=0;
                            $query.=$and." ( ";
                            while( $row_isbn=$db->mysql_array($results_isbn) )
                            {
                                if( $i>0 ) $or=" OR ";
                                else $or="";
                                $query.=$or." b.id='".$row_isbn['bookID']."'";
                                $i++;
                            }
                            $query.=" ) ";
                        }
                        else
                        {
                            $query.=$and." b.id=-1 ";
                        }
                    }
                    # end isbn

                    # book cat id
                    $and=" AND ";

                    if( !empty($_GET['books_catid']) || $count_category )
                    {
                        if( $count_categories_sub )
                        {
                            $query.=$and." ( ";
                            $i=0;
                            foreach( $ul_literature['categories_sub'] as $row )
                            {
                                $i++;
                                if( $i!=1 ) $query.=" OR ";
                                $query.=" b.books_catid='".$row['books_catid']."' ";
                                //$url_search.="&amp;books_catid=".$row['books_catid'];
                            }
                            $query.=" ) ";
                        }
                        else
                        {
                            if( $count_category ) $books_catid=$books_catid;
                            else $books_catid=$_GET['books_catid'];
                            $query.=$and." b.books_catid='".$books_catid."' ";
                            $url_search.="&amp;books_catid=".$books_catid;
                        }
                    }
                    # end book cat id

                    # year
                    //if( !empty($_GET['author']) || !empty($_GET['title']) || !empty($_GET['keyword']) || !empty($_GET['books_catid']) ) $and=" AND ";
                    //else $and="";
                    $and=" AND ";

                    if( !empty($_GET['year-from']) && !empty($_GET['year-to']) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y') BETWEEN '".$_GET['year-from']."' AND '".$_GET['year-to']."' ";
                        $url_search.="&amp;year-from=".$_GET['year-from']."&amp;year-to=".$_GET['year-to'];
                    }
                    if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')>='".$_GET['year-from']."' ";
                        $url_search.="&amp;year-from=".$_GET['year-from'];
                    }
                    if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')<='".$_GET['year-to']."' ";
                        $url_search.="&amp;year-to=".$_GET['year-to'];
                    }
                    /*
                    if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')='".$_GET['year-from']."' ";
                        $url_search.="&amp;year-from=".$_GET['year-from'];
                    }
                    if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')='".$_GET['year-to']."' ";
                        $url_search.="&amp;year-to=".$_GET['year-to'];
                    }
                    */
                    if( !empty($year_from) && !empty($year_to) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y') BETWEEN '".$year_from."' AND '".$year_to."' ";
                    }
                    /*
                    if( !empty($year_from) && empty($year_to) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')>='".$year_from."' ";
                    }
                    if( empty($year_from) && !empty($year_to) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')<='".$year_to."' ";
                    }
                    */
                    if( !empty($year_from) && empty($year_to) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')='".$year_from."' ";
                    }
                    if( empty($year_from) && !empty($year_to) )
                    {
                        $query.=$and." DATE_FORMAT(b.link_date, '%Y')='".$year_to."' ";
                    }

                    # end year

                    # languageid
                    $and=" AND ";
                    if( !empty($_GET['languageid']) )
                    {
                        $query.=$and." b.id=bi.bookID AND ( bi.language='".$_GET['languageid']."' OR bi.language2='".$_GET['languageid']."' OR bi.language3='".$_GET['languageid']."' ) ";
                        $url_search.="&amp;languageid=".$_GET['languageid'];
                    }
                    # end languageid

                    # info variable
                    if( !empty($_GET['info']) )
                    {
                        $url_search.="&amp;info=".$_GET['info'];
                    }
                    # end info variable
                }
                /*
                elseif( !empty($_GET['section_1']) && !empty($_GET['bookid']) )
                {
                    $query.=" WHERE b.id='".$_GET['bookid']."' AND b.enable=1 ";
                }
                */
                else $query.=" WHERE b.enable=1 ";
                # end SEARCH

                $results=$db->query($query);
                $count=$db->numrows($results);
                @$pages=ceil($count/$_GET['sp']);

                # author order
                $sql_order_by_author=" isnull ASC, b.author ";
                # end

                if( $sort=="link_date" ) $sql_order_by=" DATE_FORMAT(b.link_date, '%Y') ";
                else $sql_order_by=$sort;
                $query.=" ORDER BY ";
                if( $sort=="author" ) $query.=$sql_order_by_author." ".$sort_how;
                else $query.=$sql_order_by." ".$sort_how;
                if( $sort!="author" ) $query.=", ".$sql_order_by_author;

                # LIMIT
                $query_books=$query." LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];

                if( $_SESSION['debug_page'] ) print $query_books;

                $results_books=$db->query($query_books);
                @$count_books=$db->numrows($results_books);

                # You searched for
                if( $search && !$_GET['info'] )
                {
                    $html_print_body.="<div class='div-you-searched-for'>";
                        $html_print_body.=LANG_SEARCH_YOUSEARCHEDFOR.": <br />";

                        # author
                        if( !empty($_GET['author']) ) $html_print_body.=LANG_RIGHT_SEARCH_LIT_AUTHOR.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['author'])."</em><br />";

                        # title
                        if( !empty($_GET['title']) ) $html_print_body.=LANG_RIGHT_SEARCH_LIT_TITLE.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['title'])."</em><br />";

                        # keyword
                        if( !empty($_GET['keyword']) ) $html_print_body.=LANG_RIGHT_SEARCH_LIT_KEYWORD.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['keyword'])."</em><br />";

                        # isbn
                        if( !empty($_GET['isbn']) ) $html_print_body.=LANG_LITERATURE_ISBN.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['isbn'])."</em><br />";

                        # books_catid
                        if( !empty($_GET['books_catid']) )
                        {
                            $query_where_book_cat=" WHERE books_catid='".$_GET['books_catid']."' ";
                            $query_book_cat=QUERIES::query_books_categories($db,$query_where_book_cat);
                            $results_book_cat=$db->query($query_book_cat['query']);
                            $row_book_cat=$db->mysql_array($results_book_cat);
                            $title_book_category=UTILS::row_text_value($db,$row_book_cat,"title");
                            $html_print_body.=LANG_RIGHT_SEARCH_LIT_CATEGORY.": <em class='search-highlight'>".$title_book_category."</em><br />";
                        }

                        # languageid
                        if( !empty($_GET['languageid']) )
                        {
                            $query_where_language=" WHERE languageid='".$_GET['languageid']."' ";
                            $query_language=QUERIES::query_books_languages($db,$query_where_language);
                            $results_language=$db->query($query_language['query']);
                            $row_language=$db->mysql_array($results_language);
                            $row_language=UTILS::html_decode($row_language);
                            $title_language=UTILS::row_text_value($db,$row_language,"title");

                            $html_print_body.=LANG_RIGHT_SEARCH_LIT_LANGUAGE.": <em class='search-highlight'>".$title_language."</em><br />";
                        }

                        # year-from
                        if( !empty($_GET['year-from']) ) $html_print_body.=LANG_SEARCH_YEARFROM.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['year-from'])."</em><br />";

                        # year-to
                        if( !empty($_GET['year-to']) ) $html_print_body.=LANG_SEARCH_YEARTO.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['year-to'])."</em><br />";

                        //if( $count==1 ) $s=LANG_SEARCH_RESULTS2;
                        //else $s=LANG_SEARCH_RESULTS3;
                        //if( $count!=0 ) $html_print_body.=LANG_SEARCH_RESULTS1." <em class='search-highlight'>$count</em> $s<br />";

                    $html_print_body.="</div>";
                }
                # END You searched for

                $html_print_body.="<div id='bookSearch'>\n";

                    if( $count_books>0 )
                    {
                        $url.=$url_search;

                        $html_print_body.="<div class='div-book-table-header'>";

                            //$html_print_body.="<div class='div-items-found-container'>";
                                $info_text="";
                                $info_text.=$count;
                                if( $_GET['lang']!="zh" ) $info_text.=" ";
                                if( $books_catid==5 )
                                {
                                    if( $count>1 ) $info_text.=LANG_LITERATURE_TITLES;
                                    else $info_text.=LANG_LITERATURE_TITLE;
                                }
                                elseif( $books_catid_sub==5 )
                                {
                                    if( $books_catid==10 && $_GET['lang']=="de" ) $info_text.=lcfirst($title_cat);
                                    else $info_text.=$title_cat;
                                }
                                elseif( $books_catid==7 && $_GET['lang']=="zh" )
                                {
                                    $info_text.="段视频";
                                }
                                elseif( !empty($title_category_sub) )
                                {
                                    if( $categoryid==15 )
                                    {
                                        $info_text.=$title_category;
                                    }
                                    else
                                    {
                                        $info_text.=$title_category_sub;
                                    }
                                }
                                elseif( !empty($title_cat) ) $info_text.=$title_cat;
                                else
                                {
                                    if( $count>1 ) $info_text.=LANG_SEARCH_RESULTS;
                                    else $info_text.=LANG_SEARCH_RESULT;
                                }
                            //$html_print_body.="</div>\n";
                            $html_print_body.="<div class='div-per-page-numbering-container'>";
                                # books per page to show
                                $html_show="<span class='span-show-per-page'>".LANG_SHOW."</span>"."&nbsp;";
                                //$html_print_body.=$html_show;
                                $options_per_page=array();
                                $options_per_page=$options_head;
                                $options_per_page['count']=$count_books;
                                $options_per_page['name']="sp";
                                $options_per_page['id']="sp";
                                $options_per_page['sp']=$_GET['sp'];
                                $options_per_page['onchange']=" onchange=\"location.href='?p=1&amp;sp='+this.options[selectedIndex].value+'".$url_search."'\" ";
                                $options_per_page['class']['select_pp']="select_pp";
                                $html_print_body.=$html->select_books_per_page($db,$options_per_page);
                                #end books per page to show

                                //print_r($_GET);

                                # page numbering
                                $options_page_numbering=array();
                                $options_page_numbering=$options_head;
                                $options_page_numbering['pages']=$pages;
                                $options_page_numbering['count']=$count;
                                $options_page_numbering['sp']=$_GET['sp'];
                                $options_page_numbering['p']=$_GET['p'];
                                //$options_page_numbering['url']=$url;
                                $options_page_numbering['url_after'].="&amp;sp=".$_GET['sp'].$url_search;
                                $options_page_numbering['class']['div-page-navigation']="div-page-navigation-lit";
                                $options_page_numbering['info-text']=$info_text;
                                $options_page_numbering['per_page_list']=1;
                                $options_page_numbering['thumbs_per_line']=5;
                                $html_print_body.=$html->page_numbering($db,$options_page_numbering);
                                # end page numbering

                                $html_print_body.="<div class='clearer'></div>";
                            $html_print_body.="</div>\n";
                            $html_print_body.="<div class='clearer'></div>\n";

                        $html_print_body.="</div>\n";

                    }

                    # RESULTS
                    $html_print_body.="<div id='result'>\n";

                        if( $count_books>0 )
                        {
                $onheaderclick=htmlspecialchars_decode('?'.$url_search . '&sort=');
                $options_table = array(
                    'options'       =>  array(
                        // Setup table options here...
                        'id'                => 'expandlist_literature_01',      // set DOM node id for js (uniqid() if not set)
                        'sort_by'   => array($_GET['sort'], $_GET['sort_how']), // default column sorted
                        'expandable'        => 1,                            // Allow tr tags to expand (expand-boxes required)
                        'class'        => array(),                            // Allow tr tags to expand (expand-boxes required)
                        'highlight'         =>  $_GET['search'],                // highlight patterns in content
                        'html_class'         =>  $html,
                        'html_return'         =>  $options_head['html_return'],
                        'onheaderclick'     => $onheaderclick
                    )
                    /*
                    'cols'          =>  array(
                        // Define columns here...
                        array( 'caption' => "",                       'width' => '10%',   'col' => '0',   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' ),
                        array( 'caption' => LANG_TABLE_TITLE,    'width' => '30%',   'col' => '1',   'sortable' => true, 'sort_col' => 'b.title',     'content_align' => '' ),
                        array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '29%',     'col' => '2',   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' ),
                        //array( 'caption' => LANG_RIGHT_SEARCH_LIT_CATEGORY,   'width' => '*',     'col' => '3',   'sortable' => true, 'sort_col' => 'b.books_catid',     'content_align' => '' ),
                        array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => '4',   'sortable' => true, 'sort_col' => 'b.link_date',     'content_align' => '' )
                    )
                    */
                );

                $options_table['cols']=array();
                $i_col=0;
                $options_table['cols'][]=array( 'caption' => "",                       'width' => '10%',   'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' );
                $i_col++;
                $options_table['cols'][]=array( 'caption' => LANG_TABLE_TITLE,    'width' => '38%',   'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.title',     'content_align' => '' );
                $i_col++;
                $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' );
                /*
                if($search)
                {
                    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '29%',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' );
                    $i_col++;
                    $options_table['cols'][]=array( 'caption' => LANG_RIGHT_SEARCH_LIT_CATEGORY,   'width' => '*',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.books_catid',     'content_align' => '' );
                }
                else
                {
                    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' );
                }
                */
                $i_col++;
                $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.link_date',     'content_align' => '' );

                # get literature data fro table
                $options_table_data=array();
                $options_table_data['html_return']=$options_head['html_return'];
                $options_table_data['cols']=$options_table['cols'];
                $options_table_data['sort']=$options_table['options']['sort_by'];
                $options_table_data['highlight']=$_GET['search'];
                $table_data = new Table_Data($db,$options_table_data);
                $options_table_data=array();
                $options_table_data['results']=$results_books;
                $rows=$table_data->table_data_literature($options_table_data);
                # END get literature data fro table

                $table = new Table($db, $rows, $options_table);
                $html_print_body.=$table->draw_table();

                        }
                        else $html_print_body.="<div class='result-error'>".LANG_LIT_SEARCH_NOFOUND."</div>\n";

                    $html_print_body.="</div>\n";
                    # end RESULTS

                    //if( $_GET['sp']>15 && $pages>1 )
                    if( $_GET['sp']>15 )
                    {
                        $html_print_body.="<div class='div-book-table-footer'>";
                            /*
                            $html_print_body.="<div class='div-items-found-container'>";
                                $html_print_body.=$count." ".$title_cat." ".LANG_FOUND.":";
                            $html_print_body.="</div>\n";
                            */
                            $html_print_body.="<div class='div-per-page-numbering-container'>";
                                # books per page to show
                                /*
                                $class=array();
                                $class['select_pp']="select_pp";
                                $html->select_books_per_page($db,"sp","sp-footer",$_GET['sp'],$onchange_per_page,$class);
                                */
                                #end books per page to show

                                # page numbering
                                //$options_page_numbering['class']['div-page-navigation']="div-page-navigation-lit";
                                $options_page_numbering['info-text']="";
                                $html_print_body.=$html->page_numbering($db,$options_page_numbering);
                                # end page numbering
                            $html_print_body.="</div>";

                        $html_print_body.="</div>";
                    }

                $html_print_body.="</div>\n"; # END bookSearch
            }
        $html_print_body.="</div>";

    #END div main content

}
# *** END *** MAIN LITERATURE SECTION /literature

/*
else
{
    UTILS::not_found_404();
}
*/

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>
$html_print_body.="<div class='clearer'></div>";

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
