<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html_return="";
$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_body="";

if( $_GET['section_1']=="guide" )
{
    $page_guide=1;
}
elseif( !empty($_GET['section_1']) && !preg_match('/^[0-9][0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]$/', $_GET['section_1']) && $_GET['section_1']!="search" )
{
    $page_detail_view=1;
}
elseif( $_GET['section_0']=="exhibitions" )
{
    $page_main=1;
}

$html_print_body.="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";

        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
        $html_print_body.="});";
        # END tooltips load

        if( $page_main )
        {
            # select box pretty
            $html_print_body.="$('.select-field, .select_pp').selectBoxIt({";
            $html_print_body.="});";
        }

        # show more less search options
        $html_print_body.="$.fn.toggleText = function(t1, t2){
          if (this.text() == t1) this.text(t2);
          else                   this.text(t1);
          return this;
        };";
        $html_print_body.="$( '.a-right-search-show-more-options' ).click(function() {";
            $html_print_body.="$( '.div-content-header-block-right' ).toggleClass( 'div-content-header-block-right-large' );";
            $html_print_body.="$( this ).toggleClass( 'a-right-search-show-more-options-less' );";
            $html_print_body.="$( this ).toggleText(\"".LANG_LITERATURE_SHOW_LESS_OPTIONS."\", \"".LANG_LITERATURE_SHOW_MORE_OPTIONS."\");";
            $html_print_body.="$( '.div-content-header-block-right-more-options' ).toggleClass( 'show' );";
        $html_print_body.="});";

        # tooltips load
        $html_print_body.="$( '.div-painting-info-button-box' ).tooltip({";
            $html_print_body.="items: '[data-title],[data-microsite-preview]',";
            //$html_print_body.="content: $('.div-tooltips-info-microsite').html(),";

            $html_print_body.="content: function() {";
                $html_print_body.="var element = $( this );";
                $html_print_body.="if ( element.is( '[data-microsite-preview]' ) ) {";
                    $html_print_body.="var element_data = '.div-tooltips-info-microsite';";
                    $html_print_body.="return $(element_data).html()";
                $html_print_body.="}";
            $html_print_body.="},";

            $html_print_body.="tooltipClass: 'div-tooltips-microsite-preview',";
            $html_print_body.="position: {";
                $html_print_body.="my: 'center bottom-4',";
                $html_print_body.="at: 'center top',";
            $html_print_body.="}";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="});";

    $html_print_body.="});";
$html_print_body.="</script>\n";


$html_print_body.="<div id='div-large-content' role='main' >";

$html_print_footer="";

# exhibition guide download page
if( $page_guide )
{
    $tmp=explode(".", $_GET['section_2']);
    $exhibitionid=UTILS::itemid($db,$tmp[0]);

    if( !empty($exhibitionid) )
    {
        $results_exh_guide=$db->query("SELECT exID,src_guide FROM ".TABLE_EXHIBITIONS." WHERE exID='".$exhibitionid."' LIMIT 1 ");
        $count_exh_guide=$db->numrows($results_exh_guide);
        $row_exh_guide=$db->mysql_array($results_exh_guide);
    }

    ### if found image in db
    if( sizeof($error)==0 )
    {
        if( $count_exh_guide )
        {
            $src=DATA_PATH."/files/guides/".$row_exh_guide['src_guide'];
        }
        else
        {
            UTILS::not_found_404();
        }

        ### if file exists on file system
        if( file_exists($src) )
        {
            ### if showing video straight from file system
            if( !$_GET['debug'] )
            {
                header ('Content-transfer-encoding: binary');
                header('Content-Type: '.$mime);
                header('Content-Disposition: '.$disposition.'; filename="'.$_GET['section_2'].'"');
                header ('Content-Length: ' . filesize($src));
                header ('Pragma: public');
                header ('Expires: 0');
                header ('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header ('Cache-Control: private');
                $f = fopen($src, 'rb');
                $contents = stream_get_contents($f);
                print $contents;
                fclose($f);
            }
            else
            {
                print $src;
            }
            #end
        }
        #end
        else $error[]="Can't find file on file system ".$src;

    }
    #end

    ### if no video found showing no image available
    if( sizeof($error)>0 )
    {
        if( $_GET['debug'] )
        {
            print_r($error);
            exit;
        }
        $src=LANG_NO_IMAGE_LARGE;
        $getimagesize=getimagesize($no_image);
        if( empty($getimagesize['mime']) ) $getimagesize['mime']="image/gif";
        header('Content-Type: '.$getimagesize['mime']);
        $f = fopen($src, 'rb');
        $contents = stream_get_contents($f);
        print $contents;
    }
    #end
}
# END exhibition guide download page

# /exhibitions/exhibition-title-here-2324
elseif( $page_detail_view )
{
	$title_meta="";

    $paintid=UTILS::itemid($db,$_GET['section_2']);
    if( !empty($paintid) )
    {
        //$query_where_painting=" WHERE paintID='".$paintid."' AND enable=1 ";
        $query_where_painting=" WHERE titleurl='".$_GET['section_2']."' AND enable=1 ";
        $query_painting=QUERIES::query_painting($db,$query_where_painting);
        $results_painting=$db->query($query_painting['query']);
        $count_painting=$db->numrows($results_painting);
        $row_painting=$db->mysql_array($results_painting,0);
        $row_painting=UTILS::html_decode($row_painting);
        $title_painting=UTILS::row_text_value2($db,$row_painting,"title");
        $title_meta.=$title_painting." &raquo; ";
        if( !$count_painting ) UTILS::not_found_404();
    }

    $exhibitionid=UTILS::itemid($db,$_GET['section_1']);
    //$query_where_exhibition=" WHERE exID='".$exhibitionid."' AND enable=1 ";
    $query_where_exhibition=" WHERE titleurl='".$_GET['section_1']."' AND enable=1 ";
    $query_exhibition=QUERIES::query_exhibition($db,$query_where_exhibition,$query_order,$query_limit);
    $results_exhibition=$db->query($query_exhibition['query']);
    $count_exhibition=$db->numrows($results_exhibition);
    $row_exhibition=$db->mysql_array($results_exhibition);
    $row_exhibition=UTILS::html_decode($row_exhibition);
    //$title_exhibition=UTILS::row_text_value2($db,$row_exhibition,"title");
    $title_meta.=str_replace("Gerhard Richter: ", "", $row_exhibition['title_original']);
    $title_meta=str_replace("Gerhard Richter. ", "", $title_meta);

    if( !$count_exhibition ) UTILS::not_found_404();

	# title meta
	$html->title=$title_meta." &raquo; ".LANG_HEADER_MENU_EXHIBITIONS." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

	$html->css[]="/css/main.min.css";
	$html->css[]="/css/screen.min.css";
    $html->css[]="/css/main.mobile.min.css";
	$html->css[]="/css/tabs.min.css";
	$html->css[]="/css/tabs_exh_decades.min.css";
	$html->css[]="/css/exhibitions.min.css";
	$html->css[]="/css/books.min.css";
	$html->css[]="/css/page-numbering.min.css";
	$html->css[]="/css/painting_detail.min.css";
	$html->css[]="/css/videos.min.css";
    $html->css[]="/css/painting-detail-view.min.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.min.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

	# tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
	//$html->js[]="/js/jquery_fancybox/fancybox.load.js";
	$html->js[]="/js/jquery_fancybox/fancybox.load.min.php?lang=".$_GET['lang'];

	#expand table
	$html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.min.css";
	$html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.min.js";

	# history support
	$html->js[]="/js/history/jquery.history.js";

	# right side blocks
	$html->right_side=array('search_exhibitions');

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'exhibitions',$options_head);

    $html_print_head.="<script type='text/javascript'>\n";
        $html_print_head.="var search=0;\n";
        $html_print_head.="$(function() {";

            # select box pretty
            $html_print_head.="$('.select-field').selectBoxIt({";
                //$html_print_head.="showFirstOption: false";
            $html_print_head.="});";
            //$html_print_head.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
            //$html_print_head.="selectBox.selectOption(0);";

        $html_print_head.="});";
    $html_print_head.="</script>\n";

	# left side blocks
    $tab_number=0;

    ### ARTWORK tab content

    /*
    $query_exh_painting_relations="SELECT *
            FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p
            WHERE (
                ( r.typeid1=4 AND r.itemid1='".$row_exhibition['exID']."' AND r.typeid2=1 )
                OR ( r.typeid2=4 AND r.itemid2='".$row_exhibition['exID']."' AND r.typeid1=1 )
            )
            AND (
                ( r.itemid2=p.paintID AND r.typeid2=1 )
                OR ( r.itemid1=p.paintID AND r.typeid1=1 )
            )
            AND p.enable=1 ";
    $query_exh_painting_relations.=" AND p.artworkID!=16 "; # do not display artworkID 16(Prints) while Joe hasnt allowed this
    if( $_GET['order-by']=="year" ) $query_exh_painting_relations.=" ORDER BY p.year ASC, p.sort2 ASC ";
    else $query_exh_painting_relations.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
    */

        $query_paintings=QUERIES::query_painting($db);

        $query_exh_painting_relations="
            SELECT
              ".$query_paintings['query_columns'].",
              COALESCE(
                r1.itemid2,
                r2.itemid1
              ) as itemid,
              COALESCE(
                r1.sort,
                r2.sort
              ) as sort_rel,
              COALESCE(
                r1.relationid,
                r2.relationid
              ) as relationid
            FROM
              ".TABLE_PAINTING." p
            LEFT JOIN
              ".TABLE_RELATIONS." r1 ON r1.typeid1 = 4 and r1.itemid1=".$row_exhibition['exID']." and r1.typeid2 = 1 and r1.itemid2 = p.paintID
            LEFT JOIN
              ".TABLE_RELATIONS." r2 ON r2.typeid2 = 4 and r2.itemid2=".$row_exhibition['exID']." and r2.typeid1 = 1 and r2.itemid1 = p.paintID
            WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL ) AND p.enable=1 ";
            $query_exh_painting_relations.=" AND p.artworkID!=16 "; # do not display artworkID 16(Prints) while Joe hasnt allowed this

        //$query_exh_painting_relations.=" ORDER BY sort_rel ASC, relationid DESC ";

        if( $_GET['order-by']=="year" ) $query_exh_painting_relations.=" ORDER BY p.year ASC, p.sort2 ASC ";
        else $query_exh_painting_relations.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

    //print $query_artworks;


    if( $_SESSION['debug_page'] ) $html_print_head.=$query_exh_painting_relations."<br /><br />";
    $results_exh_painting_relations=$db->query($query_exh_painting_relations);
    $count_relations=$db->numrows($results_exh_painting_relations);
    //print $count_relations;
    if( $count_relations )
    {
        $thumb_array_artworks=array();
        $i=0;
        while( $row_exh_painting_relation=$db->mysql_array($results_exh_painting_relations) )
        {
            $thumb_array_artworks[$i]=$row_exh_painting_relation;
            $i++;
        }
        $tab_artwork_number=$tab_number;
        $tab_number++;
    }
    //print count($thumb_array_artworks);
    #end

    ### RELATED EXHIBITIONS tab content
    $values_query_related_exh=array();
    $query_related_exh=QUERIES::query_exhibition($db,"","","",$values_query_related_exh);
    $results_related_exh_relations=$db->query("SELECT ".$query_related_exh['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibition['exID']."' AND r.typeid2=4 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibition['exID']."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND e.exID!='".$row_exhibition['exID']."' AND e.enable=1 ORDER BY e.date_from DESC ");
    $count_related_exh_relations=$db->numrows($results_related_exh_relations);
    if( $count_related_exh_relations>0 )
    {
        //print $tab_number;
        $tab_rel_exh_number=$tab_number;
        $tab_number++;
    }
    #end

    ### INSTALLATION SHOTS tab content
    $results_installations_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exhibition['exID']."' AND typeid2=5 ) OR ( typeid2=4 AND itemid2='".$row_exhibition['exID']."' AND typeid1=5 ) LIMIT 1 ");
    $count_installations_relations=$db->numrows($results_installations_relations);
    if( $count_installations_relations>0 )
    {
        $tab_installation_number=$tab_number;
        $tab_number++;
    }
    #end

    ### LITERATURE tab content
    $query_literature=QUERIES::query_books($db);

    //$query_relations_literature"SELECT b.id FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibition['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibition['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ";

    $query_relations_literature="SELECT
                  ".$query_literature['query_columns'].",
                  COALESCE(
                    r1.itemid2,
                    r2.itemid1
                  ) as id,
                  COALESCE(
                    r1.sort,
                    r2.sort
                  ) as sort_rel,
                  COALESCE(
                    r1.relationid,
                    r2.relationid
                  ) as relationid
                FROM
                  ".TABLE_BOOKS." b
                LEFT JOIN
                  ".TABLE_RELATIONS." r1 ON r1.typeid1 = 4 and r1.itemid1='".$row_exhibition['exID']."' and r1.typeid2 = 12 and r1.itemid2 = b.id
                LEFT JOIN
                  ".TABLE_RELATIONS." r2 ON r2.typeid2 = 4 and r2.itemid2='".$row_exhibition['exID']."' and r2.typeid1 = 12 and r2.itemid1 = b.id
                WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL ) AND b.enable=1
            ";

    $results_literature=$db->query($query_relations_literature);

    $count_literature=$db->numrows($results_literature);
    if( $count_literature>0 )
    {
        //print $tab_number;
        $tab_literature_number=$tab_number;
        $tab_number++;
    }
    #end

    ### VIDEOS tab content
    $results_videos_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=4 AND itemid1='".$row_exhibition['exID']."' AND typeid2=10 ) OR ( typeid2=4 AND itemid2='".$row_exhibition['exID']."' AND typeid1=10 ) ");
    $count_videos_relations=$db->numrows($results_videos_relations);
    if( $count_videos_relations>0 )
    {
        $tab_videos_number=$tab_number;
        $tab_number++;
    }
    #end

    ### GUIDE tab content
    if( !empty($row_exhibition['src_guide']) )
    {
        $tab_guide_number=$tab_number;
        $tab_number++;
    }
    #end

    $li_array[0]['url']="/".$_GET['lang']."/exhibitions";$li_array[0]['title']=LANG_EXH_SHOW_ALL;$li_array[0]['class']['li']="li-exh-left-show-all";
    if( !empty($thumb_array_artworks) )
    {
        $li_array[1]['url']="#";
        $li_array[1]['title']=LANG_EXH_ARTWORK;$li_array[1]['class']['a']="a-exh-left-artwork";
        $li_array[1]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_artwork_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
    if( $count_related_exh_relations )
    {
        $li_array[2]['url']="#";
        $li_array[2]['title']=LANG_EXH_RELATED_EXHIBITIONS;$li_array[2]['class']['a']="a-exh-left-related-exh";
        $li_array[2]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_rel_exh_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
    if( $count_installations_relations )
    {
        $li_array[3]['url']="#";
        $li_array[3]['title']=LANG_EXH_INST_SHOTS;$li_array[3]['class']['a']="a-exh-left-inst-shots";
        $li_array[3]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_installation_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
    if( $count_literature )
    {
        $li_array[4]['url']="#";
        $li_array[4]['title']=LANG_EXH_LITERATURE;$li_array[4]['class']['a']="a-exh-left-literature";
        $li_array[4]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_literature_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
    if( $count_videos_relations )
    {
        $li_array[5]['url']="#";
        $li_array[5]['title']=LANG_EXH_VIDEOS;$li_array[5]['class']['a']="a-exh-left-videos";
        $li_array[5]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_videos_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
    if( !empty($row_exhibition['src_guide']) )
    {
        $li_array[6]['url']="#";
        $li_array[6]['title']=LANG_EXH_GUIDE;$li_array[6]['class']['a']="a-exh-left-guide";
        $li_array[6]['onclick']="$('#tabs-exhibition-detail').tabs('option','active',".$tab_guide_number.");$('#sub-nav a.selected').removeClass('selected');$(this).addClass('selected');";
    }
	$html_print_head.="<div id='sub-nav'>\n";
	    $html_print_head.="<ul id='ul-biography'>\n";
		    foreach( $li_array as $key => $value )
		    {
		        if( $key==1 ) $selected="selected";
		        else $selected="";
		        if( $key==0 || $key==1 ) $class_first="first";
		        else $class_first="";
		        $html_print_head.="\t<li class='".$class_first." ".$value['class']['li']."'><a href='".$value['url']."' title='".$value['title']."' onclick=\"".$value['onclick']."\" class='".$value['class']['a']." ".$selected."'>".$value['title']."</a></li>\n";
		    }
		$html_print_head.="</ul>\n";
	$html_print_head.="</div>\n";
	# END left side blocks

	# div main content

        # prepare search vars
        $search_vars=UTILS::search_vars_from_get($_GET);
        //print_r($search_vars);
        # END

        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/exhibitions";
        $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_EXHIBITIONS;
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_EXHIBITIONS;

        $year_to=UTILS::roundUpToTen($row_exhibition['date_year'])-1;
        $year_from=$year_to-9;
        $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/exhibitions/".$year_from."-".$year_to;
        if( $year_to==2019 ) $year_display=LANG_EXH_ONWARDS;
        else $year_display=$year_from." &#8211; ".$year_to;
        $options_breadcrumb['items'][1]['title']=$year_display;
        $options_breadcrumb['items'][1]['inner_html']=$year_display;
        # END title_original
        if( !empty($_GET['section_2']) )
        {
            $options_breadcrumb['items'][2]['href']="/".$_GET['lang']."/exhibitions/".$row_exhibition['titleurl'];
            $options_breadcrumb['items'][2]['title']=$row_exhibition['title_original'];
        }
        $options_breadcrumb['items'][2]['inner_html']=$row_exhibition['title_original'];
        $options_breadcrumb['items'][2]['length']=70;
        if( !empty($_GET['section_2']) )
        {
            # title
            $title_painting=UTILS::row_text_value2($db,$row_painting,"title");
            if( strlen($title_painting)>$length )
            {
                $title_splited=preg_split("/gerhard\srichter.\s/i", $title_painting,null,PREG_SPLIT_NO_EMPTY);
                $title_painting=substr($title_splited[0],0,$length)."..";
            }
            # title end    }
        }
        if( !empty($options_breadcrumb['items'][2]['href']) )
        {
            $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][2]['inner_html'];
            $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][2]['href'];
        }
        elseif( !empty($options_breadcrumb['items'][1]['href']) )
        {
            $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
            $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
        }
        //print_r($options_breadcrumb);
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

		# exhibition div_detail_view_item

        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=4 AND itemid2='".$row_exhibition['exID']."' ) OR ( typeid2=17 AND typeid1=4 AND itemid1='".$row_exhibition['exID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
        $count_images=$db->numrows($results_related_image);
        if( $count_images>0 )
        {
            $images_array=array();
            while( $row_related_image=$db->mysql_array($results_related_image) )
            {
                $images_array[]=$row_related_image;
            }
            $imageid=UTILS::get_relation_id($db,"17", $images_array[0]);
            $href_image=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
            $src_image=DATA_PATH_DATADIR."/images_new/large/".$imageid.".jpg";
            list($width, $height) = @getimagesize(DATA_PATH."/images_new/large/".$imageid.".jpg");
        }
        if( !$row_exhibition['image_enable'] ) $count_images=0; # for exhibitions we check if catalogue image is uploaed

        $options_item=array();
        $options_item=$options_head;
        $options_item['row']=$row_exhibition;
        $options_item['count_images']=$count_images;
        $options_item['height']=$height;
        $options_item['width']=$width;
        $options_item['href_image']=$href_image;
        $options_item['src_image']=$src_image;
        $options_item['images_array']=$images_array;
        $options_item['title']=$row_exhibition['title_original'];
        $title_translation=UTILS::row_text_value2($db,$row_exhibition,"title");
        $options_item['title_translation']=UTILS::row_text_value2($db,$row_exhibition,"title");
        $options_item['url_admin_edit']="/admin/exhibitions/edit/?exhibitionid=".$row_exhibition['exID'];
        $options_item['class']['h1-item']="h1-item-exh";
        $options_item['class']['p-item-translation']="p-item-translation-exh";
        $options_item['class']['div-detail-view-item-right']="div-detail-view-item-left-exhibitions";
        $options_item['class']['div-detail-view-item-right']="div-detail-view-item-right-exhibitions";
        $html_print_body.=$html->div_detail_view_item_start($db,$options_item);

            $options_location=array();
            $options_location['row']=$row_exhibition;
            $location=location($db,$options_location);
            $html_print_body.="<p class='p-exh-detail-location'>".$location."</p>\n";
            $date=exhibition_date($db,$row_exhibition);
            $html_print_body.="<p class='p-exh-detail-date'>".$date."</p>\n";
            if( $row_exhibition['description_enable'] )
            {
                $description=UTILS::row_text_value2($db,$row_exhibition,'description');
                $options_div_text=array();
                $options_div_text['class']['div_text_wysiwyg']="div-exh-detail-desc";
                $html_print_body.=$html->div_text_wysiwyg($db,$description,$options_div_text);
            }

        $html_print_body.=$html->div_detail_view_item_end($db,$options_item);
        # END exhibition div_detail_view_item

        # relation tabs
        //print "\t<a name='tabs'></a>\n";
        $options_relations=array();
        $options_relations=$options_head;
        $options_relations['row']=$row_exhibition;
        $options_relations['row_painting']=$row_painting;
        $options_relations['exhibitionid']=$row_exhibition['exID'];
        $options_relations['typeid']=4;
        $options_relations['itemid']=$row_exhibition['exID'];
        $options_relations['id']="tabs-exhibition-detail";
        $options_relations['html_class']=$html;
        $options_relations['thumb_per_line_installattion_photos']=4;
        $options_relations['thumb_per_line_videos']=4;
        $options_relations['thumbs_per_line']=4; # for artwork tab
        $options_relations['tab']="tab";
        $options_relations['painting_no_section']=1;
        $options_relations['content_large']=1;
        $options_relations['class']['div-relation-tabs']="div-painting-relation-tabs-paint-detail";
        $options_relations['class']['div-relation-tabs-info']="div-book-detail-tab-info";
        $options_relations['class']['p-section-desc-text']="p-literature-section-desc-text";
        $options_relations['url_painting']="?".$search_vars['with_sp'];

        if( !empty($thumb_array_artworks) ) $options_relations['results_artworks']=$thumb_array_artworks;
        if( $count_literature ) $options_relations['results_literature']=$results_literature;

        $html_print_body.=div_relation_tabs($db,$options_relations);
        # end

	#END div main content

}
#END /exhibitions/exhibition-titlt-here-2324

# MAIN exhibitions SECTION /exhibitions
elseif( $page_main )
{
	if( $_GET['section_1']=="search" )
	{
		$title_meta=LANG_HEADER_MENU_SEARCH." &raquo; ";
        $search=1;
	}
    # main exhibition section
    else
    {
        $main_section=1;
    }

	$title_meta.=LANG_HEADER_MENU_EXHIBITIONS;

	# title meta
	$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

	$html->css[]="/css/main.min.css";
	$html->css[]="/css/screen.min.css";
    $html->css[]="/css/main.mobile.min.css";
	$html->css[]="/css/tabs.min.css";
	$html->css[]="/css/tabs_exh_decades.min.css";
	$html->css[]="/css/exhibitions.min.css";
    $html->css[]="/css/painting-detail-view.min.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.min.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

	# tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# right side blocks
	$html->right_side=array('search_exhibitions');

	# meta-tags & head
	$options_head=array();
	$options_head['meta_description']=LANG_META_DESCRIPTION_EXHIBITIONS;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'exhibitions',$options_head);

	# checking if year range is valid in url
	if( empty($_GET['section_1']) && !isset($_GET['title']) && !isset($_GET['location']) && !isset($_GET['keyword']) && !isset($_GET['year-from']) && !isset($_GET['year-to']) )
	{
	    $_GET['section_1']="2020-2029";
	    $_GET['year']=date("Y");
	}

	if( $_GET['section_1']=="2020-2029" || ( empty($_GET['section_1']) && !isset($_GET['title']) && !isset($_GET['location']) && !isset($_GET['keyword']) ) )
	{
	    if( empty($_GET['year']) && !empty($_GET['year-from']) && isset($_GET['year-from']) ) $_GET['year']=$_GET['year-from'];
	    elseif( empty($_GET['year']) && empty($_GET['year']) ) $_GET['year']=date("Y");
	}

	# if not valid decade provided show 404
	if( !preg_match('/^[0-9][0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]$/', $_GET['section_1']) && $_GET['section_1']!="search" )
	{
		UTILS::not_found_404();
	}

    $tmp=explode("-", $_GET['section_1']);
    if( is_numeric($tmp[0]) ) $year_from=$tmp[0];
    $year_to=$tmp[1];

	if( empty($_GET['year']) && $year_from==1960 ) $_GET['year']=1962;
	elseif( ( $_GET['year']<$year_from || $_GET['year']>$year_to ) && !empty($year_from) ) $_GET['year']=$year_from;
	elseif( empty($_GET['year']) ) $_GET['year']=$year_from;
	# END checking if year range is valid in url

	# left side blocks
    //$html_print_head.="<div id='sub-nav'>";
    	$options_ul_exh=array();
    	$options_ul_exh=$options_head;
    	$options_ul_exh['section_1']=$_GET['section_1'];
    	$html_print_head.=$html->ul_exhibitions($db,$options_ul_exh);
    //$html_print_head.="</div>";
	# END left side blocks

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/exhibitions";
	    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_EXHIBITIONS;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_EXHIBITIONS;
	    if( $_GET['section_1']!="search" )
	    {
	        if( $year_from==2020)
	        {
	            $html_years=LANG_EXH_ONWARDS;
	            if( !empty($_GET['year']) )
	            {
	                $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/exhibitions/".$year_from."-".$year_to;
	                $options_breadcrumb['items'][1]['title']=$html_years;
	            }
	            $options_breadcrumb['items'][1]['inner_html']=$html_years;
	            if( !empty($_GET['year']) ) $options_breadcrumb['items'][2]['inner_html']=$_GET['year'];
	        }
	        else if( is_numeric($year_from) && !empty($year_from) )
	        {
	            $html_years=$year_from." &#8211; ".$year_to;
	            if( !empty($_GET['year']) )
	            {
	                $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/exhibitions/".$year_from."-".$year_to;
	                $options_breadcrumb['items'][1]['title']=$html_years;
	            }
	            $options_breadcrumb['items'][1]['inner_html']=$html_years;
	            if( !empty($_GET['year']) ) $options_breadcrumb['items'][2]['inner_html']=$_GET['year'];
	        }
	        else $options_breadcrumb['items'][1]['inner_html']="-".LANG_HEADER_MENU_EXHIBITIONS;
	    }
	    else
	    {
	        $options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
	    }
        if( !empty($_GET['section_1']) && $mod_count>2 )
        {
            $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
            $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
        }
        else
        {
            $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
            $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
        }
        //print_r($_GET);
        //print $mod_count;
        //print_r($options_breadcrumb);
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_HEADER_MENU_EXHIBITIONS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/exhibitions/1.jpg' alt='' width='523' height='208' />";
            $html_print_body.="</div>\n";

            /*
            # search block
            $options_search_block=array();
            $options_search_block=$options_head;
            $options_search_block['html']=$html;
            $options_search_block['class']['div-search-block']="div-content-header-block-right";
            $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
            $options_search_block['more-options']="hidden";
            $options_search_block['url_form_action']="/".$_GET['lang']."/exhibitions/search/";
            $options_search_block['fields'][0]['type']="text";
            $options_search_block['fields'][0]['name']="title";
            $options_search_block['fields'][0]['id']="title_exhibition";
            $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
            $options_search_block['fields'][0]['class']="input-text-field";
            $options_search_block['fields'][1]['type']="text";
            $options_search_block['fields'][1]['name']="location";
            $options_search_block['fields'][1]['id']="location_exhibition";
            $options_search_block['fields'][1]['placeholder']=LANG_RIGHT_SEARCH_EXH_LOCATION;
            $options_search_block['fields'][1]['class']="input-text-field";
            $options_search_block['fields'][2]['type']="text";
            $options_search_block['fields'][2]['name']="keyword";
            $options_search_block['fields'][2]['id']="keyword_exhibition";
            $options_search_block['fields'][2]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
            $options_search_block['fields'][2]['class']="input-text-field";
            $options_search_block['fields'][3]['type']="select-year-from-to";
            $html_print_body.=$html->div_search_block($db,$options_search_block);
            # END search block
            */

            # search block
            $options_search_block=array();
            $options_search_block['options_head']=$options_head;
            $html_print_body.=$html->search_form_exhibitions($db,$options_search_block);
            # END search block

            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

	    # Main content

	    $query_where_exhibitions=" WHERE ";

	    # SEARCH
	    if( !empty($_GET['title']) || !empty($_GET['location']) || !empty($_GET['keyword']) )
	    {
	        $url_search="";

	        $query_where_exhibitions.=" ( ";

	        # add years from TITLE search
	        if( !empty($_GET['title']) || !empty($_GET['keyword']) )
	        {
	            if( !empty($_GET['keyword']) )
	            {
	                $title_search_term=$_GET['keyword'];
	                $keyword_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['keyword']));
	                $url_search.="&keyword=".$keyword_for_url;
	            }
	            else
	            {
	                $title_search_term=$_GET['title'];
	                $title_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['title']));
	                $url_search.="&title=".$title_for_url;
	            }

	            # prepare search keyword
	            $values_search=array();
	            $values_search['db']=$db;
	            $prepear_search=UTILS::prepear_search($title_search_term,$values_search);
	            # END prepare search keyword

	            # prepare match
	            $values_search_exh=array();
	            $values_search_exh['search']=$prepear_search;
	            $query_match_exh=QUERIES_SEARCH::query_search_exhibitions($db,$values_search_exh);
	            $query_search_title=" ".$query_match_exh;
	            # END prepare match

	            $query_where_exhibitions.=$query_search_title;

	            preg_match("/^(19|20)\d{2}$/", $_GET['keyword'], $matches_years);
	            $search_year=$matches_years[0];
	            if( !empty($search_year) ) $query_where_exhibitions.=" OR DATE_FORMAT(date_from, '%Y') BETWEEN '".$search_year."' AND '".$search_year."' ";

	        }
	        #end

	        # add years from location search
	        if( !empty($_GET['location']) || !empty($_GET['keyword']) )
	        {

	            if( !empty($_GET['keyword']) )
	            {
	                $location_search_term=$_GET['keyword'];
	                $and=" OR ";
	            }
	            else
	            {
	                $location_search_term=$_GET['location'];
	                $location_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['location']));
	                $url_search.="&location=".$location_for_url;
	                if( !empty($_GET['title']) || !empty($_GET['keyword']) ) $and=" AND ";
	            }

	            #LOCATION

	            # prepare search keyword
	            $values_search=array();
	            $values_search['db']=$db;
	            $prepear_search=UTILS::prepear_search($location_search_term,$values_search);
	            # END prepare search keyword

	            # prepare match
	            $values_search_locations=array();
	            $values_search_locations['search']=$prepear_search;
	            $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$values_search_locations);
	            $query_where_locations=" WHERE ".$query_match_locations;
	            # END prepare match

	            $query_locations=QUERIES::query_locations($db,$query_where_locations);
	            $results_locations=$db->query($query_locations['query_without_limit']);
	            $count_locations=$db->numrows($results_locations);
	            if( $_SESSION['debug_page'] ) print "<br /><br />".$query_where_locations;
	            if( $count_locations>0 )
	            {
	                while( $row_locations=$db->mysql_array($results_locations) )
	                {
	                    $results_loc_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE locationid='".$row_locations['locationid']."'");
	                    $count_loc_exh=$db->numrows($results_loc_exh);
	                    if( $count_loc_exh>0 )
	                    {
	                        while( $row_loc_exh=$db->mysql_array($results_loc_exh) )
	                        {
	                            $locations_exhibitions[$row_loc_exh['exID']]=$row_loc_exh['exID'];
	                        }
	                    }
	                }
	            }

	            # LOCATION CITY

	            # prepare match
	            $values_search_locations_city=array();
	            $values_search_locations_city['search']=$prepear_search;
	            $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$values_search_locations_city);
	            $query_where_locations_city=" WHERE ".$query_match_locations_city;
	            # END prepare match

	            $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
	            $results_locations_city=$db->query($query_locations_city['query_without_limit']);
	            $count_locations_city=$db->numrows($results_locations_city);
	            if( $_SESSION['debug_page'] ) print "<br /><br />".$query_where_locations_city;
	            if( $count_locations_city>0 )
	            {
	                while( $row_locations_city=$db->mysql_array($results_locations_city) )
	                {
	                    $results_loc_city_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE cityid='".$row_locations_city['cityid']."'");
	                    $count_loc_city_exh=$db->numrows($results_loc_city_exh);
	                    if( $count_loc_city_exh>0 )
	                    {
	                        while( $row_loc_city_exh=$db->mysql_array($results_loc_city_exh) )
	                        {
	                            $locations_exhibitions[$row_loc_city_exh['exID']]=$row_loc_city_exh['exID'];
	                        }
	                    }
	                }
	            }

	            # LOCATION COUNTRY

	            # prepare match
	            $values_search_locations_country=array();
	            $values_search_locations_country['search']=$prepear_search;
	            $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$values_search_locations_country);
	            $query_where_locations_country=" WHERE ".$query_match_locations_country;
	            # END prepare match

	            $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
	            $results_locations_country=$db->query($query_locations_country['query_without_limit']);
	            $count_locations_country=$db->numrows($results_locations_country);
	            if( $_SESSION['debug_page'] ) print "<br /><br />".$query_where_locations_country;
	            if( $count_locations_country>0 )
	            {
	                while( $row_locations_country=$db->mysql_array($results_locations_country) )
	                {
	                    $results_loc_country_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE countryid='".$row_locations_country['countryid']."'");
	                    $count_loc_country_exh=$db->numrows($results_loc_country_exh);
	                    if( $count_loc_country_exh>0 )
	                    {
	                        while( $row_loc_country_exh=$db->mysql_array($results_loc_country_exh) )
	                        {
	                            $locations_exhibitions[$row_loc_country_exh['exID']]=$row_loc_country_exh['exID'];
	                        }
	                    }
	                }
	            }

	            if( !empty($locations_exhibitions) )
	            {
	                $query_where_exhibitions.=$and." ( ";
	                $i_loc_exh=0;
	                foreach( $locations_exhibitions as $exhibitionid )
	                {
	                    if( $i_loc_exh==0 ) $or_loc="";
	                    else $or_loc=" OR ";
	                    $query_where_exhibitions.=$or_loc." exID='".$exhibitionid."' ";
	                    $i_loc_exh++;
	                }
	                $query_where_exhibitions.=" ) ";
	            }
	            elseif( !empty($_GET['location']) ) $query_where_exhibitions.=$and." exID=-1 ";

	        }
	        #end

	        $query_where_exhibitions.=" ) ";
	    }

	    if( !empty($_GET['year-from']) || !empty($_GET['year-to']) || !empty($year_from) || !empty($year_to) )
	    {
	        # year
	        if( !empty($_GET['title']) || !empty($_GET['keyword']) || !empty($locations_exhibitions) || !empty($_GET['location']) ) $and=" AND ";
	        if( !empty($_GET['year-from']) && !empty($_GET['year-to']) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y') BETWEEN '".$_GET['year-from']."' AND '".$_GET['year-to']."' ";
	            $url_search.="&year-from=".$_GET['year-from']."&year-to=".$_GET['year-to'];
	        }

	        if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')>='".$_GET['year-from']."' ";
	            $url_search.="&year-from=".$_GET['year-from'];
	        }
	        if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')<='".$_GET['year-to']."' ";
	            $url_search.="&year-to=".$_GET['year-to'];
	        }

            /*
            if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
            {
                $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')='".$_GET['year-from']."' ";
                $url_search.="&year-from=".$_GET['year-from'];
            }
            if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
            {
                $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')='".$_GET['year-to']."' ";
                $url_search.="&year-to=".$_GET['year-to'];
            }
            */

	        if( !empty($year_from) && !empty($year_to) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y') BETWEEN '".$year_from."' AND '".$year_to."' ";
	        }
            /*
	        if( !empty($year_from) && empty($year_to) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')>='".$year_from."' ";
	        }
	        if( empty($year_from) && !empty($year_to) )
	        {
	            $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')<='".$year_to."' ";
	        }
            */
            if( !empty($year_from) && empty($year_to) )
            {
                $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')='".$year_from."' ";
            }
            if( empty($year_from) && !empty($year_to) )
            {
                $query_where_exhibitions.=$and." DATE_FORMAT(date_from, '%Y')='".$year_to."' ";
            }
	        # end year
	    }
	    #end SEARCH

	    $query_where_exhibitions_solo=$query_where_exhibitions." AND e.typeid=1 AND e.enable=1 ";
	    $query_where_exhibitions_group=$query_where_exhibitions." AND e.typeid=2 AND e.enable=1 ";

	    if( $_SESSION['debug_page'] ) $html_print_body.="<br /><br />".$query_where_exhibitions_solo;
	    if( $_SESSION['debug_page'] ) $html_print_body.="<br /><br />".$query_where_exhibitions_group;

	    $query_where_solo=$query_where_exhibitions_solo;
	    $query_where_group=$query_where_exhibitions_group;

	    $query_order=" ORDER BY e.date_from ASC ";

	    $query_solo=QUERIES::query_exhibition($db,$query_where_solo,$query_order);
	    $results_solo=$db->query($query_solo['query']);
	    $count_solo=$db->numrows($results_solo);

	    $query_group=QUERIES::query_exhibition($db,$query_where_group,$query_order);
	    $results_group=$db->query($query_group['query']);
	    $count_group=$db->numrows($results_group);

	    if( $_SESSION['debug_page'] ) $html_print_body.="<br /><br />".$count_solo;
	    if( $_SESSION['debug_page'] ) $html_print_body.="<br /><br />".$count_group;

	    $i_exhibitions=0;
	    $exhibitions=array();
	    if( $count_solo )
	    {
	        while( $row=$db->mysql_array($results_solo) )
	        {
	            $exhibitions[$row['date_year']][1][$i_exhibitions]=$row;
	            $i_exhibitions++;
	        }
	    }

	    if( $count_group )
	    {
	        while( $row=$db->mysql_array($results_group) )
	        {
	            $exhibitions[$row['date_year']][2][$i_exhibitions]=$row;
	            $i_exhibitions++;
	        }
	    }

	    if( isset($_GET['title']) || isset($_GET['location']) || isset($_GET['keyword']) || isset($_GET['year-from']) || isset($_GET['year-to']) ) ksort($exhibitions);
	    ksort($exhibitions);

	    if( !empty($exhibitions) )
	    {
	        # calculate decades
	        foreach( $exhibitions as $key => $year )
	        {
	            $yr3=substr($key,0,3);
	            $min=$yr3.'0';
	            $decades[$min][]=$key;
	        }
	        $count_decades=count($decades);
	        # END calculate decades

	        # decades tabs
	        if( empty($_GET['decade']) && $_GET['section_1']=="search" )
	        {
	        	$_GET['decade']=end(@array_keys($decades));
	        }
			elseif( empty($_GET['decade']) )
			{
	        	$tmp=explode("-", $_GET['section_1']);
	        	$_GET['decade']=$tmp[0];
	    	}

	        $ul_decades_html="";
	        $ul_decades_html.="\t<ul class='ul-exh-decades-tabs ul-tabs'>\n";
	            $selected="";
	            $i=0;
	            foreach( $decades as $decade => $year )
	            {
	                //$html_print_body.=$decade."-";
	                //$html_print_body.=$year."-";
	                $i++;
	                //if( $i==1 && empty($_GET['decade']) ) $_GET['decade']=$decade;
	                # selected
	                if( $decade==$_GET['year'] || $decade==$_GET['decade'] ) $selected="selected";
	                else $selected="";
	                # END selected
	                # last
	                if( $i==count($decades) ) $last="last";
	                else $last="";
	                # END last
	                $ul_decades_html.="\t<li class='li-exh-decades-tab ".$last."'>\n";
	                    if( $_GET['section_1']=="search" )
	                    {
	                        $url="/".$_GET['lang']."/exhibitions/search/?decade=".$decade.$url_search;
	                    }
	                    else $url="/".$_GET['lang']."/exhibitions/".$decade."-".($decade+9)."/?year=".$decade;
	                    $ul_decades_html.="\t<a href=\"".$url."\" class='".$selected." ".$last."' title='".$decade."'>".$decade.LANG_EXH_DECADES_S."</a>\n";
	                $ul_decades_html.="\t</li>\n";
	            }
	        $ul_decades_html.="\t</ul>\n";

	        if( $count_decades>1 ) $html_print_body.=$ul_decades_html;
	        # END decades tabs

	        # decades tabs data
	        $html_print_body.="<div class='clearer'></div>\n";
	        if( $count_decades>1 ) $html_print_body.="<div class='div-exhibitions-decades'>\n";

	        # year tabs
	        $ul_years_html="";
	        $ul_years_html.="\t<ul class='ul-tabs'>\n";
	            $selected="";
	            $i=0;
	            foreach( $decades[$_GET['decade']] as $key => $year )
	            {
	                //$html_print_body.=$key."-";
	                //$html_print_body.=$year."-";
	                $i++;
	                if( $i==1 && empty($_GET['year']) && $_GET['decade']==2010 ) $_GET['year']=end($decades[$_GET['decade']]);
	                else if( $i==1 && empty($_GET['year']) ) $_GET['year']=$year;
	                # selected
	                if( $year==$_GET['year'] || $year==$_GET['year'] ) $selected="selected";
	                else $selected="";
	                # END selected
	                # last
	                if( $i==count($decades[$_GET['decade']]) ) $last="last";
	                else $last="";
	                # END last
	                $ul_years_html.="\t<li class='li-exh-years-tab ".$last." ".$selected."'>\n";
	                    if( $_GET['section_1']=="search" )
	                    {
	                        $url="/".$_GET['lang']."/exhibitions/search/?decade=".$_GET['decade']."&year=".$year.$url_search;
	                    }
	                    else $url="/".$_GET['lang']."/exhibitions/".$_GET['section_1']."/?year=".$year;
	                    $ul_years_html.="\t<a href=\"".$url."\" class='".$selected." ".$last."' title='".$year."'>".$year."</a>\n";
	                $ul_years_html.="\t</li>\n";
	            }
	        $ul_years_html.="\t</ul>\n";
	        # END year tabs

	        # SOLO exhibitions data
	        $count_solo_from_array=count($exhibitions[$_GET['year']][1]);
	        if( $count_solo_from_array>0 )
	        {
	            if( $count_group_from_array>0 ) $style="border-bottom:1px solid #999999;";
	            else $style="";

	            $html_print_body.=$ul_years_html;

	            $html_print_body.="<div class='clearer'></div>\n";
	            $html_print_body.="<div class='div-exhibitions' style='".$style."'>\n";
	                $html_print_body.="<div class='div-exhibitions-head' >\n";
	                    $text=LANG_SOLO_EXHS." (".$count_solo_from_array.")";
	                    /*
	                    if( $count_solo_from_array==1 ) $text=LANG_SOLO_EXH." (".$count_solo_from_array.")";
	                    else $text=LANG_SOLO_EXHS." (".$count_solo_from_array.")";
	                    */
	                    $html_print_body.="<h3>".$text."</h3>\n";
	                    #sort by select drop down list
	                    /*
	                    $html_print_body.="<div class='div-exh-order-by'>\n";
	                        $html_print_body.="<span class='span-exh-order-by-text'>".LANG_EXH_SORT_BY."</span>\n";
	                        $html_print_body.="<select name='sort_by'>\n";
	                            $html_print_body.="<option values='date'>".LANG_EXH_SORT_BY_DATE."</option>\n";
	                        $html_print_body.="</select>\n";
	                    $html_print_body.="</div>\n";
	                    */
	                    # END sort by select drop down list
	                $html_print_body.="</div>\n";
	                $html_print_body.="<div class='clearer'></div>\n";

	                if( is_array($exhibitions[$_GET['year']][1]) )
	                {
	                    $i=0;
	                    foreach( $exhibitions[$_GET['year']][1] as $row )
	                    {
	                        $i++;
	                        $options_exhibitions=array();
	                        $options_exhibitions=$options_head;
	                        $options_exhibitions['titleurl']=$_GET['year'];
	                        $options_exhibitions['type']=1;
	                        $options_exhibitions['row']=$row;
	                        $options_exhibitions['i']=$i;
	                        $options_exhibitions['count']=$count_solo_from_array;
                            /*
                            $options_exhibitions['class']['span-title-orig-length']="span-exh-list-title-original-short";
                            $options_exhibitions['class']['span-title-trans-length']="span-exh-list-title-translation-short";
                            $options_exhibitions['class']['span-location-length']="span-exh-list-location-short";
                            $options_exhibitions['class']['span-date-length']="span-exh-list-date-short";
                            */
                            $options_exhibitions['class']['div-exhibition-right-side-search']="div-exhibition-right-side-search";
                            $options_exhibitions['class']['a-exhibition-list-search']="a-exhibition-list-search";
                            $options_exhibitions['class']['div-exhibition-search']="div-exhibition-search";
                            $options_exhibitions['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                            $options_exhibitions['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                            $options_exhibitions['class']['span-location-length']="span-exh-list-location-wide";
                            $options_exhibitions['class']['span-date-length']="span-exh-list-date-wide";
	                        $html_print_body.=$html->div_exhibition($db,$options_exhibitions);
	                    }
	                }
	            $html_print_body.="</div>\n";
	            $html_print_body.="<div class='clearer'></div>\n";
	        }
	        # END SOLO exhibitions data

	        # GROUP exhibitions data
	        $count_group_from_array=count($exhibitions[$_GET['year']][2]);
	        if( $count_group_from_array>0 )
	        {
	            if( !$count_solo || $count_solo_from_array<=0 ) $html_print_body.=$ul_years_html;
	            //$html_print_body.="<br />";

	            $html_print_body.="<div class='clearer'></div>\n";
	            $html_print_body.="<div class='div-exhibitions'>\n";
	                $html_print_body.="<div class='div-exhibitions-head' >\n";
	                    $text=LANG_GROUP_EXHS." (".$count_group_from_array.")";
	                    /*
	                    if( $count_group_from_array==1 ) $text=LANG_GROUP_EXH." (".$count_group_from_array.")";
	                    else $text=LANG_GROUP_EXHS." (".$count_group_from_array.")";
	                    */
	                    $html_print_body.="<h3>".$text."</h3>\n";
	                $html_print_body.="</div>\n";
	                $html_print_body.="<div class='clearer'></div>\n";
	                if( is_array($exhibitions[$_GET['year']][2]) )
	                {
	                    $i=0;
	                    foreach( $exhibitions[$_GET['year']][2] as $row )
	                    {
	                        $i++;
	                        $options_exhibitions=array();
	                        $options_exhibitions=$options_head;
	                        $options_exhibitions['titleurl']=$_GET['year'];
	                        $options_exhibitions['type']=2;
	                        $options_exhibitions['row']=$row;
	                        $options_exhibitions['i']=$i;
	                        $options_exhibitions['count']=$count_group_from_array;
                            /*
                            $options_exhibitions['class']['span-title-orig-length']="span-exh-list-title-original-short";
                            $options_exhibitions['class']['span-title-trans-length']="span-exh-list-title-translation-short";
                            $options_exhibitions['class']['span-location-length']="span-exh-list-location-short";
                            $options_exhibitions['class']['span-date-length']="span-exh-list-date-short";
                            */
                            $options_exhibitions['class']['div-exhibition-right-side-search']="div-exhibition-right-side-search";
                            $options_exhibitions['class']['a-exhibition-list-search']="a-exhibition-list-search";
                            $options_exhibitions['class']['div-exhibition-search']="div-exhibition-search";
                            $options_exhibitions['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                            $options_exhibitions['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                            $options_exhibitions['class']['span-location-length']="span-exh-list-location-wide";
                            $options_exhibitions['class']['span-date-length']="span-exh-list-date-wide";
	                        $html_print_body.=$html->div_exhibition($db,$options_exhibitions);
	                    }
	                }
	            $html_print_body.="</div>\n";
	            $html_print_body.="<div class='clearer'></div>\n";
	        }
	        # END GROUP exhibitions data





	        if( $count_decades>1 ) $html_print_body.="</div>\n";
	        # END decades tabs data

	    }

	    $html_print_body.="<a class='top exhibitions-list-back-to-top' href='#top'>".LANG_BACKTOTOP."</a>\n";

	    if( !$count_solo && !$count_group ) $html_print_body.=LANG_SEARCH_NORESULTS;

        #END div main content

}
# *** END *** MAIN exhibitions SECTION /exhibitions

else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
