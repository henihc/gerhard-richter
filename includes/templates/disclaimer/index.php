<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html_print_head="";
$html_print_body="<div id='sub-nav'>&nbsp;</div>\n";
$html_print_body.="<div id='div-large-content' role='main' data-class=''>";
$html_print_footer="";

$html = new html_elements;
$html->title=LANG_FOOTER_DISCLIMER." &raquo; ".LANG_TITLE_HOME;
$html->lang=$_GET['lang'];
$html->css[]="/css/main.css";
$html->css[]="/css/screen.css";
$html->css[]="/css/main.mobile.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

$options_head=array();
//$options_head['meta_description']=;
$options_head['html_return']=1;
$options_head['class']['body']="body";
$html_print_head.=$html->head($db,'',$options_head);

$textid=2;

# select info about section
$values_query_text=array();
$values_query_text['where']=" WHERE textid='".$textid."' ";
$query_text=QUERIES::query_text($db,$values_query_text);
$results=$db->query($query_text['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);
$values_row_text=array();
$values_row_text['search']=$_GET['search'];
$text=UTILS::row_text_value($db,$row,"text",$values_row_text);

# div main content

if( $_GET['section_0']=="disclaimer" && empty($_GET['section_1']) )
{
    ### Breadcrumbs
    $options_breadcrumb=array();
    $options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['items'][0]['inner_html']=LANG_FOOTER_DISCLIMER;
    $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
    $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);

    $options_admin_edit=array();
    $options_admin_edit=$options_head;
    $html_print_body.=UTILS::admin_edit("/admin/pages/edit/?textid=".$textid,$options_admin_edit)."\n";

    $values_search_found=array();
    $values_search_found['text']=$text;
    $values_search_found['strip_tags']=1;
    $values_search_found['convert_utf8']=1;
    $values_search_found['search']=$_GET['search'];
    $text=UTILS::show_search_found_keywords($db,$values_search_found);

    $options_div_text=array();
    $html_print_body.=$html->div_text_wysiwyg($db,$text,$options_div_text);
}
else
{
    UTILS::not_found_404();
}


$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$options_foot['selected']=3;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>