<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['get']['sp']=6;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="$(document).ready(function() {\n";
        //$html_print_body.="$('#input-basic-search').focusEnd();\n";

        $html_print_body.="var el = $('#input-basic-search').get(0);";
        $html_print_body.="var elemLen = el.value.length;";

        $html_print_body.="el.selectionStart = elemLen;";
        $html_print_body.="el.selectionEnd = elemLen;";
        $html_print_body.="el.focus();";

    $html_print_body.="});\n";
    $html_print_body.="$(function() {\n";
        $html_print_body.="$('#div-search-loader').html(\"<img src='/g/loading.gif' alt='' class='img-search-loader' />\");\n";
        //$html_print_body.="$('#div-basic-search-results').load('search.php?search=".urldecode($_GET['search'])."', function() { ";
        $html_print_body.="$('#div-basic-search-results').load('/includes/search/search.php?search=".rawurlencode($_GET['search'])."&lang=".$_GET['lang']."', function() { ";
            $html_print_body.="$('#div-search-loader').html('');";
        $html_print_body.="});\n";
        //$html_print_body.="pageurl = \"/search/?search=\"+encodeURI('".$_GET['search']."');\n";
        //$html_print_body.="window.history.pushState({path:pageurl},'',pageurl);\n";

    $html_print_body.="});\n";
$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

if( !empty($_GET['section_1']) ) UTILS::not_found_404();

# title meta
$html->title=LANG_HEADER_MENU_SEARCH." &raquo; ".LANG_TITLE_HOME;
$html->lang=$_GET['lang'];

# css & js files
$html->css[]="/css/main.css";
$html->css[]="/css/screen.css";
$html->css[]="/css/main.mobile.css";
$html->css[]="/css/search.css";
$html->css[]="/css/books.css";
$html->css[]="/css/exhibitions.css";
$html->css[]="/css/quotes.css";
$html->css[]="/css/videos.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

# tabs and tooltips
$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

# autocomplete
$html->css[]="/js/jquery.autocomplete/css/jquery.autocomplete.css";
//$html->js[]="/js/jquery.autocomplete/jquery.min.js";
$html->js[]="/js/jquery.autocomplete/jquery.autocomplete.js";
$html->js[]="/js/jquery.autocomplete/jquery.autocomplete.load.js";

# fancybox
$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
$html->js[]="/js/jquery_fancybox/fancybox.load.js";

# tabs and tooltips
//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

#expand table
$html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";

# meta-tags & head
$options_head=array();
//$options_head['meta_description']=LANG_META_DESCRIPTION_VIDEOS;
$options_head['html_return']=1;
$options_head['search_page']=1;
$options_head['class']['body']="body";
$options_head['class']['language']="language_search";
$html_print_head.=$html->head($db,'',$options_head);

# left side blocks
$html_print_head.="<div id='sub-nav'>";
$html_print_head.="</div>\n";
# END left side blocks

# div main content

    ### Breadcrubmles
    $options_breadcrumb=array();
   	$options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['class']['div']="breadcrumb-large";
    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/search";
    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_SEARCH;
    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_SEARCH;
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
    #end 

	$html_print_body.="<form action='/".$_GET['lang']."/search/' method='get' id='form-basic-search'>\n";
        $html_print_body.="<h1 class='h1-section h1-section-search'>".LANG_HEADER_MENU_SEARCH."</h1>";
        #$html_print_body.="<a href='#' title='' class='a-search-show-more-opt'>".LANG_SEARCH_SHOW_MORE_OPTIONS."</a>";
	    $value=$_GET['search'];
	    $title_search=str_replace('"', "&#34;", $value);
	    $title_search=stripslashes(stripslashes($title_search));
	    $html_print_body.="<input type='text' name='search' value=\"".$title_search."\" id='input-basic-search' autocomplete='off' placeholder='".LANG_HEADER_MENU_SEARCH."' />\n";
	    $html_print_body.="<input type='button' value='".LANG_HEADER_MENU_SEARCH."' class='submit-basic-search' id='button-submit-basic-search' />\n";
	    $html_print_body.="<div id='div-search-loader'>\n";
	        //$html_print_body.="<img src='/g/loading.gif' alt='' class='img-search-loader' />\n";
	    $html_print_body.="</div>\n";
	$html_print_body.="</form>\n";
    $html_print_body.="<div class='div-form-basic-search-bottom'></div>\n";
	 
	$html_print_body.="<div id='div-basic-search-results' role='main'>\n";
	    //$html_print_body.="Type to search!\n";
	$html_print_body.="</div>\n";

#END div main content

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
