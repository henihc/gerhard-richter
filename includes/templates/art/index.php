<?php
session_start();

require ($_SERVER["DOCUMENT_ROOT"]."/includes/search-art.inc.php");

$html_return="";
$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
if( empty($_GET['sp']) ) $options_get['get']['sp']=32;
$_GET=UTILS::if_empty_get($db,$options_get);

# which artwork it is
$artwork_1=array();
if( $_GET['section_1']=="paintings" )
{
	$artwork_1['title']=LANG_LEFT_SIDE_PAINTINGS;
	$artwork_1['row']['artworkID']="Paintings";
	$artworkid_search="paintings";
}
else
{
	$options_artwork_info=array();
	$options_artwork_info['titleurl']=$_GET['section_1'];
	$artwork_1=UTILS::get_artwork_data($db,$options_artwork_info);
	$artworkid_search=$artwork_1['row']['artworkID'];
}

//print_r($options_artwork_info);
//print_r($artwork_1);

$options_head=array();
$options_head['html_return']=1;
$options_head['meta']['viewport']=1;
$class=array();
$html_print_head="";
$html_print_body.="<div id='div-large-content' role='main' >";
$html_print_footer="";

# javascript
$html_script="";
$html_script.="<script type='text/javascript'>\n";
    $html_script.="var search=0;\n";
    $html_script.="$(function() {";

        # tooltips load
        $html_script.="$( document ).tooltip({";
            $html_script.="items: '[data-title]',";
            $html_script.="content: $('.div-tooltips-info-content').html(),";
            $html_script.="tooltipClass: 'div-tooltips-search-help'";
            //$html_script.="hide: {duration: 1000000 }"; //for debuging
        $html_script.="});";
        # END tooltips load

        # select box pretty
        $html_script.="$('.select-field, .select_pp').selectBoxIt({";
            //$html_script.="showFirstOption: false";
        $html_script.="});";

		//$html_script.="var selectBox = $('.select-field, .select_pp').selectBoxIt({";
			//$html_script.="showFirstOption: false";
		//$html_script.="});";


        //$html_script.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
        //$html_script.="selectBox.selectOption(0);";

        # show more less search options
        $html_script.="$.fn.toggleText = function(t1, t2){
          if (this.text() == t1) this.text(t2);
          else                   this.text(t1);
          return this;
        };";
        $html_script.="$( '.a-right-search-show-more-options' ).click(function() {";
            $html_script.="$( '.div-content-header-block-right' ).toggleClass( 'div-content-header-block-right-large' );";
            $html_script.="$( this ).toggleClass( 'a-right-search-show-more-options-less' );";
            $html_script.="$( this ).toggleText(\"".LANG_LITERATURE_SHOW_LESS_OPTIONS."\", \"".LANG_LITERATURE_SHOW_MORE_OPTIONS."\");";
            $html_script.="$( '.div-content-header-block-right-more-options' ).toggleClass( 'show' );";
        $html_script.="});";

		if( $_GET['section_1']=="overpainted-photographs" )
		{
			session_start();

	    	# opp form validation scripts
	        $html_script.="$('.form-opp-submit-info').on('submit', function(e) {";
	            $html_script.="var error=0;";
	            $html_script.="var error_codenumb=0;";

	            # first name
	            $html_script.="if( $('#first_name').val().length == 0  ){";
	                //$html_script.="console.log('first_name');";
	                $html_script.="error=1;";
	                $html_script.="$('#first_name_text').css( 'color', 'red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('#first_name_text').css( 'color', '#666666' );";
	            $html_script.="}";

	            # last name
	            $html_script.="if( $('#last_name').val().length == 0  ){";
	                //$html_script.="console.log('last_name');";
	                $html_script.="error=1;";
	                $html_script.="$('#last_name_text').css( 'color', 'red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('#last_name_text').css( 'color', '#666666' );";
	            $html_script.="}";

	            # email
	            $html_script.="if( $('#email').val().length == 0  ){";
	                //$html_script.="console.log('email');";
	                $html_script.="error=1;";
	                $html_script.="$('#email_text').css( 'color', 'red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('#email_text').css( 'color', '#666666' );";
	            $html_script.="}";

	            # Credit / courtesy
	            $html_script.="if( $('#indicate_credit').val().length == 0  ){";
	                //$html_script.="console.log('first_name');";
	                $html_script.="error=1;";
	                $html_script.="$('#indicate_credit_text').css( 'color', 'red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('#indicate_credit_text').css( 'color', '#666666' );";
	            $html_script.="}";

	            # codenumb
	            $html_script.="if( $('#codenumb').val().length == 0  ){";
	                //$html_script.="console.log('codenumb');";
	                $html_script.="error=1;";
	                $html_script.="error_codenumb=1;";
	                $html_script.="$('#codenumb_error').css( 'color', 'red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('#codenumb_error').css( 'color', '#666666' );";
	            $html_script.="}";

	            # agree
	            $html_script.="if( !$('#agree').is(':checked') ){";
	                //$html_script.="console.log('agree');";
	                $html_script.="error=1;";
	                $html_script.="$('.td-agree-left').css( 'border-left', '1px solid red' );";
	                $html_script.="$('.td-agree-left').css( 'border-top', '1px solid red' );";
	                $html_script.="$('.td-agree-left').css( 'border-bottom', '1px solid red' );";
	                $html_script.="$('.td-agree-right').css( 'border-right', '1px solid red' );";
	                $html_script.="$('.td-agree-right').css( 'border-top', '1px solid red' );";
	                $html_script.="$('.td-agree-right').css( 'border-bottom', '1px solid red' );";
	            $html_script.="}";
	            $html_script.="else{";
	                $html_script.="$('.td-agree-left').css( 'border-left', '1px solid #666' );";
	                $html_script.="$('.td-agree-left').css( 'border-top', '1px solid #666' );";
	                $html_script.="$('.td-agree-left').css( 'border-bottom', '1px solid #666' );";
	                $html_script.="$('.td-agree-right').css( 'border-right', '1px solid #666' );";
	                $html_script.="$('.td-agree-right').css( 'border-top', '1px solid #666' );";
	                $html_script.="$('.td-agree-right').css( 'border-bottom', '1px solid #666' );";
	            $html_script.="}";

	            # submiting form
	            # Get some values from elements on the page:
	            $html_script.="var form = $( '.form-opp-submit-info' ),";
	            $html_script.="url = form.attr( 'action' );";


	            $html_script.="if( error ) {";
	                $html_script.="e.preventDefault();";
	                $html_script.="alert('".LANG_OPP_FORM_75_2."');";
	            $html_script.="}";

	            # else allow to submit form
	            $html_script.="else {";
	                //$html_script.="console.log('allowing to submit form');";
	            $html_script.="}";

	            # sending email to admin with error in subject some fields where left empty but capcha was ok so possibly human
	            $html_script.="if( error && !error_codenumb ) {";
	                # Send the data using post
	                $html_script.="var posting = $.post( url, $('.form-opp-submit-info').serialize())";
	            $html_script.="}";

	        $html_script.="});";

	        # for capcha reload button
	        $html_script.="$('#td-image-capcha').click(function(){";
	            $html_script.="var d = new Date();";
	            $html_script.="$('#img-capcha-opp').attr('src', $('#img-capcha-opp').attr('src') + '?' + d.getMilliseconds());";
	        $html_script.="});";

	        # load calendar picker
	        $html_script.="$( '#date, #date_of_acquisition' ).datepicker(";
	            $html_script.="{";
	                $html_script.="changeMonth: true,";
	                $html_script.="changeYear: true,";
	                $html_script.="showButtonPanel: true,";
	                $html_script.="dateFormat: 'dd/mm/yy',";
	                $html_script.="yearRange: '1986:'+(new Date().getFullYear())";
	            $html_script.="}";
	        $html_script.=");";
			# END opp form validation scripts
		}

    $html_script.="});";
$html_script.="</script>\n";
# END javascript

# header block
function html_header_block($db,$options=array())
{
	$html_header="";
	$html_header.="<div class='div-content-header-block' >";
	    # section image
	    $html_header.="<div class='div-content-header-block-left' >";
	        if( !empty($options['title_category_sub']) ) $h1_title=$options['title_category_sub'];
	        elseif( !empty($options['artwork_1']['title']) ) $h1_title=$options['artwork_1']['title'];
	        else $h1_title=LANG_HEADER_MENU_ARTWORK;
	        $html_header.="<h1 class='h1-section'>".$h1_title."</h1>";
	        if( !empty($options['src_header']) )
	        {
	        	$src_header=$options['src_header'];
	        }
	        else
	        {
	        	$src_header="/g/headers/artwork/1.jpg";
	        }
	        $html_header.="<img src='".$src_header."' alt='' width='523' height='208' />";
	    $html_header.="</div>\n";

	    # search block
	    $options_search_block=array();
	    $options_search_block=$options;
	    $html_header.=$options['html']->search_form_art($db,$options_search_block);
	    # END search block

	    $html_header.="<div class='clearer'></div>";
	$html_header.="</div>\n";

	return $html_header;
}
# END header block

############################ art/paintings/photo_paintings/.... - DETAIL PAINTING VIEW
if(
	# for paintings & opp thumbnail view
	(
		( $_GET['section_1']=="paintings" ) && !empty($_GET['section_2']) && !empty($_GET['section_4'])
		||
		( $_GET['section_1']=="overpainted-photographs" ) && !empty($_GET['section_2']) && !empty($_GET['section_3'])
		||
		( $_GET['section_1']=="other" ) && !empty($_GET['section_2']) && !empty($_GET['section_3'])
	) ||
	# for all other non category section thumbnail views
	(
		( $_GET['section_1']=="atlas" || $_GET['section_1']=="editions" || $_GET['section_1']=="drawings" || $_GET['section_1']=="oils-on-paper" || $_GET['section_1']=="watercolours" || $_GET['section_1']=="prints" )
		&& !empty($_GET['section_2'])
	)
)
{
	# which artwork it is
	$options_artwork_info=array();
	if( $_GET['section_1']=="paintings" )
	{
		$section=$_GET['section_2'];
		$titleurl_painting=$_GET['section_4'];
		if( $section!="photo-paintings" && $section!="abstracts" )
		{
			UTILS::not_found_404();
		}
	}
	elseif( $_GET['section_1']=="editions" && $_GET['section_3']=="individual-works" )
	{
		$section=$_GET['section_1'];
		$titleurl_edition=$_GET['section_2'];
		$paintid_edition=UTILS::itemid($db,$titleurl_edition);
		$titleurl_painting=$_GET['section_4'];
	}
	else
	{
		$section=$_GET['section_1'];
		$titleurl_painting=$_GET['section_2'];
	}
	$options_artwork_info['titleurl']=$section;
	$artwork_1=UTILS::get_artwork_data($db,$options_artwork_info);
	# if its an opp then get category title
		//print_r($artwork_1);
		if( $_GET['section_1']=="paintings" || $artwork_1['row']['artworkID']==13 || $artwork_1['row']['artworkID']==6 )
		{
			if( $_GET['section_1']=="paintings" )
			{
				$section=$_GET['section_3'];
				$titleurl_painting=$_GET['section_4'];
			}
			else
			{
				$section=$_GET['section_2'];
				$titleurl_painting=$_GET['section_3'];
			}
			$categoryid=UTILS::itemid($db,$section);
			$options_category_info=array();
			//$options_category_info['categoryid']=$categoryid;
			$options_category_info['section']=$section;
			$artwork_2=UTILS::get_category_data($db,$options_category_info);
			if( !$artwork_2['count'] ) UTILS::not_found_404();
		}
	# END which artwork it is

	# get paintid from url
	$paintid=UTILS::itemid($db,$titleurl_painting);
	$_GET['artworkID']=$artwork_1['row']['artworkID'];
	$_GET['categoryid']=$categoryid;
	$_GET['paintid']=$paintid;
	//print_r($_GET);
	//print_r($artwork_1);

	$title_meta="";

    ### main query to select painting info
    //$query_where_painting=" WHERE paintID='".$paintid."' ";
    $query_where_painting=" WHERE titleurl='".$titleurl_painting."' ";
    $query_where_painting.=" AND enable=1 ";
    $query_order_painting=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
    $query_painting=QUERIES::query_painting($db,$query_where_painting,$query_order_painting);
    $results_painting=$db->query($query_painting['query']);
    $count_painting=$db->numrows($results_painting);
    if( $count_painting>0 )
    {
        $row_painting=$db->mysql_array($results_painting,0);
        $title_painting=UTILS::row_text_value2($db,$row_painting,"title");
        $title_meta.=UTILS::html_decode($title_painting);
        if( !empty($row_painting['number']) ) $title_meta.=" [".$row_painting['number']."]";
    }
    else
    {
    	UTILS::not_found_404();
    }
    #end

    ### main query to select painting image
    $query_images="SELECT
        COALESCE(
            r1.itemid1,
            r2.itemid2
        ) as itemid,
        COALESCE(
            r1.sort,
            r2.sort
        ) as sort_rel,
        COALESCE(
            r1.relationid,
            r2.relationid
        ) as relationid_rel
        FROM
            ".TABLE_PAINTING." p
        LEFT JOIN
            ".TABLE_RELATIONS." r1 ON r1.typeid1 = 17 and r1.typeid2 = 1 and r1.itemid2 = p.paintID
        LEFT JOIN
            ".TABLE_RELATIONS." r2 ON r2.typeid2 = 17 and r2.typeid1 = 1 and r2.itemid1 = p.paintID
        WHERE p.paintID=".$row_painting['paintID']."
        ORDER BY sort_rel ASC, relationid_rel DESC
        LIMIT 1
    ";
    $results_related_image=$db->query($query_images);
    $row_related_image=$db->mysql_array($results_related_image);
    $imageid=$row_related_image['itemid'];
    # end

    ### querys for page numbering and selecting category paintings
    if( $_GET['referer']=="search-art" || $_GET['referer']=="search-main" || $_GET['referer']=="search" || $_GET['info']==1 )
    {
        $options_search_art=array();
        $options_search_art['column']="p.paintID,p.titleurl";
        $options_search_art['get']=$_GET;
        if( $_GET['referer']=="search" && is_numeric($options_search_art['get']['title']) )
        {
        	$options_search_art['get']['number']=$options_search_art['get']['title'];
        	unset($options_search_art['get']['title']);
        }
        $options_search_art['sp']=$_GET['sp'];
        $options_search_art['p']=$_GET['p'];
        $options_search_art['artworkid']=$_GET['artworkid'];
        $search_results=search_art($db,$options_search_art);
        $results_cat_paintings_total=$search_results['results_total'];
        $count_cat_paintings=$search_results['count'];
        if( $_SESSION['debug_page'] ) print_r($search_results);
	}
    else
    {
	    $query_where_cat_paintings=" WHERE artworkID='".$artwork_1['row']['artworkID']."' ";
	   	$query_where_cat_paintings.=" AND enable=1 AND catID='".$categoryid."' ";
	    if( $artwork_1['row']['artworkID']==4 ) $query_where_cat_paintings.=" AND edition_individual=0 ";
	    $query_order_cat_paintings=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
	    if( $_GET['sp']!='all' ) $query_limit_cat_paintings=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp']." ";
	    $options_query_paintings=array();
	    $options_query_paintings['column']="p.paintID,p.titleurl";
	    $query_cat_paintings=QUERIES::query_painting($db,$query_where_cat_paintings,$query_order_cat_paintings,$query_limit_cat_paintings,"",$options_query_paintings);
	    //print $query_cat_paintings['query'];
	    $results_cat_paintings=$db->query($query_cat_paintings['query']);
	    $results_cat_paintings_total=$db->query($query_cat_paintings['query_without_limit']);
	    $count_cat_paintings=$db->numrows($results_cat_paintings_total);
	}
    $pages=@ceil($count_cat_paintings/$_GET['sp']);
    #end

    ### Query to select info about category
    $query_where_category=" WHERE catID='".$categoryid."' AND enable=1 ";
    $query_order_category=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
    $query_category=QUERIES::query_category($db,$query_where_category,$query_order_category);
    $results_category=$db->query($query_category['query']);
    $count_category=$db->numrows($results_category);
    if( $count_category>0 )
    {
        $row_category=$db->mysql_array($results_category,0);
        $title_category=UTILS::row_text_value2($db,$row_category,"category");
        //$title_meta.=UTILS::html_decode($title_sub_category)." &raquo; ";
    }

    # if no results found show 404
	if( ( $count_painting<=0 || $count_cat_paintings<=0 ) && $_GET['referer']!="search-art" && $_GET['referer']!="search-main" && $_GET['referer']!="search" )
	{
		//print $count_painting."-".$count_cat_paintings;
		UTILS::not_found_404();
	}
	if( $count_category<=0 && ( $_GET['section_1']=="paintings" || $artwork_1['row']['artworkID']==13 || $artwork_1['row']['artworkID']==6 ) )
	{
		//print $count_category."-";
		UTILS::not_found_404();
	}
	//print_r($_GET);
	/*
	if( $_GET['section_3']!="individual-works" && $_GET['section_1']=="editions" )
	{
		UTILS::not_found_404();
	}
	*/
	if( !empty($_GET['section_5']) && ( $_GET['section_1']=="paintings" || $_GET['section_1']=="editions" ) )
	{
		UTILS::not_found_404();
	}
	elseif( !empty($_GET['section_4']) && $artwork_1['row']['artworkID']==6 )
	{
		UTILS::not_found_404();
	}
	elseif( !empty($_GET['section_3']) && ( $_GET['section_1']=="atlas" || $_GET['section_1']=="drawings" || $_GET['section_1']=="oils-on-paper" || $_GET['section_1']=="watercolours" || $_GET['section_1']=="prints" ) )
	{
		UTILS::not_found_404();
	}
	# END
	//print $count_painting."-".$count_cat_paintings."-".$categoryid."-".$artwork_1['row']['artworkID'];
	# title meta
	/*$title_meta.=$artwork_2['title'];
	if( !empty($artwork_2['title']) ) $title_meta.=" &raquo; ";
	$title_meta.=$artwork_1['title'];
	*/
	$title_meta.=" &raquo; ".LANG_HEADER_MENU_ARTWORK." &raquo; ".LANG_TITLE_HOME;
	$html->title=$title_meta;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.min.css";
	$html->css[]="/css/screen.min.css";
	$html->css[]="/css/main.mobile.min.css";
	$html->css[]="/css/tabs.min.css";
	$html->css[]="/css/exhibitions.min.css";
	$html->css[]="/css/books.min.css";
	$html->css[]="/css/page-numbering.min.css";
	$html->css[]="/css/painting_detail.min.css";
	$html->css[]="/css/painting-detail-view.min.css";
	$html->css[]="/css/videos.min.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.min.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

	# tabs and tooltips
	$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.min.css";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
	//$html->js[]="/js/jquery_fancybox/fancybox.load.js";
	$html->js[]="/js/jquery_fancybox/fancybox.load.min.php?lang=".$_GET['lang'];

	#expand table
	$html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.min.css";
	$html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.min.js";

	# history support
	$html->js[]="/js/history/jquery.history.js";

	# opp form css
	$html->css[]="/css/opp-form.min.css";

	# hotfix css
	$html->css[]="/css/hotfixes.css";

	# meta-tags & head
	$options_head['class']['body']="body";
	$options_head['class']['div-main-old']="div-main-old";

	# Facebook OG
	$options_head['facbook_metainfo']['type']="article";
	$options_head['facbook_metainfo']['title']=$title_meta;
	$options_head['facbook_metainfo']['image']=HTTP_SERVER."/datadir/images_new/xlarge/".$imageid.".jpg";
	//$options_head['facbook_metainfo']['updated_time']=;

	$html_print_head.=$html->head($db,'artwork',$options_head);

	$html_print_body.="<script type='text/javascript'>\n";

		# microsite preview
	    $html_print_body.="$(function() {";
	        # tooltips load
	        $html_print_body.="$( document ).tooltip({";
	            $html_print_body.="items: '[data-microsite-preview]',";
	            $html_print_body.="content: $('.div-tooltips-info-microsite').html(),";
	            $html_print_body.="tooltipClass: 'div-tooltips-microsite-preview',";
	            $html_print_body.="position: {";
		            $html_print_body.="my: 'center bottom-4',";
		            $html_print_body.="at: 'center top',";
		            //$html_print_body.="my: 'right bottom',";
		            //$html_print_body.="at: 'right bottom',";
		            //$html_print_body.="of: '#div-painting-info-button-box'";
		        $html_print_body.="},";
	            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
	        $html_print_body.="})";
	        # END tooltips load
	    $html_print_body.="});";
		# END microsite preview

	$html_print_body.="</script>\n";

	# left side blocks
	$options_left_side=array();
	$options_left_side=$options_head;
	$options_left_side['show_microsites']=1;
	$options_left_side['db']=$db;
	$html->left_side_menu_selected=$artwork_2['row']['artworkID'];
	$html_print_head.=$html->left_side($artwork_1['row']['artworkID'],$options_left_side);

	# div main content

        $url="/".$_GET['lang']."/art";

        # prepare search vars
        $search_vars=UTILS::search_vars_from_get($_GET);
        //print_r($search_vars);
        # END

        ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-painting-detail";
	    $options_breadcrumb['items'][0]['href']=$url;
	    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_ARTWORK;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_ARTWORK;

	    if( $_GET['section_1']=="paintings" )
	    {
	    	$url.="/paintings";
	    	$options_admin_edit=array();
	    	$options_admin_edit=$options_head;
	    	$options_breadcrumb['admin_edit']=UTILS::admin_edit("/admin/paintings/categories/edit/?catid=".$categoryid,$options_admin_edit);
	    	$options_breadcrumb['items'][1]['href']=$url;
	    	$options_breadcrumb['items'][1]['title']=LANG_SEE_ALL." ".LANG_LEFT_SIDE_PAINTINGS;
	    	$options_breadcrumb['items'][1]['inner_html']=LANG_LEFT_SIDE_PAINTINGS;
		    $options_breadcrumb['items'][2]['href']="/".$_GET['lang']."/art/paintings/#".$artwork_1['row']['titleurl'];
		    $options_breadcrumb['items'][2]['title']=LANG_SEE_ALL." ".$artwork_1['title'];
		    $options_breadcrumb['items'][2]['inner_html']=$artwork_1['title'];
	    }
	    else
	    {
		    $options_breadcrumb['items'][1]['href']=$url."/".$artwork_1['row']['titleurl'];
		    $options_breadcrumb['items'][1]['title']=LANG_SEE_ALL." ".$artwork_1['title'];
		    $options_breadcrumb['items'][1]['inner_html']=$artwork_1['title'];
	    }
	    $url.="/".$artwork_1['row']['titleurl'];
	    if( $_GET['section_1']=="paintings" || $_GET['section_1']=="overpainted-photographs" || $_GET['section_1']=="other" )
	    {
	    	$url.="/".$row_category['titleurl'];
            $options_breadcrumb['items'][3]['href']=$url;
            $options_breadcrumb['items'][3]['title']=LANG_SEE_ALL." ".$title_category;
            $inner_html=$artwork_2['title'];
            if( !$count_painting ) $inner_html.=" (".$count_cat_paintings." ".LANG_SEARCH_RESULTS3.")";
	    	$options_breadcrumb['items'][3]['inner_html']=$inner_html;
	    }
        # individual works case
        //if( $row_painting['edition_individual'] )
        if( $mod_rewrite[4]=="individual-works" )
        {
            $query_where_edition=" WHERE enable=1 AND paintID='".$paintid_edition."' ";
            $query_edition=QUERIES::query_painting($db,$query_where_edition);
            $results_edition=$db->query($query_edition['query']);
            $row_edition=$db->mysql_array($results_edition,0);
            $title_edition_painting=UTILS::row_text_value2($db,$row_edition,"title");
            $options_breadcrumb['items'][2]['href']=$options_breadcrumb['items'][1]['href']."/".$row_edition['titleurl'];
            $options_breadcrumb['items'][2]['title']=$title_edition_painting;
            $options_breadcrumb['items'][2]['inner_html']=$title_edition_painting;
            $options_breadcrumb['items'][3]['href']=$options_breadcrumb['items'][1]['href']."/".$row_edition['titleurl']."/?".$search_vars['with_sp']."&tab=individual-works-tabs";
            $options_breadcrumb['items'][3]['title']=LANG_ARTWORK_INDIVIDUAL_WORKS_TAB;
            $options_breadcrumb['items'][3]['inner_html']=LANG_ARTWORK_INDIVIDUAL_WORKS_TAB;
        }
        # END individual works case
        $options_breadcrumb['items'][4]['inner_html']=$title_painting;
        if( !empty($options_breadcrumb['items'][3]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][3]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][3]['href'];
		}
        elseif( !empty($options_breadcrumb['items'][2]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][2]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][2]['href'];
		}
        elseif( !empty($options_breadcrumb['items'][1]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
		}
	    //print_r($options_breadcrumb);
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        # END Breadcrubmles

        # next previous painting buttons
        $options_next_links=array();
        $options_next_links=$options_head;
        $options_next_links['row']=$row_painting;
        $options_next_links['url1']=$url;
        $options_next_links['title']=$_GET['title'];
	        # url back display
	        $options_url_back=array();
	        $options_url_back['referer']=$_GET['referer'];
	        if( !empty($_GET['search_main']) ) $options_url_back['title']=$_GET['search_main'];
	        else $options_url_back['title']=$_GET['title'];
	        $options_url_back['search_vars']=$search_vars['with_sp'];
	        if( $_GET['referer']!="search-art" )
	        {
	        	$options_url_back['url_back']=$url."/?".$search_vars['with_sp'];
	        	$options_url_back['url_back_text']=LANG_EXH_SHOW_ALL_WORKS;
	        }

	        $url_back_vars=$html->get_url_back($db,$options_url_back);
	        # individual works case
            //if( $row_painting['edition_individual'] )
	        if( $mod_rewrite[4]=="individual-works" )
            {
                $query_paintings=QUERIES::query_painting($db);
                $results_cat_paintings_total=$db->query("SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=1 AND r.itemid1='".$paintid_edition."' AND r.typeid2=25 ) OR ( r.typeid2=1 AND r.itemid2='".$paintid_edition."' AND r.typeid1=25 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=25 ) OR ( r.itemid1=p.paintID AND r.typeid1=25 ) ) AND p.enable=1 ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ");
                $count_cat_paintings=$db->numrows($results_cat_paintings_total);

                $options_next_links['url_back']=$options_breadcrumb['items'][1]['href']."/".$row_edition['titleurl']."/?".$search_vars['with_sp']."&tab=individual-works-tabs";
                $options_next_links['url1']="/art/editions/".$row_edition['titleurl']."/individual-works";
            }
            # END individual works case
	        else
	        {
	        	$options_next_links['url_back']=$url_back_vars['url_back'];
	        }
	        $options_next_links['back_text']=$url_back_vars['back_text'];
	        # END url back display
        $options_next_links['search_vars']=$search_vars;
        $options_next_links['results']['results_total']=$results_cat_paintings_total;
        $options_next_links['results']['count']=$count_cat_paintings;
        $options_next_links['id']['tabs']="div-painting-relation-tabs";
        # END next previous painting buttons

        # detail info about painting
        $options_next_links['imageid']=$imageid;
        $html_print_body.=$html->div_painting_info($db,$row_painting,$options_next_links);
        # END detail info about painting

        # relation tabs
        $options_relations=array();
        $options_relations=$options_head;
        $options_relations['row']=$row_painting;
        $options_relations['id']=$options_next_links['id']['tabs'];
        $options_relations['html_class']=$html;
        $options_relations['content_large']=1;
        $options_relations['tab']="tab";
        //$options_relations['thumb_per_line']=5; # for artwork tab

        $options_relations['thumb_per_line_installattion_photos']=4;
        $options_relations['thumb_per_line_videos']=4;
        $options_relations['thumbs_per_line']=4; # for artwork tab

        /*
	        # query_string
	        $query_string="/?";
	        if( !empty($_GET['p']) ) $query_string.="p=".$_GET['p'];
	        if( !empty($_GET['sp']) )
	        {
	        	if( !empty($_GET['p']) ) $query_string.="&";
	        	$query_string.="sp=".$_GET['sp'];
	        }
	        if( $query_string!="?" ) $query_string.="&";
	        # END query_string
	    */
        $options_relations['url_painting']=$options_next_links['url1']."/".$row_painting['titleurl']."?".$search_vars['with_sp'];
        $options_relations['class']['div-relation-tabs']="div-painting-relation-tabs-paint-detail";
        $options_relations['class']['div-relation-tabs-info']="div-painting-relation-tab-info";
        $options_relations['class']['p-section-desc-text']="p-literature-section-desc-text";
        $html_print_body.=div_relation_tabs($db,$options_relations);
        # END relation tabs

	#END div main content
}
############################ *** END **** art/paintings/photo_paintings/.... - DETAIL PAINTING VIEW





############################ art/paintings/photo_paintings - THUMBNAIL VIEW
elseif(
	# for paintings & opp thumbnail view
	(
		( $_GET['section_1']=="paintings" || $_GET['section_1']=="other" || $_GET['section_1']=="overpainted-photographs" )
		&& !empty($_GET['section_2']) && empty($_GET['section_4'])
	) ||
	# for all other non category section thumbnail views
	(
		( $_GET['section_1']=="atlas" || $_GET['section_1']=="editions" || $_GET['section_1']=="drawings" || $_GET['section_1']=="oils-on-paper" || $_GET['section_1']=="watercolours" || $_GET['section_1']=="prints" || $_GET['section_1']=="search" )
		&& empty($_GET['section_2'])
	)
)
{
	if( $_GET['section_1']=="search" )
	{
		if( $_SESSION['kiosk'] && $_GET['artworkid']==4 ) UTILS::redirect("/".$_GET['lang'].'/art');

		if( $_GET['artworkid']==5 || $_GET['artworkid']==6 || $_GET['artworkid']==8 )
		{
		    unset($_GET['year-from']);
		    unset($_GET['year-to']);
		}
		else
		{
		    unset($_GET['date-day-from']);
		    unset($_GET['date-month-from']);
		    unset($_GET['date-year-from']);
		    unset($_GET['date-day-to']);
		    unset($_GET['date-month-to']);
		    unset($_GET['date-year-to']);
		}

		if( !empty($_GET['number_list']) )
		{
		    foreach( $_GET['number_list'] as $key => $number_list_value )
		    {
		        if( empty($number_list_value) ) unset($_GET['number_list'][$key]);
		    }
		}

	    # search function
	    $options_search_art=array();
	    $options_search_art['get']=$_GET;
	    $options_search_art['sp']=$_GET['sp'];
	    $options_search_art['p']=$_GET['p'];
	    $options_search_art['artworkid']=$_GET['artworkid'];
	    $search_results=search_art($db,$options_search_art);
	    //print_r($search_results);
	    # END search function

		$count=$search_results['count'];
	    $pages=$search_results['pages'];
		$results_paintings=$search_results['results'];
		$results=$search_results['results_total'];
	}
	else
	{
		# which artwork it is
		$options_artwork_info=array();
		if( $_GET['section_1']=="paintings" )
		{
			$section=$_GET['section_2'];
			//print $section;
			if( $section!="photo-paintings" && $section!="abstracts" )
			{
				UTILS::not_found_404();
			}
		}
		else $section=$_GET['section_1'];
		$options_artwork_info['titleurl']=$section;
		$artwork_1=UTILS::get_artwork_data($db,$options_artwork_info);
		$desc_artwork_1=UTILS::row_text_value($db,$artwork_1['row'],"desc");

		//print_r($artwork_1);
		//print_r($options_artwork_info);
		//print_r($_GET);
		# if its an opp then get category title
			if( $_GET['section_1']=="paintings" || $artwork_1['row']['artworkID']==13 || $artwork_1['row']['artworkID']==6 )
			{
				if( $_GET['section_1']=="paintings" ) $section=$_GET['section_3'];
				else $section=$_GET['section_2'];
				$categoryid=UTILS::itemid($db,$section);
				$options_category_info=array();
				//$options_category_info['categoryid']=$categoryid;
				$options_category_info['section']=$section;
				$artwork_2=UTILS::get_category_data($db,$options_category_info);
				$title_category_sub=$artwork_2['title'];
				if( !$artwork_2['count'] && $_GET['section_2']!="thank-you" ) UTILS::not_found_404();
			}
		# END which artwork it is

	    ### querys for page numbering and selecting category paintings
	    $query_where_paintings=" WHERE artworkID='".$artwork_1['row']['artworkID']."' ";
	   	$query_where_paintings.=" AND catID='".$categoryid."' AND enable=1 ";
	    if( $artwork_1['row']['artworkID']==4 ) $query_where_paintings.=" AND edition_individual=0 ";
	    $query_order_paintings=" ORDER BY sort2 ASC, titleDE ASC, titleEN ASC ";
	    if( $_GET['sp']!='all' ) $query_limit_paintings=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp']." ";
	    $query_paintings=QUERIES::query_painting($db,$query_where_paintings,$query_order_paintings,$query_limit_paintings);
	    $results_paintings=$db->query($query_paintings['query']);
	    $results=$db->query($query_paintings['query_without_limit']);
	    $count=$db->numrows($results);
	    //if( $_SESSION['debug_page'] ) print $query_paintings['query'];
	    #end

	    $_GET['artworkID']=$artwork_1['row']['artworkID'];
		$_GET['categoryid']=$categoryid;
		//print_r($_GET);
	}
	$pages=@ceil($count/$_GET['sp']);

	# title meta
	$title_meta="";
	$title_meta.=$artwork_2['title'];
	if( !empty($artwork_2['title']) ) $title_meta.=" &raquo; ";
	if( $_GET['section_1']=="search" ) $artwork_1['title']=LANG_HEADER_MENU_SEARCH." &raquo; ".LANG_HEADER_MENU_ARTWORK;
	$title_meta.=$artwork_1['title'];
	if( !empty($artwork_1['title']) ) $title_meta.=" &raquo; ";
	$title_meta.=LANG_TITLE_HOME;
	$html->title=$title_meta;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.min.css";
	$html->css[]="/css/screen.min.css";
	$html->css[]="/css/main.mobile.min.css";
	$html->css[]="/css/page-numbering.min.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.min.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# opp form css
	$html->css[]="/css/opp-form.min.css";

	# hotfix css
	$html->css[]="/css/hotfixes.css";

    # tootltips, tabs, date picker - style jquery ui
    //$html->css[]="/js/jquery_ui/css/jquery-ui-1.10.4.custom.css";

    $html_print_body.=$html_script;

	# meta-tags & head
	//$options_head['meta_description']=;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'artwork',$options_head);

	# left side blocks
	$options_left_side=array();
	$options_left_side=$options_head;
	$options_left_side['show_microsites']=1;
	$options_left_side['db']=$db;
	$html->left_side_menu_selected=$artwork_2['row']['artworkID'];
	$html_print_head.=$html->left_side($artwork_1['row']['artworkID'],$options_left_side);

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/art";
	    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_ARTWORK;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_ARTWORK;
	    if( $_GET['section_1']=="paintings" )
	    {
	    	$options_admin_edit=array();
	    	$options_admin_edit=$options_head;
	    	$options_breadcrumb['admin_edit']=UTILS::admin_edit("/admin/paintings/categories/edit/?catid=".$categoryid,$options_admin_edit);
	    	$options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/art/paintings";
	    	$options_breadcrumb['items'][1]['title']=LANG_SEE_ALL." ".LANG_LEFT_SIDE_PAINTINGS;
	    	$options_breadcrumb['items'][1]['inner_html']=LANG_LEFT_SIDE_PAINTINGS;
		    $options_breadcrumb['items'][2]['href']="/".$_GET['lang']."/art/paintings/#".$artwork_1['row']['titleurl'];
		    $options_breadcrumb['items'][2]['title']=LANG_SEE_ALL." ".$artwork_1['title'];
		    $options_breadcrumb['items'][2]['inner_html']=$artwork_1['title'];
	    }
	    else
	    {
	    	if( $_GET['section_1']=="overpainted-photographs" || $_GET['section_1']=="other" )
	    	{
		    	$options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/art/".$artwork_1['row']['titleurl'];
		    	$options_breadcrumb['items'][1]['title']=LANG_SEE_ALL." ".$artwork_1['title'];
			}
		    if( !$_GET['info'] ) $options_breadcrumb['items'][1]['inner_html']=$artwork_1['title'];
		    else
		    {
		    	if( $_GET['artworkid']=="paintings" )
		    	{
		    		$artwork_info['title']=LANG_LEFT_SIDE_PAINTINGS;
		    	}
		    	else
		    	{
			    	$options_artwork_info=array();
					$options_artwork_info['artworkid']=$_GET['artworkid'];
					$artwork_info=UTILS::get_artwork_data($db,$options_artwork_info);
				}
		    	$options_breadcrumb['items'][1]['inner_html']=$artwork_info['title'];
		    }
	    }
	    if( ( $_GET['section_1']=="paintings" || $_GET['section_1']=="other" || $_GET['section_1']=="overpainted-photographs" ) && $count>0 && !empty($categoryid) && !empty($artwork_1['row']['artworkID']) )
	    {
	    	$options_breadcrumb['items'][3]['inner_html']=$artwork_2['title']." (".$count." ".LANG_SEARCH_RESULTS3.")";
	    }
        if( !empty($options_breadcrumb['items'][2]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][2]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][2]['href'];
		}
        elseif( !empty($options_breadcrumb['items'][1]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
		}
        elseif( !empty($options_breadcrumb['items'][0]['href']) )
        {
	    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
		}
	    //print_r($options_breadcrumb);
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    # END

	    # header
		$options_header_block=array();
		$options_header_block['title_category_sub']=$title_category_sub;
		//print "here-".$title_category_sub;
		$options_header_block['artwork_1']=$artwork_1;
		$options_header_block['options_head']=$options_head;
		$options_header_block['html']=$html;
		if( $_GET['section_1']=="prints" ) $options_header_block['src_header']="/g/headers/artwork/prints/1.jpg";
		$html_header=html_header_block($db,$options_header_block);

		$html_print_body.=$html_header;

		# Display - You searched for information
		if( !$_GET['info'] && $_GET['section_1']=="search" && $count>0 )
		{
		    $html_print_body.="<div class='div-you-searched-for'>";
			    if( empty($_GET['search_main']) ) $html_print_body.=LANG_SEARCH_YOUSEARCHEDFOR.": ";

		        if( empty($_GET['search_main']) && $count>0 && empty($_GET['artworkid']) && empty($_GET['title']) && empty($_GET['number']) && empty($_GET['museum']) && empty($_GET['year-from']) && empty($_GET['year-to']) && empty($_GET['date-day-from']) && empty($_GET['date-month-from']) && empty($_GET['date-year-from']) && empty($_GET['date-day-to']) && empty($_GET['date-month-to']) && empty($_GET['date-year-to']) && empty($_GET['size-height-min']) && empty($_GET['size-height-max']) && empty($_GET['size-width-min']) && empty($_GET['size-width-max']) && empty($_GET['colorid']) ) $html_print_body.=LANG_SEARCH_ALLPAINTINGS;

		        if( empty($_GET['search_main']) ) $html_print_body.="<br />";

				# artwork type
			    if( !empty($_GET['artworkid']) && $_GET['artworkid']=='paintings' ) $html_print_body.=ucwords(LANG_RIGHT_SEARCH_BYARTWORKTYPE2).": <em class='search-highlight'>".LANG_LEFT_SIDE_PAINTINGS."</em><br />";
			    elseif( !empty($_GET['artworkid']) && $_GET['artworkid']=='other' ) $html_print_body.=ucwords(LANG_RIGHT_SEARCH_BYARTWORKTYPE2).": <em class='search-highlight'>".LANG_LEFT_SIDE_OTHER."</em><br />";
			    elseif( !empty($_GET['artworkid']) )
			    {
			        $artwork=$db->db_prepare_input($_GET['artworkid']);
			        $query2=$db->query("SELECT * FROM ".TABLE_ARTWORKS." WHERE artworkID='".$artwork."' ");
			        $row2=$db->mysql_array($query2,0);
			        $artwork2=$row2[UTILS::select_lang($_GET['lang'],"artwork")];
			        $html_print_body.=ucwords(LANG_RIGHT_SEARCH_BYARTWORKTYPE2).": <em class='search-highlight'>".$artwork2."</em><br />";
			    }
				# title
				if( !empty($_GET['title']) ) $html_print_body.=LANG_SEARCH_TITLE.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['title'])."</em><br />";
				# number
				if( !empty($_GET['number']) )
				{
				    if( $_GET['artworkid']==1 || $_GET['artworkid']==2 || $_GET['artworkid']=="paintings" ) $number_type=LANG_SEARCH_CRNUMBER.": <em class='search-highlight'>";
				    elseif( $_GET['artworkid']==3 ) $number_type=LANG_SEARCH_SHEETNUMBER.": <em class='search-highlight'>";
				    else $number_type=LANG_SEARCH_NUMBER.": <em class='search-highlight'>";
				        $html_print_body.=$number_type;
			            $html_print_body.=UTILS::strip_slashes_recursive($_GET['number']);
			        $html_print_body.="</em><br />";
			    }

			    # location - museum
				if( !empty($_GET['museum']) ) $html_print_body.=LANG_SEARCH_LOCATION.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['museum'])."</em><br />";

				# year-from and year-to
				if( !empty($_GET['year-from']) && !empty($_GET['year-to']) )
				{
				    $html_print_body.=LANG_SEARCH_YEARFROM.": <em class='search-highlight'>".$_GET['year-from']."</em><br />";
				    $html_print_body.=LANG_SEARCH_YEARTO.": <em class='search-highlight'>".$_GET['year-to']."</em><br />";
				}

				if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
				{
				    $html_print_body.=LANG_SEARCH_YEAR.": <em class='search-highlight'>".$_GET['year-from']."</em><br />";
				}
				if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
				{
				    $html_print_body.=LANG_SEARCH_YEAR.": <em class='search-highlight'>".$_GET['year-to']."</em><br />";
				}

				# date
			    if( $_GET['artworkid']==5 || $_GET['artworkid']==6 || $_GET['artworkid']==8 )
			    {
				    if( !empty($_GET['date-day-from']) || !empty($_GET['date-month-from']) || !empty($_GET['date-year-from'])
				        || !empty($_GET['date-day-to']) || !empty($_GET['date-month-to']) || !empty($_GET['date-year-to']) )
					{
				        $html_print_body.=LANG_SEARCH_DATE.": <em class='search-highlight'>";
				    }

				    # date from
					if( !empty($_GET['date-day-from']) || !empty($_GET['date-month-from']) || !empty($_GET['date-year-from']) )
					{
				        $date_day_from=$_GET['date-day-from'];
				        $date_month_from=$month=UTILS::date_month($db,date('F', @mktime(0,0,0,$_GET['date-month-from'],1)));
				        $date_year_from=$_GET['date-year-from'];
				        if( $_GET['lang']=="de" )
				        {
				            if( !empty($_GET['date-day-from']) ) $html_print_body.=$date_day_from.". ";
				            if( !empty($_GET['date-month-from']) ) $html_print_body.=$date_month_from." ";
				            if( !empty($_GET['date-year-from']) ) $html_print_body.=$date_year_from." ";
				        }
				        elseif( $_GET['lang']=="fr" )
				        {
				            if( !empty($_GET['date-day-from']) ) $html_print_body.=$date_day_from." ";
				            if( !empty($_GET['date-month-from']) ) $html_print_body.=$date_month_from." ";
				            if( !empty($_GET['date-year-from']) ) $html_print_body.=$date_year_from." ";
				        }
				        else
				        {
				            if( !empty($_GET['date-month-from']) ) $html_print_body.=$date_month_from;
				            if( !empty($_GET['date-day-from']) ) $html_print_body.=" ".$date_day_from;
				            if( !empty($_GET['date-year-from']) && ( !empty($_GET['date-month-from']) || !empty($_GET['date-day-from']) ) ) $html_print_body.=", ";
				            if( !empty($_GET['date-day-from']) ) $html_print_body.=" ";
				            if( !empty($_GET['date-year-from']) ) $html_print_body.=$date_year_from." ";
				        }
					}

				    if( ( !empty($_GET['date-day-from']) || !empty($_GET['date-month-from']) || !empty($_GET['date-year-from']) )
				        && ( !empty($_GET['date-day-to']) || !empty($_GET['date-month-to']) || !empty($_GET['date-year-to']) ) ) $html_print_body.="- ";

				    # date to
					if( !empty($_GET['date-day-to']) || !empty($_GET['date-month-to']) || !empty($_GET['date-year-to']) )
					{
				        $date_day_to=$_GET['date-day-to'];
				        $date_month_to=UTILS::date_month($db,date('F', @mktime(0,0,0,$_GET['date-month-to'],1)));
				        $date_year_to=$_GET['date-year-to'];

				        if( $_GET['lang']=="de" )
				        {
				            if( !empty($_GET['date-day-to']) ) $html_print_body.=$date_day_to.". ";
				            if( !empty($_GET['date-month-to']) ) $html_print_body.=$date_month_to." ";
				            if( !empty($_GET['date-year-to']) ) $html_print_body.=$date_year_to." ";
				        }
				        elseif( $_GET['lang']=="fr" )
				        {
				            if( !empty($_GET['date-day-to']) ) $html_print_body.=$date_day_to." ";
				            if( !empty($_GET['date-month-to']) ) $html_print_body.=$date_month_to." ";
				            if( !empty($_GET['date-year-to']) ) $html_print_body.=$date_year_to." ";
				        }
				        else
				        {
				            if( !empty($_GET['date-month-to']) ) $html_print_body.=$date_month_to;
				            if( !empty($_GET['date-day-to']) ) $html_print_body.=" ".$date_day_to;
				            if( !empty($_GET['date-year-to']) && ( !empty($_GET['date-month-to']) || !empty($_GET['date-day-to']) ) ) $html_print_body.=", ";
				            if( !empty($_GET['date-day-to']) ) $html_print_body.=" ";
				            if( !empty($_GET['date-year-to']) ) $html_print_body.=$date_year_to." ";
				        }
					}

				    if( !empty($_GET['date-day-from']) || !empty($_GET['date-month-from']) || !empty($_GET['date-year-from'])
				        || !empty($_GET['date-day-to']) || !empty($_GET['date-month-to']) || !empty($_GET['date-year-to']) )
					{
				        $html_print_body.="</em><br />";
					}
			    }
			    # END date

				# height-att
				if( !empty($_GET['size-height-min']) || !empty($_GET['size-height-max']) )
				{
					if( !empty($_GET['size-height-min']) && !empty($_GET['size-height-max']) )
					{
						$html_print_body.=LANG_SEARCH_HEIGHT.": <em class='search-highlight'>".$_GET['size-height-min']."</em> - <em class='search-highlight'>".$_GET['size-height-max']."</em><br />";
					}
					elseif( !empty($_GET['size-height-min']) )
					{
						$html_print_body.=LANG_SEARCH_HEIGHT.": <em class='search-highlight'>".$_GET['size-height-min']."</em><br />";
					}
					elseif( !empty($_GET['size-height-max']) )
					{
						$html_print_body.=LANG_SEARCH_HEIGHT.": <em class='search-highlight'>".$_GET['size-height-max']."</em><br />";
					}
				}

			    # width-att
				if( !empty($_GET['size-width-min']) || !empty($_GET['size-width-max']) )
				{
					if( !empty($_GET['size-width-min']) && !empty($_GET['size-width-max']) )
					{
					$html_print_body.=LANG_SEARCH_WIDTH.": <em class='search-highlight'>".$_GET['size-width-min']."</em> - <em class='search-highlight'>".$_GET['size-width-max']."</em><br />";
					}
					elseif( !empty($_GET['size-width-min']) )
					{
					$html_print_body.=LANG_SEARCH_WIDTH.": <em class='search-highlight'>".$_GET['size-width-min']."</em><br />";
					}
					elseif( !empty($_GET['size-width-max']) )
					{
					$html_print_body.=LANG_SEARCH_WIDTH.": <em class='search-highlight'>".$_GET['size-width-max']."</em><br />";
					}

				}

			    # color
			    if( !empty($_GET['colorid']) )
				{
			        $query_where_colors=" WHERE colorid='".$_GET['colorid']."' ";
			        $query_colors=QUERIES::query_painting_colors($db,$query_where_colors);
			        $results_color=$db->query($query_colors['query_without_limit']);
			        $row_color=$db->mysql_array($results_color);
			        $color_title=UTILS::row_text_value($db,$row_color,"title");
			        $html_print_body.=LANG_SEARCH_COLOR.": <em class='search-highlight'>".$color_title."</em><br />";
			    }

			    if( $count==1 ) $s=LANG_SEARCH_RESULTS2;
			    else $s=LANG_SEARCH_RESULTS3;
			    if( $count!=0 ) $html_print_body.=LANG_SEARCH_RESULTS1." <em class='search-highlight'>".$count."</em> $s<br />";
		    $html_print_body.="</div>";
		}
		# *** END *** Display - You searched for information

		if( !empty($artwork_2['desc']) || !empty($desc_artwork_1) ) $class_padding="div-category-landing-page-padding-topbottom";
		else $class_padding="";

		$html_print_body.="<div class='div-section-content ".$class_padding."'>";

			# art/overpainted-photographs/thank-you - When comes from the submition of opp form
			if( $_GET['section_1']=="overpainted-photographs" && $_GET['section_2']=="thank-you" )
			{
				# if user selects to print submited info show this
				if( $_GET['print'] )
				{
					if( !empty($_GET['submitid']) && !empty($_GET['auth']) )
					{
					    $query_opp_data="SELECT * FROM ".TABLE_FORM_OPP." WHERE submitid='".$_GET['submitid']."' AND auth='".$_GET['auth']."' LIMIT 1 ";
					    $results_opp_data=$db->query($query_opp_data);
					    $row=$db->mysql_array($results_opp_data,0);

					    $style_th="style='text-align:left;width:304px;vertical-align:top;'";

					    $message="<table cellspacing='0'>";
					    //$message.="<tr><th style='text-align:center;'' colspan='2'>Print</th></tr>";
					    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_104."</th><td>".$row['submitid']."</td></tr>";
					    if( !empty($row['title']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_20."</th><td>".$row['title']."</td></tr>";
					    if( !empty($row['date']) ) $message.="<tr><th ".$style_th.">".strip_tags(LANG_OPP_FORM_21)."</th><td>".$row['date']."</td></tr>";
					    if( !empty($row['series']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_33."</th><td>".$row['series']."</td></tr>";
					    if( !empty($row['editions_number']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_39."</th><td>".$row['editions_number']."</td></tr>";
					    if( !empty($row['medium']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_22."</th><td>".$row['medium']."</td></tr>";
					    if( !empty($row['dimensions_photo_height']) || !empty($row['dimensions_photo_width']) || !empty($row['dimensions_photo_type']) )
					    {
					        $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_27."</th><td>";
					            $message.=$row['dimensions_photo_height']." ";
					            if( !empty($row['dimensions_photo_height']) && !empty($row['dimensions_photo_width']) ) $message.="&#215; ";
					            $message.=$row['dimensions_photo_width']." ";
					            $message.=$row['dimensions_photo_type'];
					        $message.="</td></tr>";
					    }
					    if( !empty($row['dimensions_mount_height']) || !empty($row['dimensions_mount_width']) || !empty($row['dimensions_mount_type']) )
					    {
					        $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_30."</th><td>";
					            $message.=$row['dimensions_mount_height']." ";
					            if( !empty($row['dimensions_mount_height']) && !empty($row['dimensions_mount_width']) ) $message.="&#215; ";
					            $message.=$row['dimensions_mount_width']." ";
					            $message.=$row['dimensions_mount_type'];
					        $message.="</td></tr>";
					    }
					    if( !empty($row['indicate_credit']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_32."</th><td>".$row['indicate_credit']."</td></tr>";
						if( !empty($row['optionid_recto_signed_1']) || !empty($row['optionid_recto_signed_2']) || !empty($row['recto_signed_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_signed_1=show_dropdown_option_title($db,$row['optionid_recto_signed_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_signed_2=show_dropdown_option_title($db,$row['optionid_recto_signed_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_1."</th><td>";
						        $message.=$optionid_recto_signed_1." ";
						        $message.=$optionid_recto_signed_2." ";
						        $message.=$row['recto_signed_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_recto_dated_1']) || !empty($row['optionid_recto_dated_2']) || !empty($row['recto_dated_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_dated_1=show_dropdown_option_title($db,$row['optionid_recto_dated_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_dated_2=show_dropdown_option_title($db,$row['optionid_recto_dated_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_2."</th><td>";
						        $message.=$optionid_recto_dated_1." ";
						        $message.=$optionid_recto_dated_2." ";
						        $message.=$row['recto_dated_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_recto_numbered_1']) || !empty($row['optionid_recto_numbered_2']) || !empty($row['recto_numbered_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_numbered_1=show_dropdown_option_title($db,$row['optionid_recto_numbered_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_numbered_2=show_dropdown_option_title($db,$row['optionid_recto_numbered_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_3."</th><td>";
						        $message.=$optionid_recto_numbered_1." ";
						        $message.=$optionid_recto_numbered_2." ";
						        $message.=$row['recto_numbered_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_recto_inscribed_1']) || !empty($row['optionid_recto_inscribed_2']) || !empty($row['recto_inscribed_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_inscribed_1=show_dropdown_option_title($db,$row['optionid_recto_inscribed_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_recto_inscribed_2=show_dropdown_option_title($db,$row['optionid_recto_inscribed_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_1_4."</th><td>";
						        $message.=$optionid_recto_inscribed_1." ";
						        $message.=$optionid_recto_inscribed_2." ";
						        $message.=$row['recto_inscribed_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_verso_signed_1']) || !empty($row['optionid_verso_signed_2']) || !empty($row['verso_signed_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_signed_1=show_dropdown_option_title($db,$row['optionid_verso_signed_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_signed_2=show_dropdown_option_title($db,$row['optionid_verso_signed_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_1."</th><td>";
						        $message.=$optionid_verso_signed_1." ";
						        $message.=$optionid_verso_signed_2." ";
						        $message.=$row['verso_signed_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_verso_dated_1']) || !empty($row['optionid_verso_dated_2']) || !empty($row['verso_dated_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_dated_1=show_dropdown_option_title($db,$row['optionid_verso_dated_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_dated_2=show_dropdown_option_title($db,$row['optionid_verso_dated_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_2."</th><td>";
						        $message.=$optionid_verso_dated_1." ";
						        $message.=$optionid_verso_dated_2." ";
						        $message.=$row['verso_dated_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_verso_numbered_1']) || !empty($row['optionid_verso_numbered_2']) || !empty($row['verso_numbered_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_numbered_1=show_dropdown_option_title($db,$row['optionid_verso_numbered_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_numbered_2=show_dropdown_option_title($db,$row['optionid_verso_numbered_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_3."</th><td>";
						        $message.=$optionid_verso_numbered_1." ";
						        $message.=$optionid_verso_numbered_2." ";
						        $message.=$row['verso_numbered_3']." ";
						    $message.="</td></tr>";
						}
						if( !empty($row['optionid_verso_inscribed_1']) || !empty($row['optionid_verso_inscribed_2']) || !empty($row['verso_inscribed_3']) )
						{
						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_inscribed_1=show_dropdown_option_title($db,$row['optionid_verso_inscribed_1'],$options_show_dropdown_title);

						    $options_show_dropdown_title=array();
						    $options_show_dropdown_title['html_return']=1;
						    $optionid_verso_inscribed_2=show_dropdown_option_title($db,$row['optionid_verso_inscribed_2'],$options_show_dropdown_title);

						    $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_40_2_4."</th><td>";
						        $message.=$optionid_verso_inscribed_1." ";
						        $message.=$optionid_verso_inscribed_2." ";
						        $message.=$row['verso_inscribed_3']." ";
						    $message.="</td></tr>";
						}
					    if( !empty($row['inscriptions']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_53."</th><td>".$row['inscriptions']."</td></tr>";
					    if( !empty($row['notations']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_54."</th><td>".$row['notations']."</td></tr>";
					    if( !empty($row['copyright_info']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_58."</th><td>".$row['copyright_info']."</td></tr>";
					    if( !empty($row['provenance']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_59."</th><td>".$row['provenance']."</td></tr>";
					    if( !empty($row['acquired_from']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_60."</th><td>".$row['acquired_from']."</td></tr>";
					    if( !empty($row['date_of_acquisition']) ) $message.="<tr><th ".$style_th.">".strip_tags(LANG_OPP_FORM_61)."</th><td>".$row['date_of_acquisition']."</td></tr>";
					    if( !empty($row['exhibition_history']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_62."</th><td>".$row['exhibition_history']."</td></tr>";
					    if( !empty($row['exhibition_history_other']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_62_1."</th><td>".$row['exhibition_history_other']."</td></tr>";
					    if( !empty($row['publication_history']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_63."</th><td>".$row['publication_history']."</td></tr>";
					    if( !empty($row['publication_history_other']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_63_1."</th><td>".$row['publication_history_other']."</td></tr>";
					    if( !empty($row['first_name']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_65."</th><td>".$row['first_name']."</td></tr>";
					    if( !empty($row['last_name']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_66."</th><td>".$row['last_name']."</td></tr>";
					    if( !empty($row['email']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_67."</th><td>".$row['email']."</td></tr>";
					    if( !empty($row['address']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68."</th><td>".$row['address']."</td></tr>";
					    if( !empty($row['country']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68_1."</th><td>".$row['country']."</td></tr>";
					    if( !empty($row['post_code']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_68_2."</th><td>".$row['post_code']."</td></tr>";
					    if( !empty($row['telephone']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_69."</th><td>".$row['telephone']."</td></tr>";
					    if( !empty($row['additional_comments']) ) $message.="<tr><th ".$style_th.">".LANG_OPP_FORM_70."</th><td>".$row['additional_comments']."</td></tr>";
					    //if( !empty($row['']) ) $message.="<tr><th ".$style_th."></th><td>".$row['']."</td></tr>";

					    $query_images="SELECT * FROM ".TABLE_FORM_OPP_IMAGES." WHERE submitid='".$row['submitid']."' ";
					    $results_images=$db->query($query_images);
					    $count_images=$db->numrows($results_images);
					    $dir="/datadir/files_opp_form";
					    if( $count_images>0 )
					    {
					        $message.="<tr><th ".$style_th.">Images</th><td>";
					        $i=0;
					        while( $row_images=$db->mysql_array($results_images,0) )
					        {
					            $i++;
					            $new_file_path=$dir."/".$row_images['src'];
					            if( $i>1 ) $message.="<br /><br />";
					            $message.="<img src='".$new_file_path."' alt='' width='450' height='' />";
					        }
					        $message.="</td></tr>";
					    }

					    $message.="</table>";

					    print "<div class='div-opp-form-print'>";
					        print $message;
					    print "</div>";

					    print "<script type='text/javascript'>";
					        print "window.print()";
					    print "</script>";

					}
					else
					{
						UTILS::redirect('/');
					}
					exit;
					# *** END *** if user selects to print submited info show this
				}
				else
				{
				    $html_print_body.="<div class='div-brief-trp' style='text-align:center;padding:50px; 0'>\n";

				        $html_print_body.="<p>".LANG_OPP_FORM_76."</p>\n";
				        $html_print_body.="<p>".LANG_OPP_FORM_77."</p>\n";
				        $html_print_body.="<p><br /><a href='/".$_GET['lang']."/art/overpainted-photographs/thank-you/?submitid=".$_GET['submitid']."&auth=".$_GET['auth']."&print=1' title='".LANG_OPP_FORM_77_2."' target='_blank' class='a-opp-thankyou-print'>".LANG_OPP_FORM_77_2."</a></p>\n";

				    $html_print_body.="</div>\n";
				}
			}
			# *** END *** art/overpainted-photographs/thank-you

			# else display thumbs of selected category also thumbs for art/search
			else
			{
		    	# if no results found show 404
				if( ( $count<=0 || empty($artwork_1['row']['artworkID']) ) && $_GET['section_1']!="search" )
				{
					UTILS::not_found_404();
				}
				if( ( $_GET['section_1']=="paintings" || $artwork_1['row']['artworkID']==13 || $artwork_1['row']['artworkID']==6 ) && empty($categoryid) )
				{
					UTILS::not_found_404();
				}
				# END

			    # artwork description
			    //print_r($artwork_1);
			    //print_r($artwork_2);
			    if( !empty($artwork_2['desc']) || !empty($desc_artwork_1) )
			    {
			    	if( !empty($artwork_2['desc']) )
			    	{
			    		$desc_text=$artwork_2['desc'];
		            	$options_admin_edit=array();
		            	$options_admin_edit=$options_head;
			        	$html_print_body.=UTILS::admin_edit("/admin/paintings/categories/edit/?catid=".$artwork_2['row']['catID'],$options_admin_edit);
		            }
		            elseif( !empty($desc_artwork_1) )
		            {
		            	$desc_text=$desc_artwork_1;
		            	$options_admin_edit=array();
		            	$options_admin_edit=$options_head;
			        	$html_print_body.=UTILS::admin_edit("/admin/paintings/types/edit/?artworkid=".$artwork_1['row']['artworkID'],$options_admin_edit);
		            }

				    $desc_length=strlen($desc_text);
				    //print $desc_length;

		            $options_div_text=array();
		            $options_div_text['class']['div-category-desc']="p-category-desc";
		            if( $desc_length<400 ) $options_div_text['class']['div_text_wysiwyg']="p-category-desc-short-text";
		            $options_div_text['id']="p-category-desc";
		            $html_print_body.=$html->div_text_wysiwyg($db,$desc_text,$options_div_text);
			        if( $desc_length>400 ) $html_print_body.="<a onclick=\"change_class('p-category-desc','p-category-desc div_text_wysiwyg p-opened',this,'[".LANG_CATEGORY_DESC_MORE."]','[".LANG_CATEGORY_DESC_CLOSE."]');\" class='a-read-more-cat-desc a-closed'>[".LANG_CATEGORY_DESC_MORE."]</a>\n";
				}

			    # prepare search vars
			    $search_vars=UTILS::search_vars_from_get($_GET);
			    # END

			    $options_page_numbering=array();
			    $options_page_numbering=$options_head;
			    //$options_page_numbering['search_vars_no_sp']=$search_vars['no_sp'];
			    $options_page_numbering['pages']=$pages;
			    $options_page_numbering['p']=$_GET['p'];
			    $options_page_numbering['sp']=$_GET['sp'];
			    $options_page_numbering['url']=$url;
			    $options_page_numbering['url_after'].="&sp=".$_GET['sp'].$search_vars['no_sp'];
			    $options_page_numbering['url_after_per_page'].=$search_vars['no_sp'];
			    $options_page_numbering['count']=$count;
			    $options_page_numbering['class']['div-page-navigation']="";
			    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-bottom";
			    $options_page_numbering['per_page_list']=1;
			   	$html_print_body.=$html->page_numbering($db,$options_page_numbering);


			    # creating thumb array and sorting how we want
			    $options_order_res=array();
			    $options_order_res['results']=$results;
			    $options_order_res['title']=$_GET['title'];
			    $thumb_array=UTILS::order_results($db,$options_order_res);
			    # end

			    $options_thumb_images=array();
			    $options_thumb_images=$options_head;
			    $options_thumb_images['results']=$thumb_array;
			    $options_thumb_images['show_title']=1;
			    $options_thumb_images['search_vars']=$search_vars['with_sp'];
			    $options_thumb_images['thumbs_per_line']=4;
			    $options_thumb_images['values_from']=($_GET['sp']*($_GET['p']-1));
			    $options_thumb_images['values_to']=$_GET['sp']*$_GET['p'];
			    if( $_GET['sp']=='all' ) $options_thumb_images['show_all']=1;
			    if( $options_thumb_images['values_to']>count($thumb_array) ) $options_thumb_images['values_to']=count($thumb_array);
			    $html_print_body.=$html->div_thumb_images($db,$options_thumb_images);

			    $options_page_numbering['class']['div-page-numbering-container-border']="div-page-numbering-container-border-top";
			    $html_print_body.=$html->page_numbering($db,$options_page_numbering);

			    if( $_SESSION['debug_page'] ) $html_print_body.=$query_paintings['query'];
			}
			# *** END *** else display thumbs of selected category

		$html_print_body.="</div>";

	#END div main content

}
############################ *** END *** art/paintings/photo_paintings - THUMBNAIL VIEW







############################ art/paintings & /art/overpainted-photographs - CATEGORY VIEW
elseif( $_GET['section_1']=="paintings" || $_GET['section_1']=="overpainted-photographs" || $_GET['section_1']=="other" )
{
	# title meta
	$html->title=$artwork_1['title']." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.min.css";
	$html->css[]="/css/screen.min.css";
	$html->css[]="/css/main.mobile.min.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.min.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# opp form css
	$html->css[]="/css/opp-form.min.css";

	# hotfix css
	$html->css[]="/css/hotfixes.css";

    # tootltips, tabs, date picker - style jquery ui
    $html->css[]="/js/jquery_ui/css/jquery-ui-1.10.4.custom.min.css";
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

	$html_print_body.=$html_script;

	# meta-tags & head
	//$options_head['meta_description']=;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'artwork',$options_head);

	# left side blocks
	$options_left_side=array();
	$options_left_side=$options_head;
	$options_left_side['show_microsites']=1;
	$options_left_side['db']=$db;
	$html_print_head.=$html->left_side($artwork_1['row']['artworkID'],$options_left_side);

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/art";
	    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_ARTWORK;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_ARTWORK;
	    $options_breadcrumb['items'][1]['inner_html']=$artwork_1['title'];
	    $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
	    $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);

	    # header
		$options_header_block=array();
		$options_header_block['title_category_sub']=$title_category_sub;
		$options_header_block['artwork_1']=$artwork_1;
		$options_header_block['options_head']=$options_head;
		$options_header_block['html']=$html;
		$html_header=html_header_block($db,$options_header_block);

		$html_print_body.=$html_header;

	    # OPP form


	if ($html->lang == 'en'){
		if( $_GET['section_1']=="overpainted-photographs" )
		{
		    $options_opp_form=array();
		    $options_opp_form=$options_head;
		    $options_opp_form['href']="mailto:".EMAIL_OPP."?subject=Gerhard Richter - Overpainted Photographs";
		    $options_opp_form['class']['div-brief-right-large']="div-brief-right-large";
		    $html_print_body.=$html->div_opp_brief($db,$options_opp_form);
		}
	}



        $html_print_body.="<div class='div-section-content div-category-landing-page-padding-topbottom'>";

		    # artwork description
		    //print_r($artwork_1);
		    //print_r($artwork_2);
        	$desc_artwork_1=UTILS::row_text_value($db,$artwork_1['row'],"desc");
		    if( !empty($desc_artwork_1) )
		    {
		    	if( !empty($desc_artwork_1) )
	            {
	            	$desc_text=$desc_artwork_1;
	            	$options_admin_edit=array();
	            	$options_admin_edit=$options_head;
		        	$html_print_body.=UTILS::admin_edit("/admin/paintings/types/edit/?artworkid=".$artwork_1['row']['artworkID'],$options_admin_edit);
	            }

			    $desc_length=strlen($desc_text);
			    //print $desc_length;

	            $options_div_text=array();
	            $options_div_text['class']['div-category-desc']="p-category-desc";
	            $options_div_text['id']="p-category-desc";
	            $html_print_body.=$html->div_text_wysiwyg($db,$desc_text,$options_div_text);
		        if( $desc_length>400 ) $html_print_body.="<a onclick=\"change_class('p-category-desc','p-category-desc div_text_wysiwyg p-opened',this,'[".LANG_CATEGORY_DESC_MORE."]','[".LANG_CATEGORY_DESC_CLOSE."]');\" class='a-read-more-cat-desc a-closed'>[".LANG_CATEGORY_DESC_MORE."]</a>\n";
			}

			//if( $_GET['section_1']=="paintings" || $_GET['section_1']=="overpainted-photographs" )
			if( $_GET['section_1']=="paintings" )
			{
				$html_print_body.="<p class='p-section-description p-section-description-modified'>";
					if( $_GET['section_1']=="paintings" )
					{
						$html_print_body.="<span class='span-category-show-all'>";
							$html_print_body.="<span class='span-category-show-all-left'>";
								$html_print_body.=LANG_ARTWORK_TEXT_PAINTINGS1;
							$html_print_body.="</span>";

				    		$html_print_body.="<a class='a-cat-show-all' href='/".$_GET['lang']."/art/search/?artworkid=paintings&amp;info=1' title='' >".LANG_LITERATURE_SHOW_ALL."</a>\n";
				    		$html_print_body.="<span class='clearer'></span>";
				    	$html_print_body.="</span>";

				    	$html_print_body.="<span class='span-category-text'>";
							$html_print_body.=LANG_ARTWORK_TEXT_PAINTINGS."<br /><br />\n";
						$html_print_body.="</span>";
					}
					/*
					elseif( $_GET['section_1']=="overpainted-photographs" )
					{
		            	$html_print_body.="<a class='forward' href='/".$_GET['lang']."/art/search/?artworkid=6&amp;info=1' title='".LANG_ARTWORK_BROWSE_OPP."' >".LANG_ARTWORK_BROWSE_OPP."</a>\n";
					}
					*/
				$html_print_body.="</p>\n";
			}

			if( $_GET['section_1']=="paintings" )
			{

				# PHOTO-PAINTINGS CATEGORIES
			    $query_where_categories="
			        LEFT JOIN ".TABLE_ARTWORKS." a ON c.artworkID=a.artworkID
			        WHERE c.enable=1 AND c.artworkID=1
			    ";

			    $query_order_categories=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
			    $options_query_category=array();
			    $options_query_category['columns']=",a.titleurl AS a_titleurl";
			    $query_categories=QUERIES::query_category($db,$query_where_categories,$query_order_categories,"","",$options_query_category);
			    $results_categories=$db->query($query_categories['query']);
			    $count_categories=$db->numrows($results_categories);
			    $categories=array();
			    while( $row_categories=$db->mysql_array($results_categories) )
				{
					$categories[]=$row_categories;
				}

		        $html_print_body.="<h3 class='h3-section-title'>";
		            $html_print_body.="<a name='photo-paintings'>".LANG_ARTWORK_SECTION_PHOTOPAINTINGS."</a>";
		        $html_print_body.="</h3>\n";

	            $options_cat_thumbs=array();
	            $options_cat_thumbs=$options_head;
		        $options_cat_thumbs['categories']=$categories;
		        $options_cat_thumbs['count_categories']=$count_categories;
	            $options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
	            $options_cat_thumbs['url_admin']="/admin/paintings/categories/edit/?catid=";
                $options_cat_thumbs['show_all']=1;
                $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/art/search/?artworkid=1&info=1";
	            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
	            $html_print_body.=$return_cat_thumbs['html_print'];
	            # end PHOTO-PAINTINGS CATEGORIES

				# ABSTRACTS CATEGORIES
			    $query_where_categories="
			        LEFT JOIN ".TABLE_ARTWORKS." a ON c.artworkID=a.artworkID
			        WHERE c.enable=1 AND c.artworkID=2
			    ";

			    $query_order_categories=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
			    $options_query_category=array();
			    $options_query_category['columns']=",a.titleurl AS a_titleurl";
			    $query_categories=QUERIES::query_category($db,$query_where_categories,$query_order_categories,"","",$options_query_category);
			    $results_categories=$db->query($query_categories['query']);
			    $count_categories=$db->numrows($results_categories);
			    $categories=array();
			    while( $row_categories=$db->mysql_array($results_categories) )
				{
					$categories[]=$row_categories;
				}

		        $html_print_body.="<h3 class='h3-section-title h3-section-title-second'>";
		            $html_print_body.="<a name='abstracts'>".LANG_ARTWORK_SECTION_ABSTRACTS."</a>";
		        $html_print_body.="</h3>\n";

	            $options_cat_thumbs=array();
	            $options_cat_thumbs=$options_head;
		        $options_cat_thumbs['categories']=$categories;
		        $options_cat_thumbs['count_categories']=$count_categories;
	            //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
	            $options_cat_thumbs['url_admin']="/admin/paintings/categories/edit/?catid=";
                $options_cat_thumbs['show_all']=1;
                $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/art/search/?artworkid=2&info=1";
	            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
	            $html_print_body.=$return_cat_thumbs['html_print'];
	            # end ABSTRACTS CATEGORIES

	            /*
				$options_categories=array();
				$options_categories=$options_head;
				$options_categories['artworkID']=1;
				$options_categories['name']="photo-paintings";
				$options_categories['title']=LANG_ARTWORK_SECTION_PHOTOPAINTINGS;
				$options_categories['categoryids']=array();
			    $html_print_body.=$html->categories($db,$options_categories);
			    */

			    /*
				$options_categories['artworkID']=2;
				$options_categories['name']="abstracts";
				$options_categories['title']=LANG_ARTWORK_SECTION_ABSTRACTS;
				$options_categories['categoryids']=array();
			    $html_print_body.=$html->categories($db,$options_categories);
			    */
			}
			elseif( $_GET['section_1']=="overpainted-photographs" )
			{
		        $results=$db->query("SELECT catID FROM ".TABLE_CATEGORY."
		        	WHERE enable=1
		        		AND artworkID='".$artwork_1['row']['artworkID']."'
		        		AND ( sub_catID IS NULL || sub_catID='' || sub_catID=0 )
		        	ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ");
		        $count=$db->numrows($results);
		        if( $count>0 )
		        {
		            $categoryids=array();
		            while( $row=$db->mysql_array($results) )
		            {
		                $categoryids[]=$row['catID'];
		            }

		            /*
					$options_categories=array();
					$options_categories=$options_head;
					$options_categories['artworkID']=6;
					$options_categories['categoryids']=$categoryids;
				    $html_print_body.=$html->categories($db,$options_categories);
				    */


					# OPP CATEGORIES
				    $query_where_categories="
				        LEFT JOIN ".TABLE_ARTWORKS." a ON c.artworkID=a.artworkID
				        WHERE c.enable=1 AND c.artworkID='".$artwork_1['row']['artworkID']."' AND ( c.sub_catID IS NULL || c.sub_catID='' || c.sub_catID=0 )
				    ";

				    $query_order_categories=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
				    $options_query_category=array();
				    $options_query_category['columns']=",a.titleurl AS a_titleurl";
				    $query_categories=QUERIES::query_category($db,$query_where_categories,$query_order_categories,"","",$options_query_category);
				    $results_categories=$db->query($query_categories['query']);
				    $count_categories=$db->numrows($results_categories);
				    $categories=array();
				    while( $row_categories=$db->mysql_array($results_categories) )
					{
						$categories[]=$row_categories;
					}

		            $options_cat_thumbs=array();
		            $options_cat_thumbs=$options_head;
			        $options_cat_thumbs['categories']=$categories;
			        $options_cat_thumbs['count_categories']=$count_categories;
		            //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
		            $options_cat_thumbs['url_admin']="/admin/paintings/categories/edit/?catid=";
	                $options_cat_thumbs['show_all']=1;
	                $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/art/search/?artworkid=".$artwork_1['row']['artworkID']."&info=1";
		            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
		            $html_print_body.=$return_cat_thumbs['html_print'];
		            # end OPP CATEGORIES

		        }
			}
			elseif( $_GET['section_1']=="other" )
			{
				/*
				$options_categories=array();
				$options_categories=$options_head;
				$options_categories['artworkID']=13;
				$options_categories['name']="other";
				$options_categories['title']=LANG_ARTWORK_SECTION_OTHERMEDIA;
				$options_categories['categoryids']=array();
			    $html_print_body.=$html->categories($db,$options_categories);
			    */

				# OTHER CATEGORIES
			    $query_where_categories="
			        LEFT JOIN ".TABLE_ARTWORKS." a ON c.artworkID=a.artworkID
			        WHERE c.enable=1 AND c.artworkID=13
			    ";

			    $query_order_categories=" ORDER BY ".UTILS::select_lang($_GET['lang'],"sort")." ASC ";
			    $options_query_category=array();
			    $options_query_category['columns']=",a.titleurl AS a_titleurl";
			    $query_categories=QUERIES::query_category($db,$query_where_categories,$query_order_categories,"","",$options_query_category);
			    $results_categories=$db->query($query_categories['query']);
			    $count_categories=$db->numrows($results_categories);
			    $categories=array();
			    while( $row_categories=$db->mysql_array($results_categories) )
				{
					$categories[]=$row_categories;
				}

		        $html_print_body.="<h3 class='h3-section-title'>";
		            $html_print_body.="<a name='other'>".LANG_ARTWORK_SECTION_OTHERMEDIA."</a>";
		        $html_print_body.="</h3>\n";

	            $options_cat_thumbs=array();
	            $options_cat_thumbs=$options_head;
		        $options_cat_thumbs['categories']=$categories;
		        $options_cat_thumbs['count_categories']=$count_categories;
	            //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
	            $options_cat_thumbs['url_admin']="/admin/paintings/categories/edit/?catid=";
                $options_cat_thumbs['show_all']=1;
                $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/art/search/?artworkid=13&info=1";
	            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
	            $html_print_body.=$return_cat_thumbs['html_print'];
	            # end OTHER CATEGORIES

			}

		$html_print_body.="</div>";

	#END div main content

}
############################ *** END *** art/paintings & /art/overpainted-photographs - CATEGORY VIEW







############################ MAIN ART SECTION /art
elseif( empty($_GET['section_1']) )
{
	# title meta
	$html->title=LANG_HEADER_MENU_ARTWORK." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.min.css";
    $html->css[]="/css/screen.min.css";
    $html->css[]="/css/main.mobile.min.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.min.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

    # tootltips, tabs, date picker - style jquery ui
    //$html->css[]="/js/jquery_ui/css/jquery-ui-1.10.4.custom.css";

	$html_print_body.=$html_script;

	# meta-tags & head
	$options_head['meta_description']=LANG_META_DESCRIPTION_ART;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'artwork',$options_head);

	# left side blocks
	$options_left_side=array();
	$options_left_side=$options_head;
	$options_left_side['show_microsites']=1;
	$options_left_side['db']=$db;
	$html_print_head.=$html->left_side("",$options_left_side);

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['inner_html']=LANG_ARTWORK_TEXT_H1;
	    $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
	    $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

	    # header
		$options_header_block=array();
		$options_header_block['title_category_sub']=$title_category_sub;
		$options_header_block['artwork_1']=$artwork_1;
		$options_header_block['options_head']=$options_head;
		$options_header_block['html']=$html;
		$html_header=html_header_block($db,$options_header_block);

		$html_print_body.=$html_header;

        $class_landing_page="div-category-landing-page-padding-topbottom";
        $class_section_content="div-section-content";

        $html_print_body.="<div class='".$class_section_content." ".$class_landing_page." '>";

            $html_print_body.="<p class='p-section-description'>".LANG_ARTWORK_TEXT."</p>";

		    $categories=array();
		    $i=0;
		    $categories[$i]['art_categoryid']=1;
		    $categories[$i]['title']=LANG_ARTWORK_PHOTO_PAINTINGS;
		    $categories[$i]['src']="/g/art/art_works_lg.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/paintings";
		    $i++;
		    $categories[$i]['art_categoryid']=2;
		    $categories[$i]['title']=LANG_ARTWORK_ATLAS;
		    $categories[$i]['src']="/g/art/art_atlas_lg.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/atlas";
		    $i++;
		    $categories[$i]['art_categoryid']=3;
		    $categories[$i]['title']=LANG_ARTWORK_EDITIONS;
		    $categories[$i]['src']="/g/art/art_editions_lg.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/editions";
			$i++;
		    $categories[$i]['art_categoryid']=4;
		    $categories[$i]['title']=LANG_ARTWORK_DRAWINGS;
		    $categories[$i]['src']="/g/art/art_drawings_lg.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/drawings";
			$i++;
		    $categories[$i]['art_categoryid']=5;
		    $categories[$i]['title']=LANG_ARTWORK_OVERPAINTEDPHOTOGRAPHS;
		    $categories[$i]['src']="/g/art/art_overpainted_lg5.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/overpainted-photographs";
			$i++;
		    $categories[$i]['art_categoryid']=6;
		    $categories[$i]['title']=LANG_ARTWORK_OILSONPAPER;
		    $categories[$i]['src']="/g/art/oil_paper.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/oils-on-paper";
			$i++;
		    $categories[$i]['art_categoryid']=7;
		    $categories[$i]['title']=LANG_ARTWORK_WATERCOLOURS;
		    $categories[$i]['src']="/g/art/art_watercolours_lg5.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/watercolours";
			$i++;
		    $categories[$i]['art_categoryid']=8;
		    $categories[$i]['title']=LANG_ARTWORK_ARTISTS_BOOKS;
		    $categories[$i]['src']="/g/art/artists_books.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/literature/artists-books";
			$i++;
		    $categories[$i]['art_categoryid']=9;
		    $categories[$i]['title']=LANG_LEFT_SIDE_PRINTS;
		    $categories[$i]['src']="/g/art/prints.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/prints";
			$i++;
		    $categories[$i]['art_categoryid']=10;
		    $categories[$i]['title']=LANG_ARTWORK_SECTION_OTHERMEDIA;
		    $categories[$i]['src']="/g/art/other.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/other";
			$i++;
		    $categories[$i]['art_categoryid']=11;
		    $categories[$i]['title']=LANG_LEFT_SIDE_MICROSITES;
		    $categories[$i]['src']="/g/art/micro.jpg";
		    $categories[$i]['href']="/".$_GET['lang']."/art/microsites";
		    $categories[$i]['class']['div-section-category']="disable";

            $options_cat_thumbs=array();
            $options_cat_thumbs=$options_head;
	        $options_cat_thumbs['categories']=$categories;
	        $options_cat_thumbs['count_categories']=count($categories)+1;
            //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
            $options_cat_thumbs['url_admin']="";
            $options_cat_thumbs['show_all']=1;
            $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/art/search/?info=1";
            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
            $html_print_body.=$return_cat_thumbs['html_print'];

		$html_print_body.="\t</div>\n";

	#END div main content

}
############################ *** END *** MAIN ART SECTION /art







############################ Zoomer art/zoomer
elseif( $_GET['section_1']=="zoomer" )
{
	$html_print_body="<div id='div-zoomer-content' role='main'>";

	if( $_GET['section_3']=="cage-1" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$_GET['section_3']."/".$_GET['section_3']."_500.jpg";
		$detail="/datadir/zoomer/cage/".$_GET['section_3']."/".$_GET['section_3']."_2500.jpg";
	    $paintid=13796;
	    $title="Cage 1";
	}
	elseif( $_GET['section_3']=="cage-2" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$dir."/".$dir."_500.jpg";
		$detail="/datadir/zoomer/cage/".$dir."/".$dir."_2500.jpg";
	    $paintid=13806;
	    $title="Cage 2";
	}
	elseif( $_GET['section_3']=="cage-3" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$dir."/".$dir."_500.jpg";
		$detail="/datadir/zoomer/cage/".$dir."/".$dir."_2500.jpg";
	    $paintid=13807;
	    $title="Cage 3";
	}
	elseif( $_GET['section_3']=="cage-4" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$dir."/".$dir."_500.jpg";
		$detail="/datadir/zoomer/cage/".$dir."/".$dir."_2500.jpg";
	    $paintid=13808;
	    $title="Cage 4";
	}
	elseif( $_GET['section_3']=="cage-5" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$dir."/".$dir."_500.jpg";
		$detail="/datadir/zoomer/cage/".$dir."/".$dir."_2500.jpg";
	    $paintid=13809;
	    $title="Cage 5";
	}
	elseif( $_GET['section_3']=="cage-6" )
	{
		$dir=$_GET['section_3'];
		$preview="/datadir/zoomer/cage/".$dir."/".$dir."_500.jpg";
		$detail="/datadir/zoomer/cage/".$dir."/".$dir."_2500.jpg";
	    $paintid=13810;
	    $title="Cage 6";
	}
	elseif( $_GET['section_2']=="blau" )
	{
		$dir=$_GET['section_2'];
		$preview="/datadir/zoomer/".$dir."/src/pic/cr-658_blau_500.jpg";
		$detail="/datadir/zoomer/".$dir."/src/pic/cr-658_blau_2500.jpg";
	    $title="Blau";
	}
	else
	{
		UTILS::not_found_404();
	}

	# title meta
	$html->title=$title." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.min.css";
    $html->js[]="/js/zoomer.js";
    $html->js[]="/js/zoomer-init.js";
    $html->onload[]="init();";
    $html->css[]="/css/zoomer.css";
    $html->css[]="/css/cage_zoomer.css";

	# meta-tags & head
	//$options_head['meta_description']=;
	$options_head['only_meta_info']=1;
	$options_head['class']['body']="body";
	$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'',$options_head);

	$html_print_body.="<div class='zoom' preview='".$preview."' detail='".$detail."' ></div>\n";
}
############################ *** END *** Zoomer art/zoomer






############################ Microsites art/microsites
elseif( $_GET['section_1']=="microsites" )
{
	if( !empty($_GET['section_2']) )
	{
		require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/microsites/microsites.php");

		$html_print_body="<div id='div-microsite-content' role='main'>";

	    $results_microsite=$db->query("SELECT * FROM ".TABLE_MICROSITES." WHERE enable=1 AND titleurl='".$_GET['section_2']."' LIMIT 1 ");
	    $count_microsite=$db->numrows($results_microsite);

	    # SNOW-WHITE
		if( $_GET['section_2']=="snow-white" )
		{
			if( !empty($_GET['section_3']) ) UTILS::not_found_404();

			if( $_GET['sp']==32 ) $_GET['sp']=20;
			if( empty($_GET['l']) ) $_GET['l']=0;
			$search_array=UTILS::search_array($_GET['s']);

			/*
			if( !empty( $search_array ) )
			{
			    foreach( $search_array as $value )
			    {
			        $tmp[]=(13557+$value);
			    }
			}
			$search_array=$tmp;
			*/

			# css & js files
			$html->js[]="/js/jquery/jquery-2.1.4.min.js";
			$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
			$html->css[]="/css/microsites/microsite.snow-white.css";
			$html->css[]="/css/main.mobile.min.css";

			# tabs and tooltips
			$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.min.css";

			# fancybox
			$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

			### information about Snow-white edition
			$paintid="12813";
			/*
			$query_edition_painting=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");
			$row_edition_painting=$db->mysql_array($query_edition_painting);
			$row_edition_painting=UTILS::html_decode($row_edition_painting);
			$title_microsite=UTILS::row_text_value2($db,$row_edition_painting,"title");
			$text=UTILS::row_text_value2($db,$row_edition_painting,"artwork_notes");
			*/
			$micrositeid=7;
		    $query_where_microsite_snow=" WHERE micrositeid='".$micrositeid."' ";
		    $query_limit_microsite_snow=" LIMIT 1 ";
		    $query_microsite_snow=QUERIES::query_microsites($db,$query_where_microsite_snow,"",$query_limit_microsite_snow);
		    $results_microsite_snow=$db->query($query_microsite_snow['query']);
		    $row_microsite_snow=$db->mysql_array($results_microsite_snow);
		    $row_microsite_snow=UTILS::html_decode($row_microsite_snow);
		    $title_microsite=UTILS::row_text_value($db,$row_microsite_snow,"title");
		    $text=UTILS::row_text_value($db,$row_microsite_snow,"text");
			# end

			$options_microsite_snowwhite=array();
			$options_microsite_snowwhite['titleurl']=$_GET['section_3'];
			$options_microsite_snowwhite['title']=$title_microsite;
			$options_microsite_snowwhite['text']=$text;
			$options_microsite_snowwhite['paintid']=$paintid;
			$options_microsite_snowwhite['micrositeid']=$micrositeid;
			$options_microsite_snowwhite['row']=$row_microsite_snow;
			$options_microsite_snowwhite['search_array']=$search_array;
			$options_microsite_snowwhite['searchstring']=$_GET['s'];
			$html_print_body.=microsite_snow_white($db,$options_microsite_snowwhite);
		}
		elseif( $_GET['section_2']=="war-cut" || $_GET['section_2']=="war-cut-ii" )
		{
			if( $_GET['section_2']=="war-cut" ) UTILS::redirect("/".$_GET['lang']."/art/microsites/war-cut-ii",301);
			if( !empty($_GET['section_3']) ) UTILS::not_found_404();

			if( $_GET['sp']==32 ) $_GET['sp']=20;
			if( empty($_GET['l']) ) $_GET['l']=0;
			$search_array=UTILS::search_array($_GET['s']);

			/*
			if( !empty( $search_array ) )
			{
			    foreach( $search_array as $value )
			    {
			        $tmp[]=(13658+$value);
			    }
			}
			$search_array=$tmp;
			*/

			# css & js files
			$html->js[]="/js/jquery/jquery-2.1.4.min.js";
			$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
			$html->css[]="/css/microsites/microsite.war-cut.css";
			$html->css[]="/css/main.mobile.min.css";

			# tabs and tooltips
			$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.min.css";

			# fancybox
			$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

			### information about War Cut edition
			$paintid="12672";
			/*
			$query_edition_painting=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");
			$row_edition_painting=$db->mysql_array($query_edition_painting);
			$row_edition_painting=UTILS::html_decode($row_edition_painting);
			$title_microsite=UTILS::row_text_value2($db,$row_edition_painting,"title");
			$text=UTILS::row_text_value2($db,$row_edition_painting,"artwork_notes");
			*/
			$micrositeid=8;
		    $query_where_microsite_war=" WHERE micrositeid='".$micrositeid."' ";
		    $query_limit_microsite_war=" LIMIT 1 ";
		    $query_microsite_war=QUERIES::query_microsites($db,$query_where_microsite_war,"",$query_limit_microsite_war);
		    $results_microsite_war=$db->query($query_microsite_war['query']);
		    $row_microsite_war=$db->mysql_array($results_microsite_war);
		    $row_microsite_war=UTILS::html_decode($row_microsite_war);
		    $title_microsite=UTILS::row_text_value($db,$row_microsite_war,"title");
		    $text=UTILS::row_text_value($db,$row_microsite_war,"text");
			# end

			$options_microsite_war_cut=array();
			$options_microsite_war_cut['titleurl']=$_GET['section_3'];
			$options_microsite_war_cut['title']=$title_microsite;
			$options_microsite_war_cut['text']=$text;
			$options_microsite_war_cut['paintid']=$paintid;
			$options_microsite_war_cut['micrositeid']=$micrositeid;
			$options_microsite_war_cut['row']=$row_microsite_war;
			$options_microsite_war_cut['search_array']=$search_array;
			$options_microsite_war_cut['searchstring']=$_GET['s'];
			$html_print_body.=microsite_war_cut($db,$options_microsite_war_cut);
		}
		elseif( $_GET['section_2']=="firenze" )
		{
			if( !empty($_GET['section_3']) ) UTILS::not_found_404();

			if( $_GET['sp']==32 ) $_GET['sp']=20;
			if( empty($_GET['l']) ) $_GET['l']=0;
			$search_array=UTILS::search_array($_GET['s']);

			# css & js files
			$html->js[]="/js/jquery/jquery-2.1.4.min.js";
			$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
			$html->css[]="/css/microsites/microsite.firenze.css";
			$html->css[]="/css/main.mobile.min.css";

			# tabs and tooltips
			$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.min.css";

			# fancybox
			$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
			$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
			$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

			### information about firenze edition
			$paintid="12798";
			/*
			$query_edition_painting=$db->query("SELECT * FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' ");
			$row_edition_painting=$db->mysql_array($query_edition_painting);
			$row_edition_painting=UTILS::html_decode($row_edition_painting);
			$title_microsite=UTILS::row_text_value2($db,$row_edition_painting,"title");
			$text=UTILS::row_text_value2($db,$row_edition_painting,"artwork_notes");
			*/
			$micrositeid=9;
		    $query_where_microsite_firenze=" WHERE micrositeid='".$micrositeid."' ";
		    $query_limit_microsite_firenze=" LIMIT 1 ";
		    $query_microsite_firenze=QUERIES::query_microsites($db,$query_where_microsite_firenze,"",$query_limit_microsite_firenze);
		    $results_microsite_firenze=$db->query($query_microsite_firenze['query']);
		    $row_microsite_firenze=$db->mysql_array($results_microsite_firenze);
		    $row_microsite_firenze=UTILS::html_decode($row_microsite_firenze);
		    $title_microsite=UTILS::row_text_value($db,$row_microsite_firenze,"title");
		    $text=UTILS::row_text_value($db,$row_microsite_firenze,"text");
			# end

			$options_microsite_firenze=array();
			$options_microsite_firenze['titleurl']=$_GET['section_3'];
			$options_microsite_firenze['title']=$title_microsite;
			$options_microsite_firenze['text']=$text;
			$options_microsite_firenze['paintid']=$paintid;
			$options_microsite_firenze['micrositeid']=$micrositeid;
			$options_microsite_firenze['row']=$row_microsite_firenze;
			$options_microsite_firenze['search_array']=$search_array;
			$options_microsite_firenze['searchstring']=$_GET['s'];
			$html_print_body.=microsite_firenze($db,$options_microsite_firenze);
		}
		elseif( $count_microsite>0 )
		{
			$row_microsite=$db->mysql_array($results_microsite);
			$row_microsite=UTILS::html_decode($row_microsite);
			$title_microsite=UTILS::row_text_value($db,$row_microsite,"title");
			$text=UTILS::row_text_value($db,$row_microsite,"text");

			# css & js files
			if( $row_microsite['micrositeid']==1 )
			{
				# 4900 COLORS
				if( !empty($_GET['section_4']) ) UTILS::not_found_404();

				# css & js files
				$html->js[]="/js/jquery/jquery-2.1.4.min.js";
				//$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
				$html->css[]="/css/microsites/microsite.4900.css";
				$html->css[]="/css/main.mobile.min.css";

				# tabs and tooltips
				//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

				# fancybox
				$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

				if( $_GET['sp']==32 ) $_GET['sp']=20;
				if( empty($_GET['l']) ) $_GET['l']=0;

				$options_microsite_4900=array();
				$options_microsite_4900['titleurl']=$_GET['section_3'];
				$options_microsite_4900['title']=$title_microsite;
				$options_microsite_4900['text']=$text;
				$html_print_body.=microsite_4900($db,$options_microsite_4900);
			}
			elseif( $row_microsite['micrositeid']==3 )
			{
				# SINBAD
				if( !empty($_GET['section_3']) ) UTILS::not_found_404();

				# css & js files
				$html->js[]="/js/jquery/jquery-2.1.4.min.js";
				//$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
				$html->css[]="/css/microsites/microsite.sinbad.css";
				$html->css[]="/css/main.mobile.min.css";

				# tabs and tooltips
				//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

				# fancybox
				$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

				if( $_GET['sp']==32 ) $_GET['sp']=20;
				if( empty($_GET['l']) ) $_GET['l']=0;

				$options_microsite_sinbad=array();
				$options_microsite_sinbad['titleurl']=$_GET['section_3'];
				$options_microsite_sinbad['title']=$title_microsite;
				$options_microsite_sinbad['text']=$text;
				$html_print_body.=microsite_sinbad($db,$options_microsite_sinbad);
			}
			elseif( $row_microsite['micrositeid']==4 )
			{
				# ELBE
				if( !empty($_GET['section_3']) ) UTILS::not_found_404();

				# css & js files
				$html->js[]="/js/jquery/jquery-2.1.4.min.js";
				//$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
				$html->css[]="/css/microsites/microsite.elbe.css";
				$html->css[]="/css/main.mobile.min.css";

				# tabs and tooltips
				//$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

				# fancybox
				$html->js[]="/js/jquery_fancybox/jquery.fancybox.min.js?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/jquery.fancybox.min.css?v=2.1.0";
				$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.css?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.min.js?v=1.0.3";
				$html->js[]="/js/jquery_fancybox/fancybox.load.min.js";

				if( $_GET['sp']==32 ) $_GET['sp']=20;
				if( empty($_GET['l']) ) $_GET['l']=0;

				$options_microsite_sinbad=array();
				$options_microsite_sinbad['titleurl']=$_GET['section_3'];
				$options_microsite_sinbad['title']=$title_microsite;
				$options_microsite_sinbad['text']=$text;
				$html_print_body.=microsite_elbe($db,$options_microsite_sinbad);
			}
			elseif( $row_microsite['micrositeid']==5 )
			{
				# NOVEMBER
				if( !empty($_GET['section_3']) ) UTILS::not_found_404();

				$html->css[0]="/includes/microsites/november/src/css/richter.ms_november.css";
				$html->css[1]['url']="/includes/microsites/november/src/css/richter.ms_november_ie8.css";
				$html->css[1]['ie']=7;
				$html->css[2]['url']="/includes/microsites/november/src/css/richter.ms_november_ie8.css";
				$html->css[2]['ie']=8;
				$html->js[]="/includes/microsites/november/src/js/jquery-1.8.3.min.js";
				$html->js[]="/includes/microsites/november/src/js/richter.ms_november.js";

				$options_microsite_november=array();
				$options_microsite_november['titleurl']=$_GET['section_3'];
				$options_microsite_november['title']=$title_microsite;
				$options_microsite_november['text']=$text;
				$html_print_body.=microsite_november($db,$options_microsite_november);
			}
		}
		else
		{
			UTILS::not_found_404();
		}

		# title meta
		$html->title=$title_microsite." &raquo; ".LANG_RIGHT_FEATURED_TITLES." &raquo; ".LANG_TITLE_HOME;
		$html->lang=$_GET['lang'];

		# css & js files
		$html->js[]="/js/common.js";

		# meta-tags & head
		//$options_head['meta_description']=;
		$options_head['only_meta_info']=1;
		$options_head['class']['titleurl']="t".$row_microsite['titleurl'];
		$options_head['class']['div-main-old']="div-main-old";
		$html_print_head.=$html->head($db,'',$options_head);

		if( $row_microsite['micrositeid']!=5 )
		{
			$html_print_body.="<div id='footer'>\n";
			    $html_print_body.="<span class='copyright'>&copy; ".date("Y")."</span>\n";
			$html_print_body.="</div>\n";
		}

	}
	else
	{
		# title meta
		$html->title=LANG_LEFT_SIDE_MICROSITES." &raquo; ".LANG_TITLE_HOME;
		$html->lang=$_GET['lang'];

	    # css & js files
	    $html->css[]="/css/main.min.css";
	    $html->css[]="/css/screen.min.css";
	    $html->css[]="/css/main.mobile.min.css";
		$html->js[]="/js/jquery/jquery-2.1.4.min.js";
		$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	    $html->js[]="/js/common.min.js";

		# mobile menu
		$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
		$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
		$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

	    # select boxes
	    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
	    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	    # tootltips, tabs, date picker - style jquery ui
	    //$html->css[]="/js/jquery_ui/css/jquery-ui-1.10.4.custom.css";

		$html_print_body.=$html_script;

		# meta-tags & head
		$options_head['meta_description']=LANG_META_DESCRIPTION_ART;
		$options_head['class']['body']="body";
		$html_print_head.=$html->head($db,'artwork',$options_head);

		# left side blocks
		$options_left_side=array();
		$options_left_side=$options_head;
		$options_left_side['show_microsites']=1;
		$options_left_side['db']=$db;
		$html_print_head.=$html->left_side("",$options_left_side);

		# div main content

		    ### Breadcrubmles
		    $options_breadcrumb=array();
		   	$options_breadcrumb=$options_head;
		    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
		    $options_breadcrumb['class']['div']="breadcrumb-large";
		    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/art";
		    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_ARTWORK;
		    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_ARTWORK;
		    $options_breadcrumb['items'][1]['inner_html']=LANG_LEFT_SIDE_MICROSITES;
			$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
	    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
		    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
		    #end

		    # header
			$options_header_block=array();
			$options_header_block['title_category_sub']=LANG_LEFT_SIDE_MICROSITES;
			$options_header_block['artwork_1']=$artwork_1;
			$options_header_block['options_head']=$options_head;
			$options_header_block['html']=$html;
			$html_header=html_header_block($db,$options_header_block);

			$html_print_body.=$html_header;

	        $class_landing_page="div-category-landing-page-padding-topbottom";
	        $class_section_content="div-section-content";

	        $html_print_body.="<div class='".$class_section_content." ".$class_landing_page." '>";

			    $categories=array();
			    $i=0;

                $results_microsites=$db->query("SELECT * FROM ".TABLE_MICROSITES." WHERE enable=1 ORDER BY sort ");
                while( $row_microsites=$db->mysql_array($results_microsites) )
                {
                    $title=UTILS::row_text_value($db,$row_microsites,"title");

                    $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=3 AND itemid2='".$row_microsites['micrositeid']."' ) OR ( typeid2=17 AND typeid1=3 AND itemid1='".$row_microsites['micrositeid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
                    $row_related_image=$db->mysql_array($results_related_image);
                    $imageid=UTILS::get_relation_id($db,"17",$row_related_image);

				    $categories[$i]['art_categoryid']=$row_microsites['micrositeid'];
				    $categories[$i]['title']=$title;
				    $categories[$i]['src']="/images/size_s__imageid_".$imageid;
				    $categories[$i]['href']="/".$_GET['lang']."/art/microsites/".$row_microsites['titleurl'];
				    $categories[$i]['target']="_blank";

				    $i++;
				}

	            $options_cat_thumbs=array();
	            $options_cat_thumbs=$options_head;
		        $options_cat_thumbs['categories']=$categories;
		        $options_cat_thumbs['count_categories']=count($categories);
	            //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-border-bottom";
	            $options_cat_thumbs['url_admin']="/admin/microsites/edit/?micrositeid=";
	            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
	            $html_print_body.=$return_cat_thumbs['html_print'];

			$html_print_body.="\t</div>\n";

		#END div main content
	}
}
############################ *** END *** art/microsites





else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
