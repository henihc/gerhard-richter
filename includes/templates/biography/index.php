<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_body="<div id='div-large-content' role='main' >";
$html_print_footer="";

# /biography/photos/xxxx-xxxx/xx DETAIL photo view
if( !empty($_GET['section_2']) && !empty($_GET['section_3']) && empty($_GET['section_4']) )
{
	$photoid=UTILS::itemid($db,$_GET['section_3']);

	# title meta
	$html->title=LANG_PHOTOS." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/biography-photos.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/fancybox.load.html.js";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'biography',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$html_print_head.=$html->ul_biography($db,'photos',$_GET['section_2'],$options_left_side);
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/biography";
	    $options_breadcrumb['items'][0]['title']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/biography/photos/#".$_GET['section_2'];
	    $options_breadcrumb['items'][1]['title']=LANG_PHOTOS;
	    $options_breadcrumb['items'][1]['inner_html']=LANG_PHOTOS;
	    $title_photos_section=str_replace("-", "&#8211;", $_GET['section_2']);
	    $options_breadcrumb['items'][2]['inner_html']=$title_photos_section;
    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end
	    # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_PHOTOS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/biography/1.jpg' alt='' width='523px' height='208px' />";
            $html_print_body.="</div>\n";
            # search block
            $html_print_body.="<div class='div-content-header-block-right'>";
	        	$html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
	        	$html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
            $html_print_body.="</div>\n";
            # END search block
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block


	    $query_photos="SELECT * FROM ".TABLE_PHOTOS." WHERE photoID='".$photoid."' LIMIT 1 ";
	    //print $query_photos;
        $results_photo=$db->query($query_photos);
        $row_photo=$db->mysql_array($results_photo);
        $count_photo=$db->numrows($results_photo);

        if( $count_photo )
        {
	        $options_next_links=array();
	        $options_next_links=$options_head;
	        $options_next_links['row']=$row_photo;
	        $options_next_links['url']="/biography/photos/".$_GET['section_2']."/";
	        $html_print_body.=$html->previous_next_links_photos($db,$options_next_links);

	        $query_photo_relations="SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=8 AND itemid2='".$row_photo['photoID']."' ) OR ( typeid2=17 AND typeid1=8 AND itemid1='".$row_photo['photoID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ";
	        //print $query_photo_relations;
	        $results_related_image=$db->query($query_photo_relations);
	        $row_related_image=$db->mysql_array($results_related_image);

	        //print_r($row_related_image);
	        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
	        //$src="/images/size_lll__imageid_".$imageid.".jpg";
	        $src=DATA_PATH_DATADIR."/images_new/lllarge/".$imageid.".jpg";

	        $html_print_body.="<img src='".$src."' alt='' />\n";
    	}
    	else
    	{
    		UTILS::not_found_404();
    	}

	#END div main content
}

# /biography/search
elseif( $_GET['section_0']=="biography" && $_GET['section_1']=="search" && empty($_GET['section_2']) )
{
	# title meta
	$html->title=LANG_HEADER_MENU_SEARCH." &raquo; ".LANG_HEADER_MENU_BIOGRAPHY." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/biography.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/fancybox.load.html.js";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'biography',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$html_print_head.=$html->ul_biography($db,'biography',"",$options_left_side);
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/biography";
	    $options_breadcrumb['items'][0]['title']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end
	    # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_PHOTOS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/biography/1.jpg' alt='' width='523px' height='208px' />";
            $html_print_body.="</div>\n";
            # search block
            $html_print_body.="<div class='div-content-header-block-right'>";
	        	$html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
	        	$html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
            $html_print_body.="</div>\n";
            # END search block
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

	    $options_search=array();
	    $options_search['db']=$db;
	    $prepear_search=UTILS::prepear_search($_GET['keyword'],$options_search);
	    # prepare match
	    $options_search_biography=array();
	    $options_search_biography['search']=$prepear_search;
	    $query_match_biography=QUERIES_SEARCH::query_search_biography($db,$options_search_biography);
	    # END prepare match

	    $query_where=" WHERE bi.enable=1 AND ".$query_match_biography;
	    $query_order=" ORDER BY bi.sort ASC ";
	    $query_limit=" LIMIT ".$limit_biography;
	    $query_biography=QUERIES::query_biography($db,$query_where,$query_order,$query_limit);
	    $results_biography_total=$db->query($query_biography['query_without_limit']);
	    $count_biography_total=$db->numrows($results_biography_total);

	    if( $count_biography_total )
	    {
	        while( $row=$db->mysql_array($results_biography_total,0) )
	        {
	            $values_row_text=array();
	            $values_row_text['search']=$_GET['keyword'];
	            $title=UTILS::row_text_value($db,$row,"title",$values_row_text);
	            $title_no_html=strip_tags($title);
	            $row=UTILS::html_decode($row);
	            $values_row_text=array();
	            $values_row_text['search']=$_GET['keyword'];
	            $text=UTILS::row_text_value($db,$row,"text",$values_row_text);
	            $text=str_replace($title,"",$text);

	            $options_search_found=array();
	            $options_search_found['search']=$_GET['keyword'];
	            $options_search_found['text']=$title;
	            $options_search_found['limit']=14;
	            $options_search_found['strip_tags']=1;
	            //$options_search_found['convert_utf8']=1;
	            $options_search_found['a_name']=1;
	            $title=UTILS::show_search_found_keywords($db,$options_search_found);

	            $options_search_found=array();
	            $options_search_found['search']=$_GET['keyword'];
	            $options_search_found['text']=$text;
	            $options_search_found['type']=1;
	            $options_search_found['limit']=70;
	            $options_search_found['show_dots']=1;
	            //$options_search_found['convert_utf8']=1;
	            $options_search_found['a_name']=1;
	            $text=UTILS::show_search_found_keywords($db,$options_search_found);

	            $url="/biography/".$row['titleurl']."/?search=".$_GET['keyword']."#".$_GET['keyword'];
	            $html_print_body.="<div class='div-biography-search-result'>\n";
	            	$options_admin_edit=array();
	    			$options_admin_edit=$options_head;
	                $html_print_body.=UTILS::admin_edit("/admin/biography/edit/?biographyid=".$row['biographyid'],$options_admin_edit);
	                $html_print_body.="<a href='".$url."' title='".$title_no_html."'>\n";
	                    $html_print_body.="<span class='span-bigraphy-search-title'>".$title."</span>\n";
	                    $html_print_body.="<span class='span-bigraphy-search-text'>".$text."</span>\n";
	                $html_print_body.="</a>\n";
	            $html_print_body.="</div>\n";
	        }
	    }
	    else $html_print_body.=LANG_SEARCH_NORESULTS;

	#END div main content
}
# END /biography/search

# /biography/timeline
elseif( $_GET['section_0']=="biography" && $_GET['section_1']=="timeline" && empty($_GET['section_2']) )
{
	# title meta
	$html->title=LANG_RIGHT_TIMELINE_INTER." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/includes/templates/biography/timeline/src/css/index.css";
	$html->css[]="/includes/templates/biography/timeline/src/css/timeline.css";
	$html->js[]="/includes/templates/biography/timeline/src/js/era.js";
	$html->js[]="/includes/templates/biography/timeline/src/js/timeline.js";
	$html->js[]="/includes/templates/biography/timeline/src/js/index.js";
	$html->onload[]="init();";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$options_head['only_meta_info']=1;
	$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'',$options_head);

	# div main content

		$url_xml="/includes/templates/biography/timeline/src/xml/timeline.xml";
        $html_print_body.="<div class='timeline' src='".$url_xml."' language='".$_GET['lang']."'></div>\n";

	#END div main content
}
# END /biography/timeline

# /biography/photos
elseif( $_GET['section_0']=="biography" && $_GET['section_1']=="photos" && empty($_GET['section_3']) )
{
	# title meta
	$html->title=LANG_PHOTOS." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/biography-photos.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/fancybox.load.html.js";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'biography',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$html_print_head.=$html->ul_biography($db,'photos',$_GET['section_2'],$options_left_side);
	$html_print_head.="</div>";

	# div main content

		$results=$db->query("SELECT * FROM ".TABLE_PHOTOS." WHERE year BETWEEN '1960' AND '1969' ORDER BY photoID ASC");
		$row=$db->mysql_array($results);

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	    $options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/biography";
	    $options_breadcrumb['items'][0]['title']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][1]['inner_html']=LANG_PHOTOS;
    	$options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
    	$options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

	    # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_PHOTOS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/biography/1.jpg' alt='' width='523px' height='208px' />";
            $html_print_body.="</div>\n";
            # search block
            $html_print_body.="<div class='div-content-header-block-right'>";
	        	$html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
	        	$html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
            $html_print_body.="</div>\n";
            # END search block
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

		$html_print_body.="<div class='div-section-content div-category-landing-page-padding-topbottom'>";

		    $years_photos=array();
		    $years_photos[0]['from']=1960;
		    $years_photos[0]['to']=1969;
		    $years_photos[1]['from']=1970;
		    $years_photos[1]['to']=1979;
		    $years_photos[2]['from']=1980;
		    $years_photos[2]['to']=1989;
		    $years_photos[3]['from']=1990;
		    $years_photos[3]['to']=1999;
		    $years_photos[4]['from']=2000;
		    $years_photos[4]['to']=date("Y");

		    foreach ($years_photos as $key => $value )
		    {
		    	$query_photos="SELECT * FROM ".TABLE_PHOTOS." WHERE year BETWEEN '".$value['from']."' AND '".$value['to']."' ORDER BY photoID ASC";
			    $results_photos=$db->query($query_photos);
			    $count_photos=$db->numrows($results_photos);
			    $html_print_body.="<a name='".$value['from']."-".$value['to']."' class='a-biography-photos-year-range'>".$value['from']."&#150;".$value['to']."</a>\n";
			    $options_thumb_img=array();
			    $options_thumb_img=$options_head;
			    $options_thumb_img['results']=$results_photos;
			    $options_thumb_img['url']="/".$_GET['lang']."/biography/photos/".$value['from']."-".$value['to']."/";
			    $options_thumb_img['thumbs_per_line']=4;
			    $options_thumb_img['values_from']=0;
			    $options_thumb_img['values_to']=$count_photos;
			    $html_print_body.=$html->div_thumb_images($db,$options_thumb_img);
		    }

	    $html_print_body.="</div>";

	#END div main content
}
# END /biography/photos

# MAIN BIOGRAPHY SECTION /biography
elseif( $_GET['section_0']=="biography" && empty($_GET['section_2']) )
{
	//print_r($_GET);
	if( empty($_GET['section_1']) )
	{
	    $query_where_biography=" WHERE enable=1 ";
	    $query_order_biography=" ORDER BY sort ASC ";
	    $query_order_limit=" LIMIT 1 ";
	    $query_biography=QUERIES::query_biography($db,$query_where_biography,$query_order_biography,$query_order_limit);
	    $results_biography=$db->query($query_biography['query']);
	    $row_biography=$db->mysql_array($results_biography,0);
	    $biographyid=$row_biography['biographyid'];
	    $section=$row_biography['titleurl'];
	}
	else
	{
		$biographyid=UTILS::itemid($db,$_GET['section_1']);
		$section=$_GET['section_1'];
	}

	if( !empty($biographyid) )
	{
	    //$query_where_biography=" WHERE enable=1 AND biographyid='".$biographyid."' ";
	    $query_where_biography=" WHERE enable=1 AND titleurl='".$section."' ";
	    $query_order_biography=" ORDER BY sort ASC ";
	    $query_biography=QUERIES::query_biography($db,$query_where_biography,$query_order_biography);
	    $results_biography=$db->query($query_biography['query']);
	    $count_biography=$db->numrows($results_biography);
	    if( $count_biography>0 )
	    {
	        $row_biography=$db->mysql_array($results_biography);
	        $title_biography=UTILS::row_text_value($db,$row_biography,"title");
	        $title_meta=$title_biography;
	    }
	}

	$title_meta.=" &raquo; ".LANG_HEADER_MENU_BIOGRAPHY;


	# title meta
	$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/biography.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# fancybox
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/fancybox.load.html.js";

	# meta-tags & head
	$options_head=array();
	$options_head['meta_description']=LANG_META_DESCRIPTION_BIOGRAPHY;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	//$options_head['class']['div-main-old']="div-main-old";
	$html_print_head.=$html->head($db,'biography',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$html_print_head.=$html->ul_biography($db,$biographyid,'',$options_left_side);
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/biography";
	    $options_breadcrumb['items'][0]['title']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_BIOGRAPHY;
	    $options_breadcrumb['items'][1]['inner_html']=$title_biography;
    	$options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
    	$options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end
        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_HEADER_MENU_BIOGRAPHY;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/biography/1.jpg' alt='' width='523px' height='208px' />";
            $html_print_body.="</div>\n";
            # search block
            $html_print_body.="<div class='div-content-header-block-right'>";
	        	$html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
	        	$html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
            $html_print_body.="</div>\n";
            # END search block
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

	    if( $count_biography )
	    {
	        $row_biography=UTILS::html_decode($row_biography);
	        $text_biography=UTILS::row_text_value($db,$row_biography,"text");

	        $values_search_found=array();
	        $values_search_found['text']=$text_biography;
	        $values_search_found['strip_tags']=1;
	        $values_search_found['convert_utf8']=1;
	        $values_search_found['search']=$_GET['search'];
	        $text_biography=UTILS::show_search_found_keywords($db,$values_search_found);

	        $title=UTILS::row_text_value($db,$row_biography,"title");
	        $values_search_found=array();
	        $values_search_found['text']=$title;
	        $values_search_found['strip_tags']=1;
	        $values_search_found['convert_utf8']=1;
	        $values_search_found['search']=$_GET['search'];
	        $title=UTILS::show_search_found_keywords($db,$values_search_found);

	        $html_print_body.="<h3 class='h3-biography-title'>".$title."</h3><br />";

	        $html_print_body.="<div class='div-section-content div-section-content-text'>";
	            $options_div_text=array();
	            $options_div_text['class']['div_text_wysiwyg_biography']="div_text_wysiwyg_biography";
	            $html_print_body.=$html->div_text_wysiwyg($db,$text_biography,$options_div_text);
            $html_print_body.="</div>";
	    }
		else
		{
			UTILS::not_found_404();
		}

	#END div main content

}
# *** END *** MAIN BIOGRAPHY SECTION /biography

else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
