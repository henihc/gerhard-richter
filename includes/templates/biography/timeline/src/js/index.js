/*
	| index.js
	| 
	| Description: Example javascript to initialise XML based Timelines (timeline.js)
	| Author: Eric Kuhnert
	| Date: 2012-01-25
	|
*/

// .............................................................................................
function init()
{
	// events
	era.Pointer.mousemove = function (_evt) { timeline.mousemove(_evt); }
	era.Pointer.mousedown = function (_evt) { timeline.mousedown(_evt); }
	era.Pointer.mouseup = function (_evt) { timeline.mouseup(_evt); }	
	era.Pointer.touchstart = function (_evt) { timeline.touchstart(_evt); }	
	era.Pointer.touchmove = function (_evt) { timeline.touchmove(_evt); }	
	era.Pointer.touchend = function (_evt) { timeline.touchend(_evt); }	
	
	// create timelines
	timeline.init();
}
