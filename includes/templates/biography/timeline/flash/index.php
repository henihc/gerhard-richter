<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/html.php");
//require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/log.php");

$html = new html_elements;
$html->title=LANG_TITLE_BIOGRAPHY_TIMELINE;
$html->js[]="/js/swfobject.js";
$html->css[]="/css/timeline.css";
$html->metainfo($class="body",0);

    $var['id']="flash_timeline";
    $var['url']="timeline.swf";
    $var['width']="800";
    $var['height']="450";
    $var['version']="1";
    $var['bgcolor']="#fff";
    if( $_SESSION['lang']=="ch" ) $lang="cn";
    else $lang=$_SESSION['lang'];
    $var['lang']=$lang;
    print $html->flash($db,$var);

$html->footer();
?>
