<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html_print_head="";
$html_print_body="";
$html_print_body.="<div id='div-large-content' role='main'>";
$html_print_footer="";

$html = new html_elements;
$html->title=LANG_HEADER_MENU_CHRONOLOGY." &raquo; ".LANG_TITLE_HOME;
$html->lang=$_GET['lang'];

$html->css[]="/css/main.css";
$html->css[]="/css/screen.css";
$html->css[]="/css/main.mobile.css";
$html->css[]="/css/chronology.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

# fancybox
$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
$html->js[]="/js/jquery_fancybox/fancybox.load.html.js";

$options_head=array();
$options_head['meta_description']=LANG_META_DESCRIPTION_CHRONOLOGY;
$options_head['html_return']=1;
$options_head['class']['body']="body";
$options_head['class']['div-main-old']="div-main-old";
$html_print_head.=$html->head($db,'chronology',$options_head);

$textid=3;

# select info about section
$values_query_text=array();
$values_query_text['where']=" WHERE textid='".$textid."' ";
$query_text=QUERIES::query_text($db,$values_query_text);
$results=$db->query($query_text['query']);
$count=$db->numrows($results);
$row=$db->mysql_array($results);
$row=UTILS::html_decode($row);
$values_row_text=array();
$values_row_text['search']=$_GET['search'];
$text=UTILS::row_text_value($db,$row,"text",$values_row_text);

$html_print_body.="\t<script type='text/javascript'>\n";
    $html_print_body.="$(document).ready(function() {";
        $html_print_body.="$('a[name]').css('cssText','color:#000 !important;border:none !important;');";
    $html_print_body.="});";
$html_print_body.="\t</script>\n";

# left side blocks
$html_print_head.="<div id='sub-nav'>";
    $options_left_side=array();
    $options_left_side=$options_head;
    $html_print_head.=$html->ul_chronology($db,"",$options_left_side);
$html_print_head.="</div>";

# div main content

if( $_GET['section_0']=="chronology" && empty($_GET['section_1']) )
{
    ### Breadcrumbs
    $options_breadcrumb=array();
    $options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['class']['div']="breadcrumb-large";
    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_CHRONOLOGY;
    $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
    $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
    #end

/*

    # section image
    $html_print_body.="<div class='div-heading-picture' >";
        $html_print_body.="<img src='/g/headers/biography/".rand(1,5).".jpg' alt='' />";
    $html_print_body.="</div>\n";

    # right side block
    $html_print_body.="<div class='div-right-side-info-box'>";
        $html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
        $html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
    $html_print_body.="</div>";
    $html_print_body.="<div class='clearer'></div>";

*/

        # right side block
    /*
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left div-content-header-block-left-full' >";
                $h1_title=LANG_HEADER_MENU_CHRONOLOGY;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/chronology/1.jpg' alt='' width='705px' height='208px' />";
            $html_print_body.="</div>\n";
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        */
        # END right side block

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                $h1_title=LANG_HEADER_MENU_CHRONOLOGY;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/chronology/1.jpg' alt='' width='523' height='208' />";
            $html_print_body.="</div>\n";
            # search block
            $html_print_body.="<div class='div-content-header-block-right'>";
                $html_print_body.="<p class='p-interactive-timeline p-interactive-timeline-".$_GET['lang']."'>".LANG_RIGHT_TIMELINE_INTER."</p>";
                $html_print_body.="<a href='/".$_GET['lang']."/biography/timeline' title='' class='fancybox-html a-timeline-play-button' data-fancybox-width='800' data-fancybox-height='390'></a>";
            $html_print_body.="</div>\n";
            # END search block
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

    $options_admin_edit=array();
    $options_admin_edit=$options_head;
    $html_print_body.=UTILS::admin_edit("/admin/pages/edit/?textid=".$textid,$options_admin_edit)."\n";

    $options_query_text=array();
    $options_query_text['where']=" WHERE textid='".$textid."' ";
    $query_text=QUERIES::query_text($db,$options_query_text);
    $results=$db->query($query_text['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);
    $row=UTILS::html_decode($row);

    $options_row_text=array();
    $options_row_text['search']=$_GET['search'];
    $text=UTILS::row_text_value($db,$row,"text",$options_row_text);

    $options_search_found=array();
    $options_search_found['text']=$text;
    $options_search_found['strip_tags']=1;
    $options_search_found['convert_utf8']=1;
    $options_search_found['search']=$_GET['search'];
    $text=UTILS::show_search_found_keywords($db,$options_search_found);

    $title=UTILS::row_text_value($db,$row,"title",$options_row_text);
    //$html_print_body.="<h3 class='h3-chronology-title'>".$title."</h3><br />";

    $html_print_body.="<div class='div-section-content div-section-content-text'>";
        $options_div_text=array();
        $options_div_text['id']="div-chronology";
        $html_print_body.=$html->div_text_wysiwyg($db,$text,$options_div_text);
    $html_print_body.="</div>";

    $html_print_body.="<a class='top exhibitions-list-back-to-top' href='#top'>".LANG_BACKTOTOP."</a>\n";
}
else
{
    UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>