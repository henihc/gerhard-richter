<?php
$url = $_SERVER['REQUEST_URI'];

if (strpos($url, 'contact')) {
  header("Location: https://www.gerhard-richter.com/");
  die();
}


session_start();

//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html_print_head="";
$html_print_body="<div id='sub-nav'>&nbsp;</div>\n";
$html_print_body.="<div id='div-large-content' role='main' data-class=''>";
$html_print_footer="";

$html = new html_elements;
$html->title=LANG_FOOTER_CONTACT." &raquo; ".LANG_TITLE_HOME;
$html->lang=$_GET['lang'];
$html->css[]="/css/main.css";
$html->css[]="/css/screen.css";
$html->css[]="/css/main.mobile.css";
$html->css[]="/css/contact.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.js";
$html->js[]="/js/validation.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

$options_head=array();
//$options_head['meta_description']=;
$options_head['html_return']=1;
$options_head['class']['body']="body";
$html_print_head.=$html->head($db,'',$options_head);

if( $_SESSION['kiosk'] ) UTILS::redirect("/");

# div main content

if( $_GET['section_0']=="contact" && $_GET['section_1']=="thank-you" )
{
    $options_breadcrumb=array();
    $options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/contact";
    $options_breadcrumb['items'][0]['title']=LANG_CONTACT_SUBNAV_TEXT_ITEM1;
    $options_breadcrumb['items'][0]['inner_html']=LANG_CONTACT_SUBNAV_TEXT_ITEM1;
    $options_breadcrumb['items'][1]['inner_html']=LANG_CONTACT_THANX1;
    $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
    $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
    $html_print_body.="<p class='p-contact-thank-you-text'>";
        $html_print_body.=LANG_CONTACT_THANX2;
    $html_print_body.="</p>\n";
}
elseif( $_GET['section_0']=="contact" && empty($_GET['section_1']) )
{
    $html_print_body.="<script type='text/javascript'>\n";
        $html_print_body.="$(document).ready(function() {";
            $html_print_body.="$('#submit').click(function(){";
                //$html_print_body.="console.debug('click');";
                $html_print_body.="$('#submit').attr('value', 'Form submiting.....');";
                $html_print_body.="$( '#contact-form' ).submit(function( event ) {";
                    $html_print_body.="$('#submit').attr('disabled', true);";
                $html_print_body.="});";
            $html_print_body.="});";
            $html_print_body.="$('#td-image-capcha').click(function(){";
                $html_print_body.="var d = new Date();";
                $html_print_body.="$('#img-capcha-contact').attr('src', $('#img-capcha-contact').attr('src') + '?' + d.getMilliseconds());";
            $html_print_body.="});";
        $html_print_body.="});";
    $html_print_body.="</script>\n";

    ### Breadcrumbs
    $options_breadcrumb=array();
    $options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['items'][0]['inner_html']=LANG_FOOTER_CONTACT;
    $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
    $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);

    # conact form
    $html_print_body.="<p class='brief' style='margin-bottom:7px;'>".LANG_CONTACT_WRITE_TEXT_BRIEF."</p>\n";
    $html_print_body.="<form id='contact-form' method='post' action='/post/post-contact.php' enctype='multipart/form-data'>\n";
        $html_print_body.="<table class='contact-form-table'>\n";
            $html_print_body.="<tr>\n";
                if( $_GET['error1'] ) $style="style='color:red;'";
                else $style="";
                $html_print_body.="<td><label for='email' id='email_text' $style>".LANG_CONTACT_EMAIL.":</label></td>\n";
                $html_print_body.="<td><input type='text' class='input' name='email' id='email' value='".$_SESSION['form_contact']['email']."' /></td>\n";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>\n";
                $html_print_body.="<td><label for='subject'>".LANG_CONTACT_SUBJECT.":</label></td>\n";
                $html_print_body.="<td>\n";
                    $html_print_body.="<input type='text' class='input' name='subject' id='subject' value='".$_SESSION['form_contact']['subject']."' />\n";
                $html_print_body.="</td>\n";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>\n";
                if( $_GET['error2'] ) $style="style='color:red;'";
                else $style="";
                $html_print_body.="<td><label for='message' id='message_text' $style>".LANG_CONTACT_MESSAGE.":</label></td>\n";
                $html_print_body.="<td><textarea name='message' id='message'  rows='6' cols='43' style='width: 335px;'>".$_SESSION['form_contact']['message']."</textarea></td>\n";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>\n";
                $html_print_body.="<td><label for='file_attachment'>".LANG_CONTACT_FILE.":</label></td>\n";
                $html_print_body.="<td>\n";
                    $html_print_body.="<input type='file' class='file' name='file_attachment' id='file_attachment' />";
                $html_print_body.="</td>\n";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>";
                $html_print_body.="<td id='td-image-capcha'>";
                    $html_print_body.="<img src='/includes/image.php' alt='' id='img-capcha-contact' title='Click on image to reload' />\n";
                    $html_print_body.="<img src='/g/icon_reload.png' alt='' id='img-reload' title='Click to reload image code' />\n";
                $html_print_body.="</td>\n";
                $html_print_body.="<td>";
                    if( $_GET['error3'] ) $style="style='color:red;'";
                    else $style="";
                    $html_print_body.="<label for='captcha' id='captcha_text' $style>";
                        $html_print_body.=LANG_CONTACT_CAPCHA;
                    $html_print_body.="</label>";
                $html_print_body.="</td>";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>";
                $html_print_body.="<td>&nbsp;</td>";
                $html_print_body.="<td>";
                    $html_print_body.="<input type='text' name='captcha' id='captcha' value='' maxlength='6' autocomplete='off' />";
                $html_print_body.="</td>";
            $html_print_body.="</tr>\n";
            $html_print_body.="<tr>\n";
            $html_print_body.="<td></td>\n";
            $html_print_body.="<td><input type='submit'  value='".LANG_CONTACT_WRITE_INPUTVALUE_SEND."' name='submit' id='submit' /></td>\n";
            $html_print_body.="</tr>\n";
        $html_print_body.="</table>\n";
        $html_print_body.="<input type='hidden' name='lang' value='".$_GET['lang']."' />";
    $html_print_body.="</form>\n";
    # end
}
else
{
    UTILS::not_found_404();
}


$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$options_foot['selected']=1;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
