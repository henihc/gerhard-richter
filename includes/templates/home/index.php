<?php
session_start();

# javascript
$html_script="";
$html_script.="<script type='text/javascript'>\n";

    $html_script.="$(function() {";

        //$html_script.="console.debug($('html').attr('class'));";
        //$html_script.="$('html').html($('html').attr('class'));";

        /*

        $html_script.="var eles = [];";

        $html_script.="$('#collage').find('img').each(function (e) {";
            $html_script.="eles.push(e);";
            //$html_script.="console.debug(e);";
        $html_script.="});";

        $html_script.="var selected = 0;";

        //$html_script.="console.debug(eles.length);";

        $html_script.="function next_prev_slide(type,selected,eles) {";

            $html_script.="$('#a-painting-' + selected ).removeClass('a-home-links-first');";

            $html_script.=" if( type==1 ) { ";
                $html_script.=" if( selected==0 ) { ";
                    $html_script.=" var next_prev = $('#a-painting-' + (eles.length-1) ) ; ";
                    $html_script.=" selected = eles.length-1; ";
                $html_script.=" } ";
                $html_script.=" else { ";
                    $html_script.=" var next_prev = $('#a-painting-' + (selected-1) ); ";
                    $html_script.=" selected = selected-1; ";
                $html_script.=" } ";
            $html_script.="};";

            $html_script.=" if( type==2 ) { ";
                $html_script.=" if( selected==(eles.length-1) ) { ";
                    $html_script.=" var next_prev = $('#a-painting-0'); ";
                    $html_script.=" selected = 0; ";
                $html_script.=" } ";
                $html_script.=" else { ";
                    $html_script.=" var next_prev = $('#a-painting-' + (selected+1) ); ";
                    $html_script.=" selected = selected+1; ";
                $html_script.=" } ";
            $html_script.="};";

            $html_script.="next_prev.addClass('a-home-links-first');";

            $html_script.="return selected;";
        $html_script.="};";

        $html_script.="$('#collage').on( \"swipeleft\", function( event ) {";
            $html_script.="selected=next_prev_slide(1,selected,eles);";
        $html_script.="});";
        $html_script.="$('#collage').on( \"swiperight\", function( event ) {";
            $html_script.="selected=next_prev_slide(2,selected,eles);";
        $html_script.="});";

        $html_script.="$('.a-home-links-prev').on('click', function(e) {";
            $html_script.="selected=next_prev_slide(1,selected,eles);";
        $html_script.="});";

        $html_script.="$('.a-home-links-next').on('click', function(e) {";
            $html_script.="selected=next_prev_slide(2,selected,eles);";
        $html_script.="});";

        */

    $html_script.="});";
$html_script.="</script>\n";
# END javascript

$html_return="";
$html = new html_elements;
$html->title=LANG_TITLE_HOME;
$html->lang=$_GET['lang'];

# css & js files
$html->css[]="/css/main.min.css";
//if( !$_GET['mobile_device_old'] ) $html->css[]="/css/main.mobile.css";
$html->css[]="/css/main.mobile.min.css";
$html->css[]="/css/home.min.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.min.js";

# jquery mobile
$html->js[]="/js/jquery_mobile/jquery.mobile.custom-1.4.5..min.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.min.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.min.css?v=201509091702";
//if( !$_GET['mobile_device_old'] ) $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.min.js?v=201509091702";

# select boxes
$html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.min.css";
$html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

$options_head=array();
$options_head['meta_description']=LANG_META_DESCRIPTION_HOME;
$options_head['html_return']=1;
$options_head['class']['body']="body";
$options_head['class']['wrapper']="wrapper-home";
$options_head['meta']['viewport']=1;
//$options_head['no_menu']=1;
$html_return.=$html->head($db,'home',$options_head);

$html_return.=$html_script;

$paintings=array();

if( $_GET['mobile_device'] )
{
    $paintings[]=7668;$paintings[]=4735;$paintings[]=10598;$paintings[]=5482;$paintings[]=7873;$paintings[]=5479;$paintings[]=5767;
    $paintings[]=6475;$paintings[]=7983;$paintings[]=5504;
}
else
{
    # LINE 1
    $paintings[]=4735;$paintings[]=10598;$paintings[]=5482;$paintings[]=7873;$paintings[]=5479;$paintings[]=5767;$paintings[]=6475;
    $paintings[]=7983;$paintings[]=5504;$paintings[]=5096;$paintings[]=6851;$paintings[]=7665;$paintings[]=8291;$paintings[]=7970;

    # LINE 2
    $paintings[]=7963;$paintings[]=8270;$paintings[]=6855;$paintings[]=4977;$paintings[]=5518;$paintings[]=7529;$paintings[]=5499;
    $paintings[]=8017;$paintings[]=5882;$paintings[]=5051;$paintings[]=8144;$paintings[]=5476;$paintings[]=7562;$paintings[]=8001;

    # LINE 3
    $paintings[]=5732;$paintings[]=10669;$paintings[]=5615;$paintings[]=5759;$paintings[]=4753;$paintings[]=6537;$paintings[]=4722;
    $paintings[]=5511;$paintings[]=7710;$paintings[]=6172;$paintings[]=5491;$paintings[]=6844;$paintings[]=8054;$paintings[]=4589;

    # LINE 4
    $paintings[]=6698;$paintings[]=5564;$paintings[]=4969;$paintings[]=8122;$paintings[]=10503;$paintings[]=5592;$paintings[]=5751;
    $paintings[]=10420;$paintings[]=5606;$paintings[]=7902;$paintings[]=10558;$paintings[]=4995;$paintings[]=7597;$paintings[]=4696;

    # LINE 5
    $paintings[]=7696;$paintings[]=6620;$paintings[]=5593;$paintings[]=6089;$paintings[]=5146;$paintings[]=6304;$paintings[]=5778;
    $paintings[]=8261;$paintings[]=7668;$paintings[]=4617;$paintings[]=5141;$paintings[]=6838;$paintings[]=5607;$paintings[]=5768;

    # LINE 6
    $paintings[]=4590;$paintings[]=6534;$paintings[]=8248;$paintings[]=7698;$paintings[]=8110;$paintings[]=5774;$paintings[]=13948;
    $paintings[]=4737;$paintings[]=4983;$paintings[]=5674;$paintings[]=8273;$paintings[]=8103;$paintings[]=8086;$paintings[]=5245;

    # LINE 7
    $paintings[]=6309;$paintings[]=5594;$paintings[]=5255;$paintings[]=8030;$paintings[]=6794;$paintings[]=5688;$paintings[]=5037;
    $paintings[]=8135;$paintings[]=7964;$paintings[]=6189;$paintings[]=5563;$paintings[]=8114;$paintings[]=6836;$paintings[]=8118;
}

$html_return.="<div id='intro' role='main'>";
    //$html_return.="<p class='intro1'>".LANG_HOME_TEXT_INTRO1."</p>";

    $textid=5;

    # select info about section
    $values_query_text=array();
    $values_query_text['where']=" WHERE textid='".$textid."' ";
    $query_text=QUERIES::query_text($db,$values_query_text);
    $results=$db->query($query_text['query']);
    $count=$db->numrows($results);
    $row=$db->mysql_array($results);

    $row=UTILS::html_decode($row);
    $values_row_text=array();
    $values_row_text['search']=$_GET['search'];
    $text=UTILS::row_text_value($db,$row,"text",$values_row_text);

    $options_admin_edit=array();
    $options_admin_edit=$options_head;
    $html_return.=UTILS::admin_edit("/admin/pages/edit/?textid=".$textid,$options_admin_edit)."\n";

    $values_search_found=array();
    $values_search_found['text']=$text;
    $values_search_found['strip_tags']=1;
    $values_search_found['convert_utf8']=1;
    $values_search_found['search']=$_GET['search'];
    $text=UTILS::show_search_found_keywords($db,$values_search_found);

    $options_div_text=array();
    $options_div_text['class']['div_text_wysiwyg']="div_text_wysiwyg-intro";
    $html_return.=$html->div_text_wysiwyg($db,$text,$options_div_text);


$html_return.="</div>\n";
$html_return.="<section class='keyvisual'>";

    if( $_GET['mobile_device_old'] ) $class_collage_img="collage-bg-img";

    $html_return.="<div id='collage' class='collage ".$class_collage_img."' data-slideshow-count='10'>";

        if( !$_GET['mobile_device_old'] )
        {
            $i=0;
            $count=count($paintings);
            foreach( $paintings as $paintid )
            {
                $i++;

                $results=$db->query("SELECT paintID,imageid,artworkID,titleEN,titleDE,titleFR,titleIT,titleZH,number,year FROM ".TABLE_PAINTING." WHERE paintID='".$paintid."' LIMIT 1 ");
                $row=$db->mysql_array($results);

                $title=UTILS::row_text_value2($db,$row,"title");
                $values=array();
                $values['artworkid']=$row['artworkID'];
                $values['paintid']=$row['paintID'];
                $painting_url=UTILS::get_painting_url($db,$values);

                $data_info=$row['number'];
                if( !empty($row['number']) && !empty($row['year']) ) $data_info.=", ";
                $data_info.=$row['year'];

                if( !empty($data_info) ) $data_info="CR ".$data_info;

                if( $i==1 )
                {
                    $html_return.="<div class='information'>";
                        $html_return.="<span class='title'>".$title."</span>";
                        $html_return.="<span class='data'>".$data_info."</span>";
                        $html_return.="<button class='prev' ></button>";
                        $html_return.="<button class='next' ></button>";
                    $html_return.="</div>";
                    $html_return.="<div class='container'>";
                }

                //$src="/images/size_xl__imageid_".$row['imageid'].".jpg";
                $src="/datadir/images_new/xlarge/".$row['imageid'].".jpg";
                //$src="/datadir/images_new/medium/".$row['imageid'].".jpg";

                $html_return.="\t<a href='".$painting_url['url']."' data-img='".$src."' data-info='".$data_info."' title='".$title."' class='".$class."' style='".$style."' id='a-painting-".($i-1)."' >";

                    $html_return.="<div class='img'></div>";
                      // $html_return.="<div class='img test' style='background-image: url('".$src."')'></div>";

                $html_return.="\t</a>\n";

            }
        }

        $html_return.="</div>"; //div class='container' end

    $html_return.="\t</div>\n";

$html_return.="\t</section>\n";

$options_menu=array();
$options_menu=$options_head;
$options_menu['no_nav']=1;
$options_menu['id_menu']="main-nav-mobile-home";
$options_menu['class']['main-nav']="disable";
$html_return.=$html->menu($db,$page,$options_menu);


$options_foot=array();
$options_foot=$options_head;
$html_return.=$html->foot($db,$options_foot);
print $html_return;
?>
