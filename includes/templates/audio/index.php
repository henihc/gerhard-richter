<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";
        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="})";
        # END tooltips load
    $html_print_body.="});";
$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

if( !empty($_GET['section_2']) ) UTILS::not_found_404();

# MAIN AUDIO SECTION /audio
if( empty($_GET['section_1']) )
{
	$title_meta="";

	$title_meta.=LANG_HEADER_MENU_AUDIO;

	# title meta
	$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";
	$html->js[]="/js/swfobject.js";
	$html->css[]="/css/audio.css";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# meta-tags & head
	$options_head=array();
	$options_head['meta_description']=LANG_META_DESCRIPTION_AUDIO;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'videos',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
		$options_left_side=array();
		$options_left_side=$options_head;
		$options_left_side['categoryid']=$categoryid;
		$ul_videos=$html->ul_videos($db,2,$options_left_side);
		$html_print_head.=$ul_videos['return'];
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
		$options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_AUDIO;
	    $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
	    $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left div-content-header-block-left-full' >";
                $h1_title=LANG_HEADER_MENU_AUDIO;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/videos/1.jpg' alt='' width='705px' height='208px' />";
            $html_print_body.="</div>\n";
            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block

	    $query_where_audios=" WHERE enable=1 ";
	    $query_order=" ORDER BY sort ASC ";
	    $query_audios=QUERIES::query_audios($db,$query_where_audios,$query_order,$query_limit);
	    $results=$db->query($query_audios['query_without_limit']);

	    while( $row=$db->mysql_array($results) )
	    {
	        $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=11 AND itemid2='".$row['audioid']."' ) OR ( typeid2=17 AND typeid1=11 AND itemid1='".$row['audioid']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
	        $row_related_image=$db->mysql_array($results_related_image);
	        $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
	        $src="/images/imageid_".$imageid.".jpg";
	        //$src="/images/imageid_".$imageid."__maxwidth_100__maxheight_130__thumb_1__border_1.jpg";

	        $title=UTILS::row_text_value($db,$row,"title");
	        $description=UTILS::row_text_value($db,$row,"description");
	        $artwork=UTILS::row_text_value($db,$row,"artwork");
	        $html_print_body.="<div class='audiobox'>\n";
	            $html_print_body.="<div class='div-audio-img'>\n";
	                $html_print_body.="<img src='".$src."' class='audiobox' alt='' />\n";
	            $html_print_body.="</div>\n";
	            $html_print_body.="<div class='div-audio-info'>\n";
	                $html_print_body.="<h2>";
	                    $html_print_body.=$title;
	                    $options_admin_edit=array();
	                    $options_admin_edit=$options_head;
	                    $html_print_body.=UTILS::admin_edit("/admin/audio/edit/?audioid=".$row['audioid'],$options_admin_edit);
	                $html_print_body.="</h2>\n";
	                if( !empty($row['year']) ) $html_print_body.="<p>".$row['year']."</p>\n";
	                if( !empty($row['width']) && !empty($row['height']) ) $html_print_body.="<p>".$row['width']." x ".$row['height']." cm"."</p>\n";
	                if( !empty($artwork) ) $html_print_body.="<p>".$artwork."</p>\n";
	                if( !empty($row['number']) )
	                {
	                	$link_number="/en/art/search?number=".$row['number'];
	                	$link_number=UTILS::replace_lang_for_link($db,$link_number,$_GET['lang']);
	                	$html_print_body.="<p>".LANG_AUDIO_CR." <a href='".$link_number."' title='' target='_blank'><u>".$row['number']."</u></a></p>\n";
	                }
	                $html_print_body.="".$description."\n";

	                $html_print_body.="<div class='audio'>\n";
	                    $browser = new Browser();
	                    if( ($browser->getBrowser() == Browser::BROWSER_IE && $browser->getVersion() >= 9) ) $browser="ie9";
	                    else $browser="";
	                    $browser = new Browser();
	                    if( $browser->getBrowser() == Browser::BROWSER_IPAD ) $class_audio="audio-ipad";
	                    else $class_audio="";
	                    if( $browser!="ie9" )
	                    {
	                        $html_print_body.="<audio controls='controls' class='".$class_audio."'>\n";
	                            //$mp3="/includes/retrieve.audio.php?audioid=".$row['audioid']."&type=mp3";
	                            $mp3="/datadir/audio/mp3/".$row['audioid'].".mp3";
	                            //$ogg="/includes/retrieve.audio.php?audioid=".$row['audioid']."&type=ogg";
	                            $ogg="/datadir/audio/ogg/".$row['audioid'].".ogg";
	                            $html_print_body.="<source src='".$mp3."' type='audio/mp3' />\n";
	                            $html_print_body.="<source src='".$ogg."' type='audio/ogg' />\n";
	                    }
	                            $html_print_body.="<div class='audioerror'>HTML5 audio tag is not supported. Please <a class='audioerror' href='".$mp3."' target='_blank'>download mp3 file</a>.</div>\n";
	                    if( $browser!="ie9" )
	                    {
	                            $html_print_body.="</audio>\n";
	                    }
	                $html_print_body.="</div>\n";
	            $html_print_body.="</div>\n";
	        $html_print_body.="</div>\n";
	        $html_print_body.="<div class='clearer'></div>\n";
	    }


	#END div main content

}
# *** END ***  MAIN VIDEO SECTION /videos

else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
