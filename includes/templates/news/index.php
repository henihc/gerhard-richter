<?php
session_start();

//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html_return="";
$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_body="<div id='div-large-content' role='main' >";
$html_print_footer="";

# javascript
$html_script="";
$html_script.="<script type='text/javascript'>\n";
    $html_script.="var search=0;\n";
    $html_script.="$(function() {";

        # opp form validation scripts
        $html_script.="$('.form-opp-submit-info').on('submit', function(e) {";
            $html_script.="var error=0;";
            $html_script.="var error_codenumb=0;";

            # first name
            $html_script.="if( $('#first_name').val().length == 0  ){";
                //$html_script.="console.log('first_name');";
                $html_script.="error=1;";
                $html_script.="$('#first_name_text').css( 'color', 'red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('#first_name_text').css( 'color', '#666666' );";
            $html_script.="}";

            # last name
            $html_script.="if( $('#last_name').val().length == 0  ){";
                //$html_script.="console.log('last_name');";
                $html_script.="error=1;";
                $html_script.="$('#last_name_text').css( 'color', 'red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('#last_name_text').css( 'color', '#666666' );";
            $html_script.="}";

            # email
            $html_script.="if( $('#email').val().length == 0  ){";
                //$html_script.="console.log('email');";
                $html_script.="error=1;";
                $html_script.="$('#email_text').css( 'color', 'red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('#email_text').css( 'color', '#666666' );";
            $html_script.="}";

            # Credit / courtesy
            $html_script.="if( $('#indicate_credit').val().length == 0  ){";
                //$html_script.="console.log('first_name');";
                $html_script.="error=1;";
                $html_script.="$('#indicate_credit_text').css( 'color', 'red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('#indicate_credit_text').css( 'color', '#666666' );";
            $html_script.="}";

            # codenumb
            $html_script.="if( $('#codenumb').val().length == 0  ){";
                //$html_script.="console.log('codenumb');";
                $html_script.="error=1;";
                $html_script.="error_codenumb=1;";
                $html_script.="$('#codenumb_error').css( 'color', 'red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('#codenumb_error').css( 'color', '#666666' );";
            $html_script.="}";

            # agree
            $html_script.="if( !$('#agree').is(':checked') ){";
                //$html_script.="console.log('agree');";
                $html_script.="error=1;";
                $html_script.="$('.td-agree-left').css( 'border-left', '1px solid red' );";
                $html_script.="$('.td-agree-left').css( 'border-top', '1px solid red' );";
                $html_script.="$('.td-agree-left').css( 'border-bottom', '1px solid red' );";
                $html_script.="$('.td-agree-right').css( 'border-right', '1px solid red' );";
                $html_script.="$('.td-agree-right').css( 'border-top', '1px solid red' );";
                $html_script.="$('.td-agree-right').css( 'border-bottom', '1px solid red' );";
            $html_script.="}";
            $html_script.="else{";
                $html_script.="$('.td-agree-left').css( 'border-left', '1px solid #666' );";
                $html_script.="$('.td-agree-left').css( 'border-top', '1px solid #666' );";
                $html_script.="$('.td-agree-left').css( 'border-bottom', '1px solid #666' );";
                $html_script.="$('.td-agree-right').css( 'border-right', '1px solid #666' );";
                $html_script.="$('.td-agree-right').css( 'border-top', '1px solid #666' );";
                $html_script.="$('.td-agree-right').css( 'border-bottom', '1px solid #666' );";
            $html_script.="}";

            # submiting form
            # Get some values from elements on the page:
            $html_script.="var form = $( '.form-opp-submit-info' ),";
            $html_script.="url = form.attr( 'action' );";


            $html_script.="if( error ) {";
                $html_script.="e.preventDefault();";
                $html_script.="alert('".LANG_OPP_FORM_75_2."');";
            $html_script.="}";

            # else allow to submit form
            $html_script.="else {";
                //$html_script.="console.log('allowing to submit form');";
            $html_script.="}";

            # sending email to admin with error in subject some fields where left empty but capcha was ok so possibly human
            $html_script.="if( error && !error_codenumb ) {";
                # Send the data using post
                $html_script.="var posting = $.post( url, $('.form-opp-submit-info').serialize())";
            $html_script.="}";

        $html_script.="});";

        # for capcha reload button
        $html_script.="$('#td-image-capcha').click(function(){";
            $html_script.="var d = new Date();";
            $html_script.="$('#img-capcha-opp').attr('src', $('#img-capcha-opp').attr('src') + '?' + d.getMilliseconds());";
        $html_script.="});";

        # load calendar picker
        $html_script.="$( '#date, #date_of_acquisition' ).datepicker(";
            $html_script.="{";
                $html_script.="changeMonth: true,";
                $html_script.="changeYear: true,";
                $html_script.="showButtonPanel: true,";
                $html_script.="dateFormat: 'dd/mm/yy',";
                $html_script.="yearRange: '1986:'+(new Date().getFullYear())";
            $html_script.="}";
        $html_script.=");";
        # END opp form validation scripts

    $html_script.="});";
$html_script.="</script>\n";
# END javascript

if( !empty($_GET['section_3']) )
{
    UTILS::not_found_404();
}

# news main section /news
elseif( empty($_GET['section_1']) || $_GET['section_1']=="upcoming" || $_GET['section_1']=="talks" || $_GET['section_1']=="auctions" || ( $_GET['section_1']=="auctions" && $_GET['section_2']=="results" ) || $_GET['section_1']=="publications" )
{
    $title_meta="";

    if( $_GET['section_1']=="upcoming" ) $title_meta.=LANG_NEWS_UPCOMING." &raquo; ";
    elseif( $_GET['section_1']=="talks" ) $title_meta.=LANG_NEWS_TALKS." &raquo; ";
    elseif( $_GET['section_1']=="auctions" )
    {
        if( $_GET['section_2']=="results" ) $title_meta.=LANG_NEWS_RESULTS." &raquo; ";
        $title_meta.=LANG_NEWS_AUCTIONS." &raquo; ";
    }
    elseif( $_GET['section_1']=="publications" ) $title_meta.=LANG_NEWS_PUBLICATIONS." &raquo; ";

    $title_meta.=LANG_HEADER_MENU_NEWS;

    # title meta
    $html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.css";
    $html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
    $html->css[]="/css/news.css";
    $html->css[]="/css/tabs.css";
    $html->css[]="/css/exhibitions.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

    # fancybox
    $html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
    $html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
    $html->js[]="/js/jquery_fancybox/fancybox.load.js";

    # opp form css
    $html->css[]="/css/opp-form.css";

    #expand table
    $html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";
    $html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.js";

    # tootltips, tabs, date picker - style jquery ui
    $html->css[]="/js/jquery_ui/css/jquery-ui-1.10.4.custom.css";

    $html_print_body.=$html_script;

    # meta-tags & head
    $options_head=array();
    if( $_GET['section_1']=="upcoming" )
    {
        //$options_head['meta_description']=LANG_META_DESCRIPTION_NEWS;
    }
    else $options_head['meta_description']=LANG_META_DESCRIPTION_NEWS;
    $options_head['html_return']=1;
    $options_head['class']['body']="body";
    $html_print_head.=$html->head($db,'news',$options_head);

    # left side blocks
    $html_print_head.="<div id='sub-nav'>";
        $options_left_side=array();
        $options_left_side=$options_head;
        if( $_GET['section_1']=="talks" ) $options_left_side['selectedid']=2;
        elseif( $_GET['section_1']=="auctions" ) $options_left_side['selectedid']=3;
        elseif( $_GET['section_1']=="publications" ) $options_left_side['selectedid']=4;
        else $options_left_side['selectedid']=1;
        $html_print_head.=$html->ul_news($db,$options_left_side);
    $html_print_head.="</div>";

    # div main content

        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/news";
        $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_NEWS;
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_NEWS;
        if( empty($_GET['section_1']) || $_GET['section_1']=="upcoming" )
        {
            $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/news";
            $options_breadcrumb['items'][1]['title']=LANG_HEADER_MENU_EXHIBITIONS;
            $options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_EXHIBITIONS;
        }
        if( $_GET['section_1']=="upcoming" )
        {
            $options_breadcrumb['items'][2]['inner_html']=LANG_NEWS_UPCOMING;
        }
        elseif( $_GET['section_1']=="talks" )
        {
            $options_breadcrumb['items'][2]['inner_html']=LANG_NEWS_TALKS;
        }
        elseif( $_GET['section_1']=="auctions" )
        {
            $options_breadcrumb['items'][2]['inner_html']=LANG_NEWS_AUCTIONS;
        }
        elseif( $_GET['section_1']=="publications" )
        {
            $options_breadcrumb['items'][2]['inner_html']=LANG_NEWS_PUBLICATIONS;
        }
        else $options_breadcrumb['items'][2]['inner_html']=LANG_NEWS_CURRENT;

        if( !empty($_GET['section_1']) )
        {
            $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
            $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
        }
        else
        {
            $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
            $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
        }
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

/*
        # section image
        $html_print_body.="<div class='div-heading-picture' >";
            $html_print_body.="<img src='/g/headers/news/".rand(1,5).".jpg' alt='' />";
        $html_print_body.="</div>\n";

        # right side block
        $html_print_body.="<div class='div-right-side-info-box div-right-side-info-box-news-publications'>";
            $options_social=array();
            $options_social=$options_head;
            //$options_social['sliding_title']=1;
            $options_social['class']['twitter']="twitter";
            $options_social['class']['facebook']="facebook";
            $options_social['class']['span-follow-us']="span-social-box-follow";
            $html_print_body.=$html->social_buttons($options_social);
        $html_print_body.="</div>";
        $html_print_body.="<div class='clearer'></div>";

*/

        # right side block
        $html_print_body.="<div class='div-content-header-block' >";
            # section image
            $html_print_body.="<div class='div-content-header-block-left' >";
                if( !empty($title_category_sub) ) $h1_title=$title_category_sub;
                elseif( !empty($title_category) ) $h1_title=$title_category;
                else $h1_title=LANG_HEADER_MENU_NEWS;
                $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
                $html_print_body.="<img src='/g/headers/news/1.jpg' alt='' width='523' height='208' />";
            $html_print_body.="</div>\n";

            # social block
            $html_print_body.="<div class='div-content-header-block-right'>";
                $options_social=array();
                $options_social=$options_head;
                //$options_social['sliding_title']=1;
                $options_social['class']['twitter']="twitter";
                $options_social['class']['facebook']="facebook";
                $options_social['class']['span-follow-us']="span-social-box-follow";
                $options_social['class']['a-twitter']="a-twitter-news";
                $options_social['class']['a-facebook']="a-facebook-news";
                $html_print_body.=$html->social_buttons($options_social);
            $html_print_body.="</div>";

            $html_print_body.="<div class='clearer'></div>";
        $html_print_body.="</div>\n";
        # END right side block
/*
        # opp form
        $options_opp_brief=array();
        $options_opp_brief=$options_head;
        $options_opp_brief['large_content']=1;
        $options_opp_brief['class']['div-brief-right-large']="div-brief-right-large";
        $html_print_body.=$html->div_opp_brief($db,$options_opp_brief);
        # END opp form
*/
        $html_print_body.="<h2 class='h2-section-title'>";
            if( $_GET['section_1']=="upcoming" )
            {
                $html_print_body.=LANG_NEWS_EXHIBITIONS;
            }
            elseif( $_GET['section_1']=="talks" )
            {
                $html_print_body.=LANG_NEWS_TALKS;
            }
            elseif( $_GET['section_1']=="auctions" )
            {
                $html_print_body.=LANG_NEWS_AUCTIONS;
            }
            elseif( $_GET['section_1']=="publications" )
            {
                $html_print_body.=LANG_NEWS_NEW_PUBLICATIONS;
            }
            else $html_print_body.=LANG_NEWS_EXHIBITIONS;
        $html_print_body.="</h2>";

        if( $_GET['section_1']=="talks" )
        {
            $html_print_body.="<div id='div-news-publications'>\n";

                $query_where_news=" WHERE type=3 AND NOW()<=dateto ";
                $query_order_news=" ORDER BY datefrom ASC, dateto ASC ";
                $query_news=QUERIES::query_news($db,$query_where_news,$query_order_news);
                $results=$db->query($query_news['query']);
                $count=$db->numrows($results);

                $class="page-numb-news-top";
                //$html->page_numbering_pages($db,$pages,$_GET['p'],$_GET['sp'],"",$class,1);
                //$html_print_body.="<div class='clearer' ></div>\n";

                if( $count>0 )
                {
                    //$html_print_body.="<p class='p-new-publications-count'>".LANG_NEWS_PUBLICATIONS_COUNT1." ".$count." ".LANG_NEWS_PUBLICATIONS_COUNT2."</p>";
                    $i=0;
                    while( $row=$db->mysql_array($results) )
                    {
                        $i++;
                        $row=UTILS::html_decode($row);

                        # title
                        $title=UTILS::row_text_value2($db,$row,"title");
                        $values_search_found=array();
                        $values_search_found['search']=$values['search'];
                        $values_search_found['text']=$title;
                        $values_search_found['a_name']=1;
                        $title=UTILS::show_search_found_keywords($db,$values_search_found);
                        # location
                        $values_location=array();
                        $values_location['row']=$row;
                        $location=location($db,$values_location);
                        $values_search_found=array();
                        $values_search_found['search']=$values['search'];
                        $values_search_found['text']=$location;
                        $values_search_found['class']['span-keyword-found2']="span-keyword-found2";
                        $values_search_found['a_name']=1;
                        $location=UTILS::show_search_found_keywords($db,$values_search_found);
                        # text
                        $text=UTILS::row_text_value2($db,$row,"text");
                        /*
                        $values_search_found=array();
                        $values_search_found['search']=$values['search'];
                        $values_search_found['text']=$text;
                        $values_search_found['class']['span-keyword-found2']="span-keyword-found2";
                        $values_search_found['strip_tags']=$values['strip_tags'];
                        $values_search_found['a_name']=1;
                        $text=UTILS::show_search_found_keywords($db,$values_search_found);
                         */


                        if( $i==1 ) $class_first="div-news-publication-first";
                        else $class_first="";
                        $html_print_body.="<div class='div-news-publication ".$class_first."'>\n";
                            $html_print_body.="<div class='div-news-publication-item'>\n";

                            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=7 AND itemid2='".$row['newsID']."' ) OR ( typeid2=17 AND typeid1=7 AND itemid1='".$row['newsID']."' ) ORDER BY sort ASC, relationid DESC");
                            $count_images=$db->numrows($results_related_image);

                            $html_print_body.="<a name='".$row['newsID']."' ></a>\n";
                            if( !$count_images ) $class_large="div-news-publication-large";
                            else $class_large="";
                            $html_print_body.="<div class='div-news-publication-left ".$class_large."' >\n";
                                # title
                                $html_print_body.="<p class='p-news-publication-title'>";
                                    $html_print_body.=$row['title_original'];
                                    # admin edit link
                                    $html_print_body.="&nbsp";
                                    $options_admin_edit=array();
                                    $options_admin_edit=$options_head;
                                    $html_print_body.=UTILS::admin_edit("/admin/news/edit/?newsid=".$row['newsID'],$options_admin_edit);
                                    # END
                                $html_print_body.="</p>\n";
                                $html_print_body.="<table cellspacing='1' cellpadding='1' class='table-news-publication-values'>";

                                    if( !empty($location) )
                                    {
                                        $html_print_body.="<tr>\n";
                                            $html_print_body.="<td>";
                                                $html_print_body.=$location;
                                            $html_print_body.="</td>\n";
                                        $html_print_body.="</tr>\n";
                                    }

                                    if( !empty($row['person']) )
                                    {
                                        $html_print_body.="<tr>\n";
                                            $html_print_body.="<td>";
                                                $html_print_body.=$row['person'];
                                            $html_print_body.="</td>\n";
                                        $html_print_body.="</tr>\n";
                                    }

                                    $html_print_body.="<tr>";
                                        $html_print_body.="<td >";
                                            $ndatefrom=UTILS::row_text_value($db,$row,"ndatefrom");
                                            $ndateto=UTILS::row_text_value($db,$row,"ndateto");
                                            $ndatefrom_display=UTILS::date_month($db,$ndatefrom);
                                            $ndateto_display=UTILS::date_month($db,$ndateto);
                                            if( $row['ndatefrom']==$row['ndateto'] )
                                            {
                                                $date=$ndatefrom_display."&nbsp;&nbsp;&nbsp;".$row['time'];
                                            }
                                            else
                                            {
                                                $date=$ndatefrom_display;
                                                $date.=" - ";
                                                $date.=$ndateto_display;
                                            }
                                            if( (!empty($row['ndatefrom']) || !empty($row['ndateto'])) && $row['showdate'] )
                                            {
                                                $html_print_body.=$date;
                                            }
                                        $html_print_body.="</td>\n";
                                    $html_print_body.="</tr>";

                                    if( !empty($text) )
                                    {
                                        $html_print_body.="<tr><td><br />";
                                            $options_div_text=array();
                                            $html_print_body.=$html->div_text_wysiwyg($db,$text,$options_div_text);
                                        $html_print_body.="</td></tr>\n";
                                    }

                                $html_print_body.="</table>";

                            $html_print_body.="</div>\n";

                            # book image
                            if( $count_images>0 )
                            {
                                $html_print_body.="<div class='div-news-publication-right' >\n";
                                    $images_array=array();
                                    while( $row_related_image=$db->mysql_array($results_related_image) )
                                    {
                                        $images_array[]=$row_related_image;
                                    }
                                    $imageid=UTILS::get_relation_id($db,"17", $images_array[0]);
                                    //$href="/images/size_xl__imageid_".$imageid.".jpg";
                                    $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                                    $html_print_body.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='".$row['newsID']."'>\n";
                                        $html_print_body.="<img class='table_item_attrib' src='/images/size_l__imageid_".$imageid.".jpg' alt=''>\n";
                                    $html_print_body.="</a>\n";
                                    for($i=1;$i<count($images_array);$i++)
                                    {
                                        $imageid=UTILS::get_relation_id($db,"17",$images_array[$i]);
                                        //$href="/images/size_xl__imageid_".$imageid.".jpg";
                                        $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                                        $html_print_body.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='".$row['newsID']."'>\n";
                                            $html_print_body.="<img style='display:none;' src='' alt=''>\n";
                                        $html_print_body.="</a>\n";
                                    }
                                $html_print_body.="</div>\n";
                            }
                            $html_print_body.="<div class='clearer'></div>\n";
                            $html_print_body.="</div>\n";
                        $html_print_body.="</div>\n";
                    }
                }
                else
                {
                    $html_print_body.="<p class='p-new-publications-count'>".LANG_NEWS_NO_TALKS."</p>";
                }
            $html_print_body.="</div>\n";
        }
        elseif( $_GET['section_1']=="auctions" )
        {
            $html_print_body.="<ul class='ul-tabs'>\n";
                if( empty($_GET['section_2']) ) $class_selected="selected";
                else $class_selected="";
                $html_print_body.="<li><a href='/".$_GET['lang']."/news/auctions' class='".$class_selected."'>".LANG_NEWS_UPCOMING."</a></li>\n";
                if( $_GET['section_2']=="results" ) $class_selected="selected";
                else $class_selected="";
                $html_print_body.="<li class='last'><a href='/".$_GET['lang']."/news/auctions/results' class='".$class_selected."'>".LANG_NEWS_RESULTS."</a></li>\n";
            $html_print_body.="</ul>\n";
            $html_print_body.="<div class='clearer'></div>";

            if( empty($_GET['section_2']) || $_GET['section_2']=="results" )
            {
                if( empty($_GET['sort']) )
                {
                    $_GET['sort']=5;
                    if( $_GET['section_2']=="results" )
                    {
                        $_GET['sort_how']=-1;
                    }
                    else
                    {
                        $_GET['sort_how']=1;
                    }
                }
                else
                {
                    $tmp=explode(",", $_GET['sort']);
                    $_GET['sort']=$tmp[0];
                    if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
                    else $_GET['sort_how']=1;
                }

                $options_table = array(
                    'options'       =>  array(
                        // Setup table options here...
                        'id'                => 'expandlist_literature_01',  // set DOM node id for js (uniqid() if not set)
                        'sort_by'   => array($_GET['sort'], $_GET['sort_how']),                         // default column sorted
                        'expandable'        => false,                       // Allow tr tags to expand (expand-boxes required)
                        'highlight'         =>  array (                     // highlight patterns in content
                                                //  'Auth',
                                                //  'Adriani'
                                                ),
                        'html_class'         =>  $html,
                        'html_return'         =>  $options_head['html_return'],
                        'onheaderclick'     => $_SERVER['PATH_INFO'] . '?sort='
                    )
                );

                if( $_GET['section_2']=="results" )
                {
                    $options_table['cols']=array(
                            // Define columns here...
                            array( 'caption' => '',                 'width' => '52px',   'col' => '0',   'sortable' => false, 'sort_col' => 'p.saleDate',    'content_align' => 'center'),
                            array( 'caption' => LANG_ARTWORK_BOOKS_REL_ARTWORK,          'width' => '163px',     'col' => '1',   'sortable' => true,  'sort_col' => 'p.titleDE',   'content_align' => 'left' ),
                            array( 'caption' => LANG_NEWS_AUCTIONS_AUCTIONHOUSE,    'width' => '110px',   'col' => '2',   'sortable' => true,  'sort_col' => 'ah.house',   'content_align' => 'left' ),
                            array( 'caption' => LANG_ESTIMATE,      'width' => '22%',    'col' => '3',   'sortable' => true, 'sort_col' => 's.estLowUSD',    'content_align' => 'left' ),
                            array( 'caption' => LANG_SOLDPR,        'width' => '*',   'col' => '4',   'sortable' => true, 'sort_col' => 's.soldForUSD',    'content_align' => 'left' ),
                            array( 'caption' => LANG_NEWS_AUCTIONS_DATE,             'width' => '9%',   'col' => '5',   'sortable' => true, 'sort_col' => 's.saleDate',    'content_align' => 'left' )
                        );
                }
                else
                {
                    $options_table['cols']=array(
                            // Define columns here...
                            array( 'caption' => '',                 'width' => '52px',   'col' => '0',   'sortable' => false, 'sort_col' => 's.saleDate',    'content_align' => 'center'),
                            array( 'caption' => LANG_ARTWORK_BOOKS_REL_ARTWORK,          'width' => '168px',     'col' => '1',   'sortable' => true, 'sort_col' => 'p.titleDE',     'content_align' => 'left' ),
                            array( 'caption' => LANG_NEWS_AUCTIONS_AUCTIONHOUSE,    'width' => '110px',   'col' => '2',   'sortable' => true, 'sort_col' => 'ah.house',     'content_align' => 'left' ),
                            array( 'caption' => LANG_LOT,              'width' => '8%',    'col' => '3',   'sortable' => true, 'sort_col' => 's.lotNo',     'content_align' => 'left' ),
                            array( 'caption' => LANG_ESTIMATE,         'width' => '*',   'col' => '4',   'sortable' => true, 'sort_col' => 's.estLowUSD',     'content_align' => 'left' ),
                            array( 'caption' => LANG_NEWS_AUCTIONS_DATE,             'width' => '9%',   'col' => '5',   'sortable' => true, 'sort_col' => 's.saleDate',     'content_align' => 'left' )
                        );
                }

                $options_table_data=array();
                $options_table_data['html_return']=$options_head['html_return'];
                $options_table_data['cols']=$options_table['cols'];
                $options_table_data['html_class']=$options_table['options']['html_class'];
                $options_table_data['sort']=$options_table['options']['sort_by'];
                $table_data = new Table_Data($db,$options_table_data);
                $options_table_data=array();
                if( $_GET['section_2']=="results" ) $options_table_data['results']=1;
                $rows=$table_data->table_data_auctions($options_table_data);

                //print "<p class='p-section-title'>";
                $html_print_body.="<div class='div-tabs-headline'>";
                    if( $rows['count'] )
                    {
                        if( $_GET['section_2']=="results" ) $html_print_body.=LANG_NEWS_REACEN_AUCTION_RES_DESC;
                        else $html_print_body.=LANG_NEWS_UPCOMING_FOR_AUCTION_DESC;
                    }
                    else $html_print_body.=LANG_NEWS_NO_AUCTIONS;
                $html_print_body.="</div>";


                if( $rows['count'] )
                {
                    //print_r($rows);
                    $table = new Table($db, $rows, $options_table);
                    $html_print_body.=$table->draw_table();

                    $html_print_body.="<p class='p-news-dislaimer-title'>".LANG_NEWS_DISCLIMER."</p>";

                    $values_query_text=array();
                    $values_query_text['where']=" WHERE textid=2 ";
                    $query_text=QUERIES::query_text($db,$values_query_text);
                    $results=$db->query($query_text['query']);
                    $count=$db->numrows($results);
                    $row=$db->mysql_array($results);
                    $row=UTILS::html_decode($row);
                    $text=UTILS::row_text_value($db,$row,"text");

                    $options_admin_edit=array();
                    $options_admin_edit=$options_head;
                    $html_print_body.=UTILS::admin_edit("/admin/pages/edit/?textid=2",$options_admin_edit);
                    $options_div_text=array();
                    $options_div_text['class']['div_text_wysiwyg']="div-news-dislaimer";
                    $html_print_body.=$html->div_text_wysiwyg($db,$text,$options_div_text);
                }
                else
                {
                    //$html_print_body.="<p class='p-no-news'>".LANG_NEWS_NOCURRENT."</p>";
                }
            }
            else
            {
                UTILS::not_found_404();
            }
        }
        elseif( $_GET['section_1']=="publications" )
        {
            $html_print_body.="<div id='div-news-publications'>\n";

                $query_where_books=" WHERE newsid=1 AND enable=1 ";
                $query_order_books=" ORDER BY sort_publications ASC, id DESC";
                $query_books=QUERIES::query_books($db,$query_where_books,$query_order_books);
                $results=$db->query($query_books['query']);
                $count=$db->numrows($results);

                if( $count>0 )
                {
                    //$html_print_body.="<p class='p-new-publications-count'>".LANG_NEWS_PUBLICATIONS_COUNT1." ".$count." ".LANG_NEWS_PUBLICATIONS_COUNT2."</p>";
                    $i=0;
                    while( $row=$db->mysql_array($results) )
                    {
                        $i++;
                        $row=UTILS::html_decode($row);

                        if( $i==1 ) $class_first="div-news-publication-first";
                        else $class_first="";
                        $html_print_body.="<div class='div-news-publication ".$class_first."'>\n";
                            $html_print_body.="<div class='div-news-publication-item'>\n";

                            $results_related_image=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row['id']."' ) ORDER BY sort ASC, relationid DESC");
                            $count_images=$db->numrows($results_related_image);

                            $html_print_body.="<a name='".$row['id']."' ></a>\n";
                            if( !$count_images ) $class_large="div-news-publication-large";
                            else $class_large="";
                            $html_print_body.="<div class='div-news-publication-left ".$class_large."' >\n";
                                # title
                                $html_print_body.="<p class='p-news-publication-title'>";
                                    $html_print_body.=$row['title'];
                                    # admin edit link
                                    $html_print_body.="&nbsp";
                                    $options_admin_edit=array();
                                    $options_admin_edit=$options_head;
                                    $html_print_body.=UTILS::admin_edit("/admin/books/edit/?bookid=".$row['id'],$options_admin_edit);
                                    # END
                                $html_print_body.="</p>\n";
                                $html_print_body.="<table cellspacing='1' cellpadding='1' class='table-news-publication-values'>";
                                    $html_print_body.="<tr>";
                                        $html_print_body.="<td>".LANG_BOOKS_BY."</td>";
                                        $html_print_body.="<td>".$row['author']."</td>";
                                    $html_print_body.="</tr>";
                                    $html_print_body.="<tr>";
                                        $html_print_body.="<td>".LANG_LIT_SEARCH_PUBBY."</td>";
                                        $html_print_body.="<td>";
                                            if( !empty($row['publisher']) ) $html_print_body.=$row['publisher'];
                                            else $html_print_body.=LANG_LIT_SEARCH_UNKNOWN;
                                            if( !empty($row['date_display_year']) ) $html_print_body.=" (".$row['date_display_year'].")";
                                        $html_print_body.="</td>";
                                    $html_print_body.="</tr>";
                                    if( !empty($row['coverid']) || $row['NOpages']!=0 )
                                    {
                                        $html_print_body.="<tr>";
                                            $html_print_body.="<td>".LANG_BOOKS_DETAILS."</td>";
                                            $html_print_body.="<td>";
                                                if( !empty($row['coverid']) )
                                                {
                                                    $i_cover=0;
                                                    $coverids=explode(",", $row['coverid']);
                                                    $cover="";
                                                    foreach( $coverids AS $coverid )
                                                    {
                                                        $values_cover=array();
                                                        $values_cover['coverid']=$coverid;
                                                        if( $i_cover>0 ) $cover.=", ";
                                                        $cover.=UTILS::get_cook_cover($db,$values_cover);
                                                        $i_cover++;
                                                    }
                                                    $html_print_body.=$cover;
                                                }
                                                if( $row['NOpages']!=0 )
                                                {
                                                    if( !empty($row['coverid']) ) $html_print_body.=", ";
                                                    $html_print_body.=$row['NOpages']." ".LANG_LIT_SEARCH_PAGES;
                                                }
                                            $html_print_body.="</td>";
                                        $html_print_body.="</tr>";
                                    }

                                    # ISBN LANGUAGES
                                    $results_isbn=$db->query("SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$row['id']."' ORDER BY id ASC");
                                    $count_isbn=$db->numrows($results_isbn);
                                    if( $count_isbn>0 )
                                    {
                                        $html_isbn="";
                                        $html_language="";
                                        $i=0;
                                        while( $row_isbn=$db->mysql_array($results_isbn) )
                                        {
                                            $i++;
                                            if( $row_isbn['isbn']!="" )
                                            {
                                                $html_isbn.="\t<tr>\n";
                                                    $html_isbn.="\t<td>ISBN</td>\n";
                                                    $html_isbn.="\t<td>".$row_isbn['isbn']."</td>\n";
                                                $html_isbn.="\t</tr>\n";
                                            }
                                            if( !empty($row_isbn['language']) || !empty($row_isbn['language2']) || !empty($row_isbn['language3']) )
                                            {
                                                $query_where_language=" WHERE languageid='".$row_isbn['language']."' ";
                                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                                $results_language=$db->query($query_language['query']);
                                                $row_language=$db->mysql_array($results_language);
                                                $row_language=UTILS::html_decode($row_language);
                                                $title_language=UTILS::row_text_value($db,$row_language,"title");
                                                $query_where_language=" WHERE languageid='".$row_isbn['language2']."' ";
                                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                                $results_language=$db->query($query_language['query']);
                                                $row_language=$db->mysql_array($results_language);
                                                $row_language=UTILS::html_decode($row_language);
                                                $title_language2=UTILS::row_text_value($db,$row_language,"title");
                                                $query_where_language=" WHERE languageid='".$row_isbn['language3']."' ";
                                                $query_language=QUERIES::query_books_languages($db,$query_where_language);
                                                $results_language=$db->query($query_language['query']);
                                                $row_language=$db->mysql_array($results_language);
                                                $row_language=UTILS::html_decode($row_language);
                                                $title_language3=UTILS::row_text_value($db,$row_language,"title");
                                                $count3=0;
                                                if( !empty($row_isbn['language']) ) $count3++;
                                                if( !empty($row_isbn['language2']) ) $count3++;
                                                if( !empty($row_isbn['language3']) ) $count3++;

                                                if( $count3==1 ) $language=LANG_BOOKS_LANGUAGE;
                                                else $language=LANG_BOOKS_LANGUAGES;
                                                if( $i==1 ) $html_language.="\t<tr>\n";
                                                    if( $i==1 ) $html_language.="\t<td>".$language."</td>\n";
                                                    if( $i==1 ) $html_language.="\t<td>\n";
                                                        if( !empty($row_isbn['language']) )
                                                        {
                                                            if( !empty($html_language) && $i!=1 ) $html_language.=", ";
                                                            $html_language.=$title_language;
                                                        }
                                                        if( !empty($row_isbn['language2']) )
                                                        {
                                                            $html_language.=", ";
                                                            $html_language.=$title_language2;
                                                        }
                                                        if( !empty($row_isbn['language3']) )
                                                        {
                                                            $html_language.=", ";
                                                            $html_language.=$title_language3;
                                                        }
                                            }
                                        }
                                        if( !empty($html_language) )
                                        {
                                                $html_language.="\t</td>\n";
                                            $html_language.="\t</tr>\n";
                                        }
                                        $html_print_body.=$html_isbn;
                                        $html_print_body.=$html_language;
                                    }
                                $html_print_body.="</table>";

                                # more button
                                $infosmall=UTILS::row_text_value($db,$row,"infosmall");
                                //if( !empty($infosmall) )
                                //{
                                    $options_book_info=array();
                                    $options_book_info['bookid']=$row['id'];
                                    $book_info=UTILS::get_literature_url($db,$options_book_info);
                                    $html_print_body.="<a href='".$book_info['url']."' class='a-news-publications-more'>".ucwords(LANG_BOOKS_MORE)."</a>";
                                //}
                            $html_print_body.="</div>\n";

                            # book image
                            if( $count_images>0 )
                            {
                                $html_print_body.="<div class='div-news-publication-right' >\n";
                                    $images_array=array();
                                    while( $row_related_image=$db->mysql_array($results_related_image) )
                                    {
                                        $images_array[]=$row_related_image;
                                    }
                                    $imageid=UTILS::get_relation_id($db,"17", $images_array[0]);
                                    //$href="/images/size_xl__imageid_".$imageid.".jpg";
                                    $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                                    $html_print_body.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='".$row['id']."'>\n";
                                        $src="/images/size_l__imageid_".$imageid.".jpg";
                                        $src=DATA_PATH_DATADIR."/images_new/large/".$imageid.".jpg";
                                        $html_print_body.="<img class='table_item_attrib' src='".$src."' alt=''>\n";
                                    $html_print_body.="</a>\n";
                                    for($i=1;$i<count($images_array);$i++)
                                    {
                                        $imageid=UTILS::get_relation_id($db,"17",$images_array[$i]);
                                        //$href="/images/size_xl__imageid_".$imageid.".jpg";
                                        $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                                        $html_print_body.="<a href='".$href."' class='fancybox-buttons' data-fancybox-group='".$row['id']."'>\n";
                                            $html_print_body.="<img style='display:none;' src='' alt=''>\n";
                                        $html_print_body.="</a>\n";
                                    }
                                $html_print_body.="</div>\n";
                            }
                            $html_print_body.="<div class='clearer'></div>\n";
                            $html_print_body.="</div>\n";
                        $html_print_body.="</div>\n";
                    }
                }
                else
                {
                    //$html_print_body.="<p class='p-no-news'>".LANG_NEWS_NOCURRENT."</p>";
                    $html_print_body.="<p class='p-new-publications-count'>".LANG_NEWS_NOCURRENT."</p>";
                }
            $html_print_body.="</div>\n";
        }
        else
        {
            $html_print_body.="<div id='tabs' class='div-tabs-news-exh'>\n";
                $html_print_body.="<ul class='ul-tabs'>\n";
                    if( empty($_GET['section_1']) )
                    {
                        $class_selected="selected";
                    }
                    else $class_selected="";
                    $html_print_body.="<li class='li-exh-years-tab'><a href='/".$_GET['lang']."/news' class='".$class_selected."' title='".LANG_NEWS_CURRENT."'>".LANG_NEWS_CURRENT."</a></li>\n";
                    ### counting upcoming
                    $query_where_upcoming=" WHERE NOW()<date_from and NOW()<date_to ";
                    $query_upcoming=QUERIES::query_exhibition($db,$query_where_upcoming);
                    $results_upcoming=$db->query($query_upcoming['query_without_limit']);
                    $count_upcoming=$db->numrows($results_upcoming);
                    #end
                    if( $_GET['section_1']=="upcoming" )
                    {
                        $class_selected="selected";
                    }
                    else $class_selected="";
                    if( $count_upcoming>0 ) $html_print_body.="<li><a href='/".$_GET['lang']."/news/upcoming' class='last ".$class_selected."' title='".LANG_NEWS_UPCOMING."'>".LANG_NEWS_UPCOMING."</a></li>\n";
                    //print_r($row_up);
                    //$html_print_body.="<li><a href='/news/results' title='".LANG_NEWS_RESULTS."'>".LANG_NEWS_RESULTS."</a></li>\n";
                $html_print_body.="</ul>\n";
                $html_print_body.="<div class='clearer'></div>";
            $html_print_body.="</div>";

            ### main query to select news info
            if( $_GET['section_1']=="upcoming" ) $query_where_exhibitions=" WHERE NOW()<date_from AND NOW()<date_to ";
            else $query_where_exhibitions=" WHERE NOW()>=date_from AND NOW()<=date_to ";
            $query_where_exhibitions_solo=$query_where_exhibitions." AND e.typeid=1 AND e.enable=1 ";
            $query_where_exhibitions_group=$query_where_exhibitions." AND e.typeid=2 AND e.enable=1 ";
            $query_order_news=" ORDER BY date_from ASC, date_to ASC ";
            $query_news_solo=QUERIES::query_exhibition($db,$query_where_exhibitions_solo,$query_order_news);
            $query_news_group=QUERIES::query_exhibition($db,$query_where_exhibitions_group,$query_order_news);
            $results_news_solo_no_limit=$db->query($query_news_solo['query_without_limit']);
            $results_news_group_no_limit=$db->query($query_news_group['query_without_limit']);
            $count_solo=$db->numrows($results_news_solo_no_limit);
            $count_group=$db->numrows($results_news_group_no_limit);
            $count=$count_solo+$count_group;
            $pages=@ceil($count/$_GET['sp']);
            $results_news_solo=$db->query($query_news_solo['query']);
            $results_news_group=$db->query($query_news_group['query']);
            #end

            if( $count_solo>0 )
            {
                if( $count_group>0 ) $style="border-bottom:1px solid #999999;";
                else $style="";
                $html_print_body.=" <div class='div-exhibitions' style='".$style."'>\n";
                    $html_print_body.="<div class='div-exhibitions-head-news' >\n";
                        $text=LANG_SOLO_EXHS." (".$count_solo.")";
                        $html_print_body.="<h3>".$text."</h3>\n";
                    $html_print_body.="</div>\n";
                    $html_print_body.="<div class='clearer'></div>\n";

                    while( $row=$db->mysql_array($results_news_solo) )
                    {
                        $options_related_exh=array();
                        $options_related_exh=$options_head;
                        $options_related_exh['row']=$row;
                        $options_related_exh['count']=$count_solo;
                        $options_related_exh['class']['a-exhibition-list-paint-detail']="a-exhibition-list-paint-detail";
                        $options_related_exh['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                        $options_related_exh['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                        $options_related_exh['class']['span-location-length']="span-exh-list-location-wide";
                        $options_related_exh['class']['span-date-length']="span-exh-list-date-wide";
                        $html_print_body.=$html->div_exhibition($db,$options_related_exh);
                    }
                $html_print_body.="</div>\n";
            }

            if( $count_group>0 )
            {
                $html_print_body.="<div class='clearer' ></div>\n";
                $html_print_body.=" <div class='div-exhibitions' style='border-top: none;margin-top:1px;'>\n";
                    $html_print_body.="<div class='div-exhibitions-head-news' >\n";
                        $text=LANG_GROUP_EXHS." (".$count_group.")";
                        $html_print_body.="<h3>".$text."</h3>\n";
                    $html_print_body.="</div>\n";
                    $html_print_body.="<div class='clearer'></div>\n";

                    while( $row=$db->mysql_array($results_news_group) )
                    {
                        $options_related_exh=array();
                        $options_related_exh=$options_head;
                        $options_related_exh['row']=$row;
                        $options_related_exh['count']=$count_group;
                        $options_related_exh['class']['a-exhibition-list-paint-detail']="a-exhibition-list-paint-detail";
                        $options_related_exh['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                        $options_related_exh['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                        $options_related_exh['class']['span-location-length']="span-exh-list-location-wide";
                        $options_related_exh['class']['span-date-length']="span-exh-list-date-wide";
                        $html_print_body.=$html->div_exhibition($db,$options_related_exh);
                    }
                $html_print_body.="</div>\n";
            }

            if( $count_solo<=0 && $count_group<=0 )
            {
                $html_print_body.="<p class='nonews'>".LANG_NEWS_NOCURRENT."</p>";
            }
        }
}
# END news main section /news

else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
