<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//if( $_SESSION['kiosk'] ) UTILS::redirect('/');

//$db=new dbCLASS;

$html_return="";
$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$options_head=array();
$options_head['html_return']=1;

$class=array();
$html_print_head="";

/*
$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";
        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="})";
        # END tooltips load
    $html_print_body.="});";
$html_print_body.="</script>\n";
*/



$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";

        # tooltips load
        $html_print_body.="$( document ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-tooltips-info-content').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="});";
        # END tooltips load

        # select box pretty
        $html_print_body.="$('.select-field, .select_pp').selectBoxIt({";
            //$html_print_body.="showFirstOption: false";
        $html_print_body.="});";
        //$html_print_body.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
        //$html_print_body.="selectBox.selectOption(0);";

        # show more less search options
        $html_print_body.="$.fn.toggleText = function(t1, t2){
          if (this.text() == t1) this.text(t2);
          else                   this.text(t1);
          return this;
        };";
        $html_print_body.="$( '.a-right-search-show-more-options' ).click(function() {";
            $html_print_body.="$( '.div-content-header-block-right' ).toggleClass( 'div-content-header-block-right-large' );";
            $html_print_body.="$( this ).toggleClass( 'a-right-search-show-more-options-less' );";
            $html_print_body.="$( this ).toggleText(\"".LANG_LITERATURE_SHOW_LESS_OPTIONS."\", \"".LANG_LITERATURE_SHOW_MORE_OPTIONS."\");";
            $html_print_body.="$( '.div-content-header-block-right-more-options' ).toggleClass( 'show' );";
        $html_print_body.="});";

    $html_print_body.="});";
$html_print_body.="</script>\n";

# right side block
$html_right_ride_articles="";
$html_right_ride_articles.="<div class='div-content-header-block' >";
    # section image
    $html_right_ride_articles.="<div class='div-content-header-block-left' >";
        if( empty($_GET['section_1']) ) $h1_title=LANG_HEADER_MENU_CONTACT;
        else $h1_title=LANG_LINKS_ARTICLES;
        $html_right_ride_articles.="<h1 class='h1-section'>".$h1_title."</h1>";
        $html_right_ride_articles.="<img src='/g/headers/links/1.jpg' alt='' width='523' height='208' />";
    $html_right_ride_articles.="</div>\n";

    /*
    # search block
    $options_search_block=array();
    $options_search_block['html_return']=1;
    $options_search_block['html']=$html;
    $options_search_block['class']['div-search-block']="div-content-header-block-right";
    $options_search_block['h1-title']=LANG_HEADER_MENU_SEARCH;
    $options_search_block['more-options']="hidden";
    $options_search_block['url_form_action']="/".$_GET['lang']."/links/articles/search/";
    $options_search_block['fields'][0]['type']="text";
    $options_search_block['fields'][0]['name']="author";
    $options_search_block['fields'][0]['id']="author_articles";
    $options_search_block['fields'][0]['placeholder']=LANG_RIGHT_SEARCH_LIT_AUTHOR;
    $options_search_block['fields'][0]['class']="input-text-field";
    $options_search_block['fields'][1]['type']="text";
    $options_search_block['fields'][1]['name']="title";
    $options_search_block['fields'][1]['id']="title_articles";
    $options_search_block['fields'][1]['placeholder']=LANG_RIGHT_SEARCH_LIT_TITLE;
    $options_search_block['fields'][1]['class']="input-text-field";
    $options_search_block['fields'][2]['type']="text";
    $options_search_block['fields'][2]['name']="keyword";
    $options_search_block['fields'][2]['id']="keyword_articles";
    $options_search_block['fields'][2]['placeholder']=LANG_RIGHT_SEARCH_LIT_KEYWORD;
    $options_search_block['fields'][2]['class']="input-text-field";
    $options_search_block['fields'][3]['type']="select";
    $options_search_block['fields'][3]['name']="articles_catid";
    $options_search_block['fields'][3]['id']="articles_catid_articles";
    $options_search_block['fields'][3]['class']="input-text-field";
    $options_search_block['fields'][4]['type']="select";
    $options_search_block['fields'][4]['name']="languageid";
    $options_search_block['fields'][4]['id']="languageid_articles";
    $options_search_block['fields'][4]['class']="input-text-field";
    $options_search_block['fields'][5]['type']="select-year-from-to";
    $html_right_ride_articles.=$html->div_search_block($db,$options_search_block);
    # END search block
    */

    # search block
    $options_search_block=array();
    $options_search_block['options_head']=$options_head;
    $html_right_ride_articles.=$html->search_form_links($db,$options_search_block);
    # END search block

    $html_right_ride_articles.="<div class='clearer'></div>";
$html_right_ride_articles.="</div>\n";
# END right side block

$html_print_body.="<div id='div-large-content' role='main' >";
$html_print_footer="";

if( !empty($_GET['section_3']) )
{        
    UTILS::not_found_404();
}

# links main section /links
elseif( empty($_GET['section_1']) )
{
    $title_meta.=LANG_HEADER_MENU_CONTACT;

    # title meta
    $html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.css";
    $html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
    $html->css[]="/css/painting-detail-view.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

    # tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

    # meta-tags & head
    $options_head['meta_description']=LANG_META_DESCRIPTION_LINKS;
    $options_head['class']['body']="body";
    $html_print_head.=$html->head($db,'contact',$options_head);

    # left side blocks
    $html_print_head.="<div id='sub-nav'>";
        $options_left_side=array();
        $options_left_side=$options_head;
        $html_print_head.=$html->ul_links($db,$options_left_side);
    $html_print_head.="</div>";

    # div main content

        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_CONTACT;
        $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
        $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

        $html_print_body.=$html_right_ride_articles;

    $html_print_body.="<div class='div-section-content div-category-landing-page-content'>";

        # print sub category thumbs

        $categories=array();
        $categories[0]['links_categoryid']=1;
        $categories[0]['title']=LANG_LINKS_ARTICLES;
        $categories[0]['src']="/g/links/articles_new.jpg";
        $categories[0]['href']="/".$_GET['lang']."/links/articles";
        /*
        $categories[1]['links_categoryid']=2;
        $categories[1]['title']=LANG_LINKS_DEALERS;
        $categories[1]['src']="/g/links/dealers_new.jpg";
        $categories[1]['href']="/".$_GET['lang']."/links/dealers";
        */

        $options_cat_thumbs=array();
        $options_cat_thumbs=$options_head;
        $options_cat_thumbs['categories']=$categories;
        $options_cat_thumbs['count_categories']=count($categories);
        //$options_cat_thumbs['class']['div-section-categories']="div-section-categories-quotes";
        $options_cat_thumbs['url_admin']="";
        $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
        $html_print_body.=$return_cat_thumbs['html_print'];
        # END print sub category thumbs

    $html_print_body.="</div>\n";


}
# END links main section /links

# artciles page
elseif( $_GET['section_1']=="articles" )
{
    # MAIN articles SECTION /links/articles
	if( $_GET['section_2']=="search" )
	{
		$title_meta=LANG_HEADER_MENU_SEARCH." &raquo; ";
	}
	
    # articles category
    if( !empty($_GET['section_2']) && $_GET['section_2']!="search" )
    {        
        $query_category_where=" WHERE titleurl='".$_GET['section_2']."' ";
        $query_category=QUERIES::query_articles_categories ($db,$query_category_where);
        $results_category=$db->query($query_category['query']);
        $row_category=$db->mysql_array($results_category);
        $count_category=$db->numrows($results_category);
        $title_category=UTILS::row_text_value($db,$row_category,"title");
        $categoryid=$row_category['articles_catid'];
        $title_meta.=$title_category." &raquo; ";
        if( !$count_category ) UTILS::not_found_404();
    }

	$title_meta.=LANG_LINKS_ARTICLES;

	# title meta
	$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
	$html->css[]="/css/tabs.css";
	$html->css[]="/css/tabs_exh_decades.css";
    $html->css[]="/css/articles.css";
    $html->css[]="/css/painting-detail-view.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # select boxes
    $html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
    $html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=LANG_META_DESCRIPTION_EXHIBITIONS;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'contact',$options_head);

    if( empty($_GET['section_2']) && empty($_GET['year']) && empty($_GET['decade']) && !isset($_GET['author']) && !isset($_GET['title']) && !isset($_GET['keyword']) && !isset($_GET['languageid']) && !isset($_GET['categoryid']) && !isset($_GET['year-from']) && !isset($_GET['year-to']) )
    {
        $_GET['year']=date("Y");
        //$_GET['year']=2014;
    }

    if( empty($_GET['section_2']) && !isset($_GET['author']) && !isset($_GET['title']) && !isset($_GET['keyword']) && !isset($_GET['languageid']) && !isset($_GET['articles_catid']) )
    {
        if( empty($_GET['year']) && isset($_GET['year-from']) ) $_GET['year']=$_GET['year-from'];
    }

	# left side blocks
    $html_print_head.="<div id='sub-nav'>";
    	$options_ul_exh=array();
    	$options_ul_exh=$options_head;
    	$options_ul_exh['section_2']=$_GET['section_2'];
        $options_ul_exh['selectedid']=1;
    	$html_print_head.=$html->ul_links($db,$options_ul_exh);
    $html_print_head.="</div>";
	# END left side blocks

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/links";
        $options_breadcrumb['items'][0]['title']=LANG_LINKS;
        $options_breadcrumb['items'][0]['inner_html']=LANG_LINKS;
        $options_breadcrumb['items'][1]['inner_html']=LANG_LINKS_ARTICLES;
	    if( $_GET['section_2']=="search" )
	    {
            $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/links/articles";
            $options_breadcrumb['items'][1]['title']=LANG_LINKS_ARTICLES;
	        $options_breadcrumb['items'][2]['inner_html']=LANG_HEADER_MENU_SEARCH;
	    }
        $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
        $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end  

/*
    	# section image
	    $html_print_body.="<div class='trp heading-picture' style='background:#b8b8b8 url(/g/headers/literature/".rand(1,5).".jpg) no-repeat 2px 2px;'></div>";
	    # END section image
*/

        # right side block
        $html_print_body.=$html_right_ride_articles;
        # END right side block

	    # Main content
        $search=0;

        $query_where_articles_decades="
                    WHERE 
                        link_date IS NOT NULL 
                        AND link IS NOT NULL 
                        AND link!='' 
                        AND (books_catid=6 OR books_catid=12 OR books_catid=13 OR books_catid=19) 
                        AND enable=1 
                    ";
        if( !empty($categoryid) ) $query_where_articles_decades.=" AND articles_catid='".$categoryid."' ";
        elseif( $_GET['section_2']=="search" )
        {
            $search=1;

            # SEARCH
            if( !empty($_GET['author']) || !empty($_GET['title']) || !empty($_GET['keyword']) || !empty($_GET['year-from']) || !empty($_GET['year-to']) || !empty($year_from) || !empty($year_to) || !empty($_GET['languageid']) || !empty($_GET['articles_catid']) )
            {   
                $url_search="";

                # author
                if( !empty($_GET['author']) ) 
                {   
                    # prepare search keyword
                    $values_search=array();
                    $values_search['db']=$db;
                    $prepear_search=UTILS::prepear_search($_GET['author'],$values_search);
                    # END prepare search keyword

                    # prepare match
                    $values_search_literature_author=array();
                    $values_search_literature_author['search']=$prepear_search;
                    $query_match_literature_author=QUERIES_SEARCH::query_search_literature_author($db,$values_search_literature_author);
                    $query_where_articles_decades.=" AND ".$query_match_literature_author;
                    # END prepare match

                    $author_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['author']));
                    $url_search.="&author=".$author_for_url;
                }
                # end author

                # title
                if( !empty($_GET['title']) )
                {
                    # prepare search keyword
                    $values_search=array();
                    $values_search['db']=$db;
                    $prepear_search=UTILS::prepear_search($_GET['title'],$values_search);
                    # END prepare search keyword

                    # prepare match
                    $values_search_literature_title=array();
                    $values_search_literature_title['search']=$prepear_search;
                    $query_match_literature_title=QUERIES_SEARCH::query_search_articles_title($db,$values_search_literature_title);
                    $query_where_articles_decades.=$and." AND ".$query_match_literature_title;
                    # END prepare match

                    $title_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['title']));
                    $url_search.="&amp;title=".$title_for_url;
                }
                # end title

                # keyword
                if( !empty($_GET['keyword']) )
                {
                    $values_search=array();
                    $values_search['db']=$db;
                    $prepear_search=UTILS::prepear_search($_GET['keyword'],$values_search);
                    # prepare match
                    $values_search_lit=array();
                    $values_search_lit['search']=$prepear_search;
                    $query_match_lit=queries_search::query_search_literature($db,$values_search_lit);
                    $query_where_articles_decades.=$and." AND ".$query_match_lit;
                    # end prepare match

                    $keyword_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['keyword']));
                    $url_search.="&amp;keyword=".$keyword_for_url;
                }
                # end keyword

                # articles_catid
                if( !empty($_GET['articles_catid']) )
                {
                    $query_where_articles_decades.=$and." AND b.articles_catid='".$_GET['articles_catid']."' ";
                    $url_search.="&amp;articles_catid=".$_GET['articles_catid'];
                }
                # end articles_catid

                # year
                $and=" AND ";
                $query_where_articles_year="";
                if( !empty($_GET['year-from']) && !empty($_GET['year-to']) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y') BETWEEN '".$_GET['year-from']."' AND '".$_GET['year-to']."' ";
                    $url_search.="&amp;year-from=".$_GET['year-from']."&amp;year-to=".$_GET['year-to'];
                }
                if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')>='".$_GET['year-from']."' ";
                    $url_search.="&amp;year-from=".$_GET['year-from'];
                }
                if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')<='".$_GET['year-to']."' ";
                    $url_search.="&amp;year-to=".$_GET['year-to'];
                }
                /*
                if( !empty($_GET['year-from']) && empty($_GET['year-to']) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')='".$_GET['year-from']."' ";
                    $url_search.="&amp;year-from=".$_GET['year-from'];
                }
                if( empty($_GET['year-from']) && !empty($_GET['year-to']) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')='".$_GET['year-to']."' ";
                    $url_search.="&amp;year-to=".$_GET['year-to'];
                }
                */
                if( !empty($year_from) && !empty($year_to) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y') BETWEEN '".$year_from."' AND '".$year_to."' ";
                }
                /*
                if( !empty($year_from) && empty($year_to) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')>='".$year_from."' ";
                }
                if( empty($year_from) && !empty($year_to) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')<='".$year_to."' ";
                }
                */
                if( !empty($year_from) && empty($year_to) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')='".$year_from."' ";
                }
                if( empty($year_from) && !empty($year_to) )
                {
                    $query_where_articles_year.=$and." DATE_FORMAT(b.link_date, '%Y')='".$year_to."' ";
                }
                $query_where_articles_decades.=$query_where_articles_year;
                # end year

                # languageid
                if( !empty($_GET['languageid']) )
                {
                    $query_where_articles_decades.=" AND b.id=bi.bookID AND ( bi.language='".$_GET['languageid']."' OR bi.language2='".$_GET['languageid']."' OR bi.language3='".$_GET['languageid']."' ) ";
                    $url_search.="&amp;languageid=".$_GET['languageid'];
                }
                # end languageid

            }

            # end SEARCH
        }

        if( !empty($_GET['languageid']) )
        {
            $query_tables=", ".TABLE_BOOKS_ISBN." bi ";
        }
        $query_order=" ORDER BY b.link_date ASC ";
        $options_articles=array(); //!!
        $options_articles['distinct_id']=1; //!!
        $query_articles_decades=QUERIES::query_books($db,$query_where_articles_decades,$query_order,$query_limit,$query_tables,$options_articles);
        $results_years=$db->query($query_articles_decades['query_without_limit']);
        if( $_SESSION['debug_page'] ) $html_print_body.=$query_articles_decades['query_without_limit'];
        $count_years=$db->numrows($results_years);

        if( $count_years>0 )
        {
            $i_books=0;
            $articles=array();
            while( $row_years=$db->mysql_array($results_years) )
            {
                $articles[$row_years['date_year']][$i_books]=$row_years;
                $i_books++;
                //print_r($row_years);
            }
            //print_r($articles);


            # keyword - searching for linked exhibitions to article
            if( !empty($_GET['keyword']) )
            {
                # prepare search keyword
                $values_search=array();
                $values_search['db']=$db;
                $prepear_search=UTILS::prepear_search($_GET['keyword'],$values_search);
                # END prepare search keyword

                # prepare match
                $values_search_exh=array();
                $values_search_exh['search']=$prepear_search;
                $query_match_exh=QUERIES_SEARCH::query_search_exhibitions($db,$values_search_exh);
                $query_search_exh=$query_match_exh;
                # END prepare match


                $query_exh="SELECT exID FROM ".TABLE_EXHIBITIONS." e WHERE ".$query_search_exh;
                $results_exh=$db->query($query_exh);
                $count_exh=$db->numrows($results_exh);
                if( $count_exh>0 )
                {
                    $i=0;
                    while( $row_exh=$db->mysql_array($results_exh) )
                    {
                        $query_where_exh_links=" WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exh['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exh['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 AND (b.books_catid=6 OR b.books_catid=12 OR b.books_catid=13 OR b.books_catid=19) ".$query_where_articles_year;
                        if( !empty($articles_catid) ) $query_where_exh_links.=" AND articles_catid='".$articles_catid."' ";
                        $query_tables_exh_links=" , ".TABLE_RELATIONS." r ";
                        $query_exh_links=QUERIES::query_books($db,$query_where_exh_links,"","",$query_tables_exh_links);
                        $results_exh_links=$db->query($query_exh_links['query']);
                        $count_exh_links=$db->numrows($results_exh_links);
                        if( $count_exh_links )
                        {
                            while( $row_exh_links=$db->mysql_array($results_exh_links) )
                            {
                                $row_exh_links=UTILS::html_decode($row_exh_links);

                                # adding to found books to array
                                if( !empty($row_exh_links['date_year']) )
                                {
                                    $articles[$row_exh_links['date_year']][$i_books]=$row_exh_links;
                                    $i_books++;
                                }
                                //print "bookid-<a href='/admin/books/edit/?bookid=".$row_exh_links['id']."'>".$row_exh_links['id']."</a> year-".$row_exh_links['date_year']."<br />";
                            }
                        }
                    }
                }
            }
            # end keyword

            # calculate decades
            foreach( $articles as $key => $year )
            {    
                $yr3=substr($key,0,3);
                $min=$yr3.'0';
                $decades[$min][]=$key;
            }    
            $count_decades=count($decades);
            # END calculate decades

            # decades tabs
            if( empty($_GET['decade']) ) $_GET['decade']=end(@array_keys($decades));
            if( empty($_GET['year']) ) $_GET['year']=end($decades[$_GET['decade']]);

            foreach( $articles as $key => $year )
            {
                foreach( $year as $row_article )
                {
                    if( $key==$_GET['year'] )
                    {
                        $count_articles++;
                        $results_articles[]=$row_article;
                    }
                    else
                    {
                        //print $row_article['id']."-";
                    }
                }
            }
            unset($articles);
        }

        if( $count_years>0 )
        {
            $ul_decades_html="";
            $ul_decades_html.="\t<ul class='ul-exh-decades-tabs ul-tabs'>\n";
                $selected="";
                $i=0;
                foreach( $decades as $decade => $year )
                {
                    //print $decade."-";
                    //print $year."-";
                    $i++;
                    if( $i==1 && empty($_GET['decade']) ) $_GET['decade']=$decade;
                    $ul_decades_html.="\t<li class='li-exh-decades-tab'>\n";
                        # selected
                        if( $decade==$_GET['decade'] ) $selected="selected";
                        else $selected="";
                        # END selected
                        # last
                        if( $i==count($decades) ) $last="last";
                        else $last="";
                        # END last
                        $url="/".$_GET['lang']."/links/articles";
                        if( !empty($_GET['section_2']) ) $url.="/".$_GET['section_2'];
                        $url.="/?decade=".$decade.$url_search;
                        $ul_decades_html.="\t<a href=\"".$url."\" class='".$selected." ".$last."' title='".$decade."'>".$decade.LANG_EXH_DECADES_S."</a>\n";
                    $ul_decades_html.="\t</li>\n";
                }
            $ul_decades_html.="\t</ul>\n";

            if( $count_decades>1 ) $html_print_body.=$ul_decades_html;
            # END decades tabs

            # decades tabs data
            $html_print_body.="<div class='clearer'></div>\n";
            if( $count_decades>1 ) $html_print_body.="<div class='div-exhibitions-decades'>\n";


            # year tabs
            $ul_years_html="";
            $ul_years_html.="\t<ul class='ul-exh-years-tabs ul-tabs'>\n";
                $selected="";
                $i=0;
                if( is_array($decades[$_GET['decade']]) )
                {
                    foreach( $decades[$_GET['decade']] as $key => $year )
                    {
                        //print $key."-";
                        //print $year."-";
                        $i++;
                        //if( $i==1 && empty($_GET['year']) ) $_GET['year']=end($decades[$_GET['decade']]);
                        $ul_years_html.="\t<li class='li-exh-years-tab'>\n";
                            # selected
                            if( $year==$_GET['year'] ) $selected="selected";
                            else $selected="";
                            # END selected
                            # last
                            if( $i==count($decades[$_GET['decade']]) ) $last="last";
                            else $last="";
                            # END last
                            $url="/".$_GET['lang']."/links/articles";
                            if( !empty($_GET['section_2']) ) $url.="/".$_GET['section_2'];
                            $url.="/?decade=".$_GET['decade']."&amp;year=".$year.$url_search;
                            $ul_years_html.="\t<a href=\"".$url."\" class='".$selected." ".$last."' title='".$year."'>".$year."</a>\n";
                        $ul_years_html.="\t</li>\n";
                    }
                }
            $ul_years_html.="\t</ul>\n";
            # END year tabs

            $html_print_body.=$ul_years_html;
            $html_print_body.="<div class='clearer'></div>\n";
            $html_print_body.="<div class='div-articles'>\n";

                if( !$search )
                {
                    $query_where_articles=$query_where_articles_decades." AND DATE_FORMAT(link_date, '%Y')='".$_GET['year']."' ";
                    //if( !empty($articles_catid) ) $query_where_articles.=" AND articles_catid='".$articles_catid."' ";
                    $query_order=" ORDER BY link_date DESC ";
                    $query_articles=QUERIES::query_books($db,$query_where_articles,$query_order,$query_limit);
                    $results=$db->query($query_articles['query_without_limit']);
                    if( $_SESSION['debug_page'] ) $html_print_body.=$query_articles['query_without_limit'];
                    $count_articles=$db->numrows($results);
                    $results_articles=$db->query($query_articles['query']);
                }

                $html_print_body.="<div class='div-articles-head' >\n";
                    $text=LANG_LINKS_ARTICLES." (".$count_articles.")";
                    $html_print_body.="<h3>".$text."</h3>\n";
                $html_print_body.="</div>\n";
                $html_print_body.="<div class='clearer'></div>\n";

                $options_articles=array();
                $options_articles=$options_head;
                $options_articles['results']=$results_articles;
                $html_print_body.=$html->div_articles($db,$options_articles);

            $html_print_body.="</div>\n";

            if( $count_decades>1 ) $html_print_body.="</div>\n";
            # END decades tabs data

        }
        else $html_print_body.=LANG_SEARCH_NORESULTS;

	   #END div main content

}
# END artciles page

# dealers page
/*
elseif ( $_GET['section_1']=="dealers" ) 
{
    $title_meta.=LANG_LINKS_DEALERS;

    # title meta
    $html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
    $html->lang=$_GET['lang'];

    # css & js files
    $html->css[]="/css/main.css";
    $html->css[]="/css/screen.css";
    $html->css[]="/css/main.mobile.css";
    $html->css[]="/css/links.css";
    $html->css[]="/css/painting-detail-view.css";
    $html->js[]="/js/jquery/jquery-2.1.4.min.js";
    $html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
    $html->js[]="/js/common.js";

    # mobile menu
    $html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
    $html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
    $html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

    # tabs and tooltips
    //$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

    # meta-tags & head
    $options_head=array();
    //$options_head['meta_description']=LANG_META_DESCRIPTION_LINKS;
    $options_head['html_return']=1;
    $options_head['class']['body']="body";
    $html_print_head.=$html->head($db,'contact',$options_head);

    # left side blocks
    $html_print_head.="<div id='sub-nav'>";
        $options_left_side=array();
        $options_left_side=$options_head;
        $options_left_side['selectedid']=2;
        $html_print_head.=$html->ul_links($db,$options_left_side);
    $html_print_head.="</div>";

    # div main content

        ### Breadcrubmles
        $options_breadcrumb=array();
        $options_breadcrumb=$options_head;
        $options_breadcrumb['class']['ul']="ul-breadcrumbles";
        //$options_breadcrumb['class']['div']="breadcrumb-large";
        $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_CONTACT;
        $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/links";
        $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_CONTACT;
        $options_breadcrumb['items'][1]['inner_html']=LANG_LINKS_DEALERS;
        $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
        $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
        $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
        #end

        $html_print_body.="<h2 class='h2-dealers'>".LANG_CONTACT_SUBNAV_TEXT_ITEM2."</h2>\n";

        $options_query_text=array();
        $options_query_text['where']=" WHERE textid=4 ";
        $query_text=QUERIES::query_text($db,$options_query_text);
        $results=$db->query($query_text['query']);
        $count=$db->numrows($results);
        $row=$db->mysql_array($results);
        $row=UTILS::html_decode($row);
        $options_row_text=array();
        $options_row_text['search']=$_GET['search'];
        $text=UTILS::row_text_value($db,$row,"text",$options_row_text);

        $options_search_found=array();
        $options_search_found['text']=$text;
        $options_search_found['strip_tags']=1;
        $options_search_found['search']=$_GET['search'];
        $text=UTILS::show_search_found_keywords($db,$options_search_found);

        $options_admin_edit=array();
        $options_admin_edit=$options_head;
        $html_print_body.=UTILS::admin_edit("/admin/pages/edit/?textid=4",$options_admin_edit);
        $options_div_text=array();
        $html_print_body.=$html->div_text_wysiwyg($db,$text,$options_div_text);

}
# END dealers page
*/
else
{
	UTILS::not_found_404();
}

$html_print_body.="</div>"; # END - <div id='content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
