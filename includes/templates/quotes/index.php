<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body="<script type='text/javascript'>\n";
    $html_print_body.="var search=0;\n";
    $html_print_body.="$(function() {";
        # tooltips load
        $html_print_body.="$( '.help' ).tooltip({";
            $html_print_body.="items: '[data-title]',";
            $html_print_body.="content: $('.div-search-help').html(),";
            $html_print_body.="tooltipClass: 'div-tooltips-search-help'";
        $html_print_body.="});";
        # END tooltips load
        # tooltips quote source text and search ? load
        $html_print_body.="$( '.div-quote' ).tooltip({";
            $html_print_body.="items: '[data-title], [data-source]',";
            $html_print_body.="position: {";
                $html_print_body.="my: 'left+60 top'";
            $html_print_body.="},";
            $html_print_body.="content: function() {";
                $html_print_body.="var element = $( this );";
                $html_print_body.="if ( element.is( '[data-source]' ) ) {";
                    $html_print_body.="var element_data = '.div-tooltips-info-content-quotes-'+element.attr( 'data-source' );";
                    $html_print_body.="return $(element_data).html()";
                $html_print_body.="}";
            $html_print_body.="},";
            $html_print_body.="tooltipClass: 'div-tooltips-quotes-source',";
            //$html_print_body.="hide: {duration: 1000000 }"; //for debuging
        $html_print_body.="});";
        # END tooltips load
        # select box pretty
        $html_print_body.="$('.select-field, .select_pp').selectBoxIt({";
            //$html_print_body.="showFirstOption: false";
        $html_print_body.="});";
        //$html_print_body.="var selectBox = $('.select-field').data('selectBox-selectBoxIt');";
        //$html_print_body.="selectBox.selectOption(0);";
    $html_print_body.="});";
$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

# MAIN QUOTES SECTION /quotes
$title_meta="";
if( $_GET['section_1']=="search" )
{
	$title_meta.=LANG_HEADER_MENU_SEARCH." &raquo; ";
	$search=1;
}
elseif( !empty($_GET['section_1']) )
{
	# quotes category
	if( !empty($_GET['section_2']) )
	{
		$categoryid=UTILS::itemid($db,$_GET['section_1']);
		$categoryid_sub=UTILS::itemid($db,$_GET['section_2']);
		$categoryid_query=$categoryid_sub;

	    //$query_category_sub_where=" WHERE enable=1 AND quotes_categoryid='".$categoryid_query."' ";
        $query_category_sub_where=" WHERE enable=1 AND titleurl='".$_GET['section_2']."' ";
	    $query_category_sub=QUERIES::query_quotes_categories($db,$query_category_sub_where);
	    $results_category_sub=$db->query($query_category_sub['query']);
	    $row_category_sub=$db->mysql_array($results_category_sub);
	    $count_category_sub=$db->numrows($results_category_sub);
	    $title_category_sub=UTILS::row_text_value($db,$row_category_sub,"title");
	    //$categoryid_sub=$row_category_sub['quotes_categoryid'];
	    $title_meta.=$title_category_sub." &raquo; ";
	    if( !$count_category_sub ) UTILS::not_found_404();
	}

	# quotes category
	if( !empty($_GET['section_1']) )
	{
		$categoryid=UTILS::itemid($db,$_GET['section_1']);
		if( empty($_GET['section_2']) ) $categoryid_query=$categoryid;

	    //$query_category_where=" WHERE enable=1 AND quotes_categoryid='".$categoryid."' ";
        $query_category_where=" WHERE enable=1 AND titleurl='".$_GET['section_1']."' ";
	    $query_category=QUERIES::query_quotes_categories($db,$query_category_where);
	    $results_category=$db->query($query_category['query']);
	    $row_category=$db->mysql_array($results_category);
	    $count_category=$db->numrows($results_category);
	    $title_category=UTILS::row_text_value($db,$row_category,"title");
	    //$categoryid=$row_category['quotes_categoryid'];
	    if( !$count_category_sub ) $title_meta.=$title_category." &raquo; ";
	    if( !$count_category ) UTILS::not_found_404();
	}

	if( !empty($_GET['section_3']) ) UTILS::not_found_404();
}
# main quotes section
else
{
    $main_section=1;
}

$title_meta.=LANG_HEADER_MENU_QUOTES;

# title meta
$html->title=$title_meta." &raquo; ".LANG_TITLE_HOME;
$html->lang=$_GET['lang'];

# css & js files
$html->css[]="/css/main.css";
$html->css[]="/css/screen.css";
$html->css[]="/css/main.mobile.css";
$html->css[]="/css/quotes.css";
$html->css[]="/css/painting-detail-view.css";
$html->js[]="/js/jquery/jquery-2.1.4.min.js";
$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
$html->js[]="/js/common.js";

# mobile menu
$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

# tabs and tooltips
$html->css[]="/js/jquery_ui/css/jquery-ui.structure-1.11.4.css";

# select boxes
$html->css[]="/js/jquery_selectbox/jquery.selectBoxIt.css";
$html->js[]="/js/jquery_selectbox/jquery.selectBoxIt.min.js";

# meta-tags & head
$options_head=array();
if( $main_section ) $options_head['meta_description']=LANG_META_DESCRIPTION_QUOTES;
$options_head['html_return']=1;
$options_head['class']['body']="body";
$html_print_head.=$html->head($db,'quotes',$options_head);

# left side blocks
$html_print_head.="<div id='sub-nav'>";
	$options_left_side=array();
	$options_left_side=$options_head;
	$options_left_side['categoryid']=$categoryid;
	$options_left_side['categoryid_sub']=$categoryid_sub;
	$ul_quotes=$html->ul_quotes($db,3,$options_left_side);
    $html_print_head.=$ul_quotes['return'];
$html_print_head.="</div>";

# div main content

    ### Breadcrubmles
    $options_breadcrumb=array();
   	$options_breadcrumb=$options_head;
    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
    $options_breadcrumb['class']['div']="breadcrumb-large";
    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/quotes";
    $options_breadcrumb['items'][0]['title']=LANG_HEADER_MENU_QUOTES;
    $options_breadcrumb['items'][0]['inner_html']=LANG_HEADER_MENU_QUOTES;
    if( $search )
    {
    	$options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
    }
    else
    {
	    $title_category=str_replace("<br/>"," ",$title_category);
	    $title_category_sub=str_replace("<br/>"," ",$title_category_sub);
	    if( !empty($title_category) )
	    {
	        if( !empty($title_category_sub) )
	        {
	            $options_breadcrumb['items'][1]['href']="/".$_GET['lang']."/quotes/".$row_category['titleurl'];
	            $options_breadcrumb['items'][1]['title']=$title_category;
	        }
	        $options_breadcrumb['items'][1]['inner_html']=$title_category;
	    }
	    if( !empty($title_category_sub) ) $options_breadcrumb['items'][2]['inner_html']=$title_category_sub;
	}

    if( !empty($options_breadcrumb['items'][0]['href']) && empty($_GET['section_1']) )
    {
        $options_breadcrumb['mobile_button_text']=LANG_HEADER_MENU_HOME;
        $options_breadcrumb['mobile_button_url']="/".$_GET['lang'];
    }
    elseif( !empty($options_breadcrumb['items'][1]['href']) && !empty($_GET['section_1']) )
    {
        $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][1]['inner_html'];
        $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][1]['href'];
    }
    elseif( !empty($options_breadcrumb['items'][0]['href']) )
    {
        $options_breadcrumb['mobile_button_text']=$options_breadcrumb['items'][0]['inner_html'];
        $options_breadcrumb['mobile_button_url']=$options_breadcrumb['items'][0]['href'];
    }
    //print_r($_GET);
    //print_r($options_breadcrumb);
    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
    #end

    # right side block
    $html_print_body.="<div class='div-content-header-block' >";
        # section image
        $html_print_body.="<div class='div-content-header-block-left' >";
            if( !empty($title_category_sub) ) $h1_title=$title_category_sub;
            elseif( !empty($title_category) ) $h1_title=$title_category;
            else $h1_title=LANG_HEADER_MENU_QUOTES;
            $html_print_body.="<h1 class='h1-section'>".$h1_title."</h1>";
            $html_print_body.="<img src='/g/headers/quotes/1.jpg' alt='' width='523' height='208' />";
        $html_print_body.="</div>\n";

        /*
        # search block
        $options_quotes_search=array();
        $options_quotes_search=$options_head;
        $options_quotes_search['html']=$html;
        $options_quotes_search['class']['div-search-block']="div-content-header-block-right";
        $options_quotes_search['h1-title']=LANG_RIGHT_SEARCH_EXH_H1;
        $options_quotes_search['more-options']="hidden";
        $options_quotes_search['books_catid']=$books_catid;
        $html_print_body.=div_quotes_search($db,$options_quotes_search);
        # END search block
        */

        # search block
        $options_search_block=array();
        $options_search_block['options_head']=$options_head;
        $html_print_body.=$html->search_form_quotes($db,$options_search_block);
        # END search block

        $html_print_body.="<div class='clearer'></div>";
    $html_print_body.="</div>\n";

    $query_category_sub_where=" WHERE enable=1 AND sub_categoryid='".$categoryid_query."' ";
    $query_category_sub_order=" ORDER BY sort ";
    $query_category_sub=QUERIES::query_quotes_categories($db,$query_category_sub_where,$query_category_sub_order);
    $results_category_sub=$db->query($query_category_sub['query']);
    $count_sub=$db->numrows($results_category_sub);

    if( !$count_sub || $search ) $class_cont_quotes="div-category-landing-page-content-quotes";

    if( !$search ) $html_print_body.="<div class='div-section-content div-category-landing-page-padding-topbottom ".$class_cont_quotes."'>";

        # quotes main section /quotes
        if( $main_section )
        {
            /*
            $html_print_body.="<p class='p-section-description'>";
                //$html_print_body.=LANG_LITERATURE_DESC_TEXT;
                # browse all quotes
                if( $_GET['search']!="all" )
                {
                    //$html_print_body.="<br /><br />";
                    $html_print_body.="<a class='forward' href='/".$_GET['lang']."/quotes/search/?search=all' title='".LANG_RIGHT_SEARCH_QUOTES_SEARCHALL."' >".LANG_RIGHT_SEARCH_QUOTES_SEARCHALL."</a>\n";
                }
            $html_print_body.="</p>";
            */

            # print category thumbs
            $options_cat_thumbs=array();
            $options_cat_thumbs=$options_head;
            $options_cat_thumbs['categories']=$ul_quotes['categories'];
            $options_cat_thumbs['count_categories']=$ul_quotes['count_categories'];
            $options_cat_thumbs['class']['div-section-categories']="div-section-categories-quotes";
            $options_cat_thumbs['url_admin']="/admin/quotes/categories/edit/?quotes_categoryid=";
            $options_cat_thumbs['show_all']=1;
            $options_cat_thumbs['url_show_all']="/".$_GET['lang']."/quotes/search/?search=all";
            $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
            $html_print_body.=$return_cat_thumbs['html_print'];
            # END print category thumbs
        }
        # quote search
        elseif( $search )
        {
            # You searched for
            if( $search && !$_GET['info'] )
            {
                $html_print_body.="<div class='div-you-searched-for'>";
                    $html_print_body.=LANG_SEARCH_YOUSEARCHEDFOR.": <br />";

                    # keyword
                    if( !empty($_GET['keyword']) ) $html_print_body.=LANG_RIGHT_SEARCH_LIT_KEYWORD.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['keyword'])."</em><br />";

                    # year-from
                    if( !empty($_GET['year-from']) ) $html_print_body.=LANG_SEARCH_YEARFROM.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['year-from'])."</em><br />";

                    # year-to
                    if( !empty($_GET['year-to']) ) $html_print_body.=LANG_SEARCH_YEARTO.": <em class='search-highlight'>".UTILS::strip_slashes_recursive($_GET['year-to'])."</em><br />";

                $html_print_body.="</div>";
            }
            # END You searched for

    	    $query_where="";

    	    if( !empty($_GET['keyword']) || !empty($_GET['year-from']) || !empty($_GET['year-to']) ) $query_where.=" WHERE ";
    	    if( !empty($_GET['keyword']) )
    	    {

    	        # prepare search keyword
    	        $options_search=array();
    	        $options_search['db']=$db;
    	        $title_search=UTILS::prepear_search($_GET['keyword'],$options_search);
    	        # END prepare search keyword

    	        # prepare match
    	        $options_search_quotes=array();
    	        $options_search_quotes['search']=$title_search;
    	        $query_match=QUERIES_SEARCH::query_search_quotes($db,$options_search_quotes);
    	        $query_where.=$query_match;
    	        # END prepare match

    	    }
    	    if( !empty($_GET['keyword']) ) $and=" AND ";

            # year search
    	    if( !empty($_GET['year-from']) && !empty($_GET['year-to']) ) $query_where.=$and." year BETWEEN '".$_GET['year-from']."' AND '".$_GET['year-to']."' ";
    	    elseif( !empty($_GET['year-from']) ) $query_where.=$and." year>='".$_GET['year-from']."' ";
            //elseif( !empty($_GET['year-from']) && empty($_GET['year-to']) ) $query_where.=$and." year='".$_GET['year-from']."' ";
    	    elseif( !empty($_GET['year-to']) ) $query_where.=$and." year<='".$_GET['year-to']."' ";
            //elseif( !empty($_GET['year-to']) && empty($_GET['year-from']) ) $query_where.=$and." year='".$_GET['year-to']."' ";


    	    $query_order=" ORDER BY year ASC ";
    	    $query_quote=QUERIES::query_quotes($db,$query_where,$query_order);

            $options_quotes=array();
            $options_quotes=$options_head;
            $options_quotes['query']=$query_quote['query'];
            $options_quotes['back_to_top']=1;
            $options_quotes['search']=$_GET['keyword'];
    	    $options_quotes['search_info']=1;
            $html_print_body.=$html->quotes($db,$options_quotes);
        }
        else
        {
    	    if( !$count_sub )
    	    {
    	        $query_where_quote=" WHERE quotes_categoryid='".$categoryid_query."' ";
    	        $query_order=" ORDER BY year ASC ";
    	        $query_quote=QUERIES::query_quotes($db,$query_where_quote,$query_order,"");

    	        $options_quotes=array();
    	        $options_quotes=$options_head;
    	        $options_quotes['count_sub_categories']=$count_category_sub;
    	        $options_quotes['query']=$query_quote['query'];
    	        $options_quotes['back_to_top']=1;
    	        $options_quotes['search']=$_GET['keyword'];
    	        $html_print_body.=$html->quotes($db,$options_quotes);
    	    }
            else
            {
                # print sub category thumbs
                $options_cat_thumbs=array();
                $options_cat_thumbs=$options_head;
                $options_cat_thumbs['categoryid']=$categoryid_query;
                //print $categoryid_query;
                //print $categoryid;
                $options_cat_thumbs['categories']=$ul_quotes['categories_sub'];
                $options_cat_thumbs['count_categories']=$ul_quotes['count_sub_categories'];
                $options_cat_thumbs['class']['div-section-categories']="div-section-categories-quotes";
                $options_cat_thumbs['url_admin']="/admin/quotes/categories/edit/?quotes_categoryid=";
                $return_cat_thumbs=$html->section_categories_thumbs($db,$options_cat_thumbs);
                $html_print_body.=$return_cat_thumbs['html_print'];
                # END print sub category thumbs
            }
    	}

    if( !$search ) $html_print_body.="</div>";

#END div main content

# *** END *** MAIN QUOTES SECTION /quotes



$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>
$html_print_body.="<div class='clearer'></div>";

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
