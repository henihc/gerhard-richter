<?php
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");
//require ($_SERVER["DOCUMENT_ROOT"]."/includes/mod_rewrite.php");

//if( empty($_GET['search']) ) UTILS::redirect('/search');

$db=new dbCLASS;

$html = new html_elements;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$_GET=UTILS::if_empty_get($db,$options_get);

$class=array();
$html_print_head="";
$html_print_footer="";

$html_print_body="<script type='text/javascript'>\n";

$html_print_body.="</script>\n";

$html_print_body.="<div id='div-large-content' role='main' >";

if( !empty($_GET['section_1']) ) UTILS::not_found_404();


	$title_meta.=LANG_HEADER_MENU_SEARCH." &raquo; ".LANG_NEWS_AUCTIONS." &raquo; ";

	# title meta
	$html->title=$title_meta.LANG_TITLE_HOME;
	$html->lang=$_GET['lang'];

	# css & js files
	$html->css[]="/css/main.css";
	$html->css[]="/css/screen.css";
	$html->css[]="/css/main.mobile.css";
	$html->css[]="/css/news.css";
	$html->css[]="/css/page-numbering.css";
	$html->css[]="/css/auctions.css";
	$html->js[]="/js/jquery/jquery-2.1.4.min.js";
	$html->js[]="/js/jquery_ui/jquery-ui-1.11.4.min.js";
	$html->js[]="/js/common.js";
	$html->css[]="/css/tabs.css";

	# mobile menu
	$html->css[]="/js/mobile/menu/css/main.css?v=201509091702";
	$html->css[]="/js/mobile/menu/css/main.mobile.css?v=201509091702";
	$html->js[]="/js/mobile/menu/js/main.js?v=201509091702";

	# fancybox
	//$html->js[]="/js/jquery_fancybox/jquery-1.8.0.min.js";
	$html->js[]="/js/jquery_fancybox/jquery.fancybox.js?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/jquery.fancybox.css?v=2.1.0";
	$html->css[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.3";
	$html->js[]="/js/jquery_fancybox/fancybox.load.js";

	# jquery slide more
	//$html->js[]="/js/jquery_slide/jquery_slide.php";
	# jquery opp form validate
	//$html->js[]="/js/jquery_opp_form/jquery_opp_form.php";
	$html->css[]="/css/opp-form.css";

	#expand table
	//$html->js[]="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js";
	$html->css[]="/js/expandlist/src/css/gerhard-richter.mod.expandlist.css";
	$html->js[]="/js/expandlist/src/js/gerhard-richter.mod.expandlist.js";

	# meta-tags & head
	$options_head=array();
	//$options_head['meta_description']=LANG_META_DESCRIPTION_VIDEOS;
	$options_head['html_return']=1;
	$options_head['class']['body']="body";
	$html_print_head.=$html->head($db,'',$options_head);

	# left side blocks
	$html_print_head.="<div id='sub-nav'>";
	    $html_print_head.="<ul>\n";
	        $html_print_head.="<li class='first selected'><a href='/".$_GET['lang']."/auctions/?search=".$_GET['search']."' title='".LANG_NEWS_AUCTIONS."'>".LANG_NEWS_AUCTIONS."</a></li>\n";
	    $html_print_head.="</ul>\n";
	$html_print_head.="</div>";

	# div main content

	    ### Breadcrubmles
	    $options_breadcrumb=array();
	   	$options_breadcrumb=$options_head;
	    $options_breadcrumb['class']['ul']="ul-breadcrumbles";
	    $options_breadcrumb['class']['div']="breadcrumb-large";
	    $options_breadcrumb['items'][0]['href']="/".$_GET['lang']."/auctions/?search=".$_GET['search'];
	    $options_breadcrumb['items'][0]['title']=LANG_NEWS_AUCTIONS;
	    $options_breadcrumb['items'][0]['inner_html']=LANG_NEWS_AUCTIONS;
	    $options_breadcrumb['items'][1]['inner_html']=LANG_HEADER_MENU_SEARCH;
	    $html_print_body.=$html->ul_breadcrumbles($db,$options_breadcrumb);
	    #end

	    # section image
	    $html_print_body.="<div class='div-heading-picture' >";
	        $html_print_body.="<img src='/g/headers/news/".rand(1,5).".jpg' alt='' />";
	    $html_print_body.="</div>\n";

    	# right side block
	    $html_print_body.="<div class='div-right-side-info-box div-right-side-info-box-news-publications'>";
	        $options_social=array();
	        $options_social=$options_head;
	        $options_social['class']['twitter']="twitter";
	        $options_social['class']['facebook']="facebook";
	        $options_social['class']['span-follow-us']="span-social-box-follow";
	        $html_print_body.=$html->social_buttons($options_social);
	    $html_print_body.="</div>";
	   	$html_print_body.="<div class='clearer'></div>";

	    $html_print_body.="<h2 class='h2-section-title'>".LANG_NEWS_UPCOMING_FOR_AUCTION."</h2>";

	    $search_for_url=$_GET['search'];
	    $search_for_url=UTILS::strip_slashes_recursive($search_for_url);
	    $search_for_url=str_replace('"',"",$search_for_url);
	    $search_for_url=urlencode($search_for_url);

	    $options_search=array();
	    $options_search['db']=$db;
	    $prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
	    # prepare match
	    $options_search_auctionhouse=array();
	    $options_search_auctionhouse['search']=$prepear_search;
	    $query_match_auctionhouse=queries_search::query_search_auctionhouse($db,$options_search_auctionhouse);
	    $query_auctionhouse="SELECT ahID AS auctionhouseid FROM ".TABLE_AUCTIONHOUSE." a WHERE ".$query_match_auctionhouse;
	    # end prepare match
	    # prepare match
	    $options_search_sales_history=array();
	    $options_search_sales_history['search']=$prepear_search;
	    $query_match_sales_history=queries_search::query_search_sales_history($db,$options_search_sales_history);
	    # end prepare match
	    $results_auctionhouse=$db->query($query_auctionhouse);
	    
	    $auctionhouses=array();
	    while( $row_auctionhouse=$db->mysql_array($results_auctionhouse) )
	    {
	        $auctionhouses[]=$row_auctionhouse['auctionhouseid'];
	    }

	    if( empty($_GET['sort']) ) 
	    {   
	        $_GET['sort']=5;
	        $_GET['sort_how']=-1;
	    }   
	    else 
	    {   
	        $tmp=explode(",", $_GET['sort']);
	        $_GET['sort']=$tmp[0];
	        if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
	        else $_GET['sort_how']=-1;
	    } 

	    $options_table = array(
	        'options'       =>  array(
	            // Setup table options here...
	            'id'                => 'expandlist_literature_01',  // set DOM node id for js (uniqid() if not set)
	            'sort_by'   => array($_GET['sort'], $_GET['sort_how']),                         // default column sorted                
	            'expandable'        => false,                       // Allow tr tags to expand (expand-boxes required)
	            'highlight'         =>  $_GET['search'],                     // highlight patterns in content
	            'html_class'         =>  $html,
	            'html_return'         =>  1,
	            'onheaderclick'     => $_SERVER['PATH_INFO'] . '?search='.$search_for_url.'&sort='
	        ),  
	        'cols'          =>  array(
	            // Define columns here...
	            array( 'caption' => '',                 'width' => '52px',   'col' => '0',   'sortable' => false, 'sort_col' => 'p.saleDate',    'content_align' => 'center'),
	            array( 'caption' => LANG_ARTWORK_BOOKS_REL_ARTWORK,          'width' => '168px',     'col' => '1',   'sortable' => true,  'sort_col' => 'p.titleDE',   'content_align' => 'left' ), 
	            array( 'caption' => LANG_NEWS_AUCTIONS_AUCTIONHOUSE,    'width' => '110px',   'col' => '2',   'sortable' => true,  'sort_col' => 'ah.house',   'content_align' => 'left' ), 
	            array( 'caption' => LANG_ESTIMATE,      'width' => '22%',    'col' => '3',   'sortable' => true, 'sort_col' => 's.estLowUSD',    'content_align' => 'left' ), 
	            array( 'caption' => LANG_SOLDPR,        'width' => '*',   'col' => '4',   'sortable' => true, 'sort_col' => 's.soldForUSD',    'content_align' => 'left' ), 
	            array( 'caption' => LANG_NEWS_AUCTIONS_DATE,             'width' => '9%',   'col' => '5',   'sortable' => true, 'sort_col' => 's.saleDate',    'content_align' => 'left' )
	        )
	    );  


	    $options_table_data=array();
	    $options_table_data['html_return']=$options_table['options']['html_return'];
	    $options_table_data['cols']=$options_table['cols'];
	    $options_table_data['html_class']=$options_table['options']['html_class'];
	    $options_table_data['sort']=$options_table['options']['sort_by'];
	    $options_table_data['highlight']=$_GET['search'];
	    $table_data = new Table_Data($db,$options_table_data);
	    $options_table_data=array();
	    $options_table_data['auctionhouses']=$auctionhouses;
	    $options_table_data['search']=$_GET['search'];
	    $options_table_data['query_match_sales_history']=$query_match_sales_history;
	    $options_table_data['limit']=" LIMIT ".($_GET['sp']*($_GET['p']-1)).",".$_GET['sp'];;
	    $options_table_data['results']=1;
	    $rows=$table_data->table_data_auctions($options_table_data);

	    $html_print_body.="<div class='div-tabs-headline'>";
	        $pages=@ceil($rows['count']/$_GET['sp']);
	        $options_page_numbering=array();
	        $options_page_numbering=$options_head;
	        $options_page_numbering['per_page']=1;
	        $options_page_numbering['pages']=$pages;
	        $options_page_numbering['count']=$rows['count'];
	        $options_page_numbering['sp']=$_GET['sp'];
	        $options_page_numbering['p']=$_GET['p'];
	        $options_page_numbering['url']="?search=".$search_for_url;
	        $options_page_numbering['url_after'].="&sp=".$_GET['sp'];
	        if( !empty($_GET['sort']) ) 
	        {    
	            $options_page_numbering['url_after'].="&sort=".$_GET['sort'].",".$_GET['sort_how'];
	            $options_page_numbering['url_after_per_page'].="&sort=".$_GET['sort'].",".$_GET['sort_how'];
	        }    
	        $options_page_numbering['class']['div-per-page']="div-per-page-auctions";
	        $options_page_numbering['class']['div-page-navigation']="div-page-navigation-auctions";
	        $html_print_body.=$html->page_numbering($db,$options_page_numbering);
	    $html_print_body.="</div>";

	    if( $rows['count'] )
	    {
	        $table = new Table($db, $rows, $options_table);
	        $html_print_body.=$table->draw_table();

	        $html_print_body.="<div class='div-tabs-headline div-tabs-headline-bottom'>";        
	            $html_print_body.=$html->page_numbering($db,$options_page_numbering);
	        $html_print_body.="</div>";

	        $html_print_body.="<p class='p-news-dislaimer-title'>".LANG_NEWS_DISCLIMER."</p>";

	        $options_query_text=array();
	        $options_query_text['where']=" WHERE textid=2 ";
	        $query_text=QUERIES::query_text($db,$options_query_text);
	        $results=$db->query($query_text['query']);
	        $count=$db->numrows($results);
	        $row=$db->mysql_array($results);
	        $row=UTILS::html_decode($row);
	        $text=UTILS::row_text_value($db,$row,"text");

	        $options_admin_edit=array();
	        $options_admin_edit=$options_head;
	        $html_print_body.=UTILS::admin_edit("/admin/pages/edit/?textid=2",$options_admin_edit);
	        $html_print_body.="<div id='div-news-disclimer' class='div_text_wysiwyg'>\n";
	            $html_print_body.=$text;
	        $html_print_body.="</div>\n";

	    }
	    else
	    {   
	        //$html_print_body.="<p class='p-no-news'>".LANG_NEWS_NOCURRENT."</p>";
	    }  

	#END div main content


$html_print_body.="</div>"; # END - <div id='div-large-content' role='main'>

print $html_print_head;
print $html_print_body;

# footer
$options_foot=array();
$options_foot=$options_head;
$html_print_footer.=$html->foot($db,$options_foot);
print $html_print_footer;

?>
