<?php
class QUERIES_SEARCH
{

    public static function query_search_paintings($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' || $_GET['lang']=='zh' ) $query_match.="p.titleEN,";
        $query_match.="p.titleDE,";
        if( $_GET['lang']=='fr' ) $query_match.="p.titleFR,";
        if( $_GET['lang']=='it' ) $query_match.="p.titleIT,";
        if( $_GET['lang']=='zh' ) $query_match.="p.titleZH,";
        $query_match.="p.title_search_1,";
        $query_match.="p.title_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="p.title_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="p.title_search_4,";
        //if( $values['global_search'] ) $query_match.="p.number,";
        if( $values['global_search'] ) 
        {
            $query_match.="p.number,";
            $query_match.="p.number_search,";
            $query_match.="p.year,";
        }
        $query_match.="p.keywords";
        
        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_microsites($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="m.title_en,";
        if( $_GET['lang']=='de' ) $query_match.="m.title_de,";
        if( $_GET['lang']=='fr' ) $query_match.="m.title_fr,";
        if( $_GET['lang']=='it' ) $query_match.="m.title_it,";
        if( $_GET['lang']=='zh' ) $query_match.="m.title_zh,";
        if( $_GET['lang']=='en' ) $query_match.="m.text_en";
        if( $_GET['lang']=='de' ) $query_match.="m.text_de";
        if( $_GET['lang']=='fr' ) $query_match.="m.text_fr";
        if( $_GET['lang']=='it' ) $query_match.="m.text_it";
        if( $_GET['lang']=='zh' ) $query_match.="m.text_zh";
        if( $values['global_search'] ) 
        {
            //$query_match.="m.number,";
        }
        
        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_museums($db,$values=array())
    {
        $query_match="
            ( 
                mu.museum LIKE '%".$values['search']."%' OR 
                mu.museum_search_1 LIKE '%".$values['search']."%' OR 
                mu.museum_search_1 LIKE '%".$values['search']."%' 
            ) ";

        return $query_match;
    }

    public static function query_search_biography($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="bi.title_en,";
        if( $_GET['lang']=='de' ) $query_match.="bi.title_de,";
        if( $_GET['lang']=='fr' ) $query_match.="bi.title_fr,";
        if( $_GET['lang']=='it' ) $query_match.="bi.title_it,";
        if( $_GET['lang']=='zh' ) $query_match.="bi.title_zh,";

        if( $_GET['lang']=='en' ) $query_match.="bi.text_en";
        if( $_GET['lang']=='de' ) $query_match.="bi.text_de,";
        if( $_GET['lang']=='fr' ) $query_match.="bi.text_fr,";
        if( $_GET['lang']=='it' ) $query_match.="bi.text_it,";
        if( $_GET['lang']=='zh' ) $query_match.="bi.text_zh";

        if( $_GET['lang']=='de' ) $query_match.="bi.title_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="bi.title_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="bi.title_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="bi.title_search_4,";

        if( $_GET['lang']=='de' )$query_match.="bi.text_search_1, ";
        if( $_GET['lang']=='de' )$query_match.="bi.text_search_2 ";
        if( $_GET['lang']=='fr' ) $query_match.="bi.text_search_3 ";
        if( $_GET['lang']=='it' ) $query_match.="bi.text_search_4 ";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_quotes($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="q.text_en,";
        if( $_GET['lang']=='de' ) $query_match.="q.text_de,";
        if( $_GET['lang']=='fr' ) $query_match.="q.text_fr,";
        if( $_GET['lang']=='it' ) $query_match.="q.text_it,";
        if( $_GET['lang']=='zh' ) $query_match.="q.text_zh,";
        if( $_GET['lang']=='en' ) $query_match.="q.source_en";
        if( $_GET['lang']=='de' ) $query_match.="q.source_de,";
        if( $_GET['lang']=='fr' ) $query_match.="q.source_fr,";
        if( $_GET['lang']=='it' ) $query_match.="q.source_it,";
        if( $_GET['lang']=='zh' ) $query_match.="q.source_zh";
        if( $_GET['lang']=='de' ) $query_match.="q.text_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="q.text_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="q.text_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="q.text_search_4,";
        if( $_GET['lang']=='de' ) $query_match.="q.source_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="q.source_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="q.source_search_3";
        if( $_GET['lang']=='it' ) $query_match.="q.source_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_exhibitions($db,$values=array())
    {
        $query_match="MATCH(";

        $query_match.="e.title_original,";
        $query_match.="e.title_original_search_1,";
        $query_match.="e.title_original_search_2,";
        if( $_GET['lang']=='en' ) $query_match.="e.titleEN";
        if( $_GET['lang']=='de' ) $query_match.="e.titleDE,";
        if( $_GET['lang']=='fr' ) $query_match.="e.titleFR,";
        if( $_GET['lang']=='it' ) $query_match.="e.titleIT,";
        if( $_GET['lang']=='zh' ) $query_match.="e.titleZH";
        if( $_GET['lang']=='de' ) $query_match.="e.title_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="e.title_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="e.title_search_3";
        if( $_GET['lang']=='it' ) $query_match.="e.title_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_locations($db,$values=array())
    {
        $query_match="MATCH(";

        $query_match.="lo.location_en,";
        $query_match.="lo.location_de,";
        $query_match.="lo.location_fr,";
        $query_match.="lo.location_it,";
        $query_match.="lo.location_zh,";
        $query_match.="lo.location_search_1,";
        $query_match.="lo.location_search_2,";
        $query_match.="lo.location_search_3,";
        $query_match.="lo.location_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_locations_city($db,$values=array())
    {
        $query_match="MATCH(";

        /*
        if( $_GET['lang']=='en' ) $query_match.="ci.city_en";
        if( $_GET['lang']=='de' ) $query_match.="ci.city_de,";
        if( $_GET['lang']=='fr' ) $query_match.="ci.city_fr,";
        if( $_GET['lang']=='it' ) $query_match.="ci.city_it,";
        if( $_GET['lang']=='zh' ) $query_match.="ci.city_zh";
        if( $_GET['lang']=='de' ) $query_match.="ci.city_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="ci.city_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="ci.city_search_3";
        if( $_GET['lang']=='it' ) $query_match.="ci.city_search_4";
         */

        $query_match.="ci.city_en,";
        $query_match.="ci.city_de,";
        $query_match.="ci.city_fr,";
        $query_match.="ci.city_it,";
        $query_match.="ci.city_zh,";
        $query_match.="ci.city_search_1,";
        $query_match.="ci.city_search_2,";
        $query_match.="ci.city_search_3,";
        $query_match.="ci.city_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_locations_country($db,$values=array())
    {
        $query_match="MATCH(";

        /*
        if( $_GET['lang']=='en' ) $query_match.="co.country_en";
        if( $_GET['lang']=='de' ) $query_match.="co.country_de,";
        if( $_GET['lang']=='fr' ) $query_match.="co.country_fr,";
        if( $_GET['lang']=='it' ) $query_match.="co.country_it,";
        if( $_GET['lang']=='zh' ) $query_match.="co.country_zh";
        if( $_GET['lang']=='de' ) $query_match.="co.country_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="co.country_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="co.country_search_1";
        if( $_GET['lang']=='it' ) $query_match.="co.country_search_1";
         */

        $query_match.="co.country_en,";
        $query_match.="co.country_de,";
        $query_match.="co.country_fr,";
        $query_match.="co.country_it,";
        $query_match.="co.country_zh,";
        $query_match.="co.country_search_1,";
        $query_match.="co.country_search_2,";
        $query_match.="co.country_search_3,";
        $query_match.="co.country_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_literature_author($db,$values=array())
    {
        $query_match="
        MATCH(
            b.author,
            b.author_essay,
            b.author_essay_title,
            b.author_search_1,
            b.author_search_2
        ) AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_literature_title($db,$values=array())
    {
        $query_match="
        MATCH(
            b.title,
            b.title_search_1,
            b.title_search_2
        ) AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_literature_keyword($db,$values=array())
    {
        $query_match="MATCH(";

        $query_match.="b.title,";
        $query_match.="b.title_periodical,";
        $query_match.="b.title_search_1,";
        $query_match.="b.title_search_2,";
        $query_match.="b.author,";
        $query_match.="b.author_essay,";
        $query_match.="b.author_essay_title,";
        $query_match.="b.author_search_1,";
        $query_match.="b.author_search_2,";
        if( $_GET['lang']=='en' ) $query_match.="b.infoEN,";
        if( $_GET['lang']=='de' ) $query_match.="b.infoDE,";
        if( $_GET['lang']=='fr' ) $query_match.="b.infoFR,";
        if( $_GET['lang']=='it' ) $query_match.="b.infoIT,";
        if( $_GET['lang']=='zh' ) $query_match.="b.infoZH,";
        if( $_GET['lang']=='de' ) $query_match.="b.info_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="b.info_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="b.info_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="b.info_search_4,";
        if( $_GET['lang']=='en' ) $query_match.="b.notesEN,";
        if( $_GET['lang']=='de' ) $query_match.="b.notesDE,";
        if( $_GET['lang']=='fr' ) $query_match.="b.notesFR,";
        if( $_GET['lang']=='it' ) $query_match.="b.notesIT,";
        if( $_GET['lang']=='zh' ) $query_match.="b.notesZH,";
        if( $_GET['lang']=='de' || $_GET['lang']=='en' ) $query_match.="b.notes_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="b.notes_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="b.notes_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="b.notes_search_4,";
        $query_match.="b.publisher,";
        $query_match.="b.keywords";
        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_literature($db,$values=array())
    {
        $query_match="MATCH(";

        $query_match.="b.author,";
        $query_match.="b.author_essay,";
        $query_match.="b.author_essay_title,";
        $query_match.="b.author_search_1,";
        $query_match.="b.author_search_2,";
        $query_match.="b.title,";
        $query_match.="b.title_search_1,";
        $query_match.="b.title_search_2,";
        $query_match.="b.title_periodical,";
        $query_match.="b.title_periodical_search_1,";
        $query_match.="b.title_periodical_search_2,";
        if( $_GET['lang']=='en' ) $query_match.="b.infoEN,";
        if( $_GET['lang']=='de' ) $query_match.="b.infoDE,";
        if( $_GET['lang']=='fr' ) $query_match.="b.infoFR,";
        if( $_GET['lang']=='it' ) $query_match.="b.infoIT,";
        if( $_GET['lang']=='zh' ) $query_match.="b.infoZH,";
        if( $_GET['lang']=='de' ) $query_match.="b.info_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="b.info_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="b.info_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="b.info_search_4,";
        if( $_GET['lang']=='en' ) $query_match.="b.notesEN,";
        if( $_GET['lang']=='de' ) $query_match.="b.notesDE,";
        if( $_GET['lang']=='fr' ) $query_match.="b.notesFR,";
        if( $_GET['lang']=='it' ) $query_match.="b.notesIT,";
        if( $_GET['lang']=='zh' ) $query_match.="b.notesZH,";
        if( $_GET['lang']=='de' ) $query_match.="b.notes_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="b.notes_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="b.notes_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="b.notes_search_4,";
        $query_match.="b.publisher,";
        $query_match.="b.keywords";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_literature_categories($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="bc.title_en,";
        if( $_GET['lang']=='de' ) $query_match.="bc.title_de,";
        if( $_GET['lang']=='fr' ) $query_match.="bc.title_fr,";
        if( $_GET['lang']=='it' ) $query_match.="bc.title_it,";
        if( $_GET['lang']=='zh' ) $query_match.="bc.title_zh";
        if( $_GET['lang']=='en' ) $query_match.="bc.title_search_4";
        if( $_GET['lang']=='de' ) $query_match.="bc.title_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="bc.title_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="bc.title_search_3";
        if( $_GET['lang']=='it' ) $query_match.="bc.title_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_articles_title($db,$values=array())
    {
        $query_match="
        MATCH(
            b.title,
            b.title_search_1,
            b.title_search_2,
            b.title_periodical,
            b.title_periodical_search_1,
            b.title_periodical_search_2
        ) AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }


    public static function query_search_videos($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="v.titleEN,";
        if( $_GET['lang']=='de' ) $query_match.="v.titleDE,";
        if( $_GET['lang']=='fr' ) $query_match.="v.titleFR,";
        if( $_GET['lang']=='it' ) $query_match.="v.titleIT,";
        if( $_GET['lang']=='zh' ) $query_match.="v.titleZH,";
        if( $_GET['lang']=='de' ) $query_match.="v.title_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="v.title_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="v.title_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="v.title_search_4,";
        if( $_GET['lang']=='en' ) $query_match.="v.info_en,";
        if( $_GET['lang']=='de' ) $query_match.="v.info_de,";
        if( $_GET['lang']=='fr' ) $query_match.="v.info_fr,";
        if( $_GET['lang']=='it' ) $query_match.="v.info_it,";
        if( $_GET['lang']=='zh' ) $query_match.="v.info_zh,";
        if( $_GET['lang']=='de' ) $query_match.="v.info_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="v.info_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="v.info_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="v.info_search_4,";
        if( $_GET['lang']=='de' ) $query_match.="v.name_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="v.name_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="v.name_search_1,";
        if( $_GET['lang']=='it' ) $query_match.="v.name_search_1,";
        $query_match.="v.name";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_text($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="t.text_en,";
        if( $_GET['lang']=='de' ) $query_match.="t.text_de,";
        if( $_GET['lang']=='fr' ) $query_match.="t.text_fr,";
        if( $_GET['lang']=='it' ) $query_match.="t.text_it,";
        if( $_GET['lang']=='zh' ) $query_match.="t.text_zh";
        if( $_GET['lang']=='en' ) $query_match.="t.text_search_1";
        if( $_GET['lang']=='de' ) $query_match.="t.text_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="t.text_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="t.text_search_3";
        if( $_GET['lang']=='it' ) $query_match.="t.text_search_4";
            
        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_auctionhouse($db,$values=array())
    {
        $query_match="
        MATCH(
            a.house,
            a.house_search_1,
            a.house_search_2
        ) AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_sales_history($db,$values=array())
    {
        $query_match="
        MATCH(
            s.saleName
        ) AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

    public static function query_search_news_talks($db,$values=array())
    {
        $query_match="MATCH(";

        if( $_GET['lang']=='en' ) $query_match.="n.titleEN,";
        if( $_GET['lang']=='de' ) $query_match.="n.titleDE,";
        if( $_GET['lang']=='fr' ) $query_match.="n.titleFR,";
        if( $_GET['lang']=='it' ) $query_match.="n.titleIT,";
        if( $_GET['lang']=='zh' ) $query_match.="n.titleZH,";
        if( $_GET['lang']=='de' ) $query_match.="n.title_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="n.title_search_2,";
        if( $_GET['lang']=='fr' ) $query_match.="n.title_search_3,";
        if( $_GET['lang']=='it' ) $query_match.="n.title_search_4,";
        if( $_GET['lang']=='en' ) $query_match.="n.textEN";
        if( $_GET['lang']=='de' ) $query_match.="n.textDE,";
        if( $_GET['lang']=='fr' ) $query_match.="n.textFR,";
        if( $_GET['lang']=='it' ) $query_match.="n.textIT,";
        if( $_GET['lang']=='zh' ) $query_match.="n.textZH";
        if( $_GET['lang']=='de' ) $query_match.="n.text_search_1,";
        if( $_GET['lang']=='de' ) $query_match.="n.text_search_2";
        if( $_GET['lang']=='fr' ) $query_match.="n.text_search_3";
        if( $_GET['lang']=='it' ) $query_match.="n.text_search_4";

        $query_match.=") AGAINST ('".$values['search']."' IN BOOLEAN MODE) ";

        return $query_match;
    }

}
