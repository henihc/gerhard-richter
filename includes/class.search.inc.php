<?php

class Search
{
    private $subject;

    public function __construct($subject)
    {
        $this->subject = strip_tags($subject, '<em><i><strong><b><u><sub><sup><hr>');
    }

    public function find($keyword, $words_left = 6, $words_right = 6)
    {
        //print_r($keyword);
        $matches = null;
        $pattern="/";
        $i=0;
        foreach( $keyword as $search_word )
        {
            $search_word=UTILS::strip_slashes_recursive($search_word);
            $i++;
            if( $i==1 ) $kw = preg_quote($search_word);
            //$pattern.=$kw."+";
            $pattern.="(\d*\w*)(".$kw."+)(\d*\w*)";
            if( $i<(count($keyword)) ) $pattern.="|";
            //print $pattern."-".."|<br /><br />";
        }
        $pattern.="/i";


        //$pattern="'#'.$kw.'#Ui'";
        //$pattern="/kunst+/i";
        //$pattern="/(\d*\w*)(get+)(\d*\w*)/i";
        //print $keyword."[-".$pattern."-]";
        if (preg_match($pattern, $this->subject, $matches, PREG_OFFSET_CAPTURE)) {
            $result = $matches[0][0];
            $offset = $matches[0][1];
            //if( $_SESSION['debug_page'] ) print_r($matches);
            //print_r($matches);
            //$behind = $this->locate_words_behind($words_right, $offset + strlen($kw));
            //$behind = $this->locate_words_behind($words_right, $matches[2][1] + strlen($kw));
            $behind = $this->locate_words_behind($words_right, $matches[3][1] + strlen($matches[3][0]));
            //print "<p style='color:red;'>".$words_right."-".$offset."-".$kw."</p>";
            $before = $this->locate_words_before($words_left, $offset);

            $return=array();
            $return['found']=$before.' '.$result.' '.$behind;
            $return['keywords']=$matches[0];
            
            //print "|||".$before.'----'.$result.'----'.$behind."|||";  
            //return $before.' '.$result.' '.$behind;
            return $return;
        }

        return NULL;
    }

    protected function locate_words_behind($count, $offset)
    {
        $subject = substr($this->subject, $offset);

        $words = preg_split('/\s+/', $subject, $count + 1);
        
        if (count($words) > $count) {
            array_pop($words);
        }

        return implode(' ', $words);
    }

    protected function locate_words_before($count, $offset)
    {
        $subject = substr($this->subject, 0, $offset);

        $words = preg_split('/\s+/', $subject);

        while (count($words) > $count) {
            array_shift($words);
        }

        return implode(' ', $words);
    }
}

?>
