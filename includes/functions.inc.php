<?php

function get_browsers_default_language($http_accept_language)
{
    $browser_language = substr($http_accept_language, 0, 2);

    return $browser_language;
}

function location($db,$values)
{
    if( $values['return_array'] ) $location=array();
    else $location="";
    if( !empty($values['row']['ah_cityid']) ) $values['row']['cityid']=$values['row']['ah_cityid'];
    if( !empty($values['row']['ah_countryid']) ) $values['row']['countryid']=$values['row']['ah_countryid'];
    //print $values['row']['cityid']."--".$values['row']['countryid']."--||";
    if( !empty($values['row']['locationid']) )
    {
        $query_where_location=" WHERE locationid='".$values['row']['locationid']."' ";
        $query_location=QUERIES::query_locations($db,$query_where_location);
        $results_location=$db->query($query_location['query_without_limit']);
        $count=$db->numrows($results_location);
        $row_location=$db->mysql_array($results_location);
        $row_location=UTILS::html_decode($row_location);
        $values_row_text=array();
        $values_row_text['search']=$values['search'];
        $location_loc=UTILS::row_text_value($db,$row_location,"location",$values_row_text);
        $values_search_found=array();
        $values_search_found['search']=$values['search'];
        $values_search_found['text']=$location_loc;
        $values_search_found['a_name']=1;
        $location_loc=UTILS::show_search_found_keywords($db,$values_search_found);
        if( $values['return_array'] ) $location['location']=$location_loc;
        else $location.=$location_loc;
    }
    # not specified
    if( $values['row']['cityid']==668 )
    {
        //$location['city']="";
        //$location.="";
    }
    #END not specified
    elseif( !empty($values['row']['cityid']) )
    {
        $query_where_city=" WHERE cityid='".$values['row']['cityid']."' ";
        $query_city=QUERIES::query_locations_city($db,$query_where_city);
        $results_city=$db->query($query_city['query_without_limit']);
        $count=$db->numrows($results_city);
        $row_city=$db->mysql_array($results_city);
        $row_city=UTILS::html_decode($row_city);
        $values_row_text=array();
        $values_row_text['search']=$values['search'];
        $city=UTILS::row_text_value($db,$row_city,"city",$values_row_text);
        $values_search_found=array();
        $values_search_found['search']=$values['search'];
        $values_search_found['text']=$city;
        $values_search_found['a_name']=1;
        $city=UTILS::show_search_found_keywords($db,$values_search_found);
        if( !empty($location_loc) ) $comma=", ";
        else $comma="";
        //$location.=$comma.$city."-".$values['row']['cityid'];
        if( $values['return_array'] ) $location['city']=$city;
        elseif( !empty($city) ) $location.=$comma.$city;
    }
    # not specified
    if( $values['row']['countryid']==83 )
    {
        //$location['country']="";
        //$location.="";
    }
    #END not specified
    elseif( !empty($values['row']['countryid']) )
    {
        $query_where_country=" WHERE countryid='".$values['row']['countryid']."' ";
        $query_country=QUERIES::query_locations_country($db,$query_where_country);
        $results_country=$db->query($query_country['query_without_limit']);
        $count=$db->numrows($results_country);
        $row_country=$db->mysql_array($results_country);
        $row_country=UTILS::html_decode($row_country);
        $values_row_text=array();
        $values_row_text['search']=$values['search'];
        $country=UTILS::row_text_value($db,$row_country,"country",$values_row_text);
        $values_search_found=array();
        $values_search_found['search']=$values['search'];
        $values_search_found['text']=$country;
        $values_search_found['a_name']=1;
        $country=UTILS::show_search_found_keywords($db,$values_search_found);
        if( $country!=$city )
        {
            if( !empty($location_loc) || !empty($city) ) $comma=", ";
            else $comma="";
            if( $values['return_array'] ) $location['country']=$country;
            else $location.=$comma.$country;
        }
    }

    if( ( !empty($values['row']['optionid_loan_type']) || !empty($values['row']['loan_locationid']) ) && $values['museum'] )
    {
        if( !empty($location_loc) || !empty($city) || !empty($loaction) ) $comma=" ";
        else $comma="";
        $loan=$comma."(";
        # loan type
        $options_show_dropdown_title=array();
        $options_show_dropdown_title['search']=$values['search'];
        $options_show_dropdown_title['html_return']=1;
        $loan_type=show_dropdown_option_title($db,$values['row']['optionid_loan_type'],$options_show_dropdown_title);

        $values_search_found=array();
        $values_search_found['search']=$values['search'];
        $values_search_found['text']=$loan_type;
        $values_search_found['a_name']=1;
        $loan_type=UTILS::show_search_found_keywords($db,$values_search_found);
        $loan.=$loan_type;
        # END loan type
        # loan location
        $loan_location=array();
        $loan_location['row']['locationid']=$values['row']['loan_locationid'];
        $loan_location['row']['cityid']=$values['row']['loan_cityid'];
        $loan_location['row']['countryid']=$values['row']['loan_countryid'];
        $options_location_loan=array();
        $options_location_loan['row']=$loan_location['row'];
        $location_loan=location($db,$options_location_loan);
        if( !empty($loan_type) ) $comma=" ";
        else $comma="";
        if( !empty($location_loan) ) $loan.=$comma.$location_loan;
        # END loan location
        $loan.=")";
        if( $values['return_array'] ) $location['loan']=$loan;
        else $location.=$loan;
    }

    return $location;
}

function exhibition_date($db,$row,$lang="en")
{
    if( !empty($_GET['lang']) ) $lang=$_GET['lang'];
    $row['start_month']=UTILS::date_month($db,$row['start_month']);
    $row['end_month']=UTILS::date_month($db,$row['end_month']);

    $tmp_from="startDate3_".$lang;
    $tmp_to="endDate3_".$lang;
    $date_from=$row[$tmp_from];
    $date_to=$row[$tmp_to];
    $date_from=UTILS::date_month($db,$date_from);
    $date_to=UTILS::date_month($db,$date_to);

    # start date month or year display
    if( $row['showyear_start'] )
    {
        $date_start=$row['startDate4'];
    }
    elseif( $row['showmonth_start'] )
    {
        $date_start=$row['start_month'];
    }
    else
    {
    	$date_start=$date_from;
    }

    # end date month or year display
    if( $row['showyear_end'] )
    {
        $date_end=$row['end_year'];
    }
    elseif( $row['showmonth_end'] )
    {
        $date_end=$row['end_month'];
    }
    else
    {
    	$date_end=$date_to;
    }

    # building date display

    # $date_from -
    if( !empty($date_from) && empty($date_end) )
    {
        $date=$date_start." &#8211; ";
    }
    # $date_from
    elseif( !empty($date_start) && !empty($date_end) && $date_start==$date_end )
    {
        $date=$date_start;
    }
    # $date_from - $date_to
    else
    {
        $date=$date_start;
        if( !empty($date_start) && !empty($date_end) ) $date.=" &#8211; ";
        if( !empty($row['date_to']) && $row['date_to']!="0000-00-00" && !empty($date_end) ) $date.=$date_end;
    }

    return $date;
}

function select_books_categories($db,$selectedid,$options=array())
{
    $html_print="";

    $query_category="SELECT * FROM ".TABLE_BOOKS_CATEGORIES." WHERE ( sub_books_catid='' OR sub_books_catid IS NULL OR sub_books_catid=0 ) ".$options['where']." ORDER BY sort ";
    //print $query_category;
    $results=$db->query($query_category);
    $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".@$options['class']['select_books_catid']." ".$options['class']['select-field']." ".$options['class']['select-large']."' >\n";
        if( empty($options['multiple']) ) $html_print.="<option value=''>".LANG_RIGHT_SEARCH_LIT_CATEGORY."...</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            $row=UTILS::html_decode($row);
            $title=UTILS::row_text_value($db,$row,"title");
            $query_sub="SELECT * FROM ".TABLE_BOOKS_CATEGORIES." WHERE sub_books_catid='".$row['books_catid']."' ORDER BY sort ";
            $result_sub=$db->query($query_sub);
            $count_sub=$db->numrows($result_sub);
            if( $options['sub'] && $count_sub )
            {
                if( !$options['optgroup'] ) $html_print.="<optgroup label='".$title."'>";
                    while( $row_sub=$db->mysql_array($result_sub) )
                    {
                        $row_sub=UTILS::html_decode($row_sub);
                        $title_sub=UTILS::row_text_value($db,$row_sub,"title");
                        if( $row_sub['books_catid']==$selectedid ) $selected="selected='selected'";
                        else $selected="";
                        $html_print.="<option value='".$row_sub['books_catid']."' $selected >".$title_sub."</option>\n";
                    }
                if( !$options['optgroup'] ) $html_print.="</optgroup>";
            }
            else
            {
                if( $row['books_catid']==$selectedid || @in_array($row['books_catid'], $selectedid) ) $selected="selected='selected'";
                else $selected="";
                $html_print.="<option value='".$row['books_catid']."' ".$selected." >".$title."</option>\n";
            }
        }
    $html_print.="</select>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function select_books_languages($db,$selectedid,$options=array())
{
    $html_print="";
    $i=0;
    if( empty($_GET['lang']) ) $_GET['lang']="en";
    $results=$db->query("SELECT * FROM ".TABLE_BOOKS_LANGUAGES." ".$where." ORDER BY sort_".$_GET['lang']." ");
    $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['$onchange']." ".$options['multiple']." class='".@$options['class']['select_books_languageid']." ".$options['class']['select-field']." ".$options['class']['select-large']."' >\n";
        if( empty($selectedid) ) $selected="selected='selected'";
        else $selected="";
        if( empty($options['multiple']) ) $html_print.="<option value='' ".$selected.">".$options['select_title']."</option>\n";
        while( $row=$db->mysql_array($results) )
        {
            $i++;
            if( $row['languageid']==$selectedid || @in_array($row['languageid'], $selectedid) ) $selected="selected='selected'";
            else $selected="";
            $row=UTILS::html_decode($row);
            $title=UTILS::row_text_value($db,$row,"title");
            $html_print.="<option value='".$row['languageid']."' ".$selected." >".$title."</option>\n";
            if( $i==3 ) $html_print.="<option value='' disabled='disabled' >-----------------------</option>\n";
        }
    $html_print.="</select>\n";

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function select_dropdown($db,$selectid,$selectedid,$options=array())
{
    $html_print="";

    if( !empty($selectid) )
    {
        $query_dropdown_where=" WHERE selectid='".$selectid."' ";
        $query_dropdown_order="";
        $query_dropdown_limit=" LIMIT 1 ";
        $query_dropdown=QUERIES::query_select_dropdowns($db,$query_dropdown_where,$query_dropdown_order,$query_dropdown_limit);
        $results_dropdown=$db->query($query_dropdown['query']);
        $count_dropdown=$db->numrows($results_dropdown);

        if( $count_dropdown )
        {
            $row_dropdown=$db->mysql_array($results_dropdown);

            $query_dropdown_options_where=" WHERE selectid='".$row_dropdown['selectid']."' ";
            $query_dropdown_options_order=" ORDER BY sort ";
            $query_dropdown_options_limit="";
            $query_dropdown_options=QUERIES::query_select_dropdowns_options($db,$query_dropdown_options_where,$query_dropdown_options_order,$query_dropdown_options_limit);
            $results_dropdown_options=$db->query($query_dropdown_options['query']);

            if( empty($options['name']) )
            {
                $options['name']=$row_dropdown['name'];
            }

            if( empty($options['id']) )
            {
                $options['id']=$row_dropdown['name'];
            }

            $html_print.="<select name='".$options['name']."' id='".$options['id']."' ".$options['onchange']." ".$options['multiple']." class='".$options['class']['select_dropdown']."' >\n";
                if( empty($options['multiple']) ) $html_print.="<option value=''>".LANG_SELECT."</option>\n";
                while( $row_dropdown_options=$db->mysql_array($results_dropdown_options) )
                {
                    $title=UTILS::row_text_value($db,$row_dropdown_options,"title");
                    if( $row_dropdown_options['optionid']==$selectedid || @in_array($row_dropdown_options['optionid'], $selectedid) ) $selected="selected='selected'";
                    else $selected="";
                    $html_print.="<option value='".$row_dropdown_options['optionid']."' ".$selected." >".$title."</option>\n";
                }
            $html_print.="</select>\n";
        }
    }

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

function show_dropdown_option_title($db,$optionid,$options=array())
{
    $html_print="";

    if( !empty($optionid) )
    {
        $query_option_where=" WHERE optionid='".$optionid."' ";
        $query_option_order="";
        $query_option_limit=" LIMIT 1 ";
        $query_option=QUERIES::query_select_dropdowns_options($db,$query_option_where);
        $results_option=$db->query($query_option['query']);
        $count_option=$db->numrows($results_option);
        $row_option=$db->mysql_array($results_option);
        $row_option=UTILS::html_decode($row_option);
        $options_row_text=array();
        $options_row_text['search']=$options['search'];
        $option_title=UTILS::row_text_value($db,$row_option,"title",$options_row_text);

        $html_print.=$option_title;
    }

    if( $options['html_return'] ) return $html_print;
    else print $html_print;
}

?>
