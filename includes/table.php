<?php
/**
 * Class to create automated tables
 *
 * Based on row data given class created html tables.
 *
 * @author Pauls Daugerts <pauls.daugerts@gmail.com>
 * @copyright 2013 Pauls Daugerts
 * @license http://www.php.net/license/3_01.txt PHP License 3.01
 */

class Table
{
    private $_db;
    private $_options;
    private $_row;
    private $_row_item;
    private $_images;
    private $_paintid;
    private $_id;
    private $_sort_by;
    private $_expandable=0;
    private $_highlight="";
    private $_onheaderclick="";
    private $_cols="";
    private $_html_return=0;
    private $_html_class;
    private $_class;
    private $_desc_text;

    public function __construct($db,$row,$options)
    {
        $this->_db=$db;
        $this->_options=$options;
        $this->_row=$row['rows'];
        $this->_item_row=$row['item_row'];
        $this->_images=$row['images'];
        $this->_id=$options['options']['id'];
        $this->_paintid=$options['options']['paintid'];
        $this->_html_class=$options['options']['html_class'];
        $this->_sort_by=$options['options']['sort_by'];
        if( $options['options']['expandable']!="" ) $this->_expandable=$options['options']['expandable'];
        $this->_highlight=$options['options']['highlight'];
        $this->_onheaderclick=$options['options']['onheaderclick'];
        $this->_cols=$options['cols'];
        $this->_class=$options['options']['class'];
        $this->_desc_text=$options['options']['desc_text'];
        if( $options['options']['html_return']!="" ) $this->_html_return=$options['options']['html_return'];
    }

    public function expand_list_highlight ($_content, $_search_string)
    {
        $_words=explode(" ", $_search_string);
        if ($_words == null) return $_content;
        if (gettype($_content) == 'string')
        {
            /*
            $patterns = array();
            foreach ($_words as $w) $patterns[] = '/' . $w . '/';
            $replacement = '<span class="highlight">' . "$0" . '</span>';
            return preg_replace($patterns, $replacement, $_content);
            */
            foreach( $_words as $search_word )
            {
                if( !empty($search_word) )
                {
                    $search_word=UTILS::strip_slashes_recursive($search_word);

                    $options_search_found_keywords=array();
                    $options_search_found_keywords['search']=$search_word;
                    $options_search_found_keywords['text']=$_content;
                    $options_search_found_keywords['limit']=14;
                    $options_search_found_keywords['strip_tags']=1;
                    $options_search_found_keywords['a_name']=1;
                    $_content=UTILS::show_search_found_keywords($db,$options_search_found_keywords);

                    /*
                    $replaced_search_word="";
                    //if( !$values['a_name'] ) $replaced_search_word.="<a name='".$search_word."' class='h1i1g1h1l1i1g1h1t1'>";
                        $replaced_search_word.="<span class='f1o1u1n1d3'>$1</span>";
                    //if( !$values['a_name'] ) $replaced_search_word.="</a>";
                    $search_word=str_replace("/","\/",$search_word);
                    $_content=preg_replace('/('.$search_word.')/i', $replaced_search_word, $_content);
                    */

                }
            }
        }
        return $_content;
    }


    public function draw_table()
    {
        $html_print="";

        # desc text on top
        if( !empty($this->_desc_text) )
        {
            $html_print.="<p class='".$this->_class['p-section-desc-text']."'>".$this->_desc_text."</p>";
        }
        # END

        # table
        $table = '<table class="expandlist" cellpadding="1" cellspacing="1" border="0" id="' . $this->_id . '" >' . "\n";

        # thead
        $table .= '<thead>' . "\n";
            $table .= '<tr>' . "\n";
                $col="";
                foreach ($this->_cols as $c_key => $c_value)
                {
                    # sort icon
                    $sort_icon = '';
                    if ($c_value['sortable'] and (isset($this->_sort_by)) and (($this->_sort_by[0] == $c_key)))
                    {
                        if ($this->_sort_by[1] > 0) $sort_icon = '<span class="sort_up"></span>';
                        if ($this->_sort_by[1] < 0) $sort_icon = '<span class="sort_down"></span>';
                    }

                    # adding gap - divider border
                    if( $col != '' )
                    {
                        $col .= '<th class="gap" style="width:1px;"></th>';
                    }

                    # th header col title
                    if( $c_value['sortable'] ) $data_sortable="1";
                    else $data_sortable="0";
                    $col .= '<th style="width:' . $c_value['width'] . ';" '.$onclick.' data-sortable="'.$data_sortable.'" >';
                        $col .= $c_value['caption'] . $sort_icon;
                    $col .= '</th>' . chr(10);
                }
                $table .= $col;
            $table .= '</tr>'. "\n";
        $table .= '</thead>'. "\n";

        # tbody
        $table .= "\n" . '<tbody>';
        //print_r($this->_row);
        //foreach ($this->_row as $d_key => $d_value)
        for($i=0;$i<count($this->_row);$i++)
        {
            //$html_print.=$d_key."-".$d_value;
            //print_r($d_value);
            $col = '';
            $ii=0;
            $tr_info_text = "";
            //print_r($this->_row[$i]);
            foreach ($this->_cols as $c_key => $c_value)
            {
                # if info text provided showing info row after this item
                if( isset($this->_row[$i]['info_text_row']) && $ii==0 )
                {
                    $count_columns=count($this->_cols);
                    $colspan=$count_columns+($count_columns-1);
                    $tr_info_text = "\n" . '<tr class="tr-info-text"><td colspan="'.$colspan.'">' . $this->_row[$i]['info_text_row'] . '</td></tr>' . chr(10);
                    //print $this->_row[$i]['info_text_row'];
                    //print count($this->_cols);
                    //unset($this->_row[$i]['row'][$c_value['col']]['info_text_row']);
                }

                # if expandlable
                if( $ii==0  && $this->_expandable>0 )
                {
                    $html_col=$this->_row[$i]['row'][$c_value['col']];

                        /*
                        # if essay section create overlay content
                        $info=UTILS::row_text_value2($this->_db,$this->_item_row[$i]['row'],"info");
                        if( $this->_item_row[$i]['row']['books_catid']==19 && !empty($info) )
                        {
                            $expandable_content="<div class='div-book-overlay-content overlay-book-".$this->_item_row[$i]['row']['id']."'>";
                                $options_div_text=array();
                                $options_div_text['class']['div_text_wysiwyg']="div-book-overlay-content-wysiwyg";
                                $expandable_content.=$this->_html_class->div_text_wysiwyg($db,$info,$options_div_text);
                            $expandable_content.="</div>";
                        }
                        # else print expandable content
                        else
                        {
                        */
                            # if expandable get expandable part data
                            $options_table_expand=array();
                            $options_table_expand['highlight']=$this->_highlight;
                            $options_table_expand['item_row']=$this->_item_row[$i];
                            $options_table_expand['images']=$this->_images[$i];
                            $options_table_expand['html_class']=$this->_html_class;
                            $options_table_expand['paintid']=$this->_paintid;
                            $options_table_expand['class']=$this->_class;
                            $options_table_expand['html_return']=$this->_html_return;
                            $table_expand = new Table_expand($this->_db,$options_table_expand);
                            if( $this->_expandable==1 ) $table_expand->_expandable_content=$table_expand->table_expand_data_literature();
                            $expandable_content=$table_expand->table_expand();
                            //$expandable_content=$this->_item_row[$i]['row']['books_catid'];
                            # END if expandable get expandable part data
                        //}

                        $html_col.=$expandable_content;

                    $this->_row[$i]['row'][$c_value['col']]=$html_col;
                }
                # END if expandlable

                # onclick url
                if( !empty($this->_row[$i]['url']) )
                {
                    $window_open='var win=window.open(\''.$this->_row[$i]['url'].'\',\'_blank\');win.focus();';
                    $window_location='window.location=\''.$this->_row[$i]['url'].'\';';
                    $onclick='onclick="if( second_click==0 ){if(event.metaKey){'.$window_open.'}else{'.$window_location.'}}else{second_click=0;}"';
                    $onclick2='onclick="second_click=1;'.$window_open.'"';
                }
                else
                {
                    $onclick="";
                    $onclick2="";
                }
                # END

                if( $col != '' ) $col .= "\n" . '<td class="gap" contextmenu="table-item-menu'.$i.'" ></td>';
                $col .= "\n" . '<td '.$onclick.' class="'.$c_value['content_align'].'" contextmenu="table-item-menu'.$i.'">';
                    # context menu
                    if( $ii==0 && !empty($onclick2) )
                    {
                        $col .="<menu type='context' id='table-item-menu".$i."'>";
                            $col .="<menuitem label='Open Link in New Tab' ".$onclick2." ></menuitem>";
                        $col .="</menu>";
                    }
                    # END context menu
                        $col .= $this->_row[$i]['row'][$c_value['col']];
                $col .= "\n" . '</td>';

                $ii++;
            }

            $table .= "\n" . '<tr data-tr="'.$this->_item_row[$i]['row']['id'].'" >' . $col . '</tr>' . chr(10);

            $table .= $tr_info_text;
        }

        $table .= "\n" . '</tbody>';

        # table end
        $table .= "\n" . '</table>';

        $table .= "\n" . '<script type="text/javascript">';
            $table .= "\n" . 'var second_click=0;';
            unset($this->_options['options']['html_class']);
            $table .="\n" . 'document.getElementById("'. $this->_options['options']['id'] . '").options = ' . json_encode($this->_options['options']).';';

            //$table .= "\n" . '$(window).keydown(function (e){';
                //$table .= "\n" . 'if (e.metaKey) console.debug("key");';
            //$table .= "\n" . '});';

            $table.="(function($) {";
                $table.="$(document).ready(";
                    $table.="function()";
                    $table.="{";

                        $table.="$('.a-read-essay').fancybox({";
                            $table.="type: 'inline',";
                            $table.="padding: 0,";
                            $table.="helpers : {";
                                $table.="overlay: {";
                                    $table.="css : {";
                                        //$table.="'background' : 'rgba(58, 42, 45, 2)'";
                                    $table.="}";
                                $table.="}";
                            $table.="},";
                            //$table.="content: overlay_content,";
                            $table.="beforeShow: function () {";
                                // Disable right click
                                $table.="$.fancybox.wrap.bind('contextmenu', function (e) {";
                                    $table.="return false;";
                                $table.="});";
                                // Disable drag
                                $table.="$.fancybox.wrap.bind('dragstart', function (e) {";
                                    $table.="return false;";
                                $table.="});";
                            $table.="}";
                        $table.="});";

                    $table.="}";
                $table.=");";
            $table.="})(jQuery);";

        $table .= "\n" . '</script>';

        $html_print.=$table;

        if( $this->_html_return ) return $html_print;
        else print $html_print;
    }

}



class Table_Data
{
    private $_db;
    private $_options;
    private $_sort;
    private $_sort_by;
    private $_highlight="";
    private $_html_class;
    private $_html_return=0;

    public function __construct($db,$options)
    {
        $this->_db=$db;
        $this->_options=$options;
        $this->_sort=$options['sort'];
        $this->_highlight=$options['highlight'];
        $this->_html_class=$options['html_class'];
        $this->_html_return=$options['html_return'];

        foreach ($this->_options['cols'] as $c_key => $c_value)
        {
            if( $c_value['col']==$this->_sort[0] )
            {
                $this->_sort_by=$c_value['sort_col'];
                if( $this->_sort[1]<0 ) $this->_sort_how="DESC";
                else $this->_sort_how="ASC";
            }
        }
    }

    public function table_data_auctions($options=array())
    {
        $options_table = array();
        $options_table = array(
            'options'       =>  array(
                'highlight'         =>  $options['higlight'],                     // highlight patterns in content
            ));
        $table = new Table($this->_db, $rows['rows'], $options_table);

        $_return=array();

        $query_columns_saleshistory=QUERIES::query_sale_history($this->_db);
        $query_columns_auctions=QUERIES::query_auction_houses($this->_db);
        $query_columns_paintings=QUERIES::query_painting($this->_db);
        $query_sales=" SELECT ".$query_columns_saleshistory['query_columns'].",".$query_columns_auctions['query_columns'].", ah.cityid AS ahcityid, ah.countryid AS ahcountryid,".$query_columns_paintings['query_columns']." ";

        $query_sales.="
        FROM
          ".TABLE_SALEHISTORY." s
        LEFT JOIN
          ".TABLE_AUCTIONHOUSE." ah ON s.ahID=ah.ahID
        LEFT JOIN
          ".TABLE_RELATIONS." r1 ON r1.typeid1 = 1 and r1.typeid2 = 2 and r1.itemid2 = s.saleID
        LEFT JOIN
          ".TABLE_RELATIONS." r2 ON r2.typeid2 = 1 and r2.typeid1 = 2 and r2.itemid1 = s.saleID
        LEFT JOIN
          ".TABLE_PAINTING." p ON ( r1.itemid1=p.paintID OR r2.itemid2=p.paintID )";

        $query_sales.="
        WHERE
            p.enable=1
        ";

        # for search page
        if( count($options['auctionhouses'])>0 || !empty($options['search']) )
        {
            $query_sales.=" AND ( ";
            $i_sales=0;
            foreach( $options['auctionhouses'] as $auctionhouseid )
            {
                $i_sales++;
                if( $i_sales==1 ) $query_sales.=" ( ";
                else $query_sales.=" OR ";
                $query_sales.=" s.ahID='".$auctionhouseid."' ";
                if( $i_sales==count($options['auctionhouses']) ) $query_sales.=" ) ";
            }
            if( count($options['auctionhouses'])>0 ) $query_sales.=" OR ";
            $query_sales.=$options['query_match_sales_history'];
            $query_sales.=" ) ";
            $query_sales.=" ORDER BY ".$this->_sort_by." ".$this->_sort_how.", ah.house ASC, s.lotNo+0 ASC, p.titleEN ";
        }
        # for NEWS - RESULTS page
        elseif( $options['results'] )
        {
            $query_sales.=" AND s.news_auction=1 AND ( DATEDIFF(NOW(),s.saleDate)>=1 OR s.soldFor!='' OR s.soldForUSD!='' OR s.boughtIn=1 ) AND DATEDIFF(NOW(),s.saleDate)<=186 AND s.enable=1 ";
            $query_sales.=" ORDER BY ".$this->_sort_by." ".$this->_sort_how.", ah.house ASC, s.lotNo+0 ASC, p.titleEN ";
        }
        else
        {
            $query_sales.=" AND s.news_auction=1 AND s.soldFor='' AND s.soldForUSD='' AND s.boughtIn!=1 AND DATEDIFF(NOW(),s.saleDate)<1 AND s.enable=1";
            $query_sales.=" ORDER BY ".$this->_sort_by." ".$this->_sort_how.", ah.house ASC, s.lotNo+0 ASC, p.titleEN ";
        }
        $results=$this->_db->query($query_sales);
        if( !empty($options['limit']) )
        {
            $query_sales.=$options['limit'];
        }
        $results_limit=$this->_db->query($query_sales);
        //if( $_SESSION['debug_page'] ) print $query_sales['query_without_limit'];
        //if( $_SESSION['debug_page'] ) print $query_sales;
        $_return['query_sales']=$query_sales;
        $count=$this->_db->numrows($results);
        $count_limit=$this->_db->numrows($results_limit);
        $_return['count']=$count;
        $_return['count_limit']=$count_limit;
        //print $count;

        $i=0;
        while( $row_sale=$this->_db->mysql_array($results_limit,0) )
        {
            $row_sale=UTILS::html_decode($row_sale);
            //if( $_SESSION['debug_page'] ) print_r($row_sale);
            $number=0;

            # artwork info
            /*
            $query_paintings_relations="SELECT typeid1,itemid1,typeid2,itemid2 from ".TABLE_RELATIONS." WHERE ( typeid1=2 AND itemid1='".$row_sale['saleID']."' AND typeid2=1 ) OR ( typeid2=2 AND itemid2='".$row_sale['saleID']."' AND typeid1=1 ) ORDER BY sort ASC, relationid DESC LIMIT 1 ";
            $results_paintings_relations=$this->_db->query($query_paintings_relations);
            $row_paintings_relations=$this->_db->mysql_array($results_paintings_relations);
            $paintid=UTILS::get_relation_id($this->_db,"1",$row_paintings_relations);
            $query_where_painting=" WHERE paintID='".$paintid."' ANd enable=1 ";
            $query_painting=QUERIES::query_painting($this->_db,$query_where_painting);
            $results_painting=$this->_db->query($query_painting['query']);
            $row_painting=$this->_db->mysql_array($results_painting);
            */

            # artwork url
            $values=array();
            $values['artworkid']=$row_sale['artworkID'];
            $values['paintid']=$row_sale['paintID'];
            $painting_url=UTILS::get_painting_url($this->_db,$values);
            $painting_url['url'].="/?referer=";
            if( $options['results'] ) $painting_url['url'].="news-results";
            else $painting_url['url'].="news-upcoming";
            $_return['rows'][$i]['url']=$painting_url['url'];

            # 0 artwrok image
            $_return['rows'][$i]['row'][$number]="<div class='thumb_placeholder'></div>";
            $_return['rows'][$i]['row'][$number].="<div class='thumb'>";
            $results_related_image=$this->_db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=1 AND itemid2='".$row_sale['paintID']."' ) OR ( typeid2=17 AND typeid1=1 AND itemid1='".$row_sale['paintID']."' ) ORDER BY sort ASC, relationid DESC LIMIT 1 ");
            while( $row_related_image=$this->_db->mysql_array($results_related_image) )
            {
                $imageid=UTILS::get_relation_id($this->_db,"17",$row_related_image);
                //$src=DATA_PATH_DATADIR."/images_new/cache2/".$imageid.".jpg";
                //$src="/images/size_s__imageid_".$imageid.".jpg";
                $src=DATA_PATH_DATADIR."/images_new/small/".$imageid.".jpg";
                $_return['rows'][$i]['row'][$number].="<img src='".$src."' alt='' />";
            }
            $_return['rows'][$i]['row'][$number].="</div>";
            $number++;

            # 1 artwrok
            $_return['rows'][$i]['row'][$number]="<p class='p-news-painting-title'>";
                $values_title=array();
                $values_title['row']=$row_sale;
                $values_title['return']=1;
                $_return['rows'][$i]['row'][$number].=UTILS::get_painting_title($this->_db,$values_title);
            $_return['rows'][$i]['row'][$number].="</p>";
            if( !empty($row_sale['year']) ) $_return['rows'][$i]['row'][$number].=$row_sale['year']."<br />";
            if( !empty($row_sale['size']) ) $_return['rows'][$i]['row'][$number].=$row_sale['size']."<br />";
            if( !empty($row_sale['mID']) )
            {
                $results_media=$this->_db->query("SELECT * FROM ".TABLE_MEDIA." WHERE mID='".$row_sale['mID']."'");
                $row_media=$this->_db->mysql_array($results_media);
                $row_media=UTILS::html_decode($row_media);
                $media=UTILS::row_text_value2($this->_db,$row_media,"medium");
                $_return['rows'][$i]['row'][$number].=$media."<br />";
            }
            if( !empty($row_sale['number']) )
            {
                $artwork_type=UTILS::get_artwork_info($this->_db,$row_sale['artworkID']);
                $_return['rows'][$i]['row'][$number].=$artwork_type['nr'].": ".$row_sale['number']."<br />";
            }
                # admin edit link
                $options_admin_edit=array();
                $options_admin_edit['html_return']=$this->_html_return;
                $admin_edit=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$row_sale['paintID'],$options_admin_edit);
                $_return['rows'][$i]['row'][$number].="&nbsp;".$admin_edit;
            $number++;

            # insert info text that folowing artworks are from the same lot
            //print "lot_no_check=_".$lot_no_check."_ | lotNo=_".$row_sale['lotNo']."_ | lot_no_previous=_".$lot_no_previous."_ | ahID=_".$row_sale['ahID']."_ | ahid_previous=_".$ahid_previous."_<br />";
            if( $lot_no_previous==$row_sale['lotNo'] && $ahid_previous==$row_sale['ahID'] && !empty($lot_no_previous) && !empty($row_sale['lotNo']) )
            {
                if( $lot_no_set!=$row_sale['lotNo'] )
                {
                    $_return['rows'][($i-1)]['info_text_row']=LANG_TABLE_FURTHER_ARTWORKS_IN_LOT;
                }
                $lot_no_set=$row_sale['lotNo'];
            }

            if( empty($row_sale['lotNo']) ) $lot_no_previous="not_available_".UTILS::r_string(3);
            else
            {
            	$lot_no_previous=$row_sale['lotNo'];
            	//$ahid_previous=$row_sale['ahID'];
            }

            if( empty($row_sale['lotNo']) )
            {
            	$lot_no_check=$lot_no_previous;
            	//$ahid_previous=$row_sale['ahID'];
            }

            /*
            if( empty($ahid_previous) )
            {
            	$ahid_previous=$row_sale['ahID'];
            }
            */

            # if artworks from the same lot do not repeat sale info and add info text on top
            //if( ( $lot_no_check!=$row_sale['lotNo'] && !empty($row_sale['lotNo']) ) || empty($lot_no_check) )
            //if( $lot_no_check!=$row_sale['lotNo'] && !empty($row_sale['lotNo']) )
            //if( ( $lot_no_check!=$row_sale['lotNo'] && $ahid_previous==$row_sale['ahID'] ) || ( $lot_no_check==$row_sale['lotNo'] && $ahid_previous!=$row_sale['ahID'] ) )
            if( $lot_no_check==$row_sale['lotNo'] && $ahid_previous==$row_sale['ahID'] && !empty($ahid_previous) )
            {
				//if( $_SESSION['debug_page'] ) print "lot_no_check=_".$lot_no_check."_ | lotNo=_".$row_sale['lotNo']."_ | lot_no_previous=_".$lot_no_previous."_ | ahID=_".$row_sale['ahID']."_ | ahid_previous=_".$ahid_previous."_<br />";
            }
            else
            {
                //print "here-".$row_sale['saleID'];
                if( empty($row_sale['lotNo']) ) $lot_no_check=$lot_no_previous;
                else $lot_no_check=$row_sale['lotNo'];
                //$ahid_previous=$row_sale['ahID'];

                # 2 auction house
                $values_location=array();
                $values_location['row']['cityid']=$row_sale['ahcityid'];
                $values_location['row']['countryid']=$row_sale['ahcountryid'];
                $location=location($this->_db,$values_location);
                $auction_house_name=$row_sale['house'];
                $auction_house_name.=",".$location;
                $auction_house_name=preg_replace('/,/', '<br />', $auction_house_name, 1);
                $_return['rows'][$i]['row'][$number]=$table->expand_list_highlight($auction_house_name,$this->_highlight);
                    # admin edit link
                    $options_admin_edit=array();
                    $options_admin_edit['html_return']=$this->_html_return;
                    $admin_edit=UTILS::admin_edit("/admin/auctions-sales/auctionhouse/edit/?auctionhouseid=".$row_sale['ahID'],$options_admin_edit);
                    $_return['rows'][$i]['row'][$number].="&nbsp;".$admin_edit;
                $number++;

                # 3 Lot
                if( !$options['results'] )
                {
                    $_return['rows'][$i]['row'][$number]=$row_sale['lotNo'];
                    $number++;
                }

                # 4 Estimate
                $_return['rows'][$i]['row'][$number]="";

                    # prepare estimate 1
                    $currency1=UTILS::get_currency($this->_db,$row_sale['estCurrID'],"currency");
                    $estimate1="";
                    if( !empty($row_sale['estLow']) ) $estimate1.=$currency1." ".UTILS::display_numbers($db,$row_sale['estLow']);
                    if( !empty($row_sale['estLow']) && !empty($row_sale['estHigh']) ) $estimate1.=" &#8211; ";
                    if( !empty($row_sale['estHigh']) ) $estimate1.=UTILS::display_numbers($db,$row_sale['estHigh']);
                    if( empty($estimate1) ) $estimate1="";
                    # prepare estimate 2
                    $currency2=UTILS::get_currency($this->_db,1,"currency");
                    $estimate2="";
                    if( !empty($row_sale['estLowUSD']) ) $estimate2.=$currency2." ".UTILS::display_numbers($db,$row_sale['estLowUSD']);
                    if( !empty($row_sale['estLowUSD']) && !empty($row_sale['estHighUSD']) ) $estimate2.=" &#8211; ";
                    if( !empty($row_sale['estHighUSD']) ) $estimate2.=UTILS::display_numbers($db,$row_sale['estHighUSD']);
                    if( empty($estimate2) ) $estimate2="";

                if( !empty($row_sale['tbannounced']) ) $_return['rows'][$i]['row'][$number].=LANG_SALES_HISTORY_TB_ANNOUNCED;
                elseif( !empty($row_sale['upon_request']) ) $_return['rows'][$i]['row'][$number].=LANG_SALES_HISTORY_UPON_REQUEST;
                elseif( !empty($estimate1) ) $_return['rows'][$i]['row'][$number].=$estimate1;
                else $_return['rows'][$i]['row'][$number].=$estimate2;
                if( !empty($estimate2) && !empty($estimate1) ) $_return['rows'][$i]['row'][$number].="<br />".$estimate2;
                    # admin edit link
                    $options_admin_edit=array();
                    $options_admin_edit['html_return']=$this->_html_return;
                    $admin_edit=UTILS::admin_edit("/admin/auctions-sales/saleHistory/edit/?saleid=".$row_sale['saleID'],$options_admin_edit);
                    $_return['rows'][$i]['row'][$number].="&nbsp;".$admin_edit;
                $number++;

                # 5 Sold For
                if( $options['results'] )
                {
                    $_return['rows'][$i]['row'][$number]="";

                    # sale type
                    $saletype="";
                    if( (!empty($row_sale['soldFor']) && !empty($row_sale['soldForCurrID'])) || !empty($row_sale['soldForUSD']) )
                    {
                        if( !empty($row_sale['saleType']) && $row_sale['saleType']!=3 )
                        {
                            if( $row_sale['saleType']==1 ) $saletype.=LANG_PREMIUM;
                            elseif( $row_sale['saleType']==2 ) $saletype.=LANG_HAMMER;
                            //elseif( $row_sale['saleType']==3 ) $saletype.=LANG_UNKNOWN;
                        }
                    }
                    else $saletype="";

                    # currency
                    $currency1=UTILS::get_currency($this->_db,$row_sale['soldForCurrID'],"currency");
                    $soldfor1=$currency1." ".UTILS::display_numbers($db,$row_sale['soldFor']);
                    $currency2=UTILS::get_currency($this->_db,1,"currency");
                    $soldfor2=$currency2." ".UTILS::display_numbers($db,$row_sale['soldForUSD']);

                    # sold for other currencies then USD $
                    if( !$row_sale['boughtIn'] && !$row_sale['withdrawn'] )
                    {
	                    if( !empty($row_sale['soldFor']) )
	                    {
	                        if( !empty($row_sale['soldFor']) ) $_return['rows'][$i]['row'][$number].=$soldfor1;
	                        else $_return['rows'][$i]['row'][$number].=$soldfor2;
	                    }
	                    elseif( !empty($row_sale['soldForUSD']) )
	                    {
	                        $_return['rows'][$i]['row'][$number].=$soldfor2;
	                        if( empty($row_sale['soldFor']) && empty($row_sale['soldForCurrID']) )
	                        {
	                            $_return['rows'][$i]['row'][$number].="<br />".$saletype;
	                        }
	                    }
                	}
                    else
                    {
                    	if( $row_sale['withdrawn'] ) $_return['rows'][$i]['row'][$number].=LANG_WITHDRAWN;
                        else $_return['rows'][$i]['row'][$number].=LANG_BOUGHT_IN;
                    }

                    # sold for USD $
                    if( !empty($row_sale['soldForUSD']) && !empty($row_sale['soldFor']) && !$row_sale['boughtIn'] )
                    {
                        $_return['rows'][$i]['row'][$number].="<br />".$soldfor2;
                        if( empty($row_sale['soldFor']) && empty($row_sale['soldForCurrID']) )
                        {
                            $_return['rows'][$i]['row'][$number].="<br />".$saletype;
                        }
                    }

                    # sale type
                    if( empty($row_sale['soldForUSD']) || (!empty($row_sale['soldForUSD']) && (!empty($row_sale['soldFor']) && !empty($row_sale['soldForCurrID']))) && !$row_sale['boughtIn'] )
                    {
                        $_return['rows'][$i]['row'][$number].="<br />".$saletype;
                    }

                    $number++;
                }

                # 5 Date
                $_return['rows'][$i]['row'][$number]=UTILS::date_month($this->_db,UTILS::row_text_value($db,$row_sale,"sale_date_short"),1);
                $number++;

            }
            /*
            else
            {
            	if( $_SESSION['debug_page'] ) print "lot_no_check=_".$lot_no_check."_ | lotNo=_".$row_sale['lotNo']."_ | lot_no_previous=_".$lot_no_previous."_ | ahID=_".$row_sale['ahID']."_ | ahid_previous=_".$ahid_previous."_<br />";
            }
            */

            $ahid_previous=$row_sale['ahID'];

            $i++;
        }

        return $_return;
    }

    public function table_data_literature($options=array())
    {
        $options_table = array();
        $options_table = array(
            'options'       =>  array(
                'highlight'         =>  $options['higlight'],                     // highlight patterns in content
            ));
        $table = new Table($this->_db, $rows['rows'], $options_table);

        $_return=array();

        $query_columns_literature=QUERIES::query_books($this->_db);
/*
        $query_literature=" SELECT ".$query_columns_literature['query_columns'].", relationid ";

        $query_literature.="
        FROM
          ".TABLE_SALEHISTORY." s
        LEFT JOIN
          ".TABLE_AUCTIONHOUSE." ah ON s.ahID=ah.ahID
        LEFT JOIN
          ".TABLE_RELATIONS." r1 ON r1.typeid1 = 1 and r1.typeid2 = 2 and r1.itemid2 = s.saleID
        LEFT JOIN
          ".TABLE_RELATIONS." r2 ON r2.typeid2 = 1 and r2.typeid1 = 2 and r2.itemid1 = s.saleID
        LEFT JOIN
          ".TABLE_PAINTING." p ON ( r1.itemid1=p.paintID OR r2.itemid2=p.paintID )";
*/

        if( !empty($options['typeid']) && !empty($options['itemid']) )
        {
            $query_literature_relations="SELECT
                    ".$query_columns_literature['query_columns'].",
                    relationid
                FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b
                WHERE (
                    ( r.typeid1=".$options['typeid']." AND r.itemid1='".$options['itemid']."' AND r.typeid2=12 )
                    OR ( r.typeid2=".$options['typeid']." AND r.itemid2='".$options['itemid']."' AND r.typeid1=12 ) )
                    AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) )
                    AND b.enable=1 ";
            $query_literature_relations.=" ORDER BY b.books_catid ASC, ".$this->_sort_by." ".$this->_sort_how.", isnull ASC, b.author ASC ";
            if( $_SESSION['debug_page'] ) print $query_literature_relations;
            $results_literature=$this->_db->query($query_literature_relations);
        }
        else
        {
            $results_literature=$options['results'];
        }
        $count=$this->_db->numrows($results_literature);
        $_return['count']=$count;
        $i=0;
        while( $row_literature=$this->_db->mysql_array($results_literature) )
        {
            $row_literature=UTILS::html_decode($row_literature);
            $_return['item_row'][$i]['row']=$row_literature;
            $number=0;

            # 0 image
            $results_related_image=$this->_db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=17 AND typeid2=12 AND itemid2='".$row_literature['id']."' ) OR ( typeid2=17 AND typeid1=12 AND itemid1='".$row_literature['id']."' ) ORDER BY sort ASC, relationid DESC ");
            $count_image=$this->_db->numrows($results_related_image);
            $images_array=array();
            $ii=0;
            while( $row_related_image=$this->_db->mysql_array($results_related_image) )
            {
                $imageid=UTILS::get_relation_id($db,"17",$row_related_image);
                //$src_small="/images/size_xs__imageid_".$imageid.".jpg";
                $src_small=DATA_PATH_DATADIR."/images_new/xsmall/".$imageid.".jpg";
                //$src_medium="/images/size_m__imageid_".$imageid.".jpg";
                $src_medium=DATA_PATH_DATADIR."/images_new/medium/".$imageid.".jpg";
                //$href="/images/size_xl__imageid_".$imageid.".jpg";
                $href=DATA_PATH_DATADIR."/images_new/xlarge/".$imageid.".jpg";
                $images_array[$ii]['src_small']=$src_small;
                $images_array[$ii]['src']=$src_medium;
                $images_array[$ii]['href']=$href;
                $images_array[$ii]['imageid']=$imageid;
                $ii++;
            }


            if( empty($images_array[0]) && ( $row_literature['books_catid']==6 || $row_literature['books_catid']==10 || $row_literature['books_catid']==12 || $row_literature['books_catid']==13 || $row_literature['books_catid']==19 || $row_literature['books_catid']==1 || $row_literature['books_catid']==2 || $row_literature['books_catid']==3 || $row_literature['books_catid']==4 || $row_literature['books_catid']==5 || $row_literature['books_catid']==11 ) )
            {
                $class_img="img-no-border";
                if( $row_literature['books_catid']==6 || $row_literature['books_catid']==10 || $row_literature['books_catid']==12 || $row_literature['books_catid']==13 || $row_literature['books_catid']==19 )
                {
                    $src_small="/g/literature/icons/icon_literature_article.png";
                    $images_array[0]['src']="/g/literature/icons/icon_literature_article_large.png";
                    $images_array[0]['class']=$class_img;
                }
                else if( $row_literature['books_catid']==1 || $row_literature['books_catid']==2 || $row_literature['books_catid']==3 || $row_literature['books_catid']==4 || $row_literature['books_catid']==5 || $row_literature['books_catid']==11 )
                {
                    $src_small="/g/literature/icons/icon_literature_book.png";
                    $images_array[0]['src']="/g/literature/icons/icon_literature_book_large.png";
                    $images_array[0]['class']=$class_img;
                }
                else
                {
                    $src_small="/g/literature/empty.png";
                }
            }
            else
            {
                $class_img="";
                $src_small=$images_array[0]['src_small'];
                //$images_array[]['src']=
                //$src_medium="/images/size_m__imageid_".$images_array[0].".jpg";
                //$href="/images/size_xl__imageid_".$images_array[0].".jpg";
            }
            $_return['images'][$i]=$images_array;
            $html_img='<div class="thumb_placeholder"></div>';
            $html_img.='<div class="thumb">';
                $html_img.='<img class="'.$class_img.'" src="'.$src_small.'" alt="" title="'.LANG_LITERATURE_ENLARGE.'" />';
            $html_img.='</div>';

            $_return['rows'][$i]['row'][$number]=$html_img;
            $number++;

            # 2 title
            $_return['rows'][$i]['row'][$number]=$table->expand_list_highlight($row_literature['title'],$this->_highlight);
            $number++;

            # 3 author
                # admin edit link
                $options_admin_edit=array();
                $options_admin_edit['html_return']=$this->_html_return;
                $admin_edit=UTILS::admin_edit("/admin/books/edit/?bookid=".$row_literature['id'],$options_admin_edit);

            $_return['rows'][$i]['row'][$number]=$table->expand_list_highlight($row_literature['author'],$this->_highlight);
            $_return['rows'][$i]['row'][$number].="&nbsp;".$admin_edit;
            $number++;

            # 4 Category
            if( UTILS::in_array_multi("b.books_catid", $this->_options['cols']) )
            {
                $query_where_book_category=" WHERE books_catid='".$row_literature['books_catid']."' ";
                $query_book_category=QUERIES::query_books_categories($this->_db,$query_where_book_category,""," LIMIT 1 ");
                $results_category=$this->_db->query($query_book_category['query']);
                $row_category=$this->_db->mysql_array($results_category);
                $title_book_category=UTILS::row_text_value($this->_db,$row_category,"title");
                $_return['rows'][$i]['row'][$number]=$title_book_category;
                $number++;
            }

            # 5 Date
            $_return['rows'][$i]['row'][$number]=$row_literature['date_display_year'];
            $number++;

            $i++;
        }

        return $_return;
    }

}

class Table_expand
{
    private $_db;
    private $_options;
    private $_item_row;
    private $_images;
    private $_highlight="";
    public $_expandable_content="";
    private $_html_class;
    private $_paintid;
    private $_class;
    private $_html_return=0;

    public function __construct($db,$options)
    {
        $this->_db=$db;
        $this->_options=$options;
        $this->_item_row=$options['item_row'];
        $this->_images=$options['images'];
        $this->_highlight=$options['highlight'];
        $this->_expandable_content=$options['expandable_content'];
        $this->_html_class=$options['html_class'];
        $this->_paintid=$options['paintid'];
        $this->_class=$options['class'];
        $this->_html_return=$options['html_return'];
    }

    public function table_expand($options=array())
    {
        $html_expand_content="";
        $html_expand_content.='<div class="expand-box-wrapper">';
            $html_expand_content.='<div class="expand-box '.$this->_class['expand-box-exh'].'">';
                //$html_expand_content.=$this->_html_class->expand_list_expandbox_literature ($this->_item_row[$i], $_options, $this->_db,$this->_images[$i]);
                $html_expand_content.=$this->_expandable_content;
            $html_expand_content.="</div>"; # END expand-box
        $html_expand_content.="</div>"; # END expand-box-wrapper

        return $html_expand_content;
    }


    public function table_expand_data_literature($options=array())
    {

        $options_table = array();
        $options_table = array(
            'options'       =>  array(
                'highlight'         => $this->_highlight,                     // highlight patterns in content
            ));
        $table = new Table($this->_db, $rows['rows'], $options_table);

        //print_r($this->_item_row);
        //print_r($this->_item_row['row']['relationid']);

        # literature images
        if( !empty($this->_images[0]['href']) )
        {
            $href=" href='".$this->_images[0]['href']."' ";
            $class="fancybox-buttons";
        }
        else
        {
            $href="";
            $class="";
        }
        $s.='<div class="div-expand-box-images-container">';
            $src_medium=DATA_PATH."/images_new/medium/".$this->_images[0]['imageid'].".jpg";
            $file_exists=0;
            if( file_exists($src_medium) )
            {
                $file_exists=1;
                list($width, $height) = getimagesize($src_medium);
                $style='style="height:'.$height.'px;width:'.$width.'px;"';
            }
            $s.='<div '.$style.' class="div-expand-box-images">';
                if( !empty($this->_paintid) ) $data_fancybox_group=$this->_paintid."-".$this->_item_row['row']['id'];
                else $data_fancybox_group=$this->_item_row['row']['id'];
                $s.='<a '.$href.' class="'.$class.'" data-fancybox-group="'.$data_fancybox_group.'">';
                    if( $file_exists ) $s.='<img class="img-book-enlarge-icon" alt="" src="/g/zoom-icon.png">';
                    $s.='<img src="'.$this->_images[0]['src'].'" alt="" class="'.$this->_images[0]['class'].'" />';
                $s.='</a>';
                for($i=1;$i<count($this->_images);$i++)
                {
                    $s.="<a href='".$this->_images[$i]['href']."' class='fancybox-buttons' data-fancybox-group='".$data_fancybox_group."'>";
                        $s.="<img style='display:none;' src='/g/empty.png' alt='' />";
                    $s.="</a>";
                }
            $s.='</div>';
        $s.='</div>';
        # END literature images

        $s.='<div class="expand-box-details '.$this->_class['expand-box-details-exh'].'">';
            $s.='<table>';
                # title_periodical
                if( !empty($this->_item_row['row']['title_periodical']) )
                {
                    $s.='<tr>';
                        $s.='<td class="td-expand-box-title">';
                            $s.='<h3>'.LANG_LITERATURE_IN.'</h3>';
                        $s.='</td>';
                        $s.='<td>' . $this->_item_row['row']['title_periodical'] . '</td>';
                    $s.='</tr>';
                }
                # END title_periodical

                # Journals
                if( $this->_item_row['row']['books_catid']==6 )
                {
                    if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date']) || !empty($this->_item_row['row']['issue_no']) || !empty($this->_item_row['row']['article_pages_from']) || !empty($this->_item_row['row']['article_pages_till']) )
                    {
                        $s.='<tr>';
                            $s.='<td class="td-expand-box-title">';
                                $s.='<h3>';
                                    $s.=LANG_LITERATURE_ISSUE;
                                $s.='</h3>';
                            $s.='</td>';
                            $s.='<td>';

                                # date
                                if( !empty($this->_item_row['row']['link_date']) )
                                {
                                    if( !empty($this->_item_row['row']['link_date_year_full']) )
                                    {
                                        $s.=UTILS::date_month($this->_db,$this->_item_row['row']['link_date_year_full'])." ";
                                        $s.=$this->_item_row['row']['date_display_year'];
                                    }
                                    else
                                    {
                                        if( $_GET['lang']=="de" )
                                        {
                                            $this->_item_row['row']['date_display_de']=str_replace(" 00","",$this->_item_row['row']['date_display_de']);
                                            $this->_item_row['row']['date_display_de']=str_replace("00. ","",$this->_item_row['row']['date_display_de']);
                                            if( empty($this->_item_row['row']['date_display_de']) ) $s.=$this->_item_row['row']['date_display_year'];
                                            else $s.=UTILS::date_month($this->_db,$this->_item_row['row']['date_display_de']);
                                        }
                                        else
                                        {
                                            $this->_item_row['row']['date_display_en']=str_replace(" 00","",$this->_item_row['row']['date_display_en']);
                                            $this->_item_row['row']['date_display_en']=str_replace("00. ","",$this->_item_row['row']['date_display_en']);
                                            if( empty($this->_item_row['row']['date_display_en']) ) $s.=$this->_item_row['row']['date_display_year'];
                                            else $s.=UTILS::date_month($this->_db,$this->_item_row['row']['date_display_en']);
                                        }
                                    }
                                }
                                # END date
                                # volume
                                if( !empty($this->_item_row['row']['link_date']) ) $comma=", ";
                                else $comma="";
                                if( !empty($this->_item_row['row']['vol_no']) )
                                {
                                    $s.=$comma;
                                    $s.=LANG_LITERATURE_VOL." ".$this->_item_row['row']['vol_no'];
                                }
                                # END volume
                                # issue number
                                if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($this->_item_row['row']['issue_no']) ) $s.=$comma.LANG_LITERATURE_NO." ".$this->_item_row['row']['issue_no'];
                                # END issue number
                                # pages
                                if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date']) || !empty($this->_item_row['row']['issue_no'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($this->_item_row['row']['article_pages_from']) || !empty($this->_item_row['row']['article_pages_till']) )
                                {
                                    $s.=$comma;
                                    if( !empty($this->_item_row['row']['article_pages_from']) && !empty($this->_item_row['row']['article_pages_till']) )
                                    {
                                        $s.=LANG_LITERATURE_PAGESS." ";
                                        $s.=$this->_item_row['row']['article_pages_from']."&#8211;".$this->_item_row['row']['article_pages_till'];
                                    }
                                    else
                                    {
                                        $s.=LANG_LITERATURE_PAGES." ";
                                        $s.=$this->_item_row['row']['article_pages_from'];
                                    }
                                }
                                # END pages
                            $s.='</td>';
                        $s.='</tr>';
                    }
                }
                # END Journals

                # Newspaper Articles && Online Publications
                if( $this->_item_row['row']['books_catid']==12 || $this->_item_row['row']['books_catid']==13 || $this->_item_row['row']['books_catid']==19 )
                {
                    if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date']) || !empty($this->_item_row['row']['issue_no']) || !empty($this->_item_row['row']['article_pages_from']) || !empty($this->_item_row['row']['article_pages_till']) )
                    {
                        $tr_issue_info="";
                        $tr_issue_info.='<tr>';
                            $tr_issue_info.='<td class="td-expand-box-title">';
                                $tr_issue_info.='<h3>';
                                    $tr_issue_info.=LANG_LITERATURE_ISSUE;
                                $tr_issue_info.='</h3>';
                            $tr_issue_info.='</td>';
                            $tr_issue_info.='<td>';
                                $issue_info="";
                                # date
                                if( !empty($this->_item_row['row']['link_date']) )
                                {
                                    if( !empty($this->_item_row['row']['link_date_year_full']) )
                                    {
                                        $issue_info.=UTILS::date_month($this->_db,$this->_item_row['row']['link_date_year_full'])." ";
                                        $issue_info.=$this->_item_row['row']['date_display_year'];
                                    }
                                    else
                                    {
                                        if( $_GET['lang']=="de" )
                                        {
                                            $this->_item_row['row']['date_display_de']=str_replace(" 00","",$this->_item_row['row']['date_display_de']);
                                            $this->_item_row['row']['date_display_de']=str_replace("00. ","",$this->_item_row['row']['date_display_de']);
                                            //$issue_info.=UTILS::date_month($this->_db,$this->_item_row['row']['date_display_de']);
                                            if( empty($this->_item_row['row']['date_display_de']) ) $issue_info.=$this->_item_row['row']['date_display_year'];
                                            else $issue_info.=UTILS::date_month($this->_db,$this->_item_row['row']['date_display_de']);
                                        }
                                        else
                                        {
                                            $this->_item_row['row']['date_display_en']=str_replace(" 00","",$this->_item_row['row']['date_display_en']);
                                            $this->_item_row['row']['date_display_en']=str_replace("00. ","",$this->_item_row['row']['date_display_en']);
                                            if( !empty($this->_item_row['row']['date_display_en']) )
                                            {
                                                $issue_info.=UTILS::date_month($this->_db,$this->_item_row['row']['date_display_en']);
                                            }
                                            else $issue_info.=$this->_item_row['row']['date_display_year'];
                                        }
                                    }
                                }
                                # END date
                                # volume
                                if( !empty($this->_item_row['row']['link_date']) ) $comma=", ";
                                else $comma="";
                                if( !empty($this->_item_row['row']['vol_no']) )
                                {
                                    $issue_info.=$comma;
                                    $issue_info.=LANG_LITERATURE_VOL." ".$this->_item_row['row']['vol_no'];
                                }
                                # END volume
                                if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date'])  ) $comma=", ";
                                else $comma="";
                                if( !empty($this->_item_row['row']['issue_no']) )
                                {
                                    $issue_info.=$comma.LANG_LITERATURE_NO." ".$this->_item_row['row']['issue_no'];
                                }
                                if( !empty($this->_item_row['row']['vol_no']) || !empty($this->_item_row['row']['link_date']) || !empty($this->_item_row['row']['issue_no'])  )
                                {
                                    $comma=", ";
                                }
                                else $comma="";
                                if( !empty($this->_item_row['row']['article_pages_from']) || !empty($this->_item_row['row']['article_pages_till']) )
                                {
                                    $issue_info.=$comma;
                                    if( !empty($this->_item_row['row']['article_pages_from']) && !empty($this->_item_row['row']['article_pages_till']) )
                                    {
                                        $issue_info.=LANG_LITERATURE_PAGESS." ";
                                        $issue_info.=$this->_item_row['row']['article_pages_from']."&#8211;".$this->_item_row['row']['article_pages_till'];
                                    }
                                    else
                                    {
                                        $issue_info.=LANG_LITERATURE_PAGES." ";
                                        $issue_info.=$this->_item_row['row']['article_pages_from'];
                                    }
                                }
                                $tr_issue_info.=$issue_info;
                            $tr_issue_info.='</td>';
                        $tr_issue_info.='</tr>';
                        if( !empty($issue_info) ) $s.=$tr_issue_info;
                    }
                }
                # END Newspaper Articles && Online Publications

                # if NOT Journals, Newspaper Articles, Online Publication
                if( $this->_item_row['row']['books_catid']!=6 && $this->_item_row['row']['books_catid']!=12 && $this->_item_row['row']['books_catid']!=13 )
                {
                    # for essays(19) and theses(10) where there is no publisher do not show unknown
                    if( ( $this->_item_row['row']['books_catid']==10 || $this->_item_row['row']['books_catid']==19 ) && empty($this->_item_row['row']['publisher']) )
                    {

                    }
                    else
                    {
                        $s.='<tr>';
                            $s.='<td class="td-expand-box-title">';
                                $s.='<h3>';
                                    $s.=LANG_LIT_SEARCH_PUBBY;
                                $s.='</h3>';
                            $s.='</td>';
                            $s.='<td>';
                                if( !empty($this->_item_row['row']['publisher']) )
                                {
                                    $s.=$table->expand_list_highlight($this->_item_row['row']['publisher'], $this->_highlight);
                                }
                                else $s.=LANG_LIT_SEARCH_UNKNOWN;
                                if( !empty($this->_item_row['row']['date_display_year']) ) $s.=' ('.$this->_item_row['row']['date_display_year'].')';
                            $s.='</td>';
                        $s.='</tr>';
                    }
                }
                # END if NOT Journals, Newspaper Articles, Online Publication

                # Editor
                if( !empty($this->_item_row['row']['editor']) )
                {
                    $s.='<tr>';
                        $s.='<td class="td-expand-box-title">';
                            $s.='<h3>';
                                $s.=LANG_LIT_SEARCH_EDITOR;
                            $s.='</h3>';
                        $s.='</td>';
                        $s.='<td>';
                            $s.=$table->expand_list_highlight($this->_item_row['row']['editor'], $this->_highlight);
                        $s.='</td>';
                    $s.='</tr>';
                }
                # END Editor

                # if NOT empty coverid or NOT empty NOpages
                if( !empty($this->_item_row['row']['coverid']) || $this->_item_row['row']['NOpages']!=0 )
                {
                    $s.='<tr>';
                        $s.='<td class="td-expand-box-title"><h3>'.LANG_BOOKS_DETAILS.'</h3></td>';
                        $s.='<td>';
                            if( !empty($this->_item_row['row']['coverid']) )
                            {
                                //$values_cover=array();
                                //$values_cover['coverid']=$this->_item_row['row']['coverid'];
                                //$cover=UTILS::get_cook_cover($this->_db,$values_cover);
                                //$s.=$cover;

                                $i_cover=0;
                                $coverids=explode(",", $this->_item_row['row']['coverid']);
                                $cover="";
                                foreach( $coverids AS $coverid )
                                {
                                    $values_cover=array();
                                    $values_cover['coverid']=$coverid;
                                    if( $i_cover>0 ) $cover.=", ";
                                    $cover.=UTILS::get_cook_cover($this->_db,$values_cover);
                                    $i_cover++;
                                }
                                $s.=$cover;
                            }
                            if( $this->_item_row['row']['NOpages']!=0 )
                            {
                                if( !empty($this->_item_row['row']['coverid']) ) $s.=', ';
                                $s.=$this->_item_row['row']['NOpages']." ".LANG_LIT_SEARCH_PAGES;
                            }
                        $s.='</td>';
                    $s.='</tr>';
                }
                # END if NOT empty coverid or NOT empty NOpages

                # ISBNs
                $results_isbn=$this->_db->query("SELECT isbn,language,language2,language3 FROM ".TABLE_BOOKS_ISBN." WHERE bookID='".$this->_item_row['row']['id']."' ORDER BY id ASC");
                $count_isbn=$this->_db->numrows($results_isbn);
                if( $count_isbn>0 )
                {
                    $html_isbn="";
                    $html_language="";
                    $i=0;
                    while( $row_isbn=$this->_db->mysql_array($results_isbn) )
                    {
                        $i++;
                        if( $row_isbn['isbn']!="" )
                        {
                            $html_isbn.="<tr>";
                                $html_isbn.="<td class='td-expand-box-title td-expand-box-title-".$_GET['lang']."'><h3>";
                                    if( $this->_item_row['row']['books_catid']==6 || $this->_item_row['row']['books_catid']==20 || $this->_item_row['row']['books_catid']==12 ) $html_isbn.=LANG_LITERATURE_ISSN;
                                    else $html_isbn.=LANG_LITERATURE_ISBN;
                                $html_isbn.="</h3></td>";
                                $html_isbn.="<td>".$row_isbn['isbn']."</td>\n";
                            $html_isbn.="</tr>";
                        }

                        if( !empty($row_isbn['language']) || !empty($row_isbn['language2']) || !empty($row_isbn['language3']) )
                        {
                            $query_where_language=" WHERE languageid='".$row_isbn['language']."' ";
                            $query_language=QUERIES::query_books_languages($this->_db,$query_where_language);
                            $results_language=$this->_db->query($query_language['query']);
                            $row_language=$this->_db->mysql_array($results_language);
                            $row_language=UTILS::html_decode($row_language);
                            $title_language=UTILS::row_text_value($this->_db,$row_language,"title");
                            $query_where_language=" WHERE languageid='".$row_isbn['language2']."' ";
                            $query_language=QUERIES::query_books_languages($this->_db,$query_where_language);
                            $results_language=$this->_db->query($query_language['query']);
                            $row_language=$this->_db->mysql_array($results_language);
                            $row_language=UTILS::html_decode($row_language);
                            $title_language2=UTILS::row_text_value($this->_db,$row_language,"title");
                            $query_where_language=" WHERE languageid='".$row_isbn['language3']."' ";
                            $query_language=QUERIES::query_books_languages($this->_db,$query_where_language);
                            $results_language=$this->_db->query($query_language['query']);
                            $row_language=$this->_db->mysql_array($results_language);
                            $row_language=UTILS::html_decode($row_language);
                            $title_language3=UTILS::row_text_value($this->_db,$row_language,"title");

                            $count3=0;
                            if( !empty($row_isbn['language']) ) $count3++;
                            if( !empty($row_isbn['language2']) ) $count3++;
                            if( !empty($row_isbn['language3']) ) $count3++;

                            if( $count3==1 ) $language=LANG_BOOKS_LANGUAGE;
                            else $language=LANG_BOOKS_LANGUAGES;
                            if( $i==1 ) $html_language.="<tr>";
                                if( $i==1 ) $html_language.="<td class='td-expand-box-title'><h3>".$language."</h3></td>";
                                if( $i==1 ) $html_language.="<td>";
                                    if( !empty($row_isbn['language']) )
                                    {
                                        if( !empty($html_language) && $i!=1 ) $html_language.=", ";
                                        $html_language.=$title_language;
                                    }
                                    if( !empty($row_isbn['language2']) )
                                    {
                                        $html_language.=", ";
                                        $html_language.=$title_language2;
                                    }
                                    if( !empty($row_isbn['language3']) )
                                    {
                                        $html_language.=", ";
                                        $html_language.=$title_language3;
                                    }
                        }
                    }
                    if( !empty($html_language) )
                    {
                            $html_language.="</td>";
                        $html_language.="</tr>";
                    }
                    $s.=$html_isbn;
                    $s.=$html_language;
                }
                # END ISBNs

                # category
                $s.='<tr>';
                    $s.='<td class="td-expand-box-title">';
                        $s.='<h3>';
                            $s.=LANG_BOOKS_CATEGORY;
                        $s.='</h3>';
                    $s.='</td>';
                    $s.='<td>';
                        $books_category=UTILS::get_books_category($this->_db,$this->_item_row['row']['books_catid']);
                        $s.=$books_category;
                    $s.='</td>';
                $s.='</tr>';
                #END category

                # Notes
                $notes=UTILS::row_text_value2($this->_db,$this->_item_row['row'],"notes");
                if( !empty($notes) )
                {
                    $style_td_notes=' style="padding: 15px 0;" ';
                    $s.='<tr>';
                        $s.='<td class="td-expand-box-title" '.$style_td_notes.' ><h3>'.LANG_NOTES.'</h3></td>';
                        $s.='<td '.$style_td_notes.'>';
                            $s.="<div class='div_text_wysiwyg'>";
                                $s.=$table->expand_list_highlight($notes, $this->_highlight);
                            $s.='</div>';
                        $s.='</td>';
                    $s.='</tr>';
                }
                else
                {
                    $style_td_artwork=' style="padding-top:15px;" ';
                }
                # END Notes

                # IF FROM PAINTING THEN SHOW MENTIONED, Discussed ILLUSTARTED ETC.
                if( !empty($this->_paintid) )
                {
                    //$style_td_artwork=' style="padding-top:15px;" ';
                    $query_rel_lit_where=" WHERE relationid='".$this->_item_row['row']['relationid']."' ";
                    $query_rel_lit=QUERIES::query_relations_literature($this->_db,$query_rel_lit_where);
                    $results_rel_lit=$this->_db->query($query_rel_lit['query']);
                    $count_rel_lit=$this->_db->numrows($results_rel_lit);
                    if( $count_rel_lit )
                    {
                        $row_rel_lit=$this->_db->mysql_array($results_rel_lit,0);
                        if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['show_mentioned']) || !empty($row_rel_lit['show_discussed']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) )
                        {
                            $s.="<tr>\n";
                                $s.= "<td class='table_item_detail_text_tcol1' ".$style_td_artwork.">";
                                    $s.= LANG_ARTWORK_BOOKS_REL_ARTWORK;
                                $s.= "</td>";
                                $s.= "<td ".$style_td_artwork.">";
                        }

                        # MENTIONED
                        if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['show_mentioned']) )
                        {
                            $tmp=explode(",",$row_rel_lit['mentioned']);
                            $s.= LANG_ARTWORK_BOOKS_REL_MENTIONED;
                            if( empty($row_rel_lit['show_mentioned']) )
                            {
                                $s.= ": ";
                                if( count($tmp)>1 || stripos($row_rel_lit['mentioned'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                $s.= " ".$row_rel_lit['mentioned'];
                                if( $_GET['lang']=='zh' ) $s.= LANG_ARTWORK_BOOKS_REL_CHINESE;
                            }
                            $options_admin_edit=array();
                            $options_admin_edit['html_return']=$this->_html_return;
                            $s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$this->_item_row['row']['id'],$options_admin_edit);
                        }

                        # DISCUSSED
                        if( !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_discussed']) )
                        {
                            $tmp=explode(",",$row_rel_lit['discussed']);
                            if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['show_mentioned']) )
                            {
                                $s.= "\t<br />\n";
                            }
                            $s.= LANG_ARTWORK_BOOKS_REL_DISCUSSED;
                            if( empty($row_rel_lit['show_discussed']) )
                            {
                                $s.= ": ";
                                if( count($tmp)>1 || stripos($row_rel_lit['discussed'],"-") ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                $s.= " ".$row_rel_lit['discussed'];
                                if( $_GET['lang']=='zh' ) $s.= LANG_ARTWORK_BOOKS_REL_CHINESE;
                            }
                            $options_admin_edit=array();
                            $options_admin_edit['html_return']=$this->_html_return;
                            $s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$this->_item_row['row']['id'],$options_admin_edit);
                        }

                        # ILLUSTRATED
                        if( !empty($row_rel_lit['illustrated_bw']) || !empty($row_rel_lit['illustrated']) || !empty($row_rel_lit['show_illustrated']) || !empty($row_rel_lit['show_bw']) )
                        {
                            $tmp=explode(",",$row_rel_lit['illustrated_bw']);
                            $tmp2=explode(",",$row_rel_lit['illustrated']);
                            if( !empty($row_rel_lit['mentioned']) || !empty($row_rel_lit['discussed']) || !empty($row_rel_lit['show_mentioned']) || !empty($row_rel_lit['show_discussed']) )
                            {
                                $s.= "\t<br />\n";
                            }
                            if( empty($row_rel_lit['show_illustrated']) && empty($row_rel_lit['show_bw']) )
                            {
                                $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED;
                                $s.= ": ";
                                if( count($tmp)>1 || count($tmp2)>1 || stripos($row_rel_lit['illustrated_bw'],"-") || stripos($row_rel_lit['illustrated'],"-") || (!empty($row_rel_lit['illustrated_bw']) && !empty($row_rel_lit['illustrated'])) ) $s.= LANG_ARTWORK_BOOKS_REL_PP;
                                else $s.= LANG_ARTWORK_BOOKS_REL_P;
                                if( !empty($row_rel_lit['illustrated_bw']) )
                                {
                                    $s.= " ".$row_rel_lit['illustrated_bw'];
                                    if( $_GET['lang']=="zh" )
                                    {
                                        if( count($tmp)>1 || stripos($row_rel_lit['illustrated_bw'],"-") ) $s.= " ".LANG_ARTWORK_BOOKS_REL_P2;
                                        else $s.= " ".LANG_ARTWORK_BOOKS_REL_PP2;
                                    }
                                    $s.= " (".LANG_ARTWORK_BOOKS_REL_BW.")";
                                }
                                if( !empty($row_rel_lit['illustrated']) )
                                {
                                    if( !empty($row_rel_lit['illustrated_bw']) )
                                    {
                                        $s.= ", ";
                                    }
                                    $s.= " ".$row_rel_lit['illustrated'];
                                    if( $_GET['lang']=="zh" )
                                    {
                                        if( count($tmp)>1 || stripos($row_rel_lit['illustrated'],"-") ) $s.= " ".LANG_ARTWORK_BOOKS_REL_P2;
                                        else $s.= " ".LANG_ARTWORK_BOOKS_REL_PP2;
                                    }
                                    $s.= " (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                                }
                            }
                            # else if show illustrated or show black and white ticked
                            else
                            {
                                if( !empty($row_rel_lit['show_illustrated']) )
                                {
                                    $s.= LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_COLOUR.")";
                                }
                                if( !empty($row_rel_lit['show_bw']) )
                                {
                                    if( !empty($row_rel_lit['show_illustrated']) ) $s.="<br />";
                                    $s.=LANG_ARTWORK_BOOKS_REL_ILLUSTRATED." (".LANG_ARTWORK_BOOKS_REL_BW.")";
                                }
                            }
                            $options_admin_edit=array();
                            $options_admin_edit['html_return']=$this->_html_return;
                            $s.="&nbsp;".UTILS::admin_edit("/admin/relations/literature/edit/?relationid_lit=".$row_rel_lit['relationid_lit']."&bookid=".$this->_item_row['row']['id'],$options_admin_edit);
                        }
                        # END ILLUSTRATED
                    }
                }
                # END IF FROM PAINTING THEN SHOW MENTIONED, Discussed ILLUSTARTED ETC.

                # Read Article link
                if( !empty($this->_item_row['row']['link']) )
                {
                    $s.="<tr>";
                        $s.="<td colspan='2'>";
                            $values_url=array();
                            $values_url['bookid']=$this->_item_row['row']['id'];
                            $url_literature=UTILS::get_literature_url($this->_db,$values_url);
                            $href=$url_literature['url'];
                            if( !empty($url_literature['target']) ) $target="target='".$url_literature['target']."'";
                            else $target="";

                            # if its an essay say read essay
                            if( $this->_item_row['row']['books_catid']==19 )
                            {
                                $title=LANG_BOOKS_READ_ESSAY;
                                $title_attr=$href;
                                $class_section="a-read-essay";
                            }
                            else
                            {
                                $title=LANG_BOOKS_READ_ARTICLE;
                                $title_attr=LANG_BOOKS_READ_ARTICLE;
                                $class_section="";
                            }

                            $s.="<a href='".$href."' title='".$title_attr."' ".$target." class='".$class_section." a-literature-link a-literature-link-".$_GET['lang']."'>".$title."</a>";
                        $s.="</td>";
                    $s.="</tr>";
                }

            $s.='</table>';

            # Related exhibitions
            $query_exh=QUERIES::query_exhibition($this->_db);
            $results_exhibition_relations=$this->_db->query("SELECT ".$query_exh['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1=12 AND r.itemid1='".$this->_item_row['row']['id']."' AND r.typeid2=4 ) OR ( r.typeid2=12 AND r.itemid2='".$this->_item_row['row']['id']."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND e.enable=1 ORDER BY e.date_from DESC ");
            $count_exhibition_relations=$this->_db->numrows($results_exhibition_relations);
            if( $count_exhibition_relations )
            {
                $s.='<h3 style="padding-top:15px !important;">'.LANG_BOOKS_LANGUAGE_EXHCATALOG.'</h3>';
                $s.='<ul>';
                while( $row_exhibition=$this->_db->mysql_array($results_exhibition_relations) )
                {
                    $row_exhibition=UTILS::html_decode($row_exhibition);
                    #exh. title
                    $title_exhibition=UTILS::row_text_value2($this->_db,$row_exhibition,"title");
                    $title_exh=$row_exhibition['title_original'];
                    # end exh. title
                    # admin edit link
                    $options_admin_edit=array();
                    $options_admin_edit['html_return']=$this->_html_return;
                    $admin_edit=UTILS::admin_edit("/admin/exhibitions/edit/?exhibitionid=".$row_exhibition['exID'],$options_admin_edit);
                    # END admin edit link
                    $values_exh_url=array();
                    $values_exh_url['exhibitionid']=$row_exhibition['exID'];
                    $url_exh=UTILS::get_exhibition_url($this->_db,$values_exh_url);
                    $s.='<li>';
                        $s.='<a href="'.$url_exh.'" title="">';
                            $values_location=array();
                            $values_location['row']=$row_exhibition;
                            $location=location($this->_db,$values_location);
                            $exhibition="";
                            $exhibition.=$title_exh;
                            $exhibition.=', '.$location;
                            $exhibition.=', '.$row_exhibition['date_year'];
                            $s.=$table->expand_list_highlight($exhibition, $this->_highlight);
                        $s.='</a>';
                        $s.='&nbsp;'.$admin_edit;
                    $s.='</li>';
                }
                $s.='</ul>';
            }
            # END Related exhibitions

            # info small
            $infosmall=UTILS::row_text_value($this->_db,$this->_item_row['row'],"infosmall");
            if( !empty($infosmall) && $this->_item_row['row']['books_catid']!=19 )
            {
                $s.="<div class='div_text_wysiwyg div-lit-read-more' style='padding-top:15px;'>";
                    $options_lit_url=array();
                    $options_lit_url['bookid']=$this->_item_row['row']['id'];
                    $url_literature=UTILS::get_literature_url($this->_db,$options_lit_url);
                    $href=$url_literature['url'];
                    if( !empty($this->_highlight) ) $href.="&search=".$this->_highlight;
                    # check if string ends with !?.,: remove that and add ...
                    $test_infosmall=trim($infosmall);
                    $test_infosmall=strip_tags($test_infosmall);
                    $mark=substr ( $test_infosmall , strlen($test_infosmall)-1, 1 );
                    $characters=array('?','!','.',',',':',';');
                    if( in_array($mark, $characters) )
                    {
                        $mark_to_replace=substr ( $test_infosmall , strlen($test_infosmall)-4, 4 );
                        $mark_to_replace2=str_replace($mark, "", $mark_to_replace);
                        //$s.="-".$mark_to_replace."-";
                        $infosmall=str_replace($mark_to_replace, $mark_to_replace2, $infosmall);
                    }
                    # end
                    $s.=$table->expand_list_highlight($infosmall, $this->_highlight);
                    $s.="<span style='display:inline-block;margin-left:-2px;'>...</span>";
                $s.="</div>";
            }
            $info=UTILS::row_text_value2($this->_db,$this->_item_row['row'],"info");
            if( !empty($info) )
            {
                $html_a_read_more="";
                # if its an essay open info content in overlay
                if( $this->_item_row['row']['books_catid']==19 )
                {
                    $href="#overlay-book-".$this->_item_row['row']['id']."";
                    $title=LANG_BOOKS_READ_ESSAY;
                    $title_attr=$this->_item_row['row']['title'];
                    $class_section="a-read-essay";

                    $html_a_read_more.="<div id='overlay-book-".$this->_item_row['row']['id']."' class='div-book-overlay-content overlay-book-".$this->_item_row['row']['id']."'>";
                        $options_div_text=array();
                        $options_div_text['class']['div_text_wysiwyg']="div-book-overlay-content-wysiwyg";
                        $html_a_read_more.=$this->_html_class->div_text_wysiwyg($db,$info,$options_div_text);
                    $html_a_read_more.="</div>";
                }
                # else link it to book detail view
                else
                {
                    $title=LANG_BOOKS_MORE_DETAILS;
                    $title_attr=LANG_BOOKS_MORE_DETAILS;
                    $class_section="";
                }
                $html_a_read_more.="<a class='".$class_section." a-book-more-details a-book-more-details-".$_GET['lang']."' href='".$href."' title='".$title_attr."' data-id='".$this->_item_row['row']['id']."'>".$title."</a>";
                $s.=$html_a_read_more;
            }
            # END info small

            # literature admin link
            $options_admin_edit=array();
            $options_admin_edit['html_return']=$this->_html_return;
            $admin_edit=UTILS::admin_edit("/admin/books/edit/?bookid=".$this->_item_row['row']['id'],$options_admin_edit);
            $s.=$admin_edit;
            # END literature admin link

        $s.='</div>'; # END expand-box-details

        return $s;
    }
}

?>
