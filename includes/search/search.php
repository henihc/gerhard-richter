<?php
session_start();

//print_r($_GET);
//$_GET['search']=urlencode($_GET['search']);
$_GET['search']=urldecode($_GET['search']);
if( strlen($_GET['search'])>1 || is_numeric($_GET['search']) )
{

//require_once ($_SERVER["DOCUMENT_ROOT"]."/includes/class.html.inc.php");
require ($_SERVER["DOCUMENT_ROOT"]."/includes/includes.php");

$options_lang_file=array();
$options_lang_file['get_lang']=$_GET['lang'];
$language=UTILS::load_lang_file($db,$options_lang_file);

//print_r($_GET);

$html = new html_elements;

$db=new dbCLASS;

$_GET=$db->db_prepare_input($_GET);
$options_get=array();
$options_get['get']=$_GET;
$options_get['get']['sp']=10;
$_GET=UTILS::if_empty_get($db,$options_get);

$options_ul=array();

//print $_GET['search'];

print "\t<script type='text/javascript'>\n";
    //require_once ($_SERVER["DOCUMENT_ROOT"]."/js/jquery_tooltip/jquery.tooltip.load.js");
    print "$(function() {";
        # tooltips load
        print "$( '.help' ).tooltip({";
            print "items: '[data-title]',";
            print "content: $('.div-search-help').html(),";
            print "tooltipClass: 'div-tooltips-search-help'";
            //print "hide: {duration: 1000000 }"; //for debuging
        print "});";
        # END tooltips load
        # tooltips quote source text and search ? load
        //print "$( document ).tooltip({";
        print "$( '.div-quote' ).tooltip({";
            print "items: '[data-title], [data-source]',";
            print "position: {";
                print "my: 'left+60 top'";
                //print "at: 'right bottom+5',";
                //print "collision: 'fit none'";
            print "},";
            print "content: function() {";
                //print "console.debug($( this ).attr( 'data-class' ));";
                print "var element = $( this );";
                print "if ( element.is( '[data-source]' ) ) {";
                    print "var element_data = '.div-tooltips-info-content-quotes-'+element.attr( 'data-source' );";
                    print "return $(element_data).html()";
                print "}";
            print "},";
            print "tooltipClass: 'div-tooltips-quotes-source'";
            //print "hide: {duration: 1000000 }"; //for debuging
        print "});";
        # END tooltips load
    print "});";
    require_once ($_SERVER["DOCUMENT_ROOT"]."/js/expandlist/src/js/gerhard-richter.mod.expandlist.js");
print "\t</script>\n";

$_GET['search']=trim($_GET['search']);

# if in search string is exhibitions london then cut out exhibitions and search for london
$tmp=explode(" ", $_GET['search']);
if( count($tmp)==2 && $tmp[0]=="exhibitions" )
{
    $_GET['search']=$tmp[1];
}

### PAINTINGS
$limit_paintings=8;

$pattern_number_cr_number_search="/((cr)|(CR))+\W?[0-9]+/";
$numeric=str_replace("-","",$_GET['search']);

preg_match("/^(19|20)\d{2}$/", $_GET['search'], $matches_years);
$search_year=$matches_years[0];

    # prepare search keyword
    $options_search=array();
    //$options_search['painting_search']=1;
    $options_search['search_art']=1;
    $options_search['db']=$db;
    $prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
    # END prepare search keyword

    # prepare match
    $options_search_paintings=array();
    $options_search_paintings['search']=$prepear_search;
    $options_search_paintings['global_search']=1;
    $query_match=QUERIES_SEARCH::query_search_paintings($db,$options_search_paintings);
    # END prepare match

    $query_where_paintings=" WHERE p.enable=1 AND ( ".$query_match." ";

        $location_search_term=$prepear_search;

        #LOCATION

        # prepare search keyword
        $options_search=array();
        $options_search['db']=$db;
        $prepear_search=UTILS::prepear_search($location_search_term,$options_search);
        # END prepare search keyword

        # prepare match
        $options_search_locations=array();
        $options_search_locations['search']=$prepear_search;
        $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$options_search_locations);
        $query_where_locations=" WHERE ".$query_match_locations;
        # END prepare match

        $query_locations=QUERIES::query_locations($db,$query_where_locations);
        $results_locations=$db->query($query_locations['query_without_limit']);
        $count_locations=$db->numrows($results_locations);
        if( $_SESSION['debug_page'] ) print "<br /><br />".$query_locations['query_without_limit'];
        if( $count_locations>0 )
        {
            while( $row_locations=$db->mysql_array($results_locations) )
            {
                $results_loc=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE locationid='".$row_locations['locationid']."' OR loan_locationid='".$row_locations['locationid']."' ");
                $count_loc=$db->numrows($results_loc);
                if( $count_loc>0 )
                {
                    while( $row_loc=$db->mysql_array($results_loc) )
                    {
                        $locations_paintings[$row_loc['paintID']]=$row_loc['paintID'];
                    }
                }
            }
        }

        # LOCATION CITY

        # prepare match
        $options_search_locations_city=array();
        $options_search_locations_city['search']=$prepear_search;
        $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$options_search_locations_city);
        $query_where_locations_city=" WHERE ".$query_match_locations_city." AND ci.cityid!=668 ";
        # END prepare match

        $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
        $results_locations_city=$db->query($query_locations_city['query_without_limit']);
        $count_locations_city=$db->numrows($results_locations_city);
        if( $_SESSION['debug_page'] ) print "<br /><br />".$query_locations_city['query_without_limit'];
        if( $count_locations_city>0 )
        {
            while( $row_locations_city=$db->mysql_array($results_locations_city) )
            {
                $results_loc_city=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE cityid='".$row_locations_city['cityid']."' OR loan_cityid='".$row_locations_city['cityid']."' ");
                $count_loc_city=$db->numrows($results_loc_city);
                if( $count_loc_city>0 )
                {
                    while( $row_loc_city=$db->mysql_array($results_loc_city) )
                    {
                        $locations_paintings[$row_loc_city['paintID']]=$row_loc_city['paintID'];
                    }
                }
            }
        }

        # LOCATION COUNTRY

        # prepare match
        $options_search_locations_country=array();
        $options_search_locations_country['search']=$prepear_search;
        $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$options_search_locations_country);
        $query_where_locations_country=" WHERE ".$query_match_locations_country." AND co.countryid!=83 ";
        # END prepare match

        $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
        $results_locations_country=$db->query($query_locations_country['query_without_limit']);
        $count_locations_country=$db->numrows($results_locations_country);
        if( $_SESSION['debug_page'] ) print "<br /><br />".$query_locations_country['query_without_limit'];
        if( $count_locations_country>0 )
        {
            while( $row_locations_country=$db->mysql_array($results_locations_country) )
            {
                $results_loc_country=$db->query("SELECT paintID FROM ".TABLE_PAINTING." WHERE countryid='".$row_locations_country['countryid']."' OR loan_countryid='".$row_locations_country['countryid']."' ");
                $count_loc_country=$db->numrows($results_loc_country);
                if( $count_loc_country>0 )
                {
                    while( $row_loc_country=$db->mysql_array($results_loc_country) )
                    {
                        $locations_paintings[$row_loc_country['paintID']]=$row_loc_country['paintID'];
                    }
                }
            }
        }

        if( !empty($locations_paintings) )
        {
            $query_where_paintings.=" OR ";
            $query_where_paintings.=" ( ";
            $i_loc=0;
            foreach( $locations_paintings as $paintid )
            {
                if( $i_loc==0 ) $or_loc="";
                else $or_loc=" OR ";
                $query_where_paintings.=$or_loc." paintID='".$paintid."' ";
                $i_loc++;
            }
            $query_where_paintings.=" ) ";
        }

    $query_where_paintings.=" ) ";

$query_order=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";
$query_limit=" LIMIT ".$limit_paintings;
$query_paintings=QUERIES::query_painting($db,$query_where_paintings,$query_order,$query_limit,$query_from_paintings);
$_SESSION['search']['query']=$query_paintings['query_without_limit'];
//$url_art_search="/".$_GET['lang']."/art/search/?search_main=".$_GET['search']."&keyword=".$_GET['search']."&referer=search-main";
$url_art_search="/".$_GET['lang']."/art/search/?search_main=".$_GET['search']."&title=".$_GET['search']."&referer=search-main";
$_SESSION['search']['url']=$url_art_search;
$results_paintings_total=$db->query($query_paintings['query_without_limit']);
$count_paintings_total=$db->numrows($results_paintings_total);
$results_paintings=$db->query($query_paintings['query']);
$count_paintings=$db->numrows($results_paintings);
if( empty($count_paintings_total) ) $count_paintings_total=0;
$options_ul['count_paintings']=$count_paintings_total;
$count_total=$count_total+$count_paintings_total;

### END PAINTINGS

### MICROSITES
$limit_microsites=4;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);

# prepare match FROM MICROSITES TABLE
$options_search_microsites=array();
$options_search_microsites['search']=$prepear_search;
$query_match_microsites=QUERIES_SEARCH::query_search_microsites($db,$options_search_microsites);
# END prepare match FROM MICROSITES TABLE

$query_where=" WHERE m.enable=1 AND ".$query_match_microsites;
$query_order=" ORDER BY m.sort ASC ";
$query_limit=" LIMIT ".$limit_microsites;
$query_microsites=QUERIES::query_microsites($db,$query_where,$query_order,$query_limit);
$results_microsites_total=$db->query($query_microsites['query_without_limit']);
$count_microsites_total=$count_microsites_total+$db->numrows($results_microsites_total);
$results_microsites=$db->query($query_microsites['query']);
$count_microsites=$count_microsites+$db->numrows($results_microsites);
if( empty($count_microsites_total) ) $count_microsites_total=0;
$options_ul['count_microsites']=$count_microsites_total;
$count_total=$count_total+$count_microsites_total;
### END MICROSITES

//print "<br />";
//print $count_microsites."<br />";
//print $count_snow_white_total."<br />";
//print $count_war_cut_total."<br />";

### BIOGRAPHY
$limit_biography=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_biography=array();
$options_search_biography['search']=$prepear_search;
$query_match_biography=QUERIES_SEARCH::query_search_biography($db,$options_search_biography);
# END prepare match

$query_where=" WHERE bi.enable=1 AND ".$query_match_biography;
$query_order=" ORDER BY bi.sort ASC ";
$query_limit=" LIMIT ".$limit_biography;
$query_biography=QUERIES::query_biography($db,$query_where,$query_order,$query_limit);
$results_biography_total=$db->query($query_biography['query_without_limit']);
$count_biography_total=$db->numrows($results_biography_total);
$results_biography=$db->query($query_biography['query']);
$count_biography=$db->numrows($results_biography);
if( empty($count_biography_total) ) $count_biography_total=0;
$options_ul['count_biography']=$count_biography_total;
$count_total=$count_total+$count_biography_total;
### END BIOGRAPHY

### CHRONOLOGY
$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
$options_query_text_chronology=array();
# prepare match
$options_search_text=array();
$options_search_text['search']=$prepear_search;
$query_match_text=queries_search::query_search_text($db,$options_search_text);
$options_query_text_chronology['where']=" WHERE textid=3 AND ".$query_match_text;
# end prepare match
$query_text_chronology=QUERIES::query_text($db,$options_query_text_chronology);
$results_text_chronology_total=$db->query($query_text_chronology['query_without_limit']);
$count_text_chronology_total=$db->numrows($results_text_chronology_total);
$results_text_chronology=$db->query($query_text_chronology['query']);
$count_text_chronology=$db->numrows($results_text_chronology);
if( empty($count_text_chronology_total) ) $count_text_chronology_total=0;
$options_ul['count_text_chronology']=$count_text_chronology_total;
$count_total=$count_total+$count_text_chronology_total;
### END CHRONOLOGY

### QUOTES
$limit_quotes=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_quotes=array();
$options_search_quotes['search']=$prepear_search;
$query_match_quotes=QUERIES_SEARCH::query_search_quotes($db,$options_search_quotes);
$query_where=" WHERE ".$query_match_quotes;
# END prepare match

$query_order=" ORDER BY q.sort ASC ";
$query_limit=" LIMIT ".$limit_quotes;
$query_quotes=QUERIES::query_quotes($db,$query_where,$query_order,$query_limit);
$results_quotes_total=$db->query($query_quotes['query_without_limit']);
$count_quotes_total=$db->numrows($results_quotes_total);
$results_quotes=$db->query($query_quotes['query']);
$count_quotes=$db->numrows($results_quotes);
if( empty($count_quotes_total) ) $count_quotes_total=0;
$options_ul['count_quotes']=$count_quotes_total;
$count_total=$count_total+$count_quotes_total;
### END QUOTES

### EXHIBITIONS
$limit_exhibitions=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);

# prepare match
$options_search_exh=array();
$options_search_exh['search']=$prepear_search;
$query_match_exh=queries_search::query_search_exhibitions($db,$options_search_exh);
$query_where=" WHERE enable=1 AND ".$query_match_exh;
if( !empty($search_year) ) $query_where.=" OR DATE_FORMAT(date_from, '%Y') BETWEEN '".$search_year."' AND '".$search_year."' ";
# end prepare match

# Exh location search

            # LOCATION
            # prepare match
            $options_search_locations=array();
            $options_search_locations['search']=$prepear_search;
            $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$options_search_locations);
            $query_where_locations=" WHERE ".$query_match_locations;
            # END prepare match

            $query_locations=QUERIES::query_locations($db,$query_where_locations);
            $results_locations=$db->query($query_locations['query_without_limit']);
            $count_locations=$db->numrows($results_locations);
            //print $count_locations;
            if( $count_locations>0 )
            {
                while( $row_locations=$db->mysql_array($results_locations) )
                {
                    # EXHIBITIONS
                    $results_loc_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE locationid='".$row_locations['locationid']."'");
                    $count_loc_exh=$db->numrows($results_loc_exh);
                    if( $count_loc_exh>0 )
                    {
                        while( $row_loc_exh=$db->mysql_array($results_loc_exh) )
                        {
                            $locations_exhibitions[$row_loc_exh['exID']]=$row_loc_exh['exID'];
                        }
                    }
                    # NEWS - TALKS
                    $results_loc_news_talks=$db->query("SELECT newsID FROM ".TABLE_NEWS." WHERE type=3 AND NOW()<=dateto AND locationid='".$row_locations['locationid']."'");
                    $count_loc_news_talks=$db->numrows($results_loc_news_talks);
                    if( $count_loc_news_talks>0 )
                    {
                        while( $row_loc_news_talks=$db->mysql_array($results_loc_news_talks) )
                        {
                            $locations_news_talks[$row_loc_news_talks['newsID']]=$row_loc_news_talks['newsID'];
                        }
                    }
                }
            }

            # LOCATION CITY
            # prepare match
            $options_search_locations_city=array();
            $options_search_locations_city['search']=$prepear_search;
            $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$options_search_locations_city);
            $query_where_locations_city=" WHERE ".$query_match_locations_city;
            # END prepare match

            $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
            $results_locations_city=$db->query($query_locations_city['query_without_limit']);
            $count_locations_city=$db->numrows($results_locations_city);
            //print $count_locations_city;
            if( $count_locations_city>0 )
            {
                while( $row_locations_city=$db->mysql_array($results_locations_city) )
                {
                    # EXHIBITIONS
                    $results_loc_city_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE cityid='".$row_locations_city['cityid']."'");
                    $count_loc_city_exh=$db->numrows($results_loc_city_exh);
                    if( $count_loc_city_exh>0 )
                    {
                        while( $row_loc_city_exh=$db->mysql_array($results_loc_city_exh) )
                        {
                            $locations_exhibitions[$row_loc_city_exh['exID']]=$row_loc_city_exh['exID'];
                        }
                    }
                    # NEWS - TALKS
                    $results_loc_news_talks=$db->query("SELECT newsID FROM ".TABLE_NEWS." WHERE type=3 AND NOW()<=dateto AND cityid='".$row_locations_city['cityid']."'");
                    $count_loc_news_talks=$db->numrows($results_loc_news_talks);
                    if( $count_loc_news_talks>0 )
                    {
                        while( $row_loc_news_talks=$db->mysql_array($results_loc_news_talks) )
                        {
                            $locations_news_talks[$row_loc_news_talks['newsID']]=$row_loc_news_talks['newsID'];
                        }
                    }
                }
            }

            # LOCATION COUNTRY
            # prepare match
            $options_search_locations_country=array();
            $options_search_locations_country['search']=$prepear_search;
            $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$options_search_locations_country);
            $query_where_locations_country=" WHERE ".$query_match_locations_country;
            # END prepare match

            $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
            $results_locations_country=$db->query($query_locations_country['query_without_limit']);
            $count_locations_country=$db->numrows($results_locations_country);
            //print $count_locations_country;
            if( $count_locations_country>0 )
            {
                while( $row_locations_country=$db->mysql_array($results_locations_country) )
                {
                    # EXHIBITIONS
                    $results_loc_country_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE countryid='".$row_locations_country['countryid']."'");
                    $count_loc_country_exh=$db->numrows($results_loc_country_exh);
                    if( $count_loc_country_exh>0 )
                    {
                        while( $row_loc_country_exh=$db->mysql_array($results_loc_country_exh) )
                        {
                            $locations_exhibitions[$row_loc_country_exh['exID']]=$row_loc_country_exh['exID'];
                        }
                    }
                    # NEWS - TALKS
                    $results_loc_news_talks=$db->query("SELECT newsID FROM ".TABLE_NEWS." WHERE type=3 AND NOW()<=dateto AND countryid='".$row_locations_country['countryid']."'");
                    $count_loc_news_talks=$db->numrows($results_loc_news_talks);
                    if( $count_loc_news_talks>0 )
                    {
                        while( $row_loc_news_talks=$db->mysql_array($results_loc_news_talks) )
                        {
                            $locations_news_talks[$row_loc_news_talks['newsID']]=$row_loc_news_talks['newsID'];
                        }
                    }
                }
            }

            if( !empty($locations_exhibitions) )
            {
                $query_where.=" OR ( ";
                $i_loc_exh=0;
                foreach( $locations_exhibitions as $exhibitionid )
                {
                    if( $i_loc_exh==0 ) $or_loc="";
                    else $or_loc=" OR ";
                    $query_where.=$or_loc." e.exID='".$exhibitionid."' ";
                    $i_loc_exh++;
                }
                $query_where.=" ) ";
            }



# END Exh location search


$query_order=" ORDER BY e.date_from DESC, e.typeid DESC ";
$query_limit=" LIMIT ".$limit_exhibitions;
$query_exhibitions=QUERIES::query_exhibition($db,$query_where,$query_order,$query_limit);
$results_exhibitions_total=$db->query($query_exhibitions['query_without_limit']);
$count_exhibitions_total=$db->numrows($results_exhibitions_total);
$results_exhibitions=$db->query($query_exhibitions['query']);
$count_exhibitions=$db->numrows($results_exhibitions);
if( empty($count_exhibitions_total) ) $count_exhibitions_total=0;
$options_ul['count_exhibitions']=$count_exhibitions_total;
$count_total=$count_total+$count_exhibitions_total;
### END EXHIBITIONS

### LITERATURE
$limit_literature=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_lit=array();
$options_search_lit['search']=$prepear_search;
//$query_match_lit=queries_search::query_search_literature($db,$options_search_lit);
$query_match_lit=queries_search::query_search_literature_keyword($db,$options_search_lit);
$query_where=" WHERE b.enable=1 AND ( ".$query_match_lit;
# end prepare match

    # search isbn numbers
    $results_isbn=$db->query("SELECT bookID FROM ".TABLE_BOOKS_ISBN." WHERE isbn='".$_GET['search']."' ");
    $count_isbn=$db->numrows($results_isbn);
    if( $count_isbn )
    {
        $i=0;
        $query_where.=" OR ( ";
        while( $row_isbn=$db->mysql_array($results_isbn) )
        {
            if( $i>0 ) $or=" OR ";
            else $or="";
            $query_where.=$or." b.id='".$row_isbn['bookID']."'";
            $i++;
        }
        $query_where.=" ) ";
    }
    # END

    # if book keyword year search
    if( !empty($search_year) ) $query_where.=" OR DATE_FORMAT(link_date, '%Y') BETWEEN '".$search_year."' AND '".$search_year."' ";

    # book exhibition relation
    # searching for linked exhibitions
    $locations_exhibitions=array();
    $options_search_exh2=array();
    $options_search_exh2['search']=$prepear_search;
    $query_match_exh2=QUERIES_SEARCH::query_search_exhibitions($db,$options_search_exh2);
    $results_exh2=$db->query("SELECT e.exID FROM ".TABLE_EXHIBITIONS." e WHERE ".$query_match_exh2);
    $count_exh2=$db->numrows($results_exh2);
    if( $count_exh2>0 )
    {
        while( $row_exh2=$db->mysql_array($results_exh2) )
        {
            $locations_exhibitions[$row_exh2['exID']]=$row_exh2['exID'];
        }
    }
    //print_r($locations_exhibitions);
    //print $count_exh;
    # searching for linked exhibitions

    #LOCATION

    # prepare match
    $options_search_locations=array();
    $options_search_locations['search']=$prepear_search;
    $query_match_locations=QUERIES_SEARCH::query_search_locations($db,$options_search_locations);
    $query_where_locations=" WHERE ".$query_match_locations;
    # END prepare match

    $query_locations=QUERIES::query_locations($db,$query_where_locations);
    $results_locations=$db->query($query_locations['query_without_limit']);
    $count_locations=$db->numrows($results_locations);
    //print $count_locations;
    if( $count_locations>0 )
    {
        while( $row_locations=$db->mysql_array($results_locations) )
        {
            $results_loc_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE locationid='".$row_locations['locationid']."'");
            $count_loc_exh=$db->numrows($results_loc_exh);
            if( $count_loc_exh>0 )
            {
                while( $row_loc_exh=$db->mysql_array($results_loc_exh) )
                {
                    $locations_exhibitions[$row_loc_exh['exID']]=$row_loc_exh['exID'];
                }
            }
        }
    }


    # LOCATION CITY

    # prepare match
    $options_search_locations_city=array();
    $options_search_locations_city['search']=$prepear_search;
    $query_match_locations_city=QUERIES_SEARCH::query_search_locations_city($db,$options_search_locations_city);
    $query_where_locations_city=" WHERE ".$query_match_locations_city;
    # END prepare match

    $query_locations_city=QUERIES::query_locations_city($db,$query_where_locations_city);
    $results_locations_city=$db->query($query_locations_city['query_without_limit']);
    $count_locations_city=$db->numrows($results_locations_city);
    //print $count_locations_city;
    if( $count_locations_city>0 )
    {
        while( $row_locations_city=$db->mysql_array($results_locations_city) )
        {
            $results_loc_city_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE cityid='".$row_locations_city['cityid']."'");
            $count_loc_city_exh=$db->numrows($results_loc_city_exh);
            if( $count_loc_city_exh>0 )
            {
                while( $row_loc_city_exh=$db->mysql_array($results_loc_city_exh) )
                {
                    $locations_exhibitions[$row_loc_city_exh['exID']]=$row_loc_city_exh['exID'];
                }
            }
        }
    }


    # LOCATION COUNTRY

    # prepare match
    $options_search_locations_country=array();
    $options_search_locations_country['search']=$prepear_search;
    $query_match_locations_country=QUERIES_SEARCH::query_search_locations_country($db,$options_search_locations_country);
    $query_where_locations_country=" WHERE ".$query_match_locations_country;
    # END prepare match

    $query_locations_country=QUERIES::query_locations_country($db,$query_where_locations_country);
    $results_locations_country=$db->query($query_locations_country['query_without_limit']);
    $count_locations_country=$db->numrows($results_locations_country);
    //print $count_locations_country;
    if( $count_locations_country>0 )
    {
        while( $row_locations_country=$db->mysql_array($results_locations_country) )
        {
            $results_loc_country_exh=$db->query("SELECT exID FROM ".TABLE_EXHIBITIONS." WHERE countryid='".$row_locations_country['countryid']."'");
            $count_loc_country_exh=$db->numrows($results_loc_country_exh);
            if( $count_loc_country_exh>0 )
            {
                while( $row_loc_country_exh=$db->mysql_array($results_loc_country_exh) )
                {
                    $locations_exhibitions[$row_loc_country_exh['exID']]=$row_loc_country_exh['exID'];
                }
            }
        }
    }

    $query_where_exhibitions2=" WHERE enable=1 AND ( ";

        if( !empty($locations_exhibitions) )
        {
            $query_where_exhibitions2.=$and." ( ";
            $i_loc_exh=0;
            foreach( $locations_exhibitions as $exhibitionid )
            {
                if( $i_loc_exh==0 ) $or_loc="";
                else $or_loc=" OR ";
                $query_where_exhibitions2.=$or_loc." exID='".$exhibitionid."' ";
                $i_loc_exh++;
            }
            $query_where_exhibitions2.=" ) ";
        }
        else
        {
            $query_where_exhibitions2.=$and." exID=-1 ";
        }

    $query_where_exhibitions2.=" ) ";
    # END add years from location search

    $query_exhibitions2=QUERIES::query_exhibition($db,$query_where_exhibitions2);
    $results_exhibitions2=$db->query($query_exhibitions2['query_without_limit']);
    $count_exhibitions2=$db->numrows($results_exhibitions2);
    if( $count_exhibitions2 )
    {
        $i=0;
        $ii=0;
        $query_books="";
        while($row_exhibitions2=$db->mysql_array($results_exhibitions2))
        {
            $results_literature2=$db->query("SELECT b.id FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=4 AND r.itemid1='".$row_exhibitions2['exID']."' AND r.typeid2=12 ) OR ( r.typeid2=4 AND r.itemid2='".$row_exhibitions2['exID']."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ");
            $count_literature2=$db->numrows($results_literature2);
            if( $count_literature2 )
            {
                while( $row_literature2=$db->mysql_array($results_literature2) )
                {
                    if( empty($query_books) ) $query_books.=" OR ( ";
                    if( $ii==0 ) $or="";
                    else $or="OR";
                    $query_books.=$or." b.id='".$row_literature2['id']."' ";
                    $ii++;
                }
            }
            $i++;
        }
        if( !empty($query_books) )
        {
            $query_books.=" ) ";
            $query_where.=$query_books;
        }
    }



$query_where.=" ) ";

/*
$query_where=" WHERE MATCH(
                    title,
                    title_periodical,
                    title_search1,
                    title_search2,
                    title_search3
                ) AGAINST ('".$prepear_search."' IN BOOLEAN MODE)";
 */

# searching ISBN numbers
$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
$results_isbn=$db->query("SELECT bookID FROM ".TABLE_BOOKS_ISBN." WHERE isbn='".$prepear_search."' ");
$count_isbn=$db->numrows($results_isbn);
if( $count_isbn )
{
    $i=0;
    $query_where.=" OR ( ";
    while( $row_isbn=$db->mysql_array($results_isbn) )
    {
        if( $i>0 ) $or=" OR ";
        else $or="";
        $query_where.=$or." b.id='".$row_isbn['bookID']."'";
        $i++;
    }
    $query_where.=" ) ";
}
# END

# searching for linked exhibitions
/*
$options_search_exh=array();
$options_search_exh['search']=$prepear_search;
$query_match_exh=QUERIES_SEARCH::query_search_exhibitions($db,$options_search_exh);
$results_exh=$db->query("SELECT e.exID FROM ".TABLE_EXHIBITIONS." e WHERE ".$query_match_exh);
$count_exh=$db->numrows($results_exh);
if( $count_exh>0 )
{
    while( $row_exh=$db->mysql_array($results_exh) )
    {
        $locations_exhibitions[$row_exh['exID']]=$row_exh['exID'];
    }
}
*/
# searching for linked exhibitions

# if found books category show only category
$options_search_books_cat=array();
$options_search_books_cat['search']=$prepear_search;
$query_match_books_cat=QUERIES_SEARCH::query_search_literature_categories($db,$options_search_books_cat);
$results_books_cat=$db->query("SELECT bc.books_catid FROM ".TABLE_BOOKS_CATEGORIES." bc WHERE ".$query_match_books_cat);
$count_books_cat=$db->numrows($results_books_cat);
if($count_books_cat)
{
    $row_books_cat=$db->mysql_array($results_books_cat);
    $query_where=" WHERE b.enable=1 AND b.books_catid='".$row_books_cat['books_catid']."' ";
    $options_book_cat_url=array();
    $options_book_cat_url['books_catid']=$row_books_cat['books_catid'];
    $url_literature_cat=UTILS::get_literature_cat_url($db,$options_book_cat_url);
    $url_all_literature=$url_literature_cat['url'];
    //$url_all_literature="/literature/?books_catid=".$row_books_cat['books_catid'];
}
# end

$options_books_query=array();
$options_books_query['columns']=",MATCH(b.title) AGAINST ('".$prepear_search."' IN BOOLEAN MODE) AS relevance";
//$query_order=" ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.books_catid ASC, b.author ASC ";
$query_order=" ORDER BY relevance DESC, DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.books_catid ASC, b.author ASC ";
$query_limit=" LIMIT ".$limit_literature;
$query_literature=QUERIES::query_books($db,$query_where,$query_order,$query_limit,"",$options_books_query);
$results_literature_total=$db->query($query_literature['query_without_limit']);
$count_literature_total=$db->numrows($results_literature_total);
$results_literature=$db->query($query_literature['query']);
$count_literature=$db->numrows($results_literature);
if( empty($count_literature_total) ) $count_literature_total=0;
$options_ul['count_literature']=$count_literature_total;
$count_total=$count_total+$count_literature_total;
### END LITERATURE

### VIDEOS
$limit_videos=4;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_videos=array();
$options_search_videos['search']=$prepear_search;
$query_match_videos=queries_search::query_search_videos($db,$options_search_videos);
$query_where=" WHERE v.enable=1 AND ".$query_match_videos;
# end prepare match

$query_order=" ORDER BY v.categoryid ASC, v.sort ASC ";
$query_limit=" LIMIT ".$limit_videos;
$query_videos=QUERIES::query_videos($db,$query_where,$query_order,$query_limit);
$results_videos_total=$db->query($query_videos['query_without_limit']);
$count_videos_total=$db->numrows($results_videos_total);
$results_videos=$db->query($query_videos['query']);
$count_videos=$db->numrows($results_videos);
if( empty($count_videos_total) ) $count_videos_total=0;
$options_ul['count_videos']=$count_videos_total;
$count_total=$count_total+$count_videos_total;
### END VIDEOS

### LINKS
$limit_links=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_links=array();
$options_search_links['search']=$prepear_search;
$query_match_links=queries_search::query_search_literature($db,$options_search_links);
$query_where_links=" WHERE b.link IS NOT NULL AND b.link!='' AND (books_catid=6 OR books_catid=12 OR books_catid=13) AND ".$query_match_links." AND b.enable=1 ";
# end prepare match

$query_order=" ORDER BY b.link_date DESC ";
$options_links=array();
$options_links['distinct_id']=1;
$query_limit=" LIMIT ".$limit_links;
$query_tables="";
$query_links=QUERIES::query_books($db,$query_where_links,$query_order,$query_limit,$query_tables,$options_links);
$results_links_total=$db->query($query_links['query_without_limit']);
$count_links_total=$db->numrows($results_links_total);
$results_links=$db->query($query_links['query']);
$count_links=$db->numrows($results_links);
if( empty($count_links_total) ) $count_links_total=0;
$options_ul['count_links']=$count_links_total;
$count_total=$count_total+$count_links_total;
### END LINKS

### NEWS - TALKS
$limit_news_talks=3;
$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_news_talks=array();
$options_search_news_talks['search']=$prepear_search;
$query_match_news_talks=queries_search::query_search_news_talks($db,$options_search_news_talks);
$query_news_talks_where=" WHERE type=3 AND NOW()<=dateto AND ".$query_match_news_talks;

            if( !empty($locations_news_talks) )
            {
                $query_news_talks_where.=" OR ( ";
                $i_loc_news_talks=0;
                foreach( $locations_news_talks as $newsid )
                {
                    if( $i_loc_news_talks==0 ) $or_loc="";
                    else $or_loc=" OR ";
                    $query_news_talks_where.=$or_loc." n.newsID='".$newsid."' ";
                    $i_loc_news_talks++;
                }
                $query_news_talks_where.=" ) ";
            }

# end prepare match
$query_news_talks_order=" ORDER BY datefrom ASC, dateto ASC ";
$query_news_talks_limit=" LIMIT ".$limit_news_talks;
$query_news_talks=QUERIES::query_news($db,$query_news_talks_where,$query_news_talks_order,$query_news_talks_limit);
$results_news_talks_total=$db->query($query_news_talks['query_without_limit']);
$count_news_talks_total=$db->numrows($results_news_talks_total);
$results_news_talks=$db->query($query_news_talks['query']);
$count_news_talks=$db->numrows($results_news_talks);
if( empty($count_news_talks_total) ) $count_news_talks_total=0;
$options_ul['count_news_talks']=$count_news_talks_total;
$count_total=$count_total+$count_news_talks_total;
### END NEWS - TALKS

### NEWS AUCTIONS
$limit_news_auctions=3;

$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
# prepare match
$options_search_auctionhouse=array();
$options_search_auctionhouse['search']=$prepear_search;
$query_match_auctionhouse=queries_search::query_search_auctionhouse($db,$options_search_auctionhouse);
$query_auctionhouse="SELECT ahID AS auctionhouseid FROM ".TABLE_AUCTIONHOUSE." a WHERE ".$query_match_auctionhouse;
# end prepare match
# prepare match
$options_search_sales_history=array();
$options_search_sales_history['search']=$prepear_search;
$query_match_sales_history=queries_search::query_search_sales_history($db,$options_search_sales_history);
# end prepare match
$results_auctionhouse=$db->query($query_auctionhouse);

$auctionhouses=array();
while( $row_auctionhouse=$db->mysql_array($results_auctionhouse) )
{
    $auctionhouses[]=$row_auctionhouse['auctionhouseid'];
}


            if( empty($_GET['sort']) )
            {
                $_GET['sort']=5;
                $_GET['sort_how']=-1;
            }
            else
            {
                $tmp=explode(",", $_GET['sort']);
                $_GET['sort']=$tmp[0];
                if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
                else $_GET['sort_how']=-1;
            }

            $options_auctions_table = array(
                'options'       =>  array(
                    // Setup table options here...
                    'id'                => 'expandlist_literature_02',  // set DOM node id for js (uniqid() if not set)
                    'sort_by'   => array($_GET['sort'], $_GET['sort_how']),                         // default column sorted
                    'expandable'        => false,                       // Allow tr tags to expand (expand-boxes required)
                    'highlight'         =>  $_GET['search'],                     // highlight patterns in content
                    'html_class'         =>  $html,
                    'html_return'         =>  1,
                    'onheaderclick'     => $_SERVER['PATH_INFO'] . '?sort='
                ),
                'cols'          =>  array(
                    // Define columns here...
                    array( 'caption' => '',                 'width' => '52px',   'col' => '0',   'sortable' => false, 'sort_col' => 'p.saleDate',    'content_align' => 'center'),
                    array( 'caption' => LANG_ARTWORK_BOOKS_REL_ARTWORK,          'width' => '168px',     'col' => '1',   'sortable' => false,  'sort_col' => 'p.titleDE',   'content_align' => 'left' ),
                    array( 'caption' => LANG_NEWS_AUCTIONS_AUCTIONHOUSE,    'width' => '110px',   'col' => '2',   'sortable' => false,  'sort_col' => 'ah.house',   'content_align' => 'left' ),
                    array( 'caption' => LANG_ESTIMATE,      'width' => '22%',    'col' => '3',   'sortable' => false, 'sort_col' => 's.estLowUSD',    'content_align' => 'left' ),
                    array( 'caption' => LANG_SOLDPR,        'width' => '*',   'col' => '4',   'sortable' => false, 'sort_col' => 's.soldForUSD',    'content_align' => 'left' ),
                    array( 'caption' => LANG_NEWS_AUCTIONS_DATE,             'width' => '9%',   'col' => '5',   'sortable' => false, 'sort_col' => 's.saleDate',    'content_align' => 'left' )
                )
            );


            $options_table_data=array();
            $options_table_data['html_return']=$options_auctions_table['options']['html_return'];
            $options_table_data['cols']=$options_auctions_table['cols'];
            $options_table_data['html_class']=$options_auctions_table['options']['html_class'];
            $options_table_data['sort']=$options_auctions_table['options']['sort_by'];
            $options_table_data['highlight']=$_GET['search'];
            $table_data = new Table_Data($db,$options_table_data);
            $options_table_data=array();
            $options_table_data['auctionhouses']=$auctionhouses;
            $options_table_data['search']=$_GET['search'];
            $options_table_data['query_match_sales_history']=$query_match_sales_history;
            $options_table_data['limit']=" LIMIT 0,".$limit_news_auctions;
            $options_table_data['results']=1;
            $rows_auctions=$table_data->table_data_auctions($options_table_data);


$count_news_auctions_total=$rows_auctions['count'];
$count_news_auctions=$rows_auctions['count_limit'];
if( empty($count_news_auctions_total) ) $count_news_auctions_total=0;
$options_ul['count_news_auctions']=$count_news_auctions_total;
$count_total=$count_total+$count_news_auctions_total;
### END NEWS AUCTIONS

### CREDITS
$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
$options_query_text_credits=array();
# prepare match
$options_search_text=array();
$options_search_text['search']=$prepear_search;
$query_match_text=queries_search::query_search_text($db,$options_search_text);
$options_query_text_credits['where']=" WHERE textid=1 AND ".$query_match_text;
# end prepare match
$query_text_credits=QUERIES::query_text($db,$options_query_text_credits);
$results_text_credits_total=$db->query($query_text_credits['query_without_limit']);
$count_text_credits_total=$db->numrows($results_text_credits_total);
$results_text_credits=$db->query($query_text_credits['query']);
$count_text_credits=$db->numrows($results_text_credits);
if( empty($count_text_credits_total) ) $count_text_credits_total=0;
$options_ul['count_text_credits']=$count_text_credits_total;
$count_total=$count_total+$count_text_credits_total;
### END CREDITS

### DISCLAIMER
$options_search=array();
$options_search['db']=$db;
$prepear_search=UTILS::prepear_search($_GET['search'],$options_search);
$options_query_text_disclaimer=array();
# prepare match
$options_search_text=array();
$options_search_text['search']=$prepear_search;
$query_match_text=queries_search::query_search_text($db,$options_search_text);
$options_query_text_disclaimer['where']=" WHERE textid=2 AND ".$query_match_text;
# end prepare match
$query_text_disclaimer=QUERIES::query_text($db,$options_query_text_disclaimer);
$results_text_disclaimer_total=$db->query($query_text_disclaimer['query_without_limit']);
$count_text_disclaimer_total=$db->numrows($results_text_disclaimer_total);
$results_text_disclaimer=$db->query($query_text_disclaimer['query']);
$count_text_disclaimer=$db->numrows($results_text_disclaimer);
if( empty($count_text_disclaimer_total) ) $count_text_disclaimer_total=0;
$options_ul['count_text_disclaimer']=$count_text_disclaimer_total;
$count_total=$count_total+$count_text_disclaimer_total;
### END DISCLAIMER

//print "\t<div class='div-search-basic-bg-line'>".LANG_SEARCH_RESULTS_ALL."</div>\n";

//print "\t<div id='sub-nav'>\n";
    $options_ul['count_total']=$count_total;
    $html->ul_basic_search($db,$options_ul);
//print "\t</div>\n";

//print "\t<div id='content' class='content-search-basic-results'>\n";

    if( $count_paintings>0 || $count_microsites>0 || $count_biography>0 || $count_text_chronology || $count_quotes>0 || $count_exhibitions>0 || $count_literature>0 || $count_videos>0 || $count_links || $count_news_auctions>0 || $count_text_credits>0 || $count_text_disclaimer>0 )
    {
        function h1_html($db,$options)
        {
            print "\t<h2 class='h2-search-basic-section' id='".$options['title_section_a']."'>\n";
                print $options['title_section'];
                print "\t<span class='span-basic-search-results'>\n";
                    if( $options['count']>1 ) $text_results=LANG_SEARCH_RESULTS;
                    else $text_results=LANG_SEARCH_RESULT;
                    print $options['count'];
                    if( $options['count_total']>$options['limit'] ) print "/".$options['count_total'];
                    print " ".$text_results;
                print "\t</span>\n";

                if( $options['count_total']>$options['limit'] ) $text=LANG_SEARCH_SHOW_ALL;
                else $text=LANG_SEARCH_SHOW_ALL;
                if( !empty($options['url_all']) ) print "\t <a href='".$options['url_all']."' title='".$text."' class='a-basic-search-show-all'>".$text."</a>\n";

            print "\t</h1>\n";
        }

        $search_for_url=$_GET['search'];
        $search_for_url=UTILS::strip_slashes_recursive($search_for_url);
        $search_for_url=str_replace('"',"",$search_for_url);
        $search_for_url=urlencode($search_for_url);

        if( $count_paintings>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_ART;
            $options['title_section_a']="art";
            $options['count']=$count_paintings;
            $options['count_total']=$count_paintings_total;
            $options['limit']=$limit_paintings;
            if( $count_paintings_total>$limit_paintings ) $options['url_all']=$_SESSION['search']['url'];
            h1_html($db,$options);

            # creating thumb array and sorting how we want
            $options_order_res=array();
            $options_order_res['results']=$results_paintings;
            $options_order_res['title']=$_GET['search'];
            $thumb_array=UTILS::order_results($db,$options_order_res);
            # end

            $options=array();
            $options['results']=$thumb_array;
            $options['show_title']=1;
            $title_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            $options['search_vars']="&referer=search&title=".$title_for_url."&keyword=".$title_for_url;
            $options['search']=$_GET['search'];
            $options['url']="/art/search/detail.php?paintid=";
            $options['thumbs_per_line']=4;
            $options['htaccess']=0;
            $options['html_return']=1;
            $options['values_from']=0;
            $options['values_to']=$limit_paintings;
            if($show_per_page=='all') $options['show_all']=1;
            if( $options['values_to']>count($thumb_array) ) $options['values_to']=count($thumb_array);
            $options['class']['div-section-categories']="div-thumbs-global-search";
            print $html->div_thumb_images($db,$options);

        }
        if( $count_microsites>0 )
        {
            $options=array();
            $options['title_section']=LANG_LEFT_SIDE_MICROSITES;
            $options['title_section_a']="microsites";
            $options['count']=$count_microsites;
            $options['count_total']=$count_microsites_total;
            $options['limit']=$limit_microsites;
            //if( $count_microsites_total>$limit_microsites ) $options['url_all']="/".$_GET['lang']."/art/microsites/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search'>\n";
                $thumb_array=array();
                if( $count_snow_white ) $results_microsites_seperate=$results_snow_white;
                elseif( $count_war_cut ) $results_microsites_seperate=$results_war_cut;
                elseif( $count_firenze ) $results_microsites_seperate=$results_firenze;
                if( $count_snow_white || $count_war_cut || $count_firenze )
                {
                    while( $row=$db->mysql_array($results_microsites_seperate,0) )
                    {
                        $thumb_array[]=$row;
                    }
                }
                while( $row=$db->mysql_array($results_microsites,0) )
                {
                    $thumb_array[]=$row;
                }
                if( $count_microsites )
                {
                    //print_r($thumb_array);
                    $options=array();
                    $options['results']=$thumb_array;
                    $options['show_title']=1;
                    $title_for_url=urlencode(UTILS::strip_slashes_recursive($_GET['search']));
                    //$options['search_vars']="&referer=search&title=".$title_for_url."&keyword=".$title_for_url;
                    $options['search']=$_GET['search'];
                    //$options['url']="/".$_GET['lang']."/art/microsites/";
                    $options['thumbs_per_line']=4;
                    $options['htaccess']=0;
                    $options['html_return']=1;
                    $options['values_from']=0;
                    $options['values_to']=$limit_microsites;
                    //if($show_per_page=='all') $options['show_all']=1;
                    if( $options['values_to']>count($thumb_array) ) $options['values_to']=count($thumb_array);
                    $options['class']['div-section-categories']="div-thumbs-global-search";
                    print $html->div_thumb_images($db,$options);
                }
            print "\t</div>\n";
        }
        if( $count_biography>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_BIOGRAPHY;
            $options['title_section_a']="biography";
            $options['count']=$count_biography;
            $options['count_total']=$count_biography_total;
            $options['limit']=$limit_biography;
            if( $count_biography_total>$limit_biography ) $options['url_all']="/".$_GET['lang']."/biography/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search'>\n";
                while( $row=$db->mysql_array($results_biography,0) )
                {
                    $options_row_text=array();
                    $options_row_text['search']=$_GET['search'];
                    $title=UTILS::row_text_value($db,$row,"title",$options_row_text);
                    $title_no_html=strip_tags($title);
                    $row=UTILS::html_decode($row);
                    $options_row_text=array();
                    $options_row_text['search']=$_GET['search'];
                    $text=UTILS::row_text_value($db,$row,"text",$options_row_text);
                    //$text=str_replace($title,"",$text);

                    $options=array();
                    $options['search']=$_GET['search'];
                    $options['text']=$title;
                    $options['limit']=14;
                    $options['strip_tags']=1;
                    $options['a_name']=1;
                    $title=UTILS::show_search_found_keywords($db,$options);

                    $options=array();
                    $options['search']=$_GET['search'];
                    $options['text']=$text;
                    $options['type']=1;
                    $options['limit']=70;
                    $options['show_dots']=1;
                    $options['strip_tags']=1;
                    $options['a_name']=1;
                    $text=UTILS::show_search_found_keywords($db,$options);

                    $url="/".$_GET['lang']."/biography/".$row['titleurl']."/?search=".$search_for_url."#".$search_for_url;
                    print "\t<div class='table-item-basic-search'>\n";
                        print UTILS::admin_edit("/admin/biography/edit/?biographyid=".$row['biographyid']);
                        //print "\t<a href='".$url."' title='".$title_no_html."'>\n";
                        print "\t<a href='".$url."' title=''>\n";
                            print "\t<span class='span-basic-search-title'>".$title."</span>\n";
                            print "\t<span class='span-basic-search-text'>".$text."</span>\n";
                        print "\t</a>\n";
                    print "\t</div>\n";
                }
            print "\t</div>\n";
        }
        if( $count_text_chronology>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_CHRONOLOGY;
            $options['title_section_a']="chronology";
            $options['count']=$count_text_chronology;
            h1_html($db,$options);
            print "\t<div class='table-basic-search-text-chronology'>\n";
                while( $row=$db->mysql_array($results_text_chronology) )
                {
                    $row=UTILS::html_decode($row);
                    $options_row_text=array();
                    $options_row_text['search']=$_GET['search'];
                    $text=UTILS::row_text_value($db,$row,"text",$options_row_text);
                    $text = preg_replace('~<span class="p-image-desc">.+</span>~', "", $text);
                    $text=preg_filter('~<span class="span-year">.+</span>:~', '|||br|||$0', $text);
                    $text_no_html=strip_tags($text);

                    $options_search_found=array();
                    $options_search_found['search']=$_GET['search'];
                    $options_search_found['text']=$text;
                    $options_search_found['type']=1;
                    $options_search_found['limit']=140;
                    $options_search_found['show_dots']=1;
                    $options_search_found['a_name']=1;
                    $options_search_found['convert_utf8']=1;
                    $text=UTILS::show_search_found_keywords($db,$options_search_found);
                    $text=str_replace('|||br|||',"<br />",$text);

                    $url="/".$_GET['lang']."/chronology/?search=".$search_for_url."#".$search_for_url;
                    print "\t<div class='table-item-basic-search'>\n";
                        print UTILS::admin_edit("/admin/pages/edit/?textid=".$row['textid']);
                        print "\t<a href='".$url."' title=''>\n";
                            print "\t<span class='span-basic-search-text'>".$text."</span>\n";
                        print "\t</a>\n";
                    print "\t</div>\n";
                }
                print "\t<div class='clearer'></div>\n";
            print "\t</div>\n";
        }
        if( $count_quotes>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_QUOTES;
            $options['title_section_a']="quotes";
            $options['count']=$count_quotes;
            $options['count_total']=$count_quotes_total;
            $options['limit']=$limit_quotes;
            if( $count_quotes_total>$limit_quotes ) $options['url_all']="/".$_GET['lang']."/quotes/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search-quotes'>\n";
                $options_quotes=array();
                $options_quotes['html_return']=1;
                $options_quotes['query']=$query_quotes['query'];
                $options_quotes['global_search']=$limit_quotes;
                $options_quotes['search']=$_GET['search'];
                print $html->quotes($db,$options_quotes);
            print "\t</div>\n";
        }

        if( $count_exhibitions>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_EXHIBITIONS;
            $options['title_section_a']="exhibitions";
            $options['count']=$count_exhibitions;
            $options['count_total']=$count_exhibitions_total;
            $options['limit']=$limit_exhibitions;
            if( $count_exhibitions_total>$limit_exhibitions ) $options['url_all']="/".$_GET['lang']."/exhibitions/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search'>\n";
                while( $row=$db->mysql_array($results_exhibitions) )
                {
                    $options=array();
                    $options['html_return']=1;
                    $options['class']['div-exhibition-right-side-search']="div-exhibition-right-side-search";
                    $options['class']['a-exhibition-list-search']="a-exhibition-list-search";
                    $options['class']['div-exhibition-search']="div-exhibition-search";
                    $options['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                    $options['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                    $options['class']['span-location-length']="span-exh-list-location-wide";
                    $options['class']['span-date-length']="span-exh-list-date-wide";
                    $options['type']=1;
                    $options['row']=$row;
                    $options['i']=$i;
                    $options['count']=$count_exhibitions;
                    $options['search']=$_GET['search'];
                    print $html->div_exhibition($db,$options);
                }
            print "\t</div>\n";
        }
        if( $count_literature>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_LITERATURE;
            $options['title_section_a']="literature";
            $options['count']=$count_literature;
            $options['count_total']=$count_literature_total;
            $options['limit']=$limit_literature;
            if( !empty($url_all_literature) ) $options['url_all']=$url_all_literature;
            elseif( $count_literature_total>$limit_literature ) $options['url_all']="/".$_GET['lang']."/literature/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search'>\n";

    $options_table = array(
        'options'       =>  array(
            // Setup table options here...
            'id'                => 'expandlist_literature_01',      // set DOM node id for js (uniqid() if not set)
            'expandable'        => 1,                            // Allow tr tags to expand (expand-boxes required)
            'class'        => $options['class'],                            // Allow tr tags to expand (expand-boxes required)
            'highlight'         =>  $_GET['search'],                // highlight patterns in content
            'html_class'         =>  $html,
            'html_return'         =>  1
        )
        /*
        'cols'          =>  array(
            // Define columns here...
            array( 'caption' => "",                       'width' => '10%',   'col' => '0',   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' ),
            array( 'caption' => LANG_LIT_SEARCH_TITLE,    'width' => '30%',   'col' => '1',   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => '' ),
            array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => '2',   'sortable' => false, 'sort_col' => 'b.author',     'content_align' => '' ),
            array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => '3',   'sortable' => false, 'sort_col' => 'b.link_date',     'content_align' => '' )
        )
        */
    );

    $options_table['cols']=array();
    $i_col=0;
    $options_table['cols'][]=array( 'caption' => "",                       'width' => '10%',   'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_TITLE,    'width' => '30%',   'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => '' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.author',     'content_align' => '' );
    /*
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '29%',     'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.author',     'content_align' => '' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_RIGHT_SEARCH_LIT_CATEGORY,   'width' => '*',     'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.books_catid',     'content_align' => '' );
    */
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.link_date',     'content_align' => '' );

    # get literature data fro table
    $options_table_data=array();
    $options_table_data['html_return']=$options_table['options']['html_return'];
    $options_table_data['cols']=$options_table['cols'];
    $options_table_data['sort']=$options_table['options']['sort_by'];
    $options_table_data['highlight']=$_GET['search'];
    $table_data = new Table_Data($db,$options_table_data);
    $options_table_data=array();
    $options_table_data['results']=$results_literature;
    $rows_literature=$table_data->table_data_literature($options_table_data);
    # END get literature data fro table

    $table = new Table($db, $rows_literature, $options_table);
    print $table->draw_table();

                print "\t<script type='text/javascript'>\n";
                    print "\tvar search=1;\n";
                    //print "\tinit('test');\n";
                print "\t</script>\n";

            print "\t</div>\n";
        }
        if( $count_videos>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_VIDEOS;
            $options['title_section_a']="videos";
            $options['count']=$count_videos;
            $options['count_total']=$count_videos_total;
            $options['limit']=$limit_videos;
            if( $count_videos_total>$limit_videos ) $options['url_all']="/".$_GET['lang']."/videos/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search-videos'>\n";

                $options_videos=array();
                $options_videos['html_return']=1;
                $options_videos['results']=$results_videos;
                $options_videos['values_from']=0;
                //$options_videos['values_to']=$count_videos_total;
                if( $count_videos_total<$limit_videos ) $limit_videos=$count_videos_total;
                $options_videos['values_to']=$limit_videos;
                $options_videos['search']=$_GET['search'];
                $options_videos['show_desc']=1;
                print $html->div_videos($db,$options_videos);

            print "\t</div>\n";
        }
        if( $count_links>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_LINKS;
            $options['title_section_a']="links";
            $options['count']=$count_links;
            $options['count_total']=$count_links_total;
            $options['limit']=$limit_links;
            if( $count_links_total>$limit_links ) $options['url_all']="/".$_GET['lang']."/links/articles/search/?keyword=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search-links'>\n";

                $options_articles=array();
                $options_articles['html_return']=1;
                $options_articles['results']=$results_links;
                $options_articles['search']=$_GET['search'];
                print $html->div_articles($db,$options_articles);

            print "\t</div>\n";
        }
        if( $count_news_talks>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_NEWS_TALKS;
            $options['title_section_a']="talks";
            $options['count']=$count_news_talks;
            $options['count_total']=$count_news_talks_total;
            $options['limit']=$limit_news_talks;
            if( $count_news_talks_total>$limit_news_talks ) $options['url_all']="/".$_GET['lang']."/news/talks/?search=".urlencode(UTILS::strip_slashes_recursive($_GET['search']));
            h1_html($db,$options);
            print "\t<div class='table-basic-search-news'>\n";
                $options_news_talks=array();
                $options_news_talks['html_return']=1;
                $options_news_talks['results']=$results_news_talks;
                $options_news_talks['count']=$count_news_talks;
                $options_news_talks['search']=$_GET['search'];
                $options_news_talks['strip_tags']=1;
                $options_news_talks['class']['new-search']="new-search";
                print $html->div_news($db,$options_news_talks);
                print "\t<div class='clearer'></div>\n";
            print "\t</div>\n";
        }
        if( $count_news_auctions>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_NEWS_AUCTIONS;
            $options['title_section_a']="auctions";
            $options['count']=$count_news_auctions;
            $options['count_total']=$count_news_auctions_total;
            $options['limit']=$limit_news_auctions;
            if( $count_news_auctions_total>$limit_news_auctions_talks ) $options['url_all']="/".$_GET['lang']."/auctions/?search=".$search_for_url;
            h1_html($db,$options);

            $table = new Table($db, $rows_auctions, $options_auctions_table);
            print $table->draw_table();

        }
        if( $count_text_credits>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_CREDITS;
            $options['title_section_a']="credits";
            $options['count']=$count_text_credits;
            h1_html($db,$options);
            print "\t<div class='table-basic-search-text-credits'>\n";
                while( $row=$db->mysql_array($results_text_credits) )
                {
                    $row=UTILS::html_decode($row);
                    $text=UTILS::row_text_value($db,$row,"text");
                    $text_no_html=strip_tags($text);

                    $options=array();
                    $options['search']=$_GET['search'];
                    $options['text']=$text;
                    //$options['type']=1;
                    //$options['limit']=14;
                    //$options['show_dots']=1;
                    $options['a_name']=1;
                    $text=UTILS::show_search_found_keywords($db,$options);

                    $url="/".$_GET['lang']."/credits/?search=".$search_for_url;
                    print "\t<div class='table-item-basic-search'>\n";
                        print UTILS::admin_edit("/admin/pages/edit/?textid=".$row['textid']);
                        print "\t<a href='".$url."' title=''>\n";
                            print "\t<span class='span-basic-search-text'>".$text."</span>\n";
                        print "\t</a>\n";
                    print "\t</div>\n";
                }
                print "\t<div class='clearer'></div>\n";
            print "\t</div>\n";
        }
        if( $count_text_disclaimer>0 )
        {
            $options=array();
            $options['title_section']=LANG_SEARCH_DISCLAIMER;
            $options['title_section_a']="disclaimer";
            $options['count']=$count_text_disclaimer;
            h1_html($db,$options);
            print "\t<div class='table-basic-search-text-disclaimer'>\n";
                while( $row=$db->mysql_array($results_text_disclaimer) )
                {
                    $row=UTILS::html_decode($row);
                    $options_row_text=array();
                    $options_row_text['search']=$_GET['search'];
                    $text=UTILS::row_text_value($db,$row,"text",$options_row_text);
                    $text_no_html=strip_tags($text);

                    $options=array();
                    $options['search']=$_GET['search'];
                    $options['text']=$text;
                    $options['type']=1;
                    $options['limit']=80;
                    $options['show_dots']=1;
                    $options['a_name']=1;
                    $text=UTILS::show_search_found_keywords($db,$options);

                    $url="/".$_GET['lang']."/disclaimer/?search=".$search_for_url;
                    print "\t<div class='table-item-basic-search'>\n";
                        print UTILS::admin_edit("/admin/pages/edit/?textid=".$row['textid']);
                        print "\t<a href='".$url."' title=''>\n";
                            print "\t<span class='span-basic-search-text'>".$text."</span>\n";
                        print "\t</a>\n";
                    print "\t</div>\n";
                }
                print "\t<div class='clearer'></div>\n";
            print "\t</div>\n";
        }

    }
    else
    {
        print "\t<p class='p-basic-search-no-results-found'>\n";
            print LANG_SEARCH_NORESULTS.".";
        print "\t</p>\n";
    }

//print "\t</div>\n";


}
else
{
    //print "\tType to search!\n";
}

print "\t<div class='clearer'></div>\n";

# print out debug search queries

if( $_SESSION['debug_page'] && $count_paintings ) print "<hr /><h2 style='font-weight:700;'>PAINTINGS</h2><br />".$query_paintings['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_microsites )
{
    print "<hr /><h2 style='font-weight:700;'>MICROSITES - SNOW WHITE</h2><br />".$query_snow_white['query']."<br /><br />";
    print "<hr /><h2 style='font-weight:700;'>MICROSITES - WAR CUT</h2><br />".$query_war_cut['query']."<br /><br />";
    print "<hr /><h2 style='font-weight:700;'>MICROSITES - FIRENZE</h2><br />".$query_firenze['query']."<br /><br />";
    print "<hr /><h2 style='font-weight:700;'>MICROSITES</h2><br />".$query_microsites['query']."<br /><br />";
}

if( $_SESSION['debug_page'] && $count_biography ) print "<hr /><h2 style='font-weight:700;'>BIOGRAPHY</h2><br />".$query_biography['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_text_chronology ) print "<hr /><h2 style='font-weight:700;'>CHRONOLOGY</h2><br />".$query_text_chronology['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_quotes ) print "<hr /><h2 style='font-weight:700;'>QUOTES</h2><br />".$query_quotes['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_exhibitions ) print "<hr /><h2 style='font-weight:700;'>EXHIBITIONS</h2><br />".$query_exhibitions['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_literature ) print "<hr /><h2 style='font-weight:700;'>LITERATURE</h2><br />".$query_literature['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_videos ) print "<hr /><h2 style='font-weight:700;'>VIDEOS</h2><br />".$query_videos['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_links ) print "<hr /><h2 style='font-weight:700;'>LINKS</h2><br />".$query_links['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_news_auctions ) print "<hr /><h2 style='font-weight:700;'>AUCTIONS</h2><br />".$query_match_auctionhouse['query']."<br />".$rows_auctions['query_sales']."<br /><br />";

if( $_SESSION['debug_page'] && $count_text_credits ) print "<hr /><h2 style='font-weight:700;'>CREDITS</h2><br />".$query_text_credits['query']."<br /><br />";

if( $_SESSION['debug_page'] && $count_text_disclaimer ) print "<hr /><h2 style='font-weight:700;'>DISCLAIMER</h2><br />".$query_text_disclaimer['query']."<br /><br />";
?>
