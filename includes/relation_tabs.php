<?php
function div_relation_tabs($db,$options=array())
{
    $return=array();
    $html_print="";
    $tab_number=0;

    if( !empty($options['typeid']) ) $typeid=$options['typeid'];
    else $typeid=1;
    if( !empty($options['itemid']) ) $itemid=$options['itemid'];
    else $itemid=$options['row']['paintID'];

    $query_paintings=QUERIES::query_painting($db);

    /****** TABS FOR LITERATURE *****/
    if( !empty($options['bookid']) || !empty($options['videoid']) )
    {
        # Information
        //if( !empty($options['info']) )
        if( !empty($options['row']['infoEN']) || !empty($options['row']['infoDE']) || !empty($options['row']['infoFR']) || !empty($options['row']['infoIT']) || !empty($options['row']['infoZH'])
        || !empty($options['row']['info_en']) || !empty($options['row']['info_de']) || !empty($options['row']['info_fr']) || !empty($options['row']['info_it']) || !empty($options['row']['info_zh']) )
        {
            $tab_info=$tab_number;
            $return['tab']['information']=$tab_number;
            //$html_print.="tab_info-".$tab_info."<br />";
            $tab_number++;
        }
        # end Information
    }
    /*END***** TABS FOR LITERATURE *****/

    if( ( !empty($options['exhibitionid']) || !empty($options['bookid']) || !empty($options['videoid']) ) && empty($options['results_artworks']) )
    {
        # Paintings - Artworks

        /*
        $query_artworks="SELECT ".$query_paintings['query_columns']."
            FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p
            WHERE
                ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=1 )
                    OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=1 ) )
                AND ( ( r.itemid2=p.paintID AND r.typeid2=1 )
                    OR ( r.itemid1=p.paintID AND r.typeid1=1 ) )
                AND p.enable=1 ";
        */

        $query_artworks="
            SELECT
              ".$query_paintings['query_columns'].",
              COALESCE(
                r1.itemid2,
                r2.itemid1
              ) as itemid,
              COALESCE(
                r1.sort1,
                r2.sort1
              ) as sort_rel,
              COALESCE(
                r1.relationid,
                r2.relationid
              ) as relationid
            FROM
              ".TABLE_PAINTING." p
            LEFT JOIN
              ".TABLE_RELATIONS." r1 ON r1.typeid1 = '".$typeid."' and r1.itemid1='".$itemid."' and r1.typeid2 = 1 and r1.itemid2 = p.paintID
            LEFT JOIN
              ".TABLE_RELATIONS." r2 ON r2.typeid2 = '".$typeid."' and r2.itemid2='".$itemid."' and r2.typeid1 = 1 and r2.itemid1 = p.paintID
            WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL ) AND p.enable=1 ";

        $query_artworks.=" AND p.artworkID!=16 "; # do not display artworkID 16(Prints) while Joe hasnt allowed this

        //if( !empty($options['videoid']) ) $query_artworks.=" ORDER BY r.sort1 ASC, r.relationid DESC ";
        if( !empty($options['videoid']) ) $query_artworks.=" ORDER BY sort_rel ASC, relationid DESC ";
        else $query_artworks.=" ORDER BY p.sort2 ASC, titleEN ASC ";

        //print $query_artworks;
        $results_artworks=$db->query($query_artworks);
        $count_artworks=$db->numrows($results_artworks);
        //$count_artworks=0;
        if( $count_artworks )
        {
            $tab_artworks=$tab_number;
            $return['tab']['artworks']=$tab_number;
            //$html_print.="tab_artworks-".$tab_artworks."<br />";
            $tab_number++;
        }
        # end Paintings - Artworks
    }
    elseif( count($options['results_artworks'])>0 )
    {
        $results_artworks=$options['results_artworks'];
        $count_artworks=count($options['results_artworks']);
        $tab_artworks=$tab_number;
        $return['tab']['artworks']=$tab_number;
        //$html_print.="tab_artworks-".$tab_artworks."<br />";
        $tab_number++;
    }

    /****** TABS FOR ARTWORK *****/
    if( !empty($options['row']['paintID']) )
    {
        # artwork_notes && notes images
        $artwork_notes=UTILS::row_text_value2($db,$options['row'],"artwork_notes");

        if( !empty($options['row']['optionid_photograph_courtesy']) && !empty($options['row']['optionid_photograph_courtesy_values']) )
        {
            $options_show_dropdown_title=array();
            $options_show_dropdown_title['html_return']=1;
            $optionid_photograph_courtesy=show_dropdown_option_title($db,$options['row']['optionid_photograph_courtesy'],$options_show_dropdown_title);

            $options_show_dropdown_title=array();
            $options_show_dropdown_title['html_return']=1;
            $optionid_photograph_courtesy_values=show_dropdown_option_title($db,$options['row']['optionid_photograph_courtesy_values'],$options_show_dropdown_title);

            $artwork_notes="<p>".$optionid_photograph_courtesy." ".$optionid_photograph_courtesy_values."</p>".$artwork_notes;
        }

        $query_notes_images=QUERIES::query_painting_notes($db);
        $results_notes_images=$db->query("SELECT noteid,".$query_notes_images['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING_NOTE." pn WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=15 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=15 ) ) AND ( ( r.itemid2=pn.noteid AND r.typeid2=15 ) OR ( r.itemid1=pn.noteid AND r.typeid1=15 ) ) ORDER BY sort ASC, relationid DESC ");
        $count_notes_images=$db->numrows($results_notes_images);
        if( $count_notes_images || !empty($artwork_notes) )
        {
            $tab_artwork_notes=$tab_number;
            $return['tab']['notes_images']=$tab_number;
            //$html_print.="tab_artwork_notes-".$tab_artwork_notes."<br />";
            $tab_number++;
        }
        # end artwork_notes && notes images

        # sale history
        $query_sale_hist=QUERIES::query_sale_history($db);
        $results_sale_hist=$db->query("SELECT ".$query_sale_hist['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_SALEHISTORY." s WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=2 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=2 ) ) AND ( ( r.itemid2=s.saleID AND r.typeid2=2 ) OR ( r.itemid1=s.saleID AND r.typeid1=2 ) ) AND s.enable=1 ORDER BY s.saleDate DESC ");
        $count_sale_hist=$db->numrows($results_sale_hist);
        if( $count_sale_hist )
        {
            $tab_artwork_sales_history=$tab_number;
            $return['tab']['sales_history']=$tab_number;
            //$html_print.="tab_artwork_sales_history-".$tab_artwork_sales_history."<br />";
            $tab_number++;
        }
        # end sale history

        # related museums( collections )
        $collection_notes=UTILS::row_text_value($db,$options['row'],"collection_notes");

        $query_columns_museums=QUERIES::query_museums($db);
        $query_museums="SELECT ".$query_columns_museums['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_MUSEUM." m WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=16 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=16 ) ) AND ( ( r.itemid2=m.muID AND r.typeid2=16 ) OR ( r.itemid1=m.muID AND r.typeid1=16 ) ) ORDER BY sort ASC, relationid DESC ";
        //print $query_museums;
        $results_museums=$db->query($query_museums);
        $count_museums=$db->numrows($results_museums);
        if( $count_museums || !empty($collection_notes) || !empty($options['row']['locationid']) )
        {
            if( !empty($options['row']['locationid']) || !empty($collection_notes) )
            {
                $count_museums=1;
                $tab_artwork_collection=$tab_number;
                $return['tab']['collection']=$tab_number;
                $tab_number++;
            }
            else
            {
                $count_museums=0;
            }
        }
        # end related museums( collections )

        # reletaed atlas
        $results_atlas=$db->query("SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=13 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=13 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=13 ) OR ( r.itemid1=p.paintID AND r.typeid1=13 ) ) AND p.artworkID=3 ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ");
        $count_atlas=$db->numrows($results_atlas);
        if( $count_atlas )
        {
            $tab_artwork_atlas=$tab_number;
            $return['tab']['related_atlas']=$tab_number;
            //$html_print.="tab_artwork_atlas-".$tab_artwork_atlas."<br />";
            $tab_number++;
        }
        # end reletaed atlas

        # associated atlas paintings
        $results_atlas_assoc=$db->query("SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=14 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=14 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=14 ) OR ( r.itemid1=p.paintID AND r.typeid1=14 ) ) AND p.enable=1 ORDER BY p.sort2 ASC, titleEN ASC ");
        $count_atlas_assoc=$db->numrows($results_atlas_assoc);
        if( $count_atlas_assoc )
        {
            $tab_artwork_associated_works=$tab_number;
            $return['tab']['associated_atlas_paintings']=$tab_number;
            //$html_print.="tab_artwork_associated_works-".$tab_artwork_associated_works."<br />";
            $tab_number++;
        }
        # end associated atlas paintings
    }
    /*END***** TABS FOR ARTWORK *****/

    /****** TABS FOR ALL SECTIONS *****/

    # exhibitions
    $query_exh=QUERIES::query_exhibition($db);
    $results_exh=$db->query("SELECT ".$query_exh['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_EXHIBITIONS." e WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=4 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=4 ) ) AND ( ( r.itemid2=e.exID AND r.typeid2=4 ) OR ( r.itemid1=e.exID AND r.typeid1=4 ) ) AND e.exID!='".$options['exhibitionid']."' AND e.enable=1 ORDER BY e.date_from DESC ");
    $count_exh=$db->numrows($results_exh);
    //if( $count_exh && $options['row']['artworkID']!=4 ) # do not display exhibitions tab for editions
    if( $count_exh && $options['row']['artworkID']!=16 ) # do not display exhibitions tab for prints
    {
        $tab_artwork_exhibitions=$tab_number;
        $return['tab']['exhibitions']=$tab_number;
        //$html_print.="tab_artwork_exhibitions-".$tab_artwork_exhibitions."-count=".$count_exh."<br />";
        //print $tab_number;
        $tab_number++;
    }
    else
    {
        $count_exh=0;
    }
    # end exhibitions

    ### INSTALLATION SHOTS tab content
    if( !empty($options['exhibitionid']) )
    {
        $results_installations_relations=$db->query("SELECT typeid1,itemid1,typeid2,itemid2 FROM ".TABLE_RELATIONS." WHERE ( typeid1=".$typeid." AND itemid1='".$itemid."' AND typeid2=5 ) OR ( typeid2=".$typeid." AND itemid2='".$itemid."' AND typeid1=5 ) LIMIT 1 ");
        $count_installations_relations=$db->numrows($results_installations_relations);
        if( $count_installations_relations>0 )
        {
            $tab_installation_number=$tab_number;
            $return['tab']['installation_photos']=$tab_number;
            //$html_print.="tab_installation_number-".$tab_installation_number."<br />";
            $tab_number++;
        }
    }
    #end

    # literature
    if( !empty($options['results_literature']) )
    {
        $results_literature=$options['results_literature'];
    }
    else
    {
        $query_literature=QUERIES::query_books($db);
        //$query_relations_literature="SELECT ".$query_literature['query_columns'].",relationid FROM ".TABLE_RELATIONS." r, ".TABLE_BOOKS." b WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=12 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=12 ) ) AND ( ( r.itemid2=b.id AND r.typeid2=12 ) OR ( r.itemid1=b.id AND r.typeid1=12 ) ) AND b.enable=1 ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC ";
        $query_relations_literature="SELECT
                      ".$query_literature['query_columns'].",
                      b.title AS title_delete,
                      COALESCE(
                        r1.itemid2,
                        r2.itemid1
                      ) as itemid,
                      COALESCE(
                        r1.sort,
                        r2.sort
                      ) as sort_rel,
                      COALESCE(
                        r1.relationid,
                        r2.relationid
                      ) as relationid
                    FROM
                      ".TABLE_BOOKS." b
                    LEFT JOIN
                      ".TABLE_RELATIONS." r1 ON r1.typeid1 = '".$typeid."' and r1.itemid1='".$itemid."' and r1.typeid2 = 12 and r1.itemid2 = b.id
                    LEFT JOIN
                      ".TABLE_RELATIONS." r2 ON r2.typeid2 = '".$typeid."' and r2.itemid2='".$itemid."' and r2.typeid1 = 12 and r2.itemid1 = b.id
                    WHERE ( r1.itemid2 IS NOT NULL OR r2.itemid2 IS NOT NULL ) AND b.enable=1
                ";

        //if( $_SESSION['debug_page'] ) print_r($_GET);

        if( !empty($options['videoid']) && empty($_GET['sort']) )
        {
            $query_relations_literature.=" ORDER BY sort_rel ASC, relationid DESC ";
        }
        elseif( $options['exhibitionid'] )
        {
            $query_relations_literature.=" ORDER BY FIELD(b.books_catid, 2, 3, 11) DESC ,DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC ";
        }
        else
        {
            if( !empty($_GET['sort']) )
            {
                $tmp=explode(",", $_GET['sort']);
                $_GET['sort']=$tmp[0];
                if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
                else $_GET['sort_how']=1;
            }
            else
            {
                //$_GET['sort']=3;
                $_GET['sort']=4;
                $sort="link_date";
                $_GET['sort_how']=-1;
                $sort_how="DESC";
            }

            # sort
            if( $_GET['order_by']=="title" ) $_GET['sort']=1;
            elseif( $_GET['order_by']=="author" ) $_GET['sort']=2;
            elseif( $_GET['order_by']=="link_date" || $_GET['order_by']=="year" ) $_GET['sort']=3;
            /*
            elseif( $_GET['order_by']=="books_catid" ) $_GET['sort']=3;
            elseif( $_GET['order_by']=="link_date" || $_GET['order_by']=="year" ) $_GET['sort']=4;
            */

            if( $_GET['sort']==1 ) $sort="title";
            elseif( $_GET['sort']==2 ) $sort="author";
            elseif( $_GET['sort']==3 ) $sort="link_date";
            /*
            elseif( $_GET['sort']==3 ) $sort="books_catid";
            elseif( $_GET['sort']==4 ) $sort="link_date";
            */
            # sort how
            if( $_GET['order_how']=="DESC" ) $_GET['sort_how']=-1;
            elseif( $_GET['order_how']=="ASC" ) $_GET['sort_how']=1;
            if( $_GET['sort_how']==1 ) $sort_how="ASC";
            elseif( $_GET['sort_how']==-1 ) $sort_how="DESC";

            $url="&amp;sort=".$_GET['sort'].",".$_GET['sort_how'];
            # END sort

            # author order
            $sql_order_by_author=" isnull ASC, b.author ";
            # end

            if( $sort=="link_date" ) $sql_order_by=" DATE_FORMAT(b.link_date, '%Y') ";
            else $sql_order_by=$sort;
            $query.=" ORDER BY ";
            if( $sort=="author" ) $query.=$sql_order_by_author." ".$sort_how;
            else $query.=$sql_order_by." ".$sort_how;
            if( $sort!="author" ) $query.=", ".$sql_order_by_author;

            $query_relations_literature.=$query;

            //$query_relations_literature.=" ORDER BY DATE_FORMAT(b.link_date, '%Y') DESC, isnull ASC, b.author ASC ";
        }
        if( $_SESSION['debug_page'] ) $html_print.=$query_relations_literature;

        $results_literature=$db->query($query_relations_literature);
    }

    $count_literature=$db->numrows($results_literature);
    //$html_print.=$count_literature;
    if( $count_literature )
    {
        //print $tab_number;
        $tab_artwork_literature=$tab_number;
        $return['tab']['literature']=$tab_number;
        //$html_print.="tab_artwork_literature-".$tab_artwork_literature."<br />";
        $tab_number++;
    }
    # end literature

    # videos
    $query_videos=QUERIES::query_videos($db);
    $results_videos=$db->query("SELECT ".$query_videos['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_VIDEO." v WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=10 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=10 ) ) AND ( ( r.itemid2=v.videoID AND r.typeid2=10 ) OR ( r.itemid1=v.videoID AND r.typeid1=10 ) ) AND v.enable=1 ORDER BY v.categoryid ASC, v.sort ASC ");
    $count_videos=$db->numrows($results_videos);
    if( $count_videos )
    {
        $tab_artwork_videos=$tab_number;
        $return['tab']['videos']=$tab_number;
        //$html_print.="tab_artwork_videos-".$tab_artwork_videos."<br />";
        $tab_number++;
    }
    # end videos

    # painting_images
    $query_painting_images=QUERIES::query_painting_images($db);
    $query_painting_images_relations="SELECT ".$query_painting_images['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING_PHOTOS." pp WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=24 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=24 ) ) AND ( ( r.itemid2=pp.painting_photoid AND r.typeid2=24 ) OR ( r.itemid1=pp.painting_photoid AND r.typeid1=24 ) ) AND pp.enable=1 ORDER BY r.sort ASC, r.relationid DESC ";
    $results_painting_images=$db->query($query_painting_images_relations);
    //print $query_painting_images_relations;
    $count_painting_images=$db->numrows($results_painting_images);
    if( $count_painting_images )
    {
        $tab_artwork_photos=$tab_number;
        $return['tab']['artwork_photos']=$tab_number;
        //$html_print.="tab_artwork_photos-".$tab_artwork_photos."<br />";
        $tab_number++;
    }
    # end painting_images

    /*END***** TABS FOR ALL SECTIONS *****/

    if( !empty($options['row']['paintID']) )
    {
        # Individual works
        $results_editions_ind=$db->query("SELECT ".$query_paintings['query_columns']." FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=".$typeid." AND r.itemid1='".$itemid."' AND r.typeid2=25 ) OR ( r.typeid2=".$typeid." AND r.itemid2='".$itemid."' AND r.typeid1=25 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=25 ) OR ( r.itemid1=p.paintID AND r.typeid1=25 ) ) AND p.enable=1 ORDER BY p.sort2 ASC, titleEN ASC ");
        $count_editions_ind=$db->numrows($results_editions_ind);
        //$count_editions_ind=0;
        if( $count_editions_ind )
        {
            $tab_editions_ind=$tab_number;
            $return['tab']['editions_individual']=$tab_number;
            //$html_print.="tab_editions_ind-".$tab_editions_ind."<br />";
            $tab_number++;
        }
        # end Individual works
    }

    /****** TABS FOR EXHIBITION *****/
    if( !empty($options['exhibitionid']) )
    {
        # GUIDE TAB
        if( !empty($options['row']['src_guide']) )
        {
            $tab_guide=$tab_number;
            $return['tab']['guide']=$tab_number;
            $tab_number++;
        }
    }
    /*END***** TABS FOR EXHIBITION *****/

    //$html_print.=$tab_number;

    $return['tab_count']=$tab_number;

    if( !empty($artwork_notes) || $count_notes_images>0 || $count_sale_hist>0 || $count_museums>0 || $count_atlas>0 || $count_atlas_assoc>0 || ( $count_exh && $options['row']['artworkID']!=4 ) || $count_videos>0 || $count_installations_relations>0 || $count_literature>0 || $count_painting_images || $count_editions_ind>0 || !empty($options['info']) || $count_artworks>0 || !empty($options['row']['src_guide']) )
    {
        if( $options['painting_detail_view_tab'] ) $name_tabs="tabs-artwork";
        else $name_tabs="tabs";

        $html_print.="<script type='text/javascript'>\n";
            $html_print.="$(function() {\n";
                $tab_id_name="#".$options['id']."";
                $html_print.="$( '".$tab_id_name."' ).tabs({\n";
                    $html_print.="cache: false,";
                    if( $_GET[$options['tab']]=="information-".$name_tabs && !empty($options['info']) ) $html_print.="active: ".$tab_info.",\n";
                    elseif( $_GET[$options['tab']]=="artworks-".$name_tabs && $count_artworks>0 ) $html_print.="active: ".$tab_artworks.",\n";
                    elseif( $_GET[$options['tab']]=="notes-".$name_tabs && !empty($artwork_notes) ) $html_print.="active: ".$tab_artwork_notes.",\n";
                    elseif( $_GET[$options['tab']]=="sales-history-".$name_tabs && $count_sale_hist>0 ) $html_print.="active: ".$tab_artwork_sales_history.",\n";
                    elseif( $_GET[$options['tab']]=="collection-".$name_tabs && $count_museums>0 ) $html_print.="active: ".$tab_artwork_collection.",\n";
                    elseif( $_GET[$options['tab']]=="atlas-".$name_tabs && $count_atlas>0 ) $html_print.="active: ".$tab_artwork_atlas.",\n";
                    elseif( $_GET[$options['tab']]=="associated-works-".$name_tabs && $count_atlas_assoc>0 ) $html_print.="active: ".$tab_artwork_associated_works.",\n";
                    //elseif( $_GET[$options['tab']]=="exhibitions-".$name_tabs && $count_exh && $options['row']['artworkID']!=4 ) $html_print.="active: ".$tab_artwork_exhibitions.",\n";
                    elseif( $_GET[$options['tab']]=="exhibitions-".$name_tabs && $count_exh ) $html_print.="active: ".$tab_artwork_exhibitions.",\n";
                    elseif( $_GET[$options['tab']]=="videos-".$name_tabs && $count_videos>0 ) $html_print.="active: ".$tab_artwork_videos.",\n";
                    elseif( $_GET[$options['tab']]=="installation-views-".$name_tabs && $count_installations_relations>0 ) $html_print.="active: ".$tab_installation_number.",\n";
                    elseif( $_GET[$options['tab']]=="literature-".$name_tabs && $count_literature>0 ) $html_print.="active: ".$tab_artwork_literature.",\n";
                    elseif( $_GET[$options['tab']]=="photos-".$name_tabs && $count_painting_images ) $html_print.="active: ".$tab_artwork_photos.",\n";
                    elseif( $_GET[$options['tab']]=="individual-works-".$name_tabs && $count_editions_ind ) $html_print.="active: ".$tab_editions_ind.",\n";
                    elseif( $_GET[$options['tab']]=="guide-".$name_tabs && !empty($options['row']['src_guide']) ) $html_print.="active: ".$tab_guide.",\n";
                    $html_print.="beforeActivate: function(event, ui)\n";
                    $html_print.="{\n";

                        # change the browser url to make sure if page is reloaded current active tad is preselected
                        if( empty($options['url_painting']) ) $options['url_painting']="?";
                        $html_print.="pageurl = \"".$options['url_painting'];
                        $html_print.="&".$options['tab']."=\"+ui.newTab[0].children[0].rel;\n";
                        //$html_print.='console.debug(ui.newTab[0].children[0].rel);';
                        //$html_print.='active_tab_href = ui.newTab[0].children[0].href;';
                        # we check if browser is mobile and if it is we do not change browser url
                        # for user to be able to navigaete back with one click
                        $html_print.="if (!/Mobi/.test(navigator.userAgent)){";
                            $html_print.="History.pushState(null, document.title, pageurl);";
                        $html_print.="}";
                    $html_print.="}";
                $html_print.="});\n";
            $html_print.="});\n";
            //$html_print.="var active_tab = $( '".$tab_id_name."' ).tabs( 'option', 'active' );";
            if( $options['exhibitionid'] )
            {
                if( $_GET[$options['tab']]=="exhibitions-".$name_tabs ) $html_print.="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-related-exh').addClass('selected');";
                elseif( $_GET[$options['tab']]=="installation-views-".$name_tabs ) $html_print.="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-inst-shots').addClass('selected');";
                elseif( $_GET[$options['tab']]=="literature-".$name_tabs ) $html_print.="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-literature').addClass('selected');";
                elseif( $_GET[$options['tab']]=="videos-".$name_tabs ) $html_print.="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-videos').addClass('selected');";
                elseif( $_GET[$options['tab']]=="guide-".$name_tabs ) $html_print.="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-guide').addClass('selected');";
            }

        $html_print.="</script>\n";

        $html_print.="<a name='".$name_tabs."'></a>\n";

        $html_print.="<div id='".$options['id']."' class='".$options['class']['div-relation-tabs']."'>\n";
            //$html_print.="<a name='tabs-artwork'></a>\n";
            //$html_print.="<div class='tabs'>\n";
                $html_print.="<ul class='ul-painting-relation-tabs ul-tabs'>\n";

                    # if tab is selected change lang url tab
                    $onclick_tab_lang_change="";
                    //$onclick_tab_lang_change.="console.debug(event.target.rel);"; # which tab selected
                    //$onclick_tab_lang_change.="console.debug($('#language a'));";
                    $onclick_tab_lang_change.="for (i = 0; i < $('#language a').length; i++) {";
                        $onclick_tab_lang_change.="$('#language a')[i].search=replaceQueryParam('".$options['tab']."', event.target.rel, $('#language a')[0].search);";
                    $onclick_tab_lang_change.="};";
                    # END if tab is selected change lang url tab

                    /****** FOR LITERATURE *****/

                    # information
                    if( !empty($options['info']) )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-information-".$name_tabs."' class='' title='".LANG_LIT_SEARCH_INFORMATION."' rel='information-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_LIT_SEARCH_INFORMATION."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end information

                    # artworks
                    if( $count_artworks>0 )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-artwork').addClass('selected');";
                        }
                        else $onclick_tab="";
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-artworks-".$name_tabs."' class='' title='".LANG_TAB_TITLE_ARTWORKS."' rel='artworks-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".LANG_TAB_TITLE_ARTWORKS."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end artworks

                    /****** FOR ARTWORK *****/

                    # artwork notes
                    if( !empty($artwork_notes) || $count_notes_images>0 )
                    {
                        $html_print.="<li class='first'>\n";
                            $html_print.="<a href='#tab-notes-".$name_tabs."' class='' title='".LANG_NOTES."' rel='notes-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_NOTES."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end artwork notes
                    # sale history
                    if( $count_sale_hist>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-sales-history-".$name_tabs."' class='' title='".LANG_SALES_HISTORY."' rel='sales-history-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_SALES_HISTORY."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end sale history
                    # museums
                    if( $count_museums>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-collection-".$name_tabs."' class='' title='".LANG_COLLECTION."' rel='collection-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_COLLECTION."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end museums
                    # atlas
                    if( $count_atlas>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-atlas-".$name_tabs."' class='' title='".LANG_LEFT_SIDE_ATLAS."' rel='atlas-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_LEFT_SIDE_ATLAS."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end atlas
                    # associated atlas paintings
                    if( $count_atlas_assoc>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-associated-works-".$name_tabs."' class='' title='".LANG_ARTWORK_TEXT_ATLAS_ASOCIATED."' rel='associated-works-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_ARTWORK_TEXT_ATLAS_ASOCIATED."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end associated atlas paintings
                    # exhibitions
                    //if( $count_exh>0 && $options['row']['artworkID']!=4 )
                    if( $count_exh>0 )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            if( $tab_number>=5 && $_GET['lang']=="de" ) $title_tab=substr(LANG_EXH_RELATED_EXHIBITIONS,0,20)."...";
                            else $title_tab=LANG_EXH_RELATED_EXHIBITIONS;
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-related-exh').addClass('selected');";
                        }
                        else
                        {
                            $title_tab=LANG_HEADER_MENU_EXHIBITIONS;
                            $onclick_tab="";
                        }
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-exhibitions-".$name_tabs."' class='' title='".$title_tab."' rel='exhibitions-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".$title_tab."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end exhibitions
                    # installation views
                    if( $count_installations_relations )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-inst-shots').addClass('selected');";
                        }
                        else $onclick_tab="";
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-installation-views-".$name_tabs."' class='' title='".LANG_EXH_INST_SHOTS."' rel='installation-views-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".LANG_EXH_INST_SHOTS."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # END installation views
                    # literature
                    if( $count_literature>0 )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-literature').addClass('selected');";
                        }
                        else $onclick_tab="";
                        $html_print.="<li data-tab-count='".$tab_artwork_literature."'>\n";
                            $html_print.="<a href='#tab-literature-".$name_tabs."' class='' title='".$tab_title_literature."' rel='literature-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".LANG_ARTWORK_BOOKS_REL_TAB."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end literature
                    # videos
                    if( $count_videos>0 )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-videos').addClass('selected');";
                        }
                        else $onclick_tab="";
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-videos-".$name_tabs."' class='' title='".LANG_HEADER_MENU_VIDEOS."' rel='videos-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".LANG_HEADER_MENU_VIDEOS."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end videos
                    # painting_images
                    if( $count_painting_images>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-photos-".$name_tabs."' class='' title='".LANG_ARTWORK_PHOTOS_TAB."' rel='photos-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_ARTWORK_PHOTOS_TAB."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end painting_images
                    # individual works
                    if( $count_editions_ind>0 )
                    {
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-individual-works-".$name_tabs."' class='' title='".LANG_ARTWORK_INDIVIDUAL_WORKS_TAB."' rel='individual-works-".$name_tabs."' onclick=\"".$onclick_tab_lang_change."\">".LANG_ARTWORK_INDIVIDUAL_WORKS_TAB."</a>\n";
                        $html_print.="</li>\n";
                    }
                    # end individual works

                    /****** FOR EXHIBITION *****/
                    if( !empty($options['row']['src_guide']) )
                    {
                        if( !empty($options['exhibitionid']) )
                        {
                            $onclick_tab="$('#sub-nav a.selected').removeClass('selected');$('a.a-exh-left-guide').addClass('selected');";
                        }
                        else $onclick_tab="";
                        $html_print.="<li>\n";
                            $html_print.="<a href='#tab-guide-".$name_tabs."' class='' title='".LANG_EXH_GUIDE."' rel='guide-".$name_tabs."' onclick=\"".$onclick_tab_lang_change.$onclick_tab."\">".LANG_EXH_GUIDE."</a>\n";
                        $html_print.="</li>\n";
                    }

                $html_print.="</ul>\n";
                //$html_print.="<div class='clearer'></div>\n";
            //$html_print.="</div>\n";
            $html_print.="<div class='clearer'></div>\n";

            /****** FOR LITERATURE *****/

            # information
            if( !empty($options['info']) )
            {
                $html_print.="<div id='tab-information-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    # highlight search keywords
                    $options_search_found=array();
                    $options_search_found['text']=$options['info'];
                    $options_search_found['strip_tags']=1;
                    $options_search_found['search']=$_GET['search'];
                    $options_search_found['convert_utf8']=1;
                    $info=UTILS::show_search_found_keywords($db,$options_search_found);
                    # print in div_text_wysiwyg
                    $options_div_text=array();
                    $html_print.=$options['html_class']->div_text_wysiwyg($db,$info,$options_div_text);
                $html_print.="</div>\n";
            }

            # artworks
            if( $count_artworks>0 )
            {
                $thumb_array=array();
                if( is_array($results_artworks) )
                {
                    $thumb_array=$results_artworks;
                    //print "here";
                }
                else
                {
                    $i=0;
                    while( $row_artworks=$db->mysql_array($results_artworks) )
                    {
                        $thumb_array[$i]=$row_artworks;
                        $i++;
                    }
                }

                $html_print.="<div id='tab-artworks-".$name_tabs."' data-count='".$count_artworks."' class='".$options['class']['div-relation-tabs-info']." ".$options['class']['div-tab-vid-paint']."'>\n";

                    # showing exhibition painting detail view
                    if( $options['exhibitionid'] && !empty($_GET['section_2']) )
                    {
                        /*
                        $query_exh_paintings="SELECT * FROM ".TABLE_RELATIONS." r, ".TABLE_PAINTING." p WHERE ( ( r.typeid1=4 AND r.itemid1='".$options['exhibitionid']."' AND r.typeid2=1 ) OR ( r.typeid2=4 AND r.itemid2='".$options['exhibitionid']."' AND r.typeid1=1 ) ) AND ( ( r.itemid2=p.paintID AND r.typeid2=1 ) OR ( r.itemid1=p.paintID AND r.typeid1=1 ) ) AND p.enable=1 ";
                        $query_exh_paintings.=" AND p.artworkID!=16 "; # do not display artworkID 16(Prints) while Joe hasnt allowed this
                        $query_exh_paintings.=" ORDER BY p.sort2 ASC, p.titleDE ASC, p.titleEN ASC ";

                        $results_exh_paintings=$db->query($query_exh_paintings);
                        $count_exh_paintings=$db->numrows($results_exh_paintings);
                        */
                        $results_exh_paintings=$thumb_array;
                        $count_exh_paintings=count($thumb_array);

                        # detail info about painting
                        $options_paint_info=array();
                        $options_paint_info['painting_no_section']=1;
                        $options_paint_info['html_return']=$options['html_return'];
                        $options_paint_info['row']=$options['row_painting'];
                        $options_paint_info['exhibitionid']=$options['exhibitionid'];
                        $options_paint_info['url1']="/".$_GET['lang']."/exhibitions/".$_GET['section_1'];
                        $options_paint_info['url_after']="#tabs";
                        $options_paint_info['search_vars']=array('with_sp'=>"&p=".$_GET['p']."&sp=".$_GET['sp']);
                        $options_paint_info['url_back']=$options_paint_info['url1']."/?".$options_paint_info['search_vars']['with_sp'];
                        $options_paint_info['back_text']=LANG_EXH_SHOW_ALL_WORKS;
                        $options_paint_info['results']['results_total']=$results_exh_paintings;
                        $options_paint_info['results']['count']=$count_exh_paintings;
                        $options_paint_info['id']['tabs']="tabs-exhibition-detail";
                        $options_paint_info['class']['div-tabs-headline-exh']="div-tabs-headline-exh";
                        $options_paint_info['class']['div-tabs-headline']="div-tabs-headline";
                        $options_paint_info['class']['div-painting-info']="div-painting-info-exh";
                        //$options_paint_info['class']['div-painting-info-img-exh']="div-painting-info-img-exh";
                        $html_print.=$options['html_class']->div_painting_info($db,$options['row_painting'],$options_paint_info);
                        # END detail info about painting

                        # relation tabs
                        $options_relations=array();
                        $options_relations['html_return']=$options['html_return'];
                        $options_relations['tab']="tab-artwork";
                        $options_relations['painting_detail_view_tab']=1;
                        $options_relations['row']=$options['row_painting'];
                        $options_relations['id']="div-painting-relation-tabs";
                        $options_relations['html_class']=$options['html_class'];
                        $options_relations['thumb_per_line_videos']=$options['thumb_per_line_videos'];
                        $options_relations['thumb_per_line_photos']=$options['thumbs_per_line'];
                        $options_relations['thumb_per_line_atlas']=$options['thumbs_per_line'];
                        $options_relations['thumb_per_line_notes']=$options['thumbs_per_line'];
                        $options_relations['url_painting']=$options_paint_info['url1']."/".$options['row_painting']['titleurl']."/?";
                        $options_relations['url_after']="#tabs";
                        $options_relations['content_large']=1;
                        $options_relations['class']['div-relation-tabs']="div-painting-relation-tabs-paint-detail";
                        $options_relations['class']['div-relation-tabs-info']="div-book-detail-tab-info";
                        $options_relations['class']['p-section-desc-text']="p-literature-section-desc-text";
                        $html_print.=div_relation_tabs($db,$options_relations);
                        # end
                    }
                    # END showing exhibition painting detail view

                    # showing exhibition painting thumbs
                    else
                    {
                        if( !$options['videoid'] )
                        {
                            $html_print.="<div class='div-exhibitions-detail-tab-sort'>\n";
                                if( count($thumb_array)==1 ) $item_text_value=LANG_EXH_ARTWORK_TAB_WORK;
                                else $item_text_value=LANG_EXH_ARTWORK_TAB_WORKS;
                                $html_print.="<p class='p-exh-detail-count-found-items'>";
                                    if( $options['bookid'] )
                                    {
                                        if( $count_artworks>1 ) $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_ARTWORKS;
                                        else $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_ARTWORK;
                                    }
                                    else
                                    {
                                        $desc_text=count($thumb_array)." ".$item_text_value;
                                    }
                                    $html_print.=$desc_text;
                                $html_print.="</p>\n";
                                $onchange="location.href='?".$options['tab']."=artworks-tabs&p=1&sp=".$_GET['sp']."&order-by='+this.options[selectedIndex].value+'#tabs'";
                                $html_print.="<select name='order-by' class='select-exh-detail-order-by' onchange=\"".$onchange."\">\n";
                                    if( $_GET['order-by']=="cr-number" ) $selected="selected='selected'";
                                    else $selected="";
                                    $html_print.="<option value='cr-number' ".$selected.">".LANG_EXH_ARTWORK_SORT_CR_NUMBER."</option>\n";
                                    if( $_GET['order-by']=="year" ) $selected="selected='selected'";
                                    else $selected="";
                                    $html_print.="<option value='year' ".$selected.">".LANG_EXH_ARTWORK_SORT_YEAR."</option>\n";
                                $html_print.="</select>\n";
                                $html_print.="<span class='span-exh-detail-order-by-text'>".LANG_EXH_ARTWORK_SORT_BY."</span>\n";
                                $html_print.="<div class='clearer'></div>\n";
                            $html_print.="</div>\n";
                        }

                        $pages=@ceil(count($thumb_array)/$_GET['sp']);

                        # page_numbering
                        $options_page_numbering=array();
                        $options_page_numbering=$options;
                        $options_page_numbering['per_page']=1;
                        $options_page_numbering['tab']="tab-artwork";
                        $options_page_numbering['pages']=$pages;
                        $options_page_numbering['count']=count($thumb_array);
                        $options_page_numbering['sp']=$_GET['sp'];
                        $options_page_numbering['p']=$_GET['p'];
                        //$options_page_numbering['thumbs_per_line']=5;
                        $url="/".$_GET['lang']."/".$_GET['section_0'];
                        if( !empty($_GET['section_1']) ) $url.="/".$_GET['section_1'];
                        if( !empty($_GET['section_2']) ) $url.="/".$_GET['section_2'];
                        if( !empty($_GET['section_3']) ) $url.="/".$_GET['section_3'];
                        $options_page_numbering['url']=$url."/?tab=artworks-".$name_tabs;
                        $options_page_numbering['url_after'].="&sp=".$_GET['sp'];
                        if( !empty($_GET['order-by']) )
                        {
                            $options_page_numbering['url_after'].="&order-by=".$_GET['order-by'];
                            $options_page_numbering['url_after_per_page'].="&order-by=".$_GET['order-by'];
                        }
                        if( $options['exhibitionid'] || $options['bookid'] )
                        {
                            $options_page_numbering['url_after_per_page'].="&tab=artworks-".$name_tabs;
                        }
                        if( !$options['videoid'] )
                        {
                            $options_page_numbering['url_after'].="#tabs";
                            $options_page_numbering['url_after_per_page'].="#tabs";
                        }
                        $options_page_numbering['class']['div-per-page']="div-per-page-exh";
                        $options_page_numbering['class']['div-page-navigation']="div-page-navigation-exh";
                        if( !$options['videoid'] ) $html_print.=$options['html_class']->page_numbering($db,$options_page_numbering);
                        # END page_numbering

                        # div_thumb_images
                        $options_artworks_thumbs=array();
                        $options_artworks_thumbs=$options_page_numbering;
                        $options_artworks_thumbs['results']=$thumb_array;
                        $options_artworks_thumbs['show_title']=1;
                        $options_artworks_thumbs['htaccess']=1;
                        $options_artworks_thumbs['class']['div-section-categories']="div-thumbs-tab-content";
                        if( $options['bookid'] || $options['videoid'] )
                        {
                            $options_artworks_thumbs['target']="_blank";
                            unset($options_artworks_thumbs['painting_no_section']);
                            $options_artworks_thumbs['url_after']="/?p=1";
                        }
                        else
                        {
                            $options_artworks_thumbs['url_after']="/?p=".$_GET['p'];
                        }
                        if( $options['videoid'] )
                        {
                            $options_artworks_thumbs['style']="width:".(172*$options_page_numbering['count'])."px;";
                            $options_artworks_thumbs['data-test']=$options_page_numbering['count'];
                        }
                        if( !empty($_GET['order-by']) ) $options['url_after'].="&order-by=".$_GET['order-by'];
                        $options_artworks_thumbs['url']=$url;
                        $options_artworks_thumbs['values_from']=($_GET['sp']*($_GET['p']-1));
                        $options_artworks_thumbs['values_to']=$_GET['sp']*$_GET['p'];
                        if( $_GET['sp']=='all' || $options['videoid'] ) $options_artworks_thumbs['show_all']=1;
                        if( $options_artworks_thumbs['values_to']>count($thumb_array) ) $options_artworks_thumbs['values_to']=count($thumb_array);

                        if( $options['videoid'] )
                        {
                            $html_print.="<div class='scrollbar'>";
                        }
                            $html_print.=$options['html_class']->div_thumb_images($db,$options_artworks_thumbs);
                        if( $options['videoid'] )
                        {
                            $html_print.="</div>";
                        }
                        # END div_thumb_images

                        if( $_SESSION['debug_page'] ) $html_print.=$query_artworks;

                        # page_numbering
                        if( !$options['videoid'] ) $html_print.=$options['html_class']->page_numbering($db,$options_page_numbering);
                    }
                    # END showing exhibition painting thumbs

                $html_print.="</div>\n";
            }
            # end artworks

            /****** FOR ARTWORK *****/

            # artwork notes && notes images
            if( !empty($artwork_notes) || $count_notes_images>0 )
            {
                $html_print.="<div id='tab-notes-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    if( !empty($artwork_notes) )
                    {
                        $options_div_text=array();
                        $html_print.=$options['html_class']->div_text_wysiwyg($db,$artwork_notes,$options_div_text);
                    }
                    if( $count_notes_images>0 )
                    {
                        $options_thumb_img=array();
                        $options_thumb_img=$options;
                        $options_thumb_img['results']=$results_notes_images;
                        $options_thumb_img['show_title']=1;
                        $options_thumb_img['show_one_title']=1;
                        //$options_thumb_img['url']="/images/size_xl__imageid_";
                        $options_thumb_img['url']=DATA_PATH_DATADIR."/images_new/xlarge/";
                        $options_thumb_img['lightbox']=1;
                        if( !empty($options['thumb_per_line_notes']) ) $thumbs_per_line_notes=$options['thumbs_per_line_notes'];
                        else $thumbs_per_line_notes=4;
                        $options_thumb_img['thumbs_per_line']=$thumbs_per_line_notes;
                        $options_thumb_img['values_from']=0;
                        $options_thumb_img['values_to']=$count_notes_images;
                        $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
                        $html_print.="<div class='clear-tab'></div>\n";
                    }
                $html_print.="</div>\n";
            }
            # end artwork notes && notes images

            # sale history
            if( $count_sale_hist>0 )
            {
                $html_print.="<div id='tab-sales-history-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $options_sale_hist_list=array();
                    $options_sale_hist_list=$options;
                    $options_sale_hist_list['results']=$results_sale_hist;
                    $html_print.=$options['html_class']->sales_history_list($db,$options_sale_hist_list);
                $html_print.="</div>\n";
            }
            # end sale history

            # museums / collection / collection notes
            if( $count_museums>0 )
            {
                $html_print.="<div id='tab-collection-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $options_location=array();
                    $options_location['row']=$options['row'];
                    $options_location['museum']=1;
                    $options_location['html']=$options['html_class'];
                    $location=location($db,$options_location);
                    $html_print.="<p>";
                        $html_print.=$location;
                    $html_print.="</p>";
                    $options_admin_edit=array();
                    $options_admin_edit=$options;
                    $html_print.=UTILS::admin_edit("/admin/paintings/edit/?paintid=".$itemid."#collection",$options_admin_edit);
                    # collection notes
                    $html_print.="<br /><br />";
                    $options_div_text=array();
                    $html_print.=$options['html_class']->div_text_wysiwyg($db,$collection_notes,$options_div_text);
                $html_print.="</div>\n";
            }
            # end sale history

            # atlas
            if( $count_atlas>0 )
            {
                $html_print.="<div id='tab-atlas-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $html_print.="<p class='p-section-title'>".LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS."</p>";
                    $options_thumb_img=array();
                    $options_thumb_img=$options;
                    $options_thumb_img['results']=$results_atlas;
                    $options_thumb_img['show_title']=1;
                    $options_thumb_img['url']="/art/atlas/atlas.php?paintid=";
                    if( !empty($options['thumb_per_line_atlas']) ) $thumbs_per_line_atlas=$options['thumbs_per_line_atlas'];
                    else $thumbs_per_line_atlas=4;
                    $options_thumb_img['thumbs_per_line']=$thumbs_per_line_atlas;
                    $options_thumb_img['values_from']=0;
                    $options_thumb_img['values_to']=$count_atlas;
                    $options_thumb_img['class']['div-thumbs']="div-thumbs-atlas";
                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
                    $html_print.="<div class='clear-tab'></div>\n";
                $html_print.="</div>\n";
            }
            # end atlas

            # associated atlas paintings
            if( $count_atlas_assoc>0 )
            {
                $html_print.="<div id='tab-associated-works-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    if( $options['row']['artworkID']==3 ) $html_print.="<p class='p-section-title'>".LANG_ARTWORK_DETAIL_VIEW_TAB_ATLAS_ASSOC."</p>";
                    else $html_print.="<p class='p-section-title'></p>";
                    $options_thumb_img=array();
                    $options_thumb_img=$options;
                    $options_thumb_img['results']=$results_atlas_assoc;
                    $options_thumb_img['show_title']=1;
                    $options_thumb_img['url']="/art/atlas/detail.php?number=".$options['row']['number']."&paintid=";
                    $options_thumb_img['thumbs_per_line']=4;
                    $options_thumb_img['values_from']=0;
                    $options_thumb_img['values_to']=$count_atlas_assoc;
                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
                    $html_print.="<div class='clear-tab'></div>\n";
                $html_print.="</div>\n";
            }
            # end associated atlas paintings

            # exhibitions
            //if( $count_exh>0 && $options['row']['artworkID']!=4 )
            if( $count_exh>0 )
            {
                $html_print.="<div id='tab-exhibitions-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']." tab-exhibitions'>\n";
                    $html_print.="<p class='p-section-title'>";
                        if( $options['bookid'] )
                        {
                            if( $count_exh>1 ) $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITIONS;
                            else $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_EXHIBITION;
                        }
                        elseif( $options['exhibitionid'] )
                        {
                            if( $count_exh==1 ) $s=LANG_EXH_FOUND;
                            else $s=LANG_EXH_FOUND_S;
                            if( $_GET['lang']!='de' ) $s=strtolower($s);
                            $desc_text=$count_exh." ".$s;
                        }
                        else $desc_text=LANG_ARTWORK_DETAIL_VIEW_TAB_EXH_ARTWROK_SHOWN;
                        $html_print.=$desc_text;
                    $html_print.="</p>";
                    $i=0;
                    while( $row_exh=$db->mysql_array($results_exh) )
                    {
                        $i++;
                        $options_exh=array();
                        $options_exh=$options;
                        $options_exh['class']['div-exhibition-right-side-search']="div-exhibition-right-side-search";
                        if( $options['content_large'] ) $options_exh['class']['a-exhibition-list-paint-detail']="a-exhibition-list-paint-detail";
                        $options_exh['class']['div-exhibition-relations']="div-exhibition-relations";
                        if( $i==1 ) $options_exh['class']['div-exhibition-relations-first']="div-exhibition-relations-first";
                        $options_exh['type']=1;
                        $options_exh['row']=$row_exh;
                        $options_exh['i']=$i;
                        $options_exh['count']=$count_exh;
                        if( $options['content_large'] )
                        {
                            $options_exh['class']['span-title-orig-length']="span-exh-list-title-original-wide";
                            $options_exh['class']['span-title-trans-length']="span-exh-list-title-translation-wide";
                            $options_exh['class']['span-location-length']="span-exh-list-location-wide";
                            $options_exh['class']['span-date-length']="span-exh-list-date-wide";
                        }
                        else
                        {
                            $options_exh['class']['span-title-orig-length']="span-exh-list-title-original-short";
                            $options_exh['class']['span-title-trans-length']="span-exh-list-title-translation-short";
                            $options_exh['class']['span-location-length']="span-exh-list-location-short";
                            $options_exh['class']['span-date-length']="span-exh-list-date-short";
                        }
                        $html_print.=$options['html_class']->div_exhibition($db,$options_exh);
                    }
                $html_print.="</div>\n";
            }
            # end exhibitions

            # INSTALLATION VIEWS TAB
            if( $count_installations_relations )
            {
                $html_print.="<div id='tab-installation-views-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $row_installations_relations=$db->mysql_array($results_installations_relations);
                    $installationid=UTILS::get_relation_id($db,"5",$row_installations_relations);

                    $query_where_installation_images=" WHERE ( typeid1=17 AND typeid2=5 AND itemid2='".$installationid."' ) OR ( typeid2=17 AND typeid1=5 AND itemid1='".$installationid."' )";
                    $query_order_installation_images=" ORDER BY sort ASC, relationid DESC ";
                    $query_installation_images=QUERIES::query_relations($db,$query_where_installation_images,$query_order_installation_images,$query_limit_images);
                    if( $_SESSION['debug_page'] ) $html_print.="<br /><br />".$query_installation_images['query'];
                    $results_installation_images=$db->query($query_installation_images['query']);
                    $results_inst_images=$db->query($query_installation_images['query_without_limit']);
                    $count_inst_images=$db->numrows($results_inst_images);

                    $html_print.="<div class='div-exhibitions-detail-tab-sort'>\n";
                        $html_print.="<p class='p-exh-detail-count-found-items'>";
                            if( $count_inst_images==1 ) $item_text_value=LANG_EXH_INST_TAB_PHOTO;
                            else $item_text_value=LANG_EXH_INST_TAB_PHOTOS;
                            $html_print.=$count_inst_images." ".$item_text_value;
                            # admin edit link
                            $options_admin_edit=array();
                            $options_admin_edit=$options;
                            $options_admin_edit['class']="";
                            $html_print.=UTILS::admin_edit("/admin/exhibitions/installations/edit/?installationid=".$installationid,$options_admin_edit);
                            # END admin edit link
                        $html_print.="</p>\n";
                        $html_print.="<div class='clearer'></div>\n";
                    $html_print.="</div>\n";

                    //if( $_GET['tab']=="artworks-tabs" )
                    //{
                        $_GET['sp']="100";
                        $_GET['p']=1;
                    //}

                    $pages=@ceil($count_inst_images/$_GET['sp']);

                    $options_page_numbering=array();
                    $options_page_numbering=$options;
                    $options_page_numbering['per_page']=0;
                    $options_page_numbering['pages']=$pages;
                    $options_page_numbering['count']=$count_inst_images;
                    $options_page_numbering['sp']=$_GET['sp'];
                    $options_page_numbering['p']=$_GET['p'];
                    $options_page_numbering['url']="/".$_GET['lang']."/exhibitions/".$_GET['section_1']."/?tab=installation-views-tabs";
                    $options_page_numbering['url_after']="&sp=".$_GET['sp']."#tabs";
                    $options_page_numbering['url_after_per_page'].="#tabs";
                    $options_page_numbering['class']['div-per-page']="div-per-page-exh";
                    $options_page_numbering['class']['div-page-navigation']="div-page-navigation-exh";
                    $html_print.=$options['html_class']->page_numbering($db,$options_page_numbering);

                    $options_thumb_img=array();
                    $options_thumb_img=$options;
                    $options_thumb_img['results']=$results_inst_images;
                    $options_thumb_img['installationid']=$installationid;
                    $options_thumb_img['show_title']=0;
                    $options_thumb_img['show_a_title']=1;
                    //$options_thumb_img['url']="/images/size_xl__imageid_";
                    $options_thumb_img['url']=DATA_PATH_DATADIR."/images_new/xlarge/";
                    $options_thumb_img['url_image']=HTTP_SERVER."/".$_GET['lang']."/exhibitions/".$_GET['section_1'];
                    $options_thumb_img['url_image_after']="/?tab=installation-views-".$name_tabs."&installation-photo=";
                    $options_thumb_img['fancybox']=1;
                    $options_thumb_img['thumbs_per_line']=$options['thumb_per_line_installattion_photos'];
                    if( $_GET['tab']=="installation-views-tabs" && ( !empty($_GET['sp']) || !empty($_GET['p']) ) )
                    {
                        $sp=$_GET['sp'];
                        $p=$_GET['p'];
                    }
                    else
                    {
                        $sp=SHOW_PER_PAGE;
                        $p=1;
                    }
                    $options_thumb_img['values_from']=($sp*($p-1));
                    $options_thumb_img['values_to']=$sp*$p;
                    if( $_GET['sp']=='all' ) $options_thumb_img['show_all']=1;
                    if( $options_thumb_img['values_to']>$count_inst_images ) $options_thumb_img['values_to']=$count_inst_images;
                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);

                    $html_print.=$options['html_class']->page_numbering($db,$options_page_numbering);

                $html_print.="</div>\n";

                # if direct link passed in url open this image directly on load
                if( !empty($_GET['installation-photo']) )
                {
                    $html_print.="<script type='text/javascript'>\n";
                        $html_print.="$(document).ready(function() {\n";
                        	/*
                            $html_print.="$('.fancybox-buttons').fancybox({";
                                $html_print.="openEffect  : 'none',";
                                $html_print.="closeEffect : 'none',";
                                $html_print.="prevEffect : 'none',";
                                $html_print.="nextEffect : 'none',";
                                $html_print.="loop : 'false',";
                                $html_print.="closeBtn  : false,";
                                $html_print.="helpers : { ";
                                    $html_print.="title : { ";
                                        $html_print.="type : 'inside'";
                                    $html_print.="},";
                                    $html_print.="buttons : {}";
                                $html_print.="},";
                                $html_print.="afterLoad : function() {";
                                    $html_print.="window.link_url=$(this.element).find('img').attr('alt');";
                                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + '<div id=\"div-fancybox-title-path\" class=\"div-fancybox-title-path\"><a class=\"a-copy-to-clipboard\" onclick=\"copyTextToClipboard(link_url);\" >".LANG_COPY_TO_CLIPBOARD."</a></div><div class=\"clearer\"></div>';";
                                $html_print.="}";
                            $html_print.="});\n";
                            */
                            $html_print.="$('#installation-photo-".$_GET['installation-photo']."').trigger('click');";
                        $html_print.="});\n";
                    $html_print.="</script>\n";
                }
                #end
            }
            # end INSTALLATION VIEWS TAB

            # literature
            if( $count_literature>0 )
            {
                $html_print.="<div id='tab-literature-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $html_print.="<p class='p-section-title'>";
                        if( $options['bookid'] )
                        {
                            if( $count_literature>1 ) $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_LITERATURES;
                            else $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_LITERATURE;
                        }
                        elseif( $options['exhibitionid'] || !empty($options['videoid']) )
                        {
                            if( $count_literature==1 ) $s=LANG_TABLE_DESC_PUBLICATION;
                            else $s=LANG_TABLE_DESC_PUBLICATIONS;
                            if( $_GET['lang']=="it" ) $desc_text=$count_literature." ".$s." ".LANG_TABLE_DESC_PUBLICATION_RELATED;
                            else $desc_text=$count_literature." ".LANG_TABLE_DESC_PUBLICATION_RELATED." ".$s;
                        }
                        elseif( !empty($options['videoid']) )
                        {
                            $tab_title_literature=LANG_TAB_TITLE_RELATED_PUBLICATIONS;
                        }
                        else
                        {
                            if( $count_literature==1 ) $desc_text=LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDED;
                            else $desc_text=LANG_ARTWORK_DETAIL_VIEW_TAB_PUBL_INCLUDEDS;
                        }
                        $html_print.=$desc_text;
                    $html_print.="</p>";

    /*
    if( empty($_GET['sort']) )
    {
        $_GET['sort']=3;
        //$_GET['sort']=4;
        $_GET['sort_how']=-1;
    }
    else
    {
        $tmp=explode(",", $_GET['sort']);
        $_GET['sort']=$tmp[0];
        if( !empty($tmp[1]) ) $_GET['sort_how']=$tmp[1];
        else $_GET['sort_how']=1;
    }
    */

    if( $options['exhibitionid'] )
    {
        //$options['class']['expand-box-exh']="expand-box-exh";
        //$options['class']['expand-box-details-exh']="expand-box-details-exh";
        $options['class']['p-section-desc-text']="p-section-title";

        /*
        //!!!!!!!!!!!!
        $exhibition_painting_url='/exhibitions/'.$_GET['sectionurl'];
        if( !empty($titleurl_painting) ) $exhibition_painting_url.='/'.$titleurl_painting;
        $exhibition_painting_url.='/?tab=literature&sort=';
        */
    }

    if( $options['painting_detail_view_tab'] ) $id_table="expandlist_literature_02";
    else $id_table="expandlist_literature_01";

    $options_table = array(
        'options'       =>  array(
            // Setup table options here...
            'id'                => $id_table,      // set DOM node id for js (uniqid() if not set)
            'sort_by'   => array($_GET['sort'], $_GET['sort_how']), // default column sorted
            'paintid'        => $itemid,                            // Allow tr tags to expand (expand-boxes required)
            'expandable'        => 1,                            // Allow tr tags to expand (expand-boxes required)
            'class'        => $options['class'],                            // Allow tr tags to expand (expand-boxes required)
            'highlight'         =>  $_GET['search'],                // highlight patterns in content
            'html_class'         =>  $options['html_class'],
            'html_return'         =>  $options['html_return'],
            //'desc_text'         =>  $desc_text,
            //'onheaderclick'     => $_SERVER['PATH_INFO'] . '?sort='
            'onheaderclick'     => $options['url_painting'] . $options['tab'] .'=literature-'.$name_tabs.'&sort='
        )
        /*
        'cols'          =>  array(
            // Define columns here...
            array( 'caption' => "",                       'width' => '10%',   'col' => '0',   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' ),
            array( 'caption' => LANG_LIT_SEARCH_TITLE,    'width' => '30%',   'col' => '1',   'sortable' => true, 'sort_col' => 'b.title',     'content_align' => '' ),
            array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => '2',   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' ),
            array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => '3',   'sortable' => true, 'sort_col' => 'b.link_date',     'content_align' => '' )
        )
        */
    );

    $options_table['cols']=array();
    $i_col=0;
    $options_table['cols'][]=array( 'caption' => "",                       'width' => '10%',   'col' => $i_col,   'sortable' => false, 'sort_col' => 'b.title',     'content_align' => 'centered' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_TITLE,    'width' => '38%',   'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.title',     'content_align' => '' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '*',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' );
    /*
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_AUTHOR,   'width' => '29%',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.author',     'content_align' => '' );
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_RIGHT_SEARCH_LIT_CATEGORY,   'width' => '*',     'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.books_catid',     'content_align' => '' );
    */
    $i_col++;
    $options_table['cols'][]=array( 'caption' => LANG_LIT_SEARCH_DATE,     'width' => '11%',   'col' => $i_col,   'sortable' => true, 'sort_col' => 'b.link_date',     'content_align' => '' );

    # get literature data for table
    $options_table_data=array();
    $options_table_data['html_return']=$options['html_return'];
    $options_table_data['cols']=$options_table['cols'];
    $options_table_data['sort']=$options_table['options']['sort_by'];
    $options_table_data['highlight']=$_GET['search'];
    $table_data = new Table_Data($db,$options_table_data);
    $options_table_data=array();
    $options_table_data['results']=$results_literature;
    $rows=$table_data->table_data_literature($options_table_data);
    # END get literature data fro table

    $table = new Table($db, $rows, $options_table);
    $html_print.=$table->draw_table();

                $html_print.="</div>\n";
            }
            # end literature

            # videos
            if( $count_videos>0 )
            {
                $html_print.="<div id='tab-videos-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $html_print.="<p class='p-section-title'>";
                        if( $options['bookid'] )
                        {
                            if( $count_videos>1 ) $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_VIDEOS;
                            else $desc_text=LANG_LITERATURE_TAB_DESC_RELATED_VIDEO;
                        }
                        elseif( $options['exhibitionid'] )
                        {
                            $desc_text=LANG_VIDEO_EXHIBITION_MENTIONED;
                        }
                        else $desc_text=LANG_VIDEO_ARTWORK_MENTIONED;
                        $html_print.=$desc_text;
                    $html_print.="</p>";
                    $options_videos=array();
                    $options_videos=$options;
                    $options_videos['class']['videos-tab']="videos-tab";
                    $options_videos['class']['video-tab']="video-tab";
                    $options_videos['results']=$results_videos;
                    $options_videos['values_from']=0;
                    $options_videos['values_to']=$count_videos;
                    if( !empty($options['thumb_per_line_videos']) ) $options_videos['thumbs_per_line']=$options['thumb_per_line_videos'];
                    else $options_videos['thumbs_per_line']=4;
                    $html_print.=$options['html_class']->div_videos($db,$options_videos);
                $html_print.="</div>\n";
            }
            #end videos

            # painting_images
            if( $count_painting_images>0 )
            {
                $html_print.="<div id='tab-photos-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    if( !empty($options['thumb_per_line_photos']) ) $thumbs_per_line_photos=$options['thumbs_per_line_photos'];
                    else $thumbs_per_line_photos=4;
                    while( $row_painting_images=$db->mysql_array($results_painting_images) )
                    {
                        if( $row_painting_images['typeid']==1 ) $photos_details[]=$row_painting_images;
                        elseif( $row_painting_images['typeid']==2 ) $photos_inst_shots[]=$row_painting_images;
                    }
                    if( !empty($photos_details) )
                    {
                        $html_print.="<p class='p-section-title'>".LANG_ARTWORK_DETAIL_VIEW_TAB_PHOTOGRAPHS."</p>\n";

	                    $options_thumb_img=array();
	                    $options_thumb_img=$options;
	                    $options_thumb_img['results']=$photos_details;
	                    $options_thumb_img['show_title']=0;
	                    $options_thumb_img['show_a_title']=1;
	                    $options_thumb_img['url']=DATA_PATH_DATADIR."/painting/photos/large/";
	                    $options_thumb_img['url_image']=HTTP_SERVER.$options['url_painting'];
	                    $options_thumb_img['url_image_after']="&tab=photos-".$name_tabs."&painting-photo=";
	                    $options_thumb_img['fancybox']=1;
	                    //$options_thumb_img['thumbs_per_line']=$options['thumb_per_line_installattion_photos'];
	                    $options_thumb_img['thumbs_per_line']=4;
	                    $options_thumb_img['values_from']=0;
	                    $options_thumb_img['values_to']=count($photos_details);
	                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
	                    $html_print.="<div class='clearer'></div>\n";

                        /*
                        $html_print.="<div class='div-painting-photo-tab-images ".$options['class']['div-painting-photo-tab-images-exh']."'>\n";
                            $i=0;
                            $ii=0;
                            foreach( $photos_details as $row_painting_photo )
                            {
                                $i++;
                                $ii++;
                                $href="/images/paintingphotoid_".$row_painting_photo['painting_photoid']."__size_l.jpg";
                                $src=DATA_PATH_DATADIR."/painting/photos/cache/".$row_painting_photo['painting_photoid'].".png";
                                if( $ii==count($photos_details) || $i==$thumbs_per_line_photos )
                                {
                                    $i=0;
                                    $class="last";
                                    $br="<div class='clearer'></div>";
                                }
                                else
                                {
                                    $class="";
                                    $br="";
                                }
                                $html_print.="<div class='div-painting-photo-tab-image ".$class."'>\n";
                                    $html_print.="<a href='".$href."' title='' data-fancybox-group='paint-photos' class='fancybox-buttons'>\n";
                                        $html_print.="<div class='div-painting-photo-tab-image-border'>\n";
                                            $html_print.="<img src='".$src."' alt='' />\n";

                                        $html_print.="</div>\n";
                                    $html_print.="</a>\n";
                                    $options_admin_edit=array();
                                    $options_admin_edit=$options;
                                    $options_admin_edit['class']="a-admin-edit-paint-detail-photos-tab";
                                    $html_print.=UTILS::admin_edit("/admin/paintings/photos/edit/?painting_photoid=".$row_painting_photo['painting_photoid'],$options_admin_edit);
                                $html_print.="</div>\n";
                                $html_print.=$br;
                            }
                            $html_print.="<div class='clearer'></div>\n";
                        $html_print.="</div>\n";
						*/

                    }

                    if( !empty($photos_inst_shots) )
                    {
                        $html_print.="<p class='p-section-title'>".LANG_ARTWORK_PHOTOS_TAB_INSTAL_SHOTS."</p>\n";

	                    $options_thumb_img=array();
	                    $options_thumb_img=$options;
	                    $options_thumb_img['results']=$photos_inst_shots;
	                    $options_thumb_img['show_title']=0;
	                    $options_thumb_img['show_a_title']=1;
	                    $options_thumb_img['url']=DATA_PATH_DATADIR."/painting/photos/large/";
	                    $options_thumb_img['url_image']=HTTP_SERVER.$options['url_painting'];
	                    $options_thumb_img['url_image_after']="&tab=photos-".$name_tabs."&painting-photo=";
	                    $options_thumb_img['fancybox']=1;
	                    //$options_thumb_img['thumbs_per_line']=$options['thumb_per_line_installattion_photos'];
	                    $options_thumb_img['thumbs_per_line']=4;
	                    $options_thumb_img['values_from']=0;
	                    $options_thumb_img['values_to']=count($photos_inst_shots);
	                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
	                    $html_print.="<div class='clearer'></div>\n";

                        /*
                        $html_print.="<div class='div-painting-photo-tab-images ".$options['class']['div-painting-photo-tab-images-exh']."'>\n";
                            $i=0;
                            $ii=0;
                            foreach( $photos_inst_shots as $row_painting_photo )
                            {
                                $i++;
                                $ii++;
                                $href="/images/paintingphotoid_".$row_painting_photo['painting_photoid']."__size_l.jpg";
                                $src=DATA_PATH_DATADIR."/painting/photos/cache/".$row_painting_photo['painting_photoid'].".png";
                                if( $ii==count($photos_details) || $i==$thumbs_per_line_photos )
                                {
                                    $i=0;
                                    $class="last";
                                    $br="<div class='clearer'></div>";
                                }
                                else
                                {
                                    $class="";
                                    $br="";
                                }
                                $html_print.="<div class='div-painting-photo-tab-image ".$class."'>\n";
                                    $html_print.="<a href='".$href."' title='' data-fancybox-group='paint-photos' class='fancybox-buttons'>\n";
                                        $html_print.="<div class='div-painting-photo-tab-image-border'>\n";
                                            $html_print.="<img src='".$src."' alt='' />\n";
                                        $html_print.="</div>\n";
                                    $html_print.="</a>\n";
                                    $options_admin_edit=array();
                                    $options_admin_edit=$options;
                                    $options_admin_edit['class']['a-admin-edit-paint-detail-photos-tab']="a-admin-edit-paint-detail-photos-tab";
                                    $html_print.=UTILS::admin_edit("/admin/paintings/photos/edit/?painting_photoid=".$row_painting_photo['painting_photoid'],$options_admin_edit);
                                $html_print.="</div>\n";
                                $html_print.=$br;
                            }
                            $html_print.="<div class='clearer'></div>\n";
                        $html_print.="</div>\n";
                        */
                    }

	                # if direct link passed in url open this image directly on load
	                if( !empty($_GET['painting-photo']) )
	                {
	                    $html_print.="<script type='text/javascript'>\n";
	                        $html_print.="$(document).ready(function() {\n";
								/*
	                            $html_print.="$('.fancybox-buttons').fancybox({";
	                                $html_print.="openEffect  : 'none',";
	                                $html_print.="closeEffect : 'none',";
	                                $html_print.="prevEffect : 'none',";
	                                $html_print.="nextEffect : 'none',";
	                                $html_print.="loop : 'false',";
	                                $html_print.="closeBtn  : false,";
	                                $html_print.="helpers : { ";
	                                    $html_print.="title : { ";
	                                        $html_print.="type : 'inside'";
	                                    $html_print.="},";
	                                    $html_print.="buttons : {}";
	                                $html_print.="},";
	                                $html_print.="afterLoad : function() {";
	                                    $html_print.="window.link_url=$(this.element).find('img').attr('alt');";
	                                    $html_print.="this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' ) + '<div id=\"div-fancybox-title-path\" class=\"div-fancybox-title-path\"><a class=\"a-copy-to-clipboard\" onclick=\"copyTextToClipboard(link_url);\" >".LANG_COPY_TO_CLIPBOARD."</a></div><div class=\"clearer\"></div>';";
	                                $html_print.="}";
	                            $html_print.="});\n";
	                            */
	                            $html_print.="$('#painting-photo-".$_GET['painting-photo']."').trigger('click');";
	                        $html_print.="});\n";
	                    $html_print.="</script>\n";
	                }
	                #end

                $html_print.="</div>\n";
            }
            # end painting_images

            # individual works
            if( $count_editions_ind>0 )
            {
                $html_print.="<div id='tab-individual-works-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']."'>\n";
                    $html_print.="<p class='p-section-title'>".LANG_ARTWORK_INDIVIDUAL_WORKS_TAB_DESC."</p>";
                    $options_thumb_img=array();
                    $options_thumb_img=$options;
                    $options_thumb_img['results']=$results_editions_ind;
                    $options_thumb_img['show_title']=1;
                    $options_thumb_img['url']="/art/editions/".$options['row']['titleurl_en']."/individual-works";
                    $options_thumb_img['htaccess']=1;
                    $options_thumb_img['thumbs_per_line']=4;
                    $options_thumb_img['values_from']=0;
                    $options_thumb_img['values_to']=$count_editions_ind;
                    $html_print.=$options['html_class']->div_thumb_images($db,$options_thumb_img);
                    $html_print.="<div class='clear-tab'></div>\n";
                $html_print.="</div>\n";
            }
            # end individual works

            # Exhibition guide
            if( !empty($options['row']['src_guide']) )
            {
                $html_print.="<div id='tab-guide-".$name_tabs."' class='".$options['class']['div-relation-tabs-info']." div-exhibitions-detail-tab-info-guide'>\n";
                //$html_print.="<div id='tabs-6' class='div-exhibitions-detail-tab-info div-exhibitions-detail-tab-info-guide'>\n";
                    $html_print.="<p>".$options['row']['guide_desc']."</p>\n";

                    $src=DATA_PATH_DISPLAY."/files/guides/".$options['row']['src_guide'];
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $mime=@finfo_file($finfo, $src);
                    finfo_close($finfo);
                    $tmp=explode(".", $options['row']['src_guide']);
                    $ext=$tmp[count($tmp)-1];

                    $html_print.="<a href='/".$_GET['lang']."/exhibitions/guide/exhibition-guide-".$options['row']['titleurl'].".".$ext."' title='' target='_blank'><u>Guide</u></a>\n";

                    $html_print.="<div class='clear-tab'></div>\n";
                $html_print.="</div>\n";
            }
            # END Exhibition guide

        $html_print.="</div>\n";
    }

    if( $options['return'] ) return $return;
    elseif( $options['html_return'] ) return $html_print;
    else print $html_print;
}
?>
